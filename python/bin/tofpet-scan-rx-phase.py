#!/usr/bin/env python3

import matplotlib.pyplot as plt
from scifi_daq.daq_board.daq_board import DaqBoard
import argparse

def main():
  """ Performs a scan of the TOFPET clock phase to determine the best value.
  This script allows to determine the best value of the TOFPET clock phase, which depends on the lenght of the cables connecting the DAQ board to the TOFPET boards.
  It will show two plots, one with the number of packets received at each phase step, one with the number of errors.
  You have to choose a phase that is as far as possible from the minimum of the number of packets and the maximum of the errors (they usually coincide, but that's not always the case).
  For example, if the minimum of the number of packets received is at a phase of 8 and the maximum of the errors is at a phase of 10, you select 1 as phase and write this value in the board.toml configuration, 'clock_tofpet_phase = 1'.
  """
  parser = argparse.ArgumentParser()
  parser.add_argument('address', type=str)
  parser.add_argument('--port', '-p', type=int, default=41968)
  args = parser.parse_args()

  d = DaqBoard(None, args.address, port=args.port)
  d.connect()
  print('Scanning, please wait...')
  rcvPackets, errors = d.scanClkTofpetPhase()

  fig, host = plt.subplots()
      
  par1 = host.twinx()  
      
  host.set_xlabel("Phase")
  host.set_ylabel("Packets")
  par1.set_ylabel("Errors")

  p1, = host.plot(rcvPackets, color='C0', label="Packets")
  p2, = par1.plot(errors, color='C1', label="Errors")

  lns = [p1, p2]
  host.legend(handles=lns, loc='best')
  host.grid(alpha=0.3)

  plt.show()

if __name__ == '__main__':
  main()