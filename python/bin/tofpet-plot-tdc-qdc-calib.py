#!/usr/bin/env python3

import argparse
import numpy as np
import matplotlib.pyplot as plt
from scifi_daq.daq_board.daq_board import DaqBoard
from scifi_daq.daq_board.daq_board_multi import DaqBoards
import logging
from scifi_daq.daq_board._calibration._calibration_functions import *
import fnmatch
import os.path as op


def main():
  logging.basicConfig(level=logging.ERROR)

  parser = argparse.ArgumentParser()
  parser.add_argument('conf_path', type=str, help='Path to the configuration board folder OR to the detector configuration folder (the one that contains configuration.toml).')
  parser.add_argument('--board-names', nargs = '+', type = str, help = 'Name of the boards you want to consider.')
  parser.add_argument('--board-ids', nargs = '+', type = int, help = 'IDs of the boards you want to consider.')
  parser.add_argument('--tofpet-ids', nargs = '+', type=int, help = 'TOFPET IDs to be displayed. If unspecified, all TOFPETs will be shown.')
  args = parser.parse_args()

  args.conf_path = op.normpath(args.conf_path)

  # case where conf_path points to a full detector configuration
  if op.exists(op.join(args.conf_path, 'configuration.toml')):
    # get the desired board IDs
    boardIds = args.board_ids if args.board_ids is not None else []
    
    # get the IDs corresponding to the desired board names
    d = DaqBoards(args.conf_path)
    if args.board_names is not None:
      boardNames = d.boardNames()
      for bname in args.board_names:
        for bn, bid in boardNames.items():
          if fnmatch.fnmatch(bn, bname):
            boardIds.append(bid)
    
    # if the user selected at least one board, use that, otherwise keep all of them
    if boardIds:
      d = DaqBoards(args.conf_path, subset=boardIds)
    selectedBoards = list(d._boards.values())
    del d
  # case where conf_path points to a single board configuration
  else:
    if not op.exists(op.join(args.conf_path, 'board.toml')):
      raise OSError(f'File {op.join(args.conf_path, "board.toml")} or {op.join(args.conf_path, "configuration.toml")} do not exist. conf_path must correspond to the board or detector configuration directory.')
    
    selectedBoards = [DaqBoard(args.conf_path, None)]
    # in this case the name is not known. Use the folder name
    selectedBoards[0].name = op.basename(args.conf_path)
  
  if args.tofpet_ids is None:
    args.tofpet_ids = [0, 1, 2, 3, 4, 5, 6, 7]

  for board in selectedBoards:
    board.readConfiguration(None)

    for tp in args.tofpet_ids:
      
      tdcTitle = f'{board.name} TDC TOFPET {tp}' if board.boardId is None else f'{board.name} (ID: {board.boardId}) TDC TOFPET {tp}'
      qdcTitle = f'{board.name} QDC TOFPET {tp}' if board.boardId is None else f'{board.name} (ID: {board.boardId}) QDC TOFPET {tp}'

      figTdc, axTdc = plt.subplots(num=tdcTitle)
      axTdc.set_title(tdcTitle)
      axTdc.set_xlabel('Phase [clock cycles]')
      axTdc.set_ylabel('v_fine')
      axTdc.grid(alpha = 0.3)
      # axTdc.canvas.manager.set_window_title(f'{name} TDC calibration, TOFPET {tp}')
      
      figQdc, axQdc = plt.subplots(num=qdcTitle)
      axQdc.set_title(qdcTitle)
      axQdc.set_xlabel('v_coarse')
      axQdc.set_ylabel('v_fine')
      axQdc.grid(alpha = 0.3)
      
      #get and sort all Tdc data
      dfTdc = board.allTdcCalData(tp)
      groups = dfTdc.groupby(['tofpet_id', 'tofpet_channel', 'tac'], sort=True)

      #plot each channel and tac line for one tofpet ID
      for (tpId, ch, tac), data in groups:
        # groups by phase and calculate mean, std deviation and number of entries in each group
        gp = data.groupby('phase')
        df_mean = gp.mean()
        df_std = gp.std()
        df_count = gp.count()

        x = np.linspace(0., 4., 224, endpoint=False)
        y_t = df_mean.t_fine
        axTdc.plot(x, y_t)
        
      dfQdc = board.allQdcCalData(tp)
     
      tdcCal = board.getTdc(tp)
      tdcCalArr = np.empty((8, 64, 4, 2, 6)) # tofpet_id, tofpet_channel, tac, tdc, params
      tdcCalArr[:] = np.nan
      for c in tdcCal.itertuples(index=False, name=None):
          i = c[0:4]
          tdcCalArr[i] = c[4:10]
      dfQdc['ts_fine'] = calculateTimestampFine(dfQdc.t_fine, *(tdcCalArr[dfQdc.tofpet_id, dfQdc.tofpet_channel, dfQdc.tac][:, 0][:, 0:4].T))
      
      cut = (dfQdc.v_fine > 0) & (dfQdc.v_coarse < 200)
      groups_Q = dfQdc[cut].groupby(['tofpet_id', 'tofpet_channel', 'tac'], sort=True)
      
      for (tpId, ch, tac), data in groups_Q:
            x = (data.v_coarse - data.ts_fine).to_numpy()
            y = data.v_fine.to_numpy()
            axQdc.plot(x, y)
          
      if not dfQdc.empty:
        plt.show() 
      else:
        #plt.close(df_T)
        plt.close('all')
        print(f'No TOFPET {tp}') 
        continue
  
 
if __name__ == '__main__':
  main()