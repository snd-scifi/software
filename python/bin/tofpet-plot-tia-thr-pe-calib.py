#!/usr/bin/env python3

import argparse
import matplotlib.pyplot as plt
from scifi_daq.daq_board.daq_board import DaqBoard
import logging
import os.path as op
from scifi_daq.daq_board.daq_board_multi import DaqBoards
import fnmatch



def main():
  logging.basicConfig(level=logging.ERROR)

  parser = argparse.ArgumentParser()
  parser.add_argument('conf_path', type=str, help='Path to the configuration board folder OR to the detector configuration folder (the one that contains configuration.toml).')
  parser.add_argument('--board-names', nargs = '+', type = str, help = 'Name of the boards you want to consider.')
  parser.add_argument('--board-ids', nargs = '+', type = int, help = 'IDs of the boards you want to consider.')
  parser.add_argument('--tofpet-ids', nargs = '+', type=int, help = 'TOFPET IDs to be displayed. If unspecified, all TOFPETs will be shown.')
  args = parser.parse_args()

  args.conf_path = op.normpath(args.conf_path)

  # case where conf_path points to a full detector configuration
  if op.exists(op.join(args.conf_path, 'configuration.toml')):
    # get the desired board IDs
    boardIds = args.board_ids if args.board_ids is not None else []
    
    # get the IDs corresponding to the desired board names
    d = DaqBoards(args.conf_path)
    if args.board_names is not None:
      boardNames = d.boardNames()
      for bname in args.board_names:
        for bn, bid in boardNames.items():
          if fnmatch.fnmatch(bn, bname):
            boardIds.append(bid)
    
    # if the user selected at least one board, use that, otherwise keep all of them
    if boardIds:
      d = DaqBoards(args.conf_path, subset=boardIds)
    selectedBoards = list(d._boards.values())
    del d
  # case where conf_path points to a single board configuration
  else:
    if not op.exists(op.join(args.conf_path, 'board.toml')):
      raise OSError(f'File {op.join(args.conf_path, "board.toml")} or {op.join(args.conf_path, "configuration.toml")} do not exist. conf_path must correspond to the board or detector configuration directory.')
    
    selectedBoards = [DaqBoard(args.conf_path, None)]
    # in this case the name is not known. Use the folder name
    selectedBoards[0].name = op.basename(args.conf_path)
  
  if args.tofpet_ids is None:
    args.tofpet_ids = [0, 1, 2, 3, 4, 5, 6, 7]

  for board in selectedBoards:
    board.readConfiguration(None)
    
    for tp in args.tofpet_ids:

      thrScanTitle = f'{board.name} Threshold scan, TOFPET {tp}' if board.boardId is None else f'{board.name} (ID: {board.boardId}) Threshold scan, TOFPET {tp}'
      tiaThrBlScanTitle = f'{board.name} TIA and threshold baseline calibration, TOFPET {tp}' if board.boardId is None else f'{board.name} (ID: {board.boardId}) TIA and threshold baseline calibration, TOFPET {tp}'

      fig, ax = plt.subplots(3, 2, figsize = (7, 7), num=tiaThrBlScanTitle)
      fig.suptitle(tiaThrBlScanTitle)

      ax[0, 0].set_title('T1 - TIA baseline')
      ax[1, 0].set_title('T2 - TIA baseline')
      ax[2, 0].set_title('E - TIA baseline')
      ax[0, 1].set_title('T1 - THR baseline')
      ax[1, 1].set_title('T2 - THR baseline')
      ax[2, 1].set_title('E - THR baseline')

      ax[0, 0].set_xlabel('TIA Baseline [DAC counts]')
      ax[1, 0].set_xlabel('TIA Baseline [DAC counts]')
      ax[2, 0].set_xlabel('TIA Baseline [DAC counts]')
      ax[0, 1].set_xlabel('Raw Threshold [DAC counts]')
      ax[1, 1].set_xlabel('Raw Threshold [DAC counts]')
      ax[2, 1].set_xlabel('Raw Threshold  [DAC counts]')

      ax[0, 0].set_ylabel('counts')
      ax[1, 0].set_ylabel('counts')
      ax[2, 0].set_ylabel('counts')
      ax[0, 1].set_ylabel('counts')
      ax[1, 1].set_ylabel('counts')
      ax[2, 1].set_ylabel('counts')

      ax[0, 0].grid(alpha = 0.3)
      ax[1, 0].grid(alpha = 0.3)
      ax[2, 0].grid(alpha = 0.3)
      ax[0, 1].grid(alpha = 0.3)
      ax[1, 1].grid(alpha = 0.3)
      ax[2, 1].grid(alpha = 0.3)

      fig2, ax2 = plt.subplots(1, 3, figsize = (13, 6), num=thrScanTitle)
      fig2.suptitle(thrScanTitle)
      
      ax2[0].set_yscale('log')
      ax2[1].set_yscale('log')
      ax2[2].set_yscale('log')

      ax2[0].set_title('T1 - Threshold scan')
      ax2[1].set_title('T2 - Threshold scan')
      ax2[2].set_title('E - Threshold scan')

      ax2[0].set_xlabel('Threshold [DAC Counts]')
      ax2[1].set_xlabel('Threshold [DAC Counts]')
      ax2[2].set_xlabel('Threshold [DAC Counts]')

      ax2[0].set_ylabel('Rate [Hz]')
      ax2[1].set_ylabel('Rate [Hz]')
      ax2[2].set_ylabel('Rate [Hz]')

      ax2[0].grid(alpha = 0.3)
      ax2[1].grid(alpha = 0.3)
      ax2[2].grid(alpha = 0.3)
    
      #get channels from Tia Cal and Thr Cal
      try: 
        counts_arr_T1, channels_arr_T1 = board.getTiaCalData('T1', tp)
        counts_arr1_T2, channels_arr1_T2 = board.getTiaCalData('T2', tp)
        counts_arr2_E, channels_arr2_E = board.getTiaCalData('E', tp)

        Tcounts_arr_T1, Tchannels_arr_T1 = board.getThrCalData('T1', tp)
        Tcounts_arr1_T2, Tchannels_arr1_T2 = board.getThrCalData('T2', tp)
        Tcounts_arr2_E, Tchannels_arr2_E = board.getThrCalData('E', tp)

        #threshold counts: pe_cal
        Prate_arr_T1, Pchannels_arr_T1 = board.getPeCalData('T1', tp)
        Prate_arr1_T2, Pchannels_arr1_T2 = board.getPeCalData('T2',tp)
        Prate_arr2_E, Pchannels_arr2_E = board.getPeCalData('E', tp)
      except Exception: 
          print(f'No tofpet number {tp} ({board.name})')
          plt.close(fig)
          plt.close(fig2)
          continue
           
      
      # use w enumerate:  for i, channel in enumerate(..):
    #amplifier calibratrion
      lines_T1 = []
      for i in range(len(channels_arr_T1)):
        l, = ax[0,0].plot(counts_arr_T1[i], label=channels_arr_T1[i])
        lines_T1.append(l)

      lines_T2 = []
      for i in range(len(channels_arr1_T2)):
        l, = ax[1,0].plot(counts_arr1_T2[i], label=channels_arr1_T2[i])
        lines_T2.append(l)

      lines_E = []
      for i in range(len(channels_arr2_E)):
        l, = ax[2,0].plot(counts_arr2_E[i], label=channels_arr2_E[i])
        lines_E.append(l)

    #threshold calibration 
      Tlines_T1 = []
      for i in range(len(Tchannels_arr_T1)):
        l, = ax[0,1].plot(Tcounts_arr_T1[i], label=Tchannels_arr_T1[i])
        Tlines_T1.append(l)

      Tlines_T2 = []
      for i in range(len(Tchannels_arr1_T2)):
        l, = ax[1,1].plot(Tcounts_arr1_T2[i], label=Tchannels_arr1_T2[i])
        Tlines_T2.append(l)

      Tlines_E = []
      for i in range(len(Tchannels_arr2_E)):
        l, = ax[2,1].plot(Tcounts_arr2_E[i], label=Tchannels_arr2_E[i])
        Tlines_E.append(l)


      #Pe threshold 
      Plines_T1 = []
      for i in range(len(Pchannels_arr_T1)):
        l, = ax2[0].plot(Prate_arr_T1[i], label=Pchannels_arr_T1[i])
        Plines_T1.append(l)

      Plines_T2 = []
      for i in range(len(Pchannels_arr1_T2)):
        l, = ax2[1].plot(Prate_arr1_T2[i], label=Pchannels_arr1_T2[i])
        Plines_T2.append(l)

      Plines_E = []
      for i in range(len(Pchannels_arr2_E)):
        l, = ax2[2].plot(Prate_arr2_E[i], label=Pchannels_arr2_E[i])
        Plines_E.append(l)

    
        
      #('check')
    #amplifier 
      annot_T1 = ax[0,0].annotate("", xy=(0, 0), xytext=(20, 20), textcoords="offset points",
                            bbox=dict(boxstyle="round", fc="w"),
                            arrowprops=dict(arrowstyle="->"))
      annot_T1.set_visible(False)

      annot_T2 = ax[1,0].annotate("", xy=(0, 0), xytext=(20, 20), textcoords="offset points",
                            bbox=dict(boxstyle="round", fc="w"),
                            arrowprops=dict(arrowstyle="->"))
      annot_T2.set_visible(False)

      annot_E = ax[2,0].annotate("", xy=(0, 0), xytext=(20, 20), textcoords="offset points",
                            bbox=dict(boxstyle="round", fc="w"),
                            arrowprops=dict(arrowstyle="->"))
      annot_E.set_visible(False)
    #threshold 
      Tannot_T1 = ax[0,1].annotate("", xy=(0, 0), xytext=(20, 20), textcoords="offset points",
                            bbox=dict(boxstyle="round", fc="w"),
                            arrowprops=dict(arrowstyle="->"))
      Tannot_T1.set_visible(False)

      Tannot_T2 = ax[1,1].annotate("", xy=(0, 0), xytext=(20, 20), textcoords="offset points",
                            bbox=dict(boxstyle="round", fc="w"),
                            arrowprops=dict(arrowstyle="->"))
      Tannot_T2.set_visible(False)

      Tannot_E = ax[2,1].annotate("", xy=(0, 0), xytext=(20, 20), textcoords="offset points",
                            bbox=dict(boxstyle="round", fc="w"),
                            arrowprops=dict(arrowstyle="->"))
      Tannot_E.set_visible(False)

    #Pe
      #threshold 
      Pannot_T1 = ax2[0].annotate("", xy=(0, 0), xytext=(20, 20), textcoords="offset points",
                            bbox=dict(boxstyle="round", fc="w"),
                            arrowprops=dict(arrowstyle="->"))
      Pannot_T1.set_visible(False)

      Pannot_T2 = ax2[1].annotate("", xy=(0, 0), xytext=(20, 20), textcoords="offset points",
                            bbox=dict(boxstyle="round", fc="w"),
                            arrowprops=dict(arrowstyle="->"))
      Pannot_T2.set_visible(False)

      Pannot_E = ax2[2].annotate("", xy=(0, 0), xytext=(20, 20), textcoords="offset points",
                            bbox=dict(boxstyle="round", fc="w"),
                            arrowprops=dict(arrowstyle="->"))
      Pannot_E.set_visible(False)

    #amplifier

      def update_annot_T1(line, idx):
            posx, posy = [line.get_xdata()[idx], line.get_ydata()[idx]]
            annot_T1.xy = (posx, posy)
            text = f'{line.get_label()}'
            annot_T1.set_text(text)
            annot_T1.get_bbox_patch().set_alpha(0.4)

      def update_annot_T2(line1, idx1):
            posx, posy = [line1.get_xdata()[idx1], line1.get_ydata()[idx1]]
            annot_T2.xy = (posx, posy)
            text = f'{line1.get_label()}'
            annot_T2.set_text(text)
            annot_T2.get_bbox_patch().set_alpha(0.4)

      def update_annot_E(line2, idx1):
            posx, posy = [line2.get_xdata()[idx1], line2.get_ydata()[idx1]]
            annot_E.xy = (posx, posy)
            text = f'{line2.get_label()}'
            annot_E.set_text(text)
            annot_E.get_bbox_patch().set_alpha(0.4)

    #threshold 

      def Tupdate_annot_T1(line, idx):
            posx, posy = [line.get_xdata()[idx], line.get_ydata()[idx]]
            Tannot_T1.xy = (posx, posy)
            text = f'{line.get_label()}'
            Tannot_T1.set_text(text)
            Tannot_T1.get_bbox_patch().set_alpha(0.4)

      def Tupdate_annot_T2(line1, idx1):
            posx, posy = [line1.get_xdata()[idx1], line1.get_ydata()[idx1]]
            Tannot_T2.xy = (posx, posy)
            text = f'{line1.get_label()}'
            Tannot_T2.set_text(text)
            Tannot_T2.get_bbox_patch().set_alpha(0.4)

      def Tupdate_annot_E(line2, idx1):
            posx, posy = [line2.get_xdata()[idx1], line2.get_ydata()[idx1]]
            Tannot_E.xy = (posx, posy)
            text = f'{line2.get_label()}'
            Tannot_E.set_text(text)
            Tannot_E.get_bbox_patch().set_alpha(0.4)
    #Pe

      def Pupdate_annot_T1(line, idx):
            posx, posy = [line.get_xdata()[idx], line.get_ydata()[idx]]
            Pannot_T1.xy = (posx, posy)
            text = f'{line.get_label()}'
            Pannot_T1.set_text(text)
            Pannot_T1.get_bbox_patch().set_alpha(0.4)

      def Pupdate_annot_T2(line1, idx1):
            posx, posy = [line1.get_xdata()[idx1], line1.get_ydata()[idx1]]
            Pannot_T2.xy = (posx, posy)
            text = f'{line1.get_label()}'
            Pannot_T2.set_text(text)
            Pannot_T2.get_bbox_patch().set_alpha(0.4)

      def Pupdate_annot_E(line2, idx1):
            posx, posy = [line2.get_xdata()[idx1], line2.get_ydata()[idx1]]
            Pannot_E.xy = (posx, posy)
            text = f'{line2.get_label()}'
            Pannot_E.set_text(text)
            Pannot_E.get_bbox_patch().set_alpha(0.4)

    #amplifier

      def hover_T1(event):
            vis = annot_T1.get_visible()
            if event.inaxes == ax[0,0]:
                for line in lines_T1:
                    cont, ind = line.contains(event)
                    if cont:
                        update_annot_T1(line, ind['ind'][0])
                        annot_T1.set_visible(True)
                        fig.canvas.draw_idle()
                    else:
                        if vis:
                            annot_T1.set_visible(False)
                            fig.canvas.draw_idle()
      def hover_T2(event1):
            vis1 = annot_T2.get_visible()
            if event1.inaxes == ax[1,0]:
                for line1 in lines_T2:
                    cont1, ind1 = line1.contains(event1)
                    if cont1:
                        update_annot_T2(line1, ind1['ind'][0])
                        annot_T2.set_visible(True)
                        fig.canvas.draw_idle()
                    else:
                        if vis1:
                            annot_T2.set_visible(False)
                            fig.canvas.draw_idle()
      def hover_E(event2):
            vis2 = annot_E.get_visible()
            if event2.inaxes == ax[2,0]:
                for line2 in lines_E:
                    cont2, ind2 = line2.contains(event2)
                    if cont2:
                        update_annot_E(line2, ind2['ind'][0])
                        annot_E.set_visible(True)
                        fig.canvas.draw_idle()
                    else:
                        if vis2:
                            annot_E.set_visible(False)
                            fig.canvas.draw_idle()


    #threshold
      def Thover_T1(event):
            vis = Tannot_T1.get_visible()
            if event.inaxes == ax[0,1]:
                for Tline in Tlines_T1:
                    cont, ind = Tline.contains(event)
                    if cont:
                        Tupdate_annot_T1(Tline, ind['ind'][0])
                        Tannot_T1.set_visible(True)
                        fig.canvas.draw_idle()
                    else:
                        if vis:
                            Tannot_T1.set_visible(False)
                            fig.canvas.draw_idle()
      def Thover_T2(event1):
            vis1 = Tannot_T2.get_visible()
            if event1.inaxes == ax[1,1]:
                for Tline1 in Tlines_T2:
                    cont1, ind1 = Tline1.contains(event1)
                    if cont1:
                        Tupdate_annot_T2(Tline1, ind1['ind'][0])
                        Tannot_T2.set_visible(True)
                        fig.canvas.draw_idle()
                    else:
                        if vis1:
                            Tannot_T2.set_visible(False)
                            fig.canvas.draw_idle()
      def Thover_E(event2):
            vis2 = Tannot_E.get_visible()
            if event2.inaxes == ax[2,1]:
                for Tline2 in Tlines_E:
                    cont2, ind2 = Tline2.contains(event2)
                    if cont2:
                        Tupdate_annot_E(Tline2, ind2['ind'][0])
                        Tannot_E.set_visible(True)
                        fig.canvas.draw_idle()
                    else:
                        if vis2:
                            Tannot_E.set_visible(False)
                            fig.canvas.draw_idle()

    #Pe
      def Phover_T1(event):
            vis = Pannot_T1.get_visible()
            if event.inaxes == ax2[0]:
                for Pline in Plines_T1:
                    cont, ind = Pline.contains(event)
                    if cont:
                        Pupdate_annot_T1(Pline, ind['ind'][0])
                        Pannot_T1.set_visible(True)
                        fig2.canvas.draw_idle()
                    else:
                        if vis:
                            Pannot_T1.set_visible(False)
                            fig2.canvas.draw_idle()
      def Phover_T2(event1):
            vis1 = Pannot_T2.get_visible()
            if event1.inaxes == ax2[1]:
                for Pline1 in Plines_T2:
                    cont1, ind1 = Pline1.contains(event1)
                    if cont1:
                        Pupdate_annot_T2(Pline1, ind1['ind'][0])
                        Pannot_T2.set_visible(True)
                        fig2.canvas.draw_idle()
                    else:
                        if vis1:
                            Pannot_T2.set_visible(False)
                            fig2.canvas.draw_idle()
      def Phover_E(event2):
            vis2 = Pannot_E.get_visible()
            if event2.inaxes == ax2[2]:
                for Pline2 in Plines_E:
                    cont2, ind2 = Pline2.contains(event2)
                    if cont2:
                        Pupdate_annot_E(Pline2, ind2['ind'][0])
                        Pannot_E.set_visible(True)
                        fig2.canvas.draw_idle()
                    else:
                        if vis2:
                            Pannot_E.set_visible(False)
                            fig2.canvas.draw_idle()


    #amplifier
      fig.canvas.mpl_connect("motion_notify_event", hover_E)
      fig.canvas.mpl_connect("motion_notify_event", hover_T2)
      fig.canvas.mpl_connect("motion_notify_event", hover_T1)
    #threshold
      fig.canvas.mpl_connect("motion_notify_event", Thover_E)
      fig.canvas.mpl_connect("motion_notify_event", Thover_T2)
      fig.canvas.mpl_connect("motion_notify_event", Thover_T1)
    #Pe
      fig2.canvas.mpl_connect("motion_notify_event", Phover_E)
      fig2.canvas.mpl_connect("motion_notify_event", Phover_T2)
      fig2.canvas.mpl_connect("motion_notify_event", Phover_T1)


      # fig.suptitle('TOFPET ID: '+ str(tp))
      fig.tight_layout()

      # fig2.suptitle('TOFPET ID: '+ str(tp))
      fig2.tight_layout()
      plt.show()


if __name__ == '__main__':
  main()