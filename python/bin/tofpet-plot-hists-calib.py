#!/usr/bin/env python3

import argparse
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import logging
import os.path as op
from scifi_daq.daq_board.daq_board_multi import DaqBoards
from scifi_daq.daq_board.daq_board import DaqBoard
import fnmatch


def main():
  logging.basicConfig(level=logging.ERROR)

  parser = argparse.ArgumentParser()
  parser.add_argument('conf_path', type=str, help='Path to the configuration board folder OR to the detector configuration folder (the one that contains configuration.toml).')
  parser.add_argument('--board-names', nargs = '+', type = str, help = 'Name of the boards you want to consider.')
  parser.add_argument('--board-ids', nargs = '+', type = int, help = 'IDs of the boards you want to consider.')
  parser.add_argument('--tofpet-ids', nargs = '+', type=int, help = 'TOFPET IDs to be displayed. If unspecified, all TOFPETs will be shown.')
  args = parser.parse_args()

  args.conf_path = op.normpath(args.conf_path)

  # case where conf_path points to a full detector configuration
  if op.exists(op.join(args.conf_path, 'configuration.toml')):
    # get the desired board IDs
    boardIds = args.board_ids if args.board_ids is not None else []
    
    # get the IDs corresponding to the desired board names
    d = DaqBoards(args.conf_path)
    if args.board_names is not None:
      boardNames = d.boardNames()
      for bname in args.board_names:
        for bn, bid in boardNames.items():
          if fnmatch.fnmatch(bn, bname):
            boardIds.append(bid)
    
    # if the user selected at least one board, use that, otherwise keep all of them
    if boardIds:
      d = DaqBoards(args.conf_path, subset=boardIds)
    selectedBoards = list(d._boards.values())
    del d
  # case where conf_path points to a single board configuration
  else:
    if not op.exists(op.join(args.conf_path, 'board.toml')):
      raise OSError(f'File {op.join(args.conf_path, "board.toml")} or {op.join(args.conf_path, "configuration.toml")} do not exist. conf_path must correspond to the board or detector configuration directory.')
    
    selectedBoards = [DaqBoard(args.conf_path, None)]
    # in this case the name is not known. Use the folder name
    selectedBoards[0].name = op.basename(args.conf_path)
  
  if args.tofpet_ids is None:
    args.tofpet_ids = [0, 1, 2, 3, 4, 5, 6, 7]

  for board in selectedBoards:
    board.readConfiguration(None)
    
    for tp in args.tofpet_ids:
    #Tdc plots

      tiaThrTitle = f'{board.name} TIA/THR TOFPET {tp}' if board.boardId is None else f'{board.name} (ID: {board.boardId}) TIA/THR TOFPET {tp}'
      tdcTitle = f'{board.name} TDC TOFPET {tp}' if board.boardId is None else f'{board.name} (ID: {board.boardId}) TDC TOFPET {tp}'
      qdcTitle = f'{board.name} QDC TOFPET {tp}' if board.boardId is None else f'{board.name} (ID: {board.boardId}) QDC TOFPET {tp}'

      figTdc, axTdc = plt.subplots(2, 3, figsize = (8,6), num=tdcTitle)

      #Qdc plots
      figQdc, axQdc = plt.subplots(2, 3, figsize = (8,6), num=qdcTitle)

      figTdc.suptitle(tdcTitle)
      figTdc.tight_layout()
      figQdc.suptitle(qdcTitle)
      figQdc.tight_layout()
      
    
      dfTdc = pd.read_csv(board._boardConf.tdcCalPath)
      dqQdc = pd.read_csv(board._boardConf.qdcCalPath)
      dfTia = pd.read_csv(board._boardConf.tiaCalPath)
      dfThrBl = pd.read_csv(board._boardConf.thrCalPath)      

      if op.exists(board._boardConf.thresholdsConfPath) == True:
        dfThr = pd.read_csv(board._boardConf.thresholdsConfPath)
        
        selection_thresholds = (dfThr['tofpet_id'] == tp)
        #thresholds.csv data   
        thr_t1 = np.array(dfThr[selection_thresholds]['thr_t1'])
        thr_t2 = np.array(dfThr[selection_thresholds]['thr_t2'])
        thr_e = np.array(dfThr[selection_thresholds]['thr_e'])

        figTia, axTia = plt.subplots(3, 3, figsize = (9, 9), num=tiaThrTitle)
      
        bin = 50
        axTia[0,2].hist(thr_t1, bins = 1)
        axTia[1,2].hist(thr_t2, bins = 1)
        axTia[2,2].hist(thr_e, bins = 1)
        axTia[2,0].axis('off')

        axTia[0,2].set_xlabel('thr_t1')
        axTia[1,2].set_xlabel('thr_t2')
        axTia[2,2].set_xlabel('thr_e')

        axTia[0,2].grid(alpha = 0.3)
        axTia[1,2].grid(alpha = 0.3)
        axTia[2,2].grid(alpha = 0.3)
        axTia[0,1].grid(alpha = 0.3)
        axTia[0,2].grid(alpha = 0.3)
        axTia[0,0].grid(alpha = 0.3)
        axTia[1,0].grid(alpha = 0.3)
        axTia[2,1].grid(alpha = 0.3)
        axTia[1,1].grid(alpha = 0.3)
      else:
        figTia, axTia = plt.subplots(3, 2)
      
      figTia.suptitle(tiaThrTitle)
      figTia.tight_layout()

      if not any(thr_t1): # TODO if file is missing, it raises
        print(f'No TOFPET {tp}')
        plt.close(figTia)
        plt.close(figTdc)
        plt.close(figQdc)

    #takes the data for just one tofpet, specified in command line 
      selection_T  = (dfTdc['tofpet_id'] == tp) 
      selection_Q  = (dqQdc['tofpet_id'] == tp) 
      selection_TIA = (dfTia['tofpet_id'] == tp)
      selection_Thr = (dfThrBl['tofpet_id'] == tp)
      

    #selecting data: a, b, c, d, e (just for Qdc) and chi2, dof 
      a_arr_T = np.array(dfTdc[selection_T]['a'])
      b_arr_T = np.array(dfTdc[selection_T]['b'])
      c_arr_T = np.array(dfTdc[selection_T]['c'])
      d_arr_T = np.array(dfTdc[selection_T]['d'])
      chi2_arr_T = np.array(dfTdc[selection_T]['chi2'])
      dof_arr_T = np.array(dfTdc[selection_T]['dof'])
      chi2_dof_arr_T = chi2_arr_T/dof_arr_T

      a_arr_Q = np.array(dqQdc[selection_Q]['a'])
      b_arr_Q = np.array(dqQdc[selection_Q]['b'])
      c_arr_Q = np.array(dqQdc[selection_Q]['c'])
      d_arr_Q = np.array(dqQdc[selection_Q]['d'])
      e_arr_Q = np.array(dqQdc[selection_Q]['e'])
      chi2_arr_Q = np.array(dqQdc[selection_Q]['chi2'])
      dof_arr_Q = np.array(dqQdc[selection_Q]['dof'])
      chi2_dof_arr_Q = chi2_arr_Q/dof_arr_Q


    #selecting tia, thr bl data
      bl_t = np.array(dfTia[selection_TIA]['bl_t'])
      bl_e = np.array(dfTia[selection_TIA]['bl_e'])

      thr_t1_bl = np.array(dfThrBl[selection_Thr]['thr_t1_bl'])
      thr_t2_bl = np.array(dfThrBl[selection_Thr]['thr_t2_bl'])
      thr_e_bl = np.array(dfThrBl[selection_Thr]['thr_e_bl'])


      axTdc[0,0].hist(a_arr_T, bins = bin)
      axTdc[1,0].hist(b_arr_T, bins = bin)
      axTdc[0,1].hist(c_arr_T, bins = bin)
      axTdc[1,1].hist(d_arr_T, bins = bin)

      axTdc[0,0].set_xlabel('a')
      axTdc[1,0].set_xlabel('b')
      axTdc[0,1].set_xlabel('c')
      axTdc[1,1].set_xlabel('d')


      axTdc[0,0].grid(alpha = 0.3)
      axTdc[1,0].grid(alpha = 0.3)
      axTdc[0,1].grid(alpha = 0.3)
      axTdc[1,1].grid(alpha = 0.3)
    
      
      axTdc[1,2].hist(chi2_dof_arr_T, bins = bin)
      axTdc[1,2].set_xlabel('chi2/dof')
      axTdc[1,2].grid(alpha = 0.3)
      axTdc[0,2].axis('off')


      axQdc[0,0].hist(a_arr_Q, bins = bin)
      axQdc[1,0].hist(b_arr_Q, bins = bin)
      axQdc[0,1].hist(c_arr_Q, bins = bin)
      axQdc[1,1].hist(d_arr_Q, bins = bin)
      axQdc[0,2].hist(e_arr_Q, bins = bin)

      axQdc[0,0].set_xlabel('a')
      axQdc[1,0].set_xlabel('b')
      axQdc[0,1].set_xlabel('c')
      axQdc[1,1].set_xlabel('d')
      axQdc[0,2].set_xlabel('e')

      axQdc[0,0].grid(alpha = 0.3)
      axQdc[1,0].grid(alpha = 0.3)
      axQdc[0,1].grid(alpha = 0.3)
      axQdc[1,1].grid(alpha = 0.3)
      axQdc[0,2].grid(alpha = 0.3)
    
      axQdc[1,2].hist(chi2_dof_arr_Q, bins = 20)
      axQdc[1,2].set_xlabel('chi2/dof')
      axQdc[1,2].grid(alpha = 0.3)

      if any(bl_t):
        bin_bl_t = np.amax(bl_t) - np.amin(bl_t) + 1
        bin_bl_e = np.amax(bl_e) - np.amin(bl_e) + 1
        bin_t1_bl =  np.amax(thr_t1_bl) - np.amin(thr_t1_bl) + 1
        bin_t2_bl = np.amax(thr_t2_bl) - np.amin(thr_t2_bl) + 1
        bin_e_bl = np.amax(thr_e_bl) - np.amin(thr_e_bl) + 1
      else: 
        continue
    
      axTia[0,0].hist(bl_t, bins = bin_bl_t, range = [np.amin(bl_t) - 0.5, np.amax(bl_t) + 0.5])
      axTia[1,0].hist(bl_e, bins = bin_bl_e, range = [np.amin(bl_e) - 0.5, np.amax(bl_e) +0.5])

      axTia[0,0].set_xlabel('TIA bl_t')
      axTia[1,0].set_xlabel('TIA bl_e')


      axTia[0,1].hist(thr_t1_bl, bins = bin_t1_bl, range = [np.amin(thr_t1_bl) - 0.5,  np.amax(thr_t1_bl) + 0.5])
      axTia[1,1].hist(thr_t2_bl, bins = bin_t2_bl, range = [np.amin(thr_t2_bl) - 0.5,  np.amax(thr_t2_bl) + 0.5])
      axTia[2,1].hist(thr_e_bl, bins = bin_e_bl, range = [np.amin(thr_e_bl) - 0.5,  np.amax(thr_e_bl) + 0.5])

      axTia[0,1].set_xlabel('thr_t1_bl')
      axTia[1,1].set_xlabel('thr_t2_bl')
      axTia[2,1].set_xlabel('thr_e_bl')
      

      plt.show()

if __name__ == '__main__':
  main()