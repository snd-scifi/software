#!/usr/bin/env python3

import argparse
import pandas as pd
import numpy as np
from scifi_daq.daq_board.daq_board_multi import DaqBoards
import logging
import fnmatch


def main():
    logging.basicConfig(level=logging.ERROR)
    parser = argparse.ArgumentParser()
    parser.add_argument('conf_path', type=str, help='Path to the folder containing configuration.toml')
    parser.add_argument('--board-ids', nargs = '+', type = int)
    parser.add_argument('--board-names', nargs = '+', type = str)
    parser.add_argument('--sort', action='store_true', help = 'Option to sort by board id.')
    parser.add_argument('--tia-bl-t-min', type = str, help = 'Specify TIA bl_t minimum. No TIA bl_t values will be analzed if left empty. ')
    parser.add_argument('--tia-bl-e-min', type = str, help = 'Specify TIA bl_e minimum. No TIA bl_e values will be analzed if left empty. ')
    parser.add_argument('--thr-t1-bl-min', type = str, help = 'Specify thr_t1_bl minimum. No thr_t1 values will be analzed if left empty. ')
    parser.add_argument('--thr-t2-bl-min', type = str, help = 'Specify thr_t2_bl minimum. No thr_t2 values will be analzed if left empty. ')
    parser.add_argument('--thr-e-bl-min', type = str, help = 'Specify thr_e_bl minimum. No thr_e values will be analzed if left empty. ')
    parser.add_argument('--tdc-max-chi2', type = str, help = 'Specify TDC maximum reduced chi2. No TDC values will be analzed if left empty. ')
    parser.add_argument('--qdc-max-chi2', type = str, help = 'Specify QDC maximum reduced chi2. No QDC values will be analzed if left empty. ')
    parser.add_argument('--thr-t1-min', type = str, help = 'Specify thr_t1 minimum. Default is 0. ')
    parser.add_argument('--thr-t1-max', type = str, help = 'Specify thr_t1 maximum. Default is 62. ')
    parser.add_argument('--thr-t2-min', type = str, help = 'Specify thr_t2 minimum. Default is 0. ')
    parser.add_argument('--thr-t2-max', type = str, help = 'Specify thr_t2 maximum. Default is 62. ')
    parser.add_argument('--thr-e-min', type = str, help = 'Specify thr_e minimum. Default is 0. ')
    parser.add_argument('--thr-e-max', type = str, help = 'Specify thr_e maximum. Default is 62. ')
    args = parser.parse_args()
    
    
    if args.board_names is not None:
      ids = []
      d = DaqBoards(args.conf_path)
      boardNames = d.boardNames()
      for name in boardNames:
        if fnmatch.fnmatch(name, f'{args.board_names[0]}'):
          ids.append(boardNames.get(name))

      args.board_ids = ids
    
  
    d = DaqBoards(args.conf_path, subset = args.board_ids)
    d.readConfiguration()
 
    items = []
    i = 0
    for id, b in d._boards.items():
      name = list(d.boardNames().items())[i][0]
      items.append([id, b, name])
      i += 1
   
    items_loop = items
    if args.sort:
      items_loop = sorted(items, key = lambda tup: tup[0])

    for id, b, name in items_loop:
      #TIA bl_t
      if args.tia_bl_t_min is not None:
        df_TIA = pd.read_csv(b._boardConf.tiaCalPath)
        
        bl_t = np.array(df_TIA['bl_t'])
        selection_bl_t = df_TIA[(bl_t < int(args.tia_bl_t_min))]

        if not selection_bl_t.empty:
          print()
          print(f'Board: {name}, ID: {id}, TIA bl_t')
          print(selection_bl_t.to_string(index = False))

      #TIA bl_e
      if args.tia_bl_e_min is not None:
        df_TIA = pd.read_csv(b._boardConf.tiaCalPath)
        bl_e = np.array(df_TIA['bl_e'])
        selection_bl_e = df_TIA[(bl_e < int(args.tia_bl_e_min))]
    
        if not selection_bl_e.empty:
          print()
          print(f'Board: {name}, ID: {id}, TIA bl_e')
          print(selection_bl_e.to_string(index = False))

      #thr_t1_bl
      if args.thr_t1_bl_min is not None:
        df_Thr = pd.read_csv(b._boardConf.thrCalPath)
        thr_t1_bl = np.array(df_Thr['thr_t1_bl'])
        selection_thr_t1 = df_Thr[(thr_t1_bl < int(args.thr_t1_bl_min))]

        if not selection_thr_t1.empty:
          print()
          print(f'Board: {name}, ID: {id}, thr_bl_t1')
          print(selection_thr_t1.to_string(index = False))
      
      #Thr_t2_bl
      if args.thr_t2_bl_min is not None:
        df_Thr = pd.read_csv(b._boardConf.thrCalPath)
        thr_t2_bl = np.array(df_Thr['thr_t2_bl'])
        selection_thr_t2 = df_Thr[(thr_t2_bl < int(args.thr_t2_bl_min))]

        if not selection_thr_t2.empty:
          print()
          print(f'Board: {name}, ID: {id}, thr_bl_t2')
          print(selection_thr_t2.to_string(index = False))
      
      #Thr_e_bl
      if args.thr_e_bl_min is not None:
        df_Thr = pd.read_csv(b._boardConf.thrCalPath)
        thr_e_bl = np.array(df_Thr['thr_e_bl'])
        selection_thr_e = df_Thr[(thr_e_bl < int(args.thr_e_bl_min))]

        if not selection_thr_e.empty:
          print()
          print(f'Board: {name}, ID: {id}, thr_bl_e')
          print(selection_thr_e.to_string(index = False))

      #Tdc
      if args.tdc_max_chi2 is not None:
        df_T = pd.read_csv(b._boardConf.tdcCalPath)
        chi2_arr_T = np.array(df_T['chi2'])
        dof_arr_T = np.array(df_T['dof'])
        chi2_dof_arr_T = chi2_arr_T/dof_arr_T
        df_T['reduced chi2'] = chi2_dof_arr_T
        selection_df_T = df_T[chi2_dof_arr_T > int(args.tdc_max_chi2)]

        if not selection_df_T.empty:
          print()
          print(f'Board: {name}, ID: {id}, TDC')
          print(selection_df_T.to_string(index = False))

      #Qdc
      if args.qdc_max_chi2 is not None:
        df_Q = pd.read_csv(b._boardConf.qdcCalPath)
        chi2_arr_Q = np.array(df_Q['chi2'])
        dof_arr_Q = np.array(df_Q['dof'])
        chi2_dof_arr_Q = chi2_arr_Q/dof_arr_Q
        df_Q['reduced chi2'] = chi2_dof_arr_Q

        selection_df_Q = df_Q[chi2_dof_arr_Q > int(args.qdc_max_chi2)]
        if not selection_df_Q.empty:
          print()
          print(f'Board: {name}, ID: {id}, QDC')
          print(selection_df_Q.to_string(index = False))

      #Threshold thr_t1
      if args.thr_t1_min is not None:
        min_t1 = int(args.thr_t1_min)
      else:
        min_t1 = 0
      if args.thr_t1_max is not None:
        max_t1 = int(args.thr_t1_max)
      else:
        max_t1 = 62
       
      df_Thr = pd.read_csv(b._boardConf.thresholdsConfPath)
      thr_t1 = np.array(df_Thr['thr_t1'])
      selection_thr_t1 = df_Thr[(thr_t1 < min_t1) | (thr_t1 > max_t1)]

      if not selection_thr_t1.empty:
          print()
          print(f'Board: {name}, ID: {id}, thr_t1')
          print(selection_thr_t1.to_string(index = False))

      #Threshold thr_t2
      if args.thr_t2_min is not None:
        min_t2 = int(args.thr_t2_min)
      else:
        min_t2 = 0
      if args.thr_t2_max is not None:
        max_t2 = int(args.thr_t2_max)
      else:
        max_t2 = 62
    
      thr_t2 = np.array(df_Thr['thr_t2'])
      selection_thr_t2 = df_Thr[(thr_t1 < min_t2) | (thr_t1 > max_t2)]

      if not selection_thr_t2.empty:
          print()
          print(f'Board: {name}, ID: {id}, thr_t2')
          print(selection_thr_t2.to_string(index = False))

      #Threshold thr_e
      if args.thr_e_min is not None:
        min_e = int(args.thr_e_min)
      else:
        min_e = 0
      if args.thr_e_max is not None:
        max_e = int(args.thr_e_max)
      else:
        max_e = 62
       
      df_Thr = pd.read_csv(b._boardConf.thresholdsConfPath)
      thr_e = np.array(df_Thr['thr_e'])
      selection_thr_e = df_Thr[(thr_e < min_e) | (thr_t1 > max_e)]

      if not selection_thr_e.empty:
          print()
          print(f'Board: {name}, ID: {id}, thr_e')
          print(selection_thr_e.to_string(index = False))
      
if __name__ == '__main__':
  main()