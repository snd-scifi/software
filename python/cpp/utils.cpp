#include <iostream>
#include <string>

#include <pybind11/pybind11.h>
#include "csv.hpp"


#define STRINGIFY(x) #x
#define MACRO_STRINGIFY(x) STRINGIFY(x)

namespace py = pybind11;

py::dict readCsvConfFile(char* path);
py::dict readCsvTdcCalFile(char* path);
py::dict readCsvQdcCalFile(char* path);

PYBIND11_MODULE(csv_to_dict_reader, m) {
  m.doc() = R"pbdoc(
    C++ utils library
    -----------------------
    .. currentmodule:: csv_to_dict_reader
    .. autosummary::
       :toctree: _generate
       readCsvConfFile
       readCsvTdcCalFile
       readCsvQdcCalFile
  )pbdoc";

  m.def("readCsvConfFile", &readCsvConfFile, R"pbdoc(
    Read a csv configuration file and converts it to a dictionary.
    Some other explanation about the readCsvConfFile function.
  )pbdoc");

  m.def("readCsvTdcCalFile", &readCsvTdcCalFile, R"pbdoc(
    Read a csv TDC calibration file and converts it to a dictionary.
    Some other explanation about the readCsvConfFile function.
  )pbdoc");

  m.def("readCsvQdcCalFile", &readCsvQdcCalFile, R"pbdoc(
    Read a csv QDC calibration file and converts it to a dictionary.
    Some other explanation about the readCsvConfFile function.
  )pbdoc");


#ifdef VERSION_INFO
  m.attr("__version__") = MACRO_STRINGIFY(VERSION_INFO);
#else
  m.attr("__version__") = "dev";
#endif
}


py::dict readCsvConfFile(char* path) {
  py::dict confDict;
  csv::CSVReader confReader(path);
  for (csv::CSVRow& row: confReader) {
    py::int_ tpId;
    py::int_ tpCh;
    py::dict channelConf;

    for (const auto& k : row.get_col_names()) {
      if (k == "tofpet_id") {
        // tpId = py::str(std::to_string(row[k].get<uint8_t>()));
        tpId = py::int_(row[k].get<uint8_t>());
      }
      else if (k == "channel") {
        // tpCh = py::str(std::to_string(row[k].get<uint8_t>()));
        tpCh = py::int_(row[k].get<uint8_t>());
      }
      else {
        switch (row[k].type()) {
        case csv::DataType::CSV_NULL:
        case csv::DataType::CSV_STRING:
          channelConf[py::str(k)] = py::str(row[k].get<>());
          break;
        case csv::DataType::CSV_INT8:
        case csv::DataType::CSV_INT16:
        case csv::DataType::CSV_INT32:
        case csv::DataType::CSV_INT64:
          channelConf[py::str(k)] = py::int_(row[k].get<long>());
          break;
        case csv::DataType::CSV_DOUBLE:
          channelConf[py::str(k)] = py::float_(row[k].get<float>());
          break;
        default:
          py::print(py::str(k), "type is not known");
          break;
        } 
      }
    }

    try {
      confDict[tpId][tpCh] = channelConf;
    }
    catch (std::exception& e) {
      confDict[tpId] = py::dict();
      confDict[tpId][tpCh] = channelConf;
    }
    
  }

  return confDict;
}

py::dict readCsvTdcCalFile(char* path){
  py::dict calDict;
  csv::CSVReader calReader(path);
  for (csv::CSVRow& row: calReader) {
    py::int_ tpId;
    py::int_ tpCh;
    py::int_ tpTac;
    py::int_ tpTdc;
    py::dict cal;

    for (const auto& k : row.get_col_names()) {
      if (k == "tofpet_id") {
        // tpId = py::str(std::to_string(row[k].get<uint8_t>()));
        tpId = py::int_(row[k].get<uint8_t>());
      }
      else if (k == "channel") {
        // tpCh = py::str(std::to_string(row[k].get<uint8_t>()));
        tpCh = py::int_(row[k].get<uint8_t>());
      }
      else if (k == "tac") {
        // tpCh = py::str(std::to_string(row[k].get<uint8_t>()));
        tpTac = py::int_(row[k].get<uint8_t>());
      }
      else if (k == "tdc") {
        // tpCh = py::str(std::to_string(row[k].get<uint8_t>()));
        tpTdc = py::int_(row[k].get<uint8_t>());
      }
      else {
        cal[py::str(k)] = py::float_(row[k].get<float>());
      }
    }

    if (!calDict.contains(tpId)) {
      calDict[tpId] = py::dict();
      calDict[tpId][tpCh] = py::dict();
      calDict[tpId][tpCh][tpTac] = py::dict();
    }
    else if (!calDict[tpId].contains(tpCh)) {
      calDict[tpId][tpCh] = py::dict();
      calDict[tpId][tpCh][tpTac] = py::dict();
    }
    else if (!calDict[tpId][tpCh].contains(tpTac)) {
      calDict[tpId][tpCh][tpTac] = py::dict();
    }

    calDict[tpId][tpCh][tpTac][tpTdc] = cal;
  }

  return calDict;
}

py::dict readCsvQdcCalFile(char* path){
  py::dict calDict;
  csv::CSVReader calReader(path);
  for (csv::CSVRow& row: calReader) {
    py::int_ tpId;
    py::int_ tpCh;
    py::int_ tpTac;
    py::dict cal;

    for (const auto& k : row.get_col_names()) {
      if (k == "tofpet_id") {
        // tpId = py::str(std::to_string(row[k].get<uint8_t>()));
        tpId = py::int_(row[k].get<uint8_t>());
      }
      else if (k == "channel") {
        // tpCh = py::str(std::to_string(row[k].get<uint8_t>()));
        tpCh = py::int_(row[k].get<uint8_t>());
      }
      else if (k == "tac") {
        // tpCh = py::str(std::to_string(row[k].get<uint8_t>()));
        tpTac = py::int_(row[k].get<uint8_t>());
      }
      else {
        cal[py::str(k)] = py::float_(row[k].get<float>());
      }
    }

    if (!calDict.contains(tpId)) {
      calDict[tpId] = py::dict();
      calDict[tpId][tpCh] = py::dict();
    }
    else if (!calDict[tpId].contains(tpCh)) {
      calDict[tpId][tpCh] = py::dict();
    }

    calDict[tpId][tpCh][tpTac] = cal;
  }

  return calDict;
}