# Auto generate requirements

Run
```
pipreqs --mode=compat --force .
```
to generate `requirements.txt`

# To install 

Install the main DAQ

```
python3.8 -m pip install -e .
```

Installing test 

```
python3.8 -m pip install -e scifi_test
```
