#!/usr/bin/env python3

import os.path as op
import subprocess
from datetime import datetime
import argparse
import threading
from time import sleep
import toml
from pathlib import Path 
import sys

from scifi_daq.daq_server.daq_server_ctrl import DaqServerCtrl
from scifi_daq.daq_board.daq_board_multi import DaqBoards

def target(*args, **kwargs):
  subprocess.run(*args, **kwargs)

def main():

  # Input filename 
  input_name = sys.argv[1]

  # PATH 
  RUNNING_PATH = Path.cwd()
  FILE_PATH = Path(__file__).resolve().parent

  # Absolute path to the software bin folder
  SOFTWARE_PATH = f"{RUNNING_PATH}/build-host/bin" 
  configuration = toml.load(f'{FILE_PATH}/conf/configuration.toml')
  configuration['daq_server']['data_path'] = f'{FILE_PATH}/output_data'
  print(f"SOFTWARE_PATH: {SOFTWARE_PATH}\nconfiguration: {FILE_PATH}/conf/configuration.toml")
  s = DaqServerCtrl(None, 'localhost')
  # the DAQ boards object is only used to get the board IDs and the board mapping
  b = DaqBoards(f'{FILE_PATH}/conf')

  print('connect')
  s.connect()
  
  print('init')
  s.setBoardIds(b.boardIds())
  # Put absolute path to the output folder

  # determine the run number as the last recorded run + 1
  # availableRuns = s.getRecordedRuns()
  # try:
  #   s.setRunNumber(availableRuns[-1]+1)
  # except IndexError:
  #   s.setRunNumber(0)
  s.setRunNumber(0)
  
  # set the writer settings, if available
  try:
    s.setDaqServerSettings(configuration['daq_server'])
  except KeyError:
    pass

  # board mapping is needed for noise filtering
  s.setBoardMapping(b.boardMapping)

  # start data acquisition on the server (i.e. wait for data to come)
  s.startDaq(overwrite=True)
  runNumber = s.getStatus()['run_number']

  with open(f'{FILE_PATH}/logs/log_{runNumber}.txt', 'w') as f:
    command = [op.join(SOFTWARE_PATH, 'boards-simulator-txt'), input_name, 'localhost', '-l', 'notice']
    # appends all the board IDs to the command, as they need to be specified
    for i in b.boardIds():
      command.append('-b')
      command.append(str(i))
    
    # start a thread to run the boards simulator
    start = datetime.now()
    thread = threading.Thread(target=target, args=[command], kwargs={'check': True})
    thread.start()

    # periodically gets the status of the DAQ server and saves it to a text file
    while True:
      status = s.getStatus()
      f.write(str(datetime.now()) + '\n')
      f.write(str(status) + '\n')
      f.flush()
      sleep(1)
      # if the boards simulator thread has finished, break and stop the DAQ
      if not thread.is_alive():
        break
  
  thread.join()

  #stopDaq returns the server status at the end of DAQ
  print(s.stopDaq())
  print('Time elapsed:', datetime.now() - start)



if __name__ == '__main__':
  main()
