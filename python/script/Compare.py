"""
    Script to perform comparison between 2 ROOT files
"""

import os
from typing import List
import sys
from scifi_test.ROOTCmp import ROOTCmp
from optparse import OptionParser
import filecmp

def get_options(): 
    parser = OptionParser()
    parser.add_option("-f","--fileName_Ref", dest="fileName_Ref", type="string",
                      help="reference file [default: %default].",
                      #default= "./SimpleTest/output_data/monitoring_run_000000/data_0000.root")
                      default= "./SimpleTest/output_data/")
    parser.add_option("-t","--treeName_Ref", dest="treeName_Ref", type="string",
                      help="reference tree name [default: %default].",
                      default= "data")
    parser.add_option("-p","--fileName_Test", dest="fileName_Test", type="string",
                      help="reference file [default: %default].",
                      #default= "./SimpleTest/output_data/monitoring_run_000000/data_0000.root")
                      default= "./SimpleTest/output_data/")
    parser.add_option("-q","--treeName_Test", dest="treeName_Test", type="string",
                      help="reference tree name [default: %default].",
                      default= "data")
    parser.add_option("-d", "--debug",
                  action="store_true", dest="verbose", default = False,
                  help="print debug messages to stdout")
    parser.add_option("-r", "--nrandomTest", dest="nrandomTest", type = "int",
                  help="Number of diff - generate offset to a value in data to invoke Diff report",  
                  default = 0)
    (options, args) = parser.parse_args() 
    return options


def GetAllFiles(path:str = "./") -> List[str]:
    filenames = [os.path.join(path, name) for path, subdirs, files in os.walk(path) for name in files]
    return filenames 

def CompareROOT(names, treeNames, kinds, debug = False, nrandomTest = 0): 
    """Comparison between two ROOT files 
    """
    # Setup files 
    comparer = ROOTCmp()
    if debug: 
        print("Set logging to DEBUG level")
        comparer.setDebugLogging()
    comparer.addFiles(fileNames = names, treeNames = treeNames, kinds = kinds)
    comparer.setupFiles() 
    if nrandomTest > 0:
        comparer.setRandomTests(nrandomTest)

    # Run comparison
    comparer.runCompareEvent()
    is_equal = comparer.generateDiffReport()

    return is_equal


def main():

    # Exit status 
    exitStatus = 0

    # Get options 
    options = get_options() 

    # Configuration 
    fileName_Ref  = options.fileName_Ref
    treeName_Ref  = options.treeName_Ref

    fileName_Test = options.fileName_Test
    treeName_Test = options.treeName_Test

    fileNames = [fileName_Ref, fileName_Test]
    treeNames = [treeName_Ref, treeName_Test]
    kinds     = ["reference", "test"] 

    supp_type = {".root", ".json"}

    debug = options.verbose
    nrandomTest = options.nrandomTest

    # Get files 
    filenames_Ref  = GetAllFiles(fileName_Ref)
    filenames_Test = GetAllFiles(fileName_Test)

    # Loop and search for the same file 
    is_equals = set()
    for nameRef in filenames_Ref: 
        if "run_timestamps.json" in nameRef: 
            continue

        if any(supp in nameRef.split("/")[-1] for supp in supp_type):
            # Match with filenames_Test
            for nameTest in filenames_Test: 
                if nameTest[len(fileName_Test):] == nameRef[len(fileName_Ref):]: 
                    break

            # Compare
            if ".root" in nameRef: 
                # ROOT 
                is_equal = CompareROOT([nameRef, nameTest], treeNames, kinds, debug, nrandomTest) 
            else: 
                # Other text files
                is_equal = filecmp.cmp(nameRef, nameTest)

            # Collect names and report
            is_equals.add((nameRef, nameTest, is_equal))
        else: 
            pass

    # Report 
    print(20*"=" + " Summary " + 20*"=")
    for _info in is_equals: 
        print(f"File ref : {_info[0]}")
        print(f"File test: {_info[1]}")
        print(f"        is_identical: {_info[2]}")
        print(49*"=")

    # Return exit status  
    truth = all(is_equal[2] for is_equal in is_equals)
    if truth: 
        sys.exit(0)
    elif nrandomTest > 0: 
        if truth:
            print("Test is expected to be different, but not the case")
            exit("Error random test fail")

        print("Random test successful! Exit normally")
        print("Please check the stdout before proceeding")
        print("Exit normally")
        sys.exit(0)
    else: 
        sys.exit("Error! Files are not identical!")


if __name__ == "__main__":
    main()
