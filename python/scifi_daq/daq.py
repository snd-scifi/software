import toml
import os.path as op

from scifi_daq.daq_board.daq_board_multi import DaqBoards
from scifi_daq.daq_server.daq_server_ctrl import DaqServerCtrl
from scifi_daq.ttc.vme_client import VmeClient

class Daq():
  def __init__(self, confDir: str, subset=None):
    confFile = op.join(confDir, 'configuration.toml')
    self.configuration = toml.load(confFile)
    
    self.daqBoards = DaqBoards(confDir, subset=subset)
    self.daqServer = DaqServerCtrl(None, self.configuration['daq']['addr'], port=self.configuration['daq']['port'])
    try:
      self.vmeClient = VmeClient(None, self.configuration['vme']['addr'])
    except KeyError:
      self.vmeClient = None
  
  def connect(self, timeoutBoards: float = None):
    if self.vmeClient is not None:
      self.vmeClient.connect()
    
    self.daqServer.connect()

    self.daqBoards.connect(timeout=timeoutBoards)
    
  def initialize(self, runNumber: int = None, externalOrbit=False, syncWithOrbit=True, externalReset=False, raiseExceptionOnFeMismatch: bool = False):
    if self.vmeClient is not None:
      self.vmeClient.softReset()

      if externalOrbit:
        self.vmeClient.setOrbitInput('external')
      else:
        self.vmeClient.setOrbitInput('internal')

      # The B-Go 0 is used to generate the synchronous reset in phase with the orbit
      # This is done by setting the trasmission in syncronous mode and adding just the reset command to the FIFO
      # The B-Go is setup in a way that it sends the reset syncronous to the orbit and it can be sent multiple times, if needed.
      # The reset is sent by calling self.vmeClient.generateSoftBGo(0): at the next orbit, the reset is issued
      
      # Setup the B-Go FIFO 0:
      # - synchronous to the orbit pulse
      # - resets the FIFO content
      # - single mode (requires to explicitly generate a B-Go command)
      # - B-Go from VME command and not front panel
      # - delay and duration should not be too important
      # - retransmit FIFO: after a B-Go you don't need to fill the FIFO again
      # - don't transmit as soon as the FIFO is not empy
      self.vmeClient.setBGo(0,
        sync=syncWithOrbit,
        reset_fifo=True,
        repetitive=False,
        front_panel_enable=externalReset,
        inhibit_delay=0,
        inhibit_duration=1,
        retransmit_fifo=True,
        transmit_on_fifo=False
      )

      # add the reset command to the FIFO
      self.vmeClient.addBChannelShort(0, 0b00000100)

    self.daqServer.setBoardIds(self.daqBoards.boardIds())
    self.daqServer.setDaqServerSettings(self.configuration['daq_server'])
    if runNumber is None:
      availableRuns = self.daqServer.getRecordedRuns()
      try:
        self.daqServer.setRunNumber(availableRuns[-1]+1)
      except IndexError:
        self.daqServer.setRunNumber(0)
    else:
      self.daqServer.setRunNumber(runNumber)
  
    self.daqBoards.fullReset()
    self.daqBoards.initialize(raiseExceptionOnFeMismatch=raiseExceptionOnFeMismatch)

    self.daqServer.setBoardMapping(self.daqBoards.boardMapping)
    self.daqServer.setSystemConfiguration(self.configuration)
    self.daqServer.setBoardsConfiguration(self.daqBoards.getFullConfiguration(strKeys=True))
  

  def startDaq(self, runNumber=None, saveConfiguration=True, enableBchannelData=False, overwrite=False):
    if runNumber is not None:
      self.daqServer.setRunNumber(runNumber)
    else:
      availableRuns = self.daqServer.getRecordedRuns()
      try:
        self.daqServer.setRunNumber(availableRuns[-1]+1)
      except IndexError:
        self.daqServer.setRunNumber(0)
    
    if self.vmeClient is not None:
      # self.vmeClient.syncFeReset()
      self.vmeClient.generateSoftBGo(0)
    else:
      self.daqBoards.feReset()

    print("starting DAQ server")
    self.daqServer.startDaq(overwrite=overwrite)

    print("starting DAQ boards")
    self.daqBoards.startDaq(self.configuration['daq']['addr_daq'], self.configuration['daq']['port_daq'], enableBchannelData=enableBchannelData)


  def stopDaq(self):
    self.daqBoards.stopDaq()
    return self.daqServer.stopDaq()


  def __del__(self):
    pass
