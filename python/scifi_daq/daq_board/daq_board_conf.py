import json
import toml
import os
import os.path as op
import logging
import pandas as pd
from scifi_daq.utils.csv_to_dict_reader import readCsvConfFile, readCsvTdcCalFile, readCsvQdcCalFile


def writeChannelsConfToFile(filename : str, configuration : dict, columns=['tofpet_id','channel','masked','meas_mode','g_t','g_e','g_q','qdc_min_intg_t','qdc_max_intg_t','thr_t1','thr_t2','thr_e']):
  lst = []
  # transform the dictionary configuration in a list, adding tofpet_id and channel entries
  for tpId, tpConf in configuration.items():
    for ch, chConf in tpConf.items():
      lst.append({'tofpet_id': tpId, 'channel': ch})
      lst[-1].update(chConf)
  
  # create pandas dataframe
  df = pd.DataFrame(lst)
  # reorder columns
  df = df[columns]
  # sort by tofpet_id and channel
  df.sort_values(['tofpet_id', 'channel'])
  # write to file
  df.to_csv(filename, index=False)
  

class DaqBoardConfiguration():
  def __init__(self, path, startupBoardStatus, raiseExceptionOnFeMismatch: bool = False):
    self.path = path
    self.pathLog = op.join(path, 'log')
    self.startupBoardStatus = startupBoardStatus

    # check if the provided path exists, otherwise create it
    if not op.isdir(self.path):
      logging.warn('Specified path does not exist. Creating it.')
      os.makedirs(self.path)
    
    if not op.isdir(self.pathLog):
      logging.info('Creating log path.')
      os.makedirs(self.pathLog)
    
    self.boardConfPath = op.join(self.path, 'board.toml')

    self.channelsConfPath = op.join(self.path, 'channels.csv')

    self.thresholdsConfPath = op.join(self.path, 'thresholds.csv')
    self.peCalDir = op.join(self.path, 'pe_cal')
    self.peCalDataFile = op.join(self.peCalDir, 'data.hdf5')

    self.tiaCalPath = op.join(self.path, 'tia_bl_cal.csv')
    self.tiaCalDir = op.join(self.path, 'tia_bl_cal')
    self.tiaCalDataFile = op.join(self.tiaCalDir, 'data.hdf5')

    self.thrCalPath = op.join(self.path, 'thr_bl_cal.csv')
    self.thrCalDir = op.join(self.path, 'thr_bl_cal')
    self.thrCalDataFile = op.join(self.thrCalDir, 'data.hdf5')

    self.tdcCalPath = op.join(self.path, 'tdc_cal.csv')
    self.tdcCalDir = op.join(self.path, 'tdc_cal')
    self.tdcCalDataFile = op.join(self.tdcCalDir, 'data.hdf5')

    self.qdcCalPath = op.join(self.path, 'qdc_cal.csv')
    self.qdcRangePath = op.join(self.path, 'qdc_range.csv')
    self.qdcCalDir = op.join(self.path, 'qdc_cal')
    self.qdcCalDataFile = op.join(self.qdcCalDir, 'data.hdf5')

    self.calibrationsLogPath = op.join(self.pathLog, 'calibrations.json')
    
    self._readBoardConfFromFile(startupBoardStatus, raiseExceptionOnFeMismatch=raiseExceptionOnFeMismatch)


    try:
      self.channelsConf = self._readChannelsConfFromFile(self.channelsConfPath)
    except:
      raise
      # logging.warn('channels.csv does not exist. Creating default file.')
      # self.channelsConf = _createDefaultChannelsConfiguration(self.boardConf)
      # writeChannelsConfToFile(self.channelsConfPath, self.channelsConf)
    
    try:
      self.thresholdsConf = self._readChannelsConfFromFile(self.thresholdsConfPath)
    except:
      # logging.warn('thresholds.csv does not exist.')
      self.thresholdsConf = None
    
    try:
      self.tiaBlConf = self._readChannelsConfFromFile(self.tiaCalPath)
    except:
      # logging.warn('tia_bl_cal.csv does not exist. Calibration must be performed.')
      self.tiaBlConf = None
    
    try:
      self.thrBlConf = self._readChannelsConfFromFile(self.thrCalPath)
    except:
      # logging.warn('thr_bl_cal.csv does not exist. Calibration must be performed.')
      self.thrBlConf = None

    try:
      self.qdcRangeConf = self._readChannelsConfFromFile(self.qdcRangePath)
    except:
      # logging.warn('qdc_range.csv does not exist.')
      self.qdcRangeConf = None
    
    try:
      self.tdcCal = self._readTdcCalFromFile(self.tdcCalPath)
    except:
      # logging.warn('qdc_range.csv does not exist.')
      self.tdcCal = None
    
    try:
      self.qdcCal = self._readQdcCalFromFile(self.qdcCalPath)
    except:
      # logging.warn('qdc_range.csv does not exist.')
      self.qdcCal = None
    
    # TODO pe calibration?

    # TODO determine readiness for daq based on configurations

  @staticmethod
  def mergeChannelConfigurations(*args):
    """ Merges channels configurations into conf
    """
    ret = {}
    for c in args:
      if c is None:
        continue
      for tpId, tpConf in c.items():
        for tpCh, chConf in tpConf.items():
          try:
            ret[tpId][tpCh].update(chConf)
          except KeyError:
            try:
              ret[tpId][tpCh] = chConf
            except KeyError:
              ret[tpId] = {}
              ret[tpId][tpCh] = chConf
    
    return ret    
  
  def calibrationValid(self):
    return (self.tiaBlConf is not None) and (self.thrBlConf is not None)

  def getFeChs(self, includeMasked=False):
    """ Returns a dictionary containing the active channels for each TOFPET.
    The returned dictionary has the following structure: {tp_id: [ch0, ch1, ...], ...}
    If includeMasked is True, the channels listed in the configuration file, but masked are included. By default these are excluded.
    """

    feChs = {}
    for tpid, fecfg in self.channelsConf.items():
      feChs[tpid] = []
      for ch, ccfg in fecfg.items():
        try:
          if includeMasked or not bool(ccfg['masked']):
            feChs[tpid].append(ch)
        except KeyError:
          feChs[tpid].append(ch)
    
    return feChs
  
  def getFeIds(self):
    """ Returns a list containing the IDs of the active TOFPETs (as per the configuration)
    """
    feIds = []
    for tpid, _ in self.channelsConf.items():
      feIds.append(tpid)
    
    return feIds

  def setTiaBaselineCalibration(self, calibration):
    self.tiaBlConf = calibration
    writeChannelsConfToFile(self.tiaCalPath, calibration, columns=['tofpet_id', 'channel', 'bl_t', 'bl_e'])
  

  def setThrBaselineCalibration(self, calibration):
    self.thrBlConf = calibration
    writeChannelsConfToFile(self.thrCalPath, calibration, columns=['tofpet_id', 'channel', 'thr_t1_bl', 'thr_t2_bl', 'thr_e_bl'])
  

  def setThresholds(self, thresholds):
    self.thresholdsConf = thresholds
    writeChannelsConfToFile(self.thresholdsConfPath, thresholds, columns=['tofpet_id', 'channel', 'thr_t1', 'thr_t2', 'thr_e'])
  

  def setTdcCalibration(self, cal : pd.DataFrame):
    # write to CSV file
    cal.to_csv(self.tdcCalPath, index=False)
    # read it back
    self.tdcCal = self._readTdcCalFromFile(self.tdcCalPath)


  def setQdcCalibration(self, cal : pd.DataFrame, range : pd.DataFrame):
    # write to CSV file
    cal.to_csv(self.qdcCalPath, index=False)
    range.to_csv(self.qdcRangePath, index=False)
    # read it back
    self.qdcCal = self._readQdcCalFromFile(self.qdcCalPath)
    self.qdcRangeConf = self._readChannelsConfFromFile(self.qdcRangePath)
  

  def updateCalibrationLog(self, which : str, log : dict):
    if which not in ['tia_cal', 'thr_cal', 'tdc_cal', 'qdc_cal', 'dcr_thresholds']:
      raise ValueError(f'updateCalibrationLog: {which} not valid')
    
    try:
      with open(self.calibrationsLogPath, 'r') as f:
        d = json.load(f)
    except FileNotFoundError:
      d = {}
    
    d[which] = log

    with open(self.calibrationsLogPath, 'w') as f:
      json.dump(d, f)
  

  def getCalibrationLog(self, which : str):
    if which not in ['tia_cal', 'thr_cal', 'tdc_cal', 'qdc_cal', 'dcr_thresholds']:
      raise ValueError(f'updateCalibrationLog: {which} not valid')
    
    try:
      with open(self.calibrationsLogPath, 'r') as f:
        d = json.load(f)
    except FileNotFoundError:
      return {}
    
    try:
      return d[which]
    except KeyError:
      return {}


  def _readBoardConfFromFile(self, startupBoardStatus, raiseExceptionOnFeMismatch: bool = False):
    # if the board configuration file exists, open it, otherwise create a default one
    try:
      cfg = toml.load(self.boardConfPath)
    except OSError:
      raise
      # logging.warn('board.toml does not exist. Creating default file.')
      # cfg = self._createDefaultBoardConfiguration(startupBoardStatus)
      # with open(self.boardConfPath, 'w') as f:
      #   toml.dump(cfg, f)
    
    #process the configuration: fe becomes a dictionary with fe_ids as keys
    procCfg = {}
    procCfg['board'] = cfg['board']
    procCfg['fe'] = {}

    try:
      for fecfg in cfg['fe']:
        feId = fecfg['fe_id']
        del fecfg['fe_id']
        procCfg['fe'][feId] = fecfg
    except KeyError:
      pass # it allows not to have any FE defined in the configuration file
    
    if startupBoardStatus is None:
      self.boardConf = procCfg
      return # do not perform the check if there is not board startup status
    
    if set(procCfg['fe'].keys()) != set(startupBoardStatus['tofpet_available']):
      logging.warn('Configuration not compatible with startup board status. Different TOFPET IDs.')
      logging.warn(f'Detected on the board: {sorted(startupBoardStatus["tofpet_available"])}. Present in the configuration: {sorted(list(procCfg["fe"].keys()))}')
      
      if raiseExceptionOnFeMismatch:
        self.boardConf = procCfg
        raise RuntimeError(f'TOFPET mismatch: Detected on the board: {sorted(startupBoardStatus["tofpet_available"])}. Present in the configuration: {sorted(list(procCfg["fe"].keys()))}')

      # take the intesection between available and configured
      usableFeIds = set(procCfg['fe'].keys()) & set(startupBoardStatus['tofpet_available'])
      for feId in list(procCfg['fe']):
        if feId not in usableFeIds:
          del procCfg['fe'][feId]
      
      logging.warn(f'Using only FE IDs: {usableFeIds}')

    self.boardConf = procCfg
  

  def cleanChannelsConf(self, conf : dict, what: str = 'channels configuration'):
    """ Removes unused FE IDs from the passed channel configuration or calibration.
    """
    for feId in list(conf): # this creates a copy of the keys list
      if feId not in self.boardConf['fe'].keys():
        del conf[feId]
        logging.warn(f'Removing {what} for missing FE ID {feId}')


  def _readChannelsConfFromFile(self, filename):
    tmpConf = readCsvConfFile(filename)
    self.cleanChannelsConf(tmpConf)
    return tmpConf
    

  def _readTdcCalFromFile(self, filename):
    tmpConf = readCsvTdcCalFile(filename)
    self.cleanChannelsConf(tmpConf, what='TDC calibration')
    return tmpConf


  def _readQdcCalFromFile(self, filename):
    tmpConf = readCsvQdcCalFile(filename)
    self.cleanChannelsConf(tmpConf, what='QDC calibration')
    return tmpConf

  