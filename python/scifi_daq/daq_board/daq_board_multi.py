
from concurrent.futures import process
import logging
import toml
import os.path as op
import matplotlib
from multiprocessing.dummy import Pool as ThreadPool
from datetime import datetime
# from multiprocessing import Pool as ProcessPool

from scifi_daq.daq_board.daq_board import DaqBoard

def plt_multithread(func):
  def wrapper(*args, **kwargs):
    oldBackend = matplotlib.get_backend()
    matplotlib.use('agg')
    oldMaxOpenWarning = matplotlib.rcParams['figure.max_open_warning']
    # needed when calibrating many boards together, to avoid warnings
    matplotlib.rcParams['figure.max_open_warning'] = 100
    try:
      func(*args, **kwargs)
    finally:
      matplotlib.use(oldBackend)
      matplotlib.rcParams['figure.max_open_warning'] = oldMaxOpenWarning
  return wrapper

class DaqBoards():
  def __init__(self, confDir, subset=None, concurrency=1):
    confFile = op.join(confDir, 'configuration.toml')
    configuration = toml.load(confFile)['boards']
    self._boards = {}
    self._subsystems = {}

    if type(configuration) is list:  # this is the case where no subsystems have been specified
      for c in configuration:
        if subset is None or c['id'] in subset:
          self._boards[c['id']] = DaqBoard(op.join(confDir, c['conf_path']), c['addr'], port=c['port'], boardId=c['id'], name=c['name'] if 'name' in c else None)
    elif type(configuration) is dict:  # this is the case where subsystems have been specified
      for subsys, cfg in configuration.items():
        if subset is None or subsys in subset:
          self._subsystems[subsys] = []
          for c in cfg:
              self._boards[c['id']] = DaqBoard(op.join(confDir, c['conf_path']), c['addr'], port=c['port'], boardId=c['id'], name=c['name'] if 'name' in c else None)
              self._subsystems[subsys].append(c['id'])

    self._concurrency = concurrency

    try:
      self.boardMapping = toml.load(op.join(confDir, 'board_mapping.toml'))
    except OSError:
      self.boardMapping = None
    
    # check consistency of the board mapping (each board and slot must be used only once)
    if self.boardMapping is not None:
      checkDict = {}
      for sub, subsysMap in self.boardMapping.items():
        for plane, planeMap in subsysMap.items():
          if planeMap['class'] == 'multiboard':
            for b in planeMap["boards"]:
              if b in checkDict:
                raise RuntimeError(f'{sub}-{plane}: Board {b} already present')
              else:
                checkDict[b] = ['A', 'B', 'C', 'D']
          elif planeMap['class'] == 'multislot':
            for s in planeMap['slots']:
              try:
                if s in checkDict[planeMap["board"]]:
                  raise RuntimeError(f'{sub}-{plane}: Board {planeMap["board"]}, slot {s} already present')
                else:
                  checkDict[planeMap["board"]].append(s)
              except KeyError:
                checkDict[planeMap["board"]] = [s]
          elif planeMap['class'] == 'mixed':
            for b, s in zip(planeMap['boards'], planeMap['slots']):
              try:
                if s in checkDict[b]:
                  raise RuntimeError(f'{sub}-{plane}: Board {b}, slot {s} already present')
                else:
                  checkDict[b].append(s)
              except KeyError:
                checkDict[b] = [s]
          else:
            raise RuntimeError(f'Unknown plane class {planeMap["class"]}')
    
      # verifies that each board has a corresponding entry in the mapping
      for bid in self._boards.keys():
        if bid not in checkDict.keys():
          raise RuntimeError(f'Board {bid} has no corresponding entry in the board mapping')
      
      #and viceversa, but this is not a problem, just report it
      for bid in checkDict.keys():
        if bid not in self._boards.keys():
          logging.warn(f'Board {bid} is present in the mapping, but not in the chosen subset')

    

  def __del__(self):
    pass

  
  def boardIds(self):
    return list(self._boards.keys())
  

  def boardNames(self, nameKey=True):
    retdict = {}
    for bid, b in self._boards.items():
      if nameKey:
        retdict[b.name] = bid
      else:
        retdict[bid] = b.name
    return retdict

  
  def connect(self, timeout: float=None):
    for _, b in self._boards.items():
      b.connect(timeout=timeout)
  

  def timeout(self, timeout: float):
    def f(b : DaqBoard):
      b.timeout(timeout)
    self._runParallelThreads(f)
  

  def readConfiguration(self):
    def f(b : DaqBoard):
      b.readConfiguration(None)
    self._runParallelThreads(f)
  

  def fullReset(self):
    def f(b: DaqBoard):
      b.fullReset()
    self._runParallelThreads(f)
  

  def feReset(self):
    def f(b: DaqBoard):
      b.feReset()
    self._runParallelThreads(f)
  

  def readConfiguration(self):
    def f(b: DaqBoard):
      b.readConfiguration(None)
    self._runParallelThreads(f)

  
  def initialize(self, raiseExceptionOnFeMismatch: bool = False):
    def f(b: DaqBoard):
      b.initialize(raiseExceptionOnFeMismatch=raiseExceptionOnFeMismatch)
    self._runParallelThreads(f)
  

  def getFullConfiguration(self, strKeys=False):
    conf = self._runParallelThreads(lambda b : b.getFullConfiguration(strKeys))
    if strKeys:
      return {str(k): v for k, v in conf.items()}
    else:
      return conf


  def getStatus(self):
    def f(b: DaqBoard):
      return b.getStatus()
    return self._runParallelThreads(f)


  @plt_multithread
  def acquireTiaCalibrationData(self):
    def f(b: DaqBoard):
      b.acquireTiaCalibrationData()
    self._runParallelThreads(f)
  

  @plt_multithread
  def calculateTiaCalibration(self):
    def f(b: DaqBoard):
      b.calculateTiaCalibration()
    self._runParallelThreads(f)

  
  def applyTiaCalibration(self):
    def f(b: DaqBoard):
      b.applyTiaCalibration()
    self._runParallelThreads(f)
  

  @plt_multithread
  def tiaCalibration(self):
    def f(b: DaqBoard):
      b.tiaCalibration()
    self._runParallelThreads(f)


  @plt_multithread
  def acquireThrCalibrationData(self):
    def f(b: DaqBoard):
      b.acquireThrCalibrationData()
    self._runParallelThreads(f)
  

  @plt_multithread
  def calculateThrCalibration(self, saveFitPlots=False):
    def f(b: DaqBoard):
      b.calculateThrCalibration(saveFitPlots=saveFitPlots)
    self._runParallelThreads(f, jobs=1)
 

  def applyThrCalibration(self):
    def f(b: DaqBoard):
      b.applyThrCalibration()
    self._runParallelThreads(f)


  @plt_multithread
  def thrCalibration(self, saveFitPlots=False):
    def f(b: DaqBoard):
      b.thrCalibration(saveFitPlots=saveFitPlots)
    self._runParallelThreads(f)

  
  def acquireTdcCalibrationData(self):
    def f(b: DaqBoard):
      b.acquireTdcCalibrationData()
    self._runParallelThreads(f)

  
  @plt_multithread
  def calculateTdcCalibration(self, redChi2Limit=40.):
    def f(b: DaqBoard):
      b.calculateTdcCalibration(redChi2Limit=redChi2Limit)
    self._runParallelThreads(f, jobs=self._concurrency)


  @plt_multithread
  def tdcCalibration(self, redChi2Limit=40.):
    self.acquireTdcCalibrationData()
    self.calculateTdcCalibration(redChi2Limit=redChi2Limit)


  def acquireQdcCalibrationData(self):
    def f(b: DaqBoard):
      b.acquireQdcCalibrationData()
    self._runParallelThreads(f)
  

  @plt_multithread
  def calculateQdcCalibration(self, redChi2Limit=5.):
    def f(b: DaqBoard):
      b.calculateQdcCalibration(redChi2Limit=redChi2Limit)
    self._runParallelThreads(f, jobs=self._concurrency)


  @plt_multithread
  def qdcCalibration(self, redChi2Limit=5.):
    self.acquireQdcCalibrationData()
    self.calculateQdcCalibration(redChi2Limit=redChi2Limit)


  @plt_multithread
  def acquireThresholdScanData(self, minTime, minEvents, maxTime, showPlots=False, discriminators=['T1', 'T2', 'E']):
    def f(b: DaqBoard):
      b.acquireThresholdScanData(minTime, minEvents, maxTime, showPlots=showPlots, discriminators=discriminators)
    self._runParallelThreads(f)


  @plt_multithread
  def calculateThresholds(self, methodT1='fixed', methodT2='rate', methodE='rate', settingsT1=None, settingsT2=None, settingsE=None):
    def f(b: DaqBoard):
      b.calculateThresholds(methodT1=methodT1, methodT2=methodT2, methodE=methodE, settingsT1=settingsT1, settingsT2=settingsT2, settingsE=settingsE)
    self._runParallelThreads(f)


  def setTrigger(self, sources=None, seqCounter=None, seqPeriod=None):
    self._runParallelThreads(lambda b : b.setTrigger(sources, seqCounter, seqPeriod))


  def getTrigger(self):
    def f(b: DaqBoard):
      return b.getTrigger()
    return self._runParallelThreads(f)

  def startTriggerSequencer(self, block):
    def f(b: DaqBoard):
      b.startTriggerSequencer(block)
    self._runParallelThreads(f)
  

  def setInjection(self, enable=None, tofpetEnable=None, phase=None, duration=None, clk=None):
    self._runParallelThreads(lambda b : b.setInjection(enable=enable, tofpetEnable=tofpetEnable, phase=phase, duration=duration, clk=clk))

  
  def getInjection(self):
    def f(b: DaqBoard):
      return b.getInjection()
    return self._runParallelThreads(f)
  

  def setDebugBoardOutput(self, sma0=None, sma1=None, header=None):
    def f(b: DaqBoard):
      b.setDebugBoardOutput(sma0=sma0, sma1=sma1, header=header)
    self._runParallelThreads(f)


  def startDaq(self, address, port, enableBchannelData=False):
    def f(b: DaqBoard):
       b.startDaq(address, port, enableBchannelData=enableBchannelData)
    self._runParallelThreads(f)


  def stopDaq(self):
    def f(b: DaqBoard):
      b.stopDaq()
    self._runParallelThreads(f)
  

  def updateFirmware(self):
    def f(b: DaqBoard):
      b.updateFirmware()
    self._runParallelThreads(f)

  
  def disableLeds(self, disable):
    def f(b: DaqBoard):
      b.disableLeds(disable)
    self._runParallelThreads(f)
    
    
  def setClk40UseQpll(self, useQpll: bool):
    def f(b: DaqBoard):
      b.setClk40UseQpll(useQpll)
    self._runParallelThreads(f)
  

    
  def getClk40UseQpll(self):
    def f(b: DaqBoard):
      return b.getClk40UseQpll()
    return self._runParallelThreads(f)
  

  def reboot(self):
    def f(b: DaqBoard):
      b.reboot()
    self._runParallelThreads(f)
  
  
  def setDateTime(self, dateTime: datetime = None):
    def f(b: DaqBoard):
      b.setDateTime(dateTime)
    self._runParallelThreads(f)


  def _runParallelThreads(self, func, jobs=None):
    processes = len(self._boards) if jobs is None else jobs
    if processes == 0:
      processes = 1
    pool = ThreadPool(len(self._boards) if jobs is None else jobs)
    res = pool.map_async(func, list(self._boards.values()))
    # print(res)
    pool.close()
    pool.join()

    return dict(zip(self.boardIds(), res.get()))
  

  # def _runParallelProcesses(self, func, jobs=None):
  #   pool = ProcessPool(len(self._boards) if jobs is None else jobs)
  #   res = pool.map_async(func, list(self._boards.values()))
  #   # print(res)
  #   pool.close()
  #   pool.join()

  #   return dict(zip(self.boardIds(), res.get()))
