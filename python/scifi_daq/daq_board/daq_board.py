#from ctypes import Union
import os
import os.path as op
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import h5py

from ._calibration._tia_calibration import *
from ._calibration._thr_calibration import *
from ._calibration._tdc_calibration import *
from ._calibration._qdc_calibration import *
from ._calibration._dcr_calibration import *
from ._calibration._thresholds import *
from scifi_daq.daq_board.daq_board_base import DaqBoardBase
# from scifi_daq.daq_board._calibration._calibration_functions import *

class DaqBoard(DaqBoardBase):
  
  def __init__(self, configurationPath, address, port=41968, boardId=None, name=None):
    super().__init__(configurationPath, address, port, boardId, name)
  
  
  def acquireTiaCalibrationData(self, showPlots=False):
    """ Acquires the data needed for the TIA baseline calibration.
    The data gets stored in the `tia_bl_cal/data.hdf5` file in the board configuration folder.
    The DAQ board is required to be connected and the bias voltage should be below breakdown.

    Parameters
    ----------
    showPlots: if True, the produced plots are also displayed and need to be manually closed to continue.
    """
    acquireTiaCalibrationData(self, showPlots=showPlots)

  
  def calculateTiaCalibration(self, showPlots=False):
    """ Calculates the TIA baseline calibration and stores it to disk.
    It also saves the counts and baselines plots inside `tia_bl_cal/plots`.
    This method will NOT update the configuration on the DAQ board.
    The DAQ board is not required to be connected.
    
    Parameters
    ----------
    showPlots: if True, the produced plots are also displayed and need to be manually closed to continue.
    """
    calculateTiaCalibration(self, showPlots=showPlots)
  

  def applyTiaCalibration(self):
    """ Applies the TIA baselines to the DAQ board.
    The DAQ board is required to be connected.
    """
    applyTiaCalibration(self)
  

  def tiaCalibration(self, showPlots=False):
    """ Performs the full TIA calibration.
      * Collects the data;
      * Calculates the calibration;
      * Applies it to the board.

    Parameters
    ----------
    showPlots: if True, the produced plots are also displayed and need to be manually closed to continue.
    """
    self.acquireTiaCalibrationData(showPlots=showPlots)
    self.calculateTiaCalibration(showPlots=showPlots)
    self.applyTiaCalibration()

  
  def acquireThrCalibrationData(self, showPlots=False):
    """ Acquires the data needed for the thresholds baseline calibration.
    The data gets stored in the `thr_bl_cal/data.hdf5` file in the board configuration folder.
    The DAQ board is required to be connected and the bias voltage should be below breakdown.
    
    Parameters
    ----------
    showPlots: if True, the produced plots are also displayed and need to be manually closed to continue.
    """
    acquireThrCalibrationData(self, showPlots=showPlots)

  
  def calculateThrCalibration(self, showPlots=False, saveFitPlots=False):
    """ Calculates the threshold baselines and stores them to disk.
    It also saves the counts and baselines plots inside `thr_bl_cal/plots`.
    Optionally, it can save all the fits in separate plots in `thr_bl_cal/plots/fits`, but it takes longer to run.
    This method will NOT update the configuration on the DAQ board.
    The DAQ board is not required to be connected.
    
    Parameters
    ----------
    showPlots: if True, the produced plots are also displayed and need to be manually closed to continue.
    saveFitPlots: if True, saves the fit plots as well. These are never shown.
    """
    calculateThrCalibration(self, showPlots=showPlots, saveFitPlots=saveFitPlots)
  

  def applyThrCalibration(self):
    """ Applies the threshold baselines to the DAQ board.
    The DAQ board is required to be connected.
    """
    applyThrCalibration(self)
  

  def thrCalibration(self, showPlots=False, saveFitPlots=False):
    """ Performs the full thresholds baseline calibration.
      * Collects the data;
      * Calculates the calibration;
      * Applies it to the board.

    Parameters
    ----------
    showPlots: if True, the produced plots are also displayed and need to be manually closed to continue.
    """
    self.acquireThrCalibrationData(showPlots=showPlots)
    self.calculateThrCalibration(showPlots=showPlots, saveFitPlots=saveFitPlots)
    self.applyThrCalibration()


  def acquireTdcCalibrationData(self):
    """ Acquires the data needed for the TDC calibration.
    The data gets stored in the `tdc_bl_cal/data.hdf5` file in the board configuration folder.
    The DAQ board is required to be connected and the bias voltage should be below breakdown.
    """
    acquireTdcCalibrationData(self)

  
  def calculateTdcCalibration(self, showPlots=False, redChi2Limit=40.):
    """ Calculates the TDC calibration and stores it to disk.
    It also saves the histograms of the calculated parameters and failed fits inside `tdc_bl_cal/plots`.
    The DAQ board is not required to be connected.
    
    Parameters
    ----------
    showPlots: if True, the produced plots are also displayed and need to be manually closed to continue.
    redChi2Limit: Minimum reduced chi^2 to consider a fit failed
    """
    calculateTdcCalibration(self, showPlots=showPlots, redChi2Limit=redChi2Limit)
  

  def tdcCalibration(self, showPlots=False, redChi2Limit=40.):
    """ Performs the full TDC calibration.
      * Collects the data;
      * Calculates the calibration.

    Parameters
    ----------
    showPlots: if True, the produced plots are also displayed and need to be manually closed to continue.
    redChi2Limit: Minimum reduced chi^2 to consider a fit failed.
    """
    self.acquireTdcCalibrationData()
    self.calculateTdcCalibration(showPlots=showPlots, redChi2Limit=redChi2Limit)


  def acquireQdcCalibrationData(self):
    """ Acquires the data needed for the QDC calibration.
    The data gets stored in the `qdc_bl_cal/data.hdf5` file in the board configuration folder.
    The DAQ board is required to be connected and the bias voltage should be below breakdown.
    """
    acquireQdcCalibrationData(self)

  
  def calculateQdcCalibration(self, showPlots=False, redChi2Limit=5.):
    """ Calculates the QDC calibration and stores it to disk.
    It also saves the histograms of the calculated parameters and failed fits inside `qdc_bl_cal/plots`.
    The DAQ board is not required to be connected.
    
    Parameters
    ----------
    showPlots: if True, the produced plots are also displayed and need to be manually closed to continue.
    redChi2Limit: Minimum reduced chi^2 to consider a fit failed.
    """
    calculateQdcCalibration(self, showPlots=showPlots, redChi2Limit=redChi2Limit)
 

  def qdcCalibration(self, showPlots=False, redChi2Limit=5.):
    """ Performs the full QDC calibration.
      * Collects the data;
      * Calculates the calibration.

    Parameters
    ----------
    showPlots: if True, the produced plots are also displayed and need to be manually closed to continue.
    redChi2Limit: Minimum reduced chi^2 to consider a fit failed.
    """
    self.acquireQdcCalibrationData()
    self.calculateQdcCalibration(showPlots=showPlots, redChi2Limit=redChi2Limit)


  
  def acquireThresholdScanData(self, minTime, minEvents, maxTime, showPlots=False, discriminators=['T1', 'T2', 'E']):
    """ Acquires the threshold scan data.
    The data gets stored in the `pe_cal/data.hdf5` file in the board configuration folder.
    Plots with the scan are saved in `pe_cal/plots`.
    The DAQ board is required to be connected and the bias voltage should be at the operation point.

    Parameters
    ----------
    showPlots: if True, the produced plots are also displayed and need to be manually closed to continue.
    """
    acquireThresholdScanData(self, minTime, minEvents, maxTime, showPlots=showPlots, discriminators=discriminators)
  

  def calculateThresholds(self, methodT1='fixed', methodT2='rate', methodE='rate', settingsT1=None, settingsT2=None, settingsE=None):
    """ Calculates the thresholds for each channel.
    It also saves the histograms of the calculated parameters and failed fits inside `qdc_bl_cal/plots`.
    The DAQ board is not required to be connected.
    
    Parameters
    ----------
    methodX: method to be used to calculate the thresholds for discriminator X (T1, T2 and E). Methods are described below.

    settingsX: settings relative to the chosen method, described below.

    Methods and settings
    --------------------
    * `methodX='fixed'`
    The thresholds of the X discriminator is set to a fixed value on all channels. The relative `settingsX` parameter must be of form `{'threshold': <thr>}`.
    * `method='rate'`
    The thresholds are set in order to have a fixed dark rate on each channel. The relative `settingsX` parameter must be of form `{'rateMax': <rate>}`.
    * `methodX='snd_us_fixed'`
    Special method to set the thresholds of the US system in SND@LHC, as it contains two types of SiPMs. The relative `settingsX` parameter must be of form `{'thresholdL': <thrL>, 'thresholdS': <thrS>}` for large and small SiPMs respectively.
    """
    calculateThresholds(self, methodT1=methodT1, methodT2=methodT2, methodE=methodE, settingsT1=settingsT1, settingsT2=settingsT2, settingsE=settingsE)

  
  def getTiaCalData(self, discriminator: str, tofpet: int, channels = None):
    """Opens Tia calibration data for a specified Tofpet ID"""
    with h5py.File(self._boardConf.tiaCalDataFile, 'r') as f:
      counts_arr = np.array(f[f'{discriminator}/tofpet_{tofpet}/counts'])
      channels_arr = np.array(f[f'{discriminator}/tofpet_{tofpet}/channels'])
    #print(f.keys())
    return counts_arr, channels_arr


  def plotTiaCalData(self, discriminator: str, tofpet: int, channels = None):
    fig, ax = plt.subplots()
    counts_arr, channels_arr = self.getTiaCalData(discriminator, tofpet, channels)
    ax.plot(counts_arr.T)
    return fig, ax, channels_arr
  

  def getThrCalData(self, discriminator: str, tofpet: int, channels = None):
    """Opens Threshold calibration data for a specified Tofpet ID"""
    with h5py.File(self._boardConf.thrCalDataFile, 'r') as f:
      counts_arr = np.array(f[f'{discriminator}/tofpet_{tofpet}/counts'])
      channels_arr = np.array(f[f'{discriminator}/tofpet_{tofpet}/channels'])
    return counts_arr, channels_arr


#modify
  def plotThrCalData(self, discriminator: str, tofpet: int, channels = None):
    fig, ax = plt.subplots()
    counts_arr, channels_arr = self.getThrCalData(discriminator, tofpet, channels)
    ax.plot(counts_arr.T)
    return fig, ax, channels_arr
  

  def getPeCalData(self, discriminator: str, tofpet: int, channels = None):
    """Opens Pe calibration data for a specified Tofpet ID"""
    with h5py.File(self._boardConf.peCalDataFile, 'r') as f:
      rate_arr = np.array(f[f'{discriminator}/tofpet_{tofpet}/rate'])
      channels_arr = np.array(f[f'{discriminator}/tofpet_{tofpet}/channels'])
    return rate_arr, channels_arr


  def plotPeCalData(self, discriminator: str, tofpet: int, channels = None):
    fig, ax = plt.subplots()
    rate_arr, channels_arr = self.getPeCalData(discriminator, tofpet, channels)
    ax.plot(rate_arr.T)
    return fig, ax, channels_arr
  

  def getTdcCalData(self, tofpet: int):
    """Opens Tdc calibration data for a specified Tofpet ID"""
    f = pd.read_hdf(self._boardConf.tdcCalDataFile, 'data')
    selection  = (f['tofpet_id'] == tofpet) 
    phase_arr = np.array(f[selection]['phase'])
    fine_arr = np.array(f[selection]['v_fine'])
    channel_arr = np.array(f[selection]['tofpet_channel'])
    tac_arr = np.array(f[selection]['tac'])
    
    return phase_arr, fine_arr, channel_arr, tac_arr
  
  def allTdcCalData(self, tofpet: int):
    """Returns all Threshold calibration data for a specified Tofpet ID"""
    f = pd.read_hdf(self._boardConf.tdcCalDataFile, 'data')
    selection  = (f['tofpet_id'] == tofpet) 
    f_tofpet =  f[selection]
    return f_tofpet


  def plotTdcCalData(self, discriminator: str, tofpet: int, channels = None):
    fig, ax = plt.subplots()
    phase_arr, fine_arr = self.getTdcCalData(discriminator, tofpet, channels)
    ax.plot(phase_arr.T)
    return fig, ax, fine_arr


  def getQdcCalData(self, tofpet: int):
    """Opens Qdc calibration data for a specified Tofpet ID"""
    f = pd.read_hdf(self._boardConf.qdcCalDataFile, 'data')
    selection  = (f['tofpet_id'] == tofpet) 
    coarse_arr = np.array(f[selection]['v_coarse'])
    fine_arr = np.array(f[selection]['v_fine'])
    channel_arr = np.array(f[selection]['tofpet_channel'])
    tac_arr = np.array(f[selection]['tac'])
    return coarse_arr, fine_arr, channel_arr, tac_arr


  def plotQdcCalData(self, discriminator: str, tofpet: int, channels = None):
    fig, ax = plt.subplots()
    phase_arr, fine_arr = self.getQdcCalData(discriminator, tofpet, channels)
    ax.plot(phase_arr.T)
    return fig, ax, fine_arr
  

  def allQdcCalData(self, tofpet: int):
    """Returns all Qdc calibration data for a specified Tofpet ID"""
    f = pd.read_hdf(self._boardConf.qdcCalDataFile, 'data')
    selection  = (f['tofpet_id'] == tofpet) 
    f_tofpet =  f[selection]
    return f_tofpet
  
  
  def getTdc(self, tofpet: int):
    tdcCal = pd.read_csv(self._boardConf.tdcCalPath)
    return tdcCal
  