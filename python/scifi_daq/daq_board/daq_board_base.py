from datetime import datetime
import json
import numpy as np
import logging


from scifi_daq.network.connection import Connection
from scifi_daq.daq_board.daq_board_conf import DaqBoardConfiguration

class DaqBoardBase():
  """ Base class used to manage the DAQ board.
  It contains the basic functionalities to read and write configurations, start and top the DAQ, etc.
  
  It is advised to use the DaqBoard class, which contains more advanced functionalities.
  """
  def __init__(self, configurationPath, address, port=41968, boardId = None, name = None):
    """ Constructs the class, initializing the configuration path and the board address and port.
    """
    self._conn = Connection(address, port)
    self.confPath = configurationPath
    self.boardId = boardId
    self.name = name


  def __del__(self):
    self.disconnect()
  

  def connect(self, timeout: float = None):
    """ Opens the connection to the DAQ board.
    """
    self._conn.connect(timeout)
  

  def disconnect(self):
    """ Opens the connection to the DAQ board.
    It's not necessary to call this, as it is called by the destructor.
    """
    self._conn.disconnect()
  

  def timeout(self, timeout: float):
    self._conn.timeout(timeout)
  

  def readConfiguration(self, startupBoardStatus, raiseExceptionOnFeMismatch: bool = False):
    """ Reads the configuration, without applying it to the board.
    
    If `raiseExceptionOnFeMismatch` is `True`, a `RuntimeError` is raised if the TOFPET IDs specified in the configuration don't match the ones found on the board.
    Otherwise, a warning is printed.
    """
    self._boardConf = DaqBoardConfiguration(self.confPath, startupBoardStatus, raiseExceptionOnFeMismatch=raiseExceptionOnFeMismatch)


  def initialize(self, raiseExceptionOnFeMismatch: bool = False):
    """ Initializes the FE with the content of the configuration files.
    
    If `raiseExceptionOnFeMismatch` is `True`, a `RuntimeError` is raised if the TOFPET IDs specified in the configuration don't match the ones found on the board.
    Otherwise, a warning is printed.
    """
    logging.info('initialize: reading board startup status')
    bss = self.getBoardStartupStatus()
    
    if self.boardId is not None and self.boardId != bss['board_id']:
      raise RuntimeError(f'Board ID from the board ({bss["board_id"]}) does not corespond to the configuration file ({self.boardId})')
    elif self.boardId is None:
      self.boardId = bss['board_id'] # if not specified, it will be taken from the bss

    if self.name is None:
      self.name = f'board_{self.boardId:02d}'

    logging.info('initialize: reading board configuration')
    self.readConfiguration(bss, raiseExceptionOnFeMismatch=raiseExceptionOnFeMismatch)

    logging.info(f'initialize: update board configuration {self._conn.address()}:{self._conn.port()}')
    self.updateBoardConf(self._boardConf.boardConf['board'])

    logging.info(f'initialize: update FE configuration {self._conn.address()}:{self._conn.port()}')
    self.updateFeConf(
      self._boardConf.boardConf['fe'],
      DaqBoardConfiguration.mergeChannelConfigurations(self._boardConf.tiaBlConf, self._boardConf.thrBlConf, self._boardConf.channelsConf, self._boardConf.qdcRangeConf, self._boardConf.thresholdsConf)
    )
  
  
  def getFullConfiguration(self, strKeys: bool = False):
    """ Returns the full board and TOFPETs configuration.
    The configuration is NOT read from the board, but from the one read from the configuration files upon construction of the instance of the class.

    If strKeys is True, all the integer keys (FE IDs, channels) are converted to strings (e.g. to make the returned dictionary convertible to JSON)
    """
    bConf = self._boardConf.boardConf['board']
    fConf = self._boardConf.boardConf['fe']
    cConf = DaqBoardConfiguration.mergeChannelConfigurations(self._boardConf.tiaBlConf, self._boardConf.thrBlConf, self._boardConf.channelsConf, self._boardConf.qdcRangeConf, self._boardConf.thresholdsConf)

    tdcCal = self._boardConf.tdcCal
    qdcCal = self._boardConf.qdcCal

    fConfStr = {}
    cConfStr = {}
    tdcCalStr = {}
    qdcCalStr = {}
    if strKeys:
      for k, v in fConf.items():
        fConfStr[str(k)] = v
      
      for k1, v1 in cConf.items():
        cConfStr[str(k1)] = {}
        for k2, v2 in v1.items():
          cConfStr[str(k1)][str(k2)] = v2
      
      for k1, v1 in tdcCal.items():
        tdcCalStr[str(k1)] = {}
        for k2, v2 in v1.items():
          tdcCalStr[str(k1)][str(k2)] = {}
          for k3, v3 in v2.items():
            tdcCalStr[str(k1)][str(k2)][str(k3)] = {}
            for k4, v4 in v3.items():
              tdcCalStr[str(k1)][str(k2)][str(k3)][str(k4)] = v4
      
      for k1, v1 in qdcCal.items():
        qdcCalStr[str(k1)] = {}
        for k2, v2 in v1.items():
          qdcCalStr[str(k1)][str(k2)] = {}
          for k3, v3 in v2.items():
            qdcCalStr[str(k1)][str(k2)][str(k3)] = v3

      
      return {'board': bConf, 'fe': fConfStr, 'channels': cConfStr, 'tdc_cal': tdcCalStr, 'qdc_cal': qdcCalStr}
    else:
      return {'board': bConf, 'fe': fConf, 'channels': cConf, 'tdc_cal': tdcCal, 'qdc_cal': qdcCal}
    

  def fullReset(self):
    """ Performs a full FPGA and TOFPET reset.
    A full reset is performed (like a FPGA reboot) and the default configuration is written in the TOFPETs (i.e. all channels are disabled).
    The processor is not reset by this command, that requires a reboot.
    """
    reply = self._conn.sendCommand(json.dumps({'command': 'full_reset'}))
    if reply['response'] != 'ok':
      raise RuntimeError(reply['result'])
  

  def feReset(self):
    """ Performs a front-end reset.
    A FE reset just clears all the DAQ-related registers (e.g. trigger counter, hit counters, FIFO flags, etc), while keeping the TOFPET configuration.
    """
    reply = self._conn.sendCommand(json.dumps({'command': 'fe_reset'}))
    if reply['response'] != 'ok':
      raise RuntimeError(reply['result'])
  

  def getBoardStartupStatus(self, reset: bool = False):
    """ Returns the board startup status.
    The startup status contains the detected FE boards (by reading the TOFPET board ID) and the TOFPET chips that respond to a configuration read.
    """
    reply = self._conn.sendCommand(json.dumps({'command': 'get_board_startup_status', 'args': {'reset': reset}}))
    if reply['response'] == 'ok':
      return reply['result']
    else:
      raise RuntimeError(reply['result'])
  

  def disableLeds(self, disable: bool):
    """ Enables or disables the LEDs on the board.
    This doesn't work on the TTCrx and Ethernet LEDs, which have to be disabled using the physical switches on the board.
    """
    reply = self._conn.sendCommand(json.dumps({'command': 'disable_leds', 'args': {'disable': disable}}))
    if reply['response'] != 'ok':
      raise RuntimeError(reply['result'])
  

  def getStatus(self):
    """ Returns the board status.
    Used for monitoring the board. It returns a dictionary with the following items:
    - board_id: ID of the DAQ board.
    - fe_ids: IDs of the TOFPET boards.
    - leds_disabled: True if the LEDs are disabled.
    - fe_enabled: 8-bit bitmap, where 1 means that that TOFPET is taking data.
    - fw_info: dictionary with firmware version and its date and time of compilation.
    - ttcrx_ready: True if the TTCrx is receiving a valid signal on the fibre.
    - qpll_locked: True if the QPLL is locked. If it is 1, the board is running synchronously with the others.
    - b_channel_fifo_full_latch: True if the B-Channel FIFO has ever been full since the last reset (board version >= 4).
    - fe_reset_reg_cnt: number of FE resets issued from the register in the FPGA. This number resets on a full FPGA reset.
    - fe_reset_ttc_cnt: number of FE resets issued from the TTC system. This number resets on a full FPGA reset.
    - trigger_cnt: number of trigger received. This number resets on a FE reset.
    - sipm_temperatures: list with the SiPM temperatures read from each FE board
    - fe_temperatures: list with the temperature of each TOFPET board
    - board_temperature: temperature read by the sensor connected to the DAQ board (board version >= 4).
    """
    reply = self._conn.sendCommand(json.dumps({'command': 'board_status'}))
    if reply['response'] == 'ok':
      return reply['result']
    else:
      raise RuntimeError(reply['result'])
  

  def buildCalibrationLog(self, startLog: dict = None):
    """ Builds the dictionary containing the conditions at which a calibration was performed.
    
    Parameters
    ----------
    startLog: Previous log, the end time will be added to this. If None, a new log is created.
    """
    status = self.getStatus()
    time = datetime.utcnow().replace(microsecond=0).strftime('%Y-%m-%dT%H:%M:%SZ')
    if startLog is None:
      return dict(start_time=time, fe_ids=status['fe_ids'], fe_temps_start=status['fe_temperatures'], sipm_temps_start=status['sipm_temperatures'])
    else:
      return {**startLog, **dict(end_time=time, fe_temps_end=status['fe_temperatures'], sipm_temps_end=status['sipm_temperatures'])}

  
  def setBoardConf(self, conf: dict):
    """ Writes the board configuration.
    conf must be a dictionary containing the keys 'clock_40_phase' and 'clock_tofpet_phase'.
    """
    try:
      self.setClk40Des1Phase(conf['clock_40_des1_phase'])
    except KeyError: # compatibility with old conf file structure
      self.setClk40Des1Phase(conf['clock_40_phase'])
    
    try:
      self.setClk40Des2Phase(conf['clock_40_des2_phase'])
    except KeyError: # compatibility with old conf file structure
      self.setClk40Des2Phase(0)
    
    try:
      self.setClk40UseQpll(conf['clock_40_use_qpll'])
    except KeyError: # compatibility with old conf file structure
      self.setClk40UseQpll(True)
    
    reply = self._conn.sendCommand(json.dumps({'command': 'set_clk_tofpet_phase', 'args': {'phase': conf['clock_tofpet_phase']}}))
    if reply['response'] != 'ok':
      raise RuntimeError(reply['result'])


  def getBoardConf(self):
    """ Reads and returns the board configuration.
    """
    clk40Des1Phase = self.getClk40Des1Phase()
    clk40Des2Phase = self.getClk40Des2Phase()
    clk40UseQpll = self.getClk40UseQpll()
    
    reply = self._conn.sendCommand(json.dumps({'command': 'get_clk_tofpet_phase', 'args': {}}))
    clkTofpetPhase = 0
    if reply['response'] == 'ok':
      clkTofpetPhase = reply['result']['phase']
    else:
      raise RuntimeError(reply['result'])
    
    return {'clock_40_des1_phase': clk40Des1Phase, 'clock_40_des2_phase': clk40Des2Phase, 'clock_40_use_qpll': clk40UseQpll, 'clock_tofpet_phase': clkTofpetPhase}
    

  def updateBoardConf(self, conf: dict):
    """ Updates the board configuration.
    It reads the board configuration, updates it with the items present in conf and writes it.
    conf should contain at least one of the keys listed in the documentation fon setBoardConf(...)
    """
    confTmp = self.getBoardConf()

    for k, c in conf.items():
      confTmp[k] = c
    
    self.setBoardConf(confTmp)


  def getFeConf(self, ids: list):
    """ Reads the FE confguration.
    ids is the list of TOFPET IDs to read the configuration from.
    """
    reply = self._conn.sendCommand(json.dumps({'command': 'get_fe_conf', 'args': {'fe_ids': ids}}))
    if reply['response'] == 'ok':
      conf = reply['result']['configurations']
      ret_dict = {}
      for feid, cfg in conf:
        ret_dict[feid] = cfg
      return ret_dict 
    else:
      raise RuntimeError(reply['result'])
  

  def setFeConf(self, configurations: dict):
    """ Writes the front-end configurations.
    Should not be used directly, use the update functions (updateFeConf, updateFeGlobalConf and updateFeChannelsConf).
    """
    cfgs = [[k, v] for k, v in configurations.items()]
    reply = self._conn.sendCommand(json.dumps({'command': 'set_fe_conf', 'args': {'configurations': cfgs}}))
    if reply['response'] != 'ok':
      raise RuntimeError(reply['result'])
  

  def updateFeConf(self, globalConf: dict, channelsConf: dict):
    """ Updates selected items in both the global and channels FE configuraition.
    See updateFeGlobalConf(...) and updateFeChannelsConf(...) for the structure of the arguments.
    """
    # read current conf (globalConf.keys() is the list of the tofpet_ids that will be read)
    conf = self.getFeConf(list(globalConf.keys()))

    # loop over global conf and modify the required elements in conf
    for tpId, newConf in globalConf.items():
      for confKey, confValue in newConf.items():
        if confKey in conf[tpId]['global']:
          conf[tpId]['global'][confKey] = confValue
        else:
          logging.warn('updateFeConf: global key ' + confKey + ' not found.')
    
    # loop over channelsConf and modiy the required elements in conf
    for tpId, newConf in channelsConf.items():
      for ch, chConf in newConf.items():
        for confKey, confValue in chConf.items():
          if confKey in conf[tpId]['channels'][ch]:
            conf[tpId]['channels'][ch][confKey] = confValue
          elif confKey == 'masked':
            conf[tpId]['channels'][ch]['trig_mode'] = 'DISABLED' if confValue else 'NORMAL'
          else:
            logging.warn('updateFeConf: channels key ' + confKey + ' not found.')
    self.setFeConf(conf)
  

  def updateFeGlobalConf(self, configurations: dict):
    """Updates selected items in global configurations.
    It reads the FE configuration from the board, updates the global part with the items in `configuration` and writes it to the board again.
    configurations must have the structure: {tp_id0: {'key': value, ...}, ...}
    Wrong keys in the configuration are ignored, but a warning is raised.
    """
    conf = self.getFeConf(list(configurations.keys()))
    for tpId, newConf in configurations.items():
      for confKey, confValue in newConf.items():
        if confKey in conf[tpId]['global']:
          conf[tpId]['global'][confKey] = confValue
        else:
          logging.warn('updateFeGlobalConf: key ' + confKey + ' not found.')
    self.setFeConf(conf)
  

  def updateFeChannelsConf(self, configurations: dict):
    """Updates selected items in channel configurations
    It reads the FE configuration from the board, updates the channels part with the items in `configuration` and writes it to the board again.
    configurations must have the structure: {tp_id0: {ch0: {...}, ch1: {...}}, ...}
    Wrong keys in the configuration are ignored, but a warning is raised.
    """
    conf = self.getFeConf(list(configurations.keys()))
    for tpId, newConf in configurations.items():
      for ch, chConf in newConf.items():
        for confKey, confValue in chConf.items():
          if confKey in conf[tpId]['channels'][ch]:
            conf[tpId]['channels'][ch][confKey] = confValue
          elif confKey == 'masked':
            conf[tpId]['channels'][ch]['trig_mode'] = 'DISABLED' if confValue else 'NORMAL'
          else:
            logging.warn('updateFeChannelsConf: key ' + confKey + ' not found.')
    self.setFeConf(conf)
  

  def startDaq(self, address: str, port: int, feIds: list = None, buildPackets: bool = False, enableBchannelData: bool = False):
    """ Starts the data acquisition.
    Parameters:
    - address: address of the DAQ server
    - port: port of the DAQ server
    - feIds: a list of the TOFPET IDs that will be enabled to take data. If None, all the TOFPETs listed in the configuration files are used.
    - buildPackets: if True, data packets are build before being trasmitted. If False, the raw data is transmitted as-is, without any verification (much faster and the verification can happen on the server).
    - enableBchannelData: trasmit the received B-Channel data in the data stream.
    """
    # TODO check that the configuration is in a valid state (missing thr and tia -> error, missing tdc, qdc -> warning)
    if feIds is None:
      feIds = self._boardConf.getFeIds()
    
    reply = self._conn.sendCommand(json.dumps({'command': 'start_daq', 'args': {'address': address, 'port': str(port), 'transmit_min': 100000, 'max_wait_time': 1.0, 'fe_ids': feIds, 'build_packets': buildPackets, 'enable_b_channel_data': enableBchannelData}}))
    if reply['response'] != 'ok':
      raise RuntimeError(reply['result'])


  def stopDaq(self):
    """ Stops the data acquisition.
    """
    reply = self._conn.sendCommand(json.dumps({'command': 'stop_daq', 'args': {}}))
    if reply['response'] != 'ok':
      raise RuntimeError(reply['result'])
  

  def setTrigger(self, sources: list = None, seqCounter: int = None, seqPeriod: int = None):
    """ Sets up the trigger and the trigger sequencer.
    Parameters:
    - sources: a list with the trigger sources to be enabled. Available values: 'ttc', 'sequencer', 'lemo' (CHECK), 'sma' (not available in the default configuration)
    - seqCounter: number of triggers generated by the sequencer
    - seqPeriod: period of the trigger sequencer, in clock cycles of the 40 MHz clock. E.g. to generate triggers with a rate of 1 kHz, use 40000
    """
    args = {}
    if sources is not None:
      args['sources'] = sources
    
    if seqCounter is not None:
      args['seq_counter'] = seqCounter
    
    if seqPeriod is not None:
      args['seq_period'] = seqPeriod
    
    reply = self._conn.sendCommand(json.dumps({'command': 'set_trigger', 'args': args}))
    if reply['response'] != 'ok':
      raise RuntimeError(reply['result'])
  

  def getTrigger(self):
    """ Returns the status of the trigger and the trigger sequencer, including how many pulses still need to be generated.
    """
    reply = self._conn.sendCommand(json.dumps({'command': 'get_trigger', 'args': {}}))
    if reply['response'] == 'ok':
      return reply['result']
    else:
      raise RuntimeError(reply['result'])
  

  def startTriggerSequencer(self, block: bool):
    """ Starts the trigger sequencer.
    Once started, it cannot be stopped (#TODO allow to stop it?)
    If block is True, the function blocks until the sequencer has finished, otherwise returns immediately and the status can be monitored with getTrigger()
    """
    reply = self._conn.sendCommand(json.dumps({'command': 'start_trigger_sequencer', 'args': {'block': block}}))
    if reply['response'] != 'ok':
      raise RuntimeError(reply['result'])
  

  def setInjection(self, enable: bool = None, tofpetEnable: bool = None, laserEnable: bool = None, phase: int = None, duration: int = None, clk: str = None):
    """ Sets up the injection.
    If a parameter is None, its value is not modified.
    Parameters:
    enable: enables the injection system altogether, allowing the test pulse to be generated on a trigger.
    tofpetEnable: if True, the test pulse is delivered to the TOFPETs. If False the pulse can still be routed to a SMA debug output with setDebugBoardOutput(...)
    phase: phase relative to the clock. 0 to 223 when using the 40 MHz clock, 0 to 55 with the 160 MHz one.
    duration: 1 or more, duration of the test pulse in clock cycles
    clk: 'clk40' or 'clk160', to select the clock used to generate the test pulse
    """
    args = {}
    if enable is not None:
      args['enable'] = enable
    
    if tofpetEnable is not None:
      args['tofpet_enable'] = tofpetEnable
    
    if laserEnable is not None:
      args['laser_enable'] = laserEnable
    
    if phase is not None:
      args['phase'] = phase
    
    if duration is not None:
      args['duration'] = duration
    
    if clk is not None:
      args['clock'] = clk
    
    reply = self._conn.sendCommand(json.dumps({'command': 'set_injection', 'args': args}))
    if reply['response'] != 'ok':
      raise RuntimeError(reply['result'])
  

  def getInjection(self):
    """ Returns the injection status.
    """
    reply = self._conn.sendCommand(json.dumps({'command': 'get_injection', 'args': {}}))
    if reply['response'] == 'ok':
      return reply['result']
    else:
      raise RuntimeError(reply['result'])
  

  # def getTiaCalibrationData(self, feChannels, disc):
  #   """ Get data for the TIA calibration.
  #   feChannels must be of the form `{id0: [ch0, ch1, ...], ...}`
  #   disc is either 'T1', 'T2' or 'E'
  #   """
  #   return self._getCounterData('tia_baseline_calibration', feChannels, disc)
  

  # def getThrCalibrationData(self, feChannels, disc):
  #   """ Get data for the threshold baseline calibration.
  #   feChannels must be of the form `{id0: [ch0, ch1, ...], ...}`
  #   disc is either 'T1', 'T2' or 'E'
  #   """
  #   return self._getCounterData('disc_thr_calibration', feChannels, disc)
  

  # def getDarkCountsData(self, feChannels, disc, minDuration, minEvents, maxDuration):
  #   """ Get dark counts.
  #   feChannels must be of the form `{id0: [ch0, ch1, ...], ...}`
  #   disc is either 'T1', 'T2' or 'E'
  #   """
  #   return self._getCounterData('get_dark_counts_2', feChannels, disc, min_duration=minDuration, min_events=minEvents, max_duration=maxDuration)
  #   # return self._getCounterData('get_dark_counts', feChannels, disc, min_duration=minDuration, min_events=minEvents, max_duration=maxDuration)


  def _getCounterData(self, command: str, feChannels: dict, disc: str, **kwargs):
    """
    Sends one of the commands that uses the TOFPET counter and formats the returned data.
    The data is a map<int, int> in C++ and this becomes a weird array in python [[k, v], [k, v]] because JSON does not support int keys.
    This method fixes it and transforms the ma in a dictionary.
    feChannels must be of the form `{id0: [ch0, ch1, ...], ...}`
    disc is either 'T1', 'T2' or 'E'
    """
    feChs = [[k, v] for k, v in feChannels.items()]
    
    args = {'fe_chs': feChs, 'disc': disc}
    args.update(kwargs)
    reply = self._conn.sendCommand(json.dumps({'command': command, 'args': args}))
    
    if reply['response'] == 'ok':
      ret_dict = {}
      for feid, fedata in reply['result']['counts']:
        channels = []
        chCounts = []
        for ch, chdata in fedata:
          channels.append(ch)
          chCounts.append(chdata)
        
        ret_dict[feid] = {}
        ret_dict[feid]['channels'] = np.array(channels)
        ret_dict[feid]['counts'] = np.array(chCounts)
        
      try:
        return np.array(reply['result']['durations']), ret_dict
      except (KeyError, TypeError):
        return ret_dict
    else:
      raise RuntimeError(reply['result'])
  

  def getTdcCalibrationData(self, nEvents: int, phase: int, simultaneousChannels: int):
    """ Runs the TDC calibration routine for a given test pulse phase and returns the collected data.
    The calibration is performed on all the non-masked channels listed in the configuration file.
    Parameters:
    nEvents: number of pulses generated during the calibration. 100 is a good value.
    phase: phase of the test pulse, relative to the clock. Can be 0 to 223.
    simultaneousChannels: how many channels are injected at the same time. 16 is a good value.
    """
    feChannels = self._boardConf.getFeChs(False)
    return self._getTdcQdcCalibration('tdc_calibration', feChannels, nEvents, phase, 1, simultaneousChannels)
  

  def getQdcCalibrationData(self, nEvents: int, phase: int, duration: int, simultaneousChannels: int):
    """ Runs the QDC calibration routine and returns the collected data.
    The calibration is performed on all the non-masked channels listed in the configuration file.
    Parameters:
    nEvents: number of pulses generated during the calibration. 100 is a good value.
    phase: phase of the test pulse, relative to the clock. Can be 0 to 55.
    duration: duration of the test pulse in number of 160 MHz clock cycles.
    simultaneousChannels: how many channels are injected at the same time. 16 is a good value.
    """
    feChannels = self._boardConf.getFeChs(False)
    return self._getTdcQdcCalibration('qdc_calibration', feChannels, nEvents, phase, duration, simultaneousChannels)
  

  def _getTdcQdcCalibration(self, command: str, feChannels: dict, nEvents: int, phase: int, duration: int, simultaneousChannels: int):
    """ Function that runs either the TDC or QDC calibration and returns the raw data as a 4xN numpy array of 32 bit integers.
    """
    feChs = [[k, v] for k, v in feChannels.items()]
    reply = self._conn.sendRawCommand(json.dumps({'command': command, 'args': {'fe_chs': feChs, 'n_events': nEvents, 'phase': phase, 'duration': duration, 'simultaneous_channels': simultaneousChannels}}))
    try:
      return np.reshape(np.frombuffer(reply, dtype='>i4'), (-1, 4))
    except TypeError:
      raise RuntimeError("not sure when this happens...") # TODO figure out when this happens
  

  def setDebugBoardOutput(self, sma0: str = None, sma1: str = None, header: str = None):
    """ Sets up the debug ouptputs on the DAQ board, i.e. the two SMAs on the back and the pin header on the front.
    None leaves the output unchanged. The available options are.
    SMA outputs:
    - none
    - clk40
    - clk40_des2
    - rx_clk
    - lvds_data
    - trigger
    - test_pulse
    - clk40_shift
    - clk160_shift
    - clk160_rx
    - clk160_tofpet

    Header outputs:
    - lvds_data
    - trigger
    - test_pulse
    - clk40_shift
    - clk160_shift
    - clk160_rx
    - clk160_tofpet
    """
    args = {}
    if sma0 is not None:
      args['sma_0'] = sma0
    
    if sma1 is not None:
      args['sma_1'] = sma1
    
    if header is not None:
      args['header'] = header
    
    reply = self._conn.sendCommand(json.dumps({'command': 'board_debug', 'args': args}))
    if reply['response'] != 'ok':
      raise RuntimeError(reply['result'])
  

  def setFeAnalogDebugOutput(self, feSet):
    settings = [[k, v] for k, v in feSet.items()]
    reply = self._conn.sendCommand(json.dumps({'command': 'fe_analog_debug', 'args': {'fe_set': settings}}))
    if reply['response'] != 'ok':
      raise RuntimeError(reply['result'])
  

  def setFeDigitalDebugOutput(self, feSet):
    settings = [[k, v] for k, v in feSet.items()]
    reply = self._conn.sendCommand(json.dumps({'command': 'fe_digital_debug', 'args': {'fe_set': settings}}))
    if reply['response'] != 'ok':
      raise RuntimeError(reply['result'])
  

  def setClk40Des1Phase(self, phase: int):
    """ Sets the phase of the clock  Des1 received from the TTCrx.
    Can be used to compensate for TTC fibres of different lengths.
    phase: 0 to 239, each step is ~104 ps
    """
    reply = self._conn.sendCommand(json.dumps({'command': 'set_clk40des1_phase', 'args': {'phase': phase}}))
    if reply['response'] != 'ok':
      logging.warn(f'setClk40Des1Phase: Error setting Clk40Des1 phase: {reply["result"]}')
      # raise RuntimeError(reply['result'])
  

  def getClk40Des1Phase(self):
    """ Returns the phase of the clock Des1 received from the TTCrx.
    """
    reply = self._conn.sendCommand(json.dumps({'command': 'get_clk40des1_phase', 'args': {}}))
    if reply['response'] == 'ok':
      return reply['result']['phase']
    else:
      logging.warn(f'getClk40Des1Phase: Error getting Clk40Des1 phase: {reply["result"]}')
      return 0
      # raise RuntimeError(reply['result'])


  def setClk40Des2Phase(self, phase: int):
    """ Sets the phase of the clock Des2 received from the TTCrx.
    Can be used to compansate for TTC fibres of different lengths.
    phase: 0 to 239, each step is ~104 ps
    """
    reply = self._conn.sendCommand(json.dumps({'command': 'set_clk40des2_phase', 'args': {'phase': phase}}))
    if reply['response'] != 'ok':
      logging.warn(f'setClk40Des2Phase: Error setting Clk40Des2 phase: {reply["result"]}')
      # raise RuntimeError(reply['result'])
  

  def getClk40Des2Phase(self):
    """ Returns the phase of the clock Des2 received from the TTCrx.
    """
    reply = self._conn.sendCommand(json.dumps({'command': 'get_clk40des2_phase', 'args': {}}))
    if reply['response'] == 'ok':
      return reply['result']['phase']
    else:
      logging.warn(f'getClk40Des2Phase: Error getting Clk40Des2 phase: {reply["result"]}')
      return 0
      # raise RuntimeError(reply['result'])
  
 
  def setClk40UseQpll(self, useQpll: bool):
    """ Sets the clock mux on the DAQ board.
    if useQpll is true, Clock40Des1 + QPLL are used.
    if useQpll is false, Clock40Des2 is used.
    """
    reply = self._conn.sendCommand(json.dumps({'command': 'set_clk40_use_qpll', 'args': {'use_qpll': useQpll}}))
    if reply['response'] != 'ok':
      logging.warn(f'setClk40UseQpll: Error setting CLK40 mux: {reply["result"]}')
      # raise RuntimeError(reply['result'])
  

  def getClk40UseQpll(self):
    """ Returns the clock mux status.
    """
    reply = self._conn.sendCommand(json.dumps({'command': 'get_clk40_use_qpll', 'args': {}}))
    if reply['response'] == 'ok':
      return reply['result']['use_qpll']
    else:
      logging.warn(f'getClk40UseQpll: Error getting CLK40 mux: {reply["result"]}')
      return None
      # raise RuntimeError(reply['result'])
  

  def scanClkTofpetPhase(self):
    reply = self._conn.sendCommand(json.dumps({'command': 'scan_clk_tofpet_phase', 'args': {}}))
    if reply['response'] == 'ok':
      return reply['result']['received_packets'], reply['result']['errors']
    else:
      raise RuntimeError(reply['result'])
  

  def setClkTofpetPhase(self, phase: int):
    """ Sets the phase of the clock used to read the data stream from the TOFPETs.
    It must be set according to the length of the cables connecting the TOFPET cards to the DAQ board.
    phase: 0 to 15
    """
    reply = self._conn.sendCommand(json.dumps({'command': 'set_clk_tofpet_phase', 'args': {'phase': phase}}))
    if reply['response'] != 'ok':
      raise RuntimeError(reply['result'])
  

  def setClkTofpetPhase(self):
    """ Returns the phase of the clock used to read the data stream from the TOFPETs.
    """
    reply = self._conn.sendCommand(json.dumps({'command': 'get_clk_tofpet_phase', 'args': {}}))
    if reply['response'] == 'ok':
      return reply['result']['phase']
    else:
      raise RuntimeError(reply['result'])
  

  def generateData(self, nEvents, delay):
    reply = self._conn.sendCommand(json.dumps({'command': 'generate_data', 'args': {'n_events': nEvents, 'delay': delay}}))
    if reply['response'] != 'ok':
      raise RuntimeError(reply['result'])
  

  def updateFirmware(self):
    reply = self._conn.sendCommand(json.dumps({'command': 'update_fw', 'args': {}}))
    if reply['response'] != 'ok':
      raise RuntimeError(reply['result'])


  def reboot(self):
    self._conn.sendCommand(json.dumps({'command': 'reboot', 'args': {}}), waitResponse=False)
  

  def enableBoardServerBootStart(self, enable: bool):
    reply = self._conn.sendCommand(json.dumps({'command': 'set_board_server_boot_start', 'args': {'enable': enable}}))
    if reply['response'] != 'ok':
      raise RuntimeError(reply['result'])
  
  
  def setDateTime(self, dateTime: datetime = None):
    if dateTime is None:
      dateTime =datetime.now()
    
    reply = self._conn.sendCommand(json.dumps({'command': 'set_date_time', 'args': {'date_time': dateTime.strftime('%Y-%m-%dT%H:%M:%S')}}))
    if reply['response'] != 'ok':
      raise RuntimeError(reply['result'])