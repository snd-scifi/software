import os
import os.path as op
import shutil
import h5py
import numpy as np
import matplotlib.pyplot as plt

from scifi_daq.daq_board.daq_board_base import DaqBoardBase
from .utils import saveShowClose

def acquireThresholdScanData(self: DaqBoardBase, minTime, minEvents, maxTime, showPlots=False, discriminators=['T1', 'T2', 'E']):
  peCalDir = self._boardConf.peCalDir
  startLog = self.buildCalibrationLog()
  feChs = self._boardConf.getFeChs(False)

  path = self._boardConf.peCalDir
  if op.exists(path):
    shutil.rmtree(path)
  os.makedirs(path)

  with h5py.File(self._boardConf.peCalDataFile, 'w') as f:
    for disc in discriminators:
      durations, drData = self._getCounterData('get_dark_counts_2', feChs, disc, min_duration=minTime, min_events=minEvents, max_duration=maxTime)
      dc = _getDarkCounts(self, durations, drData)

      for feId, dcr in dc.items():
        f.create_dataset(f'{disc}/tofpet_{feId}/channels', data=dcr['channels'])
        f.create_dataset(f'{disc}/tofpet_{feId}/rate', data=dcr['rate'])
        f.create_dataset(f'{disc}/tofpet_{feId}/rate_err', data=dcr['rate_err'])

        fig, ax = plt.subplots()
        ax.plot(dcr['rate'].T)
        ax.set_yscale('log')
        ax.set_title(f'TOFPET {feId} - {disc}')
        ax.set_xlabel('Threshold [DAC counts]')
        ax.set_ylabel('Rate [Hz]')
        ax.grid(alpha=0.3)
        saveShowClose(fig, op.join(peCalDir, f'plots/tofpet_{feId}_{disc.lower()}.png'), show=showPlots)

  endLog = self.buildCalibrationLog(startLog)
  self._boardConf.updateCalibrationLog('dcr_thresholds', endLog)


def _getDarkCounts(self: DaqBoardBase, durations, data):
  """ Calculates the DCR from counter data.
  Returns a dictionary of the form `{tofpet_id: {'channels': [...], 'rate': [...], 'rate_err': [...]}}`
  `rate` and `rate_err` are 2D arrays of size `(n_channels, 63)`
  """
  retDict = {}
  durations = np.array(durations)
  
  for feid, feData in data.items():
    counts = feData['counts']
    countsMa = np.ma.masked_equal(counts, 0xFFFFFFFFFFFFFFFF)
    countsErrMa = np.sqrt(countsMa)
    countsMa = countsMa/durations[None, :]
    countsErrMa = countsErrMa/durations[None, :]
    countsMa[countsMa.mask] = np.nan
    countsErrMa[countsMa.mask] = np.nan

    retDict[feid] = {}
    retDict[feid]['channels'] = feData['channels']
    retDict[feid]['rate'] = countsMa
    retDict[feid]['rate_err'] = countsErrMa
  return retDict