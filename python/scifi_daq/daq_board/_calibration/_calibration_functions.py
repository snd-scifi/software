import numpy as np
from scipy.special import erf # pylint: disable=no-name-in-module
from math import sqrt


def thr_fit_f(x, A, mu, sigma):
  """ Fit function for the threshold calibration (an erf)
  """
  return A/2 * (erf((x-mu)/sigma/sqrt(2)) + 1)

def qdc_cal_f(x, a, b, c, d, e):
  return - c * np.log(1 + np.exp(a*(x-e)**2 - b*(x-e))) + d

def tdc_cal_f(x, a, b, c, d):
  return a*((x-d)%1)**2 + b*((x-d)%1) + c

def calculateTimestampFine(tf, a, b, c, d):
  """ Returns the fine timestamp in units of the 160 MHz clock
  """
  return (-b - np.sqrt(b**2 - 4*a*(c-tf)))/(2*a) + d

def calculateCharge(qc, qf, tf, a, b, c, d, x0):
  x = qc - tf
  return qf - qdc_cal_f(x, a, b, c, d, x0)

def integration_clock_cycles(n: int):
  if 0 <= n and n < 16:
    return n
  elif 16 <= n and n < 32:
    return 2 * n - 16
  elif 32 <= n and n < 128:
    return 4 * n - 78
  else:
    raise ValueError('n must be between 0 and 127')

def integration_setting(n):
  return np.piecewise(
    n,
    [(0 <= n) & (n < 16), (16 <= n) & (n < 46), (46 <= n) & (n <= 430), 430 < n],
    [lambda x : x, lambda x : (x + 16) / 2, lambda x : (x + 78) / 4, lambda x : np.nan])

def getChi2(data_m, data_f, stddev, fitpar):
  return np.sum(((data_m-data_f)/stddev)**2), data_m.size - fitpar