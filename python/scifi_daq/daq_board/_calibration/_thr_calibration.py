import os
import os.path as op
import shutil
import h5py
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

from scifi_daq.daq_board.daq_board_base import DaqBoardBase
from ._calibration_functions import thr_fit_f, getChi2
from .utils import saveShowClose

def acquireThrCalibrationData(self: DaqBoardBase, showPlots=False):
  startLog = self.buildCalibrationLog()
  feChs = self._boardConf.getFeChs(True)
  
  # delete the previous calibration data, if any, and create the file to store it
  thrCalDir = self._boardConf.thrCalDir
  if op.exists(thrCalDir):
    shutil.rmtree(thrCalDir)
  os.makedirs(thrCalDir)
  f = h5py.File(self._boardConf.thrCalDataFile, 'w')

  # clear the plots folder
  if op.exists(op.join(thrCalDir, 'plots')):
    shutil.rmtree(op.join(thrCalDir, 'plots'))
  
  # get data for the three discriminators and write it to the HDF5 file
  for disc in ['T1', 'T2', 'E']:
    calData = self._getCounterData('disc_thr_calibration', feChs, disc)
  
    for tpId, tpData in calData.items():
      f.create_dataset(f'{disc}/tofpet_{tpId}/channels', data=tpData['channels'])
      f.create_dataset(f'{disc}/tofpet_{tpId}/counts', data=tpData['counts'])

      # plot the counts per channel
      fig, ax = plt.subplots()
      ax.plot(tpData['counts'].T)
      ax.set_title(f'Discriminator {disc} - TOFPET {tpId}')
      ax.set_xlabel('Threshold [DAC counts]')
      ax.set_ylabel('Counts')
      ax.grid(alpha=0.3)
      saveShowClose(fig, op.join(thrCalDir, f'plots/counts_{disc}_tofpet_{tpId}.png'), show=showPlots)

  # write the log of the calibration
  endLog = self.buildCalibrationLog(startLog)
  self._boardConf.updateCalibrationLog('thr_cal', endLog)


def calculateThrCalibration(self: DaqBoardBase, showPlots=False, saveFitPlots=False):
  thrCalDir = self._boardConf.thrCalDir
  
  feChs = self._boardConf.getFeChs(True)

  # prepare the dictionary containing the baselines and the dataframe with the fit parameters
  baselinesDict = {}
  df_params = pd.DataFrame()
  for fe, chs in feChs.items():
    baselinesDict[fe] = {}
    for ch in chs:
      baselinesDict[fe][ch] = {}
  f = h5py.File(self._boardConf.thrCalDataFile, 'r')

  for disc in ['T1', 'T2', 'E']:
    # prepare the data in the correct dictionary form to be used by _getThrBaseline
    data = f[disc]
    calDict = {}
    for feid in data:
      # feid is 'tofpet_X'
      channels = data[f'{feid}']['channels']
      counts = data[f'{feid}']['counts']
      calDict.update({int(feid[-1]): {'channels': np.array(channels), 'counts': np.array(counts)}})

    # calculate the baselines
    df = _getThrBaseline(self, calDict, showPlots=showPlots, disc=disc, plotFits=saveFitPlots)
    df_params = pd.concat([df_params, df])
    
    for _, el in df.iterrows():
      baselinesDict[el['tofpet']][el['channel']][f'thr_{disc.lower()}_bl'] = int(round(el['mu']))
  
  # write fit parameters to file
  df_params.to_csv(op.join(thrCalDir, 'fit_parameters.csv'), index = False)
  
  # update configuration
  self._boardConf.setThrBaselineCalibration(baselinesDict)


def applyThrCalibration(self: DaqBoardBase):
  self.updateFeChannelsConf(self._boardConf.thrBlConf)


def _getThrBaseline(self: DaqBoardBase, data, disc, showPlots=False, plotFits=False):
  """ Calculates the thresholds baselines.
  data must be a dictionary of the form {tofpet_id: tofpet_data, ...}, where tofpet_data is a dictionary of the form
  {'channels': [...], 'counts': counts}.
  counts is a 2D array of shape (n_channels, n_bl_steps), so (usually) (64, 63) for T and (64, 7) for E. n_channels can be smaller than 64 and anyway must be the same as the length of the 'channels' array.

  This method returns a pandas dataframe with the S-curve fit results for each channel
  """
  thrCalDir = self._boardConf.thrCalDir
  # df = pd.DataFrame(columns=['tofpet', 'channel', 'disc', 'amplitude', 'mu', 'sigma', 'amplitude_err', 'mu_err', 'sigma_err', 'chi2', 'dof'] )
  df = pd.DataFrame(columns=['tofpet', 'channel', 'disc', 'amplitude', 'mu', 'sigma', 'amplitude_err', 'mu_err', 'sigma_err'] )
  # arrays used to fill the dataframe
  tp, channel, a, mu, sigma, aErr, muErr, sigmaErr, chi2, dof = ([] for _ in range(10))
  
  for feid, feData in data.items():
    channels = feData['channels']
    counts = feData['counts']

    # arrays used for plotting
    channelPlt, aPlt, muPlt, sigmaPlt, aErrPlt, muErrPlt, sigmaErrPlt = ([] for _ in range(7))
    
    # loop over each channel
    for cnt, ch in zip(counts, channels):
      thr = np.arange(63) # thresholds are always 0 to 62
      thr0 = np.argmax(cnt > 4194304/2)
      param_bounds=([4194300, 0, 0], [4194308, 63, 10]) # bound on amplitude is quite tight, as the amplitude will basically be fixed to 2^22
      popt, pcov = curve_fit(thr_fit_f, thr, cnt, [4194304., thr0, 1], bounds=param_bounds)
      perr = np.sqrt(np.diag(pcov))
      
      # append the results to the relevant arrays
      channel.append(ch)
      channelPlt.append(ch)
      tp.append(feid)
      a.append(popt[0])
      aPlt.append(popt[0])
      mu.append(popt[1])
      muPlt.append(popt[1])
      sigma.append(popt[2])
      sigmaPlt.append(popt[2])
      aErr.append(perr[0])
      aErrPlt.append(perr[0])
      muErr.append(perr[1])
      muErrPlt.append(perr[1])
      sigmaErr.append(perr[2])
      sigmaErrPlt.append(perr[2])
      chi2l, dofl = getChi2(cnt, thr_fit_f(thr, *popt), 1, 3)
      chi2.append(chi2l)
      dof.append(dofl)
      
      # plot the fits if requested
      if plotFits:
        fig, ax = plt.subplots()
        ax.set_title(f'Discriminator {disc} - TOFPET {feid} - Channel {ch}')
        ax.plot(thr, cnt, ls='', marker='.')
        ax.plot(thr, thr_fit_f(thr, *popt), c='r', lw='1')
        ax.set_xlabel('Threshold [DAC counts]')
        ax.set_ylabel('Counts')
        ax.grid(alpha=0.3)
        saveShowClose(fig, op.join(thrCalDir, f'plots/fits/{disc}_tofpet_{feid}/channel_{ch}.png'), show=False)
    
    # plot the mu and sigma results
    fig, ax = plt.subplots(2)
    ax[0].set_title(f'Discriminator {disc} - TOFPET {feid}')
    ax[0].set_ylabel('Threhsold')
    ax[0].grid(alpha = 0.3)
    ax[0].errorbar(channelPlt, muPlt, yerr=muErrPlt, marker='.')
    ax[1].set_xlabel('Channel')
    ax[1].set_ylabel('Threshold sigma')
    ax[1].grid(alpha = 0.3)
    ax[1].errorbar(channelPlt, sigmaPlt, yerr=sigmaErrPlt, marker='.', c='C1')
    saveShowClose(fig, op.join(thrCalDir, f'plots/fit_params_{disc}_tofpet_{feid}.png'), show=showPlots)
  
  df['tofpet'] = tp
  df['channel'] = channel
  df['disc'] = disc.lower()
  df['amplitude'] =  a
  df['mu'] = mu
  df['sigma'] = sigma
  df['amplitude_err'] = aErr
  df['mu_err'] = muErr
  df['sigma_err'] = sigmaErr
  # the chi2 estimation doesn't make much sense without an uncertainty
  # df['chi2'] = chi2
  # df['dof'] = dof
  
  return df