import os
import os.path as op
import shutil
import logging
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

from scifi_daq.daq_board.daq_board_base import DaqBoardBase
from scifi_daq.storage.data_packet import processRawData
from ._calibration_functions import tdc_cal_f, getChi2
from .utils import saveShowClose

def acquireTdcCalibrationData(self: DaqBoardBase):
  startLog = self.buildCalibrationLog()
  
  # delete the previous calibration data, if any, and create the folder
  tdcCalDir = self._boardConf.tdcCalDir
  if op.exists(tdcCalDir):
    shutil.rmtree(tdcCalDir)
  os.makedirs(tdcCalDir)

  df = None
  # loops over the available phases and acquires 100 events at each phase
  for ph in range(0, 224, 1):
    # gets raw data
    data = self.getTdcCalibrationData(100, ph, 16)
    # raw data is processed into a dataframe
    df_last = processRawData(data)
    # the phase column is added
    df_last.insert(loc=0, column='phase', value=ph)
    # append the dataframe to the existing data
    if df is None:
      df = df_last
    else:
      df = pd.concat([df, df_last], ignore_index=True)
  
  # write the data to disk
  df.to_hdf(self._boardConf.tdcCalDataFile, 'data', mode='w')
  
  endLog = self.buildCalibrationLog(startLog)
  self._boardConf.updateCalibrationLog('tdc_cal', endLog)


def calculateTdcCalibration(self: DaqBoardBase, showPlots=False, redChi2Limit=40.):
  tdcCalDir = self._boardConf.tdcCalDir
  # clear the plots folder
  if op.exists(op.join(tdcCalDir, 'plots')):
    shutil.rmtree(op.join(tdcCalDir, 'plots'))

  df = pd.read_hdf(self._boardConf.tdcCalDataFile, 'data')

  d_cal = []
  columns_cal = ('tofpet_id', 'channel', 'tac', 'tdc', 'a', 'b', 'c', 'd', 'chi2', 'dof')

  # grups data by tofpet_id, channel and tac, then loop over these groups
  groups = df.groupby(['tofpet_id', 'tofpet_channel', 'tac'], sort=True)
  fit_failed_t = 0
  fit_failed_v = 0
  
  for (tpId, ch, tac), data in groups:
    # groups by phase and calculate mean, std deviation and number of entries in each group
    gp = data.groupby('phase')
    df_mean = gp.mean()
    df_std = gp.std()
    df_count = gp.count()

    # prepares data for the fit: x is scaled from 0 to 4, where 4 corresponds to 25 ns.
    # it represents the phase in a 40 MHz clock
    x = np.linspace(0., 4., 224, endpoint=False)
    # x1 is scaled from 0 to 1 and represents the phase in the 160 MHz clock domain
    x1 = x%1

    # the y values (tfine and vfine) with their uncertainties are calculated
    y_t = df_mean.t_fine
    y_t_err = df_std.t_fine / np.sqrt(df_count.t_fine)
    y_v = df_mean.v_fine
    y_v_err = df_std.v_fine / np.sqrt(df_count.v_fine)

    # perform the fits for T and E and store the data in the array
    popt_t, pcov_t, chi2_t, dof_t = _fitTdcCal(self, x, y_t, y_t_err)
    d_cal.append((tpId, ch, tac, 0, popt_t[0], popt_t[1], popt_t[2], popt_t[3], chi2_t, dof_t))

    popt_v, pcov_v, chi2_v, dof_v = _fitTdcCal(self, x, y_v, y_v_err)
    d_cal.append((tpId, ch, tac, 1, popt_v[0], popt_v[1], popt_v[2], popt_v[3], chi2_v, dof_v))
    
    # if the reduced chi2 is above a threshold, plot the fir and the data
    if chi2_t/dof_t > redChi2Limit:
      logging.warn(f'Board {self.boardId} ({self.name}) - TDC fit failed for {tpId}, {ch}, {tac}, T (reduced chi2: {chi2_t/dof_t:.2f})')
      fig, ax = plt.subplots()
      ax.set_title(f'TOFPET {tpId}, Channel {ch}, TAC {tac}, TDC T (reduced chi2: {chi2_t/dof_t:.2f})')
      ax.errorbar(x, y_t, yerr=y_t_err, ls='', marker='.')
      ax.plot(x, tdc_cal_f(x, *popt_t))
      ax.grid(alpha=0.3)
      ax.set_xlabel('Phase [clock cycles]')
      ax.set_ylabel('t_fine')
      saveShowClose(fig, op.join(tdcCalDir, f'plots/failed_fits/fit_{tpId}_{ch}_{tac}_t.png'), show=showPlots)

      fit_failed_t += 1

    if chi2_v/dof_v > redChi2Limit:
      logging.warn(f'Board {self.boardId} ({self.name}) - TDC fit failed for {tpId}, {ch}, {tac}, E (reduced chi2: {chi2_v/dof_v:.2f})')
      fig, ax = plt.subplots()
      ax.set_title(f'TOFPET {tpId}, Channel {ch}, TAC {tac}, TDC E (reduced chi2: {chi2_v/dof_v:.2f})')
      ax.errorbar(x, y_v, yerr=y_v_err, ls='', marker='.')
      ax.plot(x, tdc_cal_f(x, *popt_v))
      ax.grid(alpha=0.3)
      ax.set_xlabel('Phase [clock cycles]')
      ax.set_ylabel('v_fine')
      saveShowClose(fig, op.join(tdcCalDir, f'plots/failed_fits/fit_{tpId}_{ch}_{tac}_e.png'), show=showPlots)

      fit_failed_v += 1
    
  if fit_failed_t > 0:
    logging.warn(f'Board {self.boardId} ({self.name}) - Fit failed t: {fit_failed_t}')
  if fit_failed_v > 0:
    logging.warn(f'Board {self.boardId} ({self.name}) - Fit failed v: {fit_failed_v}')

  # create a dataframe and use it to set the TDC calibration of the DAQ board object and write it on disk
  df_cal = pd.DataFrame(d_cal, columns=columns_cal)
  self._boardConf.setTdcCalibration(df_cal)
  
  # plot the histograms of the parameters and the reduced chi2
  calGroups = df_cal.groupby(['tofpet_id', 'tdc'], sort=True)
  for (tpId, tdc), data in calGroups:
    fig, ax = plt.subplots(3, 2, figsize=(10, 8))
    ax[0][0].hist(data.a, bins=20)
    ax[1][0].hist(data.b, bins=20)
    ax[0][1].hist(data.c, bins=20)
    ax[1][1].hist(data.d, bins=20)
    ax[2][0].hist(data.chi2/data.dof, bins=20)

    ax[0][0].set_xlabel('a')
    ax[0][0].grid(alpha=0.3)
    ax[0][1].set_xlabel('b')
    ax[0][1].grid(alpha=0.3)
    ax[1][0].set_xlabel('c')
    ax[1][0].grid(alpha=0.3)
    ax[1][1].set_xlabel('d')
    ax[1][1].grid(alpha=0.3)
    ax[2][0].set_xlabel(r'$\tilde{\chi}^2$')
    ax[2][0].grid(alpha=0.3)
    ax[2][1].axis('off')
    
    textTdc = 'T' if tdc == 0 else 'E'
    fig.suptitle(f'TOFPET {tpId}, TDC {textTdc}')
    fig.tight_layout()

    saveShowClose(fig, op.join(tdcCalDir, f'plots/hists_tofpet_{tpId}_{textTdc.lower()}.png'), show=showPlots)


def _fitTdcCal(self: DaqBoardBase, x, y, y_err):
    # x1 is scaled from 0 to 1 and represents the phase in the 160 MHz clock domain
    x1 = x%1.
    # remove NaNs and Infs
    x_m = np.isfinite(x)
    y_m = np.isfinite(y)
    y_err_m = np.isfinite(y_err)
    mask = x_m & y_m & y_err_m
    x = x[mask]
    x1 = x1[mask]
    y = y[mask]
    y_err = y_err[mask]

    # if uncertainty is 0, the fit complains, so set it at least to 0.1
    y_err = np.where(y_err < 0.1, 0.1, y_err)

    init_a = -0.01 # this is normally quite small
    init_b = (np.min(y) - np.max(y)) # this is slope of the curve (delta x is 1)
    init_c = np.max(y) # this the value at x=0
    init_ph = x1[np.argmax(np.diff(y))] # the phase is where the difference between the y value is the highest

    bounds = (-np.inf, np.inf)
    bounds = [[-25, -np.inf, -np.inf, init_ph-0.03], [-0.01, np.inf, np.inf, init_ph+0.03]]

    try:
      popt, pcov = curve_fit(tdc_cal_f, x1, y, p0=[init_a, init_b, init_c, init_ph], sigma=y_err, absolute_sigma=True, bounds=bounds)
      y_fit = tdc_cal_f(x, *popt)
      chi2, dof = getChi2(y, y_fit, y_err, 4)
      popt[3] = popt[3] - 0.0
    except Exception:
      # if the fit fails, just return some default values with a very high chi2
      popt = [-0.1, -160, 370, 0.5]
      pcov = None
      chi2 = 1000000
      dof = 1

    return popt, pcov, chi2, dof