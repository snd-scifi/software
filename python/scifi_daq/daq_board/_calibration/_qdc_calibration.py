import os
import os.path as op
import shutil
import logging
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit, bisect
from math import ceil

from scifi_daq.daq_board.daq_board_base import DaqBoardBase
from scifi_daq.storage.data_packet import processRawData
from ._calibration_functions import calculateTimestampFine, qdc_cal_f, integration_setting, getChi2
from .utils import saveShowClose

def acquireQdcCalibrationData(self: DaqBoardBase):
  startLog = self.buildCalibrationLog()

  # delete the previous calibration data, if any, and create the folder
  qdcCalDir = self._boardConf.qdcCalDir
  if op.exists(qdcCalDir):
    shutil.rmtree(qdcCalDir)
  os.makedirs(qdcCalDir)

  df = None

  # loops over different pulse durations
  # TODO allow different range, maybe depending on i_mirror_bias_top or saturation
  for duration in range(1, 201, 1):
    # loops over different pahses (in this case, one per duration)
    for ph in [(duration%3)*19]:
      # gets raw data
      data = self.getQdcCalibrationData(100, ph, duration, 16)
      # raw data is processed into a dataframe
      df_last = processRawData(data)
      # the phase column is added
      if df is None:
        df = df_last
      else:
        df = pd.concat([df, df_last], ignore_index=True)
  
  # write the data to disk
  df.to_hdf(self._boardConf.qdcCalDataFile, 'data', mode='w')
  
  endLog = self.buildCalibrationLog(startLog)
  self._boardConf.updateCalibrationLog('qdc_cal', endLog)


def calculateQdcCalibration(self: DaqBoardBase, showPlots=False, redChi2Limit=5.):
  qdcCalDir = self._boardConf.qdcCalDir
  # clear the plots folder
  if op.exists(op.join(qdcCalDir, 'plots')):
    shutil.rmtree(op.join(qdcCalDir, 'plots'))
  # read the TDC calibration
  tdcCal = pd.read_csv(self._boardConf.tdcCalPath)

  df = pd.read_hdf(self._boardConf.qdcCalDataFile, 'data')

  # prepare the TDC calibration array, needed to perform the QDC calibration
  tdcCalArr = np.empty((8, 64, 4, 2, 6)) # tofpet_id, tofpet_channel, tac, tdc, params
  tdcCalArr[:] = np.nan
  # fill the array from the dataframe
  for c in tdcCal.itertuples(index=False, name=None):
    i = c[0:4]
    tdcCalArr[i] = c[4:10]

  # calculate the fine timestamp
  df['ts_fine'] = calculateTimestampFine(df.t_fine, *(tdcCalArr[df.tofpet_id, df.tofpet_channel, df.tac][:, 0][:, 0:4].T))

  d_cal = []
  d_range = []
  columns_cal = ('tofpet_id', 'channel', 'tac', 'a', 'b', 'c', 'd', 'e', 'chi2', 'dof')
  columns_range = ('tofpet_id', 'channel', 'qdc_min_intg_t','qdc_max_intg_t')

  cut = (df.v_fine > 0) & (df.v_coarse < 200)
  groups = df[cut].groupby(['tofpet_id', 'tofpet_channel', 'tac'], sort=True)

  fit_failed = 0
  fig, ax = plt.subplots()
  for (tpId, ch, tac), data in groups:
    try:
      x = (data.v_coarse - data.ts_fine).to_numpy()
      y = data.v_fine.to_numpy()

      popt, pcov = curve_fit(qdc_cal_f, x, y, p0 = [0.0001, 0.1, 40, 315, 100])
      y_fit = qdc_cal_f(x, *popt)
      chi2, dof = getChi2(y, y_fit, 0.7, 5) # 0.7 = noise from datasheet
    except Exception:
      logging.warn(f'QDC fit failed for {tpId}, {ch}, {tac}')
      popt = [0.0001, 0.1, 25, 370, 120]
      chi2 = 1000000
      dof = 1
    d_cal.append((tpId, ch, tac, *popt, chi2, dof))

    if chi2/dof > redChi2Limit:
      logging.warn(f'Board {self.boardId} ({self.name}) - QDC fit failed for {tpId}, {ch}, {tac}, (reduced chi2: {chi2/dof:.2f})')
      fig, ax = plt.subplots()
      ax.set_title(f'TOFPET {tpId}, Channel {ch}, TAC {tac}, TDC T (reduced chi2: {chi2/dof:.2f})')
      ax.scatter(x, y, s=1)
      xx = np.arange(0, 200, 1)
      ax.plot(xx, qdc_cal_f(xx, *popt), lw=1, c='r')
      ax.grid(alpha=0.3)
      ax.set_xlabel('Integration time [clock cycles]')
      ax.set_ylabel('v_fine')
      saveShowClose(fig, op.join(qdcCalDir, f'plots/failed_fits/fit_{tpId}_{ch}_{tac}.png'), show=showPlots)

      fit_failed += 1

    # to determine the integration range, we determine the x corresponding to 10% of the saturation value for each channel
    try:
      qdc_cal_f3 = lambda x, a, b, c, d, e : qdc_cal_f(x, a, b, c, d, e) - popt[3]*0.1
      intg_min = bisect(qdc_cal_f3, 0, 200, args=tuple(popt))
      intg_max = intg_min
    except Exception as e:
      # if everything fails, we set a generous integration range
      logging.warn(f'Board {self.boardId} ({self.name}) - QDC range determination failed for {tpId}, {ch}, {tac}: {e}')
      intg_min = 50
      intg_max = 50
    
    # the setting itself is not directly the number of clock cycles, needs to be calculated
    intg_min_setting = ceil(integration_setting(intg_min))
    intg_max_setting = ceil(integration_setting(intg_max))

    if tac == 0:
      # if it's the first we calculate, let's append it
      d_range.append([tpId, ch, intg_min_setting, intg_max_setting])
    else:
      # otherwise, use the max among the minimum integration windows (and viceversa) for the 4 TACs
      # (each TAC will give a different value, but the channel has a single setting)
      d_range[-1][2] = max(d_range[-1][2], intg_min_setting)
      d_range[-1][3] = min(d_range[-1][3], intg_max_setting)
    
    # if the integration settings are different, set the max = to the min
    if d_range[-1][2] != d_range[-1][3]:
      d_range[-1][3] = d_range[-1][2]

  # create the dataframs from the arrays and write the data to disk
  df_cal = pd.DataFrame(d_cal, columns=columns_cal)
  df_range = pd.DataFrame(d_range, columns=columns_range)
  self._boardConf.setQdcCalibration(df_cal, df_range)

  # plot the histograms of the parameters and the reduced chi2
  calGroups = df_cal.groupby(['tofpet_id'], sort=True)
  rangeGroups = df_range.groupby(['tofpet_id'], sort=True)
  for (tpId, calData), (tpId, rangeData) in zip(calGroups, rangeGroups):
    fig, ax = plt.subplots(4, 2, figsize=(12, 12))
    ax[0][0].hist(calData.a, bins=20)
    ax[1][0].hist(calData.b, bins=20)
    ax[0][1].hist(calData.c, bins=20)
    ax[1][1].hist(calData.d, bins=20)
    ax[2][0].hist(calData.e, bins=20)
    ax[2][1].hist(calData.chi2/calData.dof, bins=20)
    ax[3][0].hist(rangeData.qdc_min_intg_t, bins=20)
    ax[3][1].hist(rangeData.qdc_max_intg_t, bins=20)

    ax[0][0].set_xlabel('a')
    ax[0][0].grid(alpha=0.3)
    ax[0][1].set_xlabel('b')
    ax[0][1].grid(alpha=0.3)
    ax[1][0].set_xlabel('c')
    ax[1][0].grid(alpha=0.3)
    ax[1][1].set_xlabel('d')
    ax[1][1].grid(alpha=0.3)
    ax[2][0].set_xlabel('e')
    ax[2][0].grid(alpha=0.3)
    ax[2][1].set_xlabel(r'$\tilde{\chi}^2$')
    ax[2][1].grid(alpha=0.3)
    ax[3][0].set_xlabel('qdc_min_intg_t')
    ax[3][0].grid(alpha=0.3)
    ax[3][1].set_xlabel('qdc_max_intg_t')
    ax[3][1].grid(alpha=0.3)
    
    fig.suptitle(f'TOFPET {tpId}')
    fig.tight_layout()

    saveShowClose(fig, op.join(qdcCalDir, f'plots/hists_tofpet_{tpId}.png'), show=showPlots)
