import os.path as op
import h5py
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import find_peaks

from scifi_daq.daq_board.daq_board_base import DaqBoardBase
from .utils import saveShowClose


def calculateThresholds(self: DaqBoardBase, methodT1='fixed', methodT2='rate', methodE='rate', settingsT1=None, settingsT2=None, settingsE=None):
  feChs = self._boardConf.getFeChs(False)
  newThrs = {}
  for tpId, chs in feChs.items():
    newThrs[tpId] = {}
    for ch in chs:
      newThrs[tpId][ch] = {}
  
  METHODS_MAP = {
    'fixed': _determineThresholdsFixed,
    'rate': _determineThresholdsRate,
    'pe': _determineThresholdsPe,
    'snd_us_fixed': _determineThresholdsSndUsFixed,
  }

  with h5py.File(self._boardConf.peCalDataFile, 'r') as f:
    for disc, method, settings in zip(['T1', 'T2', 'E'], [methodT1, methodT2, methodE], [settingsT1, settingsT2, settingsE]):
      METHODS_MAP[method](self, f, newThrs, disc, **settings)
  
  self._boardConf.setThresholds(newThrs)
  try:
    self.updateFeChannelsConf(newThrs)
  except:
    pass


def _determineThresholdsFixed(self: DaqBoardBase, f, thrDict, disc, threshold):
  for _, chs in thrDict.items():
    for _, thrs in chs.items():
      thrs[f'thr_{disc.lower()}'] = threshold


def _determineThresholdsRate(self: DaqBoardBase, f, thrDict, disc, rateMax):
  for tpId in thrDict.keys():
    rate = np.array(f[disc][f'tofpet_{tpId}']['rate'])
    channels = np.array(f[disc][f'tofpet_{tpId}']['channels'])
    thr = 63 - np.argmax(rate[:, ::-1] > rateMax, axis=1)
    for ch, t in zip(channels, thr):
      thrDict[tpId][ch][f'thr_{disc.lower()}'] = t


def _determineThresholdsPe(self: DaqBoardBase, f, thrDict, disc, skipSamples=2):
  peCalDir = self._boardConf.peCalDir
  for tpId in thrDict.keys():
    channels = np.array(f[disc][f'tofpet_{tpId}']['channels'])
    rate = np.ma.masked_invalid(np.array(f[disc][f'tofpet_{tpId}']['rate']))[:, skipSamples:]
    rateErr = np.ma.masked_invalid(np.array(f[disc][f'tofpet_{tpId}']['rate_err']))[:, skipSamples:]/rate
    rateLog = np.log10(rate)
    mask = rateErr > 0.2
    rateLog = np.ma.masked_where(mask, rateLog)
    rateLogDiff = -np.diff(rateLog, axis=1)
    x = np.arange(skipSamples, 62, 1)
    peaks, asd = find_peaks(rateLogDiff[1, :], prominence=0.001, height=0.1)
    print(np.isfinite(rateLogDiff[1, peaks]))
    peaks = peaks[np.isfinite(rateLogDiff[1, peaks])]
    print(peaks)
    print(asd)

    fig, ax = plt.subplots()
    ax.plot(x[:, None], rateLogDiff[1:2, :].T)
    ax.vlines(x[peaks], 0, 1)
    ax.set_title(f'{tpId} {disc}')    
    saveShowClose(fig, op.join(self._boardConf.peCalDir, f'plots/tofpet_{tpId}'), show=False)


def _determineThresholdsSndUsFixed(self: DaqBoardBase, f, thrDict, disc, thresholdL, thresholdS):
  def isSmallSipm(tofpetId, channel):
    SMALL_SIPMS = ((0,58), (0,52), (0,42), (0,36), (0,26), (1,60), (1,50), (1,44), (1,34), (1,28),
                (0,59), (0,53), (0,43), (0,37), (0,27), (1,61), (1,51), (1,45), (1,35), (1,29))
    return (tofpetId%2, channel) in SMALL_SIPMS
  
  for tpId, chs in thrDict.items():
    for ch, thrs in chs.items():
      thrs[f'thr_{disc.lower()}'] = thresholdS if isSmallSipm(tpId, ch) else thresholdL
