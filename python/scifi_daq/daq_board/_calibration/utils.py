import os
import os.path as op
from matplotlib.figure import Figure
import matplotlib.pyplot as plt

def saveShowClose(fig: Figure, filepath: str, show=False):
  """ Saves the figure, shows it, if requested, and closes it.
  If the folder of the figure doesn't exist, it tries to create it.
  """
  if not op.exists(op.dirname(filepath)):
    os.makedirs(op.dirname(filepath))
  fig.savefig(filepath)
  if show:
    plt.show()
  plt.close(fig)