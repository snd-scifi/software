import os
import os.path as op
import shutil
import h5py
import numpy as np
import matplotlib.pyplot as plt

from scifi_daq.daq_board.daq_board_base import DaqBoardBase
from .utils import saveShowClose

def acquireTiaCalibrationData(self: DaqBoardBase, showPlots=False):
  startLog = self.buildCalibrationLog()
  feChs = self._boardConf.getFeChs(True)
  
  # delete the previous calibration data folder, if any, and create the file to store it
  tiaCalDir = self._boardConf.tiaCalDir
  if op.exists(tiaCalDir):
      shutil.rmtree(tiaCalDir)
  os.makedirs(tiaCalDir)
  f = h5py.File(self._boardConf.tiaCalDataFile, 'w')

  # clear the plots folder
  if op.exists(op.join(self._boardConf.tiaCalDir, 'plots')):
    shutil.rmtree(op.join(self._boardConf.tiaCalDir, 'plots'))
    
  # get data for the three discriminators and write it to the HDF5 file
  for disc in ['T1', 'T2', 'E']:
    calData = self._getCounterData('tia_baseline_calibration', feChs, disc)
    
    for tpId, tpData in calData.items():
      f.create_dataset(f'{disc}/tofpet_{tpId}/channels', data=tpData['channels'])
      f.create_dataset(f'{disc}/tofpet_{tpId}/counts', data=tpData['counts'])

      # plots the counts
      fig, ax = plt.subplots()
      ax.plot(tpData['counts'].T)
      ax.set_title(f'TOFPET {tpId} - Discriminator {disc}')
      ax.set_xlabel('Baseline [DAC counts]')
      ax.set_ylabel('Counts')
      ax.grid(alpha = 0.3)
      saveShowClose(fig, op.join(self._boardConf.tiaCalDir, 'plots', f'counts_{disc}_tofpet_{tpId}.png'), show=showPlots)
    
  # write the log of the calibration
  endLog = self.buildCalibrationLog(startLog)
  self._boardConf.updateCalibrationLog('tia_cal', endLog)


def calculateTiaCalibration(self: DaqBoardBase, showPlots=False):
  feChs = self._boardConf.getFeChs(True)
  # prepare the dictionary containing the baselines
  baselinesDict = {}
  for fe, chs in feChs.items():
    baselinesDict[fe] = {}
    for ch in chs:
      baselinesDict[fe][ch] = {}

  f = h5py.File(self._boardConf.tiaCalDataFile, 'r')
  # get data for the three discriminators
  for disc in ['T1', 'T2', 'E']:
    # prepare the data in the correct dictionary form to be used by _getTiaBaseline
    data = f[disc]
    calDict = {}
    for feid in data:
      channels = data[f'{feid}']['channels']
      counts = data[f'{feid}']['counts']
      calDict.update({int(feid[-1]): {'channels': np.array(channels), 'counts': np.array(counts)}})
    
    # calculate the baselines
    baselines = _getTiaBaseline(self, calDict, showPlots=showPlots, disc=disc)
    
    # baseline estimation for T1 and T2 is combined choosing the smallest baseline value between the two
    if disc == 'T1':
      baselinesT1 = baselines
      continue
    elif disc == 'T2':
      for feid, bl1, bl in zip(baselines.keys(), baselinesT1.values(), baselines.values()):
        # first plot the baselines for T1 and T2
        fig, ax = plt.subplots()
        ax.plot(bl1['channels'], bl1['baselines'] , label='T1', marker='.', ls='')
        ax.plot(bl['channels'], bl['baselines'] , label='T2', marker='.', ls='')

        # the calculate the final baselines, overwriting bl
        bl['baselines'] = np.where(bl['baselines'] < bl1['baselines'], bl['baselines'], bl1['baselines'])
        
        # plot the final values
        ax.plot(bl['channels'], bl['baselines'], label='Combined', marker='o', ls='')
        ax.set_title(f'TOFPET {feid} - Baseline t')
        ax.set_xlabel('Channel')
        ax.set_ylabel('Baseline [DAC counts]')
        ax.grid(alpha=0.3)
        ax.legend()

        # save the plots and show them if needed
        saveShowClose(fig, op.join(self._boardConf.tiaCalDir, f'plots/bl_t_tofpet_{feid}.png'), show=showPlots)

    for feid, bls in baselines.items():
      # add the data to the baselinesDict, used to set the baselines in the configuration
      for ch, bl in zip(bls['channels'], bls['baselines']):
        baselinesDict[feid][ch]['bl_'+('e' if disc == 'E' else 't')] = int(bl)
      
      # plot the E discriminator
      if disc == 'E':
        fig, ax = plt.subplots()
        ax.plot(bls['channels'], bls['baselines'], marker='o', ls='')
        ax.set_title(f'TOFPET {feid} - Baseline e')
        ax.set_xlabel('Channel')
        ax.set_ylabel('Baseline [DAC counts]')
        ax.grid(alpha=0.3)

        saveShowClose(fig, op.join(self._boardConf.tiaCalDir, f'plots/bl_e_tofpet_{feid}.png'), show=showPlots)

  self._boardConf.setTiaBaselineCalibration(baselinesDict)


def applyTiaCalibration(self: DaqBoardBase):
  self.updateFeChannelsConf(self._boardConf.tiaBlConf)


def _getTiaBaseline(self: DaqBoardBase, data, thr=4194303*0.95, showPlots=False, disc = ''):
  """ Calculates the TIA baselines.
  It gets the counter data values and estimates the baselines by choosing the first value below thr.
  data must be a dictionary of the form {tofpet_id: tofpet_data, ...}, where tofpet_data is a dictionary of the form
  {'channels': [...], 'counts': counts}.
  counts is a 2D array of shape (n_channels, n_bl_steps), so (usually) (64, 63) for T and (64, 7) for E. n_channels can be smaller than 64 and anyway must be the same as the length of the 'channels' array.

  This method returns a dictionary of the form {tofpet_id: {'channels': [...], 'baselines': [...]}}
  """
  self._boardConf.tiaCalDir = self._boardConf.tiaCalDir
  
  retDict = {}
  for feid, feData in data.items():
    channels = feData['channels']
    counts = feData['counts']
    # a value of 0xFFFFFFFF indicates that the data is invalid (see C++ code)
    counts = np.ma.masked_equal(counts, 0xFFFFFFFF)

    # aa is a 2D vector with true where counts[i,j] > thr
    aa = counts>thr
    # [:, ::-1] reverses the order of the array relative to each channel (the maximum is at low threshold)
    # argmax selects the first element above thr in each channel and returns its index
    # this value is subtracted from a.shape[1]-1 to retrieve the correct baseline value (since we reversed the arrays)
    retDict[feid] = {}
    retDict[feid]['channels'] = channels
    retDict[feid]['baselines'] = counts.shape[1]-1 - np.argmax(aa[:, ::-1], axis=1) - (1 if counts.shape[1] == 63 else 0)

  return retDict
