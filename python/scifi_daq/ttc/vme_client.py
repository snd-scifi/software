import os.path as op
import json
import logging
import toml

from scifi_daq.network.connection import Connection


class VmeClient():
  def __init__(self, configurationPath, address, port=41867):
    self._conn = Connection(address, port)
    self.confPath = configurationPath


  def __del__(self):
    self.disconnect()
  

  def connect(self):
    self._conn.connect()
  

  def disconnect(self):
    self._conn.disconnect()
  

  def softReset(self):
    """ Sends a software reset command.
    """
    reply = self._conn.sendCommand(json.dumps({'command': 'generate_soft_reset', 'args': {}}))
    if reply['response'] == 'ok':
      return reply['result']
    else:
      raise RuntimeError(reply['result'])


  def getBcDelay(self):
    """ Returns the BC delay setting.
    
    This is set with the selector on the TTCvi panel.
    """
    reply = self._conn.sendCommand(json.dumps({'command': 'get_bc_delay', 'args': {}}))
    if reply['response'] == 'ok':
      return reply['result']
    else:
      raise RuntimeError(reply['result'])
  

  def getL1aTrigger(self):
    """ Returns the current setting of the L1A trigger.
    
    It contains the L1A input (`l1a_input`), the random trigger rate (`random_trigger_rate`) and the status of the L1A FIFO (`l1a_fifo_empty`, `l1a_fifo_full`).
    """
    reply = self._conn.sendCommand(json.dumps({'command': 'get_l1a_trigger', 'args': {}}))
    if reply['response'] == 'ok':
      return reply['result']
    else:
      raise RuntimeError(reply['result'])
  

  def setL1aTrigger(self, l1a_input=None, random_trigger_rate=None, reset_fifo=False):
    """ Sets the settings of the L1A Trigger.
    
    If a parameter is set to None, it will be left untouched.
    Parameters
    ----------
    * l1a_input: Source of the L1A trigger signal. Can be:
    
      * `l1a-X`, `X` 0 to 3 to select the LEMO connectors on the TTCvi;
      * `vme`, to select a VME command;
      * `random`, to select the random trigger generation;
      * `calibration`, [UNSURE, CHECK THIS]
      * `disable`, to disable L1A generation
      
    * random_trigger_rate: Rate in Hz of the random trigger generator. Can be: `1`, `100`, `1k`, `5k`, `10k`, `25k`, `50k`, `100k`.
    * reset_fifo: Set to True to reset the L1A FIFO.
    """
    args = {}
    if l1a_input is not None:
      args['l1a_input'] = l1a_input
    
    if random_trigger_rate is not None:
      args['random_trigger_rate'] = random_trigger_rate
    
    args['reset_fifo'] = reset_fifo

    reply = self._conn.sendCommand(json.dumps({'command': 'set_l1a_trigger', 'args': args}))
    if reply['response'] == 'ok':
      return reply['result']
    else:
      raise RuntimeError(reply['result'])


  def generateSoftL1A(self):
    """ Generate a software L1A.
    
    The L1A input must be set to VME.    
    """
    reply = self._conn.sendCommand(json.dumps({'command': 'generate_soft_l1a', 'args': {}}))
    
    if reply['response'] != 'ok':
      raise RuntimeError(reply['result'])


  def getOrbitInput(self):
    """ Returns the current orbit input.
    """
    reply = self._conn.sendCommand(json.dumps({'command': 'get_orbit_input', 'args': {}}))
    if reply['response'] == 'ok':
      return reply['result']
    else:
      raise RuntimeError(reply['result'])
  

  def setOrbitInput(self, input : str):
    """ Set the orbit input.

    `input` is either 'internal' (orbit generated in the TTCvi) or 'external' (orbit signal received on the LEMO connector).
    """
    reply = self._conn.sendCommand(json.dumps({'command': 'set_orbit_input', 'args': {'orbit_input': input}}))
    if reply['response'] == 'ok':
      return reply['result']
    else:
      raise RuntimeError(reply['result'])


  def getEventCounter(self):
    reply = self._conn.sendCommand(json.dumps({'command': 'get_event_counter', 'args': {}}))
    if reply['response'] == 'ok':
      return reply['result']
    else:
      raise RuntimeError(reply['result'])
  

  def setEventCounter(self, value: int):
    reply = self._conn.sendCommand(json.dumps({'command': 'set_event_counter', 'args': {'counter_value': value}}))
    if reply['response'] == 'ok':
      return reply['result']
    else:
      raise RuntimeError(reply['result'])


  def sendBChannelShort(self, command: int):
    """ Send a short B channel command.
    
    """
    reply = self._conn.sendCommand(json.dumps({'command': 'send_b_channel_short', 'args': {'command': command}}))
    
    if reply['response'] != 'ok':
      raise RuntimeError(reply['result'])


  def sendBChannelLong(self, address: int, e: str, subaddress: int, command: int):
    """ Send a long B channel command.
    
    Parameters
    ----------
    * address: address of the TTCrx the command is intended for. 0 is broadcast.
    * `e` must be 'ttcrx' or 'external'
    * subaddress: user defined
    * command: user defined
    """
    reply = self._conn.sendCommand(json.dumps({'command': 'send_b_channel_long', 'args': {'address': address, 'e': e, 'subaddress': subaddress, 'command': command}}))
    
    if reply['response'] != 'ok':
      raise RuntimeError(reply['result'])
  

  def addBChannelShort(self, id: int, command: int):
    reply = self._conn.sendCommand(json.dumps({'command': 'add_b_channel_data_short', 'args': {'id': id, 'command': command}}))
    
    if reply['response'] != 'ok':
      raise RuntimeError(reply['result'])


  def addBChannelLong(self, id: int, address: int, e: str, subaddress: int, command: int):
    """
    `e` must be 'ttcrx' or 'external'
    """
    reply = self._conn.sendCommand(json.dumps({'command': 'add_b_channel_data_long', 'args': {'id': id, 'address': address, 'e': e, 'subaddress': subaddress, 'command': command}}))
    
    if reply['response'] != 'ok':
      raise RuntimeError(reply['result'])


  def getBGo(self, id : int):
    """ Get B-Go settings for a given `id` (0 to 3).    
    """
    reply = self._conn.sendCommand(json.dumps({'command': 'get_b_go', 'args': {'id': id}}))
    if reply['response'] == 'ok':
      return reply['result']
    else:
      raise RuntimeError(reply['result'])
  

  def setBGo(self, id: int, front_panel_enable=None, sync=None, repetitive=None, transmit_on_fifo=None, inhibit_delay=None, inhibit_duration=None, retransmit_fifo=None, reset_fifo=False):
    """ Sets the B-Go settings for a given `id` (0 to 3).
    
    If a parameter is set to None, it won't be modified.
    Refer to the TTCvi manual for more information.
    Parameters
    ----------
    * front_panel_enable: True to generate the B-Go from the front panel LEMO input, False to generate it with a VME command (`generateSoftBGo()`);
    * sync: wether the B-Go is generated synchronously to the orbit signal;
    * repetitive: [CHECK, BUT IT SHOULD BE WETHER IT GETS GENERATED AT EVERY ORBIT]
    * transmit_on_fifo: start transmission as soon as the FIFO is not empty;
    * inhibit_delay: inhibit delay, in clock cycles;
    * inhibit_duration: inhibit duration, in clock cycles, should be at least 1;
    * retransmit_fifo: True to keep the FIFO content after trasmission;
    * reset_fifo: True to reset the FIFO.
    """
    args = {'id': id}

    if front_panel_enable is not None:
      args['front_panel_enable'] = front_panel_enable
    
    if sync is not None:
      args['sync'] = sync

    if repetitive is not None:
      args['repetitive'] = repetitive

    if transmit_on_fifo is not None:
      args['transmit_on_fifo'] = transmit_on_fifo
    
    if inhibit_delay is not None:
      args['inhibit_delay'] = inhibit_delay

    if inhibit_duration is not None:
      args['inhibit_duration'] = inhibit_duration

    if retransmit_fifo is not None:
      args['retransmit_fifo'] = retransmit_fifo
    
    args['reset_fifo'] = reset_fifo

    reply = self._conn.sendCommand(json.dumps({'command': 'set_b_go', 'args': args}))
    if reply['response'] == 'ok':
      return reply['result']
    else:
      raise RuntimeError(reply['result'])
  

  def generateSoftBGo(self, id: int):
    """ Generate a software B-Go signal.
    
    To be used when front panel is set to False, see `setBGo(...)`.
    """
    reply = self._conn.sendCommand(json.dumps({'command': 'generate_soft_bgo', 'args': {'id': id}}))
    
    if reply['response'] != 'ok':
      raise RuntimeError(reply['result'])
  

  def syncFeReset(self):
    """ Send a synchronous FE reset.
    """
    self.sendBChannelShort(0b00000100)