import json
from enum import IntEnum

class Header(IntEnum):
  BEGIN_COMMUNICATION = 0x00001111 #///< Packet containing beginning of communication info, C-to-S.
  END_COMMUNICATION = 0x00002222 #///< Packet containing end of communication info, C-to-S.
  DATA = 0x00003333 #///< Packet containing data, C-to-S
  DATA_OK = 0x00004444 #///< Server response to data ok, S-to-C.
  DATA_ERR = 0x00005555 #///< Server response to data error (bad CRC), S-to-C.
  COMMAND = 0x00006666
  COMMAND_REPLY = 0x00007777
  RAW_COMMAND = 0x00008888
  HEARTBEAT = 0x00009999


# def _to_32_big(n):
#  return int.to_bytes(n, 4, 'big', signed=False)


# class Connection():
#   def __init__(self, socket=None, address=''):
#     self._address = address
#     self._socket = socket
  
#   def address(self):
#     return self._address
  
#   def recvAll(self, length):
#     b = b''
#     while len(b) < length:
#       b += self._socket.recv(length-len(b))
#     return b

#   def sendAll(self, data):
#     self._socket.sendall(data)

#   def sendCommand(self, command):
#     self.sendAll(self.buildCommand(0, command))
#     resp = self.recvResponse().decode('utf-8')
#     #print(resp)
#     return json.loads(resp)

#   def recvResponse(self):
#     hdr = self.recvAll(12)
#     header = int.from_bytes(hdr[0:4], 'big')
#     length = int.from_bytes(hdr[4:8], 'big')
#     ID = int.from_bytes(hdr[8:12], 'big')
#     print(length)

#     return self.recvAll(length)




  
#   def buildCommand(self, ID, jsonString):
#     return _to_32_big(Header.COMMAND) + _to_32_big(len(jsonString)) + _to_32_big(ID) + jsonString.encode('utf-8')
  
#   def buildRawCommand(self, ID, jsonString):
#     return _to_32_big(Header.RAW_COMMAND) + _to_32_big(len(jsonString)) + _to_32_big(ID) + jsonString.encode('utf-8')