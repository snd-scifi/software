from datetime import time
import json
import socket
from scifi_daq.network.network_common import Header


def _to_32_big(n):
 return int.to_bytes(n, 4, 'big', signed=False)

class Connection():
  def __init__(self, address, port):
    self._address = address
    self._port = port
    self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
  
  
  def __del__(self):
    self.disconnect()
  
  
  def connect(self, timeout: float = None):
    if timeout is not None:
      self._socket.settimeout(timeout)
    self._socket.connect((self._address, self._port))
  
  
  def disconnect(self):
    self._socket.close()
  

  def timeout(self, timeout: float):
    self._socket.settimeout(timeout)


  def address(self):
    return self._address
  
  def port(self):
    return self._port
  
  def recvAll(self, length):
    b = b''
    while len(b) < length:
      msg = self._socket.recv(4096 if length-len(b) > 4096 else length-len(b))
      if len(msg) == 0:
        raise ConnectionError('0 bytes received. Connection has been closed by peer.')
      b += msg
    return b

  def sendAll(self, data):
    self._socket.sendall(data)

  def sendCommand(self, command, waitResponse=True):
    self.sendAll(self.buildCommand(0, command))
    if not waitResponse:
      return
    resp = self.recvResponse().decode('utf-8')
    #print(resp)
    return json.loads(resp)
  
  def sendRawCommand(self, command):
    self.sendAll(self.buildRawCommand(0, command))
    reply = json.loads(self.recvResponse().decode('utf-8'))
    if reply['response'] == 'ok' and reply['result'] == 'raw_data_follows':
      return self.recvResponse()
    else:
      raise RuntimeError(reply['result'])

  def recvResponse(self): # TODO allow to receive raw data
    hdr = self.recvAll(12)
    header = int.from_bytes(hdr[0:4], 'big')
    length = int.from_bytes(hdr[4:8], 'big')
    ID = int.from_bytes(hdr[8:12], 'big')
    # print(length)

    return self.recvAll(length)


  def buildCommand(self, ID, jsonString):
    return _to_32_big(Header.COMMAND) + _to_32_big(len(jsonString)) + _to_32_big(ID) + jsonString.encode('utf-8')
  
  def buildRawCommand(self, ID, jsonString):
    return _to_32_big(Header.RAW_COMMAND) + _to_32_big(len(jsonString)) + _to_32_big(ID) + jsonString.encode('utf-8')