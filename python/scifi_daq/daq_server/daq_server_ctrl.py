import json

from scifi_daq.network.connection import Connection


class DaqServerCtrl():
  def __init__(self, configurationPath, address, port=42070):
    self._conn = Connection(address, port)
    self.confPath = configurationPath


  def __del__(self):
    self.disconnect()
  

  def connect(self):
    self._conn.connect()
  

  def disconnect(self):
    self._conn.disconnect()
  

  def setRunNumber(self, runNumber):
    reply = self._conn.sendCommand(json.dumps({'command': 'set_run_number', 'args': {'run_number': runNumber}}))
    if reply['response'] != 'ok':
      raise RuntimeError(reply['result'])
  

  def setBoardIds(self, boardIds):
    reply = self._conn.sendCommand(json.dumps({'command': 'set_board_ids', 'args': {'board_ids': boardIds}}))
    if reply['response'] != 'ok':
      raise RuntimeError(reply['result'])


  def setDaqServerSettings(self, settings):
    reply = self._conn.sendCommand(json.dumps({'command': 'set_daq_server_settings', 'args': {'daq_server_settings': settings}}))
    if reply['response'] != 'ok':
      raise RuntimeError(reply['result'])
  

  def setBoardMapping(self, boardMapping):
    reply = self._conn.sendCommand(json.dumps({'command': 'set_board_mapping', 'args': {'board_mapping': boardMapping}}))
    if reply['response'] != 'ok':
      raise RuntimeError(reply['result'])
  

  def setSystemConfiguration(self, systemConfiguration):
    reply = self._conn.sendCommand(json.dumps({'command': 'set_system_configuration', 'args': {'system_configuration': systemConfiguration}}))
    if reply['response'] != 'ok':
      raise RuntimeError(reply['result'])


  def setBoardsConfiguration(self, boardsConfiguration):
    reply = self._conn.sendCommand(json.dumps({'command': 'set_boards_configuration', 'args': {'boards_configuration': boardsConfiguration}}))
    if reply['response'] != 'ok':
      raise RuntimeError(reply['result'])
  

  def setUserInfo(self, userInfo):
    reply = self._conn.sendCommand(json.dumps({'command': 'set_user_info', 'args': {'user_info': userInfo}}))
    if reply['response'] != 'ok':
      raise RuntimeError(reply['result'])
  

  def updateUserInfo(self, userInfo):
    reply = self._conn.sendCommand(json.dumps({'command': 'update_user_info', 'args': {'user_info': userInfo}}))
    if reply['response'] != 'ok':
      raise RuntimeError(reply['result'])

  def startDaq(self, overwrite=False):
    """
    - overwrite: if True, if the run folder exists, it get overwritten. If False, the DAQ doesn't start and returns an error.
    """
    reply = self._conn.sendCommand(json.dumps({'command': 'start_daq', 'args': {'overwrite': overwrite}}))
    if reply['response'] != 'ok':
      raise RuntimeError(reply['result'])


  def stopDaq(self):
    reply = self._conn.sendCommand(json.dumps({'command': 'stop_daq', 'args': {}}))
    if reply['response'] != 'ok':
      raise RuntimeError(reply['result'])
    else:
      return reply['result']
  

  def getRecordedRuns(self):
    reply = self._conn.sendCommand(json.dumps({'command': 'get_recorded_runs', 'args': {}}))
    if reply['response'] != 'ok':
      raise RuntimeError(reply['result'])
    else:
      return reply['result']
    
  def getStatus(self):
    reply = self._conn.sendCommand(json.dumps({'command': 'get_status', 'args': {}}))
    if reply['response'] != 'ok':
      raise RuntimeError(reply['result'])
    else:
      return reply['result']
  
  # def saveConfiguration(self, boardsConfiguration):
  #   reply = self._conn.sendCommand(json.dumps({'command': 'save_configuration', 'args': {'boards': boardsConfiguration}}))
  #   if reply['response'] != 'ok':
  #     raise RuntimeError(reply['result'])