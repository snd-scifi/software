import numpy as np
import pandas as pd

class DataPacket():
  def __init__(self, data):
    self._data = np.array(data, dtype=np.uint32)


class HitPacket(DataPacket):
  def __init__(self, data):
    super().__init__(data)
    hdrs = (self._data & 0xF0000000) >> 28
    if np.any(hdrs != np.array([0, 1, 2, 3])):
      raise RuntimeError('not a hit')

    self.timestampFine = (self._data[2] & 0x3FF00) >> 8
    self.timestampCoarse = ((self._data[2] & 0xFF) << 56) + ((self._data[1] & 0x0FFFFFFF) << 28) +  (self._data[0] & 0x0FFFFFFF)
    self.valueFine = self._data[3] & 0x3FF
    self.valueCoarse = (self._data[2] & 0xFFC0000) >> 18

    self.tofpetChannel = (self._data[3] & 0x0000FC00) >> 10
    self.tofpetId = (self._data[3] & 0x07000000) >> 24
    self.tac = (self._data[3] & 0x00030000) >> 16


def processRawData(data):
  tofpetId = (data[:, 3] & 0x07000000) >> 24
  tofpetChannel = (data[:, 3] & 0x0000FC00) >> 10
  tac = (data[:, 3] & 0x00030000) >> 16
  tcoarse = ((data[:, 2] & 0xFF) << 56) | ((data[:, 1] & 0x0FFFFFFF) << 28) | (data[:, 0] & 0x0FFFFFFF)
  tfine = (data[:, 2] & 0x3FF00) >> 8
  vcoarse = (data[:, 2] & 0x0FFC0000) >> 18
  vfine = data[:, 3] & 0x3FF
  # hitMask  = ((data[:, 0] & 0xF0000000) == 0) \
  # & ((data[:, 1] & 0xF0000000) == 0x10000000) \
  # & ((data[:, 2] & 0xF0000000) == 0x20000000) \
  # & ((data[:, 3] & 0xF0000000) == 0x30000000)
  # print(hitMask)

  df = pd.DataFrame(data=np.array([tofpetId, tofpetChannel, tac, tcoarse, tfine, vcoarse, vfine]).T, columns=['tofpet_id', 'tofpet_channel', 'tac', 't_coarse', 't_fine', 'v_coarse', 'v_fine'])
  return df