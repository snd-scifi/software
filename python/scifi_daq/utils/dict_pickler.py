import builtins
import io
import pickle

safe_builtins = {
  'dict',
}

class DictUnpickler(pickle.Unpickler):
  ''' Upickler that only unpickles dictionaries
  '''
  def find_class(self, module, name):
    # Only allow safe classes from builtins.
    if module == "builtins" and name in safe_builtins:
      return getattr(builtins, name)
    # Forbid everything else.
    raise pickle.UnpicklingError("global '%s.%s' is forbidden" %
                   (module, name))

def restricted_loads(s):
  ''' Like pickle.loads(), but only accepts dictionaries
  '''
  return DictUnpickler(io.BytesIO(s)).load()