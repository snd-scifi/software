from setuptools import setup

with open('../requirements.txt') as f:
    requirements = f.read().splitlines()

setup(
    name = 'scifi_test',
    version = '0.1',
    packages = ["scifi_test"],
    python_requires='>=3.8',
    install_requires=requirements,
)
