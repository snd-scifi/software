"""
    ROOTComp description 
    
    ROOTCmp: 
        A class to compare two ROOT files whether it's match or not.  

"""
import logging
import numpy as np
import pandas as pd
import uproot
import random
from typing import List, Dict

logging.basicConfig()
logging.getLogger().setLevel(logging.INFO)

class ROOTCmp:
    """
        A class for ROOT files comparison 
        Take a list of files together with their labels (either reference or test)
        Then the method runCompareEvent runs a comparison against the reference 
        Please make sure that there is reference label in self.kinds
    """
    def __init__(self):
        """
            Init configurations
        """
        self.fileins      = [] # list of uproot files 
        self.trees        = [] # list of tree
        self.kinds        = [] # list of kind - reference or testing label
        self.randomVals   = [] # list of random vals for testing 
        self.ref_index    = None

    def setDebugLogging(self):  
        """
            Set logging to debug level 
        """
        logging.getLogger().setLevel(logging.DEBUG)


    def addFiles(self, fileNames: List[str], treeNames: List[str], kinds: List[str]) -> None:
        """
            Add ROOT files and configurations

            Keyword arguments: 
                fileNames -- list of ROOT filenames to be added 
                treeNames -- list of tree names corresponding to the ROOT file
                kinds     -- list of files' labels 
        """
        for ind, (fileName, treeName, kind) in enumerate(zip(fileNames, treeNames, kinds)): 
            self.addFile(fileName, treeName, kind)

            # Catch reference index 
            if kind == "reference":
                self.ref_index = ind

    def addFile(self, fileName: str, treeName: str, kind: str) -> None:
        """ 
            Add each file 

            Keyword arguments:
                fileName -- name of a file
                treeName -- name of a tree in the file
                kind -- either reference or test 
        """
        logging.info(f"Adding {fileName} with tree {treeName} as {kind}")
        self.fileins.append(uproot.open(fileName))
        self.trees.append(self.fileins[-1][treeName])
        self.kinds.append(kind)

    def Is_BranchesEqual(self) -> bool: 
        """
            Comparison between branches from trees
            return True of all of names are equal to ref 
        """
        # Check ref index 
        if self.ref_index == None: 
            raise ValueError("Please specify which file is a reference file")

        # Compare without ordering
        truth = [set(tree.keys()) == set(self.trees[self.ref_index].keys()) for tree in self.trees] 

        return all(truth) 


    def setupFiles(self): 
        """
            Convert trees to Pandas.DataFrame via awkward array in uproot

            Store information in
            self.bnames -- branches' names in the reference file
            self.data   -- Pandas dataframes
        """ 

        # Check and get branches' names  
        if not self.Is_BranchesEqual(): 
            raise ValueError("Branch names are not the same!")
        else:
            logging.info("Branch names are the same, proceed to the next step")
        
        # Branches' names
        self.bnames = self.trees[self.ref_index].keys()

        # Get data
        self.data = [ROOTCmp.UpTree2DF(tree, self.bnames, kind) for tree, kind in zip(self.trees, self.kinds)]


    def runCompareEvent(self) -> None:            
        """
            Compare files against reference 
        """
        self.output = []
        col_compare = list(self.data[self.ref_index].columns.values.tolist())
        col_compare.remove("label")
        for dat in self.data: 
            self.output.append(ROOTCmp.ComparePD(dat, self.data[self.ref_index], col_compare))

    def generateDiffReport(self) -> bool:
        """ 
            Generate similarity/difference report 
            Print out difference in each event
        """
        logging.info(10*"=" + " Difference " + 10*"=")

        # Report
        for outp in self.output:
            # Check main index for pretty print
            inds_diff = set([ind[0] for ind in outp.index])

            # Index  
            for ind_diff in inds_diff: 
                ROOTCmp.printDifferenceIndex(outp.loc[(ind_diff,)], ind_diff)

        # output 
        return all([True if df.empty else False for df in self.output])

    def setRandomTests(self, n_test = 1): 
        """
            Generate random values to be added to self.data
            Keyword arguments 
                n_test -- amount of values to be changed 
        """

        for _ in range(n_test):
            self.randomVals.append(self.GetRandomTest())
            self.ChangetoRandomValues(self.randomVals[-1], self.data)

        # logging 
        logging.info("     Test diff with random values. Add with ")
        logging.info(f"                                 {self.randomVals}")


    def GetRandomTest(self) -> Dict: 
        """
            Generate random value to change a value in a branch
            Feed in this information in GetData method
            return dict of a configuration of a random event
                rand_tree     - random index of tree in self.trees
                rand_nevent   - random n_event 
                rand_branch   - random branch 
                rand_nentries - random entries in a branch 
                rand_nval     - random offset [should not be zero!]
        """
        # Check file configuration
        sys_random = random.SystemRandom()
        ns_tree = list(range(len(self.trees)))
        ns_tree.remove(self.ref_index)
        rand_tree = sys_random.choice(ns_tree)
        nentries = self.trees[self.ref_index].num_entries
        nbranch  = len(self.bnames) 
    
        # Gen random  
        rand_nevent = sys_random.choice(range(nentries))
        rand_branch = sys_random.choice(self.bnames)
        nentries_ = self.data[self.ref_index].xs(rand_nevent).shape[0]
        rand_nentries = sys_random.choice(range(nentries_)) 
        rand_nval = 9e6 + sys_random.randint(0,20)

        # Set random dict in a list for future extension 
        dict_rand = {
                    "rand_tree": rand_tree,
                    "rand_nevent": rand_nevent,
                    "rand_branch": rand_branch,
                    "rand_nentries": rand_nentries,
                    "rand_nval":    rand_nval,
                    }
        return dict_rand

    @staticmethod
    def printDifferenceIndex(df: pd.DataFrame, N_event: int):
        """
            Print difference of dataframe with main index N_event 
             
            Keyword arguments: 
            df      -- Dataframe 
            N_event -- index 

        """
        # Check which branches are different in each event  
        diff_cols = []
        for col in df:
            if len(df[col].unique()) > 1:
                diff_cols.append(col)
        if len(diff_cols) > 0:
            logging.info(f"=> Event {N_event}")
            logging.info(f"\n{df[diff_cols]}")

    @staticmethod
    def UpTree2DF(uptree:uproot.models.TTree, bnames:List[str], kind:str, replaceNaN:bool = True): 
        """
           Convert between uptree awkward array to pandas.DataFrame 
           Keyword arguments: 
                uptree - TTree from uproot uproot.models.TTree
                bnames - list of branch names for column
                kind   - label 
                replaceNaN - replace NaN or not
        """
        # Conversion 
        df = uptree.arrays(bnames, library = "pd") 
        if replaceNaN: 
            # fill NaN inPlace with INF
            df.fillna(np.inf, inplace = True)

        # Add label 
        df["label"] = kind
        return df 

    @staticmethod
    def ComparePD(data1:pd.DataFrame, data2:pd.DataFrame, col_compare = []):
        """
            Comparison between two dataframes and drop drop_duplicates 
            Keyword argument:
            data1 -- Dataframe 
            data2 -- Dataframe 
            col_compare -- columns to be compared 
        """
        logging.debug(f"Comparing \n{data1}")
        logging.debug(f"Comparing \n{data2}")
        return pd.concat([data1,data2]).drop_duplicates(subset = col_compare,keep=False).sort_index() 

    @staticmethod
    def ChangetoRandomValues(dict_, datas): 
        """
            Change a value in datas by indices  
            Keyword arguments: 
            dict_ -- dict of the value to be change generated by self.GetRandomTest()
            datas -- list of pandas dataFrame
        """
        indTree = dict_["rand_tree"]
        indEntry = dict_["rand_nevent"]
        indSubEntry = dict_["rand_nentries"]
        bname = dict_["rand_branch"]
        val = dict_["rand_nval"]
        datas[indTree].at[(indEntry, indSubEntry), bname] = val

