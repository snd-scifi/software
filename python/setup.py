from setuptools import setup
from pybind11.setup_helpers import Pybind11Extension
from glob import glob
import os

cxx_std = int(os.environ.get("CMAKE_CXX_STANDARD", "17"))

ext_modules = [
    Pybind11Extension(
        "scifi_daq.utils.csv_to_dict_reader",
        sorted(glob("cpp/*.cpp")),  # Sort source files for reproducibility
        include_dirs=["../external/csv-parser/single_include"],
        cxx_std=cxx_std
    ),
]

with open('requirements.txt') as f:
    requirements = f.read().splitlines()

with open('../VERSION') as f:
    version = f.read().rstrip()

setup(
    name = 'scifi_daq',
    version = version,
    packages = ["scifi_daq"],
    python_requires='>=3.8',
    install_requires=requirements,
    scripts=['bin/tofpet-check-calib.py','bin/tofpet-plot-hists-calib.py', 'bin/tofpet-plot-tdc-qdc-calib.py','bin/tofpet-plot-tia-thr-pe-calib.py', 'bin/tofpet-scan-rx-phase.py'],
    ext_modules=ext_modules
)
