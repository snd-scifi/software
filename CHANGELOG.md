# Changelog

## User visible changes

### 2.9
* Added LPHE ECAL description in the software
* Reorganized code

### 2.8
* Added method to set date and time on the DAQ boards (`d.setDateTime()` where `d` can be of `DaqBoard` or `DaqBoards` type).

### 2.7.1
* Added track covariance to the output file.

### 2.7
* Added `scifi_tracker` processor to reconstruction. It can find multiple tracks per event.

### 2.6
* Added `simple_noise_filter` processor, with two new board mapping types (`scifi_module` and `lphe_ecal`).

### 2.5
* Added `snd-reprocess-simple` to reprocess the data (does not require fills and accelerator modes).

### 2.4.1
* Added `setClk40UseQpll(...)` and `getClk40UseQpll()` to `DaqBoards`.

### 2.4
* Added option to use CAEN V3728 (USB) VME controller.

### 2.3
* Added option to raise exception when the TOFPET IDs don't match the configuration.

### 2.2
* Added timeout to connect and to other DAQ board operations.

### 2.1
* Added `current_run_size` to the DAQ server status, which reports the size of the current run in bytes

### 2.0
* Added event reconstruction for SciFi.

### 1.2
* Added triggered event builder.
* Added option to exclude boards in the board simulator.

### 1.1
* Added option to set the tac_refresh_en and tac_refresh_period in the TOFPET global configuration.
* Added option to set the tac_max_age and tac_min_age in the TOFPET channel configuration.

### 1.0
* Restructured the data flow, now event builder, processors and writer are well separated and it is possible to select each item separately.
* The configuration files need to be updated accordingly.

### 0.26
* Added option to use CAEN V1728 and V2728 VME controllers (to be specified with `-i` option) with the VME server;
* Added option to specify the TTCvi address (`-a` option) in the VME server.

### 0.25
* Updated software to work with the DAQ board V4:
  * Commands to set the clock mux and Clock40Des phases;
  * Board temperature;
  * Executables for clock mux and board temperature.
* Some code cleanup.

### 0.24.1
* Added option to have external DAQ reset.

### 0.24
* Board calibration functions changed their names, now there is a more consistent naming scheme;
* Plots are saved when running calibration;
* Added scripts to make plots and histograms of the calibration and check calibration values.

### 0.23
* Added snd reprocessing executable.
* Added settings to `TofpetCalibrator` class (type, default): 
  * `timestamp_correct_do_t1_delay` (bool, false)
* Removed some verbose printing from detector and noise filter classes.

### 0.22
* Improved calibration code
  * corrected wrong `v_coarse` estimation in QDC
  * corrected doT1 delay in QDC and TOT
* added settings to `TofpetCalibrator` class (type, default): 
  * `qdc_correct_v_coarse` (bool, true),
  * `qdc_correct_do_t1_delay` (bool, true),
  * `qdc_correct_gain` (bool, false),
  * `tot_v_coarse_max` (uint16_t, 1024),
  * `tot_correct_do_t1_delay` (bool, true)

### 0.21.2
* Added `readConfiguration` function to `DaqBoards`.

### 0.21
* Added option to change the TTree name in EventRootWriterSingle and EventRootReaderSingle.

### 0.20
* Added binary raw packet writer, can be enabled with `pkts_bin`.
* Changed the board simulator to use binary files.
* Kept the old board simulator with a different name.

### 0.19
* The event builder now saves data in a ROOT file with a single TTree

### 0.17
* Monitoring path now contains the run number: `monitoring_run_XXXXXX`.
* Event selection for monitoring is now random, base on the prescaler settings.

### 0.16
* Monitoring stream now saves data in a single TTree.

### 0.15.1
* The `data_monitoring_stream` processor now saves one event every `prescaler_setting` (default 100) or after at most `max_delta_t` seconds (default 0.5).

### 0.15
* Added total and processed hit and event counters and rate for all hit and events to the DAQ server getStatus().
* Removed old DAQ information: `processed_evts` and so on. Now all the DAQ related data is in `daq_monitor`.
* Added disk usage information to the status.

### 0.14
* Added `data_monitoring_stream` processor, to have a separate data stream with reduced rate for monitoring purposes.

### 0.12
* Added run_timestamps.json to the data, where start, stop and end time of each run are saved.

### 0.11
* Added userInfo in the DAQ server manager, settable with python function
* Added processor to set flags in the data based on userInfo for SND (fill number, beam mode and accelerator mode)

### 0.10
* Added evtNumber and flags in TofpetEvent and the relative writers
* Added event flagging to SND fast and advanced noise filters

### 0.9
* apply-calibration executable now works with ROOT event files.

### 0.8.3
* Added laser injection enable. Requires FW version >= 72.

### 0.8.2
* Run folder is not overwritten by default if it exists.

### 0.8
* Added advanced noise filter
* Added veto in fast noise filter

### 0.7
* Sync reset is now in phase with the orbit pulse.

### 0.6
* Added executables to read temperature and disable LEDs.
* Added board name and ID in the board class.
* Added `auto_save_n_events` and `auto_save_delay_min` and `auto_save_delay_max` writer options, to force a TTree AutoSave every N events or every N seconds, to make them readable while being written.
* Allowed not to have `fe` entries in the `board.toml` configuration file.
* Added commands to reboot the board and enable/disable the board-server start at boot.
* Changed default threshold for TIA baseline determination
* Added activation script in the build folder. Calling `source activate.sh` will add the `bin` folder to the PATH.
* Added `ttc_fe_reset` option for SMA debug output. Works with FW version >= 80.

### 0.5.1
* Added `auto_flush` and `auto_save` options to event builder, to set the corresponding TTree options.

### 0.4
* Allowed to have inconsistency between available TOFPETs and configured. The intersection is taken.

### 0.3
* Added `n_qpll_locked_latch` and `n_ttcrx_ready_latch` in the returned status. They are `false` normally and true if QPLL locked or TTCrx ready was lost at any point.
* Removed `evts_trigless_writer`. Use `evts_trigless_writer_mt`.
* Info logger level is now default for all executables.


## Bugfixes

### 2.5
* Fixed bug in `boards-simulator` that required the excluded board flag to be always present.

### 2.1
* If there is a mistake in the configuration of one of the processors, the following ones are now still added.

### 1.2.1
* Fixed bug in the calculation of `value_saturation`.
* Fixed bug in `DaqBoards.getTrigger` and `DaqBoards.getInjection` (None was returned).

### 1.2
* Fixed bug in TTCvi random trigger generator (wrong string for the 100 kHz rate).

### 1.1.1
* Corrected the QDC calibration function to account for the case where `qdc_intg_t_max < qdc_intg_t_max`.

### 1.0.1
* Added retry when TDC and QDC calibration get no data.

### 0.13
* Python commands no longer hang if the server dies. A check has been placed in Connection.recvAll().

### 0.8.4
* It could happen that `qdc_max_intg_t` < `qdc_min_intg_t`. Now they are guaranteed to be equal.

### 0.8.3
* clk160 is now correctly selected in DaqBoard.setInjection(...)

### 0.6
* Fixed bug in event builder (wrong timestamp assigned to hits if BoardEvent had a different timestamp from Event)

### 0.5.2
* Fixed bug in QDC calibration

### 0.5
* DCR scan now uses a different method, different from the one described in the TOFPET datasheet, as it works better.

## Internal changes

### 2.9
* Added `MSG_NOSIGNAL` flag to `send()`, to prevent the proces from being killed by SIGPIPE.
* Added `reinterpret_cast` to the VM-USB device pointer when comparing it to 0.

### 1.2
* Changed json `operator[]` with `.at()` when relevant.

### 1.0
* DataPacket now contains the board ID.
* TofpetBoardEvent has been removed and TofpetEvent now contains hits.
* DaqInfo has been removed.
* Renamed some variables and classes to make them more consistent.
* Restructured monitors.

### 0.26
* Separated VME implementations from abstract VME class.

### 0.24
* Refactored calibration functions in different files;
* Added lines to make python work on Mac OS X.

### 0.23
* Added functions to convert from UTC string to timestamp.

### 0.21.1
* Reduced default number of jobs to 1 in TDC and QDC calculation.

### 0.18
* TOFPET configuration now re-tries several times in case of errors.

### 0.16
* Added single TTree ROOT event writer.

### 0.15
* Removed DaqInfo from getStatus, but still not removed completely.

### 0.14
* Added DataMonitoringStream processor and added it to the event builder.

### 0.13
* Connection.recvAll() now receives at most 4096 bytes at a time. In addition, if it receives zero bytes, it raises an exception.

### 0.12
* Event header flags definition for SND moved to an external repository.
* Added .vscode folder to the project

### 0.9
* Added separate ROOT event reader and writer.
* Used these writers in the triggered and triggerless event builders.
* Small cleanups.

### 0.8.1
* Fixed warning related to pandas append.

### 0.7
* Added missing TTCvi commands in the python module.
* Using B-Go 0 to send the sync reset command to the boards.

### 0.6
* Replaced pointer with iterator DataPacket::createPacket
* Replaced pandas append with concat

### 0.5
* Improved getDarkCounts2 and now it is the default used in DCR scan.
* Requires FPGA FW version >= 78.

### 0.4
* Added code to select only actually used channels as intersection of configuration and what is actually available on the board or from board configuration.

### 0.3
* Added the registers for `n_qpll_locked_latch` and `n_ttcrx_ready_latch` and the methods to return them.
* Moved writers and other host-only code to `lib-host`.
* Removed old logger.
* Removed workaround for the board shared between US and DS in the SND noise filter.
