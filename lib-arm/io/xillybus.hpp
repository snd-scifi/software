#pragma once

#include <cstdint>
#include <vector>
#include <exception>
#include <stdexcept>
#include <cstring>
#include <cerrno>
#include <mutex>

#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>

#include "utils/logger.hpp"

template<typename D = uint32_t>
class XillyFile {
public:

  void open(std::string filename, int flags) {
    std::scoped_lock<std::mutex> lock(m_mtx);
    m_filename = filename;
    m_fd = ::open(filename.c_str(), flags);

    if (m_fd < 0) {
      ::close(m_fd);
      m_fd = -1;
      THROW_SE(errno, "Failed to open device file {} ", filename);
    }
    DEBUG("Opened file {}", filename);
  }

  bool isOpen() {
    std::scoped_lock<std::mutex> lock(m_mtx);
    return m_fd > 0;
  }
    
  void close() {
    std::scoped_lock<std::mutex> lock(m_mtx);
    ::close(m_fd);
    m_fd = -1;
    DEBUG("Closed file {}", m_filename);
  }

  /**
   * Read length elements from a "normal" (non-seekable) Xillybus file, returning a vector containing them.
   * This blocks until all the requested elements have been read or until an EOF is received.
   * Throws a std::runtime_error if the reding fails for any reason.
   */
  std::vector<D> read(size_t length) {
    std::scoped_lock<std::mutex> lock(m_mtx);
    size_t received = 0;
    std::vector<D> buffer(length);
    // length is now in bytes
    length *= sizeof(D);

    while (received < length) {
      // try to read len bytes from fd
      //INFO("reading");
      int rc = ::read(m_fd, (char*) buffer.data() + received, length - received);
      //INFO("Read ", rc, " bytes out of ", length, " bytes requested");

      if ((rc < 0) && (errno == EINTR)) { // read interrupted by a signal, retry
        DEBUG("Read interrupted by a signal");
        continue;
      }
      // else if ((rc < 0) && (errno == EAGAIN)) { // no data to read, retry
      //   INFO("No data to read EAGAIN");
      //   continue;
      // }
      else if (rc < 0) { // another error occurred
        THROW_SE(errno, "Failed to read from xillybus device file");
      }
      else if (rc == 0) { // check if EOF
        ERROR("The EOF signal must not be used in the FPGA. How did you end up here?");
        break;
      }

      received += rc;
    }

    buffer.resize(received/sizeof(D));
    return buffer;
  }

  /**
   * Writes data to a "normal" (non-seekable) Xillybus file.
   * Blocks until all data has been written.
   * Throws a std::runtime_error if any error occurs.
   */
  void write(std::vector<D> data) {
    std::scoped_lock<std::mutex> lock(m_mtx);
    size_t sent = 0;

    while (sent < data.size()) {
      int rc = ::write(m_fd, (char*) data.data() + sent, data.size()*sizeof(D) - sent);
    
      if ((rc < 0) && (errno == EINTR)) { // write interrupted by a signal, retry
        continue;
      } else if (rc < 0) { // another error occurred
        THROW_RE("Failed to write to xillybus device file: ", std::strerror(errno));
      }
  
      sent += rc;
    }
  }

  XillyFile(XillyFile const&) = delete;             // Copy construct
  XillyFile(XillyFile&&) = delete;                  // Move construct
  XillyFile& operator=(XillyFile const&) = delete;  // Copy assign
  XillyFile& operator=(XillyFile &&) = delete;      // Move assign

protected:
  XillyFile() : m_fd(-1) {};
  ~XillyFile() {
    close();
  }

  int m_fd;
  std::string m_filename;
  std::mutex m_mtx;
};

template<typename D = uint32_t, typename A = uint16_t>
class XillyRegFile {
public:
  void open(std::string filename) {
    std::scoped_lock<std::mutex> lock(m_mtx);
    m_filename = filename;
    m_fd = ::open(filename.c_str(), O_RDWR);

    if (m_fd < 0) {
      auto strerr = std::strerror(errno);
      ::close(m_fd);
      m_fd = -1;
      THROW_RE("Failed to open device file ", filename, ": ", strerr);
    }
    DEBUG("Opened file {}", filename);
  }

  bool isOpen() {
    std::scoped_lock<std::mutex> lock(m_mtx);
    return m_fd > 0;
  }

  void close() {
    std::scoped_lock<std::mutex> lock(m_mtx);
    ::close(m_fd);
    m_fd = -1;
    DEBUG("Closed file {}", m_filename);
  }

  /**
   * Reads one element from a "register" (seekable) Xillybus file.
   * address must be a multiple of sizeof(D), otherwise the seek fails.
   * It throws a std::runtime_error if the seek fails or another error occurs.
   */
  D read(A address) {
    std::scoped_lock<std::mutex> lock(m_mtx);
    size_t received = 0;
    D buffer = 0;
    // length is in bytes
    size_t length = sizeof(D);

    if (lseek(m_fd, address, SEEK_SET) < 0) {
      THROW_RE("Failed to seek xillybus device file: ", std::strerror(errno));
    }

    while (received < length) {
      // try to read len bytes from fd
      int rc = ::read(m_fd, (char*) &buffer + received, length - received);
      // DEBUG("Read ", rc, " bytes out of ", length, " bytes requested");

      if ((rc < 0) && (errno == EINTR)) { // read interrupted by a signal, retry
        DEBUG("Read interrupted by a signal");
        continue;
      } else if (rc < 0) { // another error occurred
        THROW_SE(errno, "Failed to read from xillybus device file");
      } else if (rc == 0) { // check if EOF
        DEBUG("Read interrupted by EOF");
        ERROR("The EOF signal must not be used in the FPGA. How did you end up here?");
        break;
      }

      received += rc;
    }

    return buffer;
  }

  /**
   * Reads length elements from a "register" (seekable) Xillybus file, starting from address.
   * address must be a multiple of sizeof(D), otherwise the seek fails.
   * It throws a std::runtime_error if the seek fails or another error occurs.
   * Blocks until all elements have been read or an EOF occurs.
   */
  std::vector<D> read(A address, size_t length) {
    std::scoped_lock<std::mutex> lock(m_mtx);
    size_t received = 0;
    std::vector<D> buffer(length);
    // length is now in bytes
    length *= sizeof(D);

    if (lseek(m_fd, address, SEEK_SET) < 0) {
      THROW_RE("Failed to seek xillybus device file: ", std::strerror(errno));
    }

    while (received < length) {
      // try to read len bytes from fd
      int rc = ::read(m_fd, (char*) buffer.data() + received, length - received);
      // DEBUG("Read ", rc, " bytes out of ", length, " bytes requested");

      if ((rc < 0) && (errno == EINTR)) { // read interrupted by a signal, retry
        DEBUG("Read interrupted by a signal");
        continue;
      } else if (rc < 0) { // another error occurred
        THROW_SE(errno, "Failed to read from xillybus device file");
      } else if (rc == 0) { // check if EOF
        DEBUG("Read interrupted by EOF");
        ERROR("The EOF signal must not be used in the FPGA. How did you end up here?");
        break;
      }

      received += rc;
    }

    buffer.resize(received/sizeof(D));
    return buffer;
  }

  /**
   * Writes one element to a "register" (seekable) Xillybus file.
   * address must be a multiple of sizeof(D), otherwise the seek fails.
   * It throws a std::runtime_error if the seek fails or another error occurs.
   * Blocks until all data have been written.
   */
  void write(A address, D data) {
    std::scoped_lock<std::mutex> lock(m_mtx);
    size_t sent = 0;

    if (lseek(m_fd, address, SEEK_SET) < 0) {
      THROW_RE("Failed to seek xillybus device file: ", std::strerror(errno));
    }

    while (sent < sizeof(data)) {
      int rc = ::write(m_fd, (char*) &data + sent, sizeof(D) - sent);
      // DEBUG("Written ", rc, " bytes");
    
      if ((rc < 0) && (errno == EINTR)) { // write interrupted by a signal, retry
        continue;
      } else if (rc < 0) { // another error occurred
        THROW_RE("Failed to write to xillybus device file: ", std::strerror(errno));
      }
  
      sent += rc;
    }
  }

  /**
   * Writes elements to a "register" (seekable) Xillybus file, starting from address.
   * address must be a multiple of sizeof(D), otherwise the seek fails.
   * It throws a std::runtime_error if the seek fails or another error occurs.
   * Blocks until all data have been written.
   */
  void write(A address, std::vector<D> data) {
    std::scoped_lock<std::mutex> lock(m_mtx);
    size_t sent = 0;

    if (lseek(m_fd, address, SEEK_SET) < 0) {
      THROW_RE("Failed to seek xillybus device file: ", std::strerror(errno));
    }

    while (sent < data.size()) {
      int rc = ::write(m_fd, (char*) data.data() + sent, data.size()*sizeof(D) - sent);
      // DEBUG("Written ", rc, " bytes");
    
      if ((rc < 0) && (errno == EINTR)) { // write interrupted by a signal, retry
        continue;
      } else if (rc < 0) { // another error occurred
        THROW_RE("Failed to write to xillybus device file: ", std::strerror(errno));
      }
  
      sent += rc;
    }
  }

  XillyRegFile(XillyRegFile const&) = delete;             // Copy construct
  XillyRegFile(XillyRegFile&&) = delete;                  // Move construct
  XillyRegFile& operator=(XillyRegFile const&) = delete;  // Copy assign
  XillyRegFile& operator=(XillyRegFile &&) = delete;      // Move assign

protected:
  XillyRegFile() : m_fd(-1) {};
  virtual ~XillyRegFile() {
    close();
  }

  int m_fd;
  std::string m_filename;
  std::mutex m_mtx;
};
