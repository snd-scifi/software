#pragma once

#include "io/xillybus.hpp"


class DataStream : public XillyFile<uint32_t> {
public:
  static DataStream& getInstance() {
    static DataStream ds;
    if (!ds.isOpen()) {
      ds.open("/dev/xillybus_data", O_RDONLY);
    }
    return ds;
  }

  DataStream(DataStream const&) = delete;             // Copy construct
  DataStream(DataStream&&) = delete;                  // Move construct
  DataStream& operator=(DataStream const&) = delete;  // Copy assign
  DataStream& operator=(DataStream &&) = delete;      // Move assign

private:
  DataStream() {
    open("/dev/xillybus_data", O_RDONLY);
  }
};

class DataWriter : public XillyFile<uint32_t> {
public:
  static DataWriter& getInstance() {
    static DataWriter dw;
    if (!dw.isOpen()) {
      dw.open("/dev/xillybus_data_write", O_WRONLY);
    }
    return dw;
  }

  DataWriter(DataWriter const&) = delete;             // Copy construct
  DataWriter(DataWriter&&) = delete;                  // Move construct
  DataWriter& operator=(DataWriter const&) = delete;  // Copy assign
  DataWriter& operator=(DataWriter &&) = delete;      // Move assign

private:
  DataWriter() {
    open("/dev/xillybus_data_write", O_WRONLY);
  }
};

class Registers : public XillyRegFile<uint32_t, uint16_t> {
public:
  static Registers& getInstance() {
    static Registers ds;
    if (!ds.isOpen()) {
      ds.open("/dev/xillybus_registers");
    }
    return ds;
  }

  Registers(Registers const&) = delete;             // Copy construct
  Registers(Registers&&) = delete;                  // Move construct
  Registers& operator=(Registers const&) = delete;  // Copy assign
  Registers& operator=(Registers &&) = delete;      // Move assign

private:
  Registers() {
    open("/dev/xillybus_registers");
  }
};