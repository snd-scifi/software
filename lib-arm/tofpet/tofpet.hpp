#pragma once

#include <iostream>

#include "tofpet/tofpet_cfg.hpp"
#include "storage/data_packet.hpp"

class tofpet_error : public std::runtime_error {
  public:
  tofpet_error(const std::string& what_arg) : std::runtime_error(what_arg) {}
  tofpet_error(const char* what_arg) : std::runtime_error(what_arg) {}
  tofpet_error(const tofpet_error& other) : std::runtime_error(other) {}
};

void tofpetReset();

uint8_t getFeMask(const std::vector<uint8_t> feIds);

void tofpetGlobalConf(uint8_t id, const TofpetGlobalCfg& cfg);
TofpetGlobalCfg tofpetGlobalConf(uint8_t id);

void tofpetChannelConf(uint8_t id, const TofpetChannelCfg& cfg);
TofpetChannelCfg tofpetChannelConf(uint8_t id, uint8_t channel);

void tofpetConf(uint8_t id, const TofpetCfg& cfg);
TofpetCfg tofpetConf(uint8_t id);

void tofpetGlobalConfMulti(const std::map<uint8_t, TofpetCfg>& cfgs);
void tofpetGlobalConfMulti(const std::map<uint8_t, TofpetGlobalCfg>& cfgs);
std::map<uint8_t, TofpetGlobalCfg> tofpetGlobalConfMulti(std::vector<uint8_t> ids);

void tofpetChannelConfMulti(const std::map<uint8_t, TofpetCfg>& cfgs, uint8_t channel);
// std::vector<TofpetCfg> tofpetChannelConfMulti(std::vector<uint8_t> ids, uint8_t channel);

void tofpetConfMulti(const std::map<uint8_t, TofpetCfg>& cfgs);
std::map<uint8_t, TofpetCfg> tofpetConfMulti(const std::vector<uint8_t>& ids);

enum Discriminator {DISC_T1, DISC_T2, DISC_E};

std::map<uint8_t, std::map<uint8_t, std::vector<uint32_t>>> tiaBaselineCalibration(std::map<uint8_t, std::vector<uint8_t>> feChs, const Discriminator disc);
std::map<uint8_t, std::map<uint8_t, std::vector<uint32_t>>> discriminatorThrCalibration(std::map<uint8_t, std::vector<uint8_t>> feChs, const Discriminator disc);
std::pair<std::vector<double>, std::map<uint8_t, std::map<uint8_t, std::vector<uint64_t>>>> getDarkCounts(std::map<uint8_t, std::vector<uint8_t>> feChs, const Discriminator disc, const float minTime, const uint64_t minEvents, const float maxTime);
std::pair<std::vector<double>, std::map<uint8_t, std::map<uint8_t, std::vector<uint64_t>>>> getDarkCounts2(std::map<uint8_t, std::vector<uint8_t>> feChs, const Discriminator disc, const uint64_t minTime, const uint64_t minEvents, const uint64_t maxTime);

std::vector<DataPacket> tdcQdcCalibration(const bool tdc, std::map<uint8_t, std::vector<uint8_t>> feChs, const uint8_t phase, const uint8_t duration, const uint32_t nEvents, const uint8_t simultaneousChannels);


std::ostream& operator<<(std::ostream& out, const Discriminator disc);
Discriminator discriminatorFromString(std::string disc);
