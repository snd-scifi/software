#pragma once

#include <vector>
#include <bitset>
#include <cstdint>

#include "json.hpp"

#include "utils/logger.hpp"

using json = nlohmann::json;


/**
 * Class representing the channel configuration of the TOFPET2 ASIC
 */
class TofpetChannelCfg {
public:
  TofpetChannelCfg();
  TofpetChannelCfg(const TofpetChannelCfg& cfg);
  TofpetChannelCfg(uint8_t channel);
  TofpetChannelCfg(uint8_t channel, std::vector<uint32_t> data, uint8_t thrT1Bl, uint8_t thrT2Bl, uint8_t thrEBl);
  TofpetChannelCfg(uint8_t channel, json conf);
  uint8_t getChannel() const { return m_channel; }
  /**
   * Sets the channel.
   */
  void setChannel(uint8_t channel) { m_channel = channel; }

  enum TriggerMode {
    TRIG_NORMAL = 0b00, ///< Normal triggering using discriminators outputs
    TRIG_TEST_PULSE = 0b01, ///< All discriminators are replaced by TEST_PULSE
    TRIG_INVERTED = 0b10, ///< All discriminators are inverted
    TRIG_DISABLED = 0b11 ///< Channel disabled
  };
  TofpetChannelCfg& setTriggerMode(TriggerMode mode); // trigger_mode_1
  TriggerMode getTriggerMode() const;

  enum TrigGenT {
    T_DO_T1 = 0b00, ///< Should be used with delay line OFF
    T_DO_T1_AND_DO_T2 = 0b01, ///< Should NOT be used with delay line OFF
    T_DO_T1_AND_DO_E = 0b10, ///< Should NOT be used with delay line OFF
    T_DO_E = 0b11
  };
  TofpetChannelCfg& setTriggerGenerationT(TrigGenT triggerT); // trigger_mode_2_t
  TrigGenT getTriggerGenerationT() const;

  enum TrigGenQ {
    Q_DO_T1 = 0b00,
    Q_DO_T2 = 0b01,
    Q_DO_E = 0b10
  };
  TofpetChannelCfg& setTriggerGenerationQ(TrigGenQ triggerQ); // trigger_mode_2_q
  TrigGenQ getTriggerGenerationQ() const;

  enum TrigGenE {
    E_NOT_DO_T1 = 0b000,
    E_NOT_DO_T2 = 0b001,
    E_NOT_DO_E = 0b010,
    E_NOT_DO_T1_AND_DO_T2 = 0b011, ///< not (do_T1' and do_T2)
    E_NOT_DO_T1_AND_DO_E = 0b100, ///< not (do_T1' and do_E)
    E_DO_T1 = 0b101,
    E_DO_T2 = 0b110,
    E_DO_E = 0b111
  };
  TofpetChannelCfg& setTriggerGenerationE(TrigGenE triggerE); // trigger_mode_2_e
  TrigGenE getTriggerGenerationE() const;

  enum TrigGenB {
    B_DO_T1 = 0b000,
    B_DO_T2 = 0b001,
    B_DO_E = 0b010,
    B_DO_T1_OR_DO_T2 = 0b011,
    B_DO_T1_OR_DO_E = 0b100,
    B_DO_T1_OR_DO_T2_OR_DO_E = 0b101
  };
  TofpetChannelCfg& setTriggerGenerationB(TrigGenB triggerB); // trigger_mode_2_b
  TrigGenB getTriggerGenerationB() const;
  
  enum DigitalDebugMode {
    DBG_DISABLED = 0b00,
    DBG_DO_X = 0b01,
    DBG_TRIGGER_X = 0b10
  };
  TofpetChannelCfg& setDigitalDebugMode(DigitalDebugMode mode); // debug_mode
  DigitalDebugMode getDigitalDebugMode() const;

  TofpetChannelCfg& setTacMaxAge(uint8_t value); // tac_max_age, 5 bits
  uint8_t getTacMaxAge() const;
  
  TofpetChannelCfg& setTacMinAge(uint8_t value); // tac_min_age, 5 bits
  uint8_t getTacMinAge() const;
  
  //things
  
  TofpetChannelCfg& setDeadTime(uint8_t time); // dead_time, 6 bits
  uint8_t getDeadTime() const;

  enum CounterMode {
    CNT_DISABLED = 0x0,
    CNT_ALWAYS = 0x1,
    CNT_VALID_EVENTS = 0x2,
    CNT_INVALID_EVENTS = 0x3,
    CNT_ALL_EVENTS = 0x8,
    CNT_RISING_EDGE_TRIG_B = 0xC,
    CNT_CYCLES_TRIG_B_ACTIVE = 0xF
  };
  TofpetChannelCfg& setEventCounterMode(CounterMode mode); // counter_mode
  CounterMode getEventCounterMode() const;

  // things
  enum MeasurementMode {
    MEAS_TOT = 0x0,
    MEAS_QDC = 0x1,
  };
  TofpetChannelCfg& setMeasurementMode(MeasurementMode mode); //qdc_mode, intg_en, intg_signal_en
  MeasurementMode getMeasurementMode() const;

  // things
  
  TofpetChannelCfg& setQDCMinIntegrationTime(uint8_t time); // min_intg_time, 7 bits
  uint8_t getQDCMinIntegrationTime() const;

  TofpetChannelCfg& setQDCMaxIntegrationTime(uint8_t time); // max_intg_time, 7 bits
  uint8_t getQDCMaxIntegrationTime() const;

  enum GainQDC {
    GQDC_2_50 = 0b000, //requires v_att_diff_bias_ig to be 0b100011 (default)
    GQDC_1_00 = 0b001, //default, requires v_att_diff_bias_ig to be 0b100011 (default)
    GQDC_1_70 = 0b011, //requires v_att_diff_bias_ig to be 0b111000
    GQDC_3_65 = 0b100, //requires v_att_diff_bias_ig to be 0b100011 (default)
    GQDC_1_39 = 0b101, //requires v_att_diff_bias_ig to be 0b100011 (default)
  };
  TofpetChannelCfg& setQDCIntegrationGain(GainQDC gain); //att, but also v_att_diff_bias_ig in the global configuration
  GainQDC getQDCIntegrationGain() const;

  TofpetChannelCfg& setTiaTbaseline(uint8_t baseline); // baseline_t, 6 bit
  uint8_t getTiaTbaseline() const;

  TofpetChannelCfg& setTiaEbaseline(uint8_t baseline); // baseline_e, 3 bit
  uint8_t getTiaEbaseline() const;

  enum GainT {
    GT_3000_OHM = 0b00,
    GT_1500_OHM = 0b10,
    GT_750_OHM = 0b01,
    GT_375_OHM = 0b11,
  };
  TofpetChannelCfg& setTiaTgain(GainT gain); // postamp_gain_t
  GainT getTiaTgain() const;

  enum GainE {
    GE_300_OHM = 0b00,
    GE_150_OHM = 0b10,
    GE_75_OHM = 0b01,
    GE_38_OHM = 0b11,
  };
  TofpetChannelCfg& setTiaEgain(GainE gain); // postamp_gain_e
  GainE getTiaEgain() const;

  TofpetChannelCfg& setThresholdT1(uint8_t thr); // vth_t1, 6 bit
  uint8_t getThresholdT1() const;

  TofpetChannelCfg& setThresholdT2(uint8_t thr); // vth_t2, 6 bit
  uint8_t getThresholdT2() const;

  TofpetChannelCfg& setThresholdE(uint8_t thr); // vth_e, 6 bit
  uint8_t getThresholdE() const;

  TofpetChannelCfg& setThresholdT1Baseline(uint8_t thr); // vth_t1, 6 bit
  uint8_t getThresholdT1Baseline() const;

  TofpetChannelCfg& setThresholdT2Baseline(uint8_t thr); // vth_t2, 6 bit
  uint8_t getThresholdT2Baseline() const;

  TofpetChannelCfg& setThresholdEBaseline(uint8_t thr); // vth_e, 6 bit
  uint8_t getThresholdEBaseline() const;

  enum DoT1Delay {
    DELAY_OFF = 0b10000,
    DELAY_3_NS = 0b01101,
    DELAY_6_NS = 0b01110,
    DELAY_9_NS = 0b01111,
  };
  TofpetChannelCfg& setdoT1delay(DoT1Delay delay); // fe_delay, 5 bit, mangled
  TofpetChannelCfg& setdoT1delay(uint8_t delay);
  DoT1Delay getdoT1delay() const;

  std::vector<uint32_t> toArray() const;
  std::string toString() const { return m_confWord.to_string(); }
  json toJson() const;

  static std::vector<uint32_t> readArray(uint8_t channel);

  bool operator==(const TofpetChannelCfg& other) const { return (m_confWord == other.m_confWord) && (m_channel == other.m_channel); }
  bool operator!=(const TofpetChannelCfg& other) const { return !(*this == other); }

private:
  uint8_t m_channel;
  std::bitset<125> m_confWord;
  uint8_t m_thrT1Bl;
  uint8_t m_thrT1;
  uint8_t m_thrT2Bl;
  uint8_t m_thrT2;
  uint8_t m_thrEBl;
  uint8_t m_thrE;

  void setThrT1(); // vth_t1, 6 bit
  uint8_t getThrT1() const;

  void setThrT2(); // vth_t2, 6 bit
  uint8_t getThrT2() const;

  void setThrE(); // vth_e, 6 bit
  uint8_t getThrE() const;

};

std::ostream& operator<<(std::ostream& out, const TofpetChannelCfg::TriggerMode mode);
std::ostream& operator<<(std::ostream& out, const TofpetChannelCfg::TrigGenT triggerT);
std::ostream& operator<<(std::ostream& out, const TofpetChannelCfg::TrigGenQ triggerQ);
std::ostream& operator<<(std::ostream& out, const TofpetChannelCfg::TrigGenE triggerE);
std::ostream& operator<<(std::ostream& out, const TofpetChannelCfg::TrigGenB triggerB);
std::ostream& operator<<(std::ostream& out, const TofpetChannelCfg::DigitalDebugMode mode);
std::ostream& operator<<(std::ostream& out, const TofpetChannelCfg::CounterMode mode);
std::ostream& operator<<(std::ostream& out, const TofpetChannelCfg::MeasurementMode mode);
std::ostream& operator<<(std::ostream& out, const TofpetChannelCfg::GainQDC gain);
std::ostream& operator<<(std::ostream& out, const TofpetChannelCfg::GainT gain);
std::ostream& operator<<(std::ostream& out, const TofpetChannelCfg::GainE gain);
std::ostream& operator<<(std::ostream& out, const TofpetChannelCfg::DoT1Delay delay);

template <typename T>
std::string toString(T setting) {
  return fmt::format("{}", setting);
}

TofpetChannelCfg::TriggerMode triggerModeFromString(std::string mode);
// std::string toString(TofpetChannelCfg::TriggerMode mode);

TofpetChannelCfg::TrigGenT trigGenTFromString(std::string triggerT);
TofpetChannelCfg::TrigGenQ trigGenQFromString(std::string triggerQ);
TofpetChannelCfg::TrigGenE trigGenEFromString(std::string triggerE);
TofpetChannelCfg::TrigGenB trigGenBFromString(std::string triggerB);
// std::string toString(TofpetChannelCfg::TrigGenT triggerT);
// std::string toString(TofpetChannelCfg::TrigGenQ triggerQ);
// std::string toString(TofpetChannelCfg::TrigGenE triggerE);
// std::string toString(TofpetChannelCfg::TrigGenB triggerB);


TofpetChannelCfg::DigitalDebugMode digitalDebugModeFromString(std::string mode);
// std::string toString(TofpetChannelCfg::TrigGenT mode);

// TofpetChannelCfg::CounterMode counterModeFromString(std::string mode);

TofpetChannelCfg::MeasurementMode measurementModeFromString(std::string mode);
// std::string toString(TofpetChannelCfg::MeasurementMode mode);

TofpetChannelCfg::GainQDC gainQDCFromString(std::string gain);
// std::string toString(TofpetChannelCfg::GainQDC gain);

// TofpetChannelCfg::GainT gainTFromString(std::string gain);
TofpetChannelCfg::GainT gainTFromInt(int gain);
int toInt(TofpetChannelCfg::GainT gain);

// TofpetChannelCfg::GainE gainEFromString(std::string gain);
TofpetChannelCfg::GainE gainEFromInt(int gain);
int toInt(TofpetChannelCfg::GainE gain);

TofpetChannelCfg::DoT1Delay doT1DelayFromString(std::string delay);