#pragma once

#include <array>

#include "json.hpp"

#include "tofpet/tofpet_global_cfg.hpp"
#include "tofpet/tofpet_channel_cfg.hpp"
#include "utils/logger.hpp"

using json = nlohmann::json;

class TofpetCfg {
public:
  TofpetCfg();
  TofpetCfg(const TofpetCfg& cfg);
  TofpetCfg(json conf);
  
  TofpetGlobalCfg global;
  std::array<TofpetChannelCfg, 64> channels;

  json toJson() const;
};

void to_json(json& j, const TofpetCfg& p); 
void from_json(const json& j, TofpetCfg& p);

class StaticTofpetCfg {
public:
  static StaticTofpetCfg& getInstance() {
    static StaticTofpetCfg cfg;
    return cfg;
  }

  StaticTofpetCfg(StaticTofpetCfg const&) = delete;             // Copy construct
  StaticTofpetCfg(StaticTofpetCfg&&) = delete;                  // Move construct
  StaticTofpetCfg& operator=(StaticTofpetCfg const&) = delete;  // Copy assign
  StaticTofpetCfg& operator=(StaticTofpetCfg &&) = delete;      // Move assign

  TofpetCfg& operator[](int i) { return m_confs[i]; } // TODO add .at()?


private:
  StaticTofpetCfg() = default;
  std::array<TofpetCfg, 8> m_confs;

};



