#include <cassert>

#include "utils/logger.hpp"
#include "utils/crc.hpp"
#include "utils/bitset.hpp"

#include "tofpet_channel_cfg.hpp"

/**
 * Default values for the channel configuration words. It is taken from the software made available by PETsys and NOT from the datasheet.
 * There are a few differences with what is written in the datasheet.
 * For this reason, it does not correspond to the configuration of the TOFPET at reset but to the "ideal" configuration.
 */
static constexpr char DEFAULT_CHANNEL_CONFIG[] = "10100100100000100000000000011100000011010111010011101111111000111101000010001001000100111101010100101010111100000000000000000";

TofpetChannelCfg::TofpetChannelCfg() : TofpetChannelCfg(0) {}

TofpetChannelCfg::TofpetChannelCfg(const TofpetChannelCfg& cfg)
  : m_channel(cfg.m_channel)
  , m_confWord(cfg.m_confWord)
  , m_thrT1Bl(cfg.m_thrT1Bl)
  , m_thrT1(cfg.m_thrT2)
  , m_thrT2Bl(cfg.m_thrT2Bl)
  , m_thrT2(cfg.m_thrT2)
  , m_thrEBl(cfg.m_thrEBl)
  , m_thrE(cfg.m_thrE)
  {}

TofpetChannelCfg::TofpetChannelCfg(uint8_t channel)
  : m_channel(channel)
  , m_confWord(DEFAULT_CHANNEL_CONFIG)
  , m_thrT1Bl(62)
  , m_thrT1(62)
  , m_thrT2Bl(62)
  , m_thrT2(62)
  , m_thrEBl(62)
  , m_thrE(62) {
    // custom default settings
    // trigger disabled
    setTriggerMode(TRIG_DISABLED);
    // baseline as high as possible
    setTiaTbaseline(0);
    setTiaEbaseline(0);
    // thresholds as high as possible
    setThrT1();
    setThrT2();
    setThrE();
    // setThresholdT1Baseline(62);
    // setThresholdT1(62);
    // setThresholdT2Baseline(62);
    // setThresholdT2(62);
    // setThresholdEBaseline(62);
    // setThresholdE(62);
    setdoT1delay(DELAY_6_NS);
  }

TofpetChannelCfg::TofpetChannelCfg(uint8_t channel, std::vector<uint32_t> data, uint8_t thrT1Bl, uint8_t thrT2Bl, uint8_t thrEBl) 
: m_channel(channel)
, m_thrT1Bl(thrT1Bl)
, m_thrT2Bl(thrT2Bl)
, m_thrEBl(thrEBl) {
  uint8_t crc = data[0] & 0xFF;
  setBitSubSet(m_confWord, 0, std::bitset<24>((data[0] & 0xFFFFFF00) >> 8));
  setBitSubSet(m_confWord, 24, std::bitset<32>(data[1]));
  setBitSubSet(m_confWord, 56, std::bitset<32>(data[2]));
  setBitSubSet(m_confWord, 88, std::bitset<32>(data[3]));
  setBitSubSet(m_confWord, 120, std::bitset<5>(data[4] & 0x1F));

  if (crc != crcTofpet(m_confWord)) {
    THROWX(crc_error, "CRC error: {} != {}", crc, crcTofpet(m_confWord));
  }

  m_thrT1 = m_thrT1Bl - getThrT1();
  m_thrT2 = m_thrT2Bl - getThrT2();
  m_thrE = m_thrEBl - getThrE();

}

TofpetChannelCfg::TofpetChannelCfg(uint8_t channel, json conf) 
  : m_channel(channel)
  , m_confWord(DEFAULT_CHANNEL_CONFIG)
  // , m_thrT1Bl(0)
  // , m_thrT1(0)
  // , m_thrT2Bl(0)
  // , m_thrT2(0)
  // , m_thrEBl(0)
  // , m_thrE(0)
  {
  setTriggerMode(triggerModeFromString(conf.at("trig_mode").get<std::string>()));
  setMeasurementMode(measurementModeFromString(conf.at("meas_mode").get<std::string>()));
  setQDCMinIntegrationTime(conf.at("qdc_min_intg_t").get<uint8_t>());
  setQDCMaxIntegrationTime(conf.at("qdc_max_intg_t").get<uint8_t>());
  setTiaTbaseline(conf.at("bl_t").get<uint8_t>());
  setTiaEbaseline(conf.at("bl_e").get<uint8_t>());
  setThresholdT1Baseline(conf.at("thr_t1_bl").get<uint8_t>());
  setThresholdT2Baseline(conf.at("thr_t2_bl").get<uint8_t>());
  setThresholdEBaseline(conf.at("thr_e_bl").get<uint8_t>());
  setThresholdT1(conf.at("thr_t1").get<uint8_t>());
  setThresholdT2(conf.at("thr_t2").get<uint8_t>());
  setThresholdE(conf.at("thr_e").get<uint8_t>());
  setTiaTgain(gainTFromInt(conf.at("g_t").get<int>()));
  setTiaEgain(gainEFromInt(conf.at("g_e").get<int>()));
  setQDCIntegrationGain(gainQDCFromString(conf.at("g_q").get<std::string>()));
  setTriggerGenerationT(trigGenTFromString(conf.at("trig_t").get<std::string>()));
  setTriggerGenerationQ(trigGenQFromString(conf.at("trig_q").get<std::string>()));
  setTriggerGenerationE(trigGenEFromString(conf.at("trig_e").get<std::string>()));
  setTriggerGenerationB(trigGenBFromString(conf.at("trig_b").get<std::string>()));
  setdoT1delay(doT1DelayFromString(conf.at("do_t1_delay").get<std::string>()));
  setDeadTime(conf.at("dead_time").get<uint8_t>());
  setTacMaxAge(conf.at("tac_max_age").get<uint8_t>());
  setTacMinAge(conf.at("tac_min_age").get<uint8_t>());
  // setdoT1delay(conf.at("do_t1_delay").get<uint8_t>());
  setDigitalDebugMode(digitalDebugModeFromString(conf.at("digital_debug_mode").get<std::string>()));
}

/**
 * Sets the channel trigger mode. Can be used to disable channel if mode is set to TRIGGER_DISABLED.
 */
TofpetChannelCfg& TofpetChannelCfg::setTriggerMode(TofpetChannelCfg::TriggerMode mode) {
  std::bitset<2> newMode(mode);
  setBitSubSet(m_confWord, 0, newMode);
  DEBUG("TofpetChannelCfg({}): Trigger mode set to: {}", m_channel, mode);
  return *this;
}

/**
 * Returns the channel trigger mode.
 */
TofpetChannelCfg::TriggerMode TofpetChannelCfg::getTriggerMode() const {
  auto mode = bitSubSet<2>(m_confWord, 0);
  return static_cast<TriggerMode>(mode.to_ulong());
}

/**
 * Sets the discriminator outputs combination to be used for the generation of the T trigger.
 */
TofpetChannelCfg& TofpetChannelCfg::setTriggerGenerationT(TofpetChannelCfg::TrigGenT triggerT) {
  std::bitset<2> newMode(triggerT);
  setBitSubSet(m_confWord, 26, newMode);
  DEBUG("TofpetChannelCfg({}): T trigger generation set to: {}", m_channel, triggerT);
  return *this;
}

/**
 * Returns the discriminator outputs combination to be used for the generation of the T trigger.
 */
TofpetChannelCfg::TrigGenT TofpetChannelCfg::getTriggerGenerationT() const {
  auto triggerT = bitSubSet<2>(m_confWord, 26);
  return static_cast<TrigGenT>(triggerT.to_ulong());
}

/**
 * Sets the discriminator outputs combination to be used for the generation of the Q trigger.
 */
TofpetChannelCfg& TofpetChannelCfg::setTriggerGenerationQ(TofpetChannelCfg::TrigGenQ triggerQ) {
  std::bitset<2> newMode(triggerQ);
  setBitSubSet(m_confWord, 31, newMode);
  DEBUG("TofpetChannelCfg({}): Q trigger generation set to: {}", m_channel, triggerQ);
  return *this;
}

/**
 * Returns the discriminator outputs combination to be used for the generation of the Q trigger.
 */
TofpetChannelCfg::TrigGenQ TofpetChannelCfg::getTriggerGenerationQ() const {
  auto triggerQ = bitSubSet<2>(m_confWord, 31);
  return static_cast<TrigGenQ>(triggerQ.to_ulong());
}

/**
 * Sets the discriminator outputs combination to be used for the generation of the E trigger.
 */
TofpetChannelCfg& TofpetChannelCfg::setTriggerGenerationE(TofpetChannelCfg::TrigGenE triggerE) {
  std::bitset<3> newMode(triggerE);
  setBitSubSet(m_confWord, 28, newMode);
  DEBUG("TofpetChannelCfg({}): E trigger generation set to: {}", m_channel, triggerE);
  return *this;
}

/**
 * Returns the discriminator outputs combination to be used for the generation of the E trigger.
 */
TofpetChannelCfg::TrigGenE TofpetChannelCfg::getTriggerGenerationE() const {
  auto triggerE = bitSubSet<3>(m_confWord, 28);
  return static_cast<TrigGenE>(triggerE.to_ulong());
}

/**
 * Sets the discriminator outputs combination to be used for the generation of the B trigger.
 */
TofpetChannelCfg& TofpetChannelCfg::setTriggerGenerationB(TofpetChannelCfg::TrigGenB triggerB) {
  std::bitset<3> newMode(triggerB);
  setBitSubSet(m_confWord, 33, newMode);
  DEBUG("TofpetChannelCfg({}): B trigger generation set to: {}", m_channel, triggerB);
  return *this;
}

/**
 * Returns the discriminator outputs combination to be used for the generation of the B trigger.
 */
TofpetChannelCfg::TrigGenB TofpetChannelCfg::getTriggerGenerationB() const {
  auto triggerB = bitSubSet<3>(m_confWord, 33);
  return static_cast<TrigGenB>(triggerB.to_ulong());
}

/**
 * Set the digital debug mode of the channel, allowing to output either the discriminator outputs or the trigger signals on TX1, TX2, TX3.
 * Should be active on one channel at a time only.
 * Requires debug_mode in the global configuration to be set.
 */
TofpetChannelCfg& TofpetChannelCfg::setDigitalDebugMode(TofpetChannelCfg::DigitalDebugMode mode) {
  std::bitset<2> newMode(mode);
  setBitSubSet(m_confWord, 2, newMode);
  DEBUG("TofpetChannelCfg({}): digital debug mode set to: {}", m_channel, mode);
  return *this;
}

/**
 * Return the current channel debug output configuration.
 */
TofpetChannelCfg::DigitalDebugMode TofpetChannelCfg::getDigitalDebugMode() const {
  auto mode = bitSubSet<2>(m_confWord, 2);
  return static_cast<DigitalDebugMode>(mode.to_ulong());
}

/**
 * Sets the tac_max_age register.
 * This value must be between 0 and 31.
 */
TofpetChannelCfg& TofpetChannelCfg::setTacMaxAge(uint8_t value) {
  if (31 < value) {
    WARN("TofpetChannelCfg({}): tac_max_age must be < 32.", m_channel);
    return *this;
  }
  std::bitset<5> newValue(value);
  setBitSubSet(m_confWord, 16, newValue);
  DEBUG("TofpetChannelCfg({}): tac_max_age set to {}", m_channel, value);
  return *this;
}

/**
 * Returns the value of the tac_max_age register.
 */
uint8_t TofpetChannelCfg::getTacMaxAge() const {
  auto value = bitSubSet<5>(m_confWord, 16);
  return value.to_ulong();
}

/**
 * Sets the tac_min_age register.
 * This value must be between 0 and 31.
 */
TofpetChannelCfg& TofpetChannelCfg::setTacMinAge(uint8_t value) {
  if (31 < value) {
    WARN("TofpetChannelCfg({}): tac_min_age must be < 32.", m_channel);
    return *this;
  }
  std::bitset<5> newValue(value);
  setBitSubSet(m_confWord, 21, newValue);
  DEBUG("TofpetChannelCfg({}): tac_min_age set to {}", m_channel, value);
  return *this;
}

/**
 * Returns the value of the tac_min_age register.
 */
uint8_t TofpetChannelCfg::getTacMinAge() const {
  auto value = bitSubSet<5>(m_confWord, 21);
  return value.to_ulong();
}
  

/**
 * Sets the number of clock cycles after the deactivation of trigger_B before rearming.
 * This value must be between 0 and 63.
 */
TofpetChannelCfg& TofpetChannelCfg::setDeadTime(uint8_t time) {
  if (63 < time) {
    WARN("TofpetChannelCfg({}): dead time must be < 64.", m_channel);
    return *this;
  }
  std::bitset<6> newTime(time);
  setBitSubSet(m_confWord, 6, newTime);
  DEBUG("TofpetChannelCfg({}): dead time set to {}", m_channel, time);
  return *this;
}

/**
 * Returns the number of clock cycles after the deactivation of trigger_B before rearming.
 */
uint8_t TofpetChannelCfg::getDeadTime() const {
  auto deadTime = bitSubSet<6>(m_confWord, 6);
  return deadTime.to_ulong();
}

/**
 * Selects what is counted when the counter mode is active.
 */
TofpetChannelCfg& TofpetChannelCfg::setEventCounterMode(TofpetChannelCfg::CounterMode mode) {
  std::bitset<4> newmode(mode);
  setBitSubSet(m_confWord, 12, newmode);
  DEBUG("TofpetChannelCfg({}): counter mode set to {}", m_channel, mode);
  return *this;
}

/**
 * Returns what is counted when the counter mode is active.
 */
TofpetChannelCfg::CounterMode TofpetChannelCfg::getEventCounterMode() const {
  auto mode = bitSubSet<4>(m_confWord, 12);
  return static_cast<CounterMode>(mode.to_ulong());
}

/**
 * Sets the measurement mode to either QDC or dual TOT.
 */
TofpetChannelCfg& TofpetChannelCfg::setMeasurementMode(TofpetChannelCfg::MeasurementMode mode) {
  if (mode == MEAS_QDC) {
    m_confWord[38] = 1; // qdc_mode
    m_confWord[95] = 1; // intg_en
    m_confWord[96] = 1; // intg_signal_en
    DEBUG("TofpetChannelCfg({}): measurement mode set to {}", m_channel, mode);
  } else if (mode == MEAS_TOT) {
    m_confWord[38] = 0; // qdc_mode
    m_confWord[95] = 0; // intg_en
    m_confWord[96] = 0; // intg_signal_en
    DEBUG("TofpetChannelCfg({}): measurement mode set to {}", m_channel, mode);
  }
  else {
    WARN("TofpetChannelCfg({}): Unknown measurement mode: {}", m_channel, mode);
  }
  return *this;
}

/**
 * Return the current measurement mode.
 */
TofpetChannelCfg::MeasurementMode TofpetChannelCfg::getMeasurementMode() const {
  if (m_confWord[38] && m_confWord[95] && m_confWord[96]) {
    return MEAS_QDC;
  }
  else if (!m_confWord[38] && !m_confWord[95] && !m_confWord[96]) {
    return MEAS_TOT;
  }
  else {
    WARN("TofpetGlobalCfg: Invalid measurement mode configuration found. Returning qdc_mode.");
    return static_cast<MeasurementMode>(bool(m_confWord[38]));
  }
}

/**
 * Sets the minimum integration time for the QDC, according to the table below. Default is 34.
 * time = 0 to 15, (time) clock cycles
 * time = 16 to 31, (2*time - 16) clock cycles
 * time = 32 to 127 (4*time - 78) clock cycles
 * To get a fixed integration time set the minimum integration time equal to the maximum.
 * \param time must be <= 127 (7-bit number)
 */
TofpetChannelCfg& TofpetChannelCfg::setQDCMinIntegrationTime(uint8_t time) {
  if (127 < time) {
    WARN("TofpetChannelCfg({}): QDC minimum integration time must be < 128.", m_channel);
    return *this;
  }
  std::bitset<7> newTime(time);
  setBitSubSet(m_confWord, 40, newTime);
  DEBUG("TofpetChannelCfg({}): QDC minimum integration time set to {}", m_channel, time);
  return *this;
}

/**
 * Returns the minimum integration time for the QDC, in clock cycles (CHECK THIS).
 */
uint8_t TofpetChannelCfg::getQDCMinIntegrationTime() const {
  auto time = bitSubSet<7>(m_confWord, 40);
  return time.to_ulong();
}

/**
 * Sets the maximum integration time for the QDC, according to the table below. Default is 34.
 * time = 0 to 15, (time) clock cycles
 * time = 16 to 31, (2*time - 16) clock cycles
 * time = 32 to 127, (4*time - 78) clock cycles
 * To get a fixed integration time set the minimum integration time equal to the maximum.
 * \param time must be <= 127 (7-bit number)
 */
TofpetChannelCfg& TofpetChannelCfg::setQDCMaxIntegrationTime(uint8_t time) {
  if (127 < time) {
    WARN("TofpetChannelCfg({}): QDC maximum integration time must be < 128.", m_channel);
    return *this;
  }
  std::bitset<7> newTime(time);
  setBitSubSet(m_confWord, 47, newTime);
  DEBUG("TofpetChannelCfg({}): QDC maximum integration time set to ", m_channel, time);
  return *this;
}

/**
 * Returns the maximum integration time for the QDC, in clock cycles (CHECK THIS).
 */
uint8_t TofpetChannelCfg::getQDCMaxIntegrationTime() const {
  auto time = bitSubSet<7>(m_confWord, 47);
  return time.to_ulong();
}

/**
 * Sets the gain for the QDC.
 * For the moment the gain of 1.7 is not implemented, because requires a modification of the global configuration register.
 */
TofpetChannelCfg& TofpetChannelCfg::setQDCIntegrationGain(TofpetChannelCfg::GainQDC gain) {
  if (gain == GQDC_1_70) {
    WARN("TofpetChannelCfg({}): QDC integration gain {} not implemented yet. Requires modification to global conf as well.", m_channel, gain);
    return *this;
  }
  std::bitset<3> newGain(gain);
  setBitSubSet(m_confWord, 97, newGain);
  DEBUG("TofpetChannelCfg({}): QDC integration gain set to {}", m_channel, gain);
  return *this;
}

/**
 * Return the gain of the QDC.
 */
TofpetChannelCfg::GainQDC TofpetChannelCfg::getQDCIntegrationGain() const {
  auto gain = bitSubSet<3>(m_confWord, 97);
  return static_cast<GainQDC>(gain.to_ulong());
}

/**
 * Sets the baseline voltage for the T transimpedance amplifier.
 * This voltage is given by: V_baseline_T = (63 - baseline) * V_lsb_T1.
 * V_lsb_T1 is set in the global configuration register.
 * \param baseline must be < 64 (6 bit value)
 */
TofpetChannelCfg& TofpetChannelCfg::setTiaTbaseline(uint8_t baseline) {
  if (63 < baseline) {
    WARN("TofpetChannelCfg({}): T baseline must be < 64.", m_channel);
    return *this;
  }
  std::bitset<6> newBaseline(baseline);
  setBitSubSet(m_confWord, 57, newBaseline);
  DEBUG("TofpetChannelCfg({}): T baseline set to {}", m_channel, baseline);
  return *this;
}

/**
 * Returns the baseline voltage for the T transimpedance amplifier.
 * See setTiaTbaseline() for details on the conversion to volts.
 */
uint8_t TofpetChannelCfg::getTiaTbaseline() const {
  auto time = bitSubSet<6>(m_confWord, 57);
  return time.to_ulong();
}

/**
 * Sets the baseline voltage for the T transimpedance amplifier.
 * This voltage is given by: V_baseline_E = 8 * (7 - baseline) * V_lsb_E.
 * V_lsb_E is set in the global configuration register.
 * \param baseline must be < 8 (3 bit value)
 */
TofpetChannelCfg& TofpetChannelCfg::setTiaEbaseline(uint8_t baseline) {
  if (7 < baseline) {
    WARN("TofpetChannelCfg({}): E baseline must be < 8.", m_channel);
    return *this;
  }
  std::bitset<3> newBaseline(baseline);
  setBitSubSet(m_confWord, 81, newBaseline);
  DEBUG("TofpetChannelCfg({}): E baseline set to {}", m_channel, baseline);
  return *this;
}

/**
 * Returns the baseline voltage for the E transimpedance amplifier.
 * See setTiaEbaseline() for details on the conversion to volts.
 */
uint8_t TofpetChannelCfg::getTiaEbaseline() const {
  auto time = bitSubSet<3>(m_confWord, 81);
  return time.to_ulong();
}

/**
 * Sets the gain of the transimpedance amplifier on the T branch.
 */
TofpetChannelCfg& TofpetChannelCfg::setTiaTgain(TofpetChannelCfg::GainT gain) {
  // this is a reversed register, according to datasheet, but using the values from the datasheet, it doesn't appear to be the case
  std::bitset<2> newGain(gain);
  setBitSubSet(m_confWord, 89, newGain);
  DEBUG("TofpetChannelCfg({}): T transipedance amplifier gain set to {}", m_channel, gain);
  return *this;
}

/**
 * Returns the gain of the transimpedance amplifier on the T branch.
 */
TofpetChannelCfg::GainT TofpetChannelCfg::getTiaTgain() const {
  auto gain = bitSubSet<2>(m_confWord, 89);
  return static_cast<GainT>(gain.to_ulong());
}

/**
 * Sets the gain of the transimpedance amplifier on the E branch.
 */
TofpetChannelCfg& TofpetChannelCfg::setTiaEgain(TofpetChannelCfg::GainE gain) {
  // this is a reversed register, according to datasheet, but using the values from the datasheet, it doesn't appear to be the case
  std::bitset<2> newGain(gain);
  setBitSubSet(m_confWord, 91, newGain);
  DEBUG("TofpetChannelCfg({}): E transipedance amplifier gain set to {}", m_channel, gain);
  return *this;
}

/**
 * Returns the gain of the transimpedance amplifier on the E branch.
 */
TofpetChannelCfg::GainE TofpetChannelCfg::getTiaEgain() const {
  auto gain = bitSubSet<2>(m_confWord, 91);
  return static_cast<GainE>(gain.to_ulong());
}

TofpetChannelCfg& TofpetChannelCfg::setThresholdT1(uint8_t thr) {
  if (62 < static_cast<uint8_t>(m_thrT1Bl - thr)) {
    DEBUG("TofpetChannelCfg({}): T1 threshold (baseline - thr) must be < 63. Setting it to the maximum possible.", m_channel);
    m_thrT1 = m_thrT1Bl;
  }
  else {
    m_thrT1 = thr;
  }
  setThrT1();
  return *this;
}

uint8_t TofpetChannelCfg::getThresholdT1() const {
  return m_thrT1;
}

TofpetChannelCfg& TofpetChannelCfg::setThresholdT2(uint8_t thr) {
  if (62 < static_cast<uint8_t>(m_thrT2Bl - thr)) {
    DEBUG("TofpetChannelCfg({}): T2 threshold (baseline - thr) must be < 63. Setting it to the maximum possible.", m_channel);
    m_thrT2 = m_thrT2Bl;
  }
  else {
    m_thrT2 = thr;
  }
  setThrT2();
  return *this;
}

uint8_t TofpetChannelCfg::getThresholdT2() const {
  return m_thrT2;
}

TofpetChannelCfg& TofpetChannelCfg::setThresholdE(uint8_t thr) {
  if (62 < static_cast<uint8_t>(m_thrEBl - thr)) {
    DEBUG("TofpetChannelCfg({}): E threshold (baseline - thr) must be < 63. Setting it to the maximum possible.", m_channel);
    m_thrE = m_thrEBl;
  }
  else {
    m_thrE = thr;
  }
  setThrE();
  return *this;
}

uint8_t TofpetChannelCfg::getThresholdE() const {
  return m_thrE;
}

TofpetChannelCfg& TofpetChannelCfg::setThresholdT1Baseline(uint8_t thr) {
  m_thrT1Bl = thr;
  if (62 < static_cast<uint8_t>(thr - m_thrT1)) {
    DEBUG("TofpetChannelCfg({}): T1 threshold (baseline - thr) must be < 63. Setting it to the maximum possible.", m_channel);
    m_thrT1 = thr;
  }
  setThrT1();
  return *this;
}

uint8_t TofpetChannelCfg::getThresholdT1Baseline() const {
  return m_thrT1Bl;
}

TofpetChannelCfg& TofpetChannelCfg::setThresholdT2Baseline(uint8_t thr) {
  m_thrT2Bl = thr;
  if (62 < static_cast<uint8_t>(thr - m_thrT2)) {
    DEBUG("TofpetChannelCfg({}): T2 threshold (baseline - thr) must be < 63. Setting it to the maximum possible.", m_channel);
    m_thrT2 = thr;
  }
  setThrT2();
  return *this;
}

uint8_t TofpetChannelCfg::getThresholdT2Baseline() const {
  return m_thrT2Bl;
}

TofpetChannelCfg& TofpetChannelCfg::setThresholdEBaseline(uint8_t thr) {
  m_thrEBl = thr;
  if (62 < static_cast<uint8_t>(thr - m_thrE)) {
    DEBUG("TofpetChannelCfg({}): E threshold (baseline - thr) must be < 63. Setting it to the maximum possible.", m_channel);
    m_thrE = thr;
  }
  setThrE();
  return *this;
}

uint8_t TofpetChannelCfg::getThresholdEBaseline() const {
  return m_thrEBl;
}

/**
 * Sets the threshold voltage for the T1 discriminator.
 * This voltage is given by: V_th_T1 = (63 - thr) * V_lsb_T1.
 * V_lsb_T1 is set in the global configuration register.
 * \param baseline must be < 64 (6 bit value)
 */
void TofpetChannelCfg::setThrT1() {
  uint8_t thr = m_thrT1Bl - m_thrT1;
  if (62 < thr) {
    WARN("TofpetChannelCfg({}): T1 threshold must be < 63. Trying to set to  {} - {} = {}", m_channel, m_thrT1Bl, m_thrT1, thr);
    return;
  }
  std::bitset<6> newThr(thr);
  setBitSubSet(m_confWord, 63, newThr);
  DEBUG("TofpetChannelCfg({}): T1 threshold set to {}", m_channel, thr);
}

/**
 * Returns the threshold voltage for the T1 discriminator.
 * See setThresholdT1() for details on the conversion to volts.
 */
uint8_t TofpetChannelCfg::getThrT1() const {
  auto thr = bitSubSet<6>(m_confWord, 63);
  return thr.to_ulong();
}

/**
 * Sets the threshold voltage for the T2 discriminator.
 * This voltage is given by: V_th_T2 = (63 - thr) * V_lsb_T2.
 * V_lsb_T2 is set in the global configuration register.
 * \param baseline must be < 64 (6 bit value)
 */
void TofpetChannelCfg::setThrT2() {
  uint8_t thr = m_thrT2Bl - m_thrT2;
  if (62 < thr) {
    WARN("TofpetChannelCfg({}): T2 threshold must be < 63. Trying to set to {} - {} = {}", m_channel, m_thrT2Bl, m_thrT2, thr);
    return;
  }
  std::bitset<6> newThr(thr);
  setBitSubSet(m_confWord, 69, newThr);
  DEBUG("TofpetChannelCfg({}): T2 threshold set to {}", m_channel, thr);
}

/**
 * Returns the threshold voltage for the T2 discriminator.
 * See setThresholdT2() for details on the conversion to volts.
 */
uint8_t TofpetChannelCfg::getThrT2() const {
  auto thr = bitSubSet<6>(m_confWord, 69);
  return thr.to_ulong();
}

/**
 * Sets the threshold voltage for the E discriminator.
 * This voltage is given by: V_th_E = (63 - thr) * V_lsb_E.
 * V_lsb_E is set in the global configuration register.
 * \param baseline must be < 64 (6 bit value)
 */
void TofpetChannelCfg::setThrE() {
  uint8_t thr = m_thrEBl - m_thrE;
  if (62 < thr) {
    WARN("TofpetChannelCfg({}): E threshold must be < 63. Trying to set to {} - {} = {}", m_channel, m_thrEBl, m_thrE, thr);
    return;
  }
  std::bitset<6> newThr(thr);
  setBitSubSet(m_confWord, 75, newThr);
  DEBUG("TofpetChannelCfg({}): E threshold set to {}", m_channel, thr);
}

/**
 * Returns the threshold voltage for the E discriminator.
 * See setThresholdE() for details on the conversion to volts.
 */
uint8_t TofpetChannelCfg::getThrE() const {
  auto thr = bitSubSet<6>(m_confWord, 75);
  return thr.to_ulong();
}

/**
 * Sets the value of the delay line between the T1 discriminator and the trigger generator.
 */
TofpetChannelCfg& TofpetChannelCfg::setdoT1delay(TofpetChannelCfg::DoT1Delay delay) {
  std::bitset<5> newDelay(delay);
  // bit order from datasheet
  m_confWord[86] = newDelay[0];
  m_confWord[85] = newDelay[1];
  m_confWord[87] = newDelay[2];
  m_confWord[88] = newDelay[3];
  m_confWord[84] = newDelay[4];
  DEBUG("TofpetChannelCfg({}): do_T1 delay set to {}", m_channel, delay);
  return *this;
}

TofpetChannelCfg& TofpetChannelCfg::setdoT1delay(uint8_t delay) {
  std::bitset<5> newDelay(delay);
  // bit order from datasheet
  m_confWord[86] = newDelay[0];
  m_confWord[85] = newDelay[1];
  m_confWord[87] = newDelay[2];
  m_confWord[88] = newDelay[3];
  m_confWord[84] = newDelay[4];
  DEBUG("TofpetChannelCfg({}): do_T1 delay set to {}", m_channel, delay);
  return *this;
}

/**
 * Returns the value of the delay line between the T1 discriminator and the trigger generator.
 */
TofpetChannelCfg::DoT1Delay TofpetChannelCfg::getdoT1delay() const {
  uint8_t decodedDelay = 0;
  decodedDelay |= static_cast<uint8_t>(m_confWord[86]);
  decodedDelay |= static_cast<uint8_t>(m_confWord[85]) << 1;
  decodedDelay |= static_cast<uint8_t>(m_confWord[87]) << 2;
  decodedDelay |= static_cast<uint8_t>(m_confWord[88]) << 3;
  decodedDelay |= static_cast<uint8_t>(m_confWord[84]) << 4;
  return static_cast<DoT1Delay>(decodedDelay);
}

std::vector<uint32_t> TofpetChannelCfg::toArray() const {
  std::vector<uint32_t> ret(8, 0);
  std::bitset<136> fullConfWord;
  setBitSubSet(fullConfWord, 0, m_confWord);
  setBitSubSet(fullConfWord, 125, std::bitset<6>(m_channel));
  auto crc = crcTofpet(fullConfWord);

  ret[7] = bitSubSet<32>(fullConfWord, 104).to_ulong();
  ret[6] = bitSubSet<32>(fullConfWord, 72).to_ulong();
  ret[5] = bitSubSet<32>(fullConfWord, 40).to_ulong();
  ret[4] = bitSubSet<32>(fullConfWord, 8).to_ulong();
  ret[3] = bitSubSet<8>(fullConfWord, 0).to_ulong() << 24;
  ret[3] |= crc << 16;

  return ret;
}

json TofpetChannelCfg::toJson() const {
  json conf;

  conf["bl_t"] = getTiaTbaseline();
  conf["bl_e"] = getTiaEbaseline();
  conf["thr_t1_bl"] = getThresholdT1Baseline();
  conf["thr_t2_bl"] = getThresholdT2Baseline();
  conf["thr_e_bl"] = getThresholdEBaseline();
  conf["thr_t1"] = getThresholdT1();
  conf["thr_t2"] = getThresholdT2();
  conf["thr_e"] = getThresholdE();
  conf["trig_mode"] = ::toString(getTriggerMode());
  conf["meas_mode"] = ::toString(getMeasurementMode());
  conf["qdc_max_intg_t"] = getQDCMaxIntegrationTime();
  conf["qdc_min_intg_t"] = getQDCMinIntegrationTime();
  conf["g_t"] = toInt(getTiaTgain());
  conf["g_e"] = toInt(getTiaEgain());
  conf["g_q"] = ::toString(getQDCIntegrationGain());
  conf["trig_t"] = ::toString(getTriggerGenerationT());
  conf["trig_q"] = ::toString(getTriggerGenerationQ());
  conf["trig_e"] = ::toString(getTriggerGenerationE());
  conf["trig_b"] = ::toString(getTriggerGenerationB());
  conf["do_t1_delay"] = ::toString(getdoT1delay());
  conf["dead_time"] = getDeadTime();
  conf["tac_max_age"] = getTacMaxAge();
  conf["tac_min_age"] = getTacMinAge();
  // conf["do_t1_delay"] = static_cast<uint8_t>(getdoT1delay());
  conf["digital_debug_mode"] = ::toString(getDigitalDebugMode());

  return conf;
}

std::vector<uint32_t> TofpetChannelCfg::readArray(uint8_t channel) {
  std::vector<uint32_t> ret(8, 0);
  std::bitset<11> req(channel | 0x80);
  DEBUG("Read request w/o CRC: {}", req);
  auto crc = crcTofpet(req);
  ret[7] = (req.to_ulong() << 21) | (crc << 13);
  DEBUG("read request: {:#x}", ret[7]);
  return ret;
}



std::ostream& operator<<(std::ostream& out, const TofpetChannelCfg::TriggerMode mode) {
  switch (mode) {
  case TofpetChannelCfg::TRIG_NORMAL:
    return out << "NORMAL";
  case TofpetChannelCfg::TRIG_INVERTED:
    return out << "INVERTED";
  case TofpetChannelCfg::TRIG_TEST_PULSE:
    return out << "TEST_PULSE";
  case TofpetChannelCfg::TRIG_DISABLED:
    return out << "DISABLED";
  default:
    return out;
  }
}

std::ostream& operator<<(std::ostream& out, const TofpetChannelCfg::TrigGenT triggerT) {
  switch (triggerT) {
  case TofpetChannelCfg::T_DO_T1:
    return out << "T1";
  case TofpetChannelCfg::T_DO_T1_AND_DO_T2:
    return out << "T1_AND_T2";
  case TofpetChannelCfg::T_DO_T1_AND_DO_E:
    return out << "T1_AND_E";
  case TofpetChannelCfg::T_DO_E:
    return out << "E";
  default:
    return out;
  }
}

std::ostream& operator<<(std::ostream& out, const TofpetChannelCfg::TrigGenQ triggerQ) {
  switch (triggerQ) {
  case TofpetChannelCfg::Q_DO_T1:
    return out << "T1";
  case TofpetChannelCfg::Q_DO_T2:
    return out << "T2";
  case TofpetChannelCfg::Q_DO_E:
    return out << "E";
  default:
    return out;
  }
}

std::ostream& operator<<(std::ostream& out, const TofpetChannelCfg::TrigGenE triggerE) {
  switch (triggerE) {
  case TofpetChannelCfg::E_NOT_DO_T1:
    return out << "NOT_T1";
  case TofpetChannelCfg::E_NOT_DO_T2:
    return out << "NOT_T2";
  case TofpetChannelCfg::E_NOT_DO_E:
    return out << "NOT_E";
  case TofpetChannelCfg::E_NOT_DO_T1_AND_DO_T2:
    return out << "NOT_T1_AND_T2";
  case TofpetChannelCfg::E_NOT_DO_T1_AND_DO_E:
    return out << "NOT_T1_AND_E";
  case TofpetChannelCfg::E_DO_T1:
    return out << "T1";
  case TofpetChannelCfg::E_DO_T2:
    return out << "T2";
  case TofpetChannelCfg::E_DO_E:
    return out << "E";
  default:
    return out;
  }
}

std::ostream& operator<<(std::ostream& out, const TofpetChannelCfg::TrigGenB triggerB) {
  switch (triggerB) {
  case TofpetChannelCfg::B_DO_T1:
    return out << "T1";
  case TofpetChannelCfg::B_DO_T2:
    return out << "T2";
  case TofpetChannelCfg::B_DO_E:
    return out << "E";
  case TofpetChannelCfg::B_DO_T1_OR_DO_T2:
    return out << "T1_OR_T2";
  case TofpetChannelCfg::B_DO_T1_OR_DO_E:
    return out << "T1_OR_E";
  case TofpetChannelCfg::B_DO_T1_OR_DO_T2_OR_DO_E:
    return out << "T1_OR_T2_OR_E";
  default:
    return out;
  }
}

std::ostream& operator<<(std::ostream& out, const TofpetChannelCfg::DigitalDebugMode mode) {
  switch (mode) {
  case TofpetChannelCfg::DBG_DISABLED:
    return out << "DISABLED";
  case TofpetChannelCfg::DBG_DO_X:
    return out << "DO_X";
  case TofpetChannelCfg::DBG_TRIGGER_X:
    return out << "TRIGGER_X";
  default:
    return out;
  }
}

std::ostream& operator<<(std::ostream& out, const TofpetChannelCfg::CounterMode mode) {
  switch (mode) {
  case TofpetChannelCfg::CNT_DISABLED:
    return out << "CNT_DISABLED";
  case TofpetChannelCfg::CNT_ALWAYS:
    return out << "CNT_ALWAYS";
  case TofpetChannelCfg::CNT_VALID_EVENTS:
    return out << "CNT_VALID_EVENTS";
  case TofpetChannelCfg::CNT_INVALID_EVENTS:
    return out << "CNT_INVALID_EVENTS";
  case TofpetChannelCfg::CNT_ALL_EVENTS:
    return out << "CNT_ALL_EVENTS";
  case TofpetChannelCfg::CNT_RISING_EDGE_TRIG_B:
    return out << "CNT_RISING_EDGE_TRIG_B";
  case TofpetChannelCfg::CNT_CYCLES_TRIG_B_ACTIVE:
    return out << "CNT_CYCLES_TRIG_B_ACTIVE";
  default:
    return out;
  }
}

std::ostream& operator<<(std::ostream& out, const TofpetChannelCfg::MeasurementMode mode) {
  switch (mode) {
  case TofpetChannelCfg::MEAS_QDC:
    return out << "QDC";
  case TofpetChannelCfg::MEAS_TOT:
    return out << "TOT";
  default:
    return out;
  }
}

std::ostream& operator<<(std::ostream& out, const TofpetChannelCfg::GainQDC gain) {
  switch (gain) {
  case TofpetChannelCfg::GQDC_2_50:
    return out << "2_50";
  case TofpetChannelCfg::GQDC_1_00:
    return out << "1_00";
  case TofpetChannelCfg::GQDC_1_70:
    return out << "1_70";
  case TofpetChannelCfg::GQDC_3_65:
    return out << "3_65";
  case TofpetChannelCfg::GQDC_1_39:
    return out << "1_39";
  default:
    return out;
  }
}

std::ostream& operator<<(std::ostream& out, const TofpetChannelCfg::GainT gain) {
  switch (gain) {
  case TofpetChannelCfg::GT_3000_OHM:
    return out << "GT_3000_OHM";
  case TofpetChannelCfg::GT_1500_OHM:
    return out << "GT_1500_OHM";
  case TofpetChannelCfg::GT_750_OHM:
    return out << "GT_750_OHM";
  case TofpetChannelCfg::GT_375_OHM:
    return out << "GT_375_OHM";
  default:
    return out;
  }
}

std::ostream& operator<<(std::ostream& out, const TofpetChannelCfg::GainE gain) {
  switch (gain) {
  case TofpetChannelCfg::GE_300_OHM:
    return out << "GE_300_OHM";
  case TofpetChannelCfg::GE_150_OHM:
    return out << "GE_150_OHM";
  case TofpetChannelCfg::GE_75_OHM:
    return out << "GE_75_OHM";
  case TofpetChannelCfg::GE_38_OHM:
    return out << "GE_38_OHM";
  default:
    return out;
  }
}

std::ostream& operator<<(std::ostream& out, const TofpetChannelCfg::DoT1Delay delay) {
  switch (delay) {
  case TofpetChannelCfg::DELAY_OFF:
    return out << "OFF";
  case TofpetChannelCfg::DELAY_3_NS:
    return out << "3_NS";
  case TofpetChannelCfg::DELAY_6_NS:
    return out << "6_NS";
  case TofpetChannelCfg::DELAY_9_NS:
    return out << "9_NS";
  default:
    return out;
  }
}

TofpetChannelCfg::TriggerMode triggerModeFromString(std::string mode) {
  std::unordered_map<std::string, TofpetChannelCfg::TriggerMode> map{ 
    {"NORMAL", TofpetChannelCfg::TRIG_NORMAL},
    {"TEST_PULSE", TofpetChannelCfg::TRIG_TEST_PULSE},
    {"INVERTED", TofpetChannelCfg::TRIG_INVERTED},
    {"DISABLED", TofpetChannelCfg::TRIG_DISABLED}
  };

  auto it = map.find(mode);
  if (it != map.end()) {
      return it->second;
  }
  else {
      THROW_IA("Unknown trigger mode: " + mode);
  }
}


// std::string toString(TofpetChannelCfg::TriggerMode mode) {
//   std::stringstream tm;
//   tm << mode;
//   return tm.str();
// }

TofpetChannelCfg::TrigGenT trigGenTFromString(std::string triggerT) {
  std::unordered_map<std::string, TofpetChannelCfg::TrigGenT> map{ 
    {"T1", TofpetChannelCfg::T_DO_T1},
    {"T1_AND_T2", TofpetChannelCfg::T_DO_T1_AND_DO_T2},
    {"T1_AND_E", TofpetChannelCfg::T_DO_T1_AND_DO_E},
    {"E", TofpetChannelCfg::T_DO_E},
    {"t1", TofpetChannelCfg::T_DO_T1},
    {"t1_and_t2", TofpetChannelCfg::T_DO_T1_AND_DO_T2},
    {"t1_and_e", TofpetChannelCfg::T_DO_T1_AND_DO_E},
    {"e", TofpetChannelCfg::T_DO_E},
  };

  auto it = map.find(triggerT);
  if (it != map.end()) {
      return it->second;
  }
  else {
      THROW_IA("Unknown trigger T: " + triggerT);
  }
}

TofpetChannelCfg::TrigGenQ trigGenQFromString(std::string triggerQ) {
  std::unordered_map<std::string, TofpetChannelCfg::TrigGenQ> map{ 
    {"T1", TofpetChannelCfg::Q_DO_T1},
    {"T2", TofpetChannelCfg::Q_DO_T2},
    {"E", TofpetChannelCfg::Q_DO_E},
    {"t1", TofpetChannelCfg::Q_DO_T1},
    {"t2", TofpetChannelCfg::Q_DO_T2},
    {"e", TofpetChannelCfg::Q_DO_E},
  };

  auto it = map.find(triggerQ);
  if (it != map.end()) {
      return it->second;
  }
  else {
      THROW_IA("Unknown trigger Q: " + triggerQ);
  }
}

TofpetChannelCfg::TrigGenE trigGenEFromString(std::string triggerE) {
  std::unordered_map<std::string, TofpetChannelCfg::TrigGenE> map{ 
    {"NOT_T1", TofpetChannelCfg::E_NOT_DO_T1},
    {"NOT_T2", TofpetChannelCfg::E_NOT_DO_T2},
    {"NOT_E", TofpetChannelCfg::E_NOT_DO_E},
    {"NOT_T1_AND_T2", TofpetChannelCfg::E_NOT_DO_T1_AND_DO_T2},
    {"NOT_T1_AND_E", TofpetChannelCfg::E_NOT_DO_T1_AND_DO_E},
    {"T1", TofpetChannelCfg::E_DO_T1},
    {"T2", TofpetChannelCfg::E_DO_T2},
    {"E", TofpetChannelCfg::E_DO_E},
    {"not_t1", TofpetChannelCfg::E_NOT_DO_T1},
    {"not_t2", TofpetChannelCfg::E_NOT_DO_T2},
    {"not_e", TofpetChannelCfg::E_NOT_DO_E},
    {"not_t1_and_t2", TofpetChannelCfg::E_NOT_DO_T1_AND_DO_T2},
    {"not_t1_and_e", TofpetChannelCfg::E_NOT_DO_T1_AND_DO_E},
    {"t1", TofpetChannelCfg::E_DO_T1},
    {"t2", TofpetChannelCfg::E_DO_T2},
    {"e", TofpetChannelCfg::E_DO_E},
  };

  auto it = map.find(triggerE);
  if (it != map.end()) {
      return it->second;
  }
  else {
      THROW_IA("Unknown trigger E: " + triggerE);
  }
}

TofpetChannelCfg::TrigGenB trigGenBFromString(std::string triggerB) {
  std::unordered_map<std::string, TofpetChannelCfg::TrigGenB> map{ 
    {"T1", TofpetChannelCfg::B_DO_T1},
    {"T2", TofpetChannelCfg::B_DO_T2},
    {"E", TofpetChannelCfg::B_DO_E},
    {"T1_OR_T2", TofpetChannelCfg::B_DO_T1_OR_DO_T2},
    {"T1_OR_E", TofpetChannelCfg::B_DO_T1_OR_DO_E},
    {"T1_OR_T2_OR_E", TofpetChannelCfg::B_DO_T1_OR_DO_T2_OR_DO_E},
    {"t1", TofpetChannelCfg::B_DO_T1},
    {"t2", TofpetChannelCfg::B_DO_T2},
    {"e", TofpetChannelCfg::B_DO_E},
    {"t1_or_t2", TofpetChannelCfg::B_DO_T1_OR_DO_T2},
    {"t1_or_e", TofpetChannelCfg::B_DO_T1_OR_DO_E},
    {"t1_or_t2_or_e", TofpetChannelCfg::B_DO_T1_OR_DO_T2_OR_DO_E},
  };

  auto it = map.find(triggerB);
  if (it != map.end()) {
      return it->second;
  }
  else {
      THROW_IA("Unknown trigger B: " + triggerB);
  }
}

TofpetChannelCfg::DigitalDebugMode digitalDebugModeFromString(std::string mode) {
  std::unordered_map<std::string, TofpetChannelCfg::DigitalDebugMode> map{ 
    {"DISABLED", TofpetChannelCfg::DBG_DISABLED},
    {"DO_X", TofpetChannelCfg::DBG_DO_X},
    {"TRIGGER_X", TofpetChannelCfg::DBG_TRIGGER_X},
    {"disabled", TofpetChannelCfg::DBG_DISABLED},
    {"do_x", TofpetChannelCfg::DBG_DO_X},
    {"trigger_x", TofpetChannelCfg::DBG_TRIGGER_X},
  };

  auto it = map.find(mode);
  if (it != map.end()) {
      return it->second;
  }
  else {
      THROW_IA("Unknown digital debug mode: " + mode);
  }
}

// std::string toString(TofpetChannelCfg::DigitalDebugMode mode) {
//   std::stringstream tm;
//   tm << mode;
//   return tm.str();
// }

// TofpetChannelCfg::CounterMode counterModeFromString(std::string mode) {

// }

TofpetChannelCfg::MeasurementMode measurementModeFromString(std::string mode) {
  std::unordered_map<std::string, TofpetChannelCfg::MeasurementMode> map{ 
    {"QDC", TofpetChannelCfg::MEAS_QDC},
    {"TOT", TofpetChannelCfg::MEAS_TOT},
    {"qdc", TofpetChannelCfg::MEAS_QDC},
    {"tot", TofpetChannelCfg::MEAS_TOT},
  };

  auto it = map.find(mode);
  if (it != map.end()) {
      return it->second;
  }
  else {
      THROW_IA("Unknown measurement mode: " + mode);
  }
}

// std::string toString(TofpetChannelCfg::MeasurementMode mode) {
//   std::stringstream tm;
//   tm << mode;
//   return tm.str();
// }

TofpetChannelCfg::GainQDC gainQDCFromString(std::string gain) {
  std::unordered_map<std::string, TofpetChannelCfg::GainQDC> map{ 
    {"2_50", TofpetChannelCfg::GQDC_2_50},
    {"1_00", TofpetChannelCfg::GQDC_1_00},
    {"1_70", TofpetChannelCfg::GQDC_1_70},
    {"3_65", TofpetChannelCfg::GQDC_3_65},
    {"1_39", TofpetChannelCfg::GQDC_1_39},
  };

  auto it = map.find(gain);
  if (it != map.end()) {
      return it->second;
  }
  else {
      THROW_IA("Unknown QDC gain: " + gain);
  }
}

// std::string toString(TofpetChannelCfg::GainQDC gain) {
//   std::stringstream tm;
//   tm << gain;
//   return tm.str();
// }

// TofpetChannelCfg::GainT gainTFromString(std::string gain) {

// }

TofpetChannelCfg::GainT gainTFromInt(int gain) {
  switch (gain) {
  case 3000:
    return TofpetChannelCfg::GT_3000_OHM;
  case 1500:
    return TofpetChannelCfg::GT_1500_OHM;
  case 750:
    return TofpetChannelCfg::GT_750_OHM;
  case 375:
    return TofpetChannelCfg::GT_375_OHM;
  default:
    THROW_IA("Unknown T gain: " + gain);
  }
}

int toInt(TofpetChannelCfg::GainT gain) {
  switch (gain) {
  case TofpetChannelCfg::GT_3000_OHM:
    return 3000;
  case TofpetChannelCfg::GT_1500_OHM:
    return 1500;
  case TofpetChannelCfg::GT_750_OHM:
    return 750;
  case TofpetChannelCfg::GT_375_OHM:
    return 375;
  default:
    return -1;
  }
}

// TofpetChannelCfg::GainE gainEFromString(std::string gain) {

// }

TofpetChannelCfg::GainE gainEFromInt(int gain) {
  switch (gain) {
  case 300:
    return TofpetChannelCfg::GE_300_OHM;
    break;
  case 150:
    return TofpetChannelCfg::GE_150_OHM;
    break;
  case 75:
    return TofpetChannelCfg::GE_75_OHM;
    break;
  case 38:
    return TofpetChannelCfg::GE_38_OHM;
    break;
  default:
    THROW_IA("Unknown E gain: " + gain);
    break;
  }
}

int toInt(TofpetChannelCfg::GainE gain) {
  switch (gain) {
  case TofpetChannelCfg::GE_300_OHM:
    return 300;
  case TofpetChannelCfg::GE_150_OHM:
    return 150;
  case TofpetChannelCfg::GE_75_OHM:
    return 75;
  case TofpetChannelCfg::GE_38_OHM:
    return 38;
  default:
    return -1;
  }
}

TofpetChannelCfg::DoT1Delay doT1DelayFromString(std::string delay) {
   std::unordered_map<std::string, TofpetChannelCfg::DoT1Delay> map{ 
    {"OFF", TofpetChannelCfg::DELAY_OFF},
    {"3_NS", TofpetChannelCfg::DELAY_3_NS},
    {"6_NS", TofpetChannelCfg::DELAY_6_NS},
    {"9_NS", TofpetChannelCfg::DELAY_9_NS},
    {"off", TofpetChannelCfg::DELAY_OFF},
    {"3_ns", TofpetChannelCfg::DELAY_3_NS},
    {"6_ns", TofpetChannelCfg::DELAY_6_NS},
    {"9_ns", TofpetChannelCfg::DELAY_9_NS},
  };

  auto it = map.find(delay);
  if (it != map.end()) {
      return it->second;
  }
  else {
      THROW_IA("Unknown do T1 delay: " + delay);
  }
}
