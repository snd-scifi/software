#pragma once

#include <vector>
#include <bitset>
#include <cstdint>
#include <iostream>

#include "json.hpp"

#include "utils/logger.hpp"

using json = nlohmann::json;

/**
 * Class representing the global configuration of the TOFPET ASIC.
 */
class TofpetGlobalCfg {
public:
  TofpetGlobalCfg();
  TofpetGlobalCfg(const TofpetGlobalCfg& cfg);
  TofpetGlobalCfg(std::vector<uint32_t> data);
  TofpetGlobalCfg(json conf);

  TofpetGlobalCfg& setActiveLinks(int links); //tx_nlinks
  int getActiveLinks() const;

  TofpetGlobalCfg& setLinkDDR(bool ddr); //tx_ddr
  bool getLinkDDR() const;

  enum LinkMode {
    RECV_TRAINING_PATTERN_0 = 0b00, ///< sequences of 0101010101
    RECV_TRAINING_PATTERN_1 = 0b01, ///< sequences of 0000011111
    RECV_NORMAL_DATA = 0b10 ///< normal data
  };
  TofpetGlobalCfg& setLinkMode(LinkMode mode); //tx_mode
  LinkMode getLinkMode() const;

  TofpetGlobalCfg& setDigitalDebugOutputEnable(bool enable); //debug_mode
  bool getDigitalDebugOutputEnabled() const;
  
  enum VetoMode {
    VETO_DISABLED = 0b000000,
    VETO_SS = 0b000001,
    VETO_TEST_PULSE = 0b000010,
    VETO_SS_TEST_PULSE = 0b000011
  };
  TofpetGlobalCfg& setVetoMode(VetoMode mode); //veto_mode
  VetoMode getVetoMode() const; 
  
  
  TofpetGlobalCfg& setClockDivider2TDC(bool divide); // tdc_clk_div
  bool getClockDivider2TDC() const;
  
  //r_clk_en always to 0b110
  // 2 unused bits
  //stop_ramp_en, 2 bits

  TofpetGlobalCfg& setEventCounterEnabled(bool enabled); //counter_en
  bool getEventCounterEnabled() const; 

  enum CounterPeriod {
    PERIOD_2_10 = 0b000, ///< 2^10 clock cycles
    PERIOD_2_12 = 0b001, ///< 2^12 clock cycles
    PERIOD_2_14 = 0b010, ///< 2^14 clock cycles
    PERIOD_2_16 = 0b011, ///< 2^16 clock cycles
    PERIOD_2_18 = 0b100, ///< 2^18 clock cycles
    PERIOD_2_20 = 0b101, ///< 2^20 clock cycles
    PERIOD_2_22 = 0b110, ///< 2^22 clock cycles (default)
    PERIOD_2_24 = 0b111, ///< 2^24 clock cycles
  };
  TofpetGlobalCfg& setEventCounterPeriod(CounterPeriod period);
  CounterPeriod getEventCounterPeriod() const;

  TofpetGlobalCfg& setTacRefreshEnabled(bool enabled); //tac_refresh_en
  bool getTacRefreshEnabled() const;

  TofpetGlobalCfg& setTacRefreshPeriod(uint8_t value); //tac_refresh_period
  uint8_t getTacRefreshPeriod() const; 

  //data_clk_div
  //1 unused bit
  //fetp_enable
  //TofpetGlobalCfg& setInputPolarity(bool polarity); //????
  //bunch of undocumented things

  TofpetGlobalCfg& setLSBthrT1(uint8_t value); // disc_lsb_t1 0 (or 40??) to 62, NOT 63, 6 bits
  uint8_t getLSBthrT1() const;

  //things

  TofpetGlobalCfg& setLSBthrE(uint8_t value); // disc_lsb_e
  uint8_t getLSBthrE() const;

  //things

  TofpetGlobalCfg& setLSBthrT2(uint8_t value); // disc_lsb_t2
  uint8_t getLSBthrT2() const;

  //things

  TofpetGlobalCfg& setAnalogDebugOutputEnable(bool enable); //modifies adebug_out_mode and adebug_buffer
  bool getAnalogDebugOutputEnable() const;
  //tdc global dac
  //things

  TofpetGlobalCfg& setFeIb1(uint8_t value); // fe_ib1
  uint8_t getFeIb1() const;

  TofpetGlobalCfg& setFeIb2(uint8_t value); // fe_ib2, fe_ib2_x2
  uint8_t getFeIb2() const;

  TofpetGlobalCfg& setImirrorBiasTop(uint8_t value); // imirror_bias_top, 5 bit, 83, reversed, suggested 23-30 (23, 26, 29, 30)
  uint8_t getImirrorBiasTop() const;

  std::vector<uint32_t> toArray() const; //converts to array, with CRC and in the right position to be written in the FPGA registers
  std::string toString() const { return m_confWord.to_string(); }
  json toJson() const;
  void resetDefault();

  static std::vector<uint32_t> readArray();

  bool operator==(const TofpetGlobalCfg& other) const { return m_confWord == other.m_confWord; }
  bool operator!=(const TofpetGlobalCfg& other) const { return !(*this == other); }

private:
  void setLsbThr(uint8_t value, size_t offset);
  uint8_t getLsbThr(size_t offset) const;
  std::bitset<184> m_confWord;

};

std::ostream& operator<<(std::ostream& out, const TofpetGlobalCfg::LinkMode mode);
std::ostream& operator<<(std::ostream& out, const TofpetGlobalCfg::VetoMode mode);
std::ostream& operator<<(std::ostream& out, const TofpetGlobalCfg::CounterPeriod period);

TofpetGlobalCfg::LinkMode linkModeFromString(std::string mode);
TofpetGlobalCfg::VetoMode vetoModeFromString(std::string mode);
TofpetGlobalCfg::CounterPeriod counterPeriodFromString(std::string period);