#include <cassert>

#include "utils/logger.hpp"
#include "utils/crc.hpp"

#include "tofpet_cfg.hpp"


// TODO global and channels should be initialized with : ...
TofpetCfg::TofpetCfg() {
  global = TofpetGlobalCfg();
  for (uint8_t i = 0; i < 64; i++) {
    channels[i] = TofpetChannelCfg(i);
  }
}

TofpetCfg::TofpetCfg(const TofpetCfg& cfg) : global(cfg.global), channels(cfg.channels) {}

TofpetCfg::TofpetCfg(json conf) {
  global = TofpetGlobalCfg(conf.at("global"));
  for (uint8_t i = 0; i < 64; i++) {
    channels[i] = TofpetChannelCfg(i, conf.at("channels").at(i));
  }
}

json TofpetCfg::toJson() const {
  json conf;
  conf["global"] = global.toJson();
  json chnls;
  for (uint8_t i = 0; i < 64; i++) {
    chnls.push_back(channels[i].toJson());
  }
  conf["channels"] = chnls;
  return conf;
}

void to_json(json& j, const TofpetCfg& cfg) {
  j = cfg.toJson();
}

void from_json(const json& j, TofpetCfg& p) {
  p = TofpetCfg(j);
}