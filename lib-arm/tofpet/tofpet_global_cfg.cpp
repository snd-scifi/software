#include <cassert>
#include <unordered_map>

#include "utils/logger.hpp"
#include "utils/crc.hpp"
#include "utils/bitset.hpp"

#include "tofpet_global_cfg.hpp"

/**
 * Default values for the global configuration words. It is taken from the software made available by PETsys and NOT from the datasheet.
 * There are a few differences with what is written in the datasheet.
 * For this reason, it does not correspond to the configuration of the TOFPET at reset but to the "ideal" configuration.
 */
static constexpr char DEFAULT_GLOBAL_CONFIG[] = "0000001011100101111101011000001111001011011111101001000100100010101110110110000001011111001001001110111011111000100000100000010111101111111001111111011000010011110000001101000000010110";

/**
 * Default constructor. Sets the number of links to 1, SDR mode and TDC clock divider to 1.
 */
TofpetGlobalCfg::TofpetGlobalCfg()
  : m_confWord(DEFAULT_GLOBAL_CONFIG) {
    // setting board-dependend configurations, should not be changed.
    setActiveLinks(1);
    setLinkDDR(false);
    setClockDivider2TDC(false);
  }


TofpetGlobalCfg::TofpetGlobalCfg(const TofpetGlobalCfg& cfg) : m_confWord(cfg.m_confWord) {}

/**
 * Constructor that builds the global configuration from the configuration word read from the ASIC.
 * data.size() must be at least 6. The data is assumed to be right-aligned.
 */
TofpetGlobalCfg::TofpetGlobalCfg(std::vector<uint32_t> data) {
  uint8_t crc = data[0] & 0xFF;
  setBitSubSet(m_confWord, 0, std::bitset<24>((data[0] & 0xFFFFFF00) >> 8));
  setBitSubSet(m_confWord, 24, std::bitset<32>(data[1]));
  setBitSubSet(m_confWord, 56, std::bitset<32>(data[2]));
  setBitSubSet(m_confWord, 88, std::bitset<32>(data[3]));
  setBitSubSet(m_confWord, 120, std::bitset<32>(data[4]));
  setBitSubSet(m_confWord, 152, std::bitset<32>(data[5]));

  if (crc != crcTofpet(m_confWord)) {
    THROWX(crc_error, "TofpetGlobalCfg: CRC error");
  }
}

/**
 * Builds the configuration from a json object.
 * It looks for all and only the elements included in toJson().
 */
TofpetGlobalCfg::TofpetGlobalCfg(json conf) : TofpetGlobalCfg() {
  setLSBthrT1(conf.at("lsb_thr_t1").get<uint8_t>());
  setLSBthrT2(conf.at("lsb_thr_t2").get<uint8_t>());
  setLSBthrE(conf.at("lsb_thr_e").get<uint8_t>());
  setTacRefreshEnabled(conf.at("tac_refresh_en").get<uint8_t>());
  setTacRefreshPeriod(conf.at("tac_refresh_period").get<uint8_t>());
  setAnalogDebugOutputEnable(conf.at("analog_debug_output").get<bool>());
  setDigitalDebugOutputEnable(conf.at("digital_debug_output").get<bool>());
  setFeIb1(conf.at("fe_ib1").get<uint8_t>());
  setFeIb2(conf.at("fe_ib2").get<uint8_t>());
  setImirrorBiasTop(conf.at("imirror_bias_top").get<uint8_t>());
}

/**
 * Sets the number of links the TOFPET2 chip will use to send data to the FPGA.
 * This should be set to 1 for the SciFi DAQ board.
 * Requires TOFPET core reset after modification.
 * \param links can be 1, 2 or 4 (default). If it is different from these, the configuration word is not changed.
 */
TofpetGlobalCfg& TofpetGlobalCfg::setActiveLinks(int links) {
  switch (links) {
  case 1:
    m_confWord.set(0, 0);
    m_confWord.set(1, 0);
    break;
  case 2:
    m_confWord.set(0, 1);
    m_confWord.set(1, 0);
    break;
  case 4:
    m_confWord.set(0, 0);
    m_confWord.set(1, 1);
    break;
  default:
    WARN("TofpetGlobalCfg: Invalid number of links: {}. Must be 1, 2 or 4.", links);
    return *this;
  }
  DEBUG("TofpetGlobalCfg: Number of links set to {}", links);
  return *this;
}

/**
 * Returns the number of active links set in the configuration word (1, 2 or 4).
 */
int TofpetGlobalCfg::getActiveLinks() const {
  auto tx_nlinks = bitSubSet<2>(m_confWord, 0);
  switch (tx_nlinks.to_ulong()) {
  case 0b00:
    return 1;
  case 0b01:
    return 2;
  case 0b10:
    return 4;
  default:
    WARN("TofpetGlobalCfg: Invalid setting for tx_nlinks: {:#b}", tx_nlinks.to_string());
    return -1;
  }
}

/**
 * Sets the data trasmission rate between the TOFPET2 and the FPGA.
 * It can be either Single Data Rate (SDR) or Double Data Rate (DDR).
 * This should be set to SDR (ddr=false) for the SciFi DAQ board.
 * Requires TOFPET core reset after modification.
 * \param ddr true to set DDR (default), false to set SDR.
 */
TofpetGlobalCfg& TofpetGlobalCfg::setLinkDDR(bool ddr) {
  m_confWord.set(2, ddr);
  DEBUG("TofpetGlobalCfg: Link data rate set to {}", ddr ? "DDR" : "SDR");
  return *this;
}

/**
 * Returns the trasmission rate.
 */
bool TofpetGlobalCfg::getLinkDDR() const {
  return m_confWord[2];
}

/**
 * Sets the data trasmission mode: either normal data or one of two training patterns: 0101010101 or 0000011111
 * \param mode The mode of transmission, normal data by default.
 */
TofpetGlobalCfg& TofpetGlobalCfg::setLinkMode(TofpetGlobalCfg::LinkMode mode) {
  std::bitset<2> newMode(mode);
  setBitSubSet(m_confWord, 3, newMode);
  DEBUG("TofpetGlobalCfg: Transmission mode set to: {}", mode);
  return *this;
}

/**
 * Returns the trasmission mode.
 */
TofpetGlobalCfg::LinkMode TofpetGlobalCfg::getLinkMode() const {
  auto mode = bitSubSet<2>(m_confWord, 3);
  return static_cast<LinkMode>(mode.to_ulong());
}

/**
 * Enables or disables the digital debug output (on tx1, tx2, tx3).
 * To be used in conjuction with the channel configuration. Disabled by default.
 * Requires TOFPET core reset after modification.
 */
TofpetGlobalCfg& TofpetGlobalCfg::setDigitalDebugOutputEnable(bool enable) {
  if (enable && getActiveLinks() != 1) {
    WARN("TofpetGlobalCfg: To enable debug mode, the number of active links must be 1");
    return *this;
  }
  m_confWord.set(5, enable);
  DEBUG("TofpetGlobalCfg: digital debug output {}", enable ? "enabled" : "disabled");
  return *this;
}

/**
 * Returns true if the debug mode is enabled.
 */
bool TofpetGlobalCfg::getDigitalDebugOutputEnabled() const {
  return m_confWord[5];
}

/**
 * Set the veto mode, i.e. inhibits trigger for all the TOFPET channels.
 * It is disabled by default.
 * Requires TOFPET core reset after modification.
 */
TofpetGlobalCfg& TofpetGlobalCfg::setVetoMode(TofpetGlobalCfg::VetoMode mode) {
  std::bitset<6> newMode(mode);
  setBitSubSet(m_confWord, 6, newMode);
  DEBUG("TofpetGlobalCfg: Veto mode set to: {}", mode);
  return *this;
}

/**
 * Returns the veto mode.
 */
TofpetGlobalCfg::VetoMode TofpetGlobalCfg::getVetoMode() const {
  auto mode = bitSubSet<6>(m_confWord, 6);
  return static_cast<VetoMode>(mode.to_ulong());
}

/**
 * Set the clock divider for the TDC.
 * Requires TOFPET core reset after modification.
 * \param divide Clock divider set to 2 if true, 1 if false (default)
 */
TofpetGlobalCfg& TofpetGlobalCfg::setClockDivider2TDC(bool divide){
  m_confWord.set(12, divide);
  DEBUG("TofpetGlobalCfg: TDC clock divider set to {}", divide ? "2" : "1");
  return *this;
}

/**
 * Returns the TDC clock divider setting
 * \return true if clock divider is 2, false if it is 1.
 */
bool TofpetGlobalCfg::getClockDivider2TDC() const {
  return m_confWord[12];
}

/**
 * Enables or disables the event counter. Disabled by default.
 * Requires TOFPET core reset after modification.
 */
TofpetGlobalCfg& TofpetGlobalCfg::setEventCounterEnabled(bool enabled){
  m_confWord.set(20, enabled);
  DEBUG("TofpetGlobalCfg: event counter {}", enabled ? "enabled" : "disabled");
  return *this;
}

/**
 * Returns true if the event counter is enabled.
 */
bool TofpetGlobalCfg::getEventCounterEnabled() const {
  return m_confWord[20];
}

/**
 * Sets the event counter period. Default is 2^22 clock cycles.
 * Requires TOFPET core reset after modification.
 */
TofpetGlobalCfg& TofpetGlobalCfg::setEventCounterPeriod(TofpetGlobalCfg::CounterPeriod period) {
  std::bitset<3> newPeriod(period);
  setBitSubSet(m_confWord, 21, newPeriod);
  DEBUG("TofpetGlobalCfg: Event counter period set to: {}", period);
  return *this;
}

/**
 * Returns The event counter period.
 */
TofpetGlobalCfg::CounterPeriod TofpetGlobalCfg::getEventCounterPeriod() const {
  auto period = bitSubSet<3>(m_confWord, 21);
  return static_cast<CounterPeriod>(period.to_ulong());
}

/**
 * Enables or disables the TAC refresh. Enabled by default.
 * Requires TOFPET core reset after modification.
 */
TofpetGlobalCfg& TofpetGlobalCfg::setTacRefreshEnabled(bool enabled){
  m_confWord.set(24, enabled);
  DEBUG("TofpetGlobalCfg: TAC refresh {}", enabled ? "enabled" : "disabled");
  return *this;
}

/**
 * Returns true if the TAC refresh is enabled.
 */
bool TofpetGlobalCfg::getTacRefreshEnabled() const {
  return m_confWord[24];
}


/**
 * Sets the tac_refresh_period register. Default value is 9.
 * Requires TOFPET core reset after modification.
 */
TofpetGlobalCfg& TofpetGlobalCfg::setTacRefreshPeriod(uint8_t value) {
  if (15 < value) {
    WARN("TofpetGlobalCfg: tac_refresh_period must be < 16. Trying to set to {}. Doing nothing.", value);
    return *this;
  }
  std::bitset<4> newValue(value);
  setBitSubSet(m_confWord, 25, newValue);
  DEBUG("TofpetGlobalCfg: tac_refresh_period set to ", value);

  return *this;
}

/**
 * Return the value of the tac_refresh_period register.
 */
uint8_t TofpetGlobalCfg::getTacRefreshPeriod() const {
  auto value = bitSubSet<4>(m_confWord, 25);
  return value.to_ulong();
}


/**
 * Helper function to set the LSB of the thresholds of T1, T2 and E.
 */
void TofpetGlobalCfg::setLsbThr(uint8_t value, size_t offset) {
  // make sure we set the right bits. Offsets from datasheet.
  assert(offset == 98 || offset == 119 || offset == 152);
  std::string which;
  switch (offset) {
  case 98:
    which = "T1";
    break;
  case 119:
    which = "E";
    break;
  case 152:
    which = "T2";
    break;
  }
  // check if the value is acceptable. If not, do nothing.
  if (value < 40 || 62 < value) {
    WARN("TofpetGlobalCfg: {} threshold LSB value must be between 40 and 62.", which);
    return;
  }
  std::bitset<6> newValue(value);
  // these bits need to be reversed.
  reverseBitset(newValue);
  setBitSubSet(m_confWord, offset, newValue);
  DEBUG("TofpetGlobalCfg: {} threshold LSB set to {}", which, value);
}

/**
 * Helper function to return the LSB of the thresholds of T1, T2 and E.
 */
uint8_t TofpetGlobalCfg::getLsbThr(size_t offset) const {
  // make sure we set the right bits. Offsets from datasheet.
  assert(offset == 98 || offset == 119 || offset == 152);
  auto value = bitSubSet<6>(m_confWord, offset);
  // these bits need to be reversed.
  reverseBitset(value);
  return value.to_ulong();
}

/**
 * Sets the LSB value of the T1 threshold DAC. A value of 40 corresponds to V_lsb_T1=~20 mV, a value of 62 to V_lsb_T1=~1mV.
 * The relation is not perfectly linear, see datasheet for details.
 * \param value must be between 40 and 62 included.
 */
TofpetGlobalCfg& TofpetGlobalCfg::setLSBthrT1(uint8_t value) {
  setLsbThr(value, 98);
  return *this;
}

/**
 * Returns the LSB value of the T1 threshold DAC (in DAC counts, so between 40 and 62).
 */
uint8_t TofpetGlobalCfg::getLSBthrT1() const {
  return getLsbThr(98);
}

/**
 * Sets the LSB value of the E threshold DAC. A value of 40 corresponds to V_lsb_E=~20 mV, a value of 62 to V_lsb_E=~1mV.
 * The relation is not perfectly linear, see datasheet for details.
 * \param value must be between 40 and 62 included.
 */
TofpetGlobalCfg& TofpetGlobalCfg::setLSBthrE(uint8_t value) {
  setLsbThr(value, 119);
  return *this;
}

/**
 * Returns the LSB value of the E threshold DAC (in DAC counts, so between 40 and 62).
 */
uint8_t TofpetGlobalCfg::getLSBthrE() const {
  return getLsbThr(119);
}

/**
 * Sets the LSB value of the T2 threshold DAC. A value of 40 corresponds to V_lsb_T2=~20 mV, a value of 62 to V_lsb_T2=~1mV.
 * The relation is not perfectly linear, see datasheet for details.
 * \param value must be between 40 and 62 included.
 */
TofpetGlobalCfg& TofpetGlobalCfg::setLSBthrT2(uint8_t value) {
  setLsbThr(value, 152);
  return *this;
}

/**
 * Returns the LSB value of the T2 threshold DAC (in DAC counts, so between 40 and 62).
 */
uint8_t TofpetGlobalCfg::getLSBthrT2() const {
  return getLsbThr(152);
}

/**
 * Enables channel 63 analog debug output.
 */
TofpetGlobalCfg& TofpetGlobalCfg::setAnalogDebugOutputEnable(bool enable) {
  if (enable) {
    m_confWord[167] = 0; // adebug_out_mode
    m_confWord[168] = 1; // adebug_out_mode
    m_confWord[175] = 0; // adebug_buffer
    DEBUG("TofpetGlobalCfg: Analog debug output enabled");
  } else {
    m_confWord[167] = 1; // adebug_out_mode
    m_confWord[168] = 1; // adebug_out_mode
    m_confWord[175] = 1; // adebug_buffer
    DEBUG("TofpetGlobalCfg: Analog debug output disabled");
  }
  return *this;
}

/**
 * Return wether the analog debug output of channel 63 is enabled.
 */
bool TofpetGlobalCfg::getAnalogDebugOutputEnable() const {
  if (!m_confWord[167] && m_confWord[168] && !m_confWord[175]) {
    return true;
  }
  else if (m_confWord[167] && m_confWord[168] && m_confWord[175]) {
    return false;
  }
  else {
    WARN("TofpetGlobalCfg: Invalid Analog debug output configuration found. Returning adebug_buffer");
    return m_confWord[175];
  }
}

TofpetGlobalCfg& TofpetGlobalCfg::setFeIb1(uint8_t value) {
  if (62 < value) {
    WARN("TofpetGlobalCfg: fe_ib1 must be < 63. Trying to set to {}. Doing nothing", value);
    return *this;
  }
  std::bitset<6> newValue(value);
  reverseBitset(newValue);
  setBitSubSet(m_confWord, 140, newValue);
  DEBUG("TofpetGlobalCfg: fe_ib1 set to ", value);

  return *this;
}

uint8_t TofpetGlobalCfg::getFeIb1() const {
  auto value = bitSubSet<6>(m_confWord, 140);
  reverseBitset(value);
  return value.to_ulong();
}

TofpetGlobalCfg& TofpetGlobalCfg::setFeIb2(uint8_t value) { //TODO verify
  if (127 < value) {
    WARN("TofpetGlobalCfg: fe_ib2 must be < 128. Trying to set to {}. Doing nothing", value);
    return *this;
  }

  std::bitset<5> fe_ib2_lsb(value & 0x1F);
  reverseBitset(fe_ib2_lsb);
  bool fe_ib2_msb = static_cast<bool>(value & 0x20);
  bool fe_ib2_x2 = static_cast<bool>(value & 0x40);

  setBitSubSet(m_confWord, 104, fe_ib2_lsb);
  m_confWord[176] = fe_ib2_msb;
  m_confWord[134] = fe_ib2_x2;
  DEBUG("TofpetGlobalCfg: fe_ib1 set to {}", getFeIb2());

  return *this;
}

uint8_t TofpetGlobalCfg::getFeIb2() const { //TODO verify
  auto fe_ib2_lsb = bitSubSet<5>(m_confWord, 104);
  reverseBitset(fe_ib2_lsb);
  uint8_t fe_ib2_msb = m_confWord[176];
  uint8_t fe_ib2_x2 = m_confWord[134];
  return (fe_ib2_x2 << 6) | (fe_ib2_msb << 5) | static_cast<uint8_t>(fe_ib2_lsb.to_ulong());
}


TofpetGlobalCfg& TofpetGlobalCfg::setImirrorBiasTop(uint8_t value) { //TODO verify
  if (value < 23 || 30 < value) {
    WARN("TofpetGlobalCfg: imirror_bias_top should be > 22 and < 31. Trying to set to {}. Doing nothing", value);
    return *this;
  }
  std::bitset<5> newValue(value);
  reverseBitset(newValue);
  setBitSubSet(m_confWord, 83, newValue);
  DEBUG("TofpetGlobalCfg: imirror_bias_top set to {}", value);

  return *this;
} // imirror_bias_top, 5 bit, 83, reversed, suggested 23-30 (23, 26, 29, 30)

uint8_t TofpetGlobalCfg::getImirrorBiasTop() const { //TODO verify
  auto value = bitSubSet<5>(m_confWord, 83);
  reverseBitset(value);
  return value.to_ulong();
}


/**
 * Returns the configuration word as 8 32-bit unsigned integers, ready to be written to the registers of the FPGA (so with the correct header and CRC).
 * \param rightAlign if true, the configuration word will be right-aligned, if false, left aligned. Left aligned is what the TOFPET "SPI" module expects.
 */
std::vector<uint32_t> TofpetGlobalCfg::toArray() const {
  std::vector<uint32_t> ret(8, 0);
  std::bitset<188> fullConfWord;
  setBitSubSet(fullConfWord, 0, m_confWord);
  fullConfWord.set(187);
  auto crc = crcTofpet(fullConfWord);

  ret[7] = bitSubSet<32>(fullConfWord, 156).to_ulong();
  ret[6] = bitSubSet<32>(fullConfWord, 124).to_ulong();
  ret[5] = bitSubSet<32>(fullConfWord, 92).to_ulong();
  ret[4] = bitSubSet<32>(fullConfWord, 60).to_ulong();
  ret[3] = bitSubSet<32>(fullConfWord, 28).to_ulong();
  ret[2] = bitSubSet<28>(fullConfWord, 0).to_ulong() << 4;
  ret[2] |= (crc & 0xF0) >> 4;
  ret[1] = (crc & 0x0F) << 28;

  return ret;
}

/**
 * Returns a JSON representation of the configuration.
 * Not all registers are included, just the ones that are reasonable to be set by the Python module and the configuration file.
*/
json TofpetGlobalCfg::toJson() const {
  json conf;
  conf["lsb_thr_t1"] = getLSBthrT1();
  conf["lsb_thr_t2"] = getLSBthrT2();
  conf["lsb_thr_e"] = getLSBthrE();
  conf["tac_refresh_en"] = getTacRefreshEnabled();
  conf["tac_refresh_period"] = getTacRefreshPeriod();
  conf["analog_debug_output"] = getAnalogDebugOutputEnable();
  conf["digital_debug_output"] = getDigitalDebugOutputEnabled();
  conf["fe_ib1"] = getFeIb1();
  conf["fe_ib2"] = getFeIb2();
  conf["imirror_bias_top"] = getImirrorBiasTop();
  return conf;
}

/**
 * Resets the configuration to the default value.
 */
void TofpetGlobalCfg::resetDefault() {
  m_confWord = std::bitset<184>(DEFAULT_GLOBAL_CONFIG);
}

std::vector<uint32_t> TofpetGlobalCfg::readArray() {
  std::vector<uint32_t> ret(8, 0);
  std::bitset<4> req("1001");
  auto crc = crcTofpet(req);
  ret[7] = (req.to_ulong() << 28) | (crc << 20);
  DEBUG("read request: {:#x}", ret[7]);
  return ret;
}


std::ostream& operator<<(std::ostream& out, const TofpetGlobalCfg::LinkMode mode) {
  switch (mode) {
  case TofpetGlobalCfg::RECV_TRAINING_PATTERN_0:
    return out << "TRAINING_PATTERN_0";
  case TofpetGlobalCfg::RECV_TRAINING_PATTERN_1:
    return out << "TRAINING_PATTERN_1";
  case TofpetGlobalCfg::RECV_NORMAL_DATA:
    return out << "NORMAL_DATA";
  default:
    return out;
  }
}

std::ostream& operator<<(std::ostream& out, const TofpetGlobalCfg::VetoMode mode) {
  switch (mode) {
  case TofpetGlobalCfg::VETO_DISABLED:
    return out << "DISABLED";
  case TofpetGlobalCfg::VETO_SS:
    return out << "SS";
  case TofpetGlobalCfg::VETO_TEST_PULSE:
    return out << "TEST_PULSE";
  case TofpetGlobalCfg::VETO_SS_TEST_PULSE:
    return out << "SS_TEST_PULSE";
  default:
    return out;
  }
}

std::ostream& operator<<(std::ostream& out, const TofpetGlobalCfg::CounterPeriod period) {
  switch (period) {
  case TofpetGlobalCfg::PERIOD_2_10:
    return out << "2^10";
  case TofpetGlobalCfg::PERIOD_2_12:
    return out << "2^12";
  case TofpetGlobalCfg::PERIOD_2_14:
    return out << "2^14";
  case TofpetGlobalCfg::PERIOD_2_16:
    return out << "2^16";
  case TofpetGlobalCfg::PERIOD_2_18:
    return out << "2^18";
  case TofpetGlobalCfg::PERIOD_2_20:
    return out << "2^20";
  case TofpetGlobalCfg::PERIOD_2_22:
    return out << "2^22";
  case TofpetGlobalCfg::PERIOD_2_24:
    return out << "2^24";
  default:
    return out;
  }
}

TofpetGlobalCfg::LinkMode linkModeFromString(std::string mode) {
  std::unordered_map<std::string, TofpetGlobalCfg::LinkMode> map{ 
    {"TRAINING_PATTERN_0", TofpetGlobalCfg::RECV_TRAINING_PATTERN_0},
    {"TRAINING_PATTERN_1", TofpetGlobalCfg::RECV_TRAINING_PATTERN_1},
    {"NORMAL_DATA", TofpetGlobalCfg::RECV_NORMAL_DATA}
  };

  auto it = map.find(mode);
  if (it != map.end()) {
      return it->second;
  }
  else {
      THROW_IA("Unknown link mode: {}", mode);
  }
}

TofpetGlobalCfg::VetoMode vetoModeFromString(std::string mode) {
  std::unordered_map<std::string, TofpetGlobalCfg::VetoMode> map{ 
    {"DISABLED", TofpetGlobalCfg::VETO_DISABLED},
    {"SS", TofpetGlobalCfg::VETO_SS},
    {"TEST_PULSE", TofpetGlobalCfg::VETO_TEST_PULSE},
    {"SS_TEST_PULSE", TofpetGlobalCfg::VETO_SS_TEST_PULSE}
  };

  auto it = map.find(mode);
  if (it != map.end()) {
      return it->second;
  }
  else {
      THROW_IA("Unknown veto mode: {}", mode);
  }
}

TofpetGlobalCfg::CounterPeriod counterPeriodFromString(std::string period) {
  std::unordered_map<std::string, TofpetGlobalCfg::CounterPeriod> map{ 
    {"2^10", TofpetGlobalCfg::PERIOD_2_10},
    {"2^12", TofpetGlobalCfg::PERIOD_2_12},
    {"2^14", TofpetGlobalCfg::PERIOD_2_14},
    {"2^16", TofpetGlobalCfg::PERIOD_2_16},
    {"2^18", TofpetGlobalCfg::PERIOD_2_18},
    {"2^20", TofpetGlobalCfg::PERIOD_2_20},
    {"2^22", TofpetGlobalCfg::PERIOD_2_22},
    {"2^24", TofpetGlobalCfg::PERIOD_2_24},
  };

  auto it = map.find(period);
  if (it != map.end()) {
      return it->second;
  }
  else {
      THROW_IA("Unknown counter period: {}", period);
  }
}
