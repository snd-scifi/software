#include <vector>
#include <unordered_map>
#include <chrono>

#include "fpga/registers.hpp"
#include "fpga/fpga.hpp"
#include "fpga/trigger.hpp"
#include "utils/logger.hpp"
#include "utils/delay.hpp"
#include "utils/timer.hpp"
#include "fpga/ttcrx.hpp"
#include "storage/data_packet.hpp"
#include "io/xillybus-impl.hpp"


#include "tofpet.hpp"

static constexpr unsigned int MAX_RETRIES_GLOBAL{10};
static constexpr unsigned int MAX_RETRIES_CHANNEL{5};
static constexpr int DELAY_RETRY_GLOBAL{500};
static constexpr int DELAY_RETRY_CHANNEL{100};

/**
 * Resets the core and configuration of each TOFPET and writes the default configuration.
 */
void tofpetReset() {
  // enable TOFPET SPI and disable TEMP SPI (needed to have SCLK running)
  writeRegister(SPI_CSR_ADDR, SPI_CSR_EN, 0, true);
  writeRegister(TOFPET_SPI_CSR_ADDR, TOFPET_SPI_CSR_EN, 1, true);
  
  NOTICE("Resetting all TOFPETs (core and configuration)");
  resetFe();

  writeRegister(TOFPET_SPI_CSR_ADDR, TOFPET_SPI_CSR_EN, 0, true);

  TofpetCfg cfg;
  for (uint8_t i = 0; i < 8; i++) {
    NOTICE("Configuring TOFPET {}", i);
    try{
      tofpetConf(i, cfg);
    }
    catch (const tofpet_error& e) {
      ERROR("Error configuring TOFPET {}: {}", i, e.what());
    }
  }
}

/**
 * Sets the global configuration for a given TOFPET.
 * It also updates the static TOFPET configuration.
 */
void tofpetGlobalConf(uint8_t id, const TofpetGlobalCfg& cfg) {
  if (7 < id) {
    THROWX(tofpet_error, "id must be < 8");
  }

  for (unsigned int i = 1; i <= MAX_RETRIES_GLOBAL; i++) {
    DEBUG("Writing global configuration for TOFPET {}.", id);
    // enable TOFPET SPI and disable TEMP SPI
    writeRegister(SPI_CSR_ADDR, SPI_CSR_EN, 0, true);
    writeRegister(TOFPET_SPI_CSR_ADDR, TOFPET_SPI_CSR_EN, 1, true);

    // reset SPI module
    writeRegister(TOFPET_SPI_CSR_ADDR, TOFPET_SPI_CSR_RESET, 1, false);
    writeRegister(TOFPET_SPI_CSR_ADDR, TOFPET_SPI_CSR_RESET, 0, false);

    // prepare and start write request
    writeRegister(TOFPET_SPI_CSR_ADDR, TOFPET_SPI_CSR_CS, FE_0 << id, true);
    writeRegister(TOFPET_SPI_CSR_ADDR, TOFPET_SPI_CSR_READ_NWRITE, 0, true);
    writeRegister(TOFPET_SPI_CSR_ADDR, TOFPET_SPI_CSR_MODE_GLOBAL, 1, true);
    writeRegisters(TOFPET_SPI_D0_ADDR, cfg.toArray(), false);
    writeRegister(TOFPET_SPI_CSR_ADDR, TOFPET_SPI_CSR_START, 1, false);
    writeRegister(TOFPET_SPI_CSR_ADDR, TOFPET_SPI_CSR_START, 0, false);

    // wait to be finished
    while (readRegister(TOFPET_SPI_CSR_ADDR, TOFPET_SPI_CSR_BUSY));

    // probably not needed, but a delay of 200 clock cycles is required after configuration to allow internal TOFPET state machine to settle
    delayMilliseconds(1);

    // disable TOFPET SPI (the clock stops running, which is needed to avoif the configuration reset)
    writeRegister(TOFPET_SPI_CSR_ADDR, TOFPET_SPI_CSR_EN, 0, true);

    // error during configuration
    if (readRegister(TOFPET_SPI_CSR_ADDR, TOFPET_SPI_CSR_ERROR)) {
      WARN("Error writing global configuration for TOFPET {}. {}/{} tries.", id, i, MAX_RETRIES_GLOBAL);
      if (i == MAX_RETRIES_GLOBAL) break;
      delayMilliseconds(DELAY_RETRY_GLOBAL);
    }
    // configuration was successful
    else {
      // save the configuration in the static configuration
      StaticTofpetCfg::getInstance()[id].global = cfg;
      // pulse FE reset
      resetFe();
      return;
    }

  }

  // if it exits the for loop, all the tries were unsuccessful
  THROWX(tofpet_error, "error writing the global configuration for TOFPET {}", id);
}

/**
 * Reads the global configuration for a TOFPET chip.
 * id must be between 0 and 7.
 */
TofpetGlobalCfg tofpetGlobalConf(uint8_t id) {
  if (7 < id) {
    THROWX(tofpet_error, "id must be < 8");
  }

  for (unsigned int i = 1; i <= MAX_RETRIES_GLOBAL; i++) {
    DEBUG("Reading global configuration for TOFPET {}.", id);
    // enable TOFPET SPI and disable TEMP SPI
    writeRegister(SPI_CSR_ADDR, SPI_CSR_EN, 0, true);
    writeRegister(TOFPET_SPI_CSR_ADDR, TOFPET_SPI_CSR_EN, 1, true);

    // reset SPI module
    writeRegister(TOFPET_SPI_CSR_ADDR, TOFPET_SPI_CSR_RESET, 1, false);
    writeRegister(TOFPET_SPI_CSR_ADDR, TOFPET_SPI_CSR_RESET, 0, false);

    // prepare and start read request
    writeRegister(TOFPET_SPI_CSR_ADDR, TOFPET_SPI_CSR_CS, FE_0 << id, true);
    writeRegister(TOFPET_SPI_CSR_ADDR, TOFPET_SPI_CSR_READ_NWRITE, 1, true);
    writeRegister(TOFPET_SPI_CSR_ADDR, TOFPET_SPI_CSR_MODE_GLOBAL, 1, true);
    writeRegisters(TOFPET_SPI_D0_ADDR, TofpetGlobalCfg::readArray(), false);
    writeRegister(TOFPET_SPI_CSR_ADDR, TOFPET_SPI_CSR_START, 1, false);
    writeRegister(TOFPET_SPI_CSR_ADDR, TOFPET_SPI_CSR_START, 0, false);

    // wait to be finished
    while (readRegister(TOFPET_SPI_CSR_ADDR, TOFPET_SPI_CSR_BUSY));

    // disable TOFPET SPI
    writeRegister(TOFPET_SPI_CSR_ADDR, TOFPET_SPI_CSR_EN, 0, true);

    // error during configuration
    if (readRegister(TOFPET_SPI_CSR_ADDR, TOFPET_SPI_CSR_ERROR)) {
      WARN("Error reading global configuration for TOFPET {}. {}/{} tries.", id, i, MAX_RETRIES_GLOBAL);
      if (i == MAX_RETRIES_GLOBAL) break;
      delayMilliseconds(DELAY_RETRY_GLOBAL);
    }
    // configuration was successful
    else {
      auto r = readRegisters(TOFPET_SPI_D0_ADDR, 8);
      return TofpetGlobalCfg(r);
    }

  }
  // if it exits the for loop, all the tries were unsuccessful
  THROWX(tofpet_error, "error reading the global configuration for TOFPET {}.");

}

void tofpetChannelConf(uint8_t id, const TofpetChannelCfg& cfg) {
  if (7 < id) {
    THROWX(tofpet_error, "id must be < 8");
  }

  for (unsigned int i = 1; i <= MAX_RETRIES_CHANNEL; i++) {
    DEBUG("Writing channel configuration for TOFPET {}, channel {}.", id, cfg.getChannel());
    // enable TOFPET SPI and disable TEMP SPI
    writeRegister(SPI_CSR_ADDR, SPI_CSR_EN, 0, true);
    writeRegister(TOFPET_SPI_CSR_ADDR, TOFPET_SPI_CSR_EN, 1, true);

    // reset SPI module
    writeRegister(TOFPET_SPI_CSR_ADDR, TOFPET_SPI_CSR_RESET, 1, false);
    writeRegister(TOFPET_SPI_CSR_ADDR, TOFPET_SPI_CSR_RESET, 0, false);

    // prepare and start write request
    writeRegister(TOFPET_SPI_CSR_ADDR, TOFPET_SPI_CSR_CS, FE_0 << id, true);
    writeRegister(TOFPET_SPI_CSR_ADDR, TOFPET_SPI_CSR_READ_NWRITE, 0, true);
    writeRegister(TOFPET_SPI_CSR_ADDR, TOFPET_SPI_CSR_MODE_GLOBAL, 0, true);
    writeRegisters(TOFPET_SPI_D0_ADDR, cfg.toArray(), false);
    writeRegister(TOFPET_SPI_CSR_ADDR, TOFPET_SPI_CSR_START, 1, false);
    writeRegister(TOFPET_SPI_CSR_ADDR, TOFPET_SPI_CSR_START, 0, false);

    // wait to be finished
    while (readRegister(TOFPET_SPI_CSR_ADDR, TOFPET_SPI_CSR_BUSY));

    // probably not needed, but a delay of 200 clock cycles is required after configuration to allow internal TOFPET state machine to settle
    delayMilliseconds(1);
    // disable TOFPET SPI (the clock stops running, which is needed to avoid the configuration reset)
    writeRegister(TOFPET_SPI_CSR_ADDR, TOFPET_SPI_CSR_EN, 0, true);

    // error during configuration
    if (readRegister(TOFPET_SPI_CSR_ADDR, TOFPET_SPI_CSR_ERROR)) {
      WARN("Error writing channel configuration for TOFPET {}, channel {}. {}/{} tries.", id, cfg.getChannel(), i, MAX_RETRIES_CHANNEL);
      if (i == MAX_RETRIES_CHANNEL) break;
      delayMilliseconds(DELAY_RETRY_CHANNEL);
    }
    // reading was successful
    else {
      // save the configuration in the static configuration
      StaticTofpetCfg::getInstance()[id].channels[cfg.getChannel()] = cfg;
      // pulse FE reset
      resetFe();
      return;
    }

  }
  // if it exits the for loop, all the tries were unsuccessful
  THROWX(tofpet_error, "error writing the channel configuration for TOFPET {}, channel {}.", id, cfg.getChannel());

}

TofpetChannelCfg tofpetChannelConf(uint8_t id, uint8_t channel) {
  if (7 < id) {
    THROWX(tofpet_error, "id must be < 8");
  }

  for (unsigned int i = 1; i <= MAX_RETRIES_CHANNEL; i++) {
    DEBUG("Reading channel configuration for TOFPET {}, channel {}.", id, channel);
    // enable TOFPET SPI and disable TEMP SPI
    writeRegister(SPI_CSR_ADDR, SPI_CSR_EN, 0, true);
    writeRegister(TOFPET_SPI_CSR_ADDR, TOFPET_SPI_CSR_EN, 1, true);

    // reset SPI module
    writeRegister(TOFPET_SPI_CSR_ADDR, TOFPET_SPI_CSR_RESET, 1, false);
    writeRegister(TOFPET_SPI_CSR_ADDR, TOFPET_SPI_CSR_RESET, 0, false);

    // prepare and start read request
    writeRegister(TOFPET_SPI_CSR_ADDR, TOFPET_SPI_CSR_CS, FE_0 << id, true);
    writeRegister(TOFPET_SPI_CSR_ADDR, TOFPET_SPI_CSR_READ_NWRITE, 1, true);
    writeRegister(TOFPET_SPI_CSR_ADDR, TOFPET_SPI_CSR_MODE_GLOBAL, 0, true);
    writeRegisters(TOFPET_SPI_D0_ADDR, TofpetChannelCfg::readArray(channel), false);
    writeRegister(TOFPET_SPI_CSR_ADDR, TOFPET_SPI_CSR_START, 1, false);
    writeRegister(TOFPET_SPI_CSR_ADDR, TOFPET_SPI_CSR_START, 0, false);

    // wait to be finished
    while (readRegister(TOFPET_SPI_CSR_ADDR, TOFPET_SPI_CSR_BUSY));
    // disable TOFPET SPI
    writeRegister(TOFPET_SPI_CSR_ADDR, TOFPET_SPI_CSR_EN, 0, true);

    // error during configuration
    if (readRegister(TOFPET_SPI_CSR_ADDR, TOFPET_SPI_CSR_ERROR)) {
      WARN("Error reading channel configuration for TOFPET {}, channel {}. {}/{} tries.", id, channel, i, MAX_RETRIES_CHANNEL);
      if (i == MAX_RETRIES_CHANNEL) break;
      delayMilliseconds(DELAY_RETRY_CHANNEL);
    }
    // reading was successful
    else {
      auto r = readRegisters(TOFPET_SPI_D0_ADDR, 8);

      // get the threshold baselines from the static configuration
      auto thrT1Bl = StaticTofpetCfg::getInstance()[id].channels[channel].getThresholdT1Baseline();
      auto thrT2Bl = StaticTofpetCfg::getInstance()[id].channels[channel].getThresholdT2Baseline();
      auto thrEBl = StaticTofpetCfg::getInstance()[id].channels[channel].getThresholdEBaseline();

      // according to datasheet, reset must be performed after channel conf read as well
      resetFe();

      return TofpetChannelCfg(channel, r, thrT1Bl, thrT2Bl, thrEBl);
    }

    
  }
  // if it exits the for loop, all the tries were unsuccessful
  THROWX(tofpet_error, "error reading the channel configuration for TOFPET {}, channel {}.", id, channel);
}

void tofpetConf(uint8_t id, const TofpetCfg& cfg) {
  tofpetGlobalConf(id, cfg.global);
  for (uint8_t ch = 0; ch < 64; ch++) {
    tofpetChannelConf(id, cfg.channels[ch]);
  }
}

TofpetCfg tofpetConf(uint8_t id) {
  TofpetCfg cfg;
  cfg.global = tofpetGlobalConf(id);
  for (uint8_t ch = 0; ch < 64; ch++) {
    cfg.channels[ch] = tofpetChannelConf(id, ch);
  }
  return cfg;
}

uint8_t getFeMask(const std::vector<uint8_t> feIds) {
  uint8_t feChipMask = 0;
  for (const auto feId : feIds) {
    if (7 < feId) {
      THROW_IA("feId must be < 8");
    }
    if (feChipMask & (1 << feId)) {
      THROW_IA("feId ", feId, " repeated");
    }
    feChipMask |= (1 << feId);
  }

  return feChipMask;
}

void tofpetGlobalConfMulti(const std::map<uint8_t, TofpetCfg>& cfgs) {
  for (const auto& [id, cfg] : cfgs) {
    tofpetGlobalConf(id, cfg.global);
  }
}

void tofpetGlobalConfMulti(const std::map<uint8_t, TofpetGlobalCfg>& cfgs) {
  for (const auto& [id, cfg] : cfgs) {
    tofpetGlobalConf(id, cfg);
  }
}

std::map<uint8_t, TofpetGlobalCfg> tofpetGlobalConfMulti(std::vector<uint8_t> ids) {
  std::map<uint8_t, TofpetGlobalCfg> cfgs;
  for (const auto id : ids) {
    cfgs[id] = tofpetGlobalConf(id);
  }
  return cfgs;
}

void tofpetChannelConfMulti(const std::map<uint8_t, TofpetCfg>& cfgs, uint8_t channel) {
  for (const auto& [id, cfg] : cfgs) {
    tofpetChannelConf(id, cfg.channels[channel]);
  }
}

void tofpetConfMulti(const std::map<uint8_t, TofpetCfg>& cfgs) {
  for (const auto& [id, cfg] : cfgs) {
    tofpetConf(id, cfg);
  }
}

std::map<uint8_t, TofpetCfg> tofpetConfMulti(const std::vector<uint8_t>& ids) {
  std::map<uint8_t, TofpetCfg> cfgs;
  for (const auto id : ids) {
    cfgs[id] = tofpetConf(id);
  }
  return cfgs;
}

std::vector<uint8_t> getFeIds(std::map<uint8_t, std::vector<uint8_t>>& feChs) {
  std::vector<uint8_t> feIds;
  for (auto& [id, chs] : feChs) {
    // check if feId < 8
    if (7 < id) {
      THROW_IA("feId must be < 8");
    }
    // add feId to vector
    feIds.push_back(id);
    // if channel array empty, ad all channels
    if (chs.size() == 0) {
      for (uint8_t ch = 0; ch < 64; ch++) {
        chs.push_back(ch);
      }
      continue;
    }
    // if it was not empty, check channels
    for (const auto& ch : chs) {
      if (63 < ch) {
        THROW_IA("channel must be < 64");
      }
    }
  }
  return feIds;
}

/**
 * Data collection for the determination of the transimpedance amplifier output baseline such that it is in the range on the threshold voltage DAC.
 * This should be performed with the SiPM connected and biased BELOW breakdown.
 * The returned data is in the following format:
 * {id0: <fe_data>, id1: <fe_data>, ...}
 * where <fe_data> is the data of each frontend chip in the following format:
 * {ch0: <ch_data>, ch1: <ch_data>, ...}
 * where <ch_data> is a vector of length 7 or 63, depending on the baseline being calibrated (63 for T, 7 for E).
 * Each value corresponds to the counter value for the baseline corresponding to the index.
 */
std::map<uint8_t, std::map<uint8_t, std::vector<uint32_t>>> tiaBaselineCalibration(std::map<uint8_t, std::vector<uint8_t>> feChs, const Discriminator disc) {
  auto feIds = getFeIds(feChs);
  NOTICE("TIA baseline calibration: starting calibration for TOPETs {}, discriminator {}.", feIds, disc);
  auto feChipsMask = getFeMask(feIds);
  NOTICE("FE chips mask: {:#x}", feChipsMask);
  // get current configuration, to restore later
  auto initialCfgs = tofpetConfMulti(feIds);

  // disable FE chips
  feChipsEnable(0x00);
  // reset everything
  resetFe();

  auto initialRawData = rawDataEnabled();
  auto initialCounterData = counterDataEnabled();
  auto initialHitData = hitDataDisabled();
  auto initialHeartbeat = heartbeatEnabled();
  auto initialDataLoopback = dataLoopbackEnabled();
  //TODO save other things? e.g. disable triggers

  // raw data is needed to get the counter value
  rawDataEnable(false);
  counterDataEnable(true);
  hitDataDisable(true);
  heartbeatEnable(false);
  dataLoopbackEnable(false);

  INFO("TIA baseline calibration: configuring tofpet");

  // setup TOFPET according to datasheet
  auto cfgs = initialCfgs;
  for (auto& [id, cfg] : cfgs) {
    cfg.global
      .setEventCounterEnabled(true)
      .setEventCounterPeriod(TofpetGlobalCfg::PERIOD_2_22)
    ;
  
    for (uint8_t ch = 0; ch < 64; ch++) {
      // common settings
      cfg.channels[ch]
        // if channel is in the list of requested channels, enable it, otherwise disable it
        .setTriggerMode((std::find(feChs[id].begin(), feChs[id].end(), ch) != feChs[id].end()) ? TofpetChannelCfg::TRIG_NORMAL : TofpetChannelCfg::TRIG_DISABLED)
        .setEventCounterMode(TofpetChannelCfg::CNT_CYCLES_TRIG_B_ACTIVE) // 0xF
        .setThresholdT1(0)
        .setThresholdT1Baseline(0)
        .setThresholdT2(0)
        .setThresholdT2Baseline(0)
        .setThresholdE(0)
        .setThresholdEBaseline(0)
      ;
      switch (disc) {
      case DISC_T1:
        cfg.channels[ch]
          .setTriggerGenerationB(TofpetChannelCfg::B_DO_T1)
          .setThresholdT1Baseline(61)
        ;
        break;
      case DISC_T2:
        cfg.channels[ch]
          .setTriggerGenerationB(TofpetChannelCfg::B_DO_T2)
          .setThresholdT2Baseline(61)
        ;
        break;
      case DISC_E:
        cfg.channels[ch]
          .setTriggerGenerationB(TofpetChannelCfg::B_DO_E)
          .setThresholdEBaseline(61)
        ;
        break;
      default:
        break;
      }
    }
  }

  // set the initial configuration
  tofpetConfMulti(cfgs);

  uint8_t nBlSteps = disc == DISC_E ? 7 : 63;
  // data will be stored here
  std::map<uint8_t, std::map<uint8_t, std::vector<uint32_t>>> counts;
  for (const auto& [id, chs] : feChs) {
    for (const auto& ch : chs) {
      counts[id][ch] = std::vector<uint32_t>(nBlSteps, 0xFFFFFFFF);
    }
  }

  for (uint8_t bl = nBlSteps-1; bl != 0xFF; bl--) {
    INFO("TIA baseline calibration: baseline {}", bl);
    // select next baseline
    for (auto& [id, cfg] : cfgs) {
      for (const auto& ch : feChs[id]) {
        disc == DISC_E ? cfg.channels[ch].setTiaEbaseline(bl) : cfg.channels[ch].setTiaTbaseline(bl);
        tofpetChannelConf(id, cfg.channels[ch]);
      }
    }
    delayMilliseconds(100);

    DataStream::getInstance();

    // enable the desired FE for 30 ms (enough for one counter cycle)
    feChipsEnable(feChipsMask);
    delayMilliseconds(30);
    feChipsEnable(0x00);
    delayMilliseconds(10);

    // read the data
    auto wordRead = xillybusWordCounter();
    // NOTICE("WORDS: ", wordRead);
    auto data = DataStream::getInstance().read(wordRead);

    size_t nonCounterPacketsCount{0};

    for (auto it = data.cbegin(); it != data.cend();) {
      auto dp = DataPacket::createPacket(0, it, data.cend());
      switch (dp.type()) {
        case DataPacket::Counter:
        {
          CounterPacket p = static_cast<CounterPacket&>(dp);
          try {
            counts.at(p.tofpetId()).at(p.channel()).at(bl) = p.value();
          }
          catch (std::out_of_range& e) {} //ignore
        }
          break;
        default:
          nonCounterPacketsCount++;
          break;
      }
    }

    // fill the output vector for this baseline point
    // for (size_t i = 0; i < data.size()/4; i++) {
    //   try { 
    //     auto dp = DataPacket::createPacket(0, data.data()+4*i);
    //     if (dp.type() != DataPacket::Counter) {
    //       nonCounterPacketsCount++;
    //       continue;
    //     }
    //     CounterPacket p = static_cast<CounterPacket&>(dp);
    //     try {
    //       counts.at(p.tofpetId()).at(p.channel()).at(bl) = p.value();
    //     }
    //     catch (std::out_of_range& e) {} //ignore
    //   }
    //   catch (std::runtime_error& e) { // TODO use custom exception
    //     WARN("TIA baseline calibration: Invalid packet: {}", e.what());
    //   }
    // }

    if (nonCounterPacketsCount > 0) {
      WARN("Received {} non-counter packets.", nonCounterPacketsCount);
    }

    DataStream::getInstance().close();
  }

  resetFe();

  rawDataEnable(initialRawData);
  counterDataEnable(initialCounterData);
  hitDataDisable(initialHitData);
  heartbeatEnable(initialHeartbeat);
  dataLoopbackEnable(initialDataLoopback);

  // restore initial configuration
  tofpetConfMulti(initialCfgs);
  return counts;
}

std::map<uint8_t, std::map<uint8_t, std::vector<uint32_t>>> discriminatorThrCalibration(std::map<uint8_t, std::vector<uint8_t>> feChs, const Discriminator disc) {
  auto feIds = getFeIds(feChs);
  NOTICE("Disc thr calib: starting calibration for TOPETs {}, discriminator {}", feIds, disc);
  auto feChipsMask = getFeMask(feIds);
  // get current configuration, to restore later
  auto initialCfgs = tofpetConfMulti(feIds);

  // disable FE chips
  feChipsEnable(0x00);
  // reset everything
  resetFe();

  auto initialRawData = rawDataEnabled();
  auto initialCounterData = counterDataEnabled();
  auto initialHitData = hitDataDisabled();
  auto initialHeartbeat = heartbeatEnabled();
  auto initialDataLoopback = dataLoopbackEnabled();
  //TODO save other things? e.g. disable triggers

  // raw data is needed to get the counter value
  rawDataEnable(false);
  counterDataEnable(true);
  hitDataDisable(true);
  heartbeatEnable(false);
  dataLoopbackEnable(false);

  // setup TOFPET according to datasheet
  auto cfgs = initialCfgs;
  for (auto& [id, cfg] : cfgs) {
    cfg.global
      .setEventCounterEnabled(true)
      .setEventCounterPeriod(TofpetGlobalCfg::PERIOD_2_22)
      .setDigitalDebugOutputEnable(true)
    ;

    // cfg.channels[id == 6 ? 0 : 0].setDigitalDebugMode(TofpetChannelCfg::DBG_TRIGGER_X);
    
    for (uint8_t ch = 0; ch < 64; ch++) {
      // common settings
      cfg.channels[ch]
        // if channel is in the list of requested channels, enable it, otherwise disable it
        .setTriggerMode((std::find(feChs[id].begin(), feChs[id].end(), ch) != feChs[id].end()) ? TofpetChannelCfg::TRIG_NORMAL : TofpetChannelCfg::TRIG_DISABLED)
        .setEventCounterMode(TofpetChannelCfg::CNT_CYCLES_TRIG_B_ACTIVE) // 0xF
        .setThresholdT1(0)
        .setThresholdT1Baseline(0)
        .setThresholdT2(0)
        .setThresholdT2Baseline(0)
        .setThresholdE(0)
        .setThresholdEBaseline(0)
      ;
      switch (disc) {
      case DISC_T1:
        cfg.channels[ch]
          .setTriggerGenerationB(TofpetChannelCfg::B_DO_T1)
        ;
        break;
      case DISC_T2:
        cfg.channels[ch]
          .setTriggerGenerationB(TofpetChannelCfg::B_DO_T2)
        ;
        break;
      case DISC_E:
        cfg.channels[ch]
          .setTriggerGenerationB(TofpetChannelCfg::B_DO_E)
        ;
        break;
      default:
        THROW_RE("discriminatorThrCalibration: unknown discriminator");
        break;
      }
    }
  }

  // set initial configuration
  tofpetConfMulti(cfgs);

  // data will be stored here
  std::map<uint8_t, std::map<uint8_t, std::vector<uint32_t>>> counts;
  for (const auto& [id, chs] : feChs) {
    for (const auto& ch : chs) {
      counts[id][ch] = std::vector<uint32_t>(63, 0xFFFFFFFF);
    }
  }

  for (uint8_t thr = 62; thr != 0xFF; thr--) {
    INFO("Disc thr calib: threshold {}", thr);
    
    // select next threshold
    for (auto& [id, cfg] : cfgs) {
      for (const auto& ch : feChs[id]) {
        switch (disc) {
        case DISC_T1:
          cfg.channels[ch].setThresholdT1Baseline(thr);
          break;
        case DISC_T2:
          cfg.channels[ch].setThresholdT2Baseline(thr);
          break;
        case DISC_E:
          cfg.channels[ch].setThresholdEBaseline(thr);
          break;
        default:
          break;
        }
        tofpetChannelConf(id, cfg.channels[ch]);
      }
    }
    delayMilliseconds(100);

    DataStream::getInstance();

    // enable the desired FE for 30 ms (enough for one counter cycle)
    feChipsEnable(feChipsMask);
    delayMilliseconds(30);
    feChipsEnable(0x00);
    delayMilliseconds(10);

    // read the data
    uint32_t wordRead = xillybusWordCounter();
    // NOTICE("WORDS: ", wordRead);
    auto data = DataStream::getInstance().read(wordRead);

    size_t nonCounterPacketsCount{0};

    for (auto it = data.cbegin(); it != data.cend();) {
      auto dp = DataPacket::createPacket(0, it, data.cend());
      switch (dp.type()) {
        case DataPacket::Counter:
        {
          CounterPacket p = static_cast<CounterPacket&>(dp);
          try {
            counts.at(p.tofpetId()).at(p.channel()).at(thr) = p.value();
          }
          catch (std::out_of_range& e) {} //ignore
        }
          break;
        default:
          nonCounterPacketsCount++;
          break;
      }
    }

    // fill the output vector for this baseline point
    // for (size_t i = 0; i < data.size()/4; i++) {
    //   try { 
    //     auto dp = DataPacket::createPacket(0, data.data()+4*i);
    //     if (dp.type() != DataPacket::Counter) {
    //       nonCounterPacketsCount++;
    //       continue;
    //     }
    //     CounterPacket p = static_cast<CounterPacket&>(dp);
    //     try {
    //       counts.at(p.tofpetId()).at(p.channel()).at(thr) = p.value();
    //     }
    //     catch (std::out_of_range& e) {} //ignore
    //   }
    //   catch (std::runtime_error& e) { // TODO use custom exception
    //     WARN("Invalid packet: {}", e.what());
    //   }
    // }

    if (nonCounterPacketsCount > 0) {
      WARN("Received {} non-counter packets.", nonCounterPacketsCount);
    }

    DataStream::getInstance().close();
    // if (disc == DISC_T2)
    //   std::cin.get();
  }
  
  resetFe();

  rawDataEnable(initialRawData);
  counterDataEnable(initialCounterData);
  hitDataDisable(initialHitData);
  heartbeatEnable(initialHeartbeat);
  dataLoopbackEnable(initialDataLoopback);

  // restore initial configuration
  tofpetConfMulti(initialCfgs);
  return counts;
}

std::pair<std::vector<double>, std::map<uint8_t, std::map<uint8_t, std::vector<uint64_t>>>> getDarkCounts(std::map<uint8_t, std::vector<uint8_t>> feChs, const Discriminator disc, const float minTime, const uint64_t minEvents, const float maxTime) {
  auto feIds = getFeIds(feChs);
  NOTICE("Dark count: starting dark count measurement for TOPETs {}, discriminator {}", feIds, disc);
  auto feChipsMask = getFeMask(feIds);
  // get current configuration, to restore later
  auto initialCfgs = tofpetConfMulti(feIds);

  // disable FE chips
  feChipsEnable(0x00);
  // reset everything
  resetFe();

  auto initialRawData = rawDataEnabled();
  auto initialCounterData = counterDataEnabled();
  auto initialHitData = hitDataDisabled();
  auto initialHeartbeat = heartbeatEnabled();
  auto initialDataLoopback = dataLoopbackEnabled();
  //TODO save other things? e.g. disable triggers

  // raw data is needed to get the counter value
  rawDataEnable(false);
  counterDataEnable(true);
  hitDataDisable(true);
  heartbeatEnable(false);
  dataLoopbackEnable(false);

  // setup TOFPET according to datasheet
  auto cfgs = initialCfgs;
  for (auto& [id, cfg] : cfgs) {
    cfg.global
    .setEventCounterEnabled(true)
    .setEventCounterPeriod(TofpetGlobalCfg::PERIOD_2_24)
    ;

    switch (disc) {
    case DISC_T1:
      for (uint8_t ch = 0; ch < 64; ch++) {
        cfg.channels[ch]
        .setTriggerMode((std::find(feChs[id].begin(), feChs[id].end(), ch) != feChs[id].end()) ? TofpetChannelCfg::TRIG_NORMAL : TofpetChannelCfg::TRIG_DISABLED)
        .setEventCounterMode(TofpetChannelCfg::CNT_RISING_EDGE_TRIG_B) // 0xC
        .setTriggerGenerationT(TofpetChannelCfg::T_DO_T1_AND_DO_T2)
        .setdoT1delay(TofpetChannelCfg::DELAY_6_NS)
        .setTriggerGenerationB(TofpetChannelCfg::B_DO_T1)
        .setTriggerGenerationE(TofpetChannelCfg::E_NOT_DO_E)
        .setDeadTime(16)
        .setThresholdT2(cfg.channels[ch].getThresholdT2Baseline())
        .setThresholdE(cfg.channels[ch].getThresholdEBaseline())
        ;
      }
      break;
    case DISC_T2:
      for (uint8_t ch = 0; ch < 64; ch++) {
        cfg.channels[ch]
        .setTriggerMode((std::find(feChs[id].begin(), feChs[id].end(), ch) != feChs[id].end()) ? TofpetChannelCfg::TRIG_NORMAL : TofpetChannelCfg::TRIG_DISABLED)
        .setEventCounterMode(TofpetChannelCfg::CNT_RISING_EDGE_TRIG_B) // 0xC
        .setTriggerGenerationT(TofpetChannelCfg::T_DO_T1_AND_DO_T2)
        .setdoT1delay(TofpetChannelCfg::DELAY_6_NS)
        .setTriggerGenerationB(TofpetChannelCfg::B_DO_T2)
        .setTriggerGenerationE(TofpetChannelCfg::E_NOT_DO_E)
        .setDeadTime(16)
        .setThresholdT1(cfg.channels[ch].getThresholdT1Baseline())
        .setThresholdE(cfg.channels[ch].getThresholdEBaseline())
        ;
      }
      break;
    case DISC_E:
      for (uint8_t ch = 0; ch < 64; ch++) {
        cfg.channels[ch]
        .setTriggerMode((std::find(feChs[id].begin(), feChs[id].end(), ch) != feChs[id].end()) ? TofpetChannelCfg::TRIG_NORMAL : TofpetChannelCfg::TRIG_DISABLED)
        .setEventCounterMode(TofpetChannelCfg::CNT_RISING_EDGE_TRIG_B) // 0xC
        .setTriggerGenerationT(TofpetChannelCfg::T_DO_T1_AND_DO_T2)
        .setdoT1delay(TofpetChannelCfg::DELAY_6_NS)
        .setTriggerGenerationB(TofpetChannelCfg::B_DO_E)
        .setTriggerGenerationE(TofpetChannelCfg::E_NOT_DO_E)
        .setDeadTime(16)
        .setThresholdT1(cfg.channels[ch].getThresholdT1Baseline())
        .setThresholdT2(cfg.channels[ch].getThresholdT2Baseline())
        ;
      }
      break;
    default:
      break;
    }
  }

  // global conf will always be the same
  tofpetConfMulti(cfgs);

  // data will be stored here
  std::pair<std::vector<double>, std::map<uint8_t, std::map<uint8_t, std::vector<uint64_t>>>> counts;
  counts.first = std::vector<double>(63, 0.);
  for (const auto& [id, chs] : feChs) {
    for (const auto& ch : chs) {
      counts.second[id][ch] = std::vector<uint64_t>(63, 0xFFFFFFFFFFFFFFFF);
    }
  }

  uint8_t zeroCountsIterations = 0;

  for (uint8_t thr = 0; thr < 63; thr++) {
    INFO("Dark count: threshold {}", thr);
    // write the configuration to all channels
    for (auto& [id, cfg] : cfgs) {
      for (const auto& ch : feChs[id]) {
        switch (disc) {
        case DISC_T1:
          if (thr <= cfg.channels[ch].getThresholdT1Baseline()) {
            cfg.channels[ch].setThresholdT1(thr);
          }
          else {
            cfg.channels[ch].setTriggerMode(TofpetChannelCfg::TRIG_DISABLED);
          }
          break;
        case DISC_T2:
          if (thr <= cfg.channels[ch].getThresholdT2Baseline()) {
            cfg.channels[ch].setThresholdT2(thr);
          }
          else {
            cfg.channels[ch].setTriggerMode(TofpetChannelCfg::TRIG_DISABLED);
          }
          break;
        case DISC_E:
          if (thr <= cfg.channels[ch].getThresholdEBaseline()) {
            cfg.channels[ch].setThresholdE(thr);
          }
          else {
            cfg.channels[ch].setTriggerMode(TofpetChannelCfg::TRIG_DISABLED);
          }
          break;
        default:
          break;
        }
        tofpetChannelConf(id, cfg.channels[ch]);
      }
    }
    delayMilliseconds(100);

    DataStream::getInstance();

    auto tBegin = std::chrono::steady_clock::now();
    feChipsEnable(feChipsMask);

    size_t readWords{0};

    auto readData = [&counts, &readWords, &cfgs] (uint8_t thr) {
      auto xbWordCounter = xillybusWordCounter();
      auto data = DataStream::getInstance().read(xbWordCounter-readWords);

      size_t nonCounterPacketsCount{0};

      for (auto it = data.cbegin(); it != data.cend();) {
        auto dp = DataPacket::createPacket(0, it, data.cend());
        switch (dp.type()) {
          case DataPacket::Counter:
          {
            CounterPacket p = static_cast<CounterPacket&>(dp);
            if (cfgs[p.tofpetId()].channels[p.channel()].getTriggerMode() == TofpetChannelCfg::TRIG_NORMAL) {
              try {
                // if it was still invalid, set it to zero because there is data
                if (counts.second.at(p.tofpetId()).at(p.channel()).at(thr) == 0xFFFFFFFFFFFFFFFF) {
                  counts.second.at(p.tofpetId()).at(p.channel()).at(thr) = 0;
                }
                counts.second.at(p.tofpetId()).at(p.channel()).at(thr) += p.value();
                // INFO("adding ", p.value(), " to id/channel ", p.tofpetId(), "/", p.channel());
                // INFO("from packet ", std::hex, p.data());
              }
              catch (std::out_of_range& e) {} //ignore
            }
            // TODO if disabled channels count, start with 0 anche set to 0xFFF... here, otherwise keep like this and add 1 at the end
          }
            break;
          default:
            nonCounterPacketsCount++;
            break;
        }
      }

      // for (size_t i = 0; i < data.size()/4; i++) {
      //   try { 
      //     auto dp = DataPacket::createPacket(0, data.data()+4*i);
      //     if (dp.type() != DataPacket::Counter) {
      //       nonCounterPacketsCount++;
      //       continue;
      //     }
      //     CounterPacket p = static_cast<CounterPacket&>(dp);
      //     if (cfgs[p.tofpetId()].channels[p.channel()].getTriggerMode() == TofpetChannelCfg::TRIG_NORMAL) {
      //       try {
      //         // if it was still invalid, set it to zero because there is data
      //         if (counts.second.at(p.tofpetId()).at(p.channel()).at(thr) == 0xFFFFFFFFFFFFFFFF) {
      //           counts.second.at(p.tofpetId()).at(p.channel()).at(thr) = 0;
      //         }
      //         counts.second.at(p.tofpetId()).at(p.channel()).at(thr) += p.value();
      //         // INFO("adding ", p.value(), " to id/channel ", p.tofpetId(), "/", p.channel());
      //         // INFO("from packet ", std::hex, p.data());
      //       }
      //       catch (std::out_of_range& e) {} //ignore
      //     }
      //     // TODO if disabled channels count, start with 0 anche set to 0xFFF... here, otherwise keep like this and add 1 at the end
      //   }
      //   catch (std::runtime_error& e) { // TODO use custom exception
      //     WARN("Invalid packet: {}", e.what());
      //   }
      // }

      if (nonCounterPacketsCount > 0) {
        WARN("Received {} non-counter packets.", nonCounterPacketsCount);
      }

      readWords += xbWordCounter-readWords;
    };

    // function to determnine the minimum number of counts in all channels for a given threshold
    auto minCounts = [&counts = std::as_const(counts), &cfgs = std::as_const(cfgs)] (uint8_t threshold) {
      uint64_t minC = 0xFFFFFFFFFFFFFFFF;
      for(const auto& [feId, feCfg] : cfgs) {
        for (uint8_t ch = 0; ch < 64; ch++) {
          if (feCfg.channels[ch].getTriggerMode() != TofpetChannelCfg::TRIG_DISABLED) {
            auto c = counts.second.at(feId).at(ch).at(threshold);
            minC = std::min(c, minC);
          }
        }
      }
      return minC;
    };

    auto maxCounts = [&counts = std::as_const(counts), &cfgs = std::as_const(cfgs)] (uint8_t threshold) {
      uint64_t minC = 0;
      for(const auto& [feId, feCfg] : cfgs) {
        for (uint8_t ch = 0; ch < 64; ch++) {
          if (feCfg.channels[ch].getTriggerMode() != TofpetChannelCfg::TRIG_DISABLED) {
            auto c = counts.second.at(feId).at(ch).at(threshold);
            minC = std::max(c, minC);
          }
        }
      }
      return minC;
    };

    auto timeDelta = [] (auto begin) {
      return static_cast<float>(std::chrono::duration_cast<std::chrono::seconds>(std::chrono::steady_clock::now() - begin).count());
    };

    while (true) {
      delayMilliseconds(150);
      readData(thr);
      if (timeDelta(tBegin) < minTime) {
        continue;
      }
      else if (maxTime < timeDelta(tBegin) || minEvents < minCounts(thr)) {
        break;
      }
    }

    auto tEnd = std::chrono::steady_clock::now();
    feChipsEnable(0x00);
    delayMilliseconds(1);

    readData(thr);

    std::chrono::duration<double> duration = tEnd-tBegin;
    counts.first[thr] = duration.count();
    DataStream::getInstance().close();

    if (maxCounts(thr) == 0) {
      zeroCountsIterations++;
      INFO("Zero-counts iterations: {}", zeroCountsIterations);
    }

    if (9 < zeroCountsIterations) {
      INFO("Zero-counts iterations break");
      break;
    }

  }
  resetFe();

  rawDataEnable(initialRawData);
  counterDataEnable(initialCounterData);
  hitDataDisable(initialHitData);
  heartbeatEnable(initialHeartbeat);
  dataLoopbackEnable(initialDataLoopback);

  // restore initial configuration
  tofpetConfMulti(initialCfgs);
  return counts;
}

std::pair<std::vector<double>, std::map<uint8_t, std::map<uint8_t, std::vector<uint64_t>>>> getDarkCounts2(std::map<uint8_t, std::vector<uint8_t>> feChs, const Discriminator disc, const uint64_t minTime, const uint64_t minEvents, const uint64_t maxTime) {
  auto feIds = getFeIds(feChs);
  NOTICE("Dark count: starting dark count measurement for TOPETs {}, discriminator {}", feIds, disc);
  auto feChipsMask = getFeMask(feIds);
  // get current configuration, to restore later
  auto initialCfgs = tofpetConfMulti(feIds);

  // disable FE chips
  feChipsEnable(0x00);
  // reset everything
  resetFe();

  auto initialRawData = rawDataEnabled();
  auto initialCounterData = counterDataEnabled();
  auto initialHitData = hitDataDisabled();
  auto initialHeartbeat = heartbeatEnabled();
  auto initialDataLoopback = dataLoopbackEnabled();
  //TODO save other things? e.g. disable triggers

  // raw data is needed to get the counter value
  rawDataEnable(false);
  counterDataEnable(true);
  hitDataDisable(true);
  heartbeatEnable(false);
  dataLoopbackEnable(false);

  // setup TOFPET according to datasheet
  auto cfgs = initialCfgs;
  for (auto& [id, cfg] : cfgs) {
    cfg.global
    .setEventCounterEnabled(true)
    .setEventCounterPeriod(TofpetGlobalCfg::PERIOD_2_24)
    ;

    switch (disc) {
    case DISC_T1:
      for (uint8_t ch = 0; ch < 64; ch++) {
        cfg.channels[ch]
        .setEventCounterMode(TofpetChannelCfg::CNT_VALID_EVENTS)
        .setTriggerMode((std::find(feChs[id].begin(), feChs[id].end(), ch) != feChs[id].end()) ? TofpetChannelCfg::TRIG_NORMAL : TofpetChannelCfg::TRIG_DISABLED)
        .setTriggerGenerationT(TofpetChannelCfg::T_DO_T1)
        .setTriggerGenerationQ(TofpetChannelCfg::Q_DO_T1)
        .setTriggerGenerationE(TofpetChannelCfg::E_NOT_DO_T1)
        .setTriggerGenerationB(TofpetChannelCfg::B_DO_T1)
        .setdoT1delay(TofpetChannelCfg::DELAY_OFF)
        .setDeadTime(0)
        .setThresholdT2(62)
        .setThresholdE(62)
        ;
      }
      break;
    case DISC_T2:
      for (uint8_t ch = 0; ch < 64; ch++) {
        cfg.channels[ch]
        .setEventCounterMode(TofpetChannelCfg::CNT_VALID_EVENTS)
        .setTriggerMode((std::find(feChs[id].begin(), feChs[id].end(), ch) != feChs[id].end()) ? TofpetChannelCfg::TRIG_NORMAL : TofpetChannelCfg::TRIG_DISABLED)
        .setTriggerGenerationT(TofpetChannelCfg::T_DO_T1_AND_DO_T2)
        .setTriggerGenerationQ(TofpetChannelCfg::Q_DO_T2)
        .setTriggerGenerationE(TofpetChannelCfg::E_NOT_DO_T2)
        .setTriggerGenerationB(TofpetChannelCfg::B_DO_T2)
        .setdoT1delay(TofpetChannelCfg::DELAY_6_NS)
        .setDeadTime(0)
        .setThresholdT1(0)
        .setThresholdE(62)
        ;
      }
      break;
    case DISC_E:
      for (uint8_t ch = 0; ch < 64; ch++) {
        cfg.channels[ch]
        .setEventCounterMode(TofpetChannelCfg::CNT_VALID_EVENTS)
        .setTriggerMode((std::find(feChs[id].begin(), feChs[id].end(), ch) != feChs[id].end()) ? TofpetChannelCfg::TRIG_NORMAL : TofpetChannelCfg::TRIG_DISABLED)
        .setTriggerGenerationT(TofpetChannelCfg::T_DO_E)
        .setTriggerGenerationQ(TofpetChannelCfg::Q_DO_E)
        .setTriggerGenerationE(TofpetChannelCfg::E_NOT_DO_E)
        .setTriggerGenerationB(TofpetChannelCfg::B_DO_E)
        .setdoT1delay(TofpetChannelCfg::DELAY_OFF)
        .setDeadTime(0)
        .setThresholdT1(62)
        .setThresholdT2(62)
        ;
      }
      break;
    default:
      break;
    }
  }

  // global conf will always be the same
  tofpetConfMulti(cfgs);

  // data will be stored here
  std::pair<std::vector<double>, std::map<uint8_t, std::map<uint8_t, std::vector<uint64_t>>>> counts2;
  counts2.first = std::vector<double>(63, 0.);
  for (const auto& [id, chs] : feChs) {
    for (const auto& ch : chs) {
      counts2.second[id][ch] = std::vector<uint64_t>(63, 0xFFFFFFFFFFFFFFFF);
    }
  }

  uint8_t zeroCountsIterations = 0;

  for (uint8_t thr = 0; thr < 63; thr++) {
    INFO("Dark count: threshold {}", thr);
    // write the configuration to all channels
    for (auto& [id, cfg] : cfgs) {
      for (const auto& ch : feChs[id]) {
        switch (disc) {
        case DISC_T1:
          if (thr <= cfg.channels[ch].getThresholdT1Baseline()) {
            cfg.channels[ch].setThresholdT1(thr);
          }
          else {
            cfg.channels[ch].setTriggerMode(TofpetChannelCfg::TRIG_DISABLED);
          }
          break;
        case DISC_T2:
          if (thr <= cfg.channels[ch].getThresholdT2Baseline()) {
            cfg.channels[ch].setThresholdT2(thr);
          }
          else {
            cfg.channels[ch].setTriggerMode(TofpetChannelCfg::TRIG_DISABLED);
          }
          break;
        case DISC_E:
          if (thr <= cfg.channels[ch].getThresholdEBaseline()) {
            cfg.channels[ch].setThresholdE(thr);
          }
          else {
            cfg.channels[ch].setTriggerMode(TofpetChannelCfg::TRIG_DISABLED);
          }
          break;
        default:
          break;
        }
        tofpetChannelConf(id, cfg.channels[ch]);
      }
    }
    delayMilliseconds(100);

    uint64_t readWords{0};
    uint64_t nonCounterPackets{0};

    auto readData = [&counts2, &readWords, &cfgs, &nonCounterPackets] (uint8_t thr) {
      auto xbWordCounter = xillybusWordCounter();
      // INFO("Reading {} words", xbWordCounter-readWords);
      auto data = DataStream::getInstance().read(xbWordCounter-readWords);
      // INFO("Read {} words", data.size());
      for (auto it = data.cbegin(); it != data.cend();) {
        auto dp = DataPacket::createPacket(0, it, data.cend());
        switch (dp.type()) {
          case DataPacket::Counter:
          {
            CounterPacket p = static_cast<CounterPacket&>(dp);
            if (cfgs[p.tofpetId()].channels[p.channel()].getTriggerMode() == TofpetChannelCfg::TRIG_NORMAL) {
              try {
                // if it was still invalid, set it to zero because there is data
                if (counts2.second.at(p.tofpetId()).at(p.channel()).at(thr) == 0xFFFFFFFFFFFFFFFF) {
                  counts2.second.at(p.tofpetId()).at(p.channel()).at(thr) = 0;
                }
                counts2.second.at(p.tofpetId()).at(p.channel()).at(thr) += p.value();
                // INFO("adding ", p.value(), " to id/channel ", p.tofpetId(), "/", p.channel());
                // INFO("from packet ", std::hex, p.data());
              }
              catch (std::out_of_range& e) {} //ignore
            }
            // TODO if disabled channels count, start with 0 anche set to 0xFFF... here, otherwise keep like this and add 1 at the end
          }
            break;
          default:
            nonCounterPackets++;
            break;
        }
      }
      // for (size_t i = 0; i < data.size()/4; i++) {
      //   try { 
      //     auto dp = DataPacket::createPacket(0, data.data()+4*i);
      //     if (dp.type() == DataPacket::Counter) {
      //       CounterPacket p = static_cast<CounterPacket&>(dp);
      //       if (cfgs[p.tofpetId()].channels[p.channel()].getTriggerMode() == TofpetChannelCfg::TRIG_NORMAL) {
      //         try {
      //           // if it was still invalid, set it to zero because there is data
      //           if (counts2.second.at(p.tofpetId()).at(p.channel()).at(thr) == 0xFFFFFFFFFFFFFFFF) {
      //             counts2.second.at(p.tofpetId()).at(p.channel()).at(thr) = 0;
      //           }
      //           counts2.second.at(p.tofpetId()).at(p.channel()).at(thr) += p.value();
      //           // INFO("adding ", p.value(), " to id/channel ", p.tofpetId(), "/", p.channel());
      //           // INFO("from packet ", std::hex, p.data());
      //         }
      //         catch (std::out_of_range& e) {} //ignore
      //       }
      //       continue;
      //     }
      //     else {
      //       nonCounterPackets++;
      //     }
      //     // else if (dp.type() != DataPacket::Hit) {
      //     //   //WARN("Non hit packet: {:#x}", dp.data());
      //     //   hits++;
      //     //   continue;
      //     // }
      //     // HitPacket p = static_cast<HitPacket&>(dp);
      //     // if (cfgs[p.tofpetId()].channels[p.tofpetChannel()].getTriggerMode() == TofpetChannelCfg::TRIG_NORMAL) {
      //     //   try {
      //     //     // if it was still invalid, set it to zero because there is data
      //     //     if (counts.second.at(p.tofpetId()).at(p.tofpetChannel()).at(thr) == 0xFFFFFFFFFFFFFFFF) {
      //     //       counts.second.at(p.tofpetId()).at(p.tofpetChannel()).at(thr) = 0;
      //     //     }
      //     //     counts.second.at(p.tofpetId()).at(p.tofpetChannel()).at(thr) += 1;
      //     //   }
      //     //   catch (std::out_of_range& e) {} //ignore
      //     // }
      //     
      //   }
      //   catch (std::runtime_error& e) { 
      //     WARN("Invalid packet: {}", e.what());
      //   }
      // }
      readWords += xbWordCounter-readWords;
    };

    // function to determnine the minimum number of counts in all channels for a given threshold
    auto minCounts = [&counts2 = std::as_const(counts2), &cfgs = std::as_const(cfgs)] (uint8_t threshold) {
      uint64_t minC = 0xFFFFFFFFFFFFFFFF;
      for(const auto& [feId, feCfg] : cfgs) {
        for (uint8_t ch = 0; ch < 64; ch++) {
          if (feCfg.channels[ch].getTriggerMode() != TofpetChannelCfg::TRIG_DISABLED) {
            auto c = counts2.second.at(feId).at(ch).at(threshold);
            minC = std::min(c, minC);
          }
        }
      }
      return minC;
    };

    auto maxCounts = [&counts2 = std::as_const(counts2), &cfgs = std::as_const(cfgs)] (uint8_t threshold) {
      uint64_t minC = 0;
      for(const auto& [feId, feCfg] : cfgs) {
        for (uint8_t ch = 0; ch < 64; ch++) {
          if (feCfg.channels[ch].getTriggerMode() != TofpetChannelCfg::TRIG_DISABLED) {
            auto c = counts2.second.at(feId).at(ch).at(threshold);
            minC = std::max(c, minC);
          }
        }
      }
      return minC;
    };

    auto timeDelta = [] (auto begin) {
      return static_cast<uint64_t>(std::chrono::duration_cast<std::chrono::seconds>(std::chrono::steady_clock::now() - begin).count());
    };

    DataStream::getInstance();

    auto tBegin = std::chrono::steady_clock::now();
    feChipsEnable(feChipsMask);

    while (true) {
      readData(thr);
      if (timeDelta(tBegin) < minTime) {
        continue;
      }
      else if (maxTime < timeDelta(tBegin) || minEvents < minCounts(thr)) {
        break;
      }
    }

    auto tEnd = std::chrono::steady_clock::now();
    feChipsEnable(0x00);
    delayMilliseconds(200);

    readData(thr);

    std::chrono::duration<double> duration = tEnd-tBegin;
    counts2.first[thr] = duration.count();


    if (nonCounterPackets > 0) {
      WARN("Received {} non-counter packets.", nonCounterPackets);
    }

    DataStream::getInstance().close();

    if (maxCounts(thr) == 0) {
      zeroCountsIterations++;
      INFO("Zero-counts iterations: {}", zeroCountsIterations);
    }

    if (9 < zeroCountsIterations) {
      INFO("Zero-counts iterations break");
      break;
    }

  }
  resetFe();

  rawDataEnable(initialRawData);
  counterDataEnable(initialCounterData);
  hitDataDisable(initialHitData);
  heartbeatEnable(initialHeartbeat);
  dataLoopbackEnable(initialDataLoopback);

  // restore initial configuration
  tofpetConfMulti(initialCfgs);
  return counts2;
}

std::vector<DataPacket> tdcQdcCalibration(const bool tdc, std::map<uint8_t, std::vector<uint8_t>> feChs, const uint8_t phase, const uint8_t duration, const uint32_t nEvents, const uint8_t simultaneousChannels) {
  using namespace trigger;
  
  auto feIds = getFeIds(feChs);
  auto xdc = tdc ? "TDC" : "QDC";
  NOTICE("{} calibration: starting calibration for TOPETs {}", xdc, feIds);
  NOTICE("test pulse duration: {} phase: {}", duration, phase);
  auto feChipsMask = getFeMask(feIds);
  // get current configuration, to restore later
  auto initialCfgs = tofpetConfMulti(feIds);

  // disable FE chips
  feChipsEnable(0x00);
  // reset everything
  resetFe();
  // open data stream
  DataStream::getInstance();

  // auto initialRawData = rawDataEnabled();
  // auto initialHeartbeat = heartbeatEnabled();
  // auto initialDataLoopback = dataLoopbackEnabled();
  //TODO save other things? e.g. disable triggers

  rawDataEnable(false);
  counterDataEnable(false);
  hitDataDisable(false);
  heartbeatEnable(false);
  dataLoopbackEnable(false);

  // setup TOFPET according to datasheet
  auto cfgs = initialCfgs;
  for (auto& [id, cfg] : cfgs) {
    cfg.global
    .setEventCounterEnabled(false)
    ;

    for (uint8_t ch = 0; ch < 64; ch++) {
      cfg.channels[ch]
        .setTriggerMode(TofpetChannelCfg::TRIG_DISABLED)
        .setTriggerGenerationT(TofpetChannelCfg::T_DO_T1)
        .setTriggerGenerationQ(TofpetChannelCfg::Q_DO_T1)
        .setTriggerGenerationE(TofpetChannelCfg::E_DO_T1) // different from petsys, but better. No events are lost in this way
        .setTriggerGenerationB(TofpetChannelCfg::B_DO_T1)
        // .setdoT1delay(TofpetChannelCfg::DELAY_OFF)
        .setThresholdT1(0)
        .setThresholdT2(0)
        .setThresholdE(0)
        .setThresholdT1Baseline(0)
        .setThresholdT2Baseline(0)
        .setThresholdEBaseline(0)
      ;

      if (tdc) {
        cfg.channels[ch].setMeasurementMode(TofpetChannelCfg::MEAS_TOT);
      }
      else {
        cfg.channels[ch]
          .setMeasurementMode(TofpetChannelCfg::MEAS_QDC)
          .setQDCMinIntegrationTime(0)
          .setQDCMaxIntegrationTime(127)
        ;
      }
    }
  }

  tofpetConfMulti(cfgs);

  injectionPhase(phase);

  auto initialTriggerSources = trigSources();
  trigSources({"sequencer"});
  trigSeqCounter(nEvents);
  constexpr uint32_t TRIG_SEQ_PERIOD = 10000;
  trigSeqPeriod(TRIG_SEQ_PERIOD);
  tdc ? testPulseClock(tpClk40) : testPulseClock(tpClk160);
  injectionEnable(true);
  injectionTofpetEnable(true);
  injectionPulseDuration(duration);

  std::vector<DataPacket> packets;
  packets.reserve(feIds.size()*nEvents*64);

  // lambda function that finds the longest vector in the feChs map, to be used in std::max_element
  auto findLongestVector = [] (const std::pair<uint8_t, std::vector<uint8_t>>& a, const std::pair<uint8_t, std::vector<uint8_t>>& b) {
    return a.second.size() < b.second.size();
  };

  // to find the number of steps, we:
  // * take the channel vector with the highest number of channels, 
  // * divide it by the simultaneuos channels,
  // * add 1 if it does not divide exactly

  // std::max_element returns an iterator to the max element in the map, so we take the size of the vector (.second)
  auto maxChannels = (*std::max_element(feChs.begin(), feChs.end(), findLongestVector)).second.size();
  const uint8_t nSteps = maxChannels/simultaneousChannels + (maxChannels%simultaneousChannels == 0 ? 0 : 1);

  auto setChannelGroup = [&] (uint8_t step, TofpetChannelCfg::TriggerMode mode) {
    for (auto& [id, cfg] : cfgs) {
      for (uint8_t i = 0; i < simultaneousChannels; i++) {
        uint8_t chIdx = step + i*nSteps;
        if (chIdx < feChs[id].size()) {
          auto ch = feChs[id][chIdx];
          cfg.channels[ch].setTriggerMode(mode);
          tofpetChannelConf(id, cfg.channels[ch]);
        }
      }
    }
  };

  size_t retryCounter{0};
  for (uint8_t step = 0; step < nSteps; step++) {
    INFO("{} calibration, step {}", xdc, step);
    setChannelGroup(step, TofpetChannelCfg::TRIG_TEST_PULSE);

    feChipsEnable(feChipsMask);

    uint32_t wr = 0;
    trigSeqStart();
    Timer timeoutTimer;
    bool retryStep{false};
    for (uint32_t wordRead = xillybusWordCounter() - wr; wordRead != 0 || trigSeqBusy() || wr == 0; wordRead = xillybusWordCounter() - wr) {
      wordRead -= wordRead % 4;
      if (wordRead == 0 && wr == 0 && timeoutTimer.elapsed() > (TRIG_SEQ_PERIOD * nEvents * 2.5e-9 + 1) * 10 && retryCounter < 10) {
        retryStep = true;
        retryCounter++;
        WARN("No data received. Retrying {}/10.", retryCounter);
        break;
      }
      if (wordRead == 0) {
        continue;
      }
      auto data = DataStream::getInstance().read(wordRead);
      wr += wordRead;

      for (auto it = data.cbegin(); it != data.cend();) {
        auto p = DataPacket::createPacket(0, it, data.cend());
        switch (p.type()) {
          case DataPacket::Hit:
            packets.push_back(p);
            break;
          default:
            break;
        }
      }
    }

    feChipsEnable(0x00);


    setChannelGroup(step, TofpetChannelCfg::TRIG_DISABLED);
    if (retryStep) {
      step--;
    }
    else {
      retryCounter = 0;
    }
  }
  
  trigSources(initialTriggerSources);
  injectionTofpetEnable(false);
  injectionEnable(false);

  tofpetConfMulti(initialCfgs);
  DataStream::getInstance().close();

  NOTICE("Recorded {} packets", packets.size());
  return packets;
}

std::ostream& operator<<(std::ostream& out, const Discriminator disc) {
  switch (disc) {
  case DISC_E:
    return out << "E";
    break;
  case DISC_T1:
    return out << "T1";
    break;
  case DISC_T2:
    return out << "T2";
    break;
  default:
    return out;
    break;
  }
}

Discriminator discriminatorFromString(std::string disc) {
  std::unordered_map<std::string, Discriminator> map{{"T1", DISC_T1}, {"T2", DISC_T2}, {"E", DISC_E}};

  auto it = map.find(disc);
  if (it != map.end()) {
      return it->second;
  }
  else {
      THROW_IA("Unknown discriminator: " + disc);
  }
}
