#include <stdexcept>
#include <cerrno>
#include <cstring>

#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>

#include "utils/logger.hpp"

#include "gpio.hpp"

#define MODIFY_BITS(addr, msk, val) *(addr) = (*(addr) & ~(msk)) | ((val) & (msk))

Gpio::Gpio() {
  int fd;
  // open the file representing the physical memory
  if ((fd = open("/dev/mem", O_RDWR|O_SYNC)) == -1) {
    THROW_SE(errno, "error opening /dev/mem");
  }

  // map the portion of physical memory starting at GPIOX_BASE and GPIO_SPAN long to the relative pointers
  // reading and writing from these pointers is the same as reading and writing the registers
  m_gpio0_regs = static_cast<uint32_t*>(mmap(NULL, GPIO_SPAN, (PROT_READ|PROT_WRITE), MAP_SHARED, fd, GPIO0_BASE));
  if (m_gpio0_regs == MAP_FAILED) {
    THROW_SE(errno, "m_gpio0_regs: mmap failed");
  }

  m_gpio1_regs = static_cast<uint32_t*>(mmap(NULL, GPIO_SPAN, (PROT_READ|PROT_WRITE), MAP_SHARED, fd, GPIO1_BASE));
  if (m_gpio1_regs == MAP_FAILED) {
    THROW_SE(errno, "m_gpio1_regs: mmap failed");
  }

  m_gpio2_regs = static_cast<uint32_t*>(mmap(NULL, GPIO_SPAN, (PROT_READ|PROT_WRITE), MAP_SHARED, fd, GPIO2_BASE));
  if (m_gpio2_regs == MAP_FAILED) {
    THROW_SE(errno, "m_gpio2_regs: mmap failed");
  }

  // the file can be closed and the mapping remains valid
  close(fd);
}

Gpio::~Gpio() {
  if (munmap(m_gpio0_regs, GPIO_SPAN)) {
    WARN("error unmapping m_gpio0_regs: {}", std::strerror(errno));
  }

  if (munmap(m_gpio1_regs, GPIO_SPAN)) {
    WARN("error unmapping m_gpio1_regs: {}", std::strerror(errno));
  }

  if (munmap(m_gpio2_regs, GPIO_SPAN)) {
    WARN("error unmapping m_gpio2_regs: {}", std::strerror(errno));
  }
}

/**
 * Set the direction (i.e. input or output) of several GPIO pins.
 * You need to know what you are doing when using this method, so I recommend using setDirection(pin, outout) instead.
 * \param gpio is either 0, 1 or 2, to select the desired GPIO register.
 * \param mask is a bit map of the ports that needs to be changed. This map will be ANDed with GPIOX_AVAILABLE_PORTS, to make sure that you don't mess with the wrong pins (some GPIO are used by ethernet, SD card, etc and you don't want to mess with those).
 * \param value a bit map with the setting for each pin selected by mask. 0 means input, 1 means output. This value need to be aligned with the mask, it will not be shifted.
 */
void Gpio::setDirection(int gpio, uint32_t mask, uint32_t value) {
  switch (gpio) {
  case 0:
    mask = mask & GPIO0_AVAILABLE_PORTS;
    if (mask == 0) {
      WARN("mask is 0, no pins will be modified.");
      break;
    }
    MODIFY_BITS(m_gpio0_regs+1, mask, value);
    break;
  case 1:
    mask = mask & GPIO1_AVAILABLE_PORTS;
    if (mask == 0) {
      WARN("mask is 0, no pins will be modified.");
      break;
    }
    MODIFY_BITS(m_gpio1_regs+1, mask, value);
    break;
  case 2:
    mask = mask & GPIO2_AVAILABLE_PORTS;
    if (mask == 0) {
      WARN("mask is 0, no pins will be modified.");
      break;
    }
    MODIFY_BITS(m_gpio2_regs+1, mask, value);
    break;
  default:
    THROW_IA("gpio must be 0, 1 or 2.");
    break;
  }
}

/**
 * Set the direction (i.e. input or output) of a GPIO pin. 
 * Triyng to change a pin you should not touch will have no effect, save for a warning being displayed.
 * \param pin is the pin number, as reported in the CycloneV pinout, to be modified (0 to 66).
 * \param output true to set the pin to output, false to set it to input.
 */
void Gpio::setDirection(int pin, bool output) {
  if (66 < pin) {
    THROW_IA("pin must be a number between 0 and 66");
  }

  uint32_t mask = 1 << (pin % 29);
  uint32_t value = output << (pin % 29);

  setDirection(pin / 29, mask, value);

}

/**
 * Write to several GPIO pins.
 * You need to know what you are doing when using this method, so I recommend using write(pin, value) instead.
 * \param gpio is either 0, 1 or 2, to select the desired GPIO register.
 * \param mask is a bit map of the ports that needs to be changed. This map will be ANDed with GPIOX_AVAILABLE_PORTS, to make sure that you don't mess with the wrong pins (some GPIO are used by ethernet, SD card, etc and you don't want to mess with those).
 * \param value a bit map with the setting for each pin selected by mask. This value need to be aligned with the mask, it will not be shifted.
 */
void Gpio::write(int gpio, uint32_t mask, uint32_t value) {
  switch (gpio) {
  case 0:
    mask = mask & GPIO0_AVAILABLE_PORTS;
    if (mask == 0) {
      WARN("mask is 0, no pins will be modified.");
      break;
    }
    MODIFY_BITS(m_gpio0_regs, mask, value);
    break;
  case 1:
    mask = mask & GPIO1_AVAILABLE_PORTS;
    if (mask == 0) {
      WARN("mask is 0, no pins will be modified.");
      break;
    }
    MODIFY_BITS(m_gpio1_regs, mask, value);
    break;
  case 2:
    mask = mask & GPIO2_AVAILABLE_PORTS;
    if (mask == 0) {
      WARN("mask is 0, no pins will be modified.");
      break;
    }
    MODIFY_BITS(m_gpio2_regs, mask, value);
    break;
  default:
    THROW_IA("gpio must be 0, 1 or 2.");
    break;
  }
}

/**
 * Write a GPIO pin. 
 * Triyng to change a pin you should not touch will have no effect, save for a warning being displayed.
 * \param pin is the pin number, as reported in the CycloneV pinout, to be modified (0 to 66).
 * \param value true for 1, false for 0.
 */
void Gpio::write(int pin, bool value) {
  if (66 < pin) {
    THROW_IA("pin must be a number between 0 and 66");
  }

  uint32_t mask = 1 << (pin % 29);
  uint32_t val = value << (pin % 29);

  write(pin / 29, mask, val);
}

/**
 * Read several GPIO pins.
 * You need to know what you are doing when using this method, so I recommend using read(pin) instead.
 * \param gpio is either 0, 1 or 2, to select the desired GPIO register.
 * \param mask is a bit map of the ports that needs to be read. This map will NOT be ANDed with GPIOX_AVAILABLE_PORTS.
 * \return *m_gpioX_regs & mask
 */
uint32_t Gpio::read(int gpio, uint32_t mask) {
  switch (gpio) {
  case 0:
    return *m_gpio0_regs & mask;
  case 1:
    return *m_gpio1_regs & mask;
  case 2:
    return *m_gpio2_regs & mask;
  default:
    THROW_IA("gpio must be 0, 1 or 2.");
    break;
  }
}

/**
 * Returns the value of a single GPIO pin.
 */
bool Gpio::read(int pin) {
  if (66 < pin) {
    THROW_IA("pin must be a number between 0 and 66");
  }

  uint32_t mask = 1 << (pin % 29);

  return read(pin / 29, mask);
}
