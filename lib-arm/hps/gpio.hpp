#pragma once

#include <cstdint>

/**
 * Class to control the HPS GPIO pins.
 * The "right" way would be to use the altera hwlib, but that's a bit of a pain to integrate and since we only need to blink a couple of pins, we can directly access the GPIO registers.
 * To do so, the portions of the physical memory that contain the registers we need are accessed from /dev/mem and mapped to pointers.
 * This class is a singleton, i.e. only one instance can exist and it is accessed with Gpio::getInstance(), so methods are called as Gpio::getInstance().write(61, true), for example.
 */
class Gpio {
public:
  /**
  * return the instance of the Gpio class. Use it to call the other methods. 
  */
  static Gpio& getInstance() {
    static Gpio gpio;
    return gpio;
  }
  ~Gpio();

  Gpio(Gpio const&) = delete;             // No Copy construct
  Gpio(Gpio&&) = delete;                  // No Move construct
  Gpio& operator=(Gpio const&) = delete;  // No Copy assign
  Gpio& operator=(Gpio &&) = delete;      // No Move assign

  void setDirection(int gpio, uint32_t mask, uint32_t value);
  void setDirection(int pin, bool output);

  void write(int gpio, uint32_t mask, uint32_t value);
  void write(int pin, bool value);

  uint32_t read(int gpio, uint32_t mask);
  bool read(int pin);

private:
  Gpio();
  static constexpr uint32_t GPIO0_BASE = 0xFF708000;
  static constexpr uint32_t GPIO1_BASE = 0xFF709000;
  static constexpr uint32_t GPIO2_BASE = 0xFF70A000;
  static constexpr uint32_t GPIO_SPAN = 0x08; // is 0x78, but we need just the first 2 registers

  static constexpr uint32_t GPIO0_AVAILABLE_PORTS = 0x00000000;
  static constexpr uint32_t GPIO1_AVAILABLE_PORTS = 0x00080000;
  static constexpr uint32_t GPIO2_AVAILABLE_PORTS = 0x00000078;

  // registers
  uint32_t* m_gpio0_regs;
  uint32_t* m_gpio1_regs;
  uint32_t* m_gpio2_regs;

  // m_gpioX_regs -> gpio_dr (data register, to turn on and off the pins)
  // m_gpioX_regs+1 -> gpio_ddr (data direction register, to set pins as input or outputs)

};
