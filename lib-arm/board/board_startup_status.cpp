#include "board_startup_status.hpp"

#include "fpga/temperature.hpp"
#include "fpga/fpga.hpp"
#include "tofpet/tofpet.hpp"

BoardStartupStatus::BoardStartupStatus() 
  : m_boardId{0}
  , m_boardVersion{0}
  , m_feBoardIds({0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF})
  , m_feBoardVersions({0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF})
  , m_tofpets(0)
{
  reset();
}

/**
 * Clears and re-determines the startup status
 */
void BoardStartupStatus::reset() {
  NOTICE("Resetting board startup status...");
  m_boardId = 0;
  m_tofpets.clear();
  m_feBoardIds = {0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF, 0xFFFFFFFF};
  m_boardId = ::boardId();
  m_boardVersion = ::boardRevision();
  findFeBoards();
  findTofpets();
}

/**
 * Determines which FE boards are connected to the DAQ board.
 * Needs to be changed to read the ID from the EEPROM and save that.
 */
void BoardStartupStatus::findFeBoards() {
  NOTICE("Finding FE boards...");
  m_feBoardIds = feIds();
  m_feBoardVersions = feRevisions();
}

/**
 * Determines which TOFPETs are connected and reachable.
 * To do so, it tries to read their global configuration. If the read fails, it is considered not connected.
 */
void BoardStartupStatus::findTofpets() {
  NOTICE("Finding TOFPET chips...");
  for (uint8_t i = 0; i < 8; i++) {
    try {
      tofpetGlobalConf(i);
      m_tofpets.push_back(i);
      INFO("TOFPET {} found.", i);
    }
    catch (std::runtime_error& e) { INFO("TOFPET {} not found.", i); }
  }
}

void to_json(json& j, const BoardStartupStatus& p) {
  j["board_id"] = p.boardId();
  j["board_version"] = p.boardVersion();
  j["fe_board_ids"] = p.feBoardIds();
  j["fe_board_versions"] = p.feBoardVersions();
  j["tofpet_available"] = p.tofpetsAvailable();
}