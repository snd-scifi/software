#pragma once

namespace board {

void rebootBoard();

void setBootStartup(bool enable);

void setDateTime(const std::string datetime);

} // namespace board