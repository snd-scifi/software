#pragma once
#include <atomic>
#include <mutex>
#include <string>

#include "json.hpp"

using json = nlohmann::json;

class DaqManager {
public:
  DaqManager();

  void reset();
  json toJson();

  void requestStopDaq() { m_stopDaq = true; };
  bool daqStopRequested() const { return m_stopDaq; };
  void running(bool running) { m_running = running; }
  bool running() const { return m_running; }

  void error(std::string message);
  bool error() const { return m_error; }
  std::string errorMessage();

  void readWords(uint64_t readWords) { m_readWords = readWords; }
  void increaseReadWords(uint64_t readWords) { m_readWords += readWords; }
  uint64_t readWords() const { return m_readWords; }

  void sentWords(uint64_t sentWords) { m_sentWords = sentWords; }
  void increaseSentWords(uint64_t sentWords) { m_sentWords += sentWords; }
  uint64_t sentWords() const { return m_sentWords; }

private:
  std::mutex m_mutex;

  std::atomic_bool m_stopDaq;
  std::atomic_bool m_running;
  std::atomic_bool m_error;
  std::string m_errorMessage;
  std::atomic_uint64_t m_readWords;
  std::atomic_uint64_t m_sentWords;

};
