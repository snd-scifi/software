#pragma once

#include <array>
#include <vector>

#include "json.hpp"

using json = nlohmann::json;

class BoardStartupStatus {
public:
  static BoardStartupStatus& getInstance() {
    static BoardStartupStatus cfg;
    return cfg;
  }

  BoardStartupStatus(BoardStartupStatus const&) = delete;             // Copy construct
  BoardStartupStatus(BoardStartupStatus&&) = delete;                  // Move construct
  BoardStartupStatus& operator=(BoardStartupStatus const&) = delete;  // Copy assign
  BoardStartupStatus& operator=(BoardStartupStatus &&) = delete;      // Move assign

  void reset();
  uint32_t boardId() const { return m_boardId; }
  uint32_t boardVersion() const { return m_boardVersion; }
  std::array<uint32_t, 4> feBoardIds() const { return m_feBoardIds; }
  std::array<uint32_t, 4> feBoardVersions() const { return m_feBoardVersions; }
  std::vector<uint8_t> tofpetsAvailable() const { return m_tofpets; }

private:
  BoardStartupStatus();
  void findFeBoards();
  void findTofpets();
  
  std::uint32_t m_boardId;
  std::uint32_t m_boardVersion;
  std::array<uint32_t, 4> m_feBoardIds;
  std::array<uint32_t, 4> m_feBoardVersions;
  std::vector<uint8_t> m_tofpets;
};

void to_json(json& j, const BoardStartupStatus& p);