#include <unistd.h>
#include <sys/stat.h>
#include <sys/reboot.h>
#include <sys/time.h>

#include "utils/logger.hpp"

#include "board.hpp"

namespace board {

void rebootBoard() {
  NOTICE("Syncing...");
  sync();
  NOTICE("Rebooting...");
  reboot(RB_AUTOBOOT);
}

/**
 * Enables or disables automatic startup of the board-server
 */
void setBootStartup(bool enable) {

  auto fileExist = [] (const char* filename) {
    struct stat buffer;   
    return stat(filename, &buffer) == 0; 
  };

  if (enable) {
      if (!fileExist("/etc/init.d/S95boardserver")) {
        NOTICE("Renaming /etc/init.d/K95boardserver to /etc/init.d/S95boardserver.");
        auto res = rename("/etc/init.d/K95boardserver", "/etc/init.d/S95boardserver");
        if (res != 0) {
          THROW_SE(errno, "Error enabling board-server boot startup");
        }
      }
      else {
        INFO("/etc/init.d/S95boardserver already exists. Doing nothing.");
      }
    }
    else {
      if (!fileExist("/etc/init.d/K95boardserver")) {
        NOTICE("Renaming /etc/init.d/S95boardserver to /etc/init.d/K95boardserver.");
        auto res = rename("/etc/init.d/S95boardserver", "/etc/init.d/K95boardserver");
        if (res != 0) {
          THROW_SE(errno, "Error disabling board-server boot startup");
        }
      }
      else {
        INFO("/etc/init.d/K95boardserver already exists. Doing nothing.");
      }
    }
}


void setDateTime(const std::string datetime) {
  tm time;
  if (strptime(datetime.c_str(), "%Y-%m-%dT%H:%M:%S", &time) == NULL) {
    THROW_RE("Error parsing the time string '{}'. Time is not set.", datetime);
  }
  timeval tv;
  tv.tv_sec = mktime(&time);
  if (tv.tv_sec == -1) {
    THROW_SE(errno, "Error converting tm to timeval.");
  }
  tv.tv_usec = 0;

  const auto retval = settimeofday(&tv, NULL);
  if (retval < 0) {
    THROW_SE(errno, "Error setting date and time.");
  }

  NOTICE("Date set to {}.", datetime);
}

} // namespace board