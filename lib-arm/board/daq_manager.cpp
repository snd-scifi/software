#include "daq_manager.hpp"

DaqManager::DaqManager()
  : m_stopDaq{false}
  , m_running{false}
  , m_error{false}
  , m_readWords{0}
  , m_sentWords{0}
{}

void DaqManager::reset() {
  m_stopDaq = false;
  m_running = false;
  m_error = false;
  m_errorMessage = "";
  m_readWords = 0;
  m_sentWords = 0;
}

json DaqManager::toJson() {
  json ret;
  {
    std::lock_guard<std::mutex> lock(m_mutex);
    ret["running"] = m_running.load();
    ret["error"] = m_error.load();
    ret["error_message"] = m_errorMessage;
    ret["read_words"] = m_readWords.load();
    ret["sent_words"] = m_sentWords.load();
  }
  return ret;
}

void DaqManager::error(std::string message) {
  std::lock_guard<std::mutex> lock(m_mutex);
  m_error = true;
  m_errorMessage = message;
}

std::string DaqManager::errorMessage() {
  std::lock_guard<std::mutex> lock(m_mutex);
  return m_errorMessage;
}