#pragma once

//#include <cstdint>
#include <string>
#include <thread>

#include "json.hpp"

#include "board/daq_manager.hpp"
#include "network/common_server.hpp"

using json = nlohmann::json;
using JsonObject = json::object_t;
using JsonCommandCallback_t = std::function<JsonObject(JsonObject)>;
using RawCommandCallback_t = std::function<std::pair<json, std::vector<DataPacket>>(JsonObject)>;
using JsonCommandMap = std::unordered_map<std::string, JsonCommandCallback_t>;
using RawCommandMap = std::unordered_map<std::string, RawCommandCallback_t>;

class BoardServer {
public:
  BoardServer(std::string serverPort, size_t acceptMax);
  ~BoardServer();

  void start() { m_server.start(); }

  Connection acceptConnection();
  void connectionLoop(Connection connection);
  void registerJsonCommand(std::string command, JsonCommandCallback_t callback);
  void registerRawCommand(std::string command, RawCommandCallback_t callback);
  json processJsonCommand(std::string command);
  std::pair<json, std::vector<DataPacket>> processRawCommand(std::string command);

  json startDaq(json args);
  json stopDaq(json args);
  json getBoardStatus(json args);

private:
  CommonServer m_server;
  JsonCommandMap m_jsonCommands;
  RawCommandMap m_rawCommands;

  std::thread m_daqThread;
  DaqManager m_daqManager;
};