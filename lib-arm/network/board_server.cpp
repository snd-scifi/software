#include "utils/logger.hpp"
#include "tools/daq_client.hpp"
#include "board/board_startup_status.hpp"
#include "fpga/board_commands.hpp"
#include "fpga/eeprom.hpp"
#include "fpga/fpga.hpp"
#include "fpga/temperature.hpp"
#include "fpga/trigger.hpp"
#include "fpga/ttcrx.hpp"

#include "board_server.hpp"


BoardServer::BoardServer(std::string serverPort, size_t acceptMax) : m_server("BoardServer", serverPort, acceptMax) {
  using namespace boardCommands;
  registerJsonCommand("full_reset", reinitializeBoard);
  registerJsonCommand("fe_reset", feReset);
  registerJsonCommand("get_board_startup_status", getBoardStartupStatus);
  registerJsonCommand("disable_leds", disableLEDs);
  registerJsonCommand("board_status", [this] (json args) { return this->getBoardStatus(args); });
  registerJsonCommand("get_fe_conf", getFeConf);
  registerJsonCommand("set_fe_conf", setFeConf);
  registerJsonCommand("tia_baseline_calibration", tiaBaselineCalibration);
  registerJsonCommand("disc_thr_calibration", discriminatorThrCalibration);
  registerJsonCommand("get_dark_counts", getDarkCounts);
  registerJsonCommand("get_dark_counts_2", getDarkCounts2);
  registerRawCommand("tdc_calibration", tdcCalibration);
  registerRawCommand("qdc_calibration", qdcCalibration);
  registerJsonCommand("board_debug", setBoardDebug);
  registerJsonCommand("set_trigger", setTrigger);
  registerJsonCommand("get_trigger", getTrigger);
  registerJsonCommand("start_trigger_sequencer", startTriggerSequencer);
  registerJsonCommand("set_injection", setInjection);
  registerJsonCommand("get_injection", getInjection);
  registerJsonCommand("fe_analog_debug", setFeAnalogDebugOutput);
  registerJsonCommand("fe_digital_debug", setFeDigitalDebugOutput);
  registerJsonCommand("start_daq", [this] (json args) { return this->startDaq(args); });
  registerJsonCommand("stop_daq", [this] (json args) { return this->stopDaq(args); });
  registerJsonCommand("get_clk40des1_phase", getClk40Des1Phase);
  registerJsonCommand("set_clk40des1_phase", setClk40Des1Phase);
  registerJsonCommand("get_clk40des2_phase", getClk40Des2Phase);
  registerJsonCommand("set_clk40des2_phase", setClk40Des2Phase);
  registerJsonCommand("get_clk40_use_qpll", getClk40UseQpll);
  registerJsonCommand("set_clk40_use_qpll", setClk40UseQpll);
  registerJsonCommand("get_clk_tofpet_phase", getClkTofpetPhase);
  registerJsonCommand("set_clk_tofpet_phase", setClkTofpetPhase);
  registerJsonCommand("scan_clk_tofpet_phase", scanClkTofpetPhase);
  registerJsonCommand("generate_data", generateData);
  registerJsonCommand("set_eeprom_id", setEepromId);
  registerJsonCommand("update_fw", updateFirmware);
  registerJsonCommand("reboot", rebootBoard);
  registerJsonCommand("set_board_server_boot_start", setBoardServerBootStart);
  registerJsonCommand("set_date_time", setDateTime);
}

BoardServer::~BoardServer() {
  m_server.stop();
}

Connection BoardServer::acceptConnection() {
  return m_server.acceptConnection();
}

void BoardServer::connectionLoop(Connection connection) {
  Header header;
  std::string data;
  std::string command;
  
  while(1) {
    //TODO error checking
    try {
      header = connection.recvHeader();
    }
    catch (connection_closed& cc) {
      NOTICE("BoardServer: Connection from {} closed.", connection.address());
      return;
    }

    switch (header.type) {
    case BEGIN_COMMUNICATION: //TODO need to distinguish between a data connection and a control connection
      {
        try {
          data = connection.recvString(header.length);
        }
        catch (connection_closed& cc) {
          NOTICE("BoardServer: Connection from {} closed.", connection.address());
          return;
        }

        //TODO initialize
      }
    case COMMAND:
      {
        try {
          command = connection.recvString(header.length);
        }
        catch (connection_closed& cc) {
          NOTICE("BoardServer: Connection from {} closed.", connection.address());
          return;
        }
        auto reply = processJsonCommand(command).dump();
        connection.sendHeader({HeaderType::COMMAND_REPLY, static_cast<uint32_t>(reply.size()), header.id});
        connection.sendString(reply);
        break;
      }
    case RAW_COMMAND:
      {
        try {
          command = connection.recvString(header.length);
        }
        catch (connection_closed& cc) {
          NOTICE("BoardServer: Connection from {} closed.", connection.address());
          return;
        }
        auto reply = processRawCommand(command);
        auto replyJson = reply.first.dump();
        auto replyData = reply.second;
        connection.sendHeader({HeaderType::COMMAND_REPLY, static_cast<uint32_t>(replyJson.size()), header.id});
        connection.sendString(replyJson);
        if (reply.first.at("response") == "ok" && reply.first.at("result") == "raw_data_follows") {
          connection.sendData(replyData, header.id);
        }
        break;
      }
    case END_COMMUNICATION:
      {
        try {
          data = connection.recvString(header.length);
        }
        catch (connection_closed& cc) {
          NOTICE("BoardServer: Connection from {} closed.", connection.address());
          return;
        }
        return;
      }
    default:
      WARN("Unknown header");
      break;
    }
    
  }
}

json BoardServer::processJsonCommand(std::string command) {
  auto j = json::parse(command);
  std::string cmd = j.at("command");
  json args;

  // if (cmd == "start_daq") {
  //   return startDaq(args);
  // }
  // else if (cmd == "stop_daq") {
  //   return stopDaq(args);
  // }

  try {
    args = j.at("args");
  }
  catch (nlohmann::detail::out_of_range& oor) {
    DEBUG("processJsonCommand: no arguments given");
    args["args"] = json::value_t::object;
  }

  try {
    return m_jsonCommands.at(cmd)(args);
  }
  catch (std::out_of_range& oor) {
    DEBUG("processJsonCommand: unknown command");
    json response;
    response["response"] = "err";
    response["result"] = "unknown command";
    return response;
  }
}


std::pair<json, std::vector<DataPacket>> BoardServer::processRawCommand(std::string command) {
  auto j = json::parse(command);
  std::string cmd = j.at("command");
  json args;
  try {
    args = j.at("args");
  }
  catch (nlohmann::detail::out_of_range& oor) {
    DEBUG("processRawCommand: no arguments given");
    args["args"] = json::value_t::object;
  }

  try {
    return m_rawCommands.at(cmd)(args);
  }
  catch (std::out_of_range& oor) {
    WARN("processRawCommand: unknown command");
    json response;
    response["response"] = "err";
    response["result"] = "unknown command";
    return std::make_pair(response, std::vector<DataPacket>());
  }
}

void BoardServer::registerJsonCommand(std::string command, JsonCommandCallback_t callback) {
  m_jsonCommands[command] = callback;
}

void BoardServer::registerRawCommand(std::string command, RawCommandCallback_t callback) {
  m_rawCommands[command] = callback;
}

json BoardServer::startDaq(json args) {
  json reply;
  NOTICE("Start daq args: {}", args);
  try {
    auto address = args.at("address").get<std::string>();
    auto port = args.at("port").get<std::string>();
    auto transmitMin = args.at("transmit_min").get<size_t>();
    auto maxWaitTime = args.at("max_wait_time").get<double>();
    auto feIds = args.at("fe_ids").get<std::vector<uint8_t>>();
    auto buildPackets = args.at("build_packets").get<bool>();
    auto bChData = args.at("enable_b_channel_data").get<bool>();

    NOTICE("Resetting DAQ status...");
    m_daqManager.reset();
    NOTICE("Starting DAQ thread...");
    m_daqThread = std::thread(daqClient, address, port, feIds, std::ref(m_daqManager), transmitMin, maxWaitTime, buildPackets, bChData);
    
    if (!m_daqThread.joinable()) {
      reply["response"] = "err";
      reply["result"] = "Failed to start DAQ thread";
    }

    NOTICE("Waiting for DAQ running...");
    while (!m_daqManager.running() && !m_daqManager.error());
    if (m_daqManager.running()) {
      reply["response"] = "ok";
      reply["result"] = {};
    }
    else {
      m_daqThread.join();
      reply["response"] = "err";
      reply["result"] = m_daqManager.errorMessage();
    }
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = boardCommands::exceptionHandler("startDaq");
  }
  return reply;
}

json BoardServer::stopDaq(json args) {
  json reply;
  try {
    m_daqManager.requestStopDaq();
    if (m_daqThread.joinable()) {
      m_daqThread.join();
    }
    reply["response"] = "ok";
    reply["result"] = {};
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = boardCommands::exceptionHandler("stopDaq");
  }
  return reply;
}


json BoardServer::getBoardStatus(json args) { //TODO add try catch
  json result;
  result["board_id"] = boardId();
  result["fe_ids"] = feIds();
  result["rx_fifo_used_words"] = {
    rxFifoStatus(0).usedWords,
    rxFifoStatus(1).usedWords,
    rxFifoStatus(2).usedWords,
    rxFifoStatus(3).usedWords,
    rxFifoStatus(4).usedWords,
    rxFifoStatus(5).usedWords,
    rxFifoStatus(6).usedWords,
    rxFifoStatus(7).usedWords
  };
  result["rx_fifo_full_latch"] = {
    rxFifoStatus(0).full,
    rxFifoStatus(1).full,
    rxFifoStatus(2).full,
    rxFifoStatus(3).full,
    rxFifoStatus(4).full,
    rxFifoStatus(5).full,
    rxFifoStatus(6).full,
    rxFifoStatus(7).full
  };
  auto xbFifoStatus = xillybusFifoStatus();
  result["xb_fifo_used_words"] = xbFifoStatus.usedWords;
  result["xb_fifo_used_words_max"] = xbFifoStatus.usedWordsMax;
  result["xb_fifo_full_latch"] = xbFifoStatus.overflown;
  result["tofpet_word_counter"] = {
    tofpetWordCounter(0),
    tofpetWordCounter(1),
    tofpetWordCounter(2),
    tofpetWordCounter(3),
    tofpetWordCounter(4),
    tofpetWordCounter(5),
    tofpetWordCounter(6),
    tofpetWordCounter(7),
  };
  if (BoardStartupStatus::getInstance().boardVersion() >= 4) {
    result["b_channel_fifo_full_latch"] = bChannelFifoFullLatch();
  }
  result["leds_disabled"] = ledsDisabled();
  result["fe_enabled"] = feChipsEnabled();
  auto info = fwInfo();
  result["fw_info"] = {{"version", info.buildNumber}, {"date", info.buildDate}, {"time", info.buildTime}};
  result["rx_error_status"] = rxErrorStatus();
  result["watchdog_status"] = watchdogStatus();
  result["ttcrx_ready"] = ttcrx::ttcrxReady();
  result["qpll_locked"] = ttcrx::qpllLocked();
  result["n_ttcrx_ready_latch"] = ttcrx::notTtcrxReadyLatch();
  result["n_qpll_locked_latch"] = ttcrx::notQpllLockedLatch();
  result["fe_reset_reg_cnt"] = feResetRegCounter();
  result["fe_reset_ttc_cnt"] = feResetTtcCounter();
  result["trigger_cnt"] = trigger::triggerCounter();

  result["sipm_temperatures"] = sipmTemperatures();
  result["fe_temperatures"] = feTemperatures();
  if (BoardStartupStatus::getInstance().boardVersion() >= 4) {
    result["board_temperature"] = boardTemperature();
  }

  result["daq_status"] = m_daqManager.toJson();

  json reply;
  reply["response"] = "ok";
  reply["result"] = result;
  return reply;
}

