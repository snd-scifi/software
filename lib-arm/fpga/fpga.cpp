#include <cstdio>
#include <iomanip>
#include <sstream>
#include <unordered_map>
#include <fstream>
#include <unistd.h>
#include <sys/mount.h>

#include "io/xillybus-impl.hpp"
#include "board/board_startup_status.hpp"
#include "fpga/eeprom.hpp"
#include "fpga/registers.hpp"
#include "fpga/pll.hpp"
#include "fpga/spi.hpp"
#include "fpga/ttcrx.hpp"
#include "fpga/temperature.hpp"
#include "tofpet/tofpet.hpp"
#include "utils/delay.hpp"
#include "utils/timer.hpp"

#include "fmt/core.h"
#include "fmt/format.h"

#include "fpga.hpp"

/**
 * Returns true if the FPGA PLL (used to generate the 160 MHz) is locked.
 */
bool pll40Locked() {
  return readRegister(CSR2_ADDR, CSR2_PLL40_LOCKED);
}

/**
 * Pulses the FE reset,
 */
void resetFe() {
  writeRegister(TRIGGER_FE_RESET_ADDR, FE_RESET, 1, true);
  writeRegister(TRIGGER_FE_RESET_ADDR, FE_RESET, 0, true);
}

/**
 * Performs a full FPGA reset, waiting until the reset is released.
 */
void fpgaFullReset() {
  writeRegister(CSR2_ADDR, CSR2_FPGA_FULL_RESET, 1, false);
  while (readRegister(CSR2_ADDR, CSR2_FPGA_FULL_RESET));
}


void initializeBoard() {
  INFO("Performing FPGA full reset.");
  fpgaFullReset();

  spi::initialize();

  INFO("Setting up SiPM temperature sensors");
  setupSipmTemperature(0);
  setupSipmTemperature(1);
  setupSipmTemperature(2);
  setupSipmTemperature(3);

  if (boardRevision() >= 4) {
    INFO("Setting up board temperature sensor");
    setupBoardTemperature();
  }

  INFO("Setting up FE temperature sensors");
  setupFeTemperature(0);
  setupFeTemperature(1);
  setupFeTemperature(2);
  setupFeTemperature(3);

  initializeTtcrx(true);

  tofpetReset();

  // disableLeds(true);
  setLed(LedG0, true);
  setLed(LedG1, false);
  setLed(LedR0, false);
  setLed(LedR1, false);
}

/**
 * Writed the board ID in the FPGA.
 * This value is used in the trigger words.
 */
void dataBoardId(uint8_t id) {
  writeRegister(DATA_CTRL_ADDR, DATA_CTRL_BOARD_ID, id, true);
}

/**
 * Returns the current board ID written in the FPGA.
 */
uint8_t dataBoardId() {
  return readRegister(DATA_CTRL_ADDR, DATA_CTRL_BOARD_ID);
}

/**
 * Returns the board ID written in the EEPROM on the DAQ board PCB.
 */
uint32_t boardId() {
  // if the EEPROM is present, read the ID from it.
  // if it has not been programmed, it will read 0xFFFFFFFF
  if (eeprom::eepromDetected(eeprom::EepromBoard)) {
    return eeprom::read(eeprom::EepromBoard, 0x00);
  }

  // otherwise, try to read the ID from file (thos should happen in V2 boards)
  uint32_t id = 0xFFFFFFFF; // let's have the default value different, to separate the two cases
  std::ifstream f("/root/board_id.txt");
  try {
    f >> id;
  }
  catch (...) {}
  return id;
}

/**
 * Returns the board revision written in the EEPROM on the DAQ board PCB.
 */
uint32_t boardRevision() {
  // if the EEPROM is present, read the ID from it.
  // if it has not been programmed, it will read 0xFFFFFFFF
  if (eeprom::eepromDetected(eeprom::EepromBoard)) {
    return eeprom::read(eeprom::EepromBoard, 0x04);
  }
  // if the EEPROM is not there, the board version is 2
  else {
    return 2;
  }
}

/**
 * Returns the board IDs written in the EEPROMs on the FE board PCB.
 */
std::array<uint32_t, 4> feIds() {
  std::array<uint32_t, 4> ret;
  ret[0] = eeprom::read(eeprom::EepromFe0, 0x00);
  ret[1] = eeprom::read(eeprom::EepromFe1, 0x00);
  ret[2] = eeprom::read(eeprom::EepromFe2, 0x00);
  ret[3] = eeprom::read(eeprom::EepromFe3, 0x00);
  return ret;
}

/**
 * Returns the board revisions written in the EEPROMs on the FE board PCB.
 */
std::array<uint32_t, 4> feRevisions() {
  std::array<uint32_t, 4> ret;
  ret[0] = eeprom::read(eeprom::EepromFe0, 0x04);
  ret[1] = eeprom::read(eeprom::EepromFe1, 0x04);
  ret[2] = eeprom::read(eeprom::EepromFe2, 0x04);
  ret[3] = eeprom::read(eeprom::EepromFe3, 0x04);
  return ret;
}

/**
 * Enables raw data.
 * If raw data is enabled, the data is not processed in the FPGA and each word transmitted by the TOFPETs is transmitted to the CPU as a raw packet.
 * This mode needs to be used when the TOFPET is in counter mode, for example.
 */
void rawDataEnable(bool enable) {
  writeRegister(DATA_CTRL_ADDR, DATA_CTRL_RAW_DATA_EN, enable, true);
}

/**
 * Returns wether raw data is enabled.
 */
bool rawDataEnabled() {
  return readRegister(DATA_CTRL_ADDR, DATA_CTRL_RAW_DATA_EN);
}

/**
 * Enables counter data.
 * If counter data is enabled, just counter packets are transmitted to the CPU as a raw packet.
 */
void counterDataEnable(bool enable) {
  writeRegister(DATA_CTRL_ADDR, DATA_CTRL_COUNTER_DATA_EN, enable, true);
}

/**
 * Returns wether counter data is enabled.
 */
bool counterDataEnabled() {
  return readRegister(DATA_CTRL_ADDR, DATA_CTRL_COUNTER_DATA_EN);
}

/**
 * Disables hit data transmission
 * If true, normal hits are not transmitted to the CPU.
 */
void hitDataDisable(bool disable) {
  writeRegister(DATA_CTRL_ADDR, DATA_CTRL_NOT_DATA_EN, disable, true);
}

/**
 * Returns wether hit data transmission is disabled.
 */
bool hitDataDisabled() {
  return readRegister(DATA_CTRL_ADDR, DATA_CTRL_NOT_DATA_EN);
}

/**
 * Enables the transmission of heartbeat words.
 * Normally heartbeat words are sent by the TOFPETS every ~81k clock cycles (160 MHz), but are discarded by the FPGA.
 * If enable is true, the heartbeats are transmitted as raw packets.
 */
void heartbeatEnable(bool enable) {
  writeRegister(DATA_CTRL_ADDR, DATA_CTRL_HEARTBEAT_EN, enable, true);
}

/**
 * Returns wether the transmission of heartbeat words is enabled.
 */
bool heartbeatEnabled() {
  return readRegister(DATA_CTRL_ADDR, DATA_CTRL_HEARTBEAT_EN);
}

/**
 * Enables the transmission of b-channel data words.
 */
void bChannelDataEnable(bool enable) {
  if (BoardStartupStatus::getInstance().boardVersion() >= 4) {
    writeRegister(TRIGGER_FE_RESET_ADDR, B_CHANNEL_DATA_EN, enable, true);
  }
  else {
    WARN("B-Channel data is not available for boards versions < 4.");
  }
}

/**
 * Returns wether the transmission of b-channel data words is enabled.
 */
bool bChannelDataEnabled() {
  return readRegister(TRIGGER_FE_RESET_ADDR, B_CHANNEL_DATA_EN);
}

/**
 * Returns true is the B-Channel FIFO has ever been full.
 */
bool bChannelFifoFullLatch() {
  return readRegister(TRIGGER_FE_RESET_ADDR, B_CHANNEL_FIFO_FULL_LATCH);
}

/**
 * Returns the status for the RX FIFO identified by id.
 */
FifoStatus rxFifoStatus(uint8_t id) {
  uint16_t address;
  if (0 <= id && id <= 3) {
    address = RX_FIFO_0_3_STATUS_ADDR;
  }
  else if (4 <= id && id <= 7) {
    address = RX_FIFO_4_7_STATUS_ADDR;
  }
  else {
    THROW_IA("id must be between 0 and 7");
  }
  FifoStatus status;
  status.empty = readRegister(address, RX_FIFO_0_STATUS_EMPTY << ((id%4) * RX_FIFO_STATUS_STRIDE));
  status.full = readRegister(address, RX_FIFO_0_STATUS_FULL << ((id%4) * RX_FIFO_STATUS_STRIDE));
  status.usedWords = readRegister(address, RX_FIFO_0_STATUS_USED_WORDS << ((id%4) * RX_FIFO_STATUS_STRIDE));
  status.usedWordsMax = 0; // not implemented in this FIFO
  status.overflown = false; // not implemented for RX FIFOs because they will never overflow, the only one that can overflow is the xillybus FIFO

  return status;
}

/**
 * Returns the status for the Xillybus FIFO.
 */
FifoStatus xillybusFifoStatus() {
  FifoStatus status;
  status.empty = readRegister(XB_FIFO_STATUS_ADDR, XB_FIFO_STATUS_EMPTY);
  status.full = readRegister(XB_FIFO_STATUS_ADDR, XB_FIFO_STATUS_FULL);
  status.overflown = readRegister(XB_FIFO_STATUS_ADDR, XB_FIFO_STATUS_FULL_LATCH);
  status.usedWords = readRegister(XB_FIFO_STATUS_ADDR, XB_FIFO_STATUS_USED_WORDS);
  status.usedWordsMax = readRegister(XB_FIFO_STATUS_ADDR, XB_FIFO_STATUS_USED_WORDS_MAX);
  return status;
}

/**
 * Returns the value for the word counter for a TOFPET identified by id.
 */
uint32_t tofpetWordCounter(uint8_t id) {
  if (7 < id) {
    THROW_IA("id must be between 0 and 7");
  }
  return readRegister(TOFPET_0_WORD_COUNTER_ADDR + id * TOFPET_WORD_COUNTER_ADDR_STRIDE);
}

/**
 * Returns the number of 32-bit words written in the xillybus FIFO.
 */
uint64_t xillybusWordCounter() {
  // reads MSB, LSB and then MSB again.
  auto cntMsb = readRegister(XB_WORD_COUNTER_MSB_ADDR);
  auto cntLsb = readRegister(XB_WORD_COUNTER_LSB_ADDR);
  auto cntMsb2 = readRegister(XB_WORD_COUNTER_MSB_ADDR);

  // if the MSBs are different, a rollover happened, so we need to read again the LSB to be sure it's the right value
  if (cntMsb2 != cntMsb) {
    cntMsb = cntMsb2;
    cntLsb = readRegister(XB_WORD_COUNTER_LSB_ADDR);
  }

  return (static_cast<uint64_t>(cntMsb) << 32) | cntLsb;
}

/**
 * Returns the watchdog status for all the TOFPETs (8-bit value, each bt corresponds to a TOFPET chip).
 * The watchdog is set to 1 if a heartbeat word is not received within ~81k clock cycles after the previous one.
 */
uint8_t watchdogStatus() {
  return readRegister(RX_WATCHDOG_ERROR_ADDR, RX_WATCHDOG_ALL);
}

/**
 * Returns the rx error status for all the TOFPETs (8-bit value, each bt corresponds to a TOFPET chip).
 * This flag is set if a comma word is received before the end of a data word.
 * IT IS ALWAYS SET, EVEN IF DATA IS RECEIVED CORRECTLY. DO NOT TRUST IT.
 */
uint8_t rxErrorStatus() {
  return readRegister(RX_WATCHDOG_ERROR_ADDR, RX_ERROR_ALL);
}

/**
 * Enables or disables the data stream from each TOFPET chip. Id must be between 0 and 7.
 * This does not disable the ASIC. To disable it, you must reset it.
 */
void feChipEnable(uint8_t id, bool enable) {
  if (7 < id) {
    THROW_IA("id must be between 0 and 7");
  }
  writeRegister(CSR1_ADDR, CSR1_FE_CHIP_ENABLE_0 << id, enable, true);
}

/**
 * Returns wether the data stream from a TOFPET chip is enabled. id must be between 0 and 7.
 */
bool feChipEnabled(uint8_t id) {
  if (7 < id) {
    THROW_IA("id must be between 0 and 7");
  }
  return readRegister(CSR1_ADDR, CSR1_FE_CHIP_ENABLE_0 << id);
}

/**
 * Enables or disables the data stream from the tofpet chips.
 * This does not disable the ASIC. To disable it, you must reset it.
 * \param fe each bit corresponds to a TOFPET chip
 */
void feChipsEnable(uint8_t fe) {
  writeRegister(CSR1_ADDR, CSR1_FE_CHIP_ENABLE, fe, true);
}

/**
 * Returns the enabled FE chips (each bit corresponds to a TOFPET chip).
 */
uint8_t feChipsEnabled() {
  return readRegister(CSR1_ADDR, CSR1_FE_CHIP_ENABLE);
}

void rxBitStartValue(uint8_t value) {
  if (0xF < value) {
    THROW_IA("value must be between 0 and 15");
  }
  writeRegister(CSR1_ADDR, CSR1_RX_BIT_START_VALUE, value, true);
}

uint8_t rxBitStartValue() {
  return readRegister(CSR1_ADDR, CSR1_RX_BIT_START_VALUE);
}

/**
 * Enables or disables the data loopback. If it is enabled, data can be written to /dev/xillybus_data_write, it will pass through a FIFO and fed to /dev/xillybus_data.
 * If it is disabled, data from the front-end is fed to /dev/xillybus_data
 */
void dataLoopbackEnable(bool enable) {
  writeRegister(CSR2_ADDR, CSR2_DATA_LOOPBACK_EN, enable, true);
}

/**
 * Returns true is data loopback is enabled.
 */
bool dataLoopbackEnabled() {
  return readRegister(CSR2_ADDR, CSR2_DATA_LOOPBACK_EN);
}

/**
 * Enables or disables all the LEDs on the board.
 */
void disableLeds(bool disable) {
  writeRegister(CSR2_ADDR, CSR2_DISABLE_LEDS, disable, true);
}

/**
 * Sets one of the four green/red LEDs
 */
void setLed(Leds led, bool status) {
  writeRegister(DEBUG_REGISTER_ADDR, DEBUG_LEDS, status << led, true);
}

/**
 * Returns the status of the four green/red LEDs
 */
bool getLed(Leds led) {
  auto val = readRegister(DEBUG_REGISTER_ADDR, DEBUG_LEDS);
  return val & (1 << led);
}

/**
 * Returns wether the LEDs are disabled.
 */
bool ledsDisabled() {
  return readRegister(CSR2_ADDR, CSR2_DISABLE_LEDS);
}

/**
 * Returns the firmware build information (date, time, build number) in a FwInfo structure.
 */
FwInfo fwInfo() {
  FwInfo info;
  auto date = readRegister(BUILD_DATE_ADDR);
  auto time = readRegister(BUILD_TIME_ADDR);
  uint32_t number = readRegister(BUILD_NUMBER_ADDR) & 0x0FFFFFFFu;

  // the build number is saved such as its hexadecimal representation has to be read as decimal.
  // complain with Guido about this...
  info.buildNumber = std::stoi(fmt::format("{:x}", number));
  info.buildDate = fmt::format("{:x}-{:02x}-{:02x}", date & 0x0000FFFF, (date & 0x00FF0000) >> 16, (date & 0xFF000000) >> 24);
  info.buildTime = fmt::format("{:02x}:{:02x}", (time & 0x0000FF00) >> 8, time & 0x000000FF);

  return info;
}

/**
 * Determines wether the clock received by the FPGA goes through the QPLL.
 * This is used to control the clock mux on the board (IC11).
 * It only works with DAQ board V4 and above.
 * If use is true, the clock is received from the QPLL, which in turn is fed by Clock40Des1.
 * If use is false, Clock40Des2 is used directly.
*/
void clock40UseQpll(bool use){
  if (boardRevision() >= 4) {
    INFO("Selecting clock source: {}", use ? "Clock40Des1 + QPLL" : "Clock40Des2");
    writeRegister(CSR2_ADDR, CSR2_CLK_SELECT, use, true);
  }
  else {
    WARN("Cannot set clock source. Board version < 4.");
  }
}

/**
 * Return true if the clock received by the FPGA goes through the QPLL.
*/
bool clock40UseQpll(){
  if (boardRevision() >= 4) {
    return readRegister(CSR2_ADDR, CSR2_CLK_SELECT);
  }
  else {
    return true; // this is always the case for board version < 4.
  }
}


/**
 * Sets the phase (0 to 239) for Clock40Des1, wrt the clock received from the TTC system.
 * This clock is Fed through the QPLL and used in the FPGA.
 */
void clock40Des1Phase(uint8_t phase) {
  INFO("Setting Clk40Des1 phase to {}", phase);
  ttcrx::fineDelay(1, phase);
}

/**
 * Returns the current phase for Clock40Des1.
 */
uint8_t clock40Des1Phase() {
  return ttcrx::fineDelay(1);
}

/**
 * Sets the phase (0 to 239) for Clock40Des2, wrt the clock received from the TTC system.
 * This clock can be used to bypass the QPLL in boards version >= 4.
 */
void clock40Des2Phase(uint8_t phase) {
  INFO("Setting Clk40Des2 phase to {}", phase);
  ttcrx::fineDelay(2, phase);
}

/**
 * Returns the current phase for Clock40Des2.
 */
uint8_t clock40Des2Phase() {
  return ttcrx::fineDelay(2);
}

/**
 * Sets the pahse for the TOFPET clock.
 * It needs to be varied based on the length of the flat cables connecting the DAQ board to the FE board.
 */
void tofpetClockPhase(uint8_t phase) {
  Pll::phase(Pll::PllClk, Pll::kClk160TofpetId, phase);
}

/**
 * Returns the current phase of the TOFPET clock.
 */
uint8_t tofpetClockPhase() {
  return Pll::phase(Pll::PllClk, Pll::kClk160TofpetId);
}

/**
 * Performs a scan of the phase of the TOFPETs RX clock.
 * This function will:
 * - initialize the board, so all the TOFPETs are in their default state, with all channels disabled,
 * - enable the reception of heartbeat packets (these are sent by the TOFPETs every ~500 us in a while, when no other data is sent)
 * - counts how many of these packets are received and how many errors are present
 * For each phase, it collects data for 100 ms and finally returs two vectors:
 * - the first with the number total number of packets received for each phase step
 * - the second with the number of errors for each phase step (wrong packet type)
 * The phase is then chosen by the user to be as far as possible from the zone with fewer packets or more errors 
 */
std::pair<std::vector<uint32_t>, std::vector<uint32_t>> scanTofpetClockPhase() {
  NOTICE("Initializing board");
  initializeBoard();
  NOTICE("TOFPET clock phase scan");
  heartbeatEnable(true);
  auto initialPhase = tofpetClockPhase();
  std::vector<uint32_t> rxPackets(16, 0), errors(16, 0);
  for (uint8_t i = 0; i < 16; i++){
    tofpetClockPhase(i);
    DataStream::getInstance();
    feChipsEnable(0xFF);
    delayMilliseconds(1000);
    feChipsEnable(0x00);
    rxPackets.at(i) = xillybusWordCounter();
    auto data = DataStream::getInstance().read(rxPackets.at(i));
    globalLogger().setMinimalLevel(Logger::Level::Error);
    for (auto it = data.cbegin(); it != data.cend();) {
      auto p = DataPacket::createPacket(0, it, data.cend());
      if (p.type() != DataPacket::Heartbeat) {
        errors.at(i)++;
      }
    }
    globalLogger().setMinimalLevel(Logger::Level::Info);
    NOTICE("phase {}: {} packets received. {} errors.", i, rxPackets.at(i), errors.at(i));
    DataStream::getInstance().close();
    resetFe();
  }
  heartbeatEnable(false);
  tofpetClockPhase(initialPhase);
  return std::make_pair(rxPackets, errors);
}

/**
 * Resets TTCrx and QPLL, waits until they become ready, enables Clock40Des2.
 */
void initializeTtcrx(bool waitReady) {
  INFO("Resetting TTCrx and QPLL.");
  ttcrx::resetTtcrx();
  ttcrx::resetQpll();

  INFO("Waiting for TTCrx ready and QPLL locked...");
  Timer t;
  while (waitReady && (!ttcrx::ttcrxReady() || !ttcrx::qpllLocked())) {
    if (t.elapsed() > 2) {
      auto ttcrxReady = ttcrx::ttcrxReady();
      auto qpllLocked = ttcrx::qpllLocked();
      WARN("Timeout: TTCrx ready: {}, QPLL locked: {}", ttcrxReady, qpllLocked);
      if (ttcrxReady) {
        break;
      }
      WARN("TTCrx not ready: will not be configured.");
      return;
    }
  }

  // set the control register
  // 7: enable clk40 output (non deskewed)
  // 6: Enable Serial B output
  // 5: Enable Parallel output bus
  // 4: Enable ClockL1Accept output
  // 3: Enable Clock40Des2 output
  // 2: SelClock40Des2
  // 1: Enable Event Counter operation
  // 0: Enable Bunch Counter operation
  if (BoardStartupStatus::getInstance().boardVersion() >= 4) {
    ttcrx::ttcrxWrite(3, 0b00101000); // for the new boards, it make sense to enable the parallel output bus 
  }
  else {
    ttcrx::ttcrxWrite(3, 0b00001000);
  }

  // INFO("Enabling Clock40Des2");
  // ttcrx::clock40Des2Enable(true);
  ttcrx::fineDelay(1, 0);
  ttcrx::fineDelay(2, 0);
}

/**
 * Selects the signals to be presented on the debug pin header on the DAQ board.
 */
void debugHeaderOutput(DebugHeaderOut output) {
  writeRegister(DEBUG_REGISTER_ADDR, DEBUG_REGISTER_OUTPUT_SEL, output, true);
}

/**
 * Returns the current signal presented on the debug pin header.
 */
DebugHeaderOut debugHeaderOutput() {
  return static_cast<DebugHeaderOut>(readRegister(DEBUG_REGISTER_ADDR, DEBUG_REGISTER_OUTPUT_SEL));
}

/**
 * Selects the signal to present on the SMA outputs.
 */
void debugSmaOutput(DebugSma sma, DebugSmaOut output) {
  writeRegister(DEBUG_REGISTER_ADDR, sma, output, true);
}

/**
 * Returns the current signal presented on the SMA outputs.
 */
DebugSmaOut debugSmaOutput(DebugSma sma) {
  return static_cast<DebugSmaOut>(readRegister(DEBUG_REGISTER_ADDR, sma));
}

uint64_t currentTimestamp160() {
  // reads MSB, LSB and then MSB again.
  auto cntMsb = readRegister(TIMESTAMP_160_MSB_ADDR);
  auto cntLsb = readRegister(TIMESTAMP_160_LSB_ADDR);
  auto cntMsb2 = readRegister(TIMESTAMP_160_MSB_ADDR);

  // if the MSBs are different, a rollover happened, so we need to read again the LSB to be sure it's the right value
  if (cntMsb2 != cntMsb) {
    cntMsb = cntMsb2;
    cntLsb = readRegister(TIMESTAMP_160_LSB_ADDR);
  }

  return (static_cast<uint64_t>(cntMsb) << 32) | cntLsb;
}

uint8_t feResetRegCounter() {
  return readRegister(RESET_CNT_ADDR, RESET_CNT_FE_REG);
}

uint8_t feResetTtcCounter() {
  return readRegister(RESET_CNT_ADDR, RESET_CNT_FE_TTC);
}

void updateFw(std::string filename) {
    NOTICE("Mounting /dev/mmcblk0p1...");
    auto res = mount("/dev/mmcblk0p1", "/mnt", "vfat", 0, "");

    if (res != 0) {
      THROW_SE(errno, "mount failed");
    }

    auto finalize = [&res] () {
      NOTICE("Syncing...");
      sync();
      NOTICE("Unmounting /dev/mmcblk0p1...");
      res = umount("/mnt");
      if (res != 0) {
        WARN("updateFw: umount failed: {}", std::strerror(errno));
      }
    };

    NOTICE("Copying fpga.rbf...");

    char buf[BUFSIZ];
    size_t size;

    FILE* source = fopen(filename.c_str(), "rb");
    if (!source) {
      ERROR("Error opening source firmware file {}: {}", filename, std::strerror(errno));
      finalize();
      THROW_SE(errno, "Error opening source firmware file {}", filename);
    }

    FILE* dest = fopen("/mnt/fpga.rbf", "wb");
    if (!source) {
      ERROR("Error opening destination firmware file /mnt/fpga.rbf:", std::strerror(errno));
      finalize();
      THROW_SE(errno, "Error opening destination firmware file /mnt/fpga.rbf");
    }
    
    // TODO error check
    while ((size = fread(buf, 1, BUFSIZ, source))) {
        fwrite(buf, 1, size, dest);
    }

    //TODO checksum?

    fclose(source);
    fclose(dest);
    
    finalize();
    NOTICE("New firmware copied successfully, reboot the board to load it.");
}

std::ostream& operator<<(std::ostream& out, const DebugHeaderOut output) {
  switch (output) {
  case DbgHdrNone:
    return out << "NONE";
  case DbgHdrRcv:
    return out << "RCV";
  case DbgHdrTofpetSpi:
    return out << "TOFPET_SPI";
  case DbgHdrSpi:
    return out << "SPI";
  case DbgHdrEepromSpi:
    return out << "EEPROM_SPI";
  case DbgHdrI2c:
    return out << "I2C";
  case DbgHdrTrig:
    return out << "TRIG";
  default:
    return out;
  }
}

std::ostream& operator<<(std::ostream& out, const DebugSmaOut output) {
  switch (output) {
  case DbgSmaNone:
    return out << "NONE";
  case DbgSmaClk40:
    return out << "CLK40";
  case DbgSmaClk40Des2:
    return out << "CLK40_DES2";
  case DbgSmaRxClk:
    return out << "RX_CLK";
  case DbgSmaRxLvdsData:
    return out << "RX_LVDS_DATA";
  case DbgSmaTrigger:
    return out << "TRIGGER";
  case DbgSmaTestPulse:
    return out << "TEST_PULSE";
  case DbgSmaClk40Shift:
    return out << "CLK40_SHIFT";
  case DbgSmaClk160Shift:
    return out << "CLK160_SHIFT";
  case DbgSmaTtcFeReset:
    return out << "TTC_FE_RESET";
  default:
    return out;
  }
}

DebugHeaderOut debugHeaderOutFromString(std::string output) {
  std::unordered_map<std::string, DebugHeaderOut> map{ 
    {"NONE", DbgHdrNone},
    {"RCV", DbgHdrRcv},
    {"TOFPET_SPI", DbgHdrTofpetSpi},
    {"SPI", DbgHdrSpi},
    {"EEPROM_SPI", DbgHdrEepromSpi},
    {"I2C", DbgHdrI2c},
    {"TRIG", DbgHdrTrig},
    {"none", DbgHdrNone},
    {"rcv", DbgHdrRcv},
    {"tofpet_spi", DbgHdrTofpetSpi},
    {"spi", DbgHdrSpi},
    {"eeprom_spi", DbgHdrEepromSpi},
    {"i2c", DbgHdrI2c},
    {"trig", DbgHdrTrig},
  };

  auto it = map.find(output);
  if (it != map.end()) {
      return it->second;
  }
  else {
      THROW_IA("Unknown debug header output: " + output);
  }
}

DebugSmaOut debugSmaOutFromString(std::string output) {
  std::unordered_map<std::string, DebugSmaOut> map{ 
    {"NONE", DbgSmaNone},
    {"CLK40", DbgSmaClk40},
    {"CLK40_DES2", DbgSmaClk40Des2},
    {"RX_CLK", DbgSmaRxClk},
    {"LVDS_DATA", DbgSmaRxLvdsData},
    {"TRIGGER", DbgSmaTrigger},
    {"TEST_PULSE", DbgSmaTestPulse},
    {"CLK40_SHIFT", DbgSmaClk40Shift},
    {"CLK160_SHIFT", DbgSmaClk160Shift},
    {"CLK160_RX", DbgSmaClk160Rx},
    {"CLK160_TOFPET", DbgSmaClk160Tofpet},
    {"TTC_FE_RESET", DbgSmaTtcFeReset},
    {"none", DbgSmaNone},
    {"clk40", DbgSmaClk40},
    {"clk40_des2", DbgSmaClk40Des2},
    {"rx_clk", DbgSmaRxClk},
    {"lvds_data", DbgSmaRxLvdsData},
    {"trigger", DbgSmaTrigger},
    {"test_pulse", DbgSmaTestPulse},
    {"clk40_shift", DbgSmaClk40Shift},
    {"clk160_shift", DbgSmaClk160Shift},
    {"clk160_rx", DbgSmaClk160Rx},
    {"clk160_tofpet", DbgSmaClk160Tofpet},
    {"ttc_fe_reset", DbgSmaTtcFeReset},
  };

  auto it = map.find(output);
  if (it != map.end()) {
      return it->second;
  }
  else {
      THROW_IA("Unknown debug SMA output: " + output);
  }
}


