#include <utility>

#include "fpga/eeprom.hpp"
#include "fpga/fpga.hpp"
#include "fpga/temperature.hpp"
#include "fpga/trigger.hpp"
#include "fpga/ttcrx.hpp"
#include "board/board_startup_status.hpp"
#include "board/board.hpp"
#include "tools/data_generator.hpp"
#include "tofpet/tofpet.hpp"
#include "utils/logger.hpp"
#include "io/xillybus-impl.hpp"

#include "board_commands.hpp"

namespace boardCommands {

std::string exceptionHandler(std::string functionName) {
  try { throw; }
  catch (const json::out_of_range& e) {
    WARN("{}: json::out_of_range: {}", functionName, e.what());
    return e.what();
  }
  catch (const json::type_error& e) {
    WARN("{}: json::type_error: {}", functionName, e.what());
    return e.what();
  }
  catch (const tofpet_error& e) {
    WARN("{}: tofpet_error: {}", functionName, e.what());
    return e.what();
  }
  catch (const std::runtime_error& e) {
    WARN("{}: runtime_error: {}", functionName, e.what());
    return e.what();
  }
  catch (const std::invalid_argument& e) {
    WARN("{}: invalid_argument: {}", functionName, e.what());
    return e.what();
  }
  catch (const std::exception& e) {
    WARN("{}: exception: {}", functionName, e.what());
    return e.what();
  }
  catch (...) {
    WARN("{}: unknown exception", functionName);
    return "unknown exception from catch(...)";
  }
}

/**
 * Closes the DataStream file and reinitializes the board:
 * * Full FPGA reset
 * * Initialize temperature sensors
 * * Initialize TTCrx
 * * Full TOFPET reset and set default configuration
 * * Disable LEDs
 */
json reinitializeBoard(json args) {
  json reply;
  try {
    DataStream::getInstance().close();
    initializeBoard();
    reply["response"] = "ok";
    reply["result"] = {};
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = exceptionHandler("reinitializeBoard");
  }
  return reply;
}

/**
 * Closes the data stream and performs a FE reset.
 */
json feReset(json args) {
  json reply;
  try {
    DataStream::getInstance().close();
    resetFe();
    reply["response"] = "ok";
    reply["result"] = {};
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = exceptionHandler("feReset");
  }
  return reply;
}

/**
 * disables the LEDs. args must contain the bool "disable".
 * Returns "response" = "ok" on success, "err" otherwise and the error reason in "result".
 */
json disableLEDs(json args) {
  json reply;
  try {
    auto disable = args.at("disable").get<bool>();
    disableLeds(disable);
    reply["response"] = "ok";
    reply["result"] = {};
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = exceptionHandler("disableLEDs");
  }
  return reply;
}


json getBoardStartupStatus(json args) {
  json reply;
  try {
    auto reset = args.at("reset").get<bool>();
    if (reset) {
      BoardStartupStatus::getInstance().reset();
    }
    reply["response"] = "ok";
    reply["result"] = BoardStartupStatus::getInstance();
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = exceptionHandler("getBoardStartupStatus");
  }
  return reply;
}

/**
 * 
 */
json getFeConf(json args) {
  json reply;
  try {
    auto ids = args.at("fe_ids").get<std::vector<uint8_t>>();
    auto cfg = tofpetConfMulti(ids);
    reply["response"] = "ok";
    reply["result"]["configurations"] = cfg;
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = exceptionHandler("getFeConf");
  }
  return reply;
}

/**
 * 
 */
json setFeConf(json args) {
  json reply;
  try {
    auto cfgs = args.at("configurations").get<std::map<uint8_t, TofpetCfg>>();
    tofpetConfMulti(cfgs);
    reply["response"] = "ok";
    reply["result"] = {};
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = exceptionHandler("setFeConf");
  }
  return reply;
}

// json setGlobalFeConf(json args) {
//   json reply;
//   try {
//     auto cfgs = args.at("configurations"].get<std::map<uint8_t, std::map<>>>();
//     tofpetConfMulti(cfgs);
//     reply["response"] = "ok";
//     reply["result"] = {};
//   }
//   catch (json::out_of_range& oor) {
//     WARN("setFeConf: arguments configuration is required.");
//     reply["response"] = "err";
//     reply["result"] = "arguments configuration is required";
//   }
//   catch (std::runtime_error& re) {
//     WARN("setFeConf: runtime error: ", re.what());
//     reply["response"] = "err";
//     reply["result"] = re.what();
//   }
//   return reply;
// }

// json setChannelFeConf(json args) {

// }


/**
 * Performs the transimpedance amplifier calibration for a given Tofpet chip ("fe_id", 0 to 7) and a discriminator ("disc", either "T1", "T2" or "E") as described in the datasheet.
 * Returns a 2D array containing the counts for each channel and threshold value (array[channel][threshold]). If the value of a count is 0xFFFFFFFF is invalid and should be ignored.
 * T1 and T2 act on the same baseline. The resulting baseline value for each channel should be the minimum obtained by the two measurements.
 * THIS CALIBRATION MUST BE RUN WITH THE SiPMs BIASED BELOW BREAKDOWN.
 */
json tiaBaselineCalibration(json args) {
  json reply;
  try {
    auto feChs = args.at("fe_chs").get<std::map<uint8_t, std::vector<uint8_t>>>();
    auto disc = discriminatorFromString(args.at("disc").get<std::string>());
    auto cnt = tiaBaselineCalibration(feChs, disc);
    reply["response"] = "ok";
    reply["result"]["counts"] = cnt;
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = exceptionHandler("tiaBaselineCalibration");
  }
  return reply;
}

/**
 * Performs the calibration of the discriminator treshold for a given Tofpet chip ("fe_id", 0 to 7) and a discriminator ("disc", either "T1", "T2" or "E") as described in the datasheet.
 * Returns a 2D array containing the counts for each channel and threshold value (array[channel][threshold]). If the value of a count is 0xFFFFFFFF is invalid and should be ignored.
 * The threshold scan for each channel should be fitted with an erf to determine the threshold level of the baseline and its noise.
 * THIS CALIBRATION MUST BE RUN WITH THE SiPMs BIASED BELOW BREAKDOWN.
 */
json discriminatorThrCalibration(json args) {
  json reply;
  try {
    auto feChs = args.at("fe_chs").get<std::map<uint8_t, std::vector<uint8_t>>>();
    auto disc = discriminatorFromString(args.at("disc").get<std::string>());
    auto cnt = discriminatorThrCalibration(feChs, disc);
    reply["response"] = "ok";
    reply["result"]["counts"] = cnt;
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = exceptionHandler("discriminatorThrCalibration");
  }
  return reply;
}

/**
 * Performs a dark count scan for a given Tofpet chip ("fe_id", 0 to 7) and a discriminator ("disc", either "T1", "T2" or "E") as described in the datasheet.
 * Returns a 2D array containing the counts for each channel and threshold value (array[channel][threshold]). If the value of a count is 0xFFFFFFFF is invalid and should be ignored.
 * This measurements should be performed at the desired overvoltage.
 */
json getDarkCounts(json args) {
  json reply;
  try {
    auto feChs = args.at("fe_chs").get<std::map<uint8_t, std::vector<uint8_t>>>();
    auto disc = discriminatorFromString(args.at("disc").get<std::string>());
    auto minDuration = args.at("min_duration").get<float>();
    auto minEvents = args.at("min_events").get<uint64_t>();
    auto maxDuration = args.at("max_duration").get<float>();
    auto cnt = getDarkCounts(feChs, disc, minDuration, minEvents, maxDuration);
    reply["response"] = "ok";
    reply["result"]["durations"] = cnt.first;
    reply["result"]["counts"] = cnt.second;
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = exceptionHandler("getDarkCounts");
  }
  return reply;
}

json getDarkCounts2(json args) {
  json reply;
  try {
    auto feChs = args.at("fe_chs").get<std::map<uint8_t, std::vector<uint8_t>>>();
    auto disc = discriminatorFromString(args.at("disc").get<std::string>());
    auto minDuration = args.at("min_duration").get<float>();
    auto minEvents = args.at("min_events").get<uint64_t>();
    auto maxDuration = args.at("max_duration").get<float>();
    auto cnt = getDarkCounts2(feChs, disc, minDuration, minEvents, maxDuration);
    reply["response"] = "ok";
    reply["result"]["durations"] = cnt.first;
    reply["result"]["counts"] = cnt.second;
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = exceptionHandler("getDarkCounts");
  }
  return reply;
}

/**
 * Performs a calibration of the TDCs for the given front-end chips and channels. It is necessary to specify the phase of the test pulse "phase" (0 to 239, 104 ps steps) and the number of test pulses to send "n_events".
 * Returns an array of data packets that need to be processed by the caller.
 */
std::pair<json, std::vector<DataPacket>> tdcCalibration(json args) {
  json reply;
  try {
    auto feChs = args.at("fe_chs").get<std::map<uint8_t, std::vector<uint8_t>>>();
    auto phase = args.at("phase").get<uint8_t>();
    auto nEvents = args.at("n_events").get<uint32_t>();
    auto simCh = args.at("simultaneous_channels").get<uint8_t>();
    std::vector<uint8_t> channels = std::vector<uint8_t>();
    auto data = ::tdcQdcCalibration(true, feChs, phase, 1, nEvents, simCh);
    reply["response"] = "ok";
    reply["result"] = "raw_data_follows";
    return std::make_pair(reply, data);
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = exceptionHandler("tdcCalibration");
  }
  return std::make_pair(reply, std::vector<DataPacket>());
}

/**
 * Performs a calibration of the QDCs for the given front-end chips and channels. It is necessary to specify the phase of the test pulse "phase" (0 to 239, 104 ps steps), the pulse duration "duration" (from 1 to 105) and the number of test pulses to send "n_events".
 * Returns an array of data packets that need to be processed by the caller.
 */
std::pair<json, std::vector<DataPacket>> qdcCalibration(json args) {
  json reply;
  try {
    auto feChs = args.at("fe_chs").get<std::map<uint8_t, std::vector<uint8_t>>>();
    auto phase = args.at("phase").get<uint8_t>();
    auto duration = args.at("duration").get<uint8_t>();
    auto nEvents = args.at("n_events").get<uint32_t>();
    auto simCh = args.at("simultaneous_channels").get<uint8_t>();
    std::vector<uint8_t> channels = std::vector<uint8_t>();
    auto data = ::tdcQdcCalibration(false, feChs, phase, duration, nEvents, simCh);
    reply["response"] = "ok";
    reply["result"] = "raw_data_follows";
    return std::make_pair(reply, data);
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = exceptionHandler("qdcCalibration");
  }
  return std::make_pair(reply, std::vector<DataPacket>());
}

json setTrigger(json args) {
  json reply;
  std::vector<std::string> errors;

  if (args.contains("sources")) {
    try {
      auto sources = args.at("sources").get<std::vector<std::string>>();
      trigger::trigSources(sources);
    }
    catch (...) {
      errors.push_back(exceptionHandler("setTrigger"));
    }
  }

  if (args.contains("seq_counter")) {
    try {
      auto seqCounter = args.at("seq_counter").get<uint32_t>();
      trigger::trigSeqCounter(seqCounter);
    }
    catch (...) {
      errors.push_back(exceptionHandler("setTrigger"));
    }
  }

  if (args.contains("seq_period")) {
    try {
      auto seqPeriod = args.at("seq_period").get<uint32_t>();
      trigger::trigSeqPeriod(seqPeriod);
    }
    catch (...) {
      errors.push_back(exceptionHandler("setTrigger"));
    }
  }

  if (errors.size() == 0) {
    reply["response"] = "ok";
    reply["result"] = {};
  }
  else {
    reply["response"] = "err";
    reply["result"] = errors;
  }

  return reply;

}

json getTrigger(json args) {
  json reply;
  json result;

  try {
    auto sources = trigger::trigSources();
    auto seqCounter = trigger::trigSeqCounter();
    auto seqCounterRemaining = trigger::trigSeqCounterRemaining();
    auto seqPeriod = trigger::trigSeqPeriod();
    auto seqBusy = trigger::trigSeqBusy();

    result["sources"] = sources;
    result["seq_counter"] = seqCounter;
    result["seq_counter_remaining"] = seqCounterRemaining;
    result["seq_period"] = seqPeriod;
    result["seq_busy"] = seqBusy;
    
    reply["response"] = "ok";
    reply["result"] = result;
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = exceptionHandler("getTrigger");
  }
  return reply;
}

json startTriggerSequencer(json args) {
  json reply;

  try {
    auto block = args.at("block").get<bool>();
    trigger::trigSeqStart();
    if (block) {
      while (trigger::trigSeqBusy());
    }
    reply["response"] = "ok";
    reply["result"] = {};
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = exceptionHandler("startTriggerSequencer");
  }
  return reply;
}

json setInjection(json args){
  json reply;

  try {
    auto enable = args.value<bool>("enable", trigger::injectionEnabled());
    auto tofpetEnable = args.value<bool>("tofpet_enable", trigger::injectionTofpetEnabled());
    auto laserEnable = args.value<bool>("laser_enable", trigger::injectionLaserEnabled());
    auto phase = args.value<uint8_t>("phase", trigger::injectionPhase());
    auto duration = args.value<uint8_t>("duration", trigger::injectionPulseDuration());
    auto clk = args.value<trigger::TestPulseClock>("clock", trigger::testPulseClock());

    trigger::injectionEnable(enable);
    trigger::injectionTofpetEnable(tofpetEnable);
    trigger::injectionLaserEnable(laserEnable);
    trigger::injectionPulseDuration(duration);
    trigger::injectionPhase(phase);
    trigger::testPulseClock(clk);

    reply["response"] = "ok";
    reply["result"] = {};
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = exceptionHandler("setInjection");
  }
  return reply;
}

json getInjection(json args){
  json reply;
  json result;

  try {
    auto enabled = trigger::injectionEnabled();
    auto tofpetEnabled = trigger::injectionTofpetEnabled();
    auto phase = trigger::injectionPhase();
    auto duration = trigger::injectionPulseDuration();
    auto clk = trigger::testPulseClock();

    result["enabled"] = enabled;
    result["tofpet_enabled"] = tofpetEnabled;
    result["phase"] = phase;
    result["duration"] = duration;
    result["clk"] = clk;
    
    reply["response"] = "ok";
    reply["result"] = result;
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = exceptionHandler("getInjection");
  }
  return reply;
}

json setFeDigitalDebugOutput(json args) {
  json reply;
  try {
    auto feSet = args.at("fe_set").get<std::map<uint8_t, json>>();

    std::vector<uint8_t> keys;
    for(auto it = feSet.begin(); it != feSet.end(); ++it) {
      keys.push_back(it->first);
    }

    auto cfgs = tofpetConfMulti(keys);

    for (const auto& [id, setting] : feSet) {
      auto en = setting.at("enable").get<bool>();
      cfgs.at(id).global.setDigitalDebugOutputEnable(en);
      for (uint8_t ch = 0; ch < 64; ch++) {
        // if the channel is the desired one AND enable is true, then set the desired setting, otherwise disable
        auto mode = setting.at("channel").get<uint8_t>() == ch && en ? digitalDebugModeFromString(setting.at("mode").get<std::string>()) : TofpetChannelCfg::DBG_DISABLED;
        cfgs.at(id).channels.at(ch).setDigitalDebugMode(mode);
      }
    }

    tofpetConfMulti(cfgs);

    reply["response"] = "ok";
    reply["result"] = {};
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = exceptionHandler("setFeDigitalDebugOutput");
  }
  return reply;
}

json setFeAnalogDebugOutput(json args) {
  json reply;
  try {
    auto feIds = args.at("fe_set").get<std::map<uint8_t, bool>>();

    std::vector<uint8_t> keys;
    for(std::map<uint8_t, bool>::iterator it = feIds.begin(); it != feIds.end(); ++it) {
      keys.push_back(it->first);
    }

    auto cfgs = tofpetGlobalConfMulti(keys);
    for (const auto& [id, en] : feIds) {
      cfgs.at(id).setAnalogDebugOutputEnable(en);
    }

    tofpetGlobalConfMulti(cfgs);

    reply["response"] = "ok";
    reply["result"] = {};
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = exceptionHandler("setFeAnalogDebugOutput");
  }
  return reply;
}

json setBoardDebug(json args) {
  json reply;
  std::vector<std::string> errors;

  if (args.contains("sma_0")) {
    try {
      debugSmaOutput(DbgSma0, debugSmaOutFromString(args.at("sma_0").get<std::string>()));
    }
    catch (...) {
      errors.push_back(exceptionHandler("setBoardDebug"));
    }
  }

  if (args.contains("sma_1")) {
    try {
      debugSmaOutput(DbgSma1, debugSmaOutFromString(args.at("sma_1").get<std::string>()));
    }
    catch (...) {
      errors.push_back(exceptionHandler("setBoardDebug"));
    }
  }

  if (args.contains("header")) {
    try {
      debugHeaderOutput(debugHeaderOutFromString(args.at("header").get<std::string>()));
    }
    catch (...) {
      errors.push_back(exceptionHandler("setBoardDebug"));
    }
  }

  if (errors.size() == 0) {
    reply["response"] = "ok";
    reply["result"] = {};
  }
  else {
    reply["response"] = "err";
    reply["result"] = errors;
  }

  return reply;
}

json setClk40Des1Phase(json args) {
  json reply;

  try {
    auto phase = args.at("phase").get<uint8_t>();

    clock40Des1Phase(phase);

    reply["response"] = "ok";
    reply["result"] = {};
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = exceptionHandler("setClk40Des1Phase");
  }
  return reply;
}

json getClk40Des1Phase(json args) {
  json reply;
  json result;

  try {
    auto phase = clock40Des1Phase();

    result["phase"] = phase;
    
    reply["response"] = "ok";
    reply["result"] = result;
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = exceptionHandler("getClk40Des1Phase");
  }
  return reply;
}

json setClk40Des2Phase(json args) {
  json reply;

  try {
    auto phase = args.at("phase").get<uint8_t>();

    clock40Des2Phase(phase);

    reply["response"] = "ok";
    reply["result"] = {};
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = exceptionHandler("setClk40Des2Phase");
  }
  return reply;
}

json getClk40Des2Phase(json args) {
  json reply;
  json result;

  try {
    auto phase = clock40Des2Phase();

    result["phase"] = phase;
    
    reply["response"] = "ok";
    reply["result"] = result;
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = exceptionHandler("getClk40Des2Phase");
  }
  return reply;
}

json setClk40UseQpll(json args) {
  json reply;

  try {
    auto useQpll = args.at("use_qpll").get<bool>();

    clock40UseQpll(useQpll);

    reply["response"] = "ok";
    reply["result"] = {};
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = exceptionHandler("setClk40UseQpll");
  }
  return reply;
}

json getClk40UseQpll(json args) {
  json reply;
  json result;

  try {
    auto useQpll = clock40UseQpll();

    result["use_qpll"] = useQpll;
    
    reply["response"] = "ok";
    reply["result"] = result;
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = exceptionHandler("getClk40UseQpll");
  }
  return reply;
}

json setClkTofpetPhase(json args) {
  json reply;

  try {
    auto phase = args.at("phase").get<uint8_t>();

    tofpetClockPhase(phase);

    reply["response"] = "ok";
    reply["result"] = {};
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = exceptionHandler("setClkTofpetPhase");
  }
  return reply;
}

json getClkTofpetPhase(json args) {
  json reply;
  json result;

  try {
    auto phase = tofpetClockPhase();

    result["phase"] = phase;
    
    reply["response"] = "ok";
    reply["result"] = result;
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = exceptionHandler("getClkTofpetPhase");
  }
  return reply;
}


json scanClkTofpetPhase(json args) {
  json reply;
  json result;

  try {
    auto scanResult = scanTofpetClockPhase();

    result["received_packets"] = scanResult.first;
    result["errors"] = scanResult.second;
    
    reply["response"] = "ok";
    reply["result"] = result;
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = exceptionHandler("scanClkTofpetPhase");
  }
  return reply;
}


json generateData(json args){
  json reply;

  try {
    auto n = args.at("n_events").get<uint32_t>();
    auto delay = args.at("delay").get<uint32_t>();
    dataLoopbackEnable(true);
    dataGenerator(n, delay);
    dataLoopbackEnable(false);
    reply["response"] = "ok";
    reply["result"] = {};
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = exceptionHandler("generateData");
  }
  return reply;
}


json setEepromId(json args) {
  json reply;

  try {
    auto es = args.at("which").get<eeprom::EepromSelect>();
    auto id = args.at("id").get<uint32_t>();
    auto revision = args.at("revision").get<uint32_t>();

    eeprom::write(es, 0x00, id);
    eeprom::write(es, 0x04, revision);

    BoardStartupStatus::getInstance().reset();

    if (eeprom::read(es, 0x00) == id && eeprom::read(es, 0x04) == revision) {
      reply["response"] = "ok";
      reply["result"] = {};
    }
    else {
      reply["response"] = "err";
      reply["result"] = "wrong values read back.";
    }
    
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = exceptionHandler("setEepromId");
  }
  return reply;
}


json updateFirmware(json args) {
  json reply;

  try {
    updateFw();

    reply["response"] = "ok";
    reply["result"] = {};
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = exceptionHandler("updateFirmware");
  }
  return reply;
}

json rebootBoard(json args) {
  json reply;

  try {

    board::rebootBoard();
    
    reply["response"] = "ok";
    reply["result"] = {};
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = exceptionHandler("rebootBoard");
  }
  return reply;
}


json setBoardServerBootStart(json args) {
  json reply;

  try {
    auto enable = args.at("enable").get<bool>();
    
    board::setBootStartup(enable);
    
    reply["response"] = "ok";
    reply["result"] = {};
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = exceptionHandler("setBoardServerBootStart");
  }
  return reply;
}


json setDateTime(json args) {
  json reply;

  try {
    auto datetime = args.at("date_time").get<std::string>();
    
    board::setDateTime(datetime);
    
    reply["response"] = "ok";
    reply["result"] = {};
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = exceptionHandler("setDateTime");
  }
  return reply;
}

} // namespace boardCommands