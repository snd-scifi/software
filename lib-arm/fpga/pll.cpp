#include "fpga/registers.hpp"
#include "utils/logger.hpp"

#include "pll.hpp"

/**
 * Returns wether a PLL is locked.
 */
bool Pll::locked(PllSelect pll) {
  switch (pll) {
  case PllClk:
    return readRegister(PLL_CSR_ADDR, PLL_40_CSR_LOCKED);  
  case PllTestPulse:
    return readRegister(PLL_CSR_ADDR, PLL_TP_CSR_LOCKED);
  default:
    THROW_IA("uknown PllSelect");
  }
  
}

/**
 * Set the phase for the 'id' (0 to 3) ouptut of a PLL.
 */
void Pll::phase(const PllSelect pll, const uint8_t id, const uint8_t phase) {
  if (3 < id) {
    THROW_IA("id must be < 4.");
  }

  DEBUG("PLL {}, id {}: setting phase to ", pll, id, phase);

  // calculate the phase difference wrt the current phase
  auto phaseDiff = static_cast<int>(phase) - static_cast<int>(Pll::phase(pll, id));
  
  // if the difference is negative, we need to set down counting and change sign
  bool up = true;
  if (phaseDiff < 0) {
    up = false;
    phaseDiff = -phaseDiff;
  }

  //setup the phase shifting and apply the changes
  setPhaseShift(pll, id, up);
  for (int i = 0; i < phaseDiff; i++) {
    pulsePhaseEn(pll);
  }
}

/**
 * Return the current phase value of the 'id' (0 to 3) output a PLL.
 */
uint8_t Pll::phase(const PllSelect pll, const uint8_t id) {
  if (3 < id) {
    THROW_IA("id must be < 4.");
  }

  switch (pll) {
    case PllClk:
      return readRegister(PLL_40_PHASE_STATUS, 0xFF << (id * 8));
    case PllTestPulse:
      return readRegister(PLL_TP_PHASE_STATUS, 0xFF << (id * 8));
    default:
      THROW_IA("uknown PllSelect");
  }
}

/**
 * Sets which output clock's phase will be changed and wether it will be increased or decreased.
 */
void Pll::setPhaseShift(const PllSelect pll, const uint8_t id, const bool up) {
  switch (pll) {
    case PllClk:
      writeRegister(PLL_CSR_ADDR, PLL_40_CSR_UP_NDN, up, true);
      writeRegister(PLL_CSR_ADDR, PLL_40_CSR_CNT_SEL, id, true);
      break;
    case PllTestPulse:
      writeRegister(PLL_CSR_ADDR, PLL_TP_CSR_UP_NDN, up, true);
      writeRegister(PLL_CSR_ADDR, PLL_TP_CSR_CNT_SEL, id, true);
      break;
    default:
      THROW_IA("uknown PllSelect");
  }
  
}

/**
 * Changes the phase, according to the settings set by setPhaseShift, by one step.
 */
void Pll::pulsePhaseEn(const PllSelect pll) {
  switch (pll) {
    case PllClk:
      writeRegister(PLL_CSR_ADDR, PLL_40_CSR_PHASE_EN, 1, true);
      writeRegister(PLL_CSR_ADDR, PLL_40_CSR_PHASE_EN, 0, true); 
      while (!readRegister(PLL_CSR_ADDR, PLL_40_CSR_PHASE_DONE));
      break;
    case PllTestPulse:
      writeRegister(PLL_CSR_ADDR, PLL_TP_CSR_PHASE_EN, 1, true);
      writeRegister(PLL_CSR_ADDR, PLL_TP_CSR_PHASE_EN, 0, true);
      while (!readRegister(PLL_CSR_ADDR, PLL_TP_CSR_PHASE_DONE));
      break;
    default:
      THROW_IA("uknown PllSelect");
  }
}

std::ostream& operator<<(std::ostream& out, const Pll::PllSelect pll) {
  switch (pll)
  {
  case Pll::PllClk:
    out << "PLL_CLK";
    break;
  case Pll::PllTestPulse:
    out << "PLL_TP";
    break;
  default:
    out << "*** UNKNOWN PLL ***";
    break;
  }
  return out;
}
