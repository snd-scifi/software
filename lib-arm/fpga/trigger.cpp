#include <unordered_map>

#include "io/xillybus-impl.hpp"
#include "fpga/registers.hpp"
#include "fpga/fpga.hpp"
#include "fpga/pll.hpp"
#include "fpga/ttcrx.hpp"

#include "trigger.hpp"

namespace trigger {

/**
 * Returns the timestamp of the last recorded trigger.
 */
uint64_t lastTrigTimestamp() {
  // reads MSB, LSB and then MSB again.
  auto cntMsb = readRegister(LAST_TRIG_TS_MSB_ADDR);
  auto cntLsb = readRegister(LAST_TRIG_TS_LSB_ADDR);
  auto cntMsb2 = readRegister(LAST_TRIG_TS_MSB_ADDR);

  // if the MSBs are different, a rollover happened, so we need to read again the LSB to be sure it's the right value
  if (cntMsb2 != cntMsb) {
    cntMsb = cntMsb2;
    cntLsb = readRegister(LAST_TRIG_TS_LSB_ADDR);
  }

  return (static_cast<uint64_t>(cntMsb) << 32) | cntLsb;
}

/**
 * Sets the trigger sequencer period, in 40 MHz clock cycles.
 */
void trigSeqPeriod(uint32_t period) {
  writeRegister(TRIG_SEQ_PERIOD_ADDR, period, true);
}

/**
 * Returns the trigger sequencer period.
 */
uint32_t trigSeqPeriod() {
  return readRegister(TRIG_SEQ_PERIOD_ADDR);
}

/**
 * Sets the trigger sequencer counter.
 */
void trigSeqCounter(uint32_t counter) {
  writeRegister(TRIG_SEQ_COUNTER_ADDR , counter, true);
}

/**
 * Returns the trigger sequencer counter value.
 * This is the fixed value set with trigSeqCounter(...), and NOT the remaining triggers before the sequencer is finished!
 * To get the remaining triggers, use trigSeqCounterRemaining().
 */
uint32_t trigSeqCounter() {
  return readRegister(TRIG_SEQ_COUNTER_ADDR);
}

/**
 * Returns the remaining triggers before the sequencer is finished.
 * If the sequencer is not running, it returns 0.
 */
uint32_t trigSeqCounterRemaining() {
  return readRegister(TRIG_SEQ_REMAIN_CNT_ADDR);
}

static const std::unordered_map<std::string, uint8_t> triggerSourcesMap {
  {"sequencer", TRIG_SOURCE_SEQ},
  {"sma", TRIG_SOURCE_SMA},
  {"lemo", TRIG_SOURCE_LEMO},
  {"ttc", TRIG_SOURCE_TTC},
};

/**
 * Selects the trigger sources.
 * The argument is a vector containing the names of the desired sources.
 */
void trigSources(std::vector<std::string> sources) {
  uint8_t trigSourcesMask = 0;

  for (const auto& source : sources) {
    auto it = triggerSourcesMap.find(source);
    if (it != triggerSourcesMap.end()) {
        trigSourcesMask |= it->second;
    }
    else {
        THROW_IA("Unknown trigger source: {}", source);
    }
  }

  writeRegister(TRIGGER_FE_RESET_ADDR, TRIGGER_SOURCE_MASK, trigSourcesMask, true);
}

/**
 * Returns the current trigger sources.
 */
std::vector<std::string> trigSources() {
  auto trigSourceMask = readRegister(TRIGGER_FE_RESET_ADDR, TRIGGER_SOURCE_MASK);
  std::vector<std::string> ret;

  for (const auto& [trss, trsm] : triggerSourcesMap) {
    if (trigSourceMask & trsm) {
      ret.push_back(trss);
    }
  }

  return ret;
}

/**
 * Starts the trigger sequencer.
 */
void trigSeqStart() {
  writeRegister(TRIGGER_FE_RESET_ADDR, TRIGGER_SEQ_START, 1, true);
  writeRegister(TRIGGER_FE_RESET_ADDR, TRIGGER_SEQ_START, 0, true);
}

/**
 * Returns true if the trigger sequencer is still generating triggers.
 */
bool trigSeqBusy() {
  return readRegister(TRIGGER_FE_RESET_ADDR, TRIGGER_SEQ_BUSY);
}

/**
 * Returns the current trigger counter value (28 bits).
 */
uint32_t triggerCounter() {
  return readRegister(TRIGGER_CNT_ADDR);
}

/**
 * Enables injection.
 * If it is enabled, a pulse is generated at each trigger.
 * This pulse can be sent to the TEST_PULSE input of the TOFPETs, using injectionTofpetEnable(...), or to a debug output.
 * By default it goes nowhere.
 */
void injectionEnable(bool enable) {
  writeRegister(TRIGGER_FE_RESET_ADDR, TRIGGER_INJ_EN, enable, true);
}

/**
 * Returns wether the injection is enabled.
 */
bool injectionEnabled() {
  return readRegister(TRIGGER_FE_RESET_ADDR, TRIGGER_INJ_EN);
}

/**
 * Sends the injection pulse to the TEST_PULSE input of the TOPFETS.
 * Requires injection to be enabled with injectionEnable(...)
 */
void injectionTofpetEnable(bool enable) {
  writeRegister(INJECTION_ADDR, INJECTION_TOFPET_ENABLE, enable, true);
}

bool injectionTofpetEnabled() {
  return readRegister(INJECTION_ADDR, INJECTION_TOFPET_ENABLE);
}


/**
 * Sends the injection pulse to the laser header.
 * Requires injection to be enabled with injectionEnable(...)
 */
void injectionLaserEnable(bool enable) {
  writeRegister(INJECTION_ADDR, INJECTION_LASER_ENABLE, enable, true);
}

bool injectionLaserEnabled() {
  return readRegister(INJECTION_ADDR, INJECTION_LASER_ENABLE);
}

/**
 * Selects the clock used to generate the test pulse.
 * It will select either a 40 or 160 MHz clock.
 * The latter allows for a finer pulse length selection (6.25 ns), at the expense of fewer phase shift points and should be used for QDC calibration.
 * The former allows a full 25 ns phase shift, but the pulse length is a multiple of 25 ns and should be used for TDC calibration.
 * The phase shift step is always 111 ps.
 */
void testPulseClock(TestPulseClock clk) {
  writeRegister(INJECTION_ADDR, INJECTION_CLK_160_N40, clk, true);
}

/**
 * Returns the current clock used to generate the test pulse.
 */
TestPulseClock testPulseClock() {
  return static_cast<TestPulseClock>(readRegister(INJECTION_ADDR, INJECTION_CLK_160_N40));
}

/**
 * Used for syncronization of the injection with the shifted clock.
 */
void injectionSyncRisingEdge(bool risingEdge) {
  writeRegister(INJECTION_ADDR, INJECTION_SYNC_RISING_EDGE, risingEdge, true);
}

// bool injectionSyncRisingEdge() {
//   return readRegister(INJECTION_ADDR, INJECTION_SYNC_RISING_EDGE);
// }

/**
 * Selects the injection pulse duration in 40 MHz clock cycles.
 */
void injectionPulseDuration(uint8_t duration) {
  writeRegister(INJECTION_ADDR, INJECTION_DURATION, duration, true);
}

/**
 * Returns the injection pulse duration.
 */
uint8_t injectionPulseDuration() {
  return readRegister(INJECTION_ADDR, INJECTION_DURATION);
}

/**
 * Selects the injection phase with respect to the 40 MHz clock.
 * Phase must be between 0 and 223, so one 25 ns clock cycle is divided in 224 parts.
 */
void injectionPhase(uint8_t phase) {
  // the change between using the rising and falling edge is set at 150 (see below):
  // this implies that the smallest delay between trigger and test pulse is this phase value.
  // Therefore, to have a consistent change of the trigger-test_pulse delay, we set the zero at a phase of 150.
  // now increasing the phase, will always increase the delay between trigger and test_pulse.
  phase = (static_cast<int>(phase) + 150) % Pll::kPllTestPulsePhaseSteps;

  injectionSyncRisingEdge(phase < 150);

  // int curPhase1 = ttcrx::fineDelay(1); //would be used if Clk40Des2 is used for test pulse generation
  // ttcrx::fineDelay(2, (curPhase1+phase)%240);

  // move both phases at the same time
  Pll::phase(Pll::PllTestPulse, Pll::kTestPulse40Id, phase);
  Pll::phase(Pll::PllTestPulse, Pll::kTestPulse160Id, phase);

}

/**
 * Return the current injection phase.
 */
uint8_t injectionPhase() {
  // return ttcrx::getRelativeFineDelay(); //would be used if Clk40Des2 is used for test pulse generation

  // see above why the returned phase is like this.
  return (Pll::phase(Pll::PllTestPulse, Pll::kTestPulse40Id) - 150 + Pll::kPllTestPulsePhaseSteps) % Pll::kPllTestPulsePhaseSteps;
}

} //namespace trigger