
#include "eeprom.hpp"

namespace eeprom {

static constexpr uint8_t WREN = 0b00000110;
static constexpr uint8_t WRDI = 0b00000100;
static constexpr uint8_t RDSR = 0b00000101;
static constexpr uint8_t WRSR = 0b00000001;
static constexpr uint8_t READ = 0b00000011;
static constexpr uint8_t WRITE = 0b00000010;
static constexpr uint8_t RDID = 0b10000011;
static constexpr uint8_t WRID = 0b10000010;
static constexpr uint8_t RDLS = 0b10000011;
static constexpr uint8_t LID = 0b10000010;

static constexpr uint8_t SR_WIP = 0x01;
static constexpr uint8_t SR_WEL = 0x02;
static constexpr uint8_t SR_BP0 = 0x04;
static constexpr uint8_t SR_BP1 = 0x08;
static constexpr uint8_t SR_SRWD = 0x80;

uint32_t read(EepromSelect es, uint16_t address) {
  if (!eepromDetected(es)) {
    return 0xFFFFFFFF; //TODO consider exception
  }
  auto cs = static_cast<spi::CS>(es);
  spi::write(spi::SpiLen56, cs, (READ << 24) | (address << 8));
  return spi::read();
}

void write(EepromSelect es, uint16_t address, uint32_t data) {
  if (!eepromDetected(es)) {
    return; //TODO consider exception
  }
  auto cs = static_cast<spi::CS>(es);

  for (int i = 0; i < 4; i++) {
    spi::write(spi::SpiLen8, cs, WREN << 24);
    // while (!(readSR(es) & SR_WEL)); //TODO this would block if no EEPROM is present, maybe put a limit on the number of iterations
    spi::write(spi::SpiLen32, cs, (WRITE << 24) | ((address + i) << 8) | ((data >> (24 - 8*i)) & 0xFF));
    while (readSR(es) & SR_WIP); //TODO this blocks if no EEPROM is present, maybe put a limit on the number of iterations
  }
}

/**
 * Read the status register of the EEPROM.
 */
uint8_t readSR(EepromSelect es) {
  auto cs = static_cast<spi::CS>(es);
  spi::write(spi::SpiLen16, cs, RDSR << 24);
  return spi::read() & 0xFF;
}

/**
 * Returns true if the EEPROM es is detected.
 * This works by checking that the bits 6:4 of the status register are 0.
 * If the EEPROM in not connected, they will be 1, as every read request will read 0xFF.
 */
bool eepromDetected(EepromSelect es) {
  return (readSR(es) & 0x30) == 0;
}

// the installed eeprom in DAQ boards v3 and TOFPET v1 don't have the ID page

// uint8_t readId(EepromSelect es, uint16_t address) {
//   auto cs = static_cast<spi::CS>(es);
//   address &= 0x007F; // 128 bytes available in total
//   spi::write(spi::SpiLen32, cs, (RDID << 24) | (address << 8));
//   return spi::read() & 0xFF;
// }

// void writeId(EepromSelect es, uint16_t address, uint8_t data) {
//   auto cs = static_cast<spi::CS>(es);
//   address &= 0x007F; // 128 bytes available in total
//   // write a byte
//   spi::write(spi::SpiLen32, cs, (WRID << 24) | (address << 8) | data);
//   // wait for Write In Progress to return 0
//   while (readSR(es) & SR_WIP);
// }




} // namespace eeprom


