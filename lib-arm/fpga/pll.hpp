#pragma once

#include <cstdint>


class Pll {
public:
  enum PllSelect {
    PllClk,
    PllTestPulse,
  };

  static constexpr uint8_t kPllClkPhaseSteps{64};
  static constexpr uint8_t kPllTestPulsePhaseSteps{224};
  
  static constexpr uint8_t kClk40Id{0};
  static constexpr uint8_t kClk160RxId{1};
  static constexpr uint8_t kClk160TofpetId{2};

  static constexpr uint8_t kTestPulse40Id{0};
  static constexpr uint8_t kTestPulse160Id{1};

  Pll() = delete;

  static void phase(const PllSelect pll, const uint8_t id, const uint8_t phase);
  static uint8_t phase(const PllSelect pll, const uint8_t id);
  static bool locked(const PllSelect pll);

private:
  static void setPhaseShift(const PllSelect pll, const uint8_t id, const bool up);
  static void pulsePhaseEn(const PllSelect pll);

};

std::ostream& operator<<(std::ostream& out, const Pll::PllSelect pll);

