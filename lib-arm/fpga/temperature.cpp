#include <cmath>

#include "fpga/registers.hpp"
#include "fpga/spi.hpp"
#include "utils/logger.hpp"
#include "utils/delay.hpp"
#include "temperature.hpp"

float getPtTemperature(float r, float r0) {
  constexpr float A = 3.9083e-3;
  constexpr float B = -5.7750e-7;

  return (-r0 * A + std::sqrt(r0*r0*A*A - 4*r0*B*(r0-r))) / (2*r0*B);
}

/**
 * Reads the SiPM temperature, measured by the sensor present on each FE board, and returns it value in °C.
 * id must be between 0 and 3 for the SiMs and 4 for the board sensor.
 */
float sipmTemperature(uint8_t id) {
  constexpr float R_REF = 4020; //ohm
  constexpr float R_0 = 1000; // ohm

  // resistance is stored in bits 15-1 of registers 0x1 (MSB) and 0x2 (LSB)
  float R = (readMax31865(id, 1) << 7) | (readMax31865(id, 2) >> 1);

  if (R > 32766) {
    return NAN;
  }

  // and it is given as the ratio between Rrtd and Rref (times 2^15)
  R *= R_REF / 32768.;

  return getPtTemperature(R, R_0);
}

/**
 * Returns an array with the 4 SiPM temperatures.
 * If one or more SiPMs or FE boards are not connected, their temperature will be NaN
 */
std::array<float, 4> sipmTemperatures() {
  return {sipmTemperature(0), sipmTemperature(1), sipmTemperature(2), sipmTemperature(3)};
}

/**
 * Sets up the SiPM temperature sensor. It selects continuous conversion.
 */
void setupSipmTemperature(uint8_t id) {
  // register 0:
  // bit 7: VBIAS on *
  // bit 6: continuous conversion *
  // bit 5: one-shot conversion trigger
  // bit 4: 0: 2/4 wires, 1: 3 wires
  // bit 3-2: fault detection cycle control
  // bit 1: fault status clear
  // bit 0: 1: 50 Hz, 0: 60 Hz *
  writeMax31865(id, 0, 0xC1);
}


float boardTemperature() {
  return sipmTemperature(4);
}

void setupBoardTemperature() {
  setupSipmTemperature(4);
}


static constexpr std::array<spi::CS, 5> kSipmIdMap({spi::TempSipm0, spi::TempSipm1, spi::TempSipm2, spi::TempSipm3, spi::TempBoard0});

void writeMax31865(const uint8_t id, const uint8_t address, const uint8_t value){
  if (4 < id) {
    THROW_IA("id must be < 5");
  }
  if (7 < address) {
    THROW_IA("address must be < 8");
  }
  if (address == 0x1 || address == 0x2 || address == 0x7) {
    THROW_IA("address {}  is read-only", address);
  }

  spi::write(spi::SpiLen16, kSipmIdMap[id], (((address | 0x80) << 8) | value) << 16);

}

uint8_t readMax31865(const uint8_t id, const uint8_t address){
  if (4 < id) {
    THROW_IA("id must be < 5");
  }
  if (7 < address) {
    THROW_IA("address must be < 8");
  }

  spi::write(spi::SpiLen16, kSipmIdMap[id], (address << 8) << 16);
  return spi::read() & 0x000000FF;

}


float feTemperature(uint8_t id) {
  short tempRaw = readAdt7320(id, 0x02);

  // if it reads 0xFFFF (= -1 signed) it's because the sensor is not responding correctly (the temp value is 13 bit).
  // at least one of the last 3 bits should be 0, as they are mutually exclusive flags.
  if (tempRaw == -1) {
    return NAN;
  }

  // align right
  tempRaw >>= 3;

  return tempRaw / 16.;
}

/**
 * Returns an array with the 4 SiPM temperatures.
 * If one or more SiPMs or FE boards are not connected, their temperature will be NaN
 */
std::array<float, 4> feTemperatures() {
  return {feTemperature(0), feTemperature(1), feTemperature(2), feTemperature(3)};
}

void setupFeTemperature(uint8_t id) {
  resetAdt7320(id);
  // register 1 (configuration):
  // bit 7: resolution: 13-bit
  // bit 6-5: operation mode: 0b10 = 1 SPS *
  // bit 4: comparator/interrupt mode
  // bit 3: INT pin polarity
  // bit 2: CT pin polarity
  // bit 1-0: fault queue
  writeAdt7320(id, 0x01, 0b01000000);
}

static constexpr std::array<spi::CS, 4> kFeIdMap({spi::TempFe0, spi::TempFe1, spi::TempFe2, spi::TempFe3});

/**
 * Resets the serial interface of the ADT7320.
 * After this, it needs to be reconfigured.
 */
void resetAdt7320(const uint8_t id) {
  if (3 < id) {
    THROW_IA("id must be < 4");
  }
  spi::write(spi::SpiLen32, kFeIdMap[id], 0xFFFFFFFF);
}

void writeAdt7320(const uint8_t id, const uint8_t address, const uint8_t value) {
  if (3 < id) {
    THROW_IA("id must be < 4");
  }
  // let's allow the modification of configuration register only
  if (address != 1) {
    THROW_IA("address must be == 0x1");
  }

  spi::write(spi::SpiLen16, kFeIdMap[id], ((address << 3) << 24) | (value << 16));

}

uint16_t readAdt7320(const uint8_t id, const uint8_t address) {
  if (3 < id) {
    THROW_IA("id must be < 4");
  }
  if (3 < address) { 
    THROW_IA("address must be < 4");
  }

  spi::WordLength wl;
  uint32_t mask;
  if (address == 0x2) { //register 2 is the temperature value register, a 16-bit register
    wl = spi::SpiLen24;
    mask = 0x0000FFFF;
  }
  else { // registers 0 and 1 are 8-bit registers
    wl = spi::SpiLen16;
    mask = 0x000000FF;
  }

  spi::write(wl, kFeIdMap[id], ((address | 0x08) << 3) << 24);
  // INFO("spi read ", spi::read());
  return spi::read() & mask;

}