#pragma once

#include <cstdint>

static constexpr uint16_t LAST_TRIG_TS_LSB_ADDR = 0X00;
static constexpr uint16_t LAST_TRIG_TS_MSB_ADDR = 0X04;

static constexpr uint16_t DATA_CTRL_ADDR = 0X08;
static constexpr uint32_t DATA_CTRL_BOARD_ID = 0x000000FF;
static constexpr uint32_t DATA_CTRL_HEARTBEAT_EN = 0x00000100;
static constexpr uint32_t DATA_CTRL_RAW_DATA_EN = 0x00000200;
static constexpr uint32_t DATA_CTRL_RAW_DATA_TYPE = 0x00003C00;
static constexpr uint32_t DATA_CTRL_COUNTER_DATA_EN = 0x00004000;
static constexpr uint32_t DATA_CTRL_NOT_DATA_EN = 0x00008000;

static constexpr uint16_t RX_FIFO_0_3_STATUS_ADDR = 0x0C;
static constexpr uint32_t RX_FIFO_STATUS_STRIDE = 8; // can be used if a loop is needed
static constexpr uint32_t RX_FIFO_0_STATUS_USED_WORDS = 0x0000003F;
static constexpr uint32_t RX_FIFO_0_STATUS_EMPTY = 0x00000040;
static constexpr uint32_t RX_FIFO_0_STATUS_FULL = 0x00000080;
static constexpr uint32_t RX_FIFO_1_STATUS_USED_WORDS = 0x00003F00;
static constexpr uint32_t RX_FIFO_1_STATUS_EMPTY = 0x00004000;
static constexpr uint32_t RX_FIFO_1_STATUS_FULL = 0x00008000;
static constexpr uint32_t RX_FIFO_2_STATUS_USED_WORDS = 0x003F0000;
static constexpr uint32_t RX_FIFO_2_STATUS_EMPTY = 0x00400000;
static constexpr uint32_t RX_FIFO_2_STATUS_FULL = 0x00800000;
static constexpr uint32_t RX_FIFO_3_STATUS_USED_WORDS = 0x3F000000;
static constexpr uint32_t RX_FIFO_3_STATUS_EMPTY = 0x40000000;
static constexpr uint32_t RX_FIFO_3_STATUS_FULL = 0x80000000;

static constexpr uint16_t RX_FIFO_4_7_STATUS_ADDR = 0x10;
static constexpr uint32_t RX_FIFO_4_STATUS_USED_WORDS = 0x0000003F;
static constexpr uint32_t RX_FIFO_4_STATUS_EMPTY = 0x00000040;
static constexpr uint32_t RX_FIFO_4_STATUS_FULL = 0x00000080;
static constexpr uint32_t RX_FIFO_5_STATUS_USED_WORDS = 0x00003F00;
static constexpr uint32_t RX_FIFO_5_STATUS_EMPTY = 0x00004000;
static constexpr uint32_t RX_FIFO_5_STATUS_FULL = 0x00008000;
static constexpr uint32_t RX_FIFO_6_STATUS_USED_WORDS = 0x003F0000;
static constexpr uint32_t RX_FIFO_6_STATUS_EMPTY = 0x00400000;
static constexpr uint32_t RX_FIFO_6_STATUS_FULL = 0x00800000;
static constexpr uint32_t RX_FIFO_7_STATUS_USED_WORDS = 0x3F000000;
static constexpr uint32_t RX_FIFO_7_STATUS_EMPTY = 0x40000000;
static constexpr uint32_t RX_FIFO_7_STATUS_FULL = 0x80000000;

static constexpr uint16_t RX_WATCHDOG_ERROR_ADDR	 = 0x14;
static constexpr uint32_t RX_ERROR_0 = 0x00000001;
static constexpr uint32_t RX_ERROR_1 = 0x00000002;
static constexpr uint32_t RX_ERROR_2 = 0x00000004;
static constexpr uint32_t RX_ERROR_3 = 0x00000008;
static constexpr uint32_t RX_ERROR_4 = 0x00000010;
static constexpr uint32_t RX_ERROR_5 = 0x00000020;
static constexpr uint32_t RX_ERROR_6 = 0x00000040;
static constexpr uint32_t RX_ERROR_7 = 0x00000080;
static constexpr uint32_t RX_ERROR_ALL = 0x000000FF;
static constexpr uint32_t RX_WATCHDOG_0 = 0x00000100;
static constexpr uint32_t RX_WATCHDOG_1 = 0x00000200;
static constexpr uint32_t RX_WATCHDOG_2 = 0x00000400;
static constexpr uint32_t RX_WATCHDOG_3 = 0x00000800;
static constexpr uint32_t RX_WATCHDOG_4 = 0x00001000;
static constexpr uint32_t RX_WATCHDOG_5 = 0x00002000;
static constexpr uint32_t RX_WATCHDOG_6 = 0x00004000;
static constexpr uint32_t RX_WATCHDOG_7 = 0x00008000;
static constexpr uint32_t RX_WATCHDOG_ALL = 0x0000FF00;

static constexpr uint16_t TOFPET_0_WORD_COUNTER_ADDR	 = 0x18;
static constexpr uint16_t TOFPET_1_WORD_COUNTER_ADDR	 = 0x1C;
static constexpr uint16_t TOFPET_2_WORD_COUNTER_ADDR	 = 0x20;
static constexpr uint16_t TOFPET_3_WORD_COUNTER_ADDR	 = 0x24;
static constexpr uint16_t TOFPET_4_WORD_COUNTER_ADDR	 = 0x28;
static constexpr uint16_t TOFPET_5_WORD_COUNTER_ADDR	 = 0x2C;
static constexpr uint16_t TOFPET_6_WORD_COUNTER_ADDR	 = 0x30;
static constexpr uint16_t TOFPET_7_WORD_COUNTER_ADDR	 = 0x34;
static constexpr uint16_t TOFPET_WORD_COUNTER_ADDR_STRIDE	 = 0x4;

static constexpr uint16_t CSR1_ADDR = 0x38;
static constexpr uint32_t CSR1_RX_BIT_START_VALUE = 0x0000000F;
static constexpr uint32_t CSR1_FE_CHIP_ENABLE = 0x00000FF0;
static constexpr uint32_t CSR1_FE_CHIP_ENABLE_0 = 0x00000010;
static constexpr uint32_t CSR1_FE_CHIP_ENABLE_1 = 0x00000020;
static constexpr uint32_t CSR1_FE_CHIP_ENABLE_2 = 0x00000040;
static constexpr uint32_t CSR1_FE_CHIP_ENABLE_3 = 0x00000080;
static constexpr uint32_t CSR1_FE_CHIP_ENABLE_4 = 0x00000100;
static constexpr uint32_t CSR1_FE_CHIP_ENABLE_5 = 0x00000200;
static constexpr uint32_t CSR1_FE_CHIP_ENABLE_6 = 0x00000400;
static constexpr uint32_t CSR1_FE_CHIP_ENABLE_7 = 0x00000800;
// static constexpr uint32_t CSR1_XB_FIFO_USED_WORDS = 0x07FF0000;
// static constexpr uint32_t CSR1_XB_FIFO_OVERFLOW_FLAG = 0x08000000;
// static constexpr uint32_t CSR1_XB_FIFO_EMPTY = 0x40000000;
// static constexpr uint32_t CSR1_XB_FIFO_FULL = 0x80000000;

static constexpr uint16_t XB_FIFO_STATUS_ADDR = 0x3C;
static constexpr uint32_t XB_FIFO_STATUS_USED_WORDS = 0x00003FFF;
static constexpr uint32_t XB_FIFO_STATUS_EMPTY = 0x00004000;
static constexpr uint32_t XB_FIFO_STATUS_FULL = 0x00008000;
static constexpr uint32_t XB_FIFO_STATUS_USED_WORDS_MAX = 0x3FFF0000;
static constexpr uint32_t XB_FIFO_STATUS_FULL_LATCH = 0x80000000;

//to be used to identify the tofpets
static constexpr uint8_t FE_0 = 0x01;
static constexpr uint8_t FE_1 = 0x02;
static constexpr uint8_t FE_2 = 0x04;
static constexpr uint8_t FE_3 = 0x08;
static constexpr uint8_t FE_4 = 0x10;
static constexpr uint8_t FE_5 = 0x20;
static constexpr uint8_t FE_6 = 0x40;
static constexpr uint8_t FE_7 = 0x80;

static constexpr uint16_t TRIGGER_FE_RESET_ADDR	 = 0x40;
static constexpr uint32_t TRIGGER_SEQ_START = 0x00000001;
static constexpr uint32_t TRIGGER_SEQ_EN = 0x00000002;
static constexpr uint32_t TRIGGER_SMA_EN = 0x00000004;
static constexpr uint32_t TRIGGER_LEMO_EN = 0x00000008;
static constexpr uint32_t TRIGGER_TTC_EN = 0x00000010;
static constexpr uint32_t TRIGGER_INJ_EN = 0x00000020;
static constexpr uint32_t TRIGGER_INJ_TTC_CALIB_EN = 0x00000040;
static constexpr uint32_t FE_RESET = 0x00000080;
static constexpr uint32_t TRIGGER_SEQ_BUSY = 0x00000100;
static constexpr uint32_t FE_POWERUP_RESET = 0x00000200;
static constexpr uint32_t B_CHANNEL_DATA_EN = 0x00000400;
static constexpr uint32_t B_CHANNEL_FIFO_FULL_LATCH = 0x00000800;
static constexpr uint32_t EVCNT_RESET_CNT = 0xFF000000;

static constexpr uint32_t TRIGGER_SOURCE_MASK = 0x0000001E;
static constexpr uint8_t TRIG_SOURCE_SEQ = 0x01;
static constexpr uint8_t TRIG_SOURCE_SMA = 0x02;
static constexpr uint8_t TRIG_SOURCE_LEMO = 0x04;
static constexpr uint8_t TRIG_SOURCE_TTC = 0x08;

static constexpr uint16_t TRIG_SEQ_PERIOD_ADDR = 0x44;

static constexpr uint16_t TRIG_SEQ_COUNTER_ADDR = 0x48;

static constexpr uint16_t INJECTION_ADDR = 0x4C;
static constexpr uint32_t INJECTION_SYNC_RISING_EDGE = 0x00000001;
static constexpr uint32_t INJECTION_TOFPET_ENABLE = 0x00000002;
static constexpr uint32_t INJECTION_CLK_160_N40 = 0x00000004;
static constexpr uint32_t INJECTION_LASER_ENABLE = 0x00000008;
static constexpr uint32_t INJECTION_DURATION = 0x00000FF0;

static constexpr uint16_t CSR2_ADDR = 0x50;
static constexpr uint32_t CSR2_DISABLE_LEDS = 0x00000001;
static constexpr uint32_t CSR2_TTCRX_RESET = 0x00000002;
static constexpr uint32_t CSR2_QPLL_RESET = 0x00000004;
static constexpr uint32_t CSR2_DATA_LOOPBACK_EN = 0x00000008;
static constexpr uint32_t CSR2_PLL40_LOCKED = 0x00000010;
static constexpr uint32_t CSR2_QPLL_LOCKED = 0x00000020;
static constexpr uint32_t CSR2_TTCRX_READY = 0x00000040;
static constexpr uint32_t CSR2_QPLL_NLOCKED_LATCH = 0x00000080;
static constexpr uint32_t CSR2_TTCRX_NREADY_LATCH = 0x00000100;
static constexpr uint32_t CSR2_CLK_SELECT = 0x00000400;
static constexpr uint32_t CSR2_FPGA_FULL_RESET = 0x00000800;

static constexpr uint16_t TOFPET_SPI_CSR_ADDR = 0x54;
static constexpr uint32_t TOFPET_SPI_CSR_EN = 0x00000001;
static constexpr uint32_t TOFPET_SPI_CSR_CS = 0x000001FE;
static constexpr uint32_t TOFPET_SPI_CSR_READ_NWRITE = 0x00000200;
static constexpr uint32_t TOFPET_SPI_CSR_START = 0x00000400;
static constexpr uint32_t TOFPET_SPI_CSR_RESET = 0x00000800;
static constexpr uint32_t TOFPET_SPI_CSR_MODE_GLOBAL = 0x00001000;
static constexpr uint32_t TOFPET_SPI_CSR_BUSY = 0x00002000;
static constexpr uint32_t TOFPET_SPI_CSR_ERROR = 0x00004000;

static constexpr uint16_t TOFPET_SPI_D0_ADDR = 0x58;
static constexpr uint16_t TOFPET_SPI_D1_ADDR = 0x5C;
static constexpr uint16_t TOFPET_SPI_D2_ADDR = 0x60;
static constexpr uint16_t TOFPET_SPI_D3_ADDR = 0x64;
static constexpr uint16_t TOFPET_SPI_D4_ADDR = 0x68;
static constexpr uint16_t TOFPET_SPI_D5_ADDR = 0x6C;
static constexpr uint16_t TOFPET_SPI_D6_ADDR = 0x70;
static constexpr uint16_t TOFPET_SPI_D7_ADDR = 0x74;

static constexpr uint16_t SPI_CSR_ADDR = 0x78;
static constexpr uint32_t SPI_CSR_EN = 0x00000001;
static constexpr uint32_t SPI_CSR_CS = 0x00007FFE;
static constexpr uint32_t SPI_CSR_START = 0x00008000;
static constexpr uint32_t SPI_CSR_RESET = 0x00004000;
static constexpr uint32_t SPI_CSR_WORD_LENGTH = 0x007E0000;
static constexpr uint32_t SPI_CSR_BUSY = 0x00800000;

static constexpr uint16_t SPI_DATA_ADDR = 0x7C;

static constexpr uint16_t BUILD_DATE_ADDR = 0x80;

static constexpr uint16_t BUILD_TIME_ADDR = 0x84;

static constexpr uint16_t BUILD_NUMBER_ADDR = 0x88;

static constexpr uint16_t DEBUG_REGISTER_ADDR = 0x8C;
static constexpr uint32_t DEBUG_REGISTER_OUTPUT_SEL = 0x0000000F;
static constexpr uint8_t DEBUG_OUTPUT_NONE = 0x00;
static constexpr uint8_t DEBUG_OUTPUT_RCV = 0x01;
static constexpr uint8_t DEBUG_OUTPUT_TOFPET_SPI = 0x02;
static constexpr uint8_t DEBUG_OUTPUT_SPI = 0x03;
static constexpr uint8_t DEBUG_OUTPUT_EEPROM_SPI = 0x04;
static constexpr uint8_t DEBUG_OUTPUT_I2C = 0x05;
static constexpr uint8_t DEBUG_OUTPUT_TRIG = 0x06;
static constexpr uint32_t DEBUG_REGISTER_SMA0_SEL = 0x00000F00;
static constexpr uint32_t DEBUG_REGISTER_SMA1_SEL = 0x0000F000;
static constexpr uint8_t DEBUG_SMA_NONE = 0x00;
static constexpr uint8_t DEBUG_SMA_CLK40 = 0x01;
static constexpr uint8_t DEBUG_SMA_CLK40_DES2 = 0x02;
static constexpr uint8_t DEBUG_SMA_RX_CLK = 0x03;
static constexpr uint8_t DEBUG_SMA_RX_LVDS_DATA = 0x04;
static constexpr uint8_t DEBUG_SMA_TRIGGER = 0x05;
static constexpr uint8_t DEBUG_SMA_TEST_PULSE = 0x06;
static constexpr uint8_t DEBUG_SMA_CLK40_SHIFT = 0x07;
static constexpr uint8_t DEBUG_SMA_CLK160_SHIFT = 0x08;
static constexpr uint8_t DEBUG_SMA_CLK160_RX = 0x09;
static constexpr uint8_t DEBUG_SMA_CLK160_TOFPET = 0x0A;
static constexpr uint8_t DEBUG_SMA_TTC_FE_RESET = 0x0B;
static constexpr uint32_t DEBUG_LEDS = 0x000F0000;

static constexpr uint16_t I2C_CSR_ADDR = 0x90;
static constexpr uint32_t I2C_CSR_START = 0x00000002;
static constexpr uint32_t I2C_CSR_RESET = 0x00000004;
static constexpr uint32_t I2C_CSR_READ_NWRITE = 0x00000008;
static constexpr uint32_t I2C_CSR_BUSY = 0x00000010;
static constexpr uint32_t I2C_CSR_ERROR = 0x00000020;
static constexpr uint32_t I2C_CSR_DEVICE_ADDRESS = 0x00001FC0;
static constexpr uint32_t I2C_CSR_DATA_WR = 0x00FF0000;
static constexpr uint32_t I2C_CSR_DATA_RD = 0xFF000000;

static constexpr uint16_t XB_WORD_COUNTER_LSB_ADDR	 = 0x94;
static constexpr uint16_t XB_WORD_COUNTER_MSB_ADDR	 = 0x98;

static constexpr uint16_t TRIG_SEQ_REMAIN_CNT_ADDR = 0x9C;

static constexpr uint16_t TIMESTAMP_160_LSB_ADDR = 0xA0;
static constexpr uint16_t TIMESTAMP_160_MSB_ADDR = 0xA4;

static constexpr uint16_t TRIGGER_CNT_ADDR = 0xAC;

static constexpr uint16_t RESET_CNT_ADDR = 0xA8;
static constexpr uint16_t RESET_CNT_FE_REG = 0x000000FF;
static constexpr uint16_t RESET_CNT_FE_TTC = 0x0000FF00;

static constexpr uint16_t PLL_CSR_ADDR = 0xB0;
static constexpr uint32_t PLL_TP_CSR_RESET = 0x00000001;
static constexpr uint32_t PLL_TP_CSR_PHASE_EN = 0x00000002;
static constexpr uint32_t PLL_TP_CSR_UP_NDN = 0x00000004;
static constexpr uint32_t PLL_TP_CSR_CNT_SEL = 0x000000F8;
static constexpr uint32_t PLL_TP_CSR_LOCKED = 0x00000100;
static constexpr uint32_t PLL_TP_CSR_PHASE_DONE = 0x00000200;
static constexpr uint32_t PLL_40_CSR_PHASE_EN = 0x00020000;
static constexpr uint32_t PLL_40_CSR_UP_NDN = 0x00040000;
static constexpr uint32_t PLL_40_CSR_CNT_SEL = 0x00F80000;
static constexpr uint32_t PLL_40_CSR_LOCKED = 0x01000000;
static constexpr uint32_t PLL_40_CSR_PHASE_DONE = 0x02000000;

static constexpr uint16_t PLL_TP_PHASE_STATUS = 0xB4;
static constexpr uint32_t PLL_TP_PHASE_STATUS_0 = 0x000000FF;
static constexpr uint32_t PLL_TP_PHASE_STATUS_1 = 0x0000FF00;
static constexpr uint32_t PLL_TP_PHASE_STATUS_2 = 0x00FF0000;
static constexpr uint32_t PLL_TP_PHASE_STATUS_3 = 0xFF000000;

static constexpr uint16_t PLL_40_PHASE_STATUS = 0xB8;
static constexpr uint32_t PLL_40_PHASE_STATUS_0 = 0x000000FF;
static constexpr uint32_t PLL_40_PHASE_STATUS_1 = 0x0000FF00;
static constexpr uint32_t PLL_40_PHASE_STATUS_2 = 0x00FF0000;
static constexpr uint32_t PLL_40_PHASE_STATUS_3 = 0xFF000000;
