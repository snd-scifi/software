#pragma once

#include <string>
#include <vector>

#include "fpga/registers.hpp"
#include "utils/logger.hpp"

namespace spi {

// class spi_error : public std::runtime_error {
//   public:
//   spi_error(const std::string& what_arg) : std::runtime_error(what_arg) {}
//   spi_error(const char* what_arg) : std::runtime_error(what_arg) {}
//   spi_error(const spi_error& other) : std::runtime_error(other) {}
// };

enum WordLength {
  SpiLen8 = 0x07,
  SpiLen16 = 0x0F,
  SpiLen24 = 0x17,
  SpiLen32 = 0x1F,
  SpiLen40 = 0x27,
  SpiLen48 = 0x2F,
  SpiLen56 = 0x37,
  SpiLen64 = 0x3F,
};

enum CS {
  TempSipm0 = 0x0001,
  TempSipm1 = 0x0002,
  TempSipm2 = 0x0004,
  TempSipm3 = 0x0008,
  TempFe0 = 0x0010, // controlled through HPS GPIO 61
  TempFe1 = 0x0020, // controlled through HPS GPIO 62
  TempFe2 = 0x0040, // controlled through HPS GPIO 63
  TempFe3 = 0x0080, // controlled through HPS GPIO 64
  EepromFe0 = 0x0100,
  EepromFe1 = 0x0200,
  EepromFe2 = 0x0400,
  EepromFe3 = 0x0800,
  EepromBoard = 0x1000,
  TempBoard0 = 0x2000,
};

void initialize();

void write(WordLength wl, CS cs, uint32_t value, bool block = true);

uint32_t read();

} // namespace spi