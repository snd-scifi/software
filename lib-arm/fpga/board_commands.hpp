#pragma once

#include "json.hpp"
#include "storage/data_packet.hpp"

using json = nlohmann::json;
using JsonObject = json::object_t;
using JsonCommandCallback_t = std::function<JsonObject(JsonObject)>;
using RawCommandCallback_t = std::function<std::pair<json, std::vector<DataPacket>>(JsonObject)>;
using JsonCommandMap = std::unordered_map<std::string, JsonCommandCallback_t>;
using RawCommandMap = std::unordered_map<std::string, RawCommandCallback_t>;

namespace boardCommands {

std::string exceptionHandler(std::string functionName);

json reinitializeBoard(json args);
json feReset(json args);

json disableLEDs(json args);

json getBoardStartupStatus(json args);

json getFeConf(json args);
json setFeConf(json args);

json tiaBaselineCalibration(json args);
json discriminatorThrCalibration(json args);
json getDarkCounts(json args);
json getDarkCounts2(json args);

std::pair<json, std::vector<DataPacket>> tdcCalibration(json args);
std::pair<json, std::vector<DataPacket>> qdcCalibration(json args);

json setTrigger(json args);
json getTrigger(json args);
json startTriggerSequencer(json args);

json setInjection(json args);
json getInjection(json args);

json setFeDigitalDebugOutput(json args);
json setFeAnalogDebugOutput(json args);

json setBoardDebug(json args);

json setClk40Des1Phase(json args);
json getClk40Des1Phase(json args);

json setClk40Des2Phase(json args);
json getClk40Des2Phase(json args);

json setClk40UseQpll(json args);
json getClk40UseQpll(json args);

json setClkTofpetPhase(json args);
json getClkTofpetPhase(json args);
json scanClkTofpetPhase(json args);


json generateData(json args);

json setEepromId(json args);

json updateFirmware(json args);
json rebootBoard(json args);
json setBoardServerBootStart(json args);
json setDateTime(json args);

} // namespace boardCommands