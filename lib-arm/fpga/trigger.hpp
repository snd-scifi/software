#pragma once

#include <cstdint>
#include <vector>
#include <string>

#include "json.hpp"

namespace trigger {

uint64_t lastTrigTimestamp();

void trigSeqPeriod(uint32_t period);
uint32_t trigSeqPeriod();

void trigSeqCounter(uint32_t counter);
uint32_t trigSeqCounter();

uint32_t trigSeqCounterRemaining();

void trigSources(std::vector<std::string> sources);
std::vector<std::string> trigSources();

void trigSeqStart();

bool trigSeqBusy();

uint32_t triggerCounter();

void injectionEnable(bool enable);
bool injectionEnabled();

enum TestPulseClock {
  tpClk40,
  tpClk160,
};

void testPulseClock(TestPulseClock clk);
TestPulseClock testPulseClock();

void injectionTofpetEnable(bool enable);
bool injectionTofpetEnabled();

void injectionLaserEnable(bool enable);
bool injectionLaserEnabled();

// void injectionSyncRisingEdge(bool risingEdge);
// bool injectionSyncRisingEdge();

void injectionPulseDuration(uint8_t duration);
uint8_t injectionPulseDuration();

void injectionPhase(uint8_t phase);
uint8_t injectionPhase();

NLOHMANN_JSON_SERIALIZE_ENUM(TestPulseClock, {
    {tpClk40, "clk40"},
    {tpClk160, "clk160"},
})

} // namespace trigger