#include "fpga/registers.hpp"

#include "i2c.hpp"

/**
 * Writes data to the device at address on the I2C bus.
 */
void i2cWrite(uint8_t address, uint8_t data) {
  //reset i2c module
  writeRegister(I2C_CSR_ADDR, I2C_CSR_RESET, 1, false);
  writeRegister(I2C_CSR_ADDR, I2C_CSR_RESET, 0, false);

  //set write mode, address and data
  writeRegister(I2C_CSR_ADDR, I2C_CSR_READ_NWRITE, 0, false);
  writeRegister(I2C_CSR_ADDR, I2C_CSR_DEVICE_ADDRESS, address, false);
  writeRegister(I2C_CSR_ADDR, I2C_CSR_DATA_WR, data, false);

  //start transmission
  writeRegister(I2C_CSR_ADDR, I2C_CSR_START, 1, false);
  writeRegister(I2C_CSR_ADDR, I2C_CSR_START, 0, false);

  // wait to finish
  while (readRegister(I2C_CSR_ADDR, I2C_CSR_BUSY));

  if (readRegister(I2C_CSR_ADDR, I2C_CSR_ERROR)) {
    THROWX(i2c_error, "error writing to device {:#x}", address);
  }
  
}

/**
 * Reads data from device at address on the I2C bus.
 */
uint8_t i2cRead(uint8_t address) {
  //reset i2c module
  writeRegister(I2C_CSR_ADDR, I2C_CSR_RESET, 1, false);
  writeRegister(I2C_CSR_ADDR, I2C_CSR_RESET, 0, false);

  //set read mode and address
  writeRegister(I2C_CSR_ADDR, I2C_CSR_READ_NWRITE, 1, false);
  writeRegister(I2C_CSR_ADDR, I2C_CSR_DEVICE_ADDRESS, address, false);

  //start transmission
  writeRegister(I2C_CSR_ADDR, I2C_CSR_START, 1, false);
  writeRegister(I2C_CSR_ADDR, I2C_CSR_START, 0, false);

  // wait to finish
  while (readRegister(I2C_CSR_ADDR, I2C_CSR_BUSY));

  if (readRegister(I2C_CSR_ADDR, I2C_CSR_ERROR)) {
    THROWX(i2c_error, "error reading from device {:#x}", address);
  }

  return readRegister(I2C_CSR_ADDR, I2C_CSR_DATA_RD);
}

/**
 * Scans the I2C bus: performs a read at all addresses and return the ones where an ACK is received.
 */
std::vector<uint8_t> i2cScan() {
  std::vector<uint8_t> addresses;
  // addresses of the form 0b0000xxx and 1111xxx are reserved for special functions 
  for (uint8_t i = 0x8; i < 0x78; i++) {
    try {
      i2cRead(i);
    }
    catch (i2c_error& e) {
      continue;
    }
    
    addresses.push_back(i);
    DEBUG("{:#x} responded.", i);
  }

  return addresses;
}