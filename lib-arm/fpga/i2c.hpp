#pragma once

#include <string>
#include <vector>

#include "utils/logger.hpp"

class i2c_error : public std::runtime_error {
  public:
  i2c_error(const std::string& what_arg) : std::runtime_error(what_arg) {}
  i2c_error(const char* what_arg) : std::runtime_error(what_arg) {}
  i2c_error(const i2c_error& other) : std::runtime_error(other) {}
};

void i2cWrite(uint8_t address, uint8_t data);

uint8_t i2cRead(uint8_t address);

std::vector<uint8_t> i2cScan();