#pragma once

#include <cstdint>
#include <string>
#include <stdexcept>

namespace ttcrx {

class ttcrx_error : public std::runtime_error {
  public:
  ttcrx_error(const std::string& what_arg) : std::runtime_error(what_arg) {}
  ttcrx_error(const char* what_arg) : std::runtime_error(what_arg) {}
  ttcrx_error(const ttcrx_error& other) : std::runtime_error(other) {}
};

void resetQpll();
void resetTtcrx();

bool qpllLocked();
bool ttcrxReady();

bool notQpllLockedLatch();
bool notTtcrxReadyLatch();

void ttcrxWrite(uint8_t address, uint8_t data);
uint8_t ttcrxRead(uint8_t address);

void fineDelay(int id, uint8_t delay);
uint8_t fineDelay(int id);

void coarseDelay(int id, uint8_t delay);
uint8_t coarseDelay(int id);

void clock40Des2Enable(bool enable);
bool clock40Des2Enabled();

uint16_t bunchCounter();
uint32_t eventCounter();

uint8_t getRelativeFineDelay();

} // namespace ttcrx