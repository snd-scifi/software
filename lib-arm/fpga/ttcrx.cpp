#include "fpga/registers.hpp"
#include "fpga/i2c.hpp"
#include "utils/logger.hpp"

#include "ttcrx.hpp"

namespace ttcrx {

static constexpr uint8_t TTCRX_I2C_POINTER = 0x28;
static constexpr uint8_t TTCRX_I2C_DATA = 0x29;

/**
 * Pulses the QPLL reset,
 */
void resetQpll() {
  writeRegister(CSR2_ADDR, CSR2_QPLL_RESET, 1, true);
  writeRegister(CSR2_ADDR, CSR2_QPLL_RESET, 0, true);
}

/**
 * Pulses the TTCrx reset,
 */
void resetTtcrx() {
  writeRegister(CSR2_ADDR, CSR2_TTCRX_RESET, 1, true);
  writeRegister(CSR2_ADDR, CSR2_TTCRX_RESET, 0, true);
}

/**
 * Returns true if the QPLL is locked.
 */
bool qpllLocked() {
  return readRegister(CSR2_ADDR, CSR2_QPLL_LOCKED);
}

/**
 * Returns true if TTCrx is ready.
 */
bool ttcrxReady() {
  return readRegister(CSR2_ADDR, CSR2_TTCRX_READY);
}

/**
 * Returns true if the QPLL locked has ever been lost since the last reset.
 * It latches to true as soon as the QPLL locked is lost and stays so until the next FE reset.
 */
bool notQpllLockedLatch() {
  return readRegister(CSR2_ADDR, CSR2_QPLL_NLOCKED_LATCH);
}

/**
 * Returns true if the TTCrx READY has ever been lost since the last reset.
 * It latches to true as soon as the TTCrx READY is lost and stays so until the next FE reset.
 */
bool notTtcrxReadyLatch() {
  return readRegister(CSR2_ADDR, CSR2_TTCRX_NREADY_LATCH);
}

/**
 * Write data to a given TTCrx register.
 */
void ttcrxWrite(uint8_t address, uint8_t data) {
  i2cWrite(TTCRX_I2C_POINTER, address);
  i2cWrite(TTCRX_I2C_DATA, data);
}

/**
 * Read data from a TTCrx register.
 */
uint8_t ttcrxRead(uint8_t address) {
  i2cWrite(TTCRX_I2C_POINTER, address);
  return i2cRead(TTCRX_I2C_DATA);
}

/**
 * Sets the fine delay for Clock40Des1 and Clock40Des2 (selected by id, which must be 1 or 2).
 * delay must be between 0 and 239 (each step is 104.17 ps).
 */
void fineDelay(int id, uint8_t delay) {
  if (id != 1 && id != 2) {
    THROW_IA("id must be 1 or 2.");
  }
  if (239 < delay) {
    THROW_IA("delay must be < 240.");
  }

  uint8_t n = delay % 15;
  uint8_t m = (delay/15 - n + 14) % 16;

  ttcrxWrite(id - 1, (n << 4) + m);
}

/**
 * Returns the fine delay for Clock40Des1 and Clock40Des2 (selected by id, which must be 1 or 2).
 * The returned value is between 0 and 239 (each step is 104.17 ps).
 */
uint8_t fineDelay(int id) {
  if (id != 1 && id != 2) {
    THROW_IA("id must be 1 or 2.");
  }

  uint8_t nm = ttcrxRead(id - 1);
  uint8_t n = (nm & 0xF0) >> 4;
  uint8_t m = nm & 0x0F;

  return (m*15 + n*16 + 30) % 240;
}

/**
 * Sets the coarse delay (0 to 15, steps of 25 ns) for:
 * id = 1: L1Accept, BrcstStr1, BcntRes, EvCntRes, Brcst<5:2>
 * id = 2: BrcstStr2, Brcst<7:6>
 */
void coarseDelay(int id, uint8_t delay) {
  if (id != 1 && id != 2) {
    THROW_IA("id must be 1 or 2.");
  }
  if (0x0F < delay) {
    THROW_IA("delay must be < 16.");
  }
  
  uint8_t reg = ttcrxRead(2);

  switch (id) {
  case 1:
    reg &= 0xF0;
    reg |= delay;
    ttcrxWrite(2, reg);
    break;
  case 2:
    reg &= 0x0F;
    reg |= (delay << 4);
    ttcrxWrite(2, reg);
    break;
  default:
    break;
  }
}

/**
 * Returns the coarse delay (0 to 15, steps of 25 ns) for:
 * id = 1: L1Accept, BrcstStr1, BcntRes, EvCntRes, Brcst<5:2>
 * id = 2: BrcstStr2, Brcst<7:6>
 */
uint8_t coarseDelay(int id) {
  if (id != 1 && id != 2) {
    THROW_IA("id must be 1 or 2.");
  }
  
  uint8_t reg = ttcrxRead(2);
  
  switch (id) {
  case 1:
    return reg & 0x0F;
  case 2:
    return (reg & 0xF0) >> 4;
  default:
    return -1;
  }
}

/**
 * Enables or disables Clock40Des2.
 */
void clock40Des2Enable(bool enable) {
  auto reg = ttcrxRead(3);
  reg &= 0xF7;
  reg |= enable << 3;
  ttcrxWrite(3, reg);
}

/**
 * Returns true if Clock40Des2 is enabled, false otherwise.
 */
bool clock40Des2Enabled() {
  auto reg = ttcrxRead(3);
  return reg & 0x08;
}

/**
 * Returns the bunch counter value (12-bit).
 */
uint16_t bunchCounter() {
  return ttcrxRead(24) + (ttcrxRead(25) << 8);
}

/**
 * Returns the event counter value (24-bit).
 */
uint32_t eventCounter() {
  return ttcrxRead(26) + (ttcrxRead(27) << 8) + (ttcrxRead(28) << 16);
}

/**
 * Returns the relative phase between Clock40Des2 and Clock40Des1, 0 to 239.
 * Defined as (fine_del_2 - fine_del_1) % 240.
 */
uint8_t getRelativeFineDelay() {
  int curPhase1 = fineDelay(1);
  int curPhase2 = fineDelay(2);

  return (curPhase2 - curPhase1) % 240;
}



} //namespace ttcrx