#include <map>

#include "hps/gpio.hpp"

#include "spi.hpp"

namespace spi {

/**
 * Initialization of the SPI.
 * Sets up the CS pins that need to be controlled by the HPS GPIO and not by the FPGA (HW bug).
 * Resets and disables the SPI module.
 */
void initialize() {
  // set pins to output
  Gpio::getInstance().setDirection(61, true);
  Gpio::getInstance().setDirection(62, true);
  Gpio::getInstance().setDirection(63, true);
  Gpio::getInstance().setDirection(64, true);

  // write 1, as CS is active-low
  Gpio::getInstance().write(61, 1);
  Gpio::getInstance().write(62, 1);
  Gpio::getInstance().write(63, 1);
  Gpio::getInstance().write(64, 1);

  writeRegister(SPI_CSR_ADDR, SPI_CSR_RESET, 1, true);
  writeRegister(SPI_CSR_ADDR, SPI_CSR_RESET, 0, true);
  writeRegister(SPI_CSR_ADDR, SPI_CSR_EN, 0, true);

}

/**
 * Writes a word to an SPI device and blocks while SPI is transmitting, if requested.
 * \param wl word length of the SPI transmission, can be 16 or 32 bit.
 * \param cs selection of the device that will be written to
 * \param value value to be written to the SPI register. It needs to be left aligned.
 * \param block wait until SPI BUSY is false
 */
void write(WordLength wl, CS cs, uint32_t value, bool block) {
  // enable the right SPI module
  writeRegister(TOFPET_SPI_CSR_ADDR, TOFPET_SPI_CSR_EN, 0, true);
  writeRegister(SPI_CSR_ADDR, SPI_CSR_EN, 1, true);

  // reset SPI
  writeRegister(SPI_CSR_ADDR, SPI_CSR_RESET, 1, true);
  writeRegister(SPI_CSR_ADDR, SPI_CSR_RESET, 0, true);

  // set word length, CS and data
  writeRegister(SPI_CSR_ADDR, SPI_CSR_CS, cs, true);
  writeRegister(SPI_CSR_ADDR, SPI_CSR_WORD_LENGTH, wl, true);
  writeRegister(SPI_DATA_ADDR, value, false);

  // HW bug: the CS of the FE temp sensors are assigned to pins 61..64 of the HPS GPIO and cannot be controlled by the FPGA
  // remove for newer versions of the board, when this is fixed
  static const std::map<uint16_t, int> kGpioCsMap {
    {TempFe0, 61},
    {TempFe1, 62},
    {TempFe2, 63},
    {TempFe3, 64},
  };
  if (TempFe0 <= cs && cs <= TempFe3) {
    Gpio::getInstance().write(kGpioCsMap.at(cs), 0);
  }

  // start transmission
  writeRegister(SPI_CSR_ADDR, SPI_CSR_START, 1, false);
  writeRegister(SPI_CSR_ADDR, SPI_CSR_START, 0, false);

  // wait to be finished
  while (block && readRegister(SPI_CSR_ADDR, SPI_CSR_BUSY));

  // HW bug: the CS of the FE temp sensors are assigned to pins 61..64 of the HPS GPIO and cannot be controlled by the FPGA
  // remove for newer versions of the board, when this is fixed
  if (TempFe0 <= cs && cs <= TempFe3) {
    Gpio::getInstance().write(kGpioCsMap.at(cs), 1);
  }

  // disable TEMP SPI
  writeRegister(SPI_CSR_ADDR, SPI_CSR_EN, 0, true);
}

/**
 * Returns the data read by the SPI. It will be right aligned.
 */
uint32_t read() {
  return readRegister(SPI_DATA_ADDR);
}

} // namespace spi