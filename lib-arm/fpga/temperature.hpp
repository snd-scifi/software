#pragma once

#include <array>
#include <cstdint>

float sipmTemperature(uint8_t id);
std::array<float, 4> sipmTemperatures();

void setupSipmTemperature(uint8_t id);

float boardTemperature();
void setupBoardTemperature();

void writeMax31865(const uint8_t id, const uint8_t address, const uint8_t value);
uint8_t readMax31865(const uint8_t id, const uint8_t address);

float feTemperature(uint8_t id);
std::array<float, 4> feTemperatures();

void setupFeTemperature(uint8_t id);

void resetAdt7320(const uint8_t id);
void writeAdt7320(const uint8_t id, const uint8_t address, const uint8_t value);
uint16_t readAdt7320(const uint8_t id, const uint8_t address);