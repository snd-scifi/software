#pragma once

#include <array>
#include <cstdint>
#include <string>
#include <vector>

#include "fpga/register_addresses.hpp"

struct FifoStatus {
  bool empty;
  bool full;
  bool overflown;
  uint16_t usedWords;
  uint16_t usedWordsMax;
};

struct FwInfo {
  uint32_t buildNumber;
  std::string buildDate;
  std::string buildTime;
};


bool pll40Locked();

void resetFe();

void fpgaFullReset();

void initializeBoard();

void dataBoardId(uint8_t id);
uint8_t dataBoardId();

uint32_t boardId();
uint32_t boardRevision();

std::array<uint32_t, 4> feIds();
std::array<uint32_t, 4> feRevisions();

void rawDataType(uint8_t type);
uint8_t rawDataType();

void rawDataEnable(bool enable);
bool rawDataEnabled();

void counterDataEnable(bool enable);
bool counterDataEnabled();

void hitDataDisable(bool disable);
bool hitDataDisabled();

void heartbeatEnable(bool enable);
bool heartbeatEnabled();

void bChannelDataEnable(bool enable);
bool bChannelDataEnabled();

bool bChannelFifoFullLatch();

FifoStatus rxFifoStatus(uint8_t id);
FifoStatus xillybusFifoStatus();

uint32_t tofpetWordCounter(uint8_t id);
uint64_t xillybusWordCounter();

uint8_t watchdogStatus();
uint8_t rxErrorStatus();

void feChipEnable(uint8_t id, bool enable);
bool feChipEnabled(uint8_t id);

void feChipsEnable(uint8_t fe);
uint8_t feChipsEnabled();

void rxBitStartValue(uint8_t value);
uint8_t rxBitStartValue();

void dataLoopbackEnable(bool enable);
bool dataLoopbackEnabled();

void disableLeds(bool disable);
bool ledsDisabled();

enum Leds {
  LedG0,
  LedG1,
  LedR0,
  LedR1,
};

void setLed(Leds led, bool status);
bool getLed(Leds led);

FwInfo fwInfo();

void clock40UseQpll(bool use);
bool clock40UseQpll();

void clock40Des1Phase(uint8_t phase);
uint8_t clock40Des1Phase();

void clock40Des2Phase(uint8_t phase);
uint8_t clock40Des2Phase();

void tofpetClockPhase(uint8_t phase);
uint8_t tofpetClockPhase();
std::pair<std::vector<uint32_t>, std::vector<uint32_t>> scanTofpetClockPhase();

void initializeTtcrx(bool waitReady = true);

uint64_t currentTimestamp160();

uint8_t feResetRegCounter();
uint8_t feResetTtcCounter();

enum DebugHeaderOut {
  DbgHdrNone = DEBUG_OUTPUT_NONE,
  DbgHdrRcv = DEBUG_OUTPUT_RCV,
  DbgHdrTofpetSpi = DEBUG_OUTPUT_TOFPET_SPI,
  DbgHdrSpi = DEBUG_OUTPUT_SPI,
  DbgHdrEepromSpi = DEBUG_OUTPUT_EEPROM_SPI,
  DbgHdrI2c = DEBUG_OUTPUT_I2C,
  DbgHdrTrig = DEBUG_OUTPUT_TRIG,
};

enum DebugSma {
  DbgSma0 = DEBUG_REGISTER_SMA0_SEL,
  DbgSma1 = DEBUG_REGISTER_SMA1_SEL,
};

enum DebugSmaOut {
  DbgSmaNone = DEBUG_SMA_NONE,
  DbgSmaClk40 = DEBUG_SMA_CLK40,
  DbgSmaClk40Des2 = DEBUG_SMA_CLK40_DES2,
  DbgSmaRxClk = DEBUG_SMA_RX_CLK,
  DbgSmaRxLvdsData = DEBUG_SMA_RX_LVDS_DATA,
  DbgSmaTrigger = DEBUG_SMA_TRIGGER,
  DbgSmaTestPulse = DEBUG_SMA_TEST_PULSE,
  DbgSmaClk40Shift = DEBUG_SMA_CLK40_SHIFT,
  DbgSmaClk160Shift = DEBUG_SMA_CLK160_SHIFT,
  DbgSmaClk160Rx = DEBUG_SMA_CLK160_RX,
  DbgSmaClk160Tofpet = DEBUG_SMA_CLK160_TOFPET,
  DbgSmaTtcFeReset = DEBUG_SMA_TTC_FE_RESET,
};

void debugHeaderOutput(DebugHeaderOut output);
DebugHeaderOut debugHeaderOutput();

void debugSmaOutput(DebugSma sma, DebugSmaOut output);
DebugSmaOut debugSmaOutput(DebugSma sma);

std::ostream& operator<<(std::ostream& out, const DebugHeaderOut output);
std::ostream& operator<<(std::ostream& out, const DebugSmaOut output);

DebugHeaderOut debugHeaderOutFromString(std::string output);
DebugSmaOut debugSmaOutFromString(std::string output);

void updateFw(std::string filename="/opt/firmware-update/fpga.rbf");
