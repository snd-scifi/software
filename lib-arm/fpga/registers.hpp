#pragma once

#include <vector>
#include <stdexcept>

#include "fpga/register_addresses.hpp"

class register_error : public std::runtime_error {
  public:
  register_error(const std::string& what_arg) : std::runtime_error(what_arg) {}
  register_error(const char* what_arg) : std::runtime_error(what_arg) {}
  register_error(const register_error& other) : std::runtime_error(other) {}
};

uint32_t readRegister(uint16_t address);
uint32_t readRegister(uint16_t address, uint32_t mask);
std::vector<uint32_t> readRegisters(uint16_t address, size_t length);


void writeRegister(uint16_t address, uint32_t value, bool check);
void writeRegister(uint16_t address, uint32_t mask, uint32_t value, bool check);
void writeRegisters(uint16_t address, std::vector<uint32_t> value, bool check);
