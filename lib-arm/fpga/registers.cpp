#include "io/xillybus-impl.hpp"

#include "registers.hpp"

/**
 * Returns the left shift coresponding to a mask, i.e. the number of zeros on the right.
 * E.g. a mask of 0b00111000 would return 3.
 */
int maskToShift(uint32_t mask) {
    int c = 32; // c will be the number of zero bits on the right
    mask &= -mask;
    if (mask) c--;
    if (mask & 0x0000FFFF) c -= 16;
    if (mask & 0x00FF00FF) c -= 8;
    if (mask & 0x0F0F0F0F) c -= 4;
    if (mask & 0x33333333) c -= 2;
    if (mask & 0x55555555) c -= 1;

    return c;
}

/**
 * Reads the value of the register at the given address.
 */
uint32_t readRegister(uint16_t address) {
  return Registers::getInstance().read(address);
}

/**
 * Reads the bits selected by mask at a given address.
 * The bits will be right shifted, e.g. if the register contains 0xABCD and the mask is 0xFF00, the result will be 0xAB.
 */
uint32_t readRegister(uint16_t address, uint32_t mask) {
  auto data = readRegister(address);
  data = data & mask;
  return data >> maskToShift(mask);
}

/**
 * Reads a block of "length" registers from the given address.
 */
std::vector<uint32_t> readRegisters(uint16_t address, size_t length) {
  return Registers::getInstance().read(address, length);
}

/**
 * Writes a 32-bit value to the register at the given address.
 * \param check true if the written value needs to be checked, false to skip the check
 */
void writeRegister(uint16_t address, uint32_t value, bool check) {
  Registers::getInstance().write(address, value);
  if (!check) {
    return;
  }
  auto checkValue = readRegister(address);
   if (checkValue != value) {
    THROWX(register_error, "Error when writing register {:#x}. Written value: {#x}, read back value: {:#x}.", address, value, checkValue);
  }
}

/**
 * Writes the bits selected by the mask in the register at the given address.
 * The other bits of the registers are not modified.
 */
void writeRegister(uint16_t address, uint32_t mask, uint32_t value, bool check) {
  auto data = readRegister(address);
  data &= ~mask;
  data |= value << maskToShift(mask);
  writeRegister(address, data, false);
  if (!check) {
    return;
  }
  auto checkValue = readRegister(address, mask);
  if (checkValue != value) {
    THROWX(register_error, "Error when writing register {:#x}, mask {:#x}. Written value: {#x}, read back value: {:#x}.", address, mask, value, checkValue);
  }
}

/**
 * Writes a block of "length" registers from the given address.
 */
void writeRegisters(uint16_t address, std::vector<uint32_t> values, bool check) {
  Registers::getInstance().write(address, values);
  if (check && readRegisters(address, values.size()) != values) {
    THROWX(register_error, "Error when writing {} registers from address {:#x}", values.size(), address);
  }
}
