#pragma once

#include <cstdint>

#include "json.hpp"

#include "fpga/spi.hpp"


namespace eeprom {

enum EepromSelect{
  EepromFe0 = spi::EepromFe0,
  EepromFe1 = spi::EepromFe1,
  EepromFe2 = spi::EepromFe2,
  EepromFe3 = spi::EepromFe3,
  EepromBoard= spi::EepromBoard,
};

uint32_t read(EepromSelect es, uint16_t address);
void write(EepromSelect es, uint16_t address, uint32_t data);

uint8_t readSR(EepromSelect es);

bool eepromDetected(EepromSelect es);

// the installed eeprom in DAQ boards v3 and TOFPET v1 don't have the ID page
// uint8_t readId(EepromSelect es, uint16_t address);
// void writeId(EepromSelect es, uint16_t address, uint8_t data);

NLOHMANN_JSON_SERIALIZE_ENUM(EepromSelect, {
    {EepromFe0, "fe0"},
    {EepromFe1, "fe1"},
    {EepromFe2, "fe2"},
    {EepromFe3, "fe3"},
    {EepromBoard, "board"},
})

} // namespace eeprom
