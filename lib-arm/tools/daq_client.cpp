#include "network/data_client.hpp"
#include "utils/logger.hpp"
#include "utils/delay.hpp"
#include "utils/timer.hpp"
#include "io/xillybus-impl.hpp"
#include "fpga/fpga.hpp"
#include "tofpet/tofpet.hpp"

#include "daq_client.hpp"

void daqClient(const std::string serverAddress, const std::string serverPort, const std::vector<uint8_t> feIds, DaqManager& manager, const size_t transmitMin, const double maxWaitTime, const bool buildPackets, const bool enableBchannelData) {
  NOTICE("Starting daq-client.");
  rawDataEnable(false);
  heartbeatEnable(false);
  dataLoopbackEnable(false);
  bChannelDataEnable(enableBchannelData);
  feChipsEnable(0x00);

  DataStream::getInstance().close();
  DataClient client(serverAddress, serverPort, boardId(), buildPackets);

  try {
    NOTICE("connecting...");
    client.connect();
  }
  catch (const std::runtime_error& e) {
    ERROR("failed to connect: {}", e.what());
    manager.error(fmt::format("failed to connect: {}", e.what()));
    return;
  }

  try {
    NOTICE("beginning communication...");
    client.beginCommunication();
  }
  catch (const std::runtime_error& e) {
    ERROR("connection not accepted: {}", e.what());
    manager.error(fmt::format("connection not accepted: {}", e.what()));
    client.disconnect();
    return;
  }

  try {
    NOTICE("opening data stream...");
    DataStream::getInstance();
    feChipsEnable(getFeMask(feIds));
    manager.running(true);
    setLed(LedG1, true);
  }
  catch (const std::invalid_argument& e) {
    feChipsEnable(0x00);
    client.disconnect();
    ERROR("invalid argument: {}", e.what());
    manager.error(fmt::format("invalid argument: {}", e.what()));
    return;
  }

  uint64_t wordsRead = 0;
  bool breakFlag = false;
  Timer transmissionTimer;
  Timer loggingTimer;

  NOTICE("starting DAQ loop...");
  while(true) {
    // if a stop is requested, disable FE chips and wait that all data filled the xillybus
    if (manager.daqStopRequested()) {
      INFO("stop requested");
      feChipsEnable(0x00);
      delayMilliseconds(10);
      DEBUG("Transmitting queue.");
      manager.increaseSentWords(client.transmitQueue() * 4);
      DataStream::getInstance().close();
      resetFe();
      break;
      breakFlag = true;
      manager.running(false);
    }
    // determine how many words can be read
    auto wr = xillybusWordCounter() - wordsRead;
    wr -= wr%4;
    // if no words are to be read...
    if (wr == 0) {
      //if we need to stop and have nothing to transmit, stop
      if (breakFlag && client.queueSize() == 0) {
        break;
      }
      // otherwise, if we don't have to stop, wait and restart the loop
      else {
        delayMilliseconds(10);
      }
    }
    // if wr > 0...
    else {
      // read data and add to the client queue
      DEBUG("Reading {} words", wr);
      // Timer t;
      auto data = DataStream::getInstance().read(wr);
      client.addToQueue(data);
      wordsRead += wr;

      manager.increaseReadWords(wr);
      // NOTICE("read from xillybus ", wr, " words: ", t.elapsed());
    }

    // if we have enough data, transmit it
    if (transmitMin <= client.queueSize() || maxWaitTime < transmissionTimer.elapsed()) {
      DEBUG("Transmitting queue.");
      // Timer t;
      manager.increaseSentWords(client.transmitQueue() * 4);
      transmissionTimer.reset();
      // NOTICE("Transmit ", t.elapsed());
    }

    if (10 < loggingTimer.elapsed()) {
      INFO("Read words: {}", manager.readWords());
      INFO("Sent words: {}", manager.sentWords());
      loggingTimer.reset();
    }
    
    if (breakFlag) {
      break;
    }
  }
  bChannelDataEnable(false);
  setLed(LedG1, false);

  NOTICE("DAQ loop finished.");
  NOTICE("Read words: {}", manager.readWords());
  NOTICE("Sent words: {}", manager.sentWords());

  try {
    NOTICE("ending communication");
    client.endCommunication();
  }
  catch (const std::runtime_error& e) {
    WARN("error during end of communication: {}", e.what());
    manager.error(fmt::format("error during end of communication: {}", e.what()));
    client.disconnect();
    return;
  }

  NOTICE("disconnecting");
  client.disconnect();

}