#include <iostream>

#include "io/xillybus-impl.hpp"
#include "fpga/fpga.hpp"
#include "utils/delay.hpp"

#include "data_generator.hpp"


void dataGenerator(uint32_t n, uint32_t delayMs) {
  // while (1) {
  //   int n;
  //   std::cout << "Number of packet to send: ";
  //   std::cin >> n;
  //   if (n > 0) {
  //     std::vector<uint32_t> data(n*4);
  //     for (size_t i = 0; i < data.size(); i++) {
  //       data[i] = ((i%4) << 28) + i;
  //     }
  //     DataWriter::getInstance().write(data);
  //   }
  //   else {
  //     std::vector<uint32_t> data(4);
  //     for (size_t i = 0; i < data.size(); i++) {
  //       data[i] = ((i%4 + 8) << 28);
  //     }
  //     DataWriter::getInstance().write(data);
  //   }
  // }

  std::vector<uint32_t> hitP = {0x00000000, 0x10000000, 0x20000000, 0x30080000};

  for (uint32_t i = 0; i < n; i++) {
    DataWriter::getInstance().write(hitP);
    delayMilliseconds(delayMs);
  }
}