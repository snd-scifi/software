#pragma once

#include <string>
#include <vector>
#include "board/daq_manager.hpp"

/**
 * DAQ client callback, to be run in a separate thread.
 * It enabled the desired front-end chips, reads data from them, stores it locally and transmits it to the DAQ server.
 * It is stopped by setting @p stopDaq in @p manager.
 * 
 * @param serverAddress The address of the DAQ server
 * @param serverPort The port of the DAQ server
 * @param feIds The FE IDs to be enabled for data taking
 * @param manager A reference to the DAQ manager object, for communication between threads
 * @param transmitMin Minimum amount of packets to be transmitted
 * @param maxWaitTime Maximum time between two subsequent transmissions in seconds. This has priority over transmitMin, i.e. it will transmit after maxWaitTime even if the minimum number of packets is not reached
 * @param buildPackets True if packets should be built, false if data is to be transmitted "as is" from the FPGA. The former allows to spot issues in the data, the latter is more efficient.
 */
void daqClient(const std::string serverAddress, const std::string serverPort, const std::vector<uint8_t> feIds, DaqManager& manager, const size_t transmitMin, const double maxWaitTime, const bool buildPackets, const bool enableBchannelData);