#ifdef USE_ROOT

#include <algorithm>
#include <filesystem>

#include "utils/logger.hpp"

#include "event_reader_root.hpp"

namespace fs = std::filesystem;

EventReaderRoot::EventReaderRoot(std::string path, std::string dataTreeName) : EventReader(path), EventCommonRoot(dataTreeName) 
  , m_numberOfDataFiles{0}
  , m_totalEvents{0}
  , m_eventCounter{0} {
  for (const auto& entry : fs::directory_iterator(m_runDirectoryPath)) {
    auto filename = entry.path().filename().string();
    if (filename.substr(0, 5) == "data_" && filename.substr(9, 5) == ".root") {
      m_numberOfDataFiles = std::max(m_numberOfDataFiles, static_cast<unsigned int>(std::stoi(filename.substr(5, 4)) + 1));
    }
  }
  DEBUG("Found {} data_XXXX.root files.", m_numberOfDataFiles);
  determineNumberOfEvents();
  openNextFile();
}

uint64_t EventReaderRoot::numEvents() const {
  return m_totalEvents;
}

void EventReaderRoot::skip(uint64_t n) {
  while (m_eventsInFile <= m_eventIdx + n ) {
    n -= m_eventsInFile - m_eventIdx;
    m_eventCounter += m_eventsInFile - m_eventIdx;
    if (!openNextFile()) {
      WARN("Skipping more events than actually available.");
      m_eventIdx = m_eventsInFile;
      break;
    }
  }
  m_eventIdx += n;
}

bool EventReaderRoot::read(TofpetEvent& event) {
  if (m_eventsInFile <= m_eventIdx) {
    if (!openNextFile()) {
      return false;
    }
  }

  if(m_dataTree->GetEntry(m_eventIdx) <= 0) {
    THROW_RE("Could not read 'events' entry {}", m_eventIdx);
  }

  event.clear(m_evtTimestamp, m_evtNumber, m_flags);

  for (unsigned int i = 0; i < m_nHits; i++) {
    event.emplaceHit(
      m_boardId[i],
      m_tofpetId[i],
      m_tofpetChannel[i],
      m_tac[i],
      m_tCoarse[i],
      m_tFine[i],
      m_vCoarse[i],
      m_vFine[i],
      m_timestamp[i],
      m_timestampCalChi2[i],
      m_timestampCalDof[i],
      m_value[i],
      m_valueCalChi2[i],
      m_valueCalDof[i],
      m_valueSaturation[i]
    );

  }

  m_eventIdx++;
  m_eventCounter++;
  m_evtNumber++;
  return true;
}

bool EventReaderRoot::openNextFile() {
  if (m_numberOfDataFiles <= m_fileCounter) {
    return false;
  }
  
  m_filePath = m_runDirectoryPath / fmt::format("data_{:04d}.root", m_fileCounter);
  DEBUG("Opening {}", m_filePath);
  m_f = openRootRead(m_filePath);
  m_fileCounter++;

  m_f->GetObject(m_dataTreeName.c_str(), m_dataTree);
  if (!m_dataTree) {
    THROW_RE("TTree '{}' is not present in file {}", m_dataTreeName, m_filePath);
  }

  m_eventsInFile = m_dataTree->GetEntriesFast();
  m_eventIdx = 0;

  m_dataTree->SetBranchAddress("evt_timestamp", &m_evtTimestamp);

  if (m_dataTree->GetBranch("evt_number") == nullptr) {
    WARN("Branch 'evt_number' not present. Calculating event number.");
    m_evtNumber = 0;
  }
  else {
    m_dataTree->SetBranchAddress("evt_number", &m_evtNumber);
  }

  if (m_dataTree->GetBranch("evt_flags") == nullptr) {
    WARN("Branch 'flags' not present. Setting flags to 0.");
    m_flags = 0;
  }
  else {
    m_dataTree->SetBranchAddress("evt_flags", &m_flags);
  }


  m_dataTree->SetBranchAddress("n_hits", &m_nHits);
  m_dataTree->SetBranchAddress("board_id", m_boardId);
  m_dataTree->SetBranchAddress("tofpet_id", m_tofpetId);
  m_dataTree->SetBranchAddress("tofpet_channel", m_tofpetChannel);
  m_dataTree->SetBranchAddress("tac", m_tac);
  m_dataTree->SetBranchAddress("t_coarse", m_tCoarse);
  m_dataTree->SetBranchAddress("t_fine", m_tFine);
  m_dataTree->SetBranchAddress("v_coarse", m_vCoarse);
  m_dataTree->SetBranchAddress("v_fine", m_vFine);

  if (!m_dataTree->GetBranch("timestamp")) {
    INFO("Calibrated branches not available.");
    for (size_t i = 0; i < kMaxHits; i++) {
      m_timestamp[i] = std::numeric_limits<float>::quiet_NaN();
      m_value[i] = std::numeric_limits<float>::quiet_NaN();
      m_timestampCalChi2[i] = std::numeric_limits<float>::quiet_NaN();
      m_timestampCalDof[i] = std::numeric_limits<float>::quiet_NaN();
      m_valueCalChi2[i] = std::numeric_limits<float>::quiet_NaN();
      m_valueCalDof[i] = std::numeric_limits<float>::quiet_NaN();
      m_valueSaturation[i] = std::numeric_limits<float>::quiet_NaN();
    }
    return true;
  }
  m_dataTree->SetBranchAddress("timestamp", m_timestamp);
  m_dataTree->SetBranchAddress("value", m_value);
  m_dataTree->SetBranchAddress("timestamp_cal_chi2", m_timestampCalChi2);
  m_dataTree->SetBranchAddress("timestamp_cal_dof", m_timestampCalDof);
  m_dataTree->SetBranchAddress("value_cal_chi2", m_valueCalChi2);
  m_dataTree->SetBranchAddress("value_cal_dof", m_valueCalDof);
  m_dataTree->SetBranchAddress("value_saturation", m_valueSaturation);

  return true;
}


void EventReaderRoot::determineNumberOfEvents() {
  for (unsigned int i{0}; i < m_numberOfDataFiles; i++) {
    auto filePath = m_runDirectoryPath / fmt::format("data_{:04d}.root", i);
    auto f = openRootRead(filePath);
    TTree* tree;
    f->GetObject(m_dataTreeName.c_str(), tree);
    if (!tree) {
      ERROR("Could not read TTree '{}' from file {}. Reading data until file {}", m_dataTreeName, filePath, i-1);
      m_numberOfDataFiles = i;
      break;
    }
    m_totalEvents += tree->GetEntriesFast();
  }
}


#endif // USE_ROOT
