#pragma once

#include <fstream>
#include <string>
#include <vector>
#include <filesystem>

#include "io/packet_writer.hpp"

#include "json.hpp"

class PacketWriterBin : public PacketWriter {
public:
  PacketWriterBin(std::string path, nlohmann::json writerSettings); 
  virtual ~PacketWriterBin() = default;
  std::string name() const { return "PacketWriterBin"; }

  /** Add the packet to the underlying device.
   *
   * The reference to the packet is only valid for the duration of the call.
   * Errors must be handled by throwing an appropriate exception.
   */
  void append(const DataPacket& packet) override;

private:
  void openNextFile();

  std::size_t m_filePacketLimit;
  unsigned int m_filePacketCounter;
  unsigned int m_fileCounter;
  
  std::ofstream m_f;
};