#pragma once

#include <atomic>
#include <thread>

#include "daq/daq_server_manager.hpp"
#include "monitoring/event_writer_monitor.hpp"
#include "processing/event_processor.hpp"
#include "storage/evt_queue.hpp"
#include "io/event_writer.hpp"

#include "json.hpp"

class EventWriterWrapper {
public:
  EventWriterWrapper(EventQueue& eventQueue, DaqServerManager& serverManager);
  ~EventWriterWrapper() = default;
  void finalize();

private:
  void writerLoop();
  EventQueue& m_events;
  DaqServerManager& m_serverManager;
  std::unique_ptr<EventWriter> m_writer;
  std::atomic_bool m_stopRequested;

  EventWriterMonitorPtr m_monitor;

  std::thread m_writerThread;
};