#ifdef USE_ROOT

#include <algorithm>
#include <filesystem>

#include "utils/logger.hpp"

#include "event_writer_root_multi_tree.hpp"

namespace fs = std::filesystem;

EventWriterRootMultiTree::EventWriterRootMultiTree(std::string path, std::vector<uint32_t> boardIds, nlohmann::json writerSettings) 
  : EventWriter(path, writerSettings)
  , EventCommonRootMultiTree()
  , m_boardIds{boardIds}
  , m_fileEventCounter {0}
  , m_autoSaveEventCounter {0}
{
  for (const auto bid : m_boardIds) {
    m_hitsMap.try_emplace(bid);
  }
  m_fileEventLimit = m_writerSettings.value("events_per_file", 1000000UL);
  m_autoFlush = m_writerSettings.value("auto_flush", -30000000L);
  m_autoSave = m_writerSettings.value("auto_save", -300000000L);
  m_autoSaveNevents = m_writerSettings.value("auto_save_n_events", 1000UL);
  m_autoSaveDelayMin = m_writerSettings.value("auto_save_delay_min", 5.0);
  m_autoSaveDelayMax = m_writerSettings.value("auto_save_delay_max", 5.0);
  if (m_autoSaveDelayMax < m_autoSaveDelayMin) {
    WARN("Setting `auto_save_delay_min` ({}) must be <= `auto_save_delay_max` ({}). auto_save_delay_max takes priority.", m_autoSaveDelayMin, m_autoSaveDelayMax);
  }
  openNextFile();
}

void EventWriterRootMultiTree::append(const TofpetEvent& event) {
  m_evtTimestamp = event.timestamp();
  m_evtNumber = event.evtNumber();
  m_flags = event.flags();
  // m_evtDesync = evt.desync();
  // m_evtTtcrxReadyLost = evt.ttcrxReadyLost();
  // m_evtQpllLockedLost = evt.qpllLockedLost();
  m_eventTree->Fill();

  // copy the hits in a map with board ID as keys
  for (const auto& hit : event) {
    m_hitsMap.at(hit.boardId()).push_back(hit);
  }

  // write the per-board event information
  for (auto& [boardId, hits] : m_hitsMap) {
    m_nHits = hits.size();
    if (kMaxHits < m_nHits) {
      WARN("Dropping {}. One event can contain at most {} hits.", m_nHits - kMaxHits, kMaxHits);
      m_nHits = kMaxHits;
    }
    
    for (size_t i = 0; i < m_nHits; i++) {
      const auto& hit = m_hitsMap.at(boardId).at(i);
      m_tofpetId[i] = hit.tofpetId();
      m_tofpetChannel[i] = hit.tofpetChannel();
      m_tac[i] = hit.tac();
      m_tCoarse[i] = hit.tCoarse();
      m_tFine[i] = hit.tFine();
      m_timestamp[i] = hit.timestamp();
      m_vCoarse[i] = hit.vCoarse();
      m_vFine[i] = hit.vFine();
      m_value[i] = hit.value();

      m_timestampCalChi2[i] = hit.timestampCalChi2();
      m_timestampCalDof[i] = hit.timestampCalDof();
      m_valueCalChi2[i] = hit.valueCalChi2();
      m_valueCalDof[i] = hit.valueCalDof();
      m_valueSaturation[i] = hit.valueSaturation();
      // m_boardDesync = brdEvt.desync();
      // m_boardTtcrxReadyLost = brdEvt.ttcrxReadyLost();
      // m_boardQpllLockedLost = brdEvt.qpllLockedLost();
    }

    m_dataTrees.at(boardId)->Fill();
    hits.clear();
  }

  m_fileEventCounter++;
  m_autoSaveEventCounter++;

  if (m_fileEventLimit <= m_fileEventCounter) {
    openNextFile();
    return;
  }

  const auto elapsed = m_autoSaveTimer.elapsed();
  // call the autosave if the minimum time has passed AND the maximum number or events is reached OR if the maximum time has passed
  // we also write the first event, to have the trees immediately accessible
  if ((m_autoSaveDelayMin < elapsed && m_autoSaveNevents < m_autoSaveEventCounter) || m_autoSaveDelayMax < elapsed || m_fileEventCounter == 1) {
    m_eventTree->AutoSave("SaveSelf");
    for (auto& [id, dataTree] : m_dataTrees) {
      dataTree->AutoSave("SaveSelf");
    }
    m_autoSaveEventCounter = 0;
    m_autoSaveTimer.reset();
  }
}

void EventWriterRootMultiTree::openNextFile() {
  m_filePath = m_runDirectoryPath / fmt::format("data_{:04d}.root", m_fileCounter);
  DEBUG("Opening {}", m_filePath);
  m_f = openRootWrite(m_filePath,
    m_writerSettings.value("compression_algorithm", ROOT::kZSTD),
    m_writerSettings.value("compression_level", 0)
  );
  m_fileCounter++;
  m_fileEventCounter = 0;
  m_autoSaveEventCounter = 0;

  m_eventTree = new TTree("event", "Event information");
  m_eventTree->SetDirectory(m_f.get());

  m_eventTree->Branch("timestamp", &m_evtTimestamp, "evtTimestamp/L");
  m_eventTree->Branch("evt_number", &m_evtNumber, "evtNumber/l");
  m_eventTree->Branch("flags", &m_flags, "flags/l");
  // m_eventTree->Branch("desync", &m_evtDesync, "evtDesync/O");
  // m_eventTree->Branch("ttcrx_ready_lost", &m_evtTtcrxReadyLost, "evtTtcrxReadyLost/O");
  // m_eventTree->Branch("qpll_locked_lost", &m_evtQpllLockedLost, "evtQpllLockedLost/O");

  m_eventTree->SetAutoFlush(m_autoFlush);
  m_eventTree->SetAutoSave(m_autoSave);

  for (const auto& boardId : m_boardIds) {
    auto treeName = "board_" + std::to_string(boardId);
    m_dataTrees[boardId] = new TTree(treeName.c_str(), treeName.c_str());
    m_dataTrees.at(boardId)->SetDirectory(m_f.get());

    m_dataTrees.at(boardId)->Branch("n_hits", &m_nHits, "nHits/i");
    m_dataTrees.at(boardId)->Branch("tofpet_id", m_tofpetId, "tofpetId[nHits]/b");
    m_dataTrees.at(boardId)->Branch("tofpet_channel", m_tofpetChannel, "tofpetChannel[nHits]/b");
    m_dataTrees.at(boardId)->Branch("tac", m_tac, "tac[nHits]/b");
    m_dataTrees.at(boardId)->Branch("t_coarse", m_tCoarse, "tCoarse[nHits]/L");
    m_dataTrees.at(boardId)->Branch("t_fine", m_tFine, "tFine[nHits]/s");
    m_dataTrees.at(boardId)->Branch("timestamp", m_timestamp, "timestamp[nHits]/F");
    m_dataTrees.at(boardId)->Branch("v_coarse", m_vCoarse, "vCoarse[nHits]/s");
    m_dataTrees.at(boardId)->Branch("v_fine", m_vFine, "vFine[nHits]/s");
    m_dataTrees.at(boardId)->Branch("value", m_value, "value[nHits]/F");

    m_dataTrees.at(boardId)->Branch("timestamp_cal_chi2", m_timestampCalChi2, "timestampCalChi2[nHits]/F");
    m_dataTrees.at(boardId)->Branch("timestamp_cal_dof", m_timestampCalDof, "timestampCalDof[nHits]/F");
    m_dataTrees.at(boardId)->Branch("value_cal_chi2", m_valueCalChi2, "valueCalChi2[nHits]/F");
    m_dataTrees.at(boardId)->Branch("value_cal_dof", m_valueCalDof, "valueCalDof[nHits]/F");
    m_dataTrees.at(boardId)->Branch("value_saturation", m_valueSaturation, "valueSaturation[nHits]/F");


    m_dataTrees.at(boardId)->SetAutoFlush(m_autoFlush);
    m_dataTrees.at(boardId)->SetAutoSave(m_autoSave);

    // m_dataTrees.at(boardId)->Branch("desync", &m_boardDesync, "boardDesync/O");
    // m_dataTrees.at(boardId)->Branch("ttcrx_ready_lost", &m_boardTtcrxReadyLost, "boardTtcrxReadyLost/O");
    // m_dataTrees.at(boardId)->Branch("qpll_locked_lost", &m_boardQpllLockedLost, "boardQpllLockedLost/O");
  }

  m_autoSaveTimer.reset();

}

void EventWriterRootMultiTree::finalize() {
  // this prevents ROOT from hanging when writing the last file. If this happens in the destructor, somehow it doesn't work...
  m_f = uninitializedRootFilePtr();
}

#endif // USE_ROOT
