#pragma once

#include <filesystem>
#include <fstream>
#include <string>

#include "packet_reader.hpp"

class PacketReaderBin : public PacketReader {
public:
  PacketReaderBin(std::string path, std::size_t bufferSize = 100000);
  virtual ~PacketReaderBin();
  virtual std::string name() const { return "PacketReaderBin"; }

  /** Return the (minimum) number of available packets.
   *
   * \returns UINT64_MAX if the number of packets is unknown.
   *
   * Calling `read` the given number of times must succeed. Additional
   * calls could still succeed.
   */
  virtual uint64_t numPackets() const { return 0; }

  /** Skip the next n packets.
   *
   * If the call would seek beyond the range of available packets it should
   * not throw and error. Instead, the next `readNext` call should fail.
   */
  virtual void skip(uint64_t n) {}
  
  /** Read the next packet from the underlying device into the given object.
   * 
   * The Reader implementation is responsible for ensuring consistent packets and
   * clearing previous contents. Errors must be handled by throwing an
   * appropriate exception.
   * 
   * \param[out] packet Output packet.
   * \returns true if an packet was read, false if no packet was read because no more packets are available
   *
   */
  virtual bool read(DataPacket& packet);

  private:
    bool openNextFile();

    unsigned int m_filePacketCounter;
    unsigned int m_fileCounter;
    std::ifstream m_f;

};