#pragma once

#ifdef USE_ROOT

#include <string>
#include <vector>

#include "io/event_reader.hpp"
#include "io/event_common_root.hpp"

class EventReaderRoot : public EventReader, public EventCommonRoot {
public:
  EventReaderRoot(std::string path, std::string dataTreeName = "data");
  ~EventReaderRoot() = default;
  std::string name() const { return "EventReaderRoot"; }

  uint64_t numEvents() const;
  void skip(uint64_t n);
  bool read(TofpetEvent& event);
  // std::vector<uint32_t> boardIds() const { return m_boardIds; };

private:
  bool openNextFile();
  void determineNumberOfEvents();
  unsigned int m_numberOfDataFiles;
  size_t m_totalEvents;
  size_t m_eventCounter;
  size_t m_eventsInFile;
  size_t m_eventIdx;
  // std::vector<uint32_t> m_boardIds;

};

#endif // USE_ROOT
