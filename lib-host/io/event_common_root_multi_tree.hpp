#pragma once

#ifdef USE_ROOT

#include <cstdint>
#include <cstddef>
#include <filesystem>
#include <map>
#include <string>

#include "utils/root.hpp"

#include "TTree.h"

class EventCommonRootMultiTree {
protected:
  EventCommonRootMultiTree();
  virtual ~EventCommonRootMultiTree() = default;

  unsigned int m_fileEventCounter;
  unsigned int m_fileCounter;

  RootFilePtr m_f;
  TTree* m_eventTree;
  std::map<uint32_t, TTree*> m_dataTrees;

  static constexpr size_t kMaxHits = 1 << 16;

  // int64_t m_evtTimestamp;
  long long  m_evtTimestamp;
  unsigned long long  m_evtNumber;
  unsigned long long  m_flags;

  uint32_t m_nHits;
  uint8_t m_tofpetId[kMaxHits];
  uint8_t m_tofpetChannel[kMaxHits];
  uint8_t m_tac[kMaxHits];
  // int64_t m_tCoarse[kMaxHits];
  long long m_tCoarse[kMaxHits];
  uint16_t m_tFine[kMaxHits];
  float m_timestamp[kMaxHits];
  uint16_t m_vCoarse[kMaxHits];
  uint16_t m_vFine[kMaxHits];
  float m_value[kMaxHits];
  float m_timestampCalChi2[kMaxHits];
  float m_timestampCalDof[kMaxHits];
  float m_valueCalChi2[kMaxHits];
  float m_valueCalDof[kMaxHits];
  float m_valueSaturation[kMaxHits];

};

#endif // USE_ROOT