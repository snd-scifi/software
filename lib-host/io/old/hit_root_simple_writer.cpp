
#ifdef USE_ROOT

#ifdef TOREMOVE
#include "daq/daq_server_manager.hpp"
#include "utils/logger.hpp"

#include "hit_root_simple_writer.hpp"


HitRootSimpleWriter::HitRootSimpleWriter(const std::string& path, DaqServerManager& serverManager) 
  : Writer(serverManager)
  , m_f(uninitializedRootFilePtr())
  , m_dataTree(nullptr)
  , m_cntValue(0xFFFFFFFF)
  , m_trigTs(0)
  , m_trigType(0xFF)
  , m_trigCnt(0xFFFFFFFFFFFFFFFF)
  , m_daqMonitor{std::make_shared<DaqMonitor>(std::vector<uint32_t>(m_serverManager.getBoardIds()))}
{
  m_serverManager.daqMonitor(m_daqMonitor);

  m_f = openRootWrite(path);

  m_f->cd();

  m_dataTree = new TTree("data", "Data");
  m_dataTree->SetDirectory(m_f.get());

  // m_dataTree->SetAutoSave(n); negative for n of bytes, positive for entries

  m_dataTree->Branch("board_id", &m_boardId, "boardId/i");
  m_dataTree->Branch("packet_type", &m_packetType, "packetType/b");
  m_dataTree->Branch("tofpet_id", &m_tofpetId, "tofpetId/b");
  m_dataTree->Branch("tofpet_channel", &m_tofpetChannel, "tofpetChannel/b");
  m_dataTree->Branch("tac", &m_tac, "tac/b");
  m_dataTree->Branch("t_coarse", &m_tCoarse, "tCoarse/L");
  m_dataTree->Branch("t_fine", &m_tFine, "tFine/s");
  m_dataTree->Branch("v_coarse", &m_vCoarse, "vCoarse/s");
  m_dataTree->Branch("v_fine", &m_vFine, "vFine/s");
  m_dataTree->Branch("trigger_timestamp", &m_trigTs, "trigTs/l");
  m_dataTree->Branch("trigger_type", &m_trigType, "trigType/b");
  m_dataTree->Branch("trigger_count", &m_trigCnt, "trigCnt/l");
  m_dataTree->Branch("counter_value", &m_cntValue, "cntValue/i");

}

void HitRootSimpleWriter::append(const std::deque<DataPacket>& packets) {
  for (const auto& packet : packets) {
    switch (packet.type()) {
    case DataPacket::Hit:
      {
        auto p = static_cast<const HitPacket&>(packet);
        m_boardId = packet.boardId();
        m_packetType = packet.type();
        m_tofpetId = p.tofpetId();
        m_tofpetChannel = p.tofpetChannel();
        m_tac = p.tac();
        m_tCoarse = p.timestampCoarse();
        m_tFine = p.timestampFine();
        m_vCoarse = p.valueCoarse();
        m_vFine = p.valueFine();

        m_cntValue = 0xFFFFFFFF;

        m_dataTree->Fill();
      }
      break;
    case DataPacket::Trigger:
      {
        auto p = static_cast<const TriggerPacket&>(packet);
        m_boardId = packet.boardId();
        m_packetType = packet.type();
        m_trigTs = p.timestamp();
        m_trigType = p.triggerType();
        m_trigCnt = p.counter();

        m_tofpetId = 0xFF;
        m_tofpetChannel = 0xFF;
        m_tac = 0xFF;
        m_tCoarse = -1;
        m_tFine = 0xFFFF;
        m_vCoarse = 0xFFFF;
        m_vFine = 0xFFFF;
        m_cntValue = 0xFFFFFFFF;

        m_dataTree->Fill();
      }
      break;
    case DataPacket::Counter:
      {
        auto p = static_cast<const CounterPacket&>(packet);
        m_boardId = packet.boardId();
        m_packetType = packet.type();

        m_tofpetId = p.tofpetId();
        m_tofpetChannel = p.channel();
        m_tac = 0xFF;
        m_tCoarse = p.timestamp();
        m_tFine = 0xFFFF;
        m_vCoarse = 0xFFFF;
        m_vFine = 0xFFFF;
        m_cntValue = p.value();

        m_dataTree->Fill();

      }
      break;
    default:
      break;
    }
  }

}

#endif // USE_ROOT
#endif