#pragma once

#ifdef TOREMOVE
#include <fstream>

#include "io/writer.hpp"
#include "monitoring/daq_monitor.hpp"

class HitCsvWriter : public Writer {
public:  
  HitCsvWriter(const std::string& path, DaqServerManager& serverManager);
  ~HitCsvWriter() { m_os.close(); };
  std::string name() const { return "HitCsvWriter"; }

  void append(const std::deque<DataPacket>& packets);
  void finalize() {}

private:
  std::ofstream m_os;

  uint64_t m_trigTs;
  uint8_t m_trigType;
  uint64_t m_trigCnt;

  DaqMonitorPtr m_daqMonitor;

};
#endif