#include "utils/logger.hpp"
#include "utils/print_u8.hpp"
#include "daq/daq_server_manager.hpp"

#ifdef TOREMOVE
#include "hit_csv_writer.hpp"

HitCsvWriter::HitCsvWriter(const std::string& path, DaqServerManager& serverManager) 
  : Writer(serverManager)
  , m_trigTs(0)
  , m_trigType(0xFF)
  , m_trigCnt(0xFFFFFFFFFFFFFFFF)
  , m_daqMonitor{std::make_shared<DaqMonitor>(std::vector<uint32_t>(m_serverManager.getBoardIds()))}
{
  m_serverManager.daqMonitor(m_daqMonitor);
  m_os.open(path);

  if (!m_os) {
    ERROR("Could not open {} for writing.", path);
    THROW_RE("Could not open {} for writing.", path);
  }

  fmt::print(m_os, "board,type,tofpet_id,channel,tac,timestamp_coarse,timestamp_fine,value_coarse,value_fine,trigger_timestamp,trigger_type,trigger_counter\n");
}

void HitCsvWriter::append(const std::deque<DataPacket>& packets) {
  using namespace print_u8;
  for (const auto& packet : packets) {
    switch (packet.type()) {
    case DataPacket::Hit:
      {
        auto p = static_cast<const HitPacket&>(packet);
        fmt::print(m_os, "{},hit,{},{},{},{},{},{},{},{},{},{}\n", p.boardId(), p.tofpetId(), p.tofpetChannel(), p.tac(), p.timestampCoarse(), p.timestampFine(), p.valueCoarse(), p.valueFine(), m_trigTs, m_trigType, m_trigCnt);
      }
      break;
    case DataPacket::Trigger:
      {
        auto p = static_cast<const TriggerPacket&>(packet);
        m_trigTs = p.timestamp();
        m_trigType = p.triggerType();
        m_trigCnt = p.counter();
        fmt::print(m_os, "{},trigger,,,,,,,,{},{},{}\n", p.boardId(), p.timestamp(), p.triggerType(), p.counter());
      }
      break;
    case DataPacket::Counter:
      {
        auto p = static_cast<const CounterPacket&>(packet);
        fmt::print(m_os, "{},counter,{},{},,{},,{},,{},{},{}\n", p.boardId(), p.tofpetId(), p.channel(), p.timestamp(), p.value(), m_trigTs, m_trigType, m_trigCnt);
      }
      break;
    default:
      break;
    }
  }

}

#endif