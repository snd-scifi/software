#pragma once
#ifdef TOREMOVE
#include <fstream>
#include "io/writer.hpp"
#include "monitoring/daq_monitor.hpp"

class RawPacketWriter : public Writer {
public:  
  RawPacketWriter(const std::string& path, DaqServerManager& serverManager);
  ~RawPacketWriter() { m_os.close(); };
  std::string name() const { return "RawPacketWriter"; }

  void append(const std::deque<DataPacket>& packets);
  void finalize() {}

private:
  std::ofstream m_os;
  
  DaqMonitorPtr m_daqMonitor;

};
#endif