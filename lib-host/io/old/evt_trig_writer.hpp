#pragma once
#ifdef USE_ROOTT

#include <map>
#include <deque>
#include <vector>

#include "storage/tofpet_event.hpp"
#include "io/writer.hpp"
#include "io/event_writer_root_multi_tree.hpp"
#include "utils/root.hpp"
#include "monitoring/daq_mon_evt_trig_writer.hpp"
#include "processing/event_processor.hpp"

#include "TTree.h"

/**
 * Trigger based event builder.
 * Each trigger received by each board represents a new event, and all the hits coming after it are added to this event.
 * If a hit comes before the first trigger, it is ignored.
 * 
 * All the boards are expected to run synchronously and have the same trigger timestamp for each trigger.
 * If this is not the case, a new empty BoardEvent is added to syncronize (e.g. if a board misses a trigger).
 * 
 * The data is written to a ROOT file containing a TTree with the event information (the `event` TTree) and one TTree per board with the corresponding hits.
 */
class EvtTrigWriter : public Writer {
public:  
  EvtTrigWriter(const std::string& path, DaqServerManager& serverManager);
  ~EvtTrigWriter() {}
  std::string name() const { return "EvtTrigWriter"; }

  void append(const std::deque<DataPacketId>& packets);
  void finalize();

private:
  void processEvent();
  void writeEvent();

  EventWriterRootMultiTree m_rootWriter;
  
  std::map<uint32_t, std::deque<TofpetBoardEvent>> m_boardEvents;
  TofpetEvent m_eventTmp;

  std::vector<std::unique_ptr<EventProcessor>> m_eventProcessors;


  DaqMonitorEvtTrigWriterPtr m_daqMonitor;
};

#endif // USE_ROOT

