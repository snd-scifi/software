#pragma once
#ifdef TOREMOVE
#ifdef USE_ROOT

#include "io/writer.hpp"
#include "utils/root.hpp"
#include "monitoring/daq_monitor.hpp"

#include "TTree.h"

class HitRootSimpleWriter : public Writer {
public:  
  HitRootSimpleWriter(const std::string& path, DaqServerManager& serverManager);
  ~HitRootSimpleWriter() {};
  std::string name() const { return "HitRootSimpleWriter"; }

  void append(const std::deque<DataPacket>& packets);
  void finalize() {}

private:
  RootFilePtr m_f;
  TTree* m_dataTree;

  uint8_t m_tofpetChannel, m_tofpetId, m_tac, m_packetType;
  uint16_t m_tFine, m_vCoarse, m_vFine;
  uint32_t m_boardId, m_cntValue;
  int64_t m_tCoarse;

  uint64_t m_trigTs;
  uint8_t m_trigType;
  uint64_t m_trigCnt;
  
  DaqMonitorPtr m_daqMonitor;

};

#endif // USE_ROOT
#endif