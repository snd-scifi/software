#include <iomanip>

#ifdef TOREMOVE
#include "utils/logger.hpp"
#include "daq/daq_server_manager.hpp"
#include "raw_packet_writer.hpp"

RawPacketWriter::RawPacketWriter(const std::string& path, DaqServerManager& serverManager) 
  : Writer(serverManager)
  , m_daqMonitor{std::make_shared<DaqMonitor>(std::vector<uint32_t>(m_serverManager.getBoardIds()))}
{
  m_os.open(path);

  if (!m_os) {
    ERROR("Could not open {} for writing.", path);
    THROW_RE("Could not open {} for writing.", path);
  }

  m_serverManager.daqMonitor(m_daqMonitor);

}

void RawPacketWriter::append(const std::deque<DataPacket>& packets) {
  for (const auto& packet : packets) {
    fmt::print(m_os, "{:03d} {:08x}\n", packet.boardId(), fmt::join(packet.data(), " "));
  }
}

#endif