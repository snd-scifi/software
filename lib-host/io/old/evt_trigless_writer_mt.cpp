#ifdef USE_ROOTT

#include <algorithm>
#include <ctime>
#include <filesystem>
#include <fstream>

#include "daq/daq_server_manager.hpp"
#include "processing/boards_noise_filter.hpp"
#include "processing/data_monitoring_stream.hpp"
#include "processing/snd_add_flags.hpp"
#include "processing/snd_fast_noise_filter.hpp"
#include "processing/snd_advanced_noise_filter.hpp"
#include "processing/tofpet_calibrator.hpp"
#include "utils/logger.hpp"
#include "utils/timer.hpp"

#include "evt_trigless_writer_mt.hpp"

namespace fs = std::filesystem;

/**
 * Triggerless event writer constructor.
 * path is the path to the folder where data is stored.
 * Data files will be named data_0000.root, data_0001.root, etc
 */
EvtTriglessWriterMt::EvtTriglessWriterMt(const std::string& path, DaqServerManager& serverManager)
  : Writer(serverManager)
  , m_boardIds(serverManager.getBoardIds())
  , m_rootWriter(path, m_boardIds, serverManager.writerSettings())
  , m_lastGoodTimestamp(0)
  , m_lastSyncTimestamp(0)
  , m_daqMonitor{std::make_shared<DaqMonitorEvtTriglessWriterMt>(m_boardIds)}
{
  for (const auto& boardId : m_boardIds) {
    m_boardEvents.try_emplace(boardId);
    m_hitMap.try_emplace(boardId);
    m_trigMap.emplace(boardId, TriggerQueue());
    m_lastGoodBoardEvtTimestamp.emplace(boardId, 0);
  }

  const size_t nBoardEvtsThreads = m_serverManager.writerSettings().value("n_boardevt_builder_threads", 2U);
  std::vector<std::vector<uint32_t>> idsPerThread(nBoardEvtsThreads);
  size_t i = 0;
  for (const auto id : m_boardIds) {
    idsPerThread.at(i).push_back(id);
    i++;
    i %= nBoardEvtsThreads;
  }
  for (const auto ids : idsPerThread) {
    m_buildBoardEvtsThreads.push_back(std::thread([this, ids] { buildBoardEvents(ids); }));
  }

  for (const auto& processor : m_serverManager.writerSettings().value("processors", std::vector<std::string>())) {
    if (processor == "board_noise_filter") {
      m_eventProcessors.push_back(std::make_unique<SimpleNoiseFilter>(m_serverManager.writerSettings()["processors_settings"]["board_noise_filter"]));
    }
    else if (processor == "snd_fast_noise_filter") {
      m_eventProcessors.push_back(std::make_unique<SndFastNoiseFilter>(m_serverManager.boardMapping(), m_serverManager.writerSettings()["processors_settings"]["snd_fast_noise_filter"]));
    }
    else if (processor == "snd_advanced_noise_filter") {
      m_eventProcessors.push_back(std::make_unique<SndAdvancedNoiseFilter>(m_serverManager.boardMapping(), m_serverManager.writerSettings()["processors_settings"]["snd_advanced_noise_filter"]));
    }
    else if (processor == "tofpet_calibrator") {
      m_eventProcessors.push_back(std::make_unique<TofpetCalibrator>(path));
    }
    else if (processor == "snd_add_flags") {
      m_eventProcessors.push_back(std::make_unique<SndAddFlags>(m_serverManager));
    }
    else if (processor == "data_monitoring_stream") {
      m_monitoringPath = (m_serverManager.dataPath() / fmt::format("monitoring_run_{:06d}", m_serverManager.runNumber())).string();
      m_eventProcessors.push_back(std::make_unique<DataMonitoringStream>(m_monitoringPath, m_serverManager.getBoardIds(), m_serverManager.writerSettings()["processors_settings"]["data_monitoring_stream"]));
    }
    else {
      WARN("Unknown processor '{}'", processor);
    }
  }
  
  m_buildEvtsThread = std::thread([this] { buildEvents(); });
  m_processEvtsThread = std::thread([this] { processEvents(); });
  m_writeEvtsThread = std::thread([this] { writeEvents(); });

  m_serverManager.daqMonitor(m_daqMonitor);

}

/**
 * Append packets to the writer and performs the necessary processing.
 */
void EvtTriglessWriterMt::append(const std::deque<DataPacket>& packets) {
  for (const auto& [boardId, packet] : packets) {
    switch (packet.type()) {
    case DataPacket::Hit:
      {
        auto p = static_cast<const HitPacket&>(packet);
        m_hitMap[boardId].insertSorted(TofpetHit(std::move(p)));
      }
      break;
    case DataPacket::Trigger:
      {
        auto p = static_cast<const TriggerPacket&>(packet);
        m_trigMap[boardId].push_back(p);
      }
      break;
    default:
      break;
    }
  }

  for (const auto boardId : m_boardIds) {
    m_daqMonitor->globalHitQueueLength(boardId, m_hitMap[boardId].size());
    m_daqMonitor->triggerQueueLength(boardId, m_trigMap[boardId].size());
  }

  checkTriggersConsistency();
}

void EvtTriglessWriterMt::finalize() {
  INFO("finalize called. requesting BoardEvents builder threads to stop...");
  m_stopRequested = true;

  INFO("waiting for BoardEvents builder threads to stop...");
  for (auto& thr : m_buildBoardEvtsThreads) {
    thr.join();
  }
  // m_buildBoardEvtsThreadsFinished = true;

  INFO("waiting for Events builder thread to stop...");
  m_buildEvtsThread.join();
  m_buildEvtsThreadFinished = true;

  INFO("waiting for Events processor thread to stop...");
  m_processEvtsThread.join();
  m_processEvtsThreadFinished = true;

  INFO("waiting for Events writer thread to stop...");
  m_writeEvtsThread.join();

  INFO("finalize returning.");
}

/**
 * Verifies triggers are consistent, i.e. triggers with the same counter value have the same timestamp.
 * This function assumes that sometimes a trigger can be missing, but the counter will always be consistent.
 * It also assumes that all boards are correctly transmitting.
 * It assignes the value to m_lastGoodTimestamp, based on the penultimate trigger detected.
 */
void EvtTriglessWriterMt::checkTriggersConsistency() {
  // find the shortest trigger queue
  size_t nMin = std::numeric_limits<size_t>::max();
  for (auto& [id, trigq] : m_trigMap) {
    nMin = std::min(nMin, trigq.size());
  }
   
  // if the shortes trigger queue is empty or with just one trigger, do nothing
  if (nMin < 2) {
    return;
  }
  // we want to leave the last trigger in each queue to be used as a check for event building
  nMin--;

  auto lastGoodTs = m_lastGoodTimestamp.load();
  for (size_t i = 0; i < nMin; i++) {
    // check if the trigger counter is the same for all first packets
    uint32_t cntMin = std::numeric_limits<uint32_t>::max();
    uint32_t cntMax = 0;
    for (const auto& [id, trigq] : m_trigMap) {
      cntMin = std::min(cntMin, trigq.front().counter());
      cntMax = std::max(cntMax, trigq.front().counter());
    }

    // if the counters are not the same, remove the ones smaller than the max and continue
    // we allow some triggers to be lost, at the moment
    if (cntMin != cntMax) {
      WARN("Some trigger was lost: {} != {}", cntMin, cntMax);
      // removes all trigger packets with counter < cntMax (at most one per queue)
      std::for_each(m_trigMap.begin(), m_trigMap.end(), [this, cntMax] (auto& a) {
        if (a.second.front().counter() < cntMax) {
          a.second.pop_front();
        }
        else {
          m_daqMonitor->increaseMissedTriggers(a.first);
        }
      });
      m_daqMonitor->increaseProcessedTriggers();
      continue;
    }

    // determines if the first trigger packet of each queue has the same timestamp
    auto consistent = std::adjacent_find(m_trigMap.begin(), m_trigMap.end(), [] (const auto& a, const auto& b) {
      return a.second.front().timestamp() != b.second.front().timestamp(); 
    }) == m_trigMap.end();

    // store the previous last good timestamp
    auto lastGoodTsTmp = m_lastSyncTimestamp.load();

    // determines the "new" last good timestamp
    lastGoodTs = (*std::min_element(m_trigMap.begin(), m_trigMap.end(),
      [] (const auto& a, const auto& b) {
        return a.second.front().timestamp() < b.second.front().timestamp();
      })).second.front().timestamp();
    
    // if an inconsistency is detected, 
    if (consistent) {
      // we know we are consistent up to the "new" last good timestamp, as this group of triggers passed the check
      m_lastSyncTimestamp = lastGoodTs;
    }
    else {
      WARN("inconsistency found!"); // TODO write function that determines the offending boards
      TrigMap map;
      for (const auto [id, q] : m_trigMap) {
        map[id] = std::make_pair(q.front().timestamp(), q.front().counter());
        // WARN(id, ": ", q.front().timestamp(), " (", q.front().counter(), ")");
      }
      m_daqMonitor->syncStatus(false, findDesyncedBoardIds(map), map);
    }

    // m_lastGoodTimestamp is set AFTER m_lastSyncTimestamp, when everything is good, as we want the latter to be always bigger, for safety reasons
    m_lastGoodTimestamp = lastGoodTsTmp;
    
    // remove all processed trigger packets
    std::for_each(m_trigMap.begin(), m_trigMap.end(), [] (auto& a) { a.second.pop_front(); });
    m_daqMonitor->increaseProcessedTriggers();
  }

  for (const auto boardId : m_boardIds) {
    m_daqMonitor->triggerQueueLength(boardId, m_trigMap[boardId].size());
  }

}

std::vector<uint32_t> EvtTriglessWriterMt::findDesyncedBoardIds(const TrigMap map) const {
  std::vector<uint32_t> ret;
  std::map<int64_t, size_t> elementCount;

  // count the occurrences of each trigger timestamp
  for (const auto& [bid, trigInfo] : map) {
    auto trigTs = trigInfo.second;
    try {
      elementCount.at(trigTs)++;
    }
    catch (const std::out_of_range&) {
      elementCount[trigTs] = 1;
    }
  }

  // find the most frequesnt timestamp. If this appears only once, all boards are not synchronized.
  auto mostFrequentTs = (*std::max_element(elementCount.begin(), elementCount.end(), [] (const auto& a, const auto& b) { return a.second < b.second; })).first;
  if (elementCount.at(mostFrequentTs) == 1) {
    return m_boardIds;
  }

  // find all boards that have a different timestamp
  for (const auto& [bid, trigInfo] : map) {
    auto trigTs = trigInfo.second;
    if (trigTs != mostFrequentTs) {
      ret.push_back(bid);
    }
  }

  return ret;
}

/**
 * Performs event building, board by board, for the IDs stored in boardIds.
 */
void EvtTriglessWriterMt::buildBoardEvents(const std::vector<uint32_t> boardIds) {
  // define local buffer to store hits, one per board that is processed here
  std::map<uint32_t, std::deque<TofpetHit>> hitMap;
  for (const auto id : boardIds) {
    hitMap[id] = std::deque<TofpetHit>();
  }

  bool breakFlag{false};

  // loop where hits are taken from the buffers, stored locally and board events are built.
  // it will stop after finalize is called and all the hit buffers are emptied.
  while(!breakFlag) {
    // check if DAQ is stopping (m_stopRequested is set in finalize())
    breakFlag = m_stopRequested.load();
    // gets the last good timestamp that can be processed (set in checkTriggerConsistency(), only hits with timestamp < lgt will be treated)
    auto lgt = m_lastGoodTimestamp.load();
    // if the DAQ is stopping, no more hits will arrive, therefore all hits can be treated.
    // we do this by changing lgt to the maximum possible value, so a hit timestamp is always < lgt
    if (breakFlag) {
      lgt = std::numeric_limits<int64_t>::max();
    }

    // read hits and perform event building for one board at a time.
    for (const auto id : boardIds) {
      // local redefinitions, to make the code more readable
      auto& hitQueue = hitMap[id];
      auto& evtQueue = m_boardEvents[id];

      // take all hits for the current board ID from the hit buffer and insert them in the local buffer. Blocks 100 ms if no hits are present.
      auto tmpQueue = m_hitMap[id].takeAll(100);
      if (!tmpQueue.empty()) {
        hitQueue.insert(hitQueue.end(), std::make_move_iterator(tmpQueue.begin()), std::make_move_iterator(tmpQueue.end()));
      }
      tmpQueue.clear();

      // we need to guarantee that the hits are sorted
      std::sort(hitQueue.begin(), hitQueue.end(), [] (const auto& a, const auto& b) { return a.tCoarse() < b.tCoarse(); });

      m_daqMonitor->globalHitQueueLength(id, m_hitMap[id].size());
      m_daqMonitor->threadHitQueueLength(id, hitQueue.size());

      // we build events until the queue is empty or the timestamp of the first hit is larger than the last good timestamp, determined at the beginning of the loop.
      int64_t firstHitTs;
      while ( !hitQueue.empty() && ((firstHitTs = hitQueue.front().tCoarse()) < lgt) ) {
        // the first avalable hit is always added to the board event and its timestamp determines the event timestamp
        TofpetBoardEvent tmpEvt(firstHitTs, firstHitTs < m_lastSyncTimestamp);
        tmpEvt.emplaceHitRelative(hitQueue.front());
        hitQueue.pop_front();
        // then others hits are added if they are within 4 clock cycles from the first hit
        while (!hitQueue.empty() && hitQueue.front().tCoarse() < tmpEvt.timestamp() + 4) { // TODO should this be in terms of the 40 MHz clock, to have events syncronous with LHC? Should it be 5 clock cycles?
          tmpEvt.emplaceHitRelative(hitQueue.front());
          hitQueue.pop_front();
        }
        // once we are done building the board event, we can add it to the board events queue
        evtQueue.emplace_back(std::move(tmpEvt));
        m_daqMonitor->globalBoardEventQueueLength(id, evtQueue.size());
        m_daqMonitor->threadHitQueueLength(id, hitQueue.size());
      }
      // when we exit from this loop, we are sure that all possible board events happened before lgt on this board have been build, therefore they can be used to be built into full events
      m_lastGoodBoardEvtTimestamp[id] = lgt;
    }
  }

}

/**
 * Performs event building.
 */
void EvtTriglessWriterMt::buildEvents() {
  // define local buffers to store the board events generated in buildBoardEvents(), one per board.
  std::map<uint32_t, std::deque<TofpetBoardEvent>> boardEvtMap;
  for (const auto id : m_boardIds) {
    boardEvtMap[id] = std::deque<TofpetBoardEvent>();
  }

  // loop where board events are taken from the buffers, stored locally and events are built.
  // it will stop after finalize is called, all buildBoardEvents are done and all the board event buffers are emptied.
  while(true) {
    // gets the last good board event timestamp that can be processed. It will automatically be 2^63-1 when the board event builders are all finished.
    auto minLgbet = (*std::min_element(m_lastGoodBoardEvtTimestamp.begin(), m_lastGoodBoardEvtTimestamp.end(), [] (const auto& a, const auto& b) { return a.second < b.second; })).second.load();
    
    // take all board events for all boards
    for (const auto id : m_boardIds) {
      auto& boardEvtQueue = boardEvtMap[id];
      
      // move the board events to the local buffers
      auto tmpQueue = m_boardEvents[id].takeAll(100);
      if (tmpQueue.empty()) {
        continue;
      }
      boardEvtQueue.insert(boardEvtQueue.end(), std::make_move_iterator(tmpQueue.begin()), std::make_move_iterator(tmpQueue.end()));
      tmpQueue.clear();

      m_daqMonitor->globalBoardEventQueueLength(id, m_boardEvents[id].size());
      m_daqMonitor->threadBoardEventQueueLength(id, boardEvtQueue.size());
    }

    std::deque<TofpetEvent> localEventQueue;
    while (true) {
      // determine the minimum timestamp, skipping empty queues
      int64_t minTs = std::numeric_limits<int64_t>::max();
      for (const auto& [bid, evtq] : boardEvtMap) {
        if (evtq.empty()) {
          continue;
        }
        else {
          minTs = std::min(minTs, evtq.front().timestamp());
        }
      }

      // if the minimum timestamp is after the last good timestamp, we cannot build events
      // when breakFlag == true, minLgbet will necessarily be 2^63-1, so it will also break when all board event queues are empty
      if (minLgbet <= minTs) {
        break;
      }

      // create an event with the timestamp equal to the minimum timestamp of the board events
      TofpetEvent tmpEvt(minTs);

      // add to it all the board events that are within 4 clock cycles from the initial one
      for (auto& [bid, evtq] : boardEvtMap) {
        if (!evtq.empty() && evtq.front().timestamp() < minTs + 4) {
          tmpEvt.emplaceBoardEvent(bid, std::move(evtq.front()));
          evtq.pop_front();
          m_daqMonitor->threadBoardEventQueueLength(bid, evtq.size());
          // TODO if there is no BoardEvent, the sync, ttcrxReady and qpllLocked are always false! get this from the trigger?
          // in addition, the desync doesn't propagate to the trigger
        }
      }

      // add the event to the event buffer
      m_daqMonitor->increaseTotalEvents();
      m_daqMonitor->increaseTotalHits(tmpEvt.nHits());
      localEventQueue.emplace_back(std::move(tmpEvt));
    }
    m_events.putBlock(localEventQueue);
    m_daqMonitor->globalEventQueueLength(m_events.size());
    // this condition is true only when all board events builder are finished AND when this event builder is finished
    if (minLgbet == std::numeric_limits<int64_t>::max()) {
      break;
    }
  }
}

/**
 * Processes the events with the list of processors defined in the constructor.
 * Events for which all the processors return true, are queued for being written.
 */
void EvtTriglessWriterMt::processEvents() {
  bool breakFlag{false};
  uint64_t eventCounter{0};
  while (!breakFlag) {
    // check if the event builder thread has finished
    breakFlag = m_buildEvtsThreadFinished.load();
    // get all the events from the event queue
    auto evtQueue = m_events.takeAll(1000);

    m_daqMonitor->globalEventQueueLength(m_events.size());
    m_daqMonitor->threadEventQueueLength(evtQueue.size());
    // loop over them and put the good one in the processed queue
    while (!evtQueue.empty()) {
      // get the event and pop it from the front of the queue
      auto evt = std::move(evtQueue.front());
      evtQueue.pop_front();
      m_daqMonitor->threadEventQueueLength(evtQueue.size());

      // pass it though the processors. If they all return true, save it.
      bool saveEvent{true};
      for (const auto& processor : m_eventProcessors) {
        saveEvent = processor->process(evt);
        if (!saveEvent) {
          break;
        }
      }
      
      // put the event in the processed queue for writing
      if (saveEvent) {
        evt.evtNumber(eventCounter);
        eventCounter++;
        m_daqMonitor->increaseProcessedEvents();
        m_daqMonitor->increaseProcessedHits(evt.nHits());
        m_eventsProcessed.emplace_back(std::move(evt));
        m_daqMonitor->globalProcessedEventQueueLength(m_eventsProcessed.size());
      }
      else {
        m_daqMonitor->increaseDiscardedEvents(1);
        m_daqMonitor->increaseDiscardedHits(evt.nHits());
      }
    }
  }
}

void EvtTriglessWriterMt::writeEvents() {
  bool breakFlag{false};

  // workaround so Thomas can know which file is currently being written
  const auto infoRunPath = m_serverManager.dataPath() / "currently_processed_file.txt";
  auto getTime = [] () {
    time_t now;
    std::time(&now);
    char buf[20];
    std::strftime(buf, sizeof buf, "%FT%T", localtime(&now));
    // this will work too, if your compiler doesn't support %F or %T:
    //strftime(buf, sizeof buf, "%Y-%m-%dT%H:%M:%SZ", gmtime(&now));
    return std::string(buf);
  };

  auto startTime = getTime();

  while (!breakFlag) {

    // workaround so Thomas can know which file is currently being written
    {
      std::ofstream infoRun(infoRunPath);
      infoRun << "RUNNING\n";
      infoRun << startTime << '\n';
      infoRun << m_rootWriter.currentFilePath() << std::endl;
      infoRun << m_monitoringPath << "/data_0000.root" << std::endl;
    }

    // check if the event filter has finished
    breakFlag = m_processEvtsThreadFinished.load();

    // take all the filtered events
    auto evtQueue = m_eventsProcessed.takeAll(1000);

    m_daqMonitor->globalProcessedEventQueueLength(m_eventsProcessed.size());
    m_daqMonitor->threadProcessedEventQueueLength(evtQueue.size());

    // write all events
    while (!evtQueue.empty()) {
      auto evt = evtQueue.front();
      evtQueue.pop_front();
      m_daqMonitor->threadProcessedEventQueueLength(evtQueue.size());

      m_rootWriter.append(evt);

      m_daqMonitor->increaseWrittenEvents();
      m_daqMonitor->increaseWrittenHits(evt.nHits());
    }

  // workaround so Thomas can know which file is currently being written
    if (breakFlag) {
      std::ofstream infoRun(infoRunPath);
      infoRun << "FINISHED\n";
      infoRun << getTime() << '\n';
      infoRun << m_rootWriter.currentFilePath() << std::endl;
      infoRun << m_monitoringPath << "/data_0000.root" << std::endl;
    }
  }
  
}

#endif // USE_ROOT