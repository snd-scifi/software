#pragma once
#ifdef TOREMOVE
#include <fstream>
#include "io/writer.hpp"
#include "io/packet_writer_bin.hpp"
#include "monitoring/daq_monitor.hpp"

class BinPacketWriter : public Writer {
public:  
  BinPacketWriter(DaqServerManager& serverManager);
  ~BinPacketWriter() = default;
  std::string name() const { return "BinPacketWriter"; }

  void append(const std::deque<DataPacket>& packets);
  void finalize() {}

private:
  PacketWriterBin m_writer;
  DaqMonitorPtr m_daqMonitor;

};
#endif