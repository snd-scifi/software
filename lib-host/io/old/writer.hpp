#pragma once

#include <deque>

#include "storage/data_packet.hpp"
// #include "daq/daq_server_manager.hpp"

class DaqServerManager;

/**
 * Abstract class used to write data to file, with or without processing.
 */
class Writer {
public:
  Writer(DaqServerManager& serverManager) : m_serverManager(serverManager) {}
  virtual ~Writer() = default;
  virtual std::string name() const = 0;

  /**
   * Append DataPackets to the writer, which will be processed and written to file.
   * This function is called each time data is received by the DAQ server.
   */
  virtual void append(const std::deque<DataPacket>& packets) = 0;
  
  /**
   * Performs the final steps before closing the file.
   * This function is called once, when the DAQ is stopping.
   */
  virtual void finalize() = 0;

protected:
  DaqServerManager& m_serverManager;
};