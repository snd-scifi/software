#ifdef USE_ROOTT

#include <filesystem>

#include "daq/daq_server_manager.hpp"
#include "processing/boards_noise_filter.hpp"
#include "processing/snd_fast_noise_filter.hpp"
#include "processing/tofpet_calibrator.hpp"
#include "utils/logger.hpp"

#include "fmt/format.h"

#include "evt_trig_writer.hpp"

namespace fs = std::filesystem;

EvtTrigWriter::EvtTrigWriter(const std::string& path, DaqServerManager& serverManager)
  : Writer(serverManager)
  , m_rootWriter(path, m_serverManager.getBoardIds(), m_serverManager.writerSettings())
  , m_daqMonitor{std::make_shared<DaqMonitorEvtTrigWriter>(std::vector<uint32_t>(m_serverManager.getBoardIds()))}
{
  m_serverManager.daqMonitor(m_daqMonitor);

  for (const auto& processor : m_serverManager.writerSettings().value("processors", std::vector<std::string>())) {
    if (processor == "board_noise_filter") {
      m_eventProcessors.push_back(std::make_unique<SimpleNoiseFilter>(m_serverManager.writerSettings()["processors_settings"]["board_noise_filter"]));
    }
    else if (processor == "snd_fast_noise_filter") {
      m_eventProcessors.push_back(std::make_unique<SndFastNoiseFilter>(m_serverManager.boardMapping(), m_serverManager.writerSettings()["processors_settings"]["snd_fast_noise_filter"]));
    }
    else if (processor == "tofpet_calibrator") {
      m_eventProcessors.push_back(std::make_unique<TofpetCalibrator>(path));
    }
  }
  

  // m_dataTree->SetAutoSave(n); negative for n of bytes, positive for entries

}

/**
 * Append packets to the writer and performs the necessary processing.
 */
void EvtTrigWriter::append(const std::deque<DataPacketId>& packets) {
  // check the packet type of each hit
  // if it is a HIT, append it to the last BoardEvent of a given board,
  // if it is a TRIGGER, create a new BoardEvent for that board
  for (const auto& [boardId, packet] : packets) {
    switch (packet.type()) {
    case DataPacket::Hit:
      if (m_boardEvents[boardId].size() > 0) {
        auto p = static_cast<const HitPacket&>(packet);
        m_boardEvents[boardId].back().emplaceHit(std::move(p));
      }
      // else {
      //   WARN("Hit packet before trigger packet");
      // }
      break;
    case DataPacket::Trigger:
      {
        auto p = static_cast<const TriggerPacket&>(packet);
        m_boardEvents[boardId].emplace_back(p.timestamp());
      }
      break;
    default:
      break;
    }
  }

  // determines how many events are available to be written,
  // i.e. the length of the shortest queue of BoardEvents
  auto getReadyEvents = [this] () {
    size_t newEventsReady = std::numeric_limits<size_t>::max();
    for (const auto& [id, evts] : this->m_boardEvents) {
        newEventsReady = std::min(newEventsReady, evts.size());
    }
    return newEventsReady;
  };

  // we need to process all the events except the last one, as it may not be complete.
  // the remaining events at the end will be processed in finalize
  while (getReadyEvents() > 1) {
    processEvent();
    m_daqMonitor->increaseWrittenEvents(1);
    m_daqMonitor->increaseWrittenHits(m_eventTmp.nHits());
  }

}

void EvtTrigWriter::finalize() {
  // now the remaining number of events is given by the longest queue
  auto getRemainingEvents = [this] () {
    size_t newEventsReady = 0;
    for (const auto& [id, evts] : this->m_boardEvents) {
        newEventsReady = std::max(newEventsReady, evts.size());
    }
    return newEventsReady;
  };

  // we need to process all the remaining ones
  while (getRemainingEvents() > 0) {
    processEvent();
    m_daqMonitor->increaseWrittenEvents(1);
    m_daqMonitor->increaseWrittenHits(m_eventTmp.nHits());
  }

}

/**
 * Processes the first BoardEvent in each queue: verifies the timestamp consistency, builds the event and writes it to file.
 */
void EvtTrigWriter::processEvent() {
  // store the timestamps of the first BoardEvent of each queue in a map.
  // If one queue is empty, put int_64_max because later the alghorithm searches for the lowest timestamp
  std::map<uint32_t, int64_t> timestamps;
  for (const auto& [id, evt] : m_boardEvents) {
    if (evt.empty()) {
      timestamps[id] = std::numeric_limits<int64_t>::max();
    }
    else {
      timestamps[id] = evt.front().timestamp();
    }
  }

  // lambda function that compare the value of elements in a map and not their key (which is the default behaviour)
  auto compareSecond = [] (const std::pair<uint32_t, int64_t> a, const std::pair<uint32_t, int64_t> b) {
    return a.second < b.second;
  };

  // determine the minimum timestamp of the first BoardEvent of each queue
  auto minTs = (*std::min_element(timestamps.begin(), timestamps.end(), compareSecond)).second;

  // check the first BoardEvent of each queue. If it's not there (queue empty) or
  // if its timestamp is different from the minimum, add an empty event with the right timestamp
  for (auto& [id, evt] : m_boardEvents) {
    if (evt.empty()) {
      WARN("Queue for board ID {} empty. Adding BoardEvent.", id);
      evt.push_front(TofpetBoardEvent(minTs));
    }
    else if (evt.front().timestamp() != minTs) {
      WARN("Event for board ID {} with wrong timestamp. Adding BoardEvent.", id);
      evt.push_front(TofpetBoardEvent(minTs));
    }
  }

  // clear the temprary event and set its timestamp
  m_eventTmp.clear((*m_boardEvents.begin()).second.front().timestamp());
  // fill the temprary event with the front of each BoardEvents queue and remove it from the queue
  for (auto& [id, evt] : m_boardEvents) {
    m_eventTmp.emplaceBoardEvent(id, std::move(evt.front()));
    evt.pop_front();
  }

  // pass it though the processors. If they all return true, save it.
  bool saveEvent{true};
  for (const auto& processor : m_eventProcessors) {
    saveEvent = processor->process(m_eventTmp);
    if (!saveEvent) {
      return;
    }
  }

  // write the event in the TTrees
  writeEvent();
}

/**
 * Writes the temporary event m_eventTmp to the TTrees.
 */
void EvtTrigWriter::writeEvent() {
  m_rootWriter.append(m_eventTmp);
}

#endif // USE_ROOT