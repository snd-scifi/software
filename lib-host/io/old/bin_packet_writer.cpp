#include <iomanip>

#ifdef TOREMOVE
#include "utils/logger.hpp"
#include "daq/daq_server_manager.hpp"
#include "bin_packet_writer.hpp"

BinPacketWriter::BinPacketWriter(DaqServerManager& serverManager) 
  : Writer(serverManager)
  , m_writer{serverManager.runPath().string(), serverManager.daqServerSettings()}
  , m_daqMonitor{std::make_shared<DaqMonitor>(std::vector<uint32_t>(m_serverManager.getBoardIds()))}
{ // TODO to remove

  m_serverManager.daqMonitor(m_daqMonitor);

}

void BinPacketWriter::append(const std::deque<DataPacket>& packets) {
  for (const auto& packet : packets) {
    m_writer.append(packet);
  }
}

#endif