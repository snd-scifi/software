#pragma once
#ifdef USE_ROOTT

#include <atomic>
#include <condition_variable>
#include <deque>
#include <map>
#include <mutex>
#include <thread>
#include <vector>

#include "monitoring/daq_mon_evt_trigless_writer_mt.hpp"
#include "processing/event_processor.hpp"
#include "storage/deque_mt.hpp"
#include "storage/hit_queue.hpp"
#include "storage/tofpet_event.hpp"
#include "storage/tofpet_hit.hpp"
#include "io/writer.hpp"
#include "io/event_writer_root_multi_tree.hpp"
#include "io/event_writer_root.hpp"
#include "utils/root.hpp"

#include "TTree.h"

using TriggerQueue = std::deque<TriggerPacket>;
using EventQueue = DequeMt<TofpetEvent>;

/**
 * Triggerless event builder.
 * 
 * The data is written to a ROOT file containing a TTree with the event information (the `event` TTree) and one TTree per board with the corresponding hits.
 */
class EvtTriglessWriterMt : public Writer {
public:  
  EvtTriglessWriterMt(const std::string& path, DaqServerManager& serverManager);
  ~EvtTriglessWriterMt() = default;
  std::string name() const { return "EvtTriglessWriterMt"; }

  void append(const std::deque<DataPacket>& packets);
  void finalize();

private:
  void buildBoardEvents(const std::vector<uint32_t> boardIds);
  void buildEvents();
  void processEvents();
  void writeEvents();

  void checkTriggersConsistency();
  std::vector<uint32_t> findDesyncedBoardIds(const TrigMap map) const;

  std::vector<uint32_t> m_boardIds;

  std::map<uint32_t, HitQueue> m_hitMap;
  std::map<uint32_t, TriggerQueue> m_trigMap;
  EventQueue m_events;
  EventQueue m_eventsProcessed;

  std::vector<std::thread> m_buildBoardEvtsThreads;
  std::thread m_buildEvtsThread;
  std::thread m_processEvtsThread;
  std::thread m_writeEvtsThread;

  EventWriterRoot m_rootWriter;

  std::vector<std::unique_ptr<EventProcessor>> m_eventProcessors;

  std::atomic_int64_t m_lastGoodTimestamp; // hits up to this timestamp are grouped into BoardEvents
  std::atomic_int64_t m_lastSyncTimestamp; // hits up to this timestamp are guaranteed to come from a synched board //TODO should be per board
  std::map<uint32_t, std::atomic_int64_t> m_lastGoodBoardEvtTimestamp; // board events up to the minimum of this are grouped into events

  std::atomic_bool m_stopRequested{false};
  std::atomic_bool m_buildEvtsThreadFinished{false};
  std::atomic_bool m_processEvtsThreadFinished{false};

  DaqMonitorEvtTriglessWriterMtPtr m_daqMonitor;

  std::string m_monitoringPath;

};

#endif // USE_ROOT

