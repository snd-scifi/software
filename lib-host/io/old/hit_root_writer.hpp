#pragma once
#ifdef TOREMOVE
#ifdef USE_ROOT

#include "io/writer.hpp"
#include "utils/root.hpp"
#include "monitoring/daq_monitor.hpp"

#include "TTree.h"

class HitRootWriter : public Writer {
public:  
  HitRootWriter(const std::string& path, DaqServerManager& serverManager);
  ~HitRootWriter() {};
  std::string name() const { return "HitRootWriter"; }

  void append(const std::deque<DataPacket>& packets);
  void finalize() {}

private:
  RootFilePtr m_f;
  std::map<uint32_t, TTree*> m_dataTrees;

  uint8_t m_tofpetChannel, m_tofpetId, m_tac, m_packetType;
  uint16_t m_tFine, m_vCoarse, m_vFine;
  uint32_t m_cntValue;
  int64_t m_tCoarse;

  uint64_t m_trigTs;
  uint8_t m_trigType;
  uint64_t m_trigCnt;

  DaqMonitorPtr m_daqMonitor;
};

#endif // USE_ROOT
#endif