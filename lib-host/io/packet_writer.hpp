#pragma once

#include <string>
#include <vector>

// #include "daq/daq_server_manager.hpp"
#include "storage/data_packet.hpp"

#include "json.hpp"

class PacketWriter {
public:
  PacketWriter(std::string path, nlohmann::json writerSettings) : m_writerSettings(writerSettings), m_runDirectoryPath{path} {}
  virtual ~PacketWriter() = default;
  virtual std::string name() const = 0;

  /** Add the packet to the underlying device.
   *
   * The reference to the packet is only valid for the duration of the call.
   * Errors must be handled by throwing an appropriate exception.
   */
  virtual void append(const DataPacket& packet) = 0;
  virtual std::string currentFilePath() const {return m_filePath; };

protected:
  const nlohmann::json m_writerSettings;

  const std::filesystem::path m_runDirectoryPath;
  std::filesystem::path m_filePath;
};