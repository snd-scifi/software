#ifdef USE_ROOT

#include <algorithm>
#include <filesystem>

#include "utils/logger.hpp"

#include "event_reader_root_multi_tree.hpp"

namespace fs = std::filesystem;

EventReaderRootMultiTree::EventReaderRootMultiTree(std::string path) : EventReader(path), EventCommonRootMultiTree() 
 , m_numberOfDataFiles{0} {
  for (const auto& entry : fs::directory_iterator(m_runDirectoryPath)) {
    auto filename = entry.path().filename().string();
    if (filename.substr(0, 5) == "data_" && filename.substr(9, 5) == ".root") {
      m_numberOfDataFiles = std::max(m_numberOfDataFiles, static_cast<unsigned int>(std::stoi(filename.substr(5, 4)) + 1));
    }
  }
  VERBOSE("Found {} data_XXXX.root files.", m_numberOfDataFiles);
  openNextFile();
  for (const auto& [boardId, t] : m_dataTrees) {
    m_boardIds.push_back(boardId);
  }
}

uint64_t EventReaderRootMultiTree::numEvents() const {
  THROW_RE("numEvents() not implemented in the reader");
}

void EventReaderRootMultiTree::skip(uint64_t n) {
  THROW_RE("skip() not implemented in the reader");
}

bool EventReaderRootMultiTree::read(TofpetEvent& event) {
  if (m_eventsInFile <= m_eventIdx) {
    if (!openNextFile()) {
      return false;
    }
  }

  if(m_eventTree->GetEntry(m_eventIdx) <= 0) {
    THROW_RE("Could not read 'events' entry {}", m_eventIdx);
  }

  event.clear(m_evtTimestamp, m_evtNumber, m_flags);

  for(auto& [boardId, boardTree] : m_dataTrees) {
    if(boardTree->GetEntry(m_eventIdx) <= 0) {
      THROW_RE("Could not read 'board_{}' entry {}", boardId, m_eventIdx);
    }

    if (m_nHits == 0) {
      continue;
    }

    for (uint32_t i = 0; i < m_nHits; i++) {
      event.emplaceHit(
        boardId,
        m_tofpetId[i],
        m_tofpetChannel[i],
        m_tac[i],
        m_tCoarse[i],
        m_tFine[i],
        m_vCoarse[i],
        m_vFine[i],
        m_timestamp[i],
        m_timestampCalChi2[i],
        m_timestampCalDof[i],
        m_value[i],
        m_valueCalChi2[i],
        m_valueCalDof[i],
        m_valueSaturation[i]
      );
    }
  }

  m_eventIdx++;
  m_evtNumber++;
  return true;
}

bool EventReaderRootMultiTree::openNextFile() {
  if (m_numberOfDataFiles <= m_fileCounter) {
    return false;
  }
  
  m_filePath = m_runDirectoryPath / fmt::format("data_{:04d}.root", m_fileCounter);
  DEBUG("Opening {}", m_filePath);
  m_f = openRootRead(m_filePath);
  m_fileCounter++;

  m_f->GetObject("event", m_eventTree);
  if (!m_eventTree) {
    THROW_RE("TTree 'event' is not present in file {}", m_filePath);
  }
  m_eventTree->SetBranchAddress("timestamp", &m_evtTimestamp);

  if (m_eventTree->GetBranch("evt_number") == nullptr) {
    WARN("Branch 'evt_number' not present. Calculating event number.");
    m_evtNumber = 0;
  }
  else {
    m_eventTree->SetBranchAddress("evt_number", &m_evtNumber);
  }

  if (m_eventTree->GetBranch("flags") == nullptr) {
    WARN("Branch 'flags' not present. Setting flags to 0.");
    m_flags = 0;
  }
  else {
    m_eventTree->SetBranchAddress("flags", &m_flags);
  }

  
  m_flags = 0;

  m_eventsInFile = m_eventTree->GetEntriesFast();
  m_eventIdx = 0;

  auto keyList = m_f->GetListOfKeys();
  for(const auto&& key : *keyList) {
    std::string treeName(key->GetName());
    if (treeName.substr(0, 6) != "board_") {
      continue;
    }
    uint32_t boardId = std::stoul(treeName.substr(6));
    m_dataTrees.try_emplace(boardId, nullptr);
    m_f->GetObject(treeName.c_str(), m_dataTrees.at(boardId));
    if (!m_dataTrees.at(boardId)) {
      THROW_RE("Error opening TTree '{}' in file {}", treeName, m_filePath);
    }

    m_dataTrees.at(boardId)->SetBranchAddress("n_hits", &m_nHits);
    m_dataTrees.at(boardId)->SetBranchAddress("tofpet_id", m_tofpetId);
    m_dataTrees.at(boardId)->SetBranchAddress("tofpet_channel", m_tofpetChannel);
    m_dataTrees.at(boardId)->SetBranchAddress("tac", m_tac);
    m_dataTrees.at(boardId)->SetBranchAddress("t_coarse", m_tCoarse);
    m_dataTrees.at(boardId)->SetBranchAddress("t_fine", m_tFine);
    m_dataTrees.at(boardId)->SetBranchAddress("v_coarse", m_vCoarse);
    m_dataTrees.at(boardId)->SetBranchAddress("v_fine", m_vFine);

    if (!m_dataTrees.at(boardId)->GetBranch("timestamp")) {
      INFO("Calibrated branches not available for {}", treeName);
      for (size_t i = 0; i < kMaxHits; i++) {
        m_timestamp[i] = std::numeric_limits<float>::quiet_NaN();
        m_value[i] = std::numeric_limits<float>::quiet_NaN();
        m_timestampCalChi2[i] = std::numeric_limits<float>::quiet_NaN();
        m_timestampCalDof[i] = std::numeric_limits<float>::quiet_NaN();
        m_valueCalChi2[i] = std::numeric_limits<float>::quiet_NaN();
        m_valueCalDof[i] = std::numeric_limits<float>::quiet_NaN();
        m_valueSaturation[i] = std::numeric_limits<float>::quiet_NaN();
      }
      continue;
    }
    m_dataTrees.at(boardId)->SetBranchAddress("timestamp", m_timestamp);
    m_dataTrees.at(boardId)->SetBranchAddress("value", m_value);
    m_dataTrees.at(boardId)->SetBranchAddress("timestamp_cal_chi2", m_timestampCalChi2);
    m_dataTrees.at(boardId)->SetBranchAddress("timestamp_cal_dof", m_timestampCalDof);
    m_dataTrees.at(boardId)->SetBranchAddress("value_cal_chi2", m_valueCalChi2);
    m_dataTrees.at(boardId)->SetBranchAddress("value_cal_dof", m_valueCalDof);
    m_dataTrees.at(boardId)->SetBranchAddress("value_saturation", m_valueSaturation);
  }
  return true;
}

#endif // USE_ROOT
