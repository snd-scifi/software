#ifdef USE_ROOT

#include <algorithm>
#include <filesystem>

#include "utils/logger.hpp"

#include "event_writer_root.hpp"

namespace fs = std::filesystem;

EventWriterRoot::EventWriterRoot(std::string path, nlohmann::json writerSettings) 
  : EventWriter(path, writerSettings)
  , EventCommonRoot(writerSettings.value("data_tree_name", "data"))
  , m_fileEventCounter {0}
  , m_autoSaveEventCounter {0}
{
  m_fileEventLimit = m_writerSettings.value("events_per_file", 1000000UL);
  m_autoFlush = m_writerSettings.value("auto_flush", -30000000L);
  m_autoSave = m_writerSettings.value("auto_save", -300000000L);
  m_autoSaveNevents = m_writerSettings.value("auto_save_n_events", 1000UL);
  m_autoSaveDelayMin = m_writerSettings.value("auto_save_delay_min", 5.0);
  m_autoSaveDelayMax = m_writerSettings.value("auto_save_delay_max", 5.0);
  if (m_autoSaveDelayMax < m_autoSaveDelayMin) {
    WARN("Setting `auto_save_delay_min` ({}) must be <= `auto_save_delay_max` ({}). auto_save_delay_max takes priority.", m_autoSaveDelayMin, m_autoSaveDelayMax);
  }
  openNextFile();
}

void EventWriterRoot::append(const TofpetEvent& event) {
  if (m_fileEventLimit <= m_fileEventCounter) {
    openNextFile();
  }

  m_evtTimestamp = event.timestamp();
  m_evtNumber = event.evtNumber();
  m_flags = event.flags();
  m_nHits = event.nHits();

  unsigned int i{0};
  for (const auto& hit : event) {
    m_boardId[i] = hit.boardId();
    m_tofpetId[i] = hit.tofpetId();
    m_tofpetChannel[i] = hit.tofpetChannel();
    m_tac[i] = hit.tac();
    m_tCoarse[i] = hit.tCoarse();
    m_tFine[i] = hit.tFine();
    m_timestamp[i] = hit.timestamp();
    m_vCoarse[i] = hit.vCoarse();
    m_vFine[i] = hit.vFine();
    m_value[i] = hit.value();

    m_timestampCalChi2[i] = hit.timestampCalChi2();
    m_timestampCalDof[i] = hit.timestampCalDof();
    m_valueCalChi2[i] = hit.valueCalChi2();
    m_valueCalDof[i] = hit.valueCalDof();
    m_valueSaturation[i] = hit.valueSaturation();
    i++;
  }

  if (i != m_nHits) {
    WARN("Inconsistency in number of hits: {} != {}", i, m_nHits);
  }

  m_dataTree->Fill();

  m_fileEventCounter++;
  m_autoSaveEventCounter++;

  const auto elapsed = m_autoSaveTimer.elapsed();
  // call the autosave if the minimum time has passed AND the maximum number or events is reached OR if the maximum time has passed
  // we also write the first event, to have the trees immediately accessible
  if ((m_autoSaveDelayMin < elapsed && m_autoSaveNevents < m_autoSaveEventCounter) || m_autoSaveDelayMax < elapsed || m_fileEventCounter == 1) {
    m_dataTree->AutoSave("SaveSelf");
    m_autoSaveEventCounter = 0;
    m_autoSaveTimer.reset();
  }
}

void EventWriterRoot::openNextFile() {
  m_filePath = m_runDirectoryPath / fmt::format("data_{:04d}.root", m_fileCounter);
  DEBUG("Opening {}", m_filePath);
  m_f = openRootWrite(m_filePath,
    m_writerSettings.value("compression_algorithm", ROOT::kZSTD),
    m_writerSettings.value("compression_level", 0)
  );
  m_fileCounter++;
  m_fileEventCounter = 0;
  m_autoSaveEventCounter = 0;

  m_dataTree = new TTree(m_dataTreeName.c_str(), "Events");
  m_dataTree->SetDirectory(m_f.get());

  m_dataTree->Branch("evt_timestamp", &m_evtTimestamp, "evtTimestamp/L");
  m_dataTree->Branch("evt_number", &m_evtNumber, "evtNumber/l");
  m_dataTree->Branch("evt_flags", &m_flags, "evtFlags/l");
  m_dataTree->Branch("n_hits", &m_nHits, "nHits/i");

  m_dataTree->Branch("board_id", m_boardId, "boardId[nHits]/i");
  m_dataTree->Branch("tofpet_id", m_tofpetId, "tofpetId[nHits]/b");
  m_dataTree->Branch("tofpet_channel", m_tofpetChannel, "tofpetChannel[nHits]/b");
  m_dataTree->Branch("tac", m_tac, "tac[nHits]/b");
  m_dataTree->Branch("t_coarse", m_tCoarse, "tCoarse[nHits]/L");
  m_dataTree->Branch("t_fine", m_tFine, "tFine[nHits]/s");
  m_dataTree->Branch("timestamp", m_timestamp, "timestamp[nHits]/F");
  m_dataTree->Branch("v_coarse", m_vCoarse, "vCoarse[nHits]/s");
  m_dataTree->Branch("v_fine", m_vFine, "vFine[nHits]/s");
  m_dataTree->Branch("value", m_value, "value[nHits]/F");

  m_dataTree->Branch("timestamp_cal_chi2", m_timestampCalChi2, "timestampCalChi2[nHits]/F");
  m_dataTree->Branch("timestamp_cal_dof", m_timestampCalDof, "timestampCalDof[nHits]/F");
  m_dataTree->Branch("value_cal_chi2", m_valueCalChi2, "valueCalChi2[nHits]/F");
  m_dataTree->Branch("value_cal_dof", m_valueCalDof, "valueCalDof[nHits]/F");
  m_dataTree->Branch("value_saturation", m_valueSaturation, "valueSaturation[nHits]/F");

  m_dataTree->SetAutoFlush(m_autoFlush);
  m_dataTree->SetAutoSave(m_autoSave);

  m_autoSaveTimer.reset();

}

void EventWriterRoot::finalize() {
  // this prevents ROOT from hanging when writing the last file. If this happens in the destructor, somehow it doesn't work...
  m_f = uninitializedRootFilePtr();
}

#endif // USE_ROOT
