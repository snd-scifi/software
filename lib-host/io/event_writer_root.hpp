#pragma once

#ifdef USE_ROOT

#include <string>

#include "io/event_writer.hpp"
#include "io/event_common_root.hpp"

#include "utils/timer.hpp"

class EventWriterRoot : public EventWriter, public EventCommonRoot {
public:
  EventWriterRoot(std::string path, nlohmann::json writerSettings = nlohmann::json({}));
  virtual ~EventWriterRoot() = default;
  std::string name() const override { return "EventWriterRoot"; }

  /** Add the event to the underlying device.
   *
   * The reference to the event is only valid for the duration of the call.
   * Errors must be handled by throwing an appropriate exception.
   */
  void append(const TofpetEvent& event) override;
  void finalize() override;

private:
  void openNextFile();

  size_t m_fileEventCounter;
  size_t m_autoSaveEventCounter;

  Timer m_autoSaveTimer;

  unsigned long m_fileEventLimit;
  long m_autoFlush;
  long m_autoSave;
  unsigned long m_autoSaveNevents;
  double m_autoSaveDelayMin;
  double m_autoSaveDelayMax;
};

#endif // USE_ROOT
