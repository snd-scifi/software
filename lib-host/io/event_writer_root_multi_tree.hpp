#pragma once

#ifdef USE_ROOT

#include <string>

#include "io/event_writer.hpp"
#include "io/event_common_root_multi_tree.hpp"

#include "utils/timer.hpp"

class EventWriterRootMultiTree : public EventWriter, public EventCommonRootMultiTree {
public:
  EventWriterRootMultiTree(std::string path, std::vector<uint32_t> boardIds, nlohmann::json writerSettings = nlohmann::json({}));
  virtual ~EventWriterRootMultiTree() = default;
  virtual std::string name() const { return "EventWriterRootMultiTree"; }

  /** Add the event to the underlying device.
   *
   * The reference to the event is only valid for the duration of the call.
   * Errors must be handled by throwing an appropriate exception.
   */
  virtual void append(const TofpetEvent& event);
  void finalize() override;

private:
  void openNextFile();

  std::vector<uint32_t> m_boardIds;
  std::map<uint32_t, std::vector<TofpetHit>> m_hitsMap;

  size_t m_fileEventCounter;
  size_t m_autoSaveEventCounter;

  Timer m_autoSaveTimer;

  unsigned long m_fileEventLimit;
  long m_autoFlush;
  long m_autoSave;
  unsigned long m_autoSaveNevents;
  double m_autoSaveDelayMin;
  double m_autoSaveDelayMax;
};

#endif // USE_ROOT
