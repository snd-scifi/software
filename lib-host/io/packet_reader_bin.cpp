#include "utils/logger.hpp"

#include "fmt/core.h"

#include "packet_reader_bin.hpp"

PacketReaderBin::PacketReaderBin(std::string path, std::size_t bufferSize)
 : PacketReader(path)
 , m_filePacketCounter{0}
 , m_fileCounter{0}
{
  openNextFile();
}


PacketReaderBin::~PacketReaderBin() {}

bool PacketReaderBin::openNextFile() {
  if (m_f.is_open()) {
    DEBUG("Closing {}", m_filePath);
    m_f.close();
  }

  m_filePath = m_runDirectoryPath / fmt::format("data_{:04d}.raw", m_fileCounter);
  DEBUG("Opening {}", m_filePath);

  m_fileCounter++;
  m_filePacketCounter = 0;


  m_f.open(m_filePath.string(), std::ios::binary);
  return m_f.is_open();
}

bool PacketReaderBin::read(DataPacket& packet) {

  // lambda function to read the next 17 bytes, returns true on success
  auto readPacket = [&packet, this] () {
    std::vector<char> buffer(17);
    if (m_f.read(buffer.data(), 17)) {
      packet = std::move(DataPacket::deserializePacket(buffer.data()));
      return true;
    }
    else {
      return false;
    }
  };

  // tries to read the next packet. If this fails, tries to open the next file.
  // if this fails, we have no more files to read
  while (!readPacket()) {
    if (!openNextFile()) {
      return false;
    }
  }

  return true;
  
}