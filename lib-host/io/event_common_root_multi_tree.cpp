#ifdef USE_ROOT

#include "event_common_root_multi_tree.hpp"

EventCommonRootMultiTree::EventCommonRootMultiTree() 
  : m_fileEventCounter{0}
  , m_fileCounter{0}
  , m_f(uninitializedRootFilePtr())
  {}

#endif // USE_ROOT
