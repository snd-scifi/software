#include "utils/logger.hpp"

#include "fmt/core.h"

#include "packet_writer_bin.hpp"

PacketWriterBin::PacketWriterBin(std::string path, nlohmann::json writerSettings) 
  : PacketWriter(path, writerSettings)
  , m_filePacketLimit{m_writerSettings.value("packets_per_file", 100000000UL)}
  , m_filePacketCounter{0}
  , m_fileCounter{0}
{
  openNextFile();
}

void PacketWriterBin::append(const DataPacket& packet) {
  char serPacket[17];
  packet.serializePacket(serPacket);

  m_f.write(serPacket, 17);
  m_filePacketCounter++;
  if (m_filePacketCounter >= m_filePacketLimit) {
    openNextFile();
  }
}

void PacketWriterBin::openNextFile() {
  if (m_f.is_open()) {
    DEBUG("Closing {}", m_filePath);
    m_f.close();
  }

  m_filePath = m_runDirectoryPath / fmt::format("data_{:04d}.raw", m_fileCounter);
  DEBUG("Opening {}", m_filePath);

  m_fileCounter++;
  m_filePacketCounter = 0;


  m_f.open(m_filePath.string(), std::ios::binary);
}
