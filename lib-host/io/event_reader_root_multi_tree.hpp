#pragma once

#ifdef USE_ROOT

#include <string>
#include <vector>

#include "io/event_reader.hpp"
#include "io/event_common_root_multi_tree.hpp"

class EventReaderRootMultiTree : public EventReader, public EventCommonRootMultiTree {
public:
  EventReaderRootMultiTree(std::string path);
  virtual ~EventReaderRootMultiTree() = default;
  virtual std::string name() const { return "EventReaderRootMultiTree"; }

  uint64_t numEvents() const;
  void skip(uint64_t n);
  bool read(TofpetEvent& event);
  std::vector<uint32_t> boardIds() const { return m_boardIds; };

private:
  bool openNextFile();
  unsigned int m_numberOfDataFiles;
  size_t m_eventsInFile;
  size_t m_eventIdx;

  std::vector<uint32_t> m_boardIds;

};

#endif // USE_ROOT
