#include <filesystem>
#include <fstream>

#include "io/event_writer_root.hpp"
#include "utils/logger.hpp"

#include "event_writer_wrapper.hpp"

namespace fs = std::filesystem;
using json = nlohmann::json;

EventWriterWrapper::EventWriterWrapper(EventQueue& eventQueue, DaqServerManager& serverManager) 
  : m_events{eventQueue}
  , m_serverManager{serverManager}
  , m_stopRequested{false}
  , m_monitor{std::make_shared<EventWriterMonitor>()}
{
  std::string eventWriterName;
  try {
    eventWriterName = m_serverManager.eventWriterSettings().at("writer").get<std::string>();
  }
  catch (const json::out_of_range& e) {
    THROW_RE("Event writer was not specified in the configuration.");
  }

  if (eventWriterName == "events_root") {
#ifdef USE_ROOT
    m_writer = std::make_unique<EventWriterRoot>(m_serverManager.runPath(), serverManager.eventWriterSettings());
#else
    THROW_RE("Software was compiled without ROOT enabled. Call cmake with option -DUSE_ROOT=ON");
#endif
  }
  else {
    THROW_RE("Unknown event writer '{}'", eventWriterName);
  }
    
  m_serverManager.daqMonitor()->addMonitor(m_monitor);
  
  m_writerThread = std::thread([this] { writerLoop(); });
  NOTICE("Writer thread started.");
}

void EventWriterWrapper::writerLoop() {
  bool breakFlag{false};

  std::string lastFilePath;

  while (!breakFlag) {
    // check if the event filter has finished
    breakFlag = m_stopRequested.load();

    // take all the filtered events
    if (breakFlag) NOTICE("Take all events...");
    auto evtQueue = m_events.takeAll(1000);

    m_serverManager.daqMonitor()->processedEventQueueLength(m_events.size());
    m_monitor->localEventQueueLength(evtQueue.size());

    if (breakFlag) NOTICE("Update run status...");
    if (lastFilePath != m_writer->currentFilePath()) {
      lastFilePath = m_writer->currentFilePath();
      json element;
      element["currently_written_file"] = lastFilePath;
      m_serverManager.addRunStatusElements(element);
      m_serverManager.writeRunStatus();
    }

    if (breakFlag) NOTICE("Write all events...");
    // write all events
    while (!evtQueue.empty()) {
      auto evt = evtQueue.front();
      evtQueue.pop_front();
      m_monitor->localEventQueueLength(evtQueue.size());

      m_writer->append(evt);

      m_monitor->increaseWrittenEvents();
      m_monitor->increaseWrittenHits(evt.nHits());
    }
    if (breakFlag) NOTICE("All events written.");
  }
  NOTICE("calling finalize");
  m_writer->finalize();
  NOTICE("writer loop returning");
  
}

void EventWriterWrapper::finalize() {
  NOTICE("Stopping writer thread...");
  m_stopRequested = true;
  m_writerThread.join();
}