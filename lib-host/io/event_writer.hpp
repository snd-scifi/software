#pragma once

#include <string>
#include <vector>

// #include "daq/daq_server_manager.hpp"
#include "storage/tofpet_event.hpp"

#include "json.hpp"

class EventWriter {
public:
  // EventWriter(DaqServerManager& daqServerManager) : m_serverManager{daqServerManager} {}
  EventWriter(std::string path, nlohmann::json writerSettings)
    : m_writerSettings(writerSettings)
    , m_runDirectoryPath{path} {}
  virtual ~EventWriter() = default;
  virtual std::string name() const = 0;

  /** Add the event to the writer.
   *
   * The reference to the event is only valid for the duration of the call.
   * Errors must be handled by throwing an appropriate exception.
   */
  virtual void append(const TofpetEvent& event) = 0;
  virtual void finalize() {};
  virtual std::string currentFilePath() const { return m_filePath; };

protected:
  // DaqServerManager& m_serverManager;
  const nlohmann::json m_writerSettings;

  const std::filesystem::path m_runDirectoryPath;
  std::filesystem::path m_filePath;

};