#ifdef USE_ROOT

#include "event_common_root.hpp"

EventCommonRoot::EventCommonRoot(std::string dataTreeName) 
  : m_fileEventCounter{0}
  , m_fileCounter{0}
  , m_f(uninitializedRootFilePtr())
  , m_dataTreeName(dataTreeName)
  {}

#endif // USE_ROOT
