#pragma once

#include <iostream>

namespace ttcvi {

static constexpr long CONF_ID_EEPROM_OFFSET = 0x00;

static constexpr long CSR1_OFFSET = 0x80;
static constexpr long CSR1_TRIGGER_SELECT_MASK = 0x0007;
static constexpr long CSR1_ORBIT_INPUT_MASK = 0x0008;
static constexpr long CSR1_L1A_FIFO_FULL_MASK = 0x0010;
static constexpr long CSR1_L1A_FIFO_EMPTY_MASK = 0x0020;
static constexpr long CSR1_L1A_FIFO_RESET_MASK = 0x0040;
static constexpr long CSR1_VME_TRANSFER_PENDING_MASK = 0x0080;
static constexpr long CSR1_BC_DELAY_MASK = 0x0F00;
static constexpr long CSR1_TRIGGER_RATE_MASK = 0x7000;
static constexpr long CSR1_EVENT_ORBIT_COUNT_MASK = 0x8000;
static constexpr long CSR1_WRITE_ONLY_BITS = 0x0040;

static constexpr long CSR2_OFFSET = 0x82;
static constexpr long CSR2_RESET_MASK_BASE = 0x1000;
static constexpr long CSR2_RETRANSMIT_MASK_BASE = 0x0100;
static constexpr long CSR2_EMPTY_MASK_BASE = 0x0001;
static constexpr long CSR2_FULL_MASK_BASE = 0x0002;
static constexpr long CSR2_WRITE_ONLY_BITS = 0xF000;

static constexpr long SOFT_RST_GEN_OFFSET = 0x84;
static constexpr long SOFT_L1A_GEN_OFFSET = 0x86; // needs CSR1[2:0] == 4

static constexpr long EVENT_ORBIT_COUNTER_MSW_OFFSET = 0x88; // 8-bit
static constexpr long EVENT_ORBIT_COUNTER_LSW_OFFSET = 0x8A; // 16-bit

static constexpr long SOFT_INT_CNT_RST_OFFSET = 0x8C;

static constexpr long B_GO_MODE_BASE = 0x90;
static constexpr long INHIBIT_DELAY_BASE = 0x92; // 12 bits
static constexpr long INHIBIT_DURATION_BASE = 0x94; // 8 bits
static constexpr long SOFT_B_GO_GEN_BASE = 0x96;
static constexpr long B_GO_DELTA = 0x08;

static constexpr long LF_ASYNC_CYCLES_OFFSET = 0xC0; //can I do 32 bit access?
static constexpr long SF_ASYNC_CYCLES_OFFSET = 0xC4;


static constexpr long TRIGWORD_ADDRESS_OFFSET = 0xC8;
static constexpr long TRIGWORD_SUB_ADDRESS_OFFSET = 0xCA;
static constexpr long TRIGWORD_SUBADDRESS_MASK = 0x00FC;
static constexpr long TRIGWORD_INT_EXT_MASK = 0x0100;

static constexpr long BCH_DATA_B_GO_BASE = 0xB0;
static constexpr long BCH_DATA_B_GO_DELTA = 0x04;
static constexpr long BCH_LS_MASK = 0x80000000; //1 => 32-bit, 0=> 8bit
static constexpr long BCH_L_TTCrx_ADDR_MASK = 0x7FFE0000;
static constexpr long BCH_L_E_MASK = 0x00010000;
static constexpr long BCH_L_SUBADDR_MASK = 0x0000FF00;
static constexpr long BCH_L_DATA_MASK = 0x000000FF;

static constexpr long LONG_B_COMMAND_OFFSET_MSW = 0xC0; // 32-bit write
static constexpr long LONG_B_COMMAND_OFFSET_LSW = 0xC2; 
static constexpr long SHORT_B_COMMAND_OFFSET = 0xC4;


/**
 * Possible options for the selection of the L1A trigger input.
 */
enum L1ATriggerInput {
  L1A_0, //!< Front panel input 0 (ecl)
  L1A_1, //!< Front panel input 1 (NIM)
  L1A_2, //!< Front panel input 2 (NIM)
  L1A_3, //!< Front panel input 3 (NIM)
  VME_FUNC, //!< Trigger sent by a VME write access of the register at offset 0x86
  RANDOM, //!< Random trigger generation with selectable frequency
  CALIBR, //!< Calibration trigger, using B-Go<2>
  DISABLE //!< Trigger disabled
};

std::ostream& operator<<(std::ostream& out, const L1ATriggerInput value);
L1ATriggerInput l1aTriggerInputFromString(const std::string input);
std::string l1aTriggerInputToString(const L1ATriggerInput input);

/**
 * Possible options for the random trigger rate.
 */
enum RandomTriggerRate {
  TR_1, //!< 1 Hz
  TR_100, //!< 100 Hz
  TR_1k, //!< 1 kHz
  TR_5k, //!< 5 kHz
  TR_10k, //!< 10 kHz
  TR_25k, //!< 25 kHz
  TR_50k, //!< 50 kHz
  TR_100k //!< 100 kHz
};

std::ostream& operator<<(std::ostream& out, const RandomTriggerRate value);
RandomTriggerRate randomTriggerRateFromString(const std::string rate);
std::string randomTriggerRateToString(const RandomTriggerRate rate);

/**
 * Possible options for the orbit input selection.
 */
enum OrbitInput {
  EXT, //!< Orbit signal is taken from the ORBIT ecl input
  INT //!< Orbit signal is generated internally
};

std::ostream& operator<<(std::ostream& out, const OrbitInput value);
OrbitInput orbitInputFromString(const std::string input);
std::string orbitInputToString(const OrbitInput input);

/**
 * Possible options for the source of the internal 24-bit counter.
 */
enum EventOrbitCounterSource {
  EVENT_CNT, //!< The counter counts the L1A events (CHECK)
  ORBIT_CNT //!< The counter counts the orbit pulses
};
std::ostream& operator<<(std::ostream& out, const EventOrbitCounterSource value);

/**
 * In long commands, it defines wether the subaddress and data are referred to the TTCrx or to the external electronics.
 */
enum TTCrxE {
  TTCrx,
  EXTERNAL
};
std::ostream& operator<<(std::ostream& out, const TTCrxE e);
TTCrxE ttcrxeFromString(const std::string e);
std::string ttcrxeFromString(const TTCrxE e);

}