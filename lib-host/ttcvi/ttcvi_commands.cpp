#include "utils/logger.hpp"
#include "ttcvi/ttcvi.hpp"

#include "ttcvi_commands.hpp"

namespace ttcviCommands {

using namespace ttcvi;

/**
 * returns...
 */

json getBcDelay(const std::unique_ptr<TTCvi>& ttcvi, const json args) {
  json reply;
  try {
    auto delay = ttcvi->getBCDelay();
    reply["response"] = "ok";
    reply["result"] = delay;
  }
  catch (std::runtime_error& re) {
    WARN("getBcDelay: runtime error: {}", re.what());
    reply["response"] = "err";
    reply["result"] = re.what();
  }
  return reply;
}

json getL1ATrigger(const std::unique_ptr<TTCvi>& ttcvi, const json args){
  json reply;
  json result;

  try {
    result["l1a_input"] = ttcvi->getL1ATriggerInput();
    result["random_trigger_rate"] = ttcvi->getRandomTriggerRate();
    result["l1a_fifo_empty"] = ttcvi->isL1AFIFOempty();
    result["l1a_fifo_full"] = ttcvi->isL1AFIFOfull();

    reply["response"] = "ok";
    reply["result"] = result;
  }
  catch (std::runtime_error& re) {
    WARN("getBcDelay: runtime error: {}", re.what());
    reply["response"] = "err";
    reply["result"] = re.what();
  }
  return reply;
  
}

json setL1ATrigger(const std::unique_ptr<TTCvi>& ttcvi, const json args){
  json reply;
  std::vector<std::string> errors;

  if (args.contains("l1a_input")) {
    try {
      auto input = args.at("l1a_input").get<L1ATriggerInput>();
      ttcvi->setL1ATriggerInput(input);
      INFO("L1A input set to {}.", input);
    }
    catch (std::invalid_argument& ia) {
      WARN("setL1ATrigger, l1a_input: invalid argument: {}", ia.what());
      errors.push_back(ia.what());
    }
    catch (std::runtime_error& re) {
      WARN("setL1ATrigger, l1a_input: runtime error: {}", re.what());
      errors.push_back(re.what());
    }
  }

  if (args.contains("random_trigger_rate")) {
    try {
      auto rate = args.at("random_trigger_rate").get<RandomTriggerRate>();
      ttcvi->setRandomTriggerRate(rate);
      INFO("L1A random trigger rate set to {}.", rate);
    }
    catch (std::invalid_argument& ia) {
      WARN("setL1ATrigger, random_trigger_rate: invalid argument: {}", ia.what());
      errors.push_back(ia.what());
    }
    catch (std::runtime_error& re) {
      WARN("setL1ATrigger, random_trigger_rate: runtime error: {}", re.what());
      errors.push_back(re.what());
    }
  }

  if (args.contains("reset_fifo")) {
    try {
      auto reset = args.at("reset_fifo").get<bool>();
      if (reset) {
        ttcvi->resetL1AFIFO();
        INFO("L1A FIFO reset.");
      }
    }
    catch (std::invalid_argument& ia) {
      WARN("setL1ATrigger, reset_fifo: invalid argument: {}", ia.what());
      errors.push_back(ia.what());
    }
    catch (std::runtime_error& re) {
      WARN("setL1ATrigger, reset_fifo: runtime error: {}", re.what());
      errors.push_back(re.what());
    }
  }

  if (errors.size() == 0) {
    reply["response"] = "ok";
    reply["result"] = {};
  }
  else {
    reply["response"] = "err";
    reply["result"] = errors;
  }

  return reply;

}

json getOrbitInput(const std::unique_ptr<ttcvi::TTCvi>& ttcvi, const json args) {
  json reply;

  try {
    reply["response"] = "ok";
    reply["result"] = ttcvi->getOrbitInput();
  }
  catch (std::runtime_error& re) {
    WARN("getOrbitInput: runtime error: {}", re.what());
    reply["response"] = "err";
    reply["result"] = re.what();
  }
  return reply;
}

json setOrbitInput(const std::unique_ptr<ttcvi::TTCvi>& ttcvi, const json args) {
  json reply;

  try {
    auto input = args.at("orbit_input").get<OrbitInput>();
    ttcvi->setOrbitInput(input);
    INFO("Orbit input set to {}.", input);
    reply["response"] = "ok";
    reply["result"] = {};
  }
  catch (std::invalid_argument& ia) {
    WARN("setOrbitInput: invalid argument: {}", ia.what());
    reply["response"] = "err";
    reply["result"] = ia.what();
  }
  catch (std::runtime_error& re) {
    WARN("setOrbitInput: runtime error: {}", re.what());
    reply["response"] = "err";
    reply["result"] = re.what();
  }

  return reply;
}

json getEventCounter(const std::unique_ptr<ttcvi::TTCvi>& ttcvi, const json args) {
  json reply;

  try {
    reply["response"] = "ok";
    reply["result"] = ttcvi->getEventOrbitCounter();
  }
  catch (std::runtime_error& re) {
    WARN("getEventCounter: runtime error: {}", re.what());
    reply["response"] = "err";
    reply["result"] = re.what();
  }
  return reply;
}

json setEventCounter(const std::unique_ptr<ttcvi::TTCvi>& ttcvi, const json args) {
  json reply;

  try {
    auto value = args.at("counter_value").get<int>();
    ttcvi->setEventOrbitCounter(value);
    INFO("Event counter set to {}.", value);
    reply["response"] = "ok";
    reply["result"] = {};
  }
  catch (std::invalid_argument& ia) {
    WARN("setEventCounter: invalid argument: {}", ia.what());
    reply["response"] = "err";
    reply["result"] = ia.what();
  }
  catch (std::runtime_error& re) {
    WARN("setEventCounter: runtime error: {}", re.what());
    reply["response"] = "err";
    reply["result"] = re.what();
  }

  return reply;
}

json generateSoftL1A(const std::unique_ptr<ttcvi::TTCvi>& ttcvi, const json args) {
  json reply;

  try {
    ttcvi->generateSoftL1A();
    INFO("Generated software L1A.");
    reply["response"] = "ok";
    reply["result"] = {};
  }
  catch (std::runtime_error& re) {
    WARN("generateSoftL1A: runtime error: {}", re.what());
    reply["response"] = "err";
    reply["result"] = re.what();
  }

  return reply;
}

json generateSoftReset(const std::unique_ptr<ttcvi::TTCvi>& ttcvi, const json args) {
  json reply;

  try {
    ttcvi->generateSoftReset();
    INFO("Generated software reset.");
    reply["response"] = "ok";
    reply["result"] = {};
  }
  catch (std::runtime_error& re) {
    WARN("generateSoftReset: runtime error: {}", re.what());
    reply["response"] = "err";
    reply["result"] = re.what();
  }

  return reply;
}

json generateSoftBGo(const std::unique_ptr<ttcvi::TTCvi>& ttcvi, const json args) {
  json reply;

  try {
    auto id = args.at("id").get<short>();
    ttcvi->generateSoftBGo(id);
    INFO("Generated software B-Go {}.", id);
    reply["response"] = "ok";
    reply["result"] = {};
  }
  catch (nlohmann::detail::out_of_range& oor) {
    WARN("generateSoftBGo: argument id is required.");
    reply["response"] = "err";
    reply["result"] = "argument id is required";
  }
  catch (std::invalid_argument& ia) {
    WARN("generateSoftBGo: invalid argument: {}", ia.what());
    reply["response"] = "err";
    reply["result"] = ia.what();
  }
  catch (std::runtime_error& re) {
    WARN("generateSoftBGo: runtime error: {}", re.what());
    reply["response"] = "err";
    reply["result"] = re.what();
  }

  return reply;
}

json sendBChannelShort(const std::unique_ptr<ttcvi::TTCvi>& ttcvi, const json args){
  json reply;

  try {
    auto command = args.at("command").get<uint8_t>();
    ttcvi->sendBChannelShort(command);
    INFO("Sent short B-channel command: {:#04x}.", command);
    reply["response"] = "ok";
    reply["result"] = {};
  }
  catch (nlohmann::detail::out_of_range& oor) {
    WARN("sendBChannelShort: argument command is required.");
    reply["response"] = "err";
    reply["result"] = "argument command is required";
  }
  catch (std::runtime_error& re) {
    WARN("sendBChannelShort: runtime error: {}", re.what());
    reply["response"] = "err";
    reply["result"] = re.what();
  }

  return reply;
}

json sendBChannelLong(const std::unique_ptr<ttcvi::TTCvi>& ttcvi, const json args){
  json reply;

  try {
    auto address = args.at("address").get<uint16_t>();
    auto e = args.at("e").get<TTCrxE>();
    auto subaddress = args.at("subaddress").get<uint8_t>();
    auto command = args.at("command").get<uint8_t>();
    ttcvi->sendBChannelLong(address, e, subaddress, command);
    INFO("Sent long B-channel command. Addr: {:#06x}, E: {}, subaddr: {:#04x}, command: {:#04x}.", address, e, subaddress, command);
    reply["response"] = "ok";
    reply["result"] = {};
  }
  catch (nlohmann::detail::out_of_range& oor) {
    WARN("sendBChannelLong: arguments address, e, subaddress, command are required.");
    reply["response"] = "err";
    reply["result"] = "arguments address, e, subaddress, command are required.";
  }
  catch (std::runtime_error& re) {
    WARN("sendBChannelLong: runtime error: {}", re.what());
    reply["response"] = "err";
    reply["result"] = re.what();
  }

  return reply;
}

json addBChannelDataShort(const std::unique_ptr<ttcvi::TTCvi>& ttcvi, const json args) {
  json reply;

  try {
    auto id = args.at("id").get<short>();
    auto command = args.at("command").get<uint8_t>();
    ttcvi->addBChannelDataShort(id, command);
    INFO("Added short B-channel command {:#04x} to FIFO {}.", command, id);
    reply["response"] = "ok";
    reply["result"] = {};
  }
  catch (nlohmann::detail::out_of_range& oor) {
    WARN("addBChannelDataShort: arguments id, command are required.");
    reply["response"] = "err";
    reply["result"] = "arguments id, command are required";
  }
  catch (std::invalid_argument& ia) {
    WARN("addBChannelDataShort: invalid argument: {}", ia.what());
    reply["response"] = "err";
    reply["result"] = ia.what();
  }
  catch (std::runtime_error& re) {
    WARN("addBChannelDataShort: runtime error: {}", re.what());
    reply["response"] = "err";
    reply["result"] = re.what();
  }

  return reply;
}

json addBChannelDataLong(const std::unique_ptr<ttcvi::TTCvi>& ttcvi, const json args) {
  json reply;

  try {
    auto id = args.at("id").get<short>();
    auto address = args.at("address").get<uint16_t>();
    auto e = args.at("e").get<TTCrxE>();
    auto subaddress = args.at("subaddress").get<uint8_t>();
    auto command = args.at("command").get<uint8_t>();
    ttcvi->addBChannelDataLong(id, address, e, subaddress, command);
    INFO("added long B-channel command to FIFO {}. Addr: {:#06x}, E: {}, subaddr: {:#04x}, command: {:#04x}.", id, address, e, subaddress, command);
    reply["response"] = "ok";
    reply["result"] = {};
  }
  catch (nlohmann::detail::out_of_range& oor) {
    WARN("addBChannelDataLong: arguments id, address, e, subaddress, command are required.");
    reply["response"] = "err";
    reply["result"] = "arguments id, address, e, subaddress, command are required.";
  }
  catch (std::invalid_argument& ia) {
    WARN("addBChannelDataLong: invalid argument: {}", ia.what());
    reply["response"] = "err";
    reply["result"] = ia.what();
  }
  catch (std::runtime_error& re) {
    WARN("addBChannelDataLong: runtime error: {}", re.what());
    reply["response"] = "err";
    reply["result"] = re.what();
  }

  return reply;
}

json setBGo(const std::unique_ptr<ttcvi::TTCvi>& ttcvi, const json args) {
  json reply;
  std::vector<std::string> errors;
  short id;
  try {
    id = args.at("id").get<short>();
  }
  catch (nlohmann::detail::out_of_range& oor) {
    WARN("setBGo: arguments id is required.");
    reply["response"] = "err";
    reply["result"] = "arguments id is required.";
    return reply;
  }
  catch (std::invalid_argument& ia) {
    WARN("setBGo: invalid argument: {}", ia.what());
    reply["response"] = "err";
    reply["result"] = ia.what();
    return reply;
  }
  catch (std::runtime_error& re) {
    WARN("setBGo: runtime error: {}", re.what());
    reply["response"] = "err";
    reply["result"] = re.what();
    return reply;
  }

  if (args.contains("front_panel_enable")) {
    try {
      auto enable = args.at("front_panel_enable").get<bool>();
      ttcvi->setBGoFrontPanelEnable(id, enable);
      INFO("B-Go {}: front panel enable set to {}", id, enable);
    }
    catch (std::invalid_argument& ia) {
      WARN("setBGo, front_panel_enable: invalid argument: {}", ia.what());
      errors.push_back(ia.what());
    }
    catch (std::runtime_error& re) {
      WARN("setBGo, front_panel_enable: runtime error: {}", re.what());
      errors.push_back(re.what());
    }
  }

  if (args.contains("sync")) {
    try {
      auto sync = args.at("sync").get<bool>();
      ttcvi->setBGoSync(id, sync);
      INFO("B-Go {}: sync set to {}", id, sync);
    }
    catch (std::invalid_argument& ia) {
      WARN("setBGo, sync: invalid argument: {}", ia.what());
      errors.push_back(ia.what());
    }
    catch (std::runtime_error& re) {
      WARN("setBGo, sync: runtime error: {}", re.what());
      errors.push_back(re.what());
    }
  }

  if (args.contains("repetitive")) {
    try {
      auto repetitive = args.at("repetitive").get<bool>();
      ttcvi->setBGoRepetitive(id, repetitive);
      INFO("B-Go {}: repetitive set to {}", id, repetitive);
    }
    catch (std::invalid_argument& ia) {
      WARN("setBGo, repetitive: invalid argument: {}", ia.what());
      errors.push_back(ia.what());
    }
    catch (std::runtime_error& re) {
      WARN("setBGo, repetitive: runtime error: {}", re.what());
      errors.push_back(re.what());
    }
  }

  if (args.contains("transmit_on_fifo")) {
    try {
      auto transmitOnFifo = args.at("transmit_on_fifo").get<bool>();
      ttcvi->setBGoTransmitOnFIFO(id, transmitOnFifo);
      INFO("B-Go {}: transmit on FIFO set to {}", id, transmitOnFifo);
    }
    catch (std::invalid_argument& ia) {
      WARN("setBGo, transmit_on_fifo: invalid argument: {}", ia.what());
      errors.push_back(ia.what());
    }
    catch (std::runtime_error& re) {
      WARN("setBGo, transmit_on_fifo: runtime error: {}", re.what());
      errors.push_back(re.what());
    }
  }

  if (args.contains("inhibit_delay")) {
    try {
      auto delay = args.at("inhibit_delay").get<short>();
      ttcvi->setInhibitDelay(id, delay);
      INFO("B-Go {}: inhibit delay set to {}", id, delay);
    }
    catch (std::invalid_argument& ia) {
      WARN("setBGo, inhibit_delay: invalid argument: {}", ia.what());
      errors.push_back(ia.what());
    }
    catch (std::runtime_error& re) {
      WARN("setBGo, inhibit_delay: runtime error: {}", re.what());
      errors.push_back(re.what());
    }
  }

  if (args.contains("inhibit_duration")) {
    try {
      auto duration = args.at("inhibit_duration").get<short>();
      ttcvi->setInhibitDuration(id, duration);
      INFO("B-Go {}: inhibit duration set to {}", id, duration);
    }
    catch (std::invalid_argument& ia) {
      WARN("setBGo, inhibit_duration: invalid argument: {}", ia.what());
      errors.push_back(ia.what());
    }
    catch (std::runtime_error& re) {
      WARN("setBGo, inhibit_duration: runtime error: {}", re.what());
      errors.push_back(re.what());
    }
  }

  if (args.contains("reset_fifo")) {
    try {
      auto reset = args.at("reset_fifo").get<bool>();
      if (reset) {
        ttcvi->resetBGoFIFO(id);
        INFO("B-Go {}: FIFO reset.", id);
      }
    }
    catch (std::invalid_argument& ia) {
      WARN("setBGo, reset_fifo: invalid argument: {}", ia.what());
      errors.push_back(ia.what());
    }
    catch (std::runtime_error& re) {
      WARN("setBGo, reset_fifo: runtime error: {}", re.what());
      errors.push_back(re.what());
    }
  }

  if (args.contains("retransmit_fifo")) {
    try {
      auto retransmit = args.at("retransmit_fifo").get<bool>();
      ttcvi->setRetransmitBGoFIFO(id, retransmit);
      INFO("B-Go {}: retransmit FIFO set to {}", id, retransmit);
    }
    catch (std::invalid_argument& ia) {
      WARN("setBGo, retransmit_fifo: invalid argument: {}", ia.what());
      errors.push_back(ia.what());
    }
    catch (std::runtime_error& re) {
      WARN("setBGo, retransmit_fifo: runtime error: {}", re.what());
      errors.push_back(re.what());
    }
  }

  if (errors.size() == 0) {
    reply["response"] = "ok";
    reply["result"] = {};
  }
  else {
    reply["response"] = "err";
    reply["result"] = errors;
  }

  return reply;

}

json getBGo(const std::unique_ptr<ttcvi::TTCvi>& ttcvi, const json args) {
  json reply;
  json result;

  try {
    auto id = args.at("id").get<short>();
    result["front_panel_enable"] = ttcvi->getBGoFrontPanelEnable(id);
    result["sync"] = ttcvi->getBGoSync(id);
    result["repetitive"] = ttcvi->getBGoRepetitive(id);
    result["transmit_on_fifo"] = ttcvi->getBGoTransmitOnFIFO(id);
    result["inhibit_delay"] = ttcvi->getInhibitDelay(id);
    result["inhibit_duration"] = ttcvi->getInhibitDuration(id);
    result["fifo_empty"] = ttcvi->isBGoFIFOEmpty(id);
    result["fifo_full"] = ttcvi->isBGoFIFOFull(id);
    result["retransmit_fifo"] = ttcvi->getRetransmitBGoFIFO(id);

    reply["response"] = "ok";
    reply["result"] = result;
  }
  catch (nlohmann::detail::out_of_range& oor) {
    WARN("getBGo: argument id is required.");
    reply["response"] = "err";
    reply["result"] = "argument id is required";
  }
  catch (std::invalid_argument& ia) {
    WARN("getBGo: invalid argument: {}", ia.what());
    reply["response"] = "err";
    reply["result"] = ia.what();
  }
  catch (std::runtime_error& re) {
    WARN("getBGo: runtime error: {}", re.what());
    reply["response"] = "err";
    reply["result"] = re.what();
  }
  return reply;
} 
// front panel, sync, repetitive, transmit on fifo, inhibit delay, inhibit duration ,reset fifo, fifo status, set/get retransmit bgo fifo


}