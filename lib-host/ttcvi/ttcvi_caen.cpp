#include "ttcvi/ttcvi_caen.hpp"
#include "utils/logger.hpp"
#include "utils/delay.hpp"

#ifdef USE_CAENVMELIB

namespace ttcvi {

/**
 * Constructor for the TTCvi_CAEN class.
 * It requires to have a Wiener VM-USB module connected to the PC and accessible. It throws a runtime_error otherwise.
 * It also requires to have a TTCvi module on the bus, but it currently does not verify it.
 * 
 * @param addressModifier The VME address modifier. See the TTCvi constructor for more information.
 * @param baseAddress The TTCvi base address modifier. See the TTCvi constructor for more information.
 */
TTCvi_CAEN::TTCvi_CAEN(CVBoardTypes vmeBoardType, short addressModifier, long baseAddress)
  : TTCvi(addressModifier, baseAddress)
  , m_handle{0}
  , m_boardType{vmeBoardType}
  , m_connected{false}
{
  if (m_boardType != cvV1718 && m_boardType != cvV2718 && m_boardType != cvUSB_V3718_LOCAL) {
    THROW_RE("The software only supports CAEN modules V1718 and V2718");
  }
}

TTCvi_CAEN::~TTCvi_CAEN() {
  disconnect();
}

void TTCvi_CAEN::connect() {
  if (ready()) {
    THROW_RE("VM-USB already connected");
  }

  // this is set with trial and error, might not always work
  uint32_t link;
  const short conetNode{0};

  switch (m_boardType) {
  case cvV1718:
    link = 1;
    break;
  case cvV2718:
  case cvUSB_V3718_LOCAL:
  default:
    link = 0;
    break;
  }
  
  auto res = CAENVME_Init2(m_boardType, &link, conetNode, &m_handle);
  if (res != cvSuccess) {
    THROW_RE("Initialization of CAENVME failed: {}", CAENVME_DecodeError(res));
  }

  res = CAENVME_SystemReset(m_handle);
  if (res != cvSuccess) {
    THROW_RE("Reset of CAENVME failed: {}", CAENVME_DecodeError(res));
  }

  m_connected = true;

}

void TTCvi_CAEN::disconnect() {
  auto res = CAENVME_End(m_handle);
  if (res != cvSuccess) {
    ERROR("CAENVME_End() failed: {}", CAENVME_DecodeError(res));
  }
  m_connected = false;
}

} //namespace ttcvi

#endif
