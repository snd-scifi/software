#pragma once

#include "json.hpp"

#include "ttcvi/ttcvi.hpp"

using json = nlohmann::json;
using JsonObject = json::object_t;
using JsonCommandCallback_t = std::function<JsonObject(std::unique_ptr<ttcvi::TTCvi>&, JsonObject)>;
using JsonCommandMap = std::unordered_map<std::string, JsonCommandCallback_t>;

namespace ttcviCommands {

json getBcDelay(const std::unique_ptr<ttcvi::TTCvi>& ttcvi, const json args);

json getL1ATrigger(const std::unique_ptr<ttcvi::TTCvi>& ttcvi, const json args);
json setL1ATrigger(const std::unique_ptr<ttcvi::TTCvi>& ttcvi, const json args); // input and random rate, fifo status, reset fifo

json getOrbitInput(const std::unique_ptr<ttcvi::TTCvi>& ttcvi, const json args);
json setOrbitInput(const std::unique_ptr<ttcvi::TTCvi>& ttcvi, const json args);

json setEventCounter(const std::unique_ptr<ttcvi::TTCvi>& ttcvi, const json args); // source (?) and value
json getEventCounter(const std::unique_ptr<ttcvi::TTCvi>& ttcvi, const json args);

json generateSoftL1A(const std::unique_ptr<ttcvi::TTCvi>& ttcvi, const json args);
json generateSoftReset(const std::unique_ptr<ttcvi::TTCvi>& ttcvi, const json args);
json generateSoftBGo(const std::unique_ptr<ttcvi::TTCvi>& ttcvi, const json args);

json sendBChannelShort(const std::unique_ptr<ttcvi::TTCvi>& ttcvi, const json args);
json sendBChannelLong(const std::unique_ptr<ttcvi::TTCvi>& ttcvi, const json args);

json addBChannelDataShort(const std::unique_ptr<ttcvi::TTCvi>& ttcvi, const json args);
json addBChannelDataLong(const std::unique_ptr<ttcvi::TTCvi>& ttcvi, const json args);

json setBGo(const std::unique_ptr<ttcvi::TTCvi>& ttcvi, const json args);
json getBGo(const std::unique_ptr<ttcvi::TTCvi>& ttcvi, const json args); // front panel, sync, repetitive, transmit on fifo, inhibit delay, inhibit duration ,reset fifo, fifo status, set/get retransmit bgo fifo




} // namespace ttcviCommands

namespace ttcvi {

NLOHMANN_JSON_SERIALIZE_ENUM(L1ATriggerInput, {
  {L1A_0, "l1a-0"},
  {L1A_1, "l1a-1"},
  {L1A_2, "l1a-2"},
  {L1A_3, "l1a-3"},
  {VME_FUNC, "vme"},
  {RANDOM, "random"},
  {CALIBR, "calibration"},
  {DISABLE, "disable"}
})

NLOHMANN_JSON_SERIALIZE_ENUM(RandomTriggerRate, {
  {TR_1, "1"},
  {TR_100, "100"},
  {TR_1k, "1k"},
  {TR_5k, "5k"},
  {TR_10k, "10k"},
  {TR_25k, "25k"},
  {TR_50k, "50k"},
  {TR_100k, "100k"}
})

NLOHMANN_JSON_SERIALIZE_ENUM(OrbitInput, {
  {EXT, "external"},
  {INT, "internal"}
})

NLOHMANN_JSON_SERIALIZE_ENUM(TTCrxE, {
  {TTCrx, "ttcrx"},
  {EXTERNAL, "external"}
})

}