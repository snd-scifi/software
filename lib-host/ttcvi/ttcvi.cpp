#include <cstdlib>

#include "utils/logger.hpp"

#include "ttcvi.hpp"

namespace ttcvi {

/**
 * Constructor for the abstract TTCvi class.
 * It just sets the base address and the address modifier that will be used for communication.
 * 
 * @param addressModifier The VME address modifier. According to the manual, the TTCvi responds to the following values:\n
 * Standard: 0x39, 0x3A, 0x3D, 0x3E\n
 * Short: 0x29, 0x2D\n
 * Block: 0x0F, 0x0B, 0x3B, 0x3F\n
 * Extended: 0x09, 0x0A, 0x0D, 0x0E\n
 * This library has been tested with 0x29
 * 
 * @param baseAddress The TTCvi base address modifier. It is selected with four rotary switches inside the TTCvi.\n
 * SW1: VME Address bits <23..20>\n
 * SW2: VME Address bits <19..16>\n
 * SW3: VME Address bits <15..12>\n
 * SW4: VME Address bits <11..08>
 */
TTCvi::TTCvi(short addressModifier, long baseAddress) : m_addressModifier(addressModifier), m_baseAddress(baseAddress) {}

/**
 * Sets the source for L1A trigger signal.
 * 
 * @param input One of the elements of L1ATriggerInput
 */
void TTCvi::setL1ATriggerInput(L1ATriggerInput input) const {
  if (!ready()) {
    THROW_RE("TTCvi VME interface is not ready. Error in initialization?");
  }

  short res = writeRegisterMask(CSR1_OFFSET, CSR1_TRIGGER_SELECT_MASK, input);
  if (res < 0) {
    THROW_RE("error while reading CSR1 (0x80)");
  }
}

/**
 * Returns the current source for L1A trigger signal.
 */
L1ATriggerInput TTCvi::getL1ATriggerInput() const {
  if (!ready()) {
    THROW_RE("TTCvi VME interface is not ready. Error in initialization?");
  }

  long data = readRegisterMask(CSR1_OFFSET, CSR1_TRIGGER_SELECT_MASK);
  if (data < 0) {
    THROW_RE("error while reading CSR1 (0x80)");
  }
  return static_cast<L1ATriggerInput>(data);
}

/**
 * Sets the trigger rate when the L1A trigger is set to RANDOM.
 * 
 * @param rate One of the elements of RandomTriggerRate
 */
void TTCvi::setRandomTriggerRate(RandomTriggerRate rate) const {
  if (!ready()) {
    THROW_RE("TTCvi VME interface is not ready. Error in initialization?");
  }

  short res = writeRegisterMask(CSR1_OFFSET, CSR1_TRIGGER_RATE_MASK, rate);
  if (res < 0) {
    THROW_RE("error while accessing CSR1 (0x80)");
  }
}

/**
 * Returns the current rate for the random trigger.
 */
RandomTriggerRate TTCvi::getRandomTriggerRate() const {
  if (!ready()) {
    THROW_RE("TTCvi VME interface is not ready. Error in initialization?");
  }

  long data = readRegisterMask(CSR1_OFFSET, CSR1_TRIGGER_RATE_MASK);
  if (data < 0) {
    THROW_RE("error while reading CSR1 (0x80)");
  }
  return static_cast<RandomTriggerRate>(data);
}

/**
 * Selects the input for the orbit signal, that can either be external or internally generated.
 * 
 * @param input One of the elements of OrbitInput
 */
void TTCvi::setOrbitInput(OrbitInput input) const {
  if (!ready()) {
    THROW_RE("TTCvi VME interface is not ready. Error in initialization?");
  }

  short res = writeRegisterMask(CSR1_OFFSET, CSR1_ORBIT_INPUT_MASK, input);
  if (res < 0) {
    THROW_RE("error while accessing CSR1 (0x80)");
  }
}

/**
 * Returns the currently selected orbit input.
 */
OrbitInput TTCvi::getOrbitInput() const {
  if (!ready()) {
    THROW_RE("TTCvi VME interface is not ready. Error in initialization?");
  }

  long data = readRegisterMask(CSR1_OFFSET, CSR1_ORBIT_INPUT_MASK);
  if (data < 0) {
    THROW_RE("error while reading CSR1 (0x80)");
  }

  return static_cast<OrbitInput>(data);
}

/**
 * Selects the source for the internal 24-bit counter, accessible at the register at offsets 0x88 and 0x8A.
 * It can count either the L1A events or the orbit pulses.
 * WARNING: in the older version of the TTCvi module, the counter source is events and cannot be changed.
 * 
 * @param selection One of the elements of EventOrbitCounterSource
 */
void TTCvi::setEventOrbitCounterSource(EventOrbitCounterSource selection) const {
  if (!ready()) {
    THROW_RE("TTCvi VME interface is not ready. Error in initialization?");
  }

  short res = writeRegisterMask(CSR1_OFFSET, CSR1_EVENT_ORBIT_COUNT_MASK, selection);
  if (res < 0) {
    THROW_RE("error while accessing CSR1 (0x80)");
  }
}

/**
 * Returns the current source for the internal 24-bits counter.
 * WARNING: in the older version of the TTCvi module, the return value has no meaning.
 */
EventOrbitCounterSource TTCvi::getEventOrbitCounterSource() const {
  if (!ready()) {
    THROW_RE("TTCvi VME interface is not ready. Error in initialization?");
  }

  long data = readRegisterMask(CSR1_OFFSET, CSR1_EVENT_ORBIT_COUNT_MASK);
  if (data < 0) {
    THROW_RE("error while reading CSR1 (0x80)");
  }
  return static_cast<EventOrbitCounterSource>(data);
}

/**
 * Return the Bunch Crossing delay internally set in the TTCvi module in units of 2 ns.
 * This delay can be used to adjust the phase of the input clock to have the A and B channels delivered to the encoder with the correct phase window.
 */
int TTCvi::getBCDelay() const {
  if (!ready()) {
    THROW_RE("TTCvi VME interface is not ready. Error in initialization?");
  }

  long data = readRegisterMask(CSR1_OFFSET, CSR1_BC_DELAY_MASK);
  if (data < 0) {
    THROW_RE("error while reading CSR1 (0x80)");
  }
  return static_cast<int>(data);
}

/**
 * 
 */
bool TTCvi::isVMETransferPending() const {
  if (!ready()) {
    THROW_RE("TTCvi VME interface is not ready. Error in initialization?");
  }

  long data = readRegisterMask(CSR1_OFFSET, CSR1_VME_TRANSFER_PENDING_MASK);
  if (data < 0) {
    THROW_RE("error while reading CSR1 (0x80)");
  }
  return static_cast<bool>(data);
}

/**
 * Returns true if the L1A FIFO is empty.\n
 * TODO mention content of the fifo
 */
bool TTCvi::isL1AFIFOempty() const {
  if (!ready()) {
    THROW_RE("TTCvi VME interface is not ready. Error in initialization?");
  }

  long data = readRegisterMask(CSR1_OFFSET, CSR1_L1A_FIFO_EMPTY_MASK);
  if (data < 0) {
    THROW_RE("error while reading CSR1 (0x80)");
  }
  return static_cast<bool>(data);
}

/**
 * Returns true if the L1A FIFO is full.\n
 */
bool TTCvi::isL1AFIFOfull() const {
  if (!ready()) {
    THROW_RE("TTCvi VME interface is not ready. Error in initialization?");
  }

  long data = readRegisterMask(CSR1_OFFSET, CSR1_L1A_FIFO_FULL_MASK);
  if (data < 0) {
    THROW_RE("error while reading CSR1 (0x80)");
  }
  return static_cast<bool>(data);
}

/**
 * Resets the L1A FIFO.
 */
void TTCvi::resetL1AFIFO() const {
  if (!ready()) {
    THROW_RE("TTCvi VME interface is not ready. Error in initialization?");
  }

  short res = writeRegisterMask(CSR1_OFFSET, CSR1_L1A_FIFO_RESET_MASK, 1);
  if (res < 0) {
    THROW_RE("error while accessing CSR1 (0x80)");
  }
}

/**
 * Changes the internal event counter.
 */
void TTCvi::setEventOrbitCounter(int value) const {
  if (!ready()) {
    THROW_RE("TTCvi VME interface is not ready. Error in initialization?");
  }

  short res = vmeWrite16(EVENT_ORBIT_COUNTER_LSW_OFFSET, (value & 0xFFFF));
  if (res < 0) {
    THROW_RE("error while accessing LSW of event/orbit counter (0x8A)");
  }

  res = vmeWrite16(EVENT_ORBIT_COUNTER_MSW_OFFSET, ((value >> 16) & 0xFF));
  if (res < 0) {
    THROW_RE("error while accessing MSW of event/orbit counter (0x88)");
  }
}

/**
 * Returns the value of the internal event counter.
 */
int TTCvi::getEventOrbitCounter() const {
  if (!ready()) {
    THROW_RE("TTCvi VME interface is not ready. Error in initialization?");
  }

  long lsw, msw;
  short res = vmeRead16(EVENT_ORBIT_COUNTER_LSW_OFFSET, &lsw);

  if (res < 0) {
    THROW_RE("error while accessing LSW of event/orbit counter (0x8A)");
  }

  res = vmeRead16(EVENT_ORBIT_COUNTER_MSW_OFFSET, &msw);
  if (res < 0) {
    THROW_RE("error while accessing MSW of event/orbit counter (0x88)");
  }

  return static_cast<int>( (msw << 16) | lsw );
}

/**
 * Resets the B-Go FIFO corresponding to the id.
 */
void TTCvi::resetBGoFIFO(short id) const {
  if (!ready()) {
    THROW_RE("TTCvi VME interface is not ready. Error in initialization?");
  }

  if (id < 0 || id > 3) {
    THROW_IA("resetBGoFIFO: id must be 0, 1, 2, or 3");
  }

  short res = writeRegisterMask(CSR2_OFFSET, CSR2_RESET_MASK_BASE << id, 1);
  if (res < 0) {
    THROW_RE("error while accessing CSR2 (0x82)");
  }
}

/**
 * Selects what happens when the corresponding B-Go FIFO is empty.
 * 
 * @param id id of the B-Go (0 to 3)
 * @param retransmit if true, when the FIFO is empty, the read pointer is reset and the data in the FIFO is kept. If false the data in the FIFO is deleted.
 */
void TTCvi::setRetransmitBGoFIFO(short id, bool retransmit) const {
  if (!ready()) {
    THROW_RE("TTCvi VME interface is not ready. Error in initialization?");
  }

  if (id < 0 || id > 3) {
    THROW_IA("setRetransmitBGoFIFO: id must be 0, 1, 2, or 3");
  }

  // retransmit when 0
  short res = writeRegisterMask(CSR2_OFFSET, CSR2_RETRANSMIT_MASK_BASE << id, !retransmit);
  if (res < 0) {
    THROW_RE("error while accessing CSR2 (0x82)");
  }
}

/**
 * Returns wether the B-Go FIFO is set to retransmit the same data after it's emptied.
 * 
 * @param id id of the B-Go (0 to 3)
 */
bool TTCvi::getRetransmitBGoFIFO(short id) const {
  if (!ready()) {
    THROW_RE("TTCvi VME interface is not ready. Error in initialization?");
  }

  if (id < 0 || id > 3) {
    THROW_IA("getRetransmitBGoFIFO: id must be 0, 1, 2, or 3");
  }

  long data = readRegisterMask(CSR2_OFFSET, CSR2_RETRANSMIT_MASK_BASE << id);
  if (data < 0) {
    THROW_RE("error while accessing CSR2 (0x82)");
  }

  return !static_cast<bool>(data);
}

/**
 * 
 */
bool TTCvi::isBGoFIFOFull(short id) const {
  if (!ready()) {
    THROW_RE("TTCvi VME interface is not ready. Error in initialization?");
  }

  if (id < 0 || id > 3) {
    THROW_IA("isBGoFIFOFull: id must be 0, 1, 2, or 3");
  }

  long data = readRegisterMask(CSR2_OFFSET, CSR2_FULL_MASK_BASE << (id*2));
  if (data < 0) {
    THROW_RE("error while accessing CSR2 (0x82)");
  }

  return data;
}

/**
 * 
 */
bool TTCvi::isBGoFIFOEmpty(short id) const {
  if (!ready()) {
    THROW_RE("TTCvi VME interface is not ready. Error in initialization?");
  }

  if (id < 0 || id > 3) {
    THROW_IA("isBGoFIFOEmpty: id must be 0, 1, 2, or 3");
  }

  long data = readRegisterMask(CSR2_OFFSET, CSR2_EMPTY_MASK_BASE << (id*2));
  if (data < 0) {
    THROW_RE("error while accessing CSR2 (0x82)");
  }

  return data;
}


/**
 * Generates a software reset of the module.
 * This resets:
 * * the event counter,
 * * the inhibit delay and duration registers
 * * B-Go mode (i.e. front panel enable, sync, repetitive, send-on-fifo)
 * * ...
 */
void TTCvi::generateSoftReset() const {
  if (!ready()) {
    THROW_RE("TTCvi VME interface is not ready. Error in initialization?");
  }

  short res = vmeWrite16(SOFT_RST_GEN_OFFSET, 0x0);
  if (res < 0) {
    THROW_RE("error while accessing Software module reset register (0x84)");
  }
}

/**
 * Generate a software L1A trigger.
 * It requires the L1A trigger input to be set to VME_FUNC (0x04).
 */
void TTCvi::generateSoftL1A() const {
  if (!ready()) {
    THROW_RE("TTCvi VME interface is not ready. Error in initialization?");
  }

  short res = vmeWrite16(SOFT_L1A_GEN_OFFSET, 0x0);
  if (res < 0) {
    THROW_RE("error while accessing Software L1A generation register (0x86)");
  }
}

/**
 * 
 */
void TTCvi::setInhibitDelay(short id, short delay) const {
  if (!ready()) {
    THROW_RE("TTCvi VME interface is not ready. Error in initialization?");
  }

  if (id < 0 || id > 3) {
    THROW_IA("setInhibitDelay: id must be 0, 1, 2, or 3");
  }

  short res = vmeWrite16(INHIBIT_DELAY_BASE + id*B_GO_DELTA, delay);
  if (res < 0) {
    THROW_RE("error while accessing Inhibit Delay register");
  }
}

/**
 * 
 */
void TTCvi::setInhibitDuration(short id, short duration) const {
  if (!ready()) {
    THROW_RE("TTCvi VME interface is not ready. Error in initialization?");
  }

  if (id < 0 || id > 3) {
    THROW_IA("setInhibitDuration: id must be 0, 1, 2, or 3");
  }

  short res = vmeWrite16(INHIBIT_DURATION_BASE + id*B_GO_DELTA, duration);
  if (res < 0) {
    THROW_RE("error while accessing Inhibit Duration register");
  }
}

/**
 * 
 */
short TTCvi::getInhibitDelay(short id) const {
  if (!ready()) {
    THROW_RE("TTCvi VME interface is not ready. Error in initialization?");
  }

  if (id < 0 || id > 3) {
    THROW_IA("getInhibitDelay: id must be 0, 1, 2, or 3");
  }

  long data;
  short res = vmeRead16(INHIBIT_DELAY_BASE + id*B_GO_DELTA, &data);
  if (res < 0) {
    THROW_RE("error while accessing Inhibit Delay register");
  }

  return data & 0x0FFF;
}

/**
 * 
 */
short TTCvi::getInhibitDuration(short id) const {
  if (!ready()) {
    THROW_RE("TTCvi VME interface is not ready. Error in initialization?");
  }

  if (id < 0 || id > 3) {
    THROW_IA("getInhibitDuration: id must be 0, 1, 2, or 3");
  }

  long data;
  short res = vmeRead16(INHIBIT_DURATION_BASE + id*B_GO_DELTA, &data);
  if (res < 0) {
    THROW_RE("error while accessing Inhibit Duration register");
  }

  return data & 0x00FF;
}

/**
 * 
 */
void TTCvi::generateSoftBGo(short id) const {
  if (!ready()) {
    THROW_RE("TTCvi VME interface is not ready. Error in initialization?");
  }

  if (id < 0 || id > 3) {
    THROW_IA("generateSoftBGo: id must be 0, 1, 2, or 3");
  }

  short res = vmeWrite16(SOFT_B_GO_GEN_BASE + id*B_GO_DELTA, 0);
  if (res < 0) {
    THROW_RE("error while accessing Generate Software B-Go register");
  }
}

/**
 * 
 */
void TTCvi::setBGoFrontPanelEnable(uint8_t id, bool enable) const {
  if (!ready()) {
    THROW_RE("TTCvi VME interface is not ready. Error in initialization?");
  }

  if (id < 0 || id > 3) {
    THROW_IA("setBGoFrontPanelEnable: id must be 0, 1, 2, or 3");
  }

  short res = writeRegisterMask(B_GO_MODE_BASE + id*B_GO_DELTA, 0x01, !enable);
  if (res < 0) {
    THROW_RE("error while accessing B-Go Mode register");
  }
}

/**
 * 
 */
void TTCvi::setBGoSync(uint8_t id, bool sync) const {
  if (!ready()) {
    THROW_RE("TTCvi VME interface is not ready. Error in initialization?");
  }

  if (id < 0 || id > 3) {
    THROW_IA("setBGoSync: id must be 0, 1, 2, or 3");
  }

  short res = writeRegisterMask(B_GO_MODE_BASE + id*B_GO_DELTA, 0x02, !sync);
  if (res < 0) {
    THROW_RE("error while accessing B-Go Mode register");
  }
}

/**
 * 
 */
void TTCvi::setBGoRepetitive(uint8_t id, bool repetitive) const {
  if (!ready()) {
    THROW_RE("TTCvi VME interface is not ready. Error in initialization?");
  }

  if (id < 0 || id > 3) {
    THROW_IA("setBGoRepetitive: id must be 0, 1, 2, or 3");
  }

  short res = writeRegisterMask(B_GO_MODE_BASE + id*B_GO_DELTA, 0x04, repetitive);
  if (res < 0) {
    THROW_RE("error while accessing B-Go Mode register");
  }
}

/**
 * 
 */
void TTCvi::setBGoTransmitOnFIFO(uint8_t id, bool transmitIfNotEmpty) const {
  if (!ready()) {
    THROW_RE("TTCvi VME interface is not ready. Error in initialization?");
  }

  if (id < 0 || id > 3) {
    THROW_IA("setBGoTransmitOnFIFO: id must be 0, 1, 2, or 3");
  }

  short res = writeRegisterMask(B_GO_MODE_BASE + id*B_GO_DELTA, 0x08, !transmitIfNotEmpty);
  if (res < 0) {
    THROW_RE("error while accessing B-Go Mode register");
  }
}

/**
 * 
 */
bool TTCvi::getBGoFrontPanelEnable(uint8_t id) const {
  if (!ready()) {
    THROW_RE("TTCvi VME interface is not ready. Error in initialization?");
  }

  if (id < 0 || id > 3) {
    THROW_IA("getBGoFrontPanelEnable: id must be 0, 1, 2, or 3");
  }

  long data = readRegisterMask(B_GO_MODE_BASE + id*B_GO_DELTA, 0x01);
  if (data < 0) {
    THROW_RE("error while accessing B-Go Mode register");
  }

  return !static_cast<bool>(data);
}

/**
 * 
 */
bool TTCvi::getBGoSync(uint8_t id) const {
  if (!ready()) {
    THROW_RE("TTCvi VME interface is not ready. Error in initialization?");
  }

  if (id < 0 || id > 3) {
    THROW_IA("getBGoSync: id must be 0, 1, 2, or 3");
  }

  long data = readRegisterMask(B_GO_MODE_BASE + id*B_GO_DELTA, 0x02);
  if (data < 0) {
    THROW_RE("error while accessing B-Go Mode register");
  }

  return !static_cast<bool>(data);
}

/**
 * 
 */
bool TTCvi::getBGoRepetitive(uint8_t id) const {
  if (!ready()) {
    THROW_RE("TTCvi VME interface is not ready. Error in initialization?");
  }

  if (id < 0 || id > 3) {
    THROW_IA("getBGoRepetitive: id must be 0, 1, 2, or 3");
  }

  long data = readRegisterMask(B_GO_MODE_BASE + id*B_GO_DELTA, 0x04);
  if (data < 0) {
    THROW_RE("error while accessing B-Go Mode register");
  }

  return static_cast<bool>(data);
}

/**
 * 
 */
bool TTCvi::getBGoTransmitOnFIFO(uint8_t id) const {
  if (!ready()) {
    THROW_RE("TTCvi VME interface is not ready. Error in initialization?");
  }

  if (id < 0 || id > 3) {
    THROW_IA("getBGoTransmitOnFIFO: id must be 0, 1, 2, or 3");
  }

  long data = readRegisterMask(B_GO_MODE_BASE + id*B_GO_DELTA, 0x08);
  if (data < 0) {
    THROW_RE("error while accessing B-Go Mode register");
  }

  return !static_cast<bool>(data);
}

/**
 * 
 */
void TTCvi::addBChannelDataShort(short id, uint8_t data) const {
  if (!ready()) {
    THROW_RE("TTCvi VME interface is not ready. Error in initialization?");
  }

  if (id < 0 || id > 3) {
    THROW_IA("addBChannelDataShort: id must be 0, 1, 2, or 3");
  }

  if (isBGoFIFOFull(id)) {
    THROW_RE("B-Go FIFO full");
  }

  short res = vmeWrite32(BCH_DATA_B_GO_BASE + id*BCH_DATA_B_GO_DELTA, data << 23);
  if (res < 0) {
    THROW_RE("error while accessing B Channel Data register");
  }
}

/**
 * 
 */
void TTCvi::addBChannelDataLong(uint8_t id, uint16_t ttcrxAddress, TTCrxE e, uint8_t subaddress, uint8_t command) const {
  if (!ready()) {
    THROW_RE("TTCvi VME interface is not ready. Error in initialization?");
  }

  if (id < 0 || id > 3) {
    THROW_IA("addBChannelDataLong: id must be 0, 1, 2, or 3");
  }

  if (isBGoFIFOFull(id)) {
    THROW_RE("B-Go FIFO full");
  }

  // make sure the TTCrx address is 14-bit long
  ttcrxAddress &= 0x3FFF;
  short res = vmeWrite32(BCH_DATA_B_GO_BASE + id*BCH_DATA_B_GO_DELTA, 0x8000 | (ttcrxAddress << 17) | (e << 16) | (subaddress << 8) | command);
  if (res < 0) {
    THROW_RE("error while accessing Long B Channel Command register (0xC0)");
  }
}

/**
 * 
 */
void TTCvi::sendBChannelShort(uint8_t command) const {
  if (!ready()) {
    THROW_RE("TTCvi VME interface is not ready. Error in initialization?");
  }

  short res = vmeWrite16(SHORT_B_COMMAND_OFFSET, command);
  if (res < 0) {
    THROW_RE("error while accessing Short B Channel Command register (0xC4)");
  }
}

/**
 * 
 */
void TTCvi::sendBChannelLong(uint16_t ttcrxAddress, TTCrxE e, uint8_t subaddress, uint8_t command) const {
  if (!ready()) {
    THROW_RE("TTCvi VME interface is not ready. Error in initialization?");
  }

  // make sure the TTCrx address is 14-bit long
  ttcrxAddress &= 0x3FFF;
  short res = vmeWrite32(LONG_B_COMMAND_OFFSET_MSW, 0x8000 | (ttcrxAddress << 17) | (e << 16) | (subaddress << 8) | command);
  if (res < 0) {
    THROW_RE("error while accessing Long B Channel Command register (0xC0)");
  }
}

/**
 * 
 */
void TTCvi::sendBChannelLong(uint8_t subaddress, uint8_t command) const {
  if (!ready()) {
    THROW_RE("TTCvi VME interface is not ready. Error in initialization?");
  }

  short res = vmeWrite16(LONG_B_COMMAND_OFFSET_LSW, (subaddress << 8) | command);
  if (res < 0) {
    THROW_RE("error while accessing Long B Channel Command register (0xC2)");
  }
}


/**
 * Reads the bits corresponding to the mask of the 16-bit register determined by offset.
 * 
 * @param offset offset of the register.
 * @param mask the value of the bits set to 1 will be returned. For example, if mask is 0x00F0, the value of bits 4 to 7 will be aligned right and returned.
 */
long TTCvi::readRegisterMask(const long offset, const long mask) const {
  long data;
  short res = vmeRead16(offset, &data);

  if (res < 0) {
    return res;
  }
  else {
    return (data & mask) >> maskToShift(mask);
  }
}

/**
 * 
 */
short TTCvi::writeRegisterMask(const long offset, const long mask, const short value) const {
  long data;
  short res;
  // read the current value of the register
  res = vmeRead16(offset, &data);

  if (res < 0) {
    return res;
  }

  // set to 0 the bits to be modified, leave the others unchanged
  data &= ~mask;

  // clear write-only bits, that are read as 1
  switch (offset)
  {
  case CSR1_OFFSET:
    data &= ~CSR1_WRITE_ONLY_BITS;
    break;
  case CSR2_OFFSET:
    data &= ~CSR2_WRITE_ONLY_BITS;
    break;
  default:
    break;
  }

  // write the bits to be modified
  data |= (value << maskToShift(mask)) & mask;

  // write the new register value
  res = vmeWrite16(offset, data);

  if (res < 0) {
    return res;
  }

  return 0;
}

/**
 * Given a bit mask, returns its shift to the left (that is, the number of zeros on the right).
 * For example, if \p mask is 0x0700, 8 is returned.
 * 
 * @param mask the bit mask.
 */
int TTCvi::maskToShift(long mask) const {
  int c = 64; // c will be the number of zero bits on the right
  mask &= -mask;
  if (mask) c--;
  if (mask & 0x00000000FFFFFFFF) c -= 32;
  if (mask & 0x0000FFFF0000FFFF) c -= 16;
  if (mask & 0x00FF00FF00FF00FF) c -= 8;
  if (mask & 0x0F0F0F0F0F0F0F0F) c -= 4;
  if (mask & 0x3333333333333333) c -= 2;
  if (mask & 0x5555555555555555) c -= 1;

  return c;
}

}