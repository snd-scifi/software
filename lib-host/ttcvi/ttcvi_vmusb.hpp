#pragma once

#ifdef USE_VMUSB

#include "ttcvi/ttcvi.hpp"

#include "libxxusb.h"

namespace ttcvi {

class TTCvi_VMUSB : public TTCvi {
public:
  TTCvi_VMUSB(short addressModifier, long baseAddress);
  ~TTCvi_VMUSB();
  void connect();
  void disconnect();
  bool ready() const { return m_hdev; }

private:
  usb_dev_handle* m_hdev;

public:
  inline short vmeRead16(const long offset, long* data) const {
    return VME_read_16(m_hdev, m_addressModifier, m_baseAddress+offset, data);
  }

  inline short vmeWrite16(const long offset, const long data) const {
    return VME_write_16(m_hdev, m_addressModifier, m_baseAddress+offset, data);
  }

  inline short vmeRead32(const long offset, long* data) const {
    return VME_read_32(m_hdev, m_addressModifier, m_baseAddress+offset, data);
  }

  inline short vmeWrite32(const long offset, const long data) const {
    return VME_write_32(m_hdev, m_addressModifier, m_baseAddress+offset, data);
  }
};

} //namespace ttcvi

#endif
