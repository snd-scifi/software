#include <unordered_map>
#include <sstream>

#include "utils/logger.hpp"

#include "ttcvi_registers.hpp"

namespace ttcvi {

std::ostream& operator<<(std::ostream& out, const L1ATriggerInput value) {
  switch (value) {
  case L1A_0:
    return out << "l1a-0";
    break;
  case L1A_1:
    return out << "l1a-1";
    break;
  case L1A_2:
    return out << "l1a-2";
    break;
  case L1A_3:
    return out << "l1a-3";
    break;
  case VME_FUNC:
    return out << "vme";
    break;
  case RANDOM:
    return out << "random";
    break;
  case CALIBR:
    return out << "calibration";
    break;
  case DISABLE:
    return out << "disable";
    break;
  default:
    return out << "***UNKNOWN***";
    break;
  }
}

L1ATriggerInput l1aTriggerInputFromString(const std::string input) {
  static const std::unordered_map<std::string, L1ATriggerInput> table = {
    {"l1a-0", L1A_0},
    {"l1a-1", L1A_1},
    {"l1a-2", L1A_2},
    {"l1a-3", L1A_3},
    {"vme", VME_FUNC},
    {"random", RANDOM},
    {"calibration", CALIBR},
    {"disable", DISABLE}
  };
  auto it = table.find(input);
  if (it != table.end()) {
    return it->second;
  }
  else {
    THROW_IA("Unknown L1A trigger input: " + input);
  }

}

std::string l1aTriggerInputToString(const L1ATriggerInput input) {
  return fmt::format("{}", input);
}

std::ostream& operator<<(std::ostream& out, const RandomTriggerRate value) {
  switch (value) {
  case TR_1:
    return out << "1";
    break;
  case TR_100:
    return out << "100";
    break;
  case TR_1k:
    return out << "1k";
    break;
  case TR_5k:
    return out << "5k";
    break;
  case TR_10k:
    return out << "10k";
    break;
  case TR_25k:
    return out << "25k";
    break;
  case TR_50k:
    return out << "50k";
    break;
  case TR_100k:
    return out << "100k";
    break;
  default:
    return out << "***UNKNOWN***";
    break;
  }
}

RandomTriggerRate randomTriggerRateFromString(const std::string rate) {
  static const std::unordered_map<std::string, RandomTriggerRate> table = {
    {"1", TR_1},
    {"100", TR_100},
    {"1k", TR_1k},
    {"5k", TR_5k},
    {"10k", TR_10k},
    {"25k", TR_25k},
    {"50k", TR_50k},
    {"100k", TR_100k}
  };
  auto it = table.find(rate);
  if (it != table.end()) {
    return it->second;
  }
  else {
    THROW_IA("Unknown L1A random trigger rate: " + rate);
  }

}

std::string randomTriggerRateToString(const RandomTriggerRate rate) {
  return fmt::format("{}", rate);
}

std::ostream& operator<<(std::ostream& out, const OrbitInput value) {
  switch (value) {
  case EXT:
    return out << "external";
    break;
  case INT:
    return out << "internal";
    break;
  default:
    return out << "***UNKNOWN***";
    break;
  }
}

OrbitInput orbitInputFromString(const std::string input) {
  static const std::unordered_map<std::string, OrbitInput> table = {
    {"external", EXT},
    {"internal", INT},
  };
  auto it = table.find(input);
  if (it != table.end()) {
    return it->second;
  }
  else {
    THROW_IA("Unknown Orbit input: " + input);
  }
}

std::string orbitInputToString(const OrbitInput input) {
  return fmt::format("{}", input);
}

std::ostream& operator<<(std::ostream& out, const EventOrbitCounterSource value) {
  switch (value) {
  case EVENT_CNT:
    return out << "event";
    break;
  case ORBIT_CNT:
    return out << "orbit";
    break;
  default:
    return out << "***UNKNOWN***";
    break;
  }
}

std::ostream& operator<<(std::ostream& out, const TTCrxE e) {
  switch (e) {
  case TTCrx:
    return out << "ttcrx";
    break;
  case EXTERNAL:
    return out << "external";
    break;
  default:
    return out << "***UNKNOWN***";
    break;
  }
}

TTCrxE ttcrxeFromString(const std::string e) {
  static const std::unordered_map<std::string, TTCrxE> table = {
    {"ttcrx", TTCrx},
    {"external", EXTERNAL},
  };
  auto it = table.find(e);
  if (it != table.end()) {
    return it->second;
  }
  else {
    THROW_IA("Unknown TTCrxE: '" + e + "'. Valid values are: ttcrx, external");
  }
}

std::string ttcrxeToString(const TTCrxE e) {
  return fmt::format("{}", e);
}

}