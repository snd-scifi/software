#include "ttcvi/ttcvi_vmusb.hpp"
#include "utils/logger.hpp"
#include "utils/delay.hpp"

#ifdef USE_VMUSB

namespace ttcvi {

/**
 * Constructor for the TTCvi_VMUSB class.
 * It requires to have a Wiener VM-USB module connected to the PC and accessible. It throws a runtime_error otherwise.
 * It also requires to have a TTCvi module on the bus, but it currently does not verify it.
 * 
 * @param addressModifier The VME address modifier. See the TTCvi constructor for more information.
 * @param baseAddress The TTCvi base address modifier. See the TTCvi constructor for more information.
 */
TTCvi_VMUSB::TTCvi_VMUSB(short addressModifier, long baseAddress) : TTCvi(addressModifier, baseAddress), m_hdev(nullptr) {}

TTCvi_VMUSB::~TTCvi_VMUSB() {
  disconnect();
}

void TTCvi_VMUSB::connect() {
  if (ready()) {
    THROW_RE("VM-USB already connected");
  }
  xxusb_device_type dev[10];

  int ndevs = xxusb_devices_find(dev);
  if (ndevs < 1) {
    THROW_RE("No VM-USB devices found.");
  }
  else if (ndevs > 1) {
    WARN("Multiple VM-USB devices found. Opening the first one.");
  }

  // TODO here it opens the first it finds, maybe give choiche if more than one is connected
  m_hdev = xxusb_device_open(dev->usbdev);
  delayMilliseconds(100);

  if (reinterpret_cast<long>(m_hdev) < 0) {
    THROW_RE("Could not open the VM-USB device.");
  }

  // this seems to prevent failures in writing to the TTCvi later...
  for (int i : {0, 1, 2, 3}) {
    auto res = VME_LED_settings(m_hdev, i, i+4, 0, 0);
    if (res < 0) {
      THROW_RE("Failed to set LED {} settings", i);
    }
  }
}

void TTCvi_VMUSB::disconnect() {
  if (m_hdev) {
    xxusb_device_close(m_hdev);
    m_hdev = nullptr;
  }
}

} //namespace ttcvi

#endif
