#pragma once

#include <cstdint>

#include "ttcvi_registers.hpp"

namespace ttcvi {

class TTCvi {
public:
  virtual void connect() = 0;
  virtual void disconnect() = 0;

  void setL1ATriggerInput(L1ATriggerInput input) const;
  L1ATriggerInput getL1ATriggerInput() const;

  void setRandomTriggerRate(RandomTriggerRate rate) const;
  RandomTriggerRate getRandomTriggerRate() const;

  void setOrbitInput(OrbitInput input) const;
  OrbitInput getOrbitInput() const;

  void setEventOrbitCounterSource(EventOrbitCounterSource selection) const;
  EventOrbitCounterSource getEventOrbitCounterSource() const;

  void setEventOrbitCounter(int value) const;
  int getEventOrbitCounter() const;

  int getBCDelay() const;

  bool isVMETransferPending() const;
  bool isL1AFIFOempty() const;
  bool isL1AFIFOfull() const;
  void resetL1AFIFO() const;

  void resetBGoFIFO(short id) const; // CSR2
  void setRetransmitBGoFIFO(short id, bool retransmit) const;
  bool getRetransmitBGoFIFO(short id) const;
  bool isBGoFIFOFull(short id) const;
  bool isBGoFIFOEmpty(short id) const;

  void generateSoftReset() const;
  void generateSoftL1A() const;
  //void generateSoftCounterReset() const;

  void setInhibitDelay(short id, short delay) const;
  void setInhibitDuration(short id, short duration) const;

  short getInhibitDelay(short id) const;
  short getInhibitDuration(short id) const;

  void setBGoFrontPanelEnable(uint8_t id, bool enable) const;
  void setBGoSync(uint8_t id, bool sync) const;
  void setBGoRepetitive(uint8_t id, bool repetitive) const;
  void setBGoTransmitOnFIFO(uint8_t id, bool transmitIfNotEmpty) const;

  bool getBGoFrontPanelEnable(uint8_t id) const;
  bool getBGoSync(uint8_t id) const;
  bool getBGoRepetitive(uint8_t id) const;
  bool getBGoTransmitOnFIFO(uint8_t id) const;

  void generateSoftBGo(short id) const;

  void addBChannelDataShort(short id, uint8_t data) const;
  void addBChannelDataLong(uint8_t id, uint16_t ttcrxAddress, TTCrxE e, uint8_t subaddress, uint8_t command) const;

  void sendBChannelShort(uint8_t command) const;
  void sendBChannelLong(uint16_t ttcrxAddress, TTCrxE e, uint8_t subaddress, uint8_t command) const;
  void sendBChannelLong(uint8_t subaddress, uint8_t command) const;

  long readRegisterMask(const long offset, const long mask) const;
  short writeRegisterMask(const long offset, const long mask, const short value) const;

  inline virtual short vmeRead16(const long offset, long* data) const = 0;
  inline virtual short vmeWrite16(const long offset, const long data) const  = 0;
  inline virtual short vmeRead32(const long offset, long* data) const = 0;
  inline virtual short vmeWrite32(const long offset, const long data) const = 0;

protected:
  TTCvi(short addressModifier, long baseAddress);
  TTCvi() {}
  virtual bool ready() const = 0;
  short m_addressModifier;
  long m_baseAddress;
  int maskToShift(long mask) const;

};

} //namespace ttcvi
