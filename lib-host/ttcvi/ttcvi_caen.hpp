#pragma once

#ifdef USE_CAENVMELIB

#include "ttcvi/ttcvi.hpp"

#include "CAENVMElib.h"

namespace ttcvi {

class TTCvi_CAEN : public TTCvi {
public:
  TTCvi_CAEN(CVBoardTypes vmeBoardType, short addressModifier, long baseAddress);
  ~TTCvi_CAEN();
  void connect();
  void disconnect();
  bool ready() const { return m_connected; }

private:
  int32_t m_handle;
  CVBoardTypes  m_boardType;
  bool m_connected;

public:
  inline short vmeRead16(const long offset, long* data) const {
    return CAENVME_ReadCycle(m_handle, m_baseAddress+offset, data, static_cast<CVAddressModifier>(m_addressModifier), cvD16);
  }

  inline short vmeWrite16(const long offset, const long data) const {
    auto localData = data;
    return CAENVME_WriteCycle(m_handle, m_baseAddress+offset, &localData, static_cast<CVAddressModifier>(m_addressModifier), cvD16);
  }

  inline short vmeRead32(const long offset, long* data) const {
    return CAENVME_ReadCycle(m_handle, m_baseAddress+offset, data, static_cast<CVAddressModifier>(m_addressModifier), cvD32);
  }

  inline short vmeWrite32(const long offset, const long data) const {
    auto localData = data;
    return CAENVME_WriteCycle(m_handle, m_baseAddress+offset, &localData, static_cast<CVAddressModifier>(m_addressModifier), cvD32);
  }
};

} //namespace ttcvi

#endif
