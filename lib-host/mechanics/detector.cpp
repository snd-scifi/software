#include "utils/logger.hpp"

#include "detector.hpp"

Detector::Detector(nlohmann::json boardMap) {

  for (const auto& subsystemMapping : boardMap.items()) {
    const auto subsystem = subsystemMapping.key();
    for (const auto& planeMapping : subsystemMapping.value().items()) {
      const auto planeName = planeMapping.key();
      const auto& planeMap = planeMapping.value();

      const auto planeClass = planeMap.at("class").get<std::string>();
      const auto planeType = planeMap.at("type").get<PlaneType>();

      if (!m_planeMap.try_emplace(subsystem + planeName, planeType, planeName).second) {
        THROW_RE("Plane '{}' already exists.", planeName);
      }

      if (planeType == PlaneType::Invalid) {
        THROW_RE("Invalid plane type: {}", planeMap.at("type").get<std::string>());
      }

      if (planeClass == "multiboard" && (planeType == PlaneType::SndScifi || planeType == PlaneType::SciFiModule)) {
        const auto boardIds = planeMap.at("boards").get<std::vector<uint32_t>>();
        INFO("{} {} - {}", planeType, planeName, boardIds);
        for (const auto boardId : boardIds) {
          for (uint8_t slot = 0; slot < 4; slot++) {
            m_boardToPlaneMap.try_emplace(std::make_pair(boardId, slot), subsystem + planeName);
          }
        }
      }
      else if (planeClass == "multislot") {
        const auto boardId = planeMap.at("board").get<uint32_t>();
        const auto slots = planeMap.at("slots").get<std::vector<std::string>>();
        INFO("{} {} - {} {}", planeType, planeName, boardId, slots);

        for (const auto& slotLetter : slots) {
          m_boardToPlaneMap.try_emplace(std::make_pair(boardId, slotLetterToNumber(slotLetter)), subsystem + planeName);
        }
        
      }
    }
  }
}

void Detector::addHit(const TofpetHit& hit) {
  const auto planeName = m_boardToPlaneMap.at(std::make_pair(hit.boardId(), hit.tofpetId() >> 1));
  m_planeMap.at(planeName).increaseNhits();
}

// unsigned long Detector::nHitsPerPlaneType(PlaneType type) const {
//   unsigned long hitCounter{0};

//   for(const auto& [planeName, plane] : m_planeMap) {
//     if (plane.type() == type) {
//       hitCounter += plane.nHits();
//     }
//   }
//   return hitCounter;
// }

unsigned long Detector::nMaxHitsPerPlaneType(PlaneType type) const {
  unsigned long maxHitCounter{0};

  for(const auto& [planeName, plane] : m_planeMap) {
    if (plane.type() == type) {
      maxHitCounter = std::max(maxHitCounter, plane.nHits());
    }
  }
  return maxHitCounter;
}

unsigned long Detector::nHitsTotal() const {
  unsigned long hitCounter{0};

  for(const auto& [planeName, plane] : m_planeMap) {
    hitCounter += plane.nHits();
  }
  return hitCounter;
}

unsigned long Detector::nPlanesWithHits(PlaneType type) const {
  unsigned long planeCounter{0};

  for(const auto& [planeName, plane] : m_planeMap) {
    if (plane.type() == type && plane.nHits() > 0) {
      planeCounter++;
    }
  }
  return planeCounter;
}

unsigned long Detector::nPlanesWithHits() const {
  unsigned long planeCounter{0};

  for(const auto& [planeName, plane] : m_planeMap) {
    if (plane.nHits() > 0) {
      planeCounter++;
    }
  }
  return planeCounter;
}

uint8_t Detector::slotLetterToNumber(std::string slot) {
  if (slot == "A") return 0;
  else if (slot == "B") return 1;
  else if (slot == "C") return 2;
  else if (slot == "D") return 3;
  else THROW_RE("Invalid slot '{}", slot);
}