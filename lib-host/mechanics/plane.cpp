#include "plane.hpp"

#include "utils/logger.hpp"

Plane::Plane(PlaneType type, std::string name)
  : m_type{type}
  , m_name{name}
  , m_nHits{0} 
  {}

PlaneTypeMap generatePlaneTypeMap(const nlohmann::json boardMapping) {
  PlaneTypeMap retMap;
  for (const auto& subsystemMapping : boardMapping.items()) {
    const auto subsystem = subsystemMapping.key();
    for (const auto& planeMapping : subsystemMapping.value().items()) {
      const auto plane = planeMapping.key();
      const auto& planeMap = planeMapping.value();
      const auto planeClass = planeMap.at("class").get<std::string>();
      const auto planeType = planeMap.at("type").get<PlaneType>();

      if (planeType == PlaneType::Invalid) {
        THROW_RE("Invalid plane type: {}", planeMap.at("type").get<std::string>());
      }

      if (planeClass == "multiboard") {
        const auto boardIds = planeMap.at("boards").get<std::vector<uint32_t>>();
        // INFO("snd_scifi {} - {}", plane, boardIds);
        for (const auto boardId : boardIds) {
          retMap.try_emplace(boardId, planeType);
        }
      }
      else if (planeClass == "multislot") {
        const auto boardId = planeMap.at("board").get<uint32_t>();
        const auto slots = planeMap.at("slots").get<std::vector<std::string>>();
        // NOTICE("{} {} - {} {}", planeType, plane, boardId, slots);
        retMap.try_emplace(boardId, planeType);
      }
    }
  }
  return retMap;
}


std::ostream& operator<<(std::ostream& out, const PlaneType type) {
  switch (type) {
  case PlaneType::Invalid:
    return out << "invalid";
  case PlaneType::SndVeto:
    return out << "snd_veto";
  case PlaneType::SndScifi:
    return out << "snd_scifi";
  case PlaneType::SndUs:
    return out << "snd_us";
  case PlaneType::SndDsH:
    return out << "snd_dsh";
  case PlaneType::SndDsV:
    return out << "snd_dsv";
  case PlaneType::SciFiModule:
    return out << "scifi_module";
  case PlaneType::LpheEcal:
    return out << "lphe_ecal";
  default:
    return out<< "unknown_plane_type";
  }
}