#pragma once

#include <map>

#include "mechanics/plane.hpp"
#include "storage/tofpet_hit.hpp"

#include "json.hpp"

class Detector {
public:
  Detector(nlohmann::json boardMap);

  void addHit(const TofpetHit& hit);

  unsigned long nHits(std::string planeName) const { return m_planeMap.at(planeName).nHits(); }
  // unsigned long nHitsPerPlaneType(PlaneType type) const;
  unsigned long nMaxHitsPerPlaneType(PlaneType type) const;
  unsigned long nHitsTotal() const;
  unsigned long nPlanesWithHits(PlaneType type) const;
  unsigned long nPlanesWithHits() const;


private:
  uint8_t slotLetterToNumber(std::string slot);
  // map from (boardId, slot) to the name of the relevant plane. Slot is 0 to 3, corresponding to A to D and can be calculated as tofpetId >> 1
  std::map<std::pair<uint32_t, uint8_t>, std::string> m_boardToPlaneMap; // TODO check if it can be compressed into one
  // map from the plane name to the actual plane object
  std::map<std::string, Plane> m_planeMap; 
};