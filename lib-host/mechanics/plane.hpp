#pragma once

#include <map>
#include <string>

#include "json.hpp"

enum class PlaneType {
  Invalid,
  SndScifi,
  SndVeto,
  SndUs,
  SndDsH,
  SndDsV,
  SciFiModule,
  LpheEcal,
};

using PlaneTypeMap = std::map<uint32_t, PlaneType>;
PlaneTypeMap generatePlaneTypeMap(const nlohmann::json boardMapping);

class Plane {
public:
  Plane(PlaneType type, std::string name);
  PlaneType type() const { return m_type; };
  std::string name() const { return m_name; };

  void increaseNhits(unsigned long n = 1) { m_nHits += n; }
  unsigned long nHits() const { return m_nHits; }
  void clear() { m_nHits = 0; }

private:
  PlaneType m_type;
  std::string m_name;
  unsigned long m_nHits;
};


std::ostream& operator<<(std::ostream& out, const PlaneType type);


NLOHMANN_JSON_SERIALIZE_ENUM(PlaneType, {
    {PlaneType::Invalid, "invalid"},
    {PlaneType::SndScifi, "snd_scifi"},
    {PlaneType::SndVeto, "snd_veto"},
    {PlaneType::SndUs, "snd_us"},
    {PlaneType::SndDsH, "snd_dsh"},
    {PlaneType::SndDsV, "snd_dsv"},
    {PlaneType::SciFiModule, "scifi_module"},
    {PlaneType::LpheEcal, "lphe_ecal"},
})

