
#include <iostream>
#include <cstdlib>
#include <cerrno>
#include <cstring>

#include <unistd.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>

#include <arpa/inet.h>

#include "utils/logger.hpp"

#include "data_server.hpp"

/**
 * The constructor sets the port and the maximum number of connections that will be accepted by the server and starts it.
 * 
 * \param serverPort String containing the port of the server.
 * \param acceptMax Maximum number of connection that will be accepted by the server.
 */
DataServer::DataServer(std::string serverPort, size_t acceptMax) : m_server("DataServer", serverPort, acceptMax) {
  m_server.start();
}

DataServer::~DataServer() {
  m_server.stop();
}

/**
 * Accepts a connection and returns the correpondig thread.
 * 
 * This function block on acceptConnection(...) waiting for new connections from Data Clients.
 * Once a new connection is established, a new thread is created and returned.
 * This thread will receive the data and append them on the DataPacketQueue object passed to the function.
 * 
 * \param dataPacketQueue The DataPacketQueue object the received data packets will be appended to.
 */
std::thread DataServer::acceptConnection(DataPacketQueue& dataPacketQueue, DaqServerManager& serverManager) {
  auto connection = m_server.acceptConnection();
  return std::thread(connectionThread, connection, std::ref(dataPacketQueue), std::ref(serverManager));
}

/**
 * Function used to create the connection thread.
 * 
 * It is an endless loop that receives data packets from the connected Data Client.
 * Will stop with a closed connection or an END_COMMUNICATION packet.
 */
void DataServer::connectionThread(Connection connection, DataPacketQueue& dataPacketQueue, DaqServerManager& serverManager) {
  INFO("Received connection from {}.", connection.address());
  assert(serverManager.daqMonitor());

  std::vector<uint32_t> header;
  std::vector<uint32_t> data;

  uint32_t boardId = 0xFFFFFFFF;
  
  while(1) {
    try {
      header = connection.recv32(3);
    }
    catch (connection_closed& cc) {
      NOTICE("Connection closed.");
      connection.close();
      if (boardId != 0xFFFFFFFF) {
        serverManager.daqThreadStatus(boardId).connected = false;
      }
      return;
    }
    auto hd = header[0];
    auto length = header[1];
    auto id = header[2];

    switch (hd) {
    case BEGIN_COMMUNICATION:
      {
        try {
          // data = connection.recv32(length);
          // another connection with the same ID is already active
          boardId = id;
          if (!serverManager.validBoardId(boardId)) {
            ERROR("Board {} tried to connect, but its ID is not in the list. Refusing connection.", boardId);
            connection.replyErr();
            connection.close();
            return;
          }
          if (serverManager.daqThreadStatus(boardId).connected) {
            ERROR("Board {} tried to connect, but it is already connected. Refusing connection.", boardId);
            connection.replyErr();
            connection.close();
            return;
          }
        }
        catch (connection_closed& cc) {
          NOTICE("Connection closed.");
          connection.close();
          return;
        } //TODO remove this exception
        // catch (...)
        
        connection.replyOk();
        serverManager.daqThreadStatus(boardId).connected = true;
      }
      break;
    case DATA:
      {
        try {
          data = connection.recv32(length);
        }
        catch (connection_closed& cc) {
          NOTICE("Connection closed.");
          connection.close();
          serverManager.daqThreadStatus(boardId).connected = false;
          return;
        }

        std::deque<DataPacket> tmpQueue;
        serverManager.daqMonitor()->threadPacketQueueLength(boardId, tmpQueue.size());

        // for (size_t i = 0; i < length/4; i++) {
        //   tmpQueue.emplace_back(boardId, DataPacket::createPacket(data.data()+i*4));
        // }
        for (auto it = data.cbegin(); it != data.cend();) {
          auto p = DataPacket::createPacket(boardId, it, data.cend());
          // NOTICE(p.type());
          if (p.type() == DataPacket::Invalid) {
            WARN("Invalid packet: {:#x}", *it); //TODO in this case, the remaining fragment needs to be saved
            break;
          }
          tmpQueue.emplace_back(std::move(p));
          serverManager.daqMonitor()->threadPacketQueueLength(boardId, tmpQueue.size());
        }
        
        dataPacketQueue.putBlock(tmpQueue);
        serverManager.daqMonitor()->serverPacketQueueLength(dataPacketQueue.size());

        connection.sendHeader(HeaderType::DATA_OK, length, id);
      }
      break;
    case END_COMMUNICATION:
      {
        serverManager.daqThreadStatus(boardId).connected = false;
        connection.replyOk();
        connection.close();
        return;
      }
      break;
    default:
      WARN("Unknown header: {}", hd);
      break;
    }
    
  }

  connection.close();

}
