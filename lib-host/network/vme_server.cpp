#include "utils/logger.hpp"
#include "ttcvi/ttcvi_commands.hpp"
#include "ttcvi/ttcvi_vmusb.hpp"
#include "ttcvi/ttcvi_caen.hpp"

#include "vme_server.hpp"

VmeServer::VmeServer(std::string serverPort, size_t acceptMax, short addressModifier, long baseAddress, VmeInterface interface) 
  : m_server("VmeServer", serverPort, acceptMax)
{
  switch (interface) {
  case VmeInterface::VmUsb:
#ifdef USE_VMUSB
    m_ttcvi = std::make_unique<ttcvi::TTCvi_VMUSB>(addressModifier, baseAddress);
#else
    THROW_RE("Cannot instantiate TTCvi_VMUSB class. The software was compiled without the USE_VMUSB flag.");
#endif
    break;
  
  case VmeInterface::CaenV1718:
#ifdef USE_CAENVMELIB
    m_ttcvi = std::make_unique<ttcvi::TTCvi_CAEN>(cvV1718, addressModifier, baseAddress);
#else
    THROW_RE("Cannot instantiate TTCvi_CAEN class. The software was compiled without the USE_CAENVMELIB flag.");
#endif
    break;

  case VmeInterface::CaenV2718:
#ifdef USE_CAENVMELIB
    m_ttcvi = std::make_unique<ttcvi::TTCvi_CAEN>(cvV2718, addressModifier, baseAddress);
#else
    THROW_RE("Cannot instantiate TTCvi_CAEN class. The software was compiled without the USE_CAENVMELIB flag.");
#endif
    break;
  
  case VmeInterface::CaenV3718Usb:
#ifdef USE_CAENVMELIB
    m_ttcvi = std::make_unique<ttcvi::TTCvi_CAEN>(cvUSB_V3718_LOCAL, addressModifier, baseAddress);
#else
    THROW_RE("Cannot instantiate TTCvi_CAEN class. The software was compiled without the USE_CAENVMELIB flag.");
#endif
    break;
  
  default:
    THROW_RE("Unknown VME interface.");
    break;
  }

  using namespace ttcviCommands;
  registerJsonCommand("get_bc_delay", getBcDelay);
  registerJsonCommand("get_l1a_trigger", getL1ATrigger);
  registerJsonCommand("set_l1a_trigger", setL1ATrigger);
  registerJsonCommand("get_orbit_input", getOrbitInput);
  registerJsonCommand("set_orbit_input", setOrbitInput);
  registerJsonCommand("get_event_counter", getEventCounter);
  registerJsonCommand("set_event_counter", setEventCounter);
  registerJsonCommand("generate_soft_l1a", generateSoftL1A);
  registerJsonCommand("generate_soft_reset", generateSoftReset);
  registerJsonCommand("generate_soft_bgo", generateSoftBGo);
  registerJsonCommand("send_b_channel_short", sendBChannelShort);
  registerJsonCommand("send_b_channel_long", sendBChannelLong);
  registerJsonCommand("add_b_channel_data_short", addBChannelDataShort);
  registerJsonCommand("add_b_channel_data_long", addBChannelDataLong);
  registerJsonCommand("get_b_go", getBGo);
  registerJsonCommand("set_b_go", setBGo);
}

VmeServer::~VmeServer() {
  m_server.stop();
  m_ttcvi->disconnect();
}

void VmeServer::start() { 
  m_ttcvi->connect();
  m_server.start();
}

Connection VmeServer::acceptConnection() {
  return m_server.acceptConnection();
}

void VmeServer::connectionLoop(Connection connection) {
  Header header;
  std::string data;
  std::string command;
  
  while(1) {
    //TODO error checking
    try {
      header = connection.recvHeader();
    }
    catch (connection_closed& cc) {
      NOTICE("VmeServer: Connection from {} closed.", connection.address());
      return;
    }

    switch (header.type) {
    case BEGIN_COMMUNICATION: //TODO need to distinguish between a data connection and a control connection
      {
        try {
          data = connection.recvString(header.length);
        }
        catch (connection_closed& cc) {
          NOTICE("VmeServer: Connection from {} closed.", connection.address());
          return;
        }

        //TODO initialize
      }
    case COMMAND:
      {
        try {
          command = connection.recvString(header.length);
        }
        catch (connection_closed& cc) {
          NOTICE("VmeServer: Connection from {} closed.", connection.address());
          return;
        }
        auto response = processJsonCommand(command).dump();
        connection.sendHeader({HeaderType::COMMAND_REPLY, static_cast<uint32_t>(response.size()), header.id});
        connection.sendString(response);
        break;
      }
    case END_COMMUNICATION:
      {
        try {
          data = connection.recvString(header.length);
        }
        catch (connection_closed& cc) {
          NOTICE("VmeServer: Connection from {} closed.", connection.address());
          return;
        }
        return;
      }
    default:
      WARN("Unknown header {}", header.type);
      break;
    }
    
  }
}

json VmeServer::processJsonCommand(std::string command) {
  auto j = json::parse(command);
  std::string cmd = j.at("command");
  json args;
  try {
    args = j.at("args");
  }
  catch (nlohmann::detail::out_of_range& oor) {
    DEBUG("processJsonCommand: no arguments given");
    args["args"] = json::value_t::object;
  }

  try {
    return m_jsonCommands.at(cmd)(m_ttcvi, args);
  }
  catch (std::out_of_range& oor) {
    DEBUG("processJsonCommand: unknown command");
    json response;
    response["response"] = "err";
    response["result"] = "unknown command";
    return response;
  }
}

void VmeServer::registerJsonCommand(std::string command, JsonCommandCallback_t callback) {
  m_jsonCommands[command] = callback;
}

std::istream& operator>> (std::istream& is, VmeServer::VmeInterface& interface) {
  std::string tmp;
  is >> tmp;

  //make tmp lowercase
  std::transform(tmp.begin(), tmp.end(), tmp.begin(),
    [](unsigned char c){ return std::tolower(c); });
  
  std::unordered_map<std::string, VmeServer::VmeInterface> map{
    {"vm_usb", VmeServer::VmeInterface::VmUsb},
    {"caen_v1718", VmeServer::VmeInterface::CaenV1718},
    {"caen_v2718", VmeServer::VmeInterface::CaenV2718},
    {"caen_v3718_usb", VmeServer::VmeInterface::CaenV3718Usb},
  };

  auto it = map.find(tmp);
  if (it != map.end()) {
      interface = it->second;
  }
  else {
      THROW_IA("Unknown VME interface: {}", tmp);
  }

  return is;
}

std::ostream& operator<< (std::ostream& os, const VmeServer::VmeInterface& interface) {
  switch (interface) {
  case VmeServer::VmeInterface::VmUsb:
    return os << "vm_usb";
  case VmeServer::VmeInterface::CaenV1718:
    return os << "caen_v1718";
  case VmeServer::VmeInterface::CaenV2718:
    return os << "caen_v2718";
  case VmeServer::VmeInterface::CaenV3718Usb:
    return os << "caen_v3718_usb";
  default:
    return os << "*** UNKNOWN ***";
  }
}