#pragma once

#include <string>

#include "json.hpp"

#include "network/common_server.hpp"
#include "ttcvi/ttcvi.hpp"

using json = nlohmann::json;
using JsonObject = json::object_t;
using JsonCommandCallback_t = std::function<JsonObject(std::unique_ptr<ttcvi::TTCvi>&, JsonObject)>; //TODO use commandserver, but requires to template the type of the functions
using JsonCommandMap = std::unordered_map<std::string, JsonCommandCallback_t>;

class VmeServer {
public:
  enum class VmeInterface {
    VmUsb,
    CaenV1718,
    CaenV2718,
    CaenV3718Usb,
  };

  VmeServer(std::string serverPort, size_t acceptMax, short addressModifier, long baseAddress, VmeInterface interface);
  ~VmeServer();

  void start();

  Connection acceptConnection();
  void connectionLoop(Connection connection);
  void registerJsonCommand(std::string command, JsonCommandCallback_t callback);
  json processJsonCommand(std::string command);

private:
  CommonServer m_server;
  JsonCommandMap m_jsonCommands;

  std::unique_ptr<ttcvi::TTCvi> m_ttcvi;

};

std::istream& operator>> (std::istream& is, VmeServer::VmeInterface& interface);
std::ostream& operator<< (std::ostream& os, const VmeServer::VmeInterface& interface);