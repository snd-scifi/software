#include <filesystem>
#include <algorithm>
#include <fstream>
#include <set>

#include "utils/logger.hpp"

#include "daq_cmd_server.hpp"

DaqCmdServer::DaqCmdServer(std::string port, DaqServerManager& serverManager) : CommandServer(port, 1), m_serverManager(serverManager)  {
  registerJsonCommand("set_run_number", [this] (json args) { return this->setRunNumber(args); });
  registerJsonCommand("set_board_ids", [this] (json args) { return this->setBoardIds(args); });
  registerJsonCommand("set_daq_server_settings", [this] (json args) { return this->setDaqServerSettings(args); });
  registerJsonCommand("set_board_mapping", [this] (json args) { return this->setBoardMapping(args); });
  registerJsonCommand("set_system_configuration", [this] (json args) { return this->setSystemConfiguration(args); });
  registerJsonCommand("set_boards_configuration", [this] (json args) { return this->setBoardsConfiguration(args); });
  registerJsonCommand("set_user_info", [this] (json args) { return this->setUserInfo(args); });
  registerJsonCommand("update_user_info", [this] (json args) { return this->updateUserInfo(args); });
  registerJsonCommand("start_daq", [this] (json args) { return this->startDaq(args); });
  registerJsonCommand("stop_daq", [this] (json args) { return this->stopDaq(args); });
  registerJsonCommand("get_recorded_runs", [this] (json args) { return this->getRecordedRuns(args); });
  registerJsonCommand("get_status", [this] (json args) { return this->getStatus(args); });
  start();
}

std::thread DaqCmdServer::commandThread() {
  auto loop = [this] () {
    while (!m_serverManager.daqServerStopRequested()) {
      try {
        auto connection = acceptConnection();
        connectionLoop(connection);
      }
      catch (const connection_timeout& e) {}
    }
  };

  return std::thread(loop);
  
}


std::string DaqCmdServer::exceptionHandler(std::string functionName) {
  try { throw; }
  catch (const json::out_of_range& e) {
    WARN("{}: json::out_of_range: {}", functionName, e.what());
    return fmt::format("{}: json::out_of_range: {}", functionName, e.what());
  }
  catch (const json::type_error& e) {
    WARN("{}: json::type_error: {}", functionName, e.what());
    return fmt::format("{}: json::out_of_range: {}", functionName, e.what());
  }
  catch (const std::runtime_error& e) {
    WARN("{}: runtime_error: {}", functionName, e.what());
    return fmt::format("{}: json::out_of_range: {}", functionName, e.what());
  }
  catch (const std::invalid_argument& e) {
    WARN("{}: invalid_argument: {}", functionName, e.what());
    return fmt::format("{}: json::out_of_range: {}", functionName, e.what());
  }
  catch (const std::exception& e) {
    WARN("{}: exception: {}", functionName, e.what());
    return fmt::format("{}: json::out_of_range: {}", functionName, e.what());
  }
  catch (...) {
    WARN("{}: unknown exception");
    return "unknown exception from catch(...)";
  }
}

json DaqCmdServer::setRunNumber(json args) {
  json reply;

  try {
    auto runNumber = args.at("run_number").get<std::uint32_t>();
    m_serverManager.runNumber(runNumber);
    // INFO("Run number set to ", runNumber);
    reply["response"] = "ok";
    reply["result"] = {};
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = exceptionHandler("setRunNumber");
  }
  
  return reply;
}

json DaqCmdServer::setBoardIds(json args) {
  json reply;

  try {
    auto boardIds = args.at("board_ids").get<std::vector<uint32_t>>();
    m_serverManager.setBoardIds(boardIds);
    reply["response"] = "ok";
    reply["result"] = {};
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = exceptionHandler("setBoardIds");
  }
  
  return reply;
}

json DaqCmdServer::setDaqServerSettings(json args) {
  json reply;

  try {
    auto settings = args.at("daq_server_settings");
    m_serverManager.daqServerSettings(settings);
    reply["response"] = "ok";
    reply["result"] = {};
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = exceptionHandler("setDaqServerSettings");
  }
  
  return reply;
}

json DaqCmdServer::setBoardMapping(json args) {
  json reply;

  try {
    auto mapping = args.at("board_mapping");
    m_serverManager.boardMapping(mapping);
    reply["response"] = "ok";
    reply["result"] = {};
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = exceptionHandler("setBoardMapping");
  }
  
  return reply;
}

json DaqCmdServer::setSystemConfiguration(json args) {
  json reply;

  try {
    auto conf = args.at("system_configuration");
    m_serverManager.systemConfiguration(conf);
    reply["response"] = "ok";
    reply["result"] = {};
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = exceptionHandler("setSystemConfiguration");
  }
  
  return reply;
}

json DaqCmdServer::setBoardsConfiguration(json args) {
  json reply;

  try {
    auto conf = args.at("boards_configuration");
    m_serverManager.boardsConfiguration(conf);
    reply["response"] = "ok";
    reply["result"] = {};
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = exceptionHandler("setBoardsConfiguration");
  }
  
  return reply;
}

json DaqCmdServer::setUserInfo(json args) {
  json reply;

  try {
    auto userInfo = args.at("user_info");
    m_serverManager.userInfo(userInfo);
    reply["response"] = "ok";
    reply["result"] = {};
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = exceptionHandler("setUserInfo");
  }
  
  return reply;
}

json DaqCmdServer::updateUserInfo(json args) {
  json reply;

  try {
    auto userInfo = args.at("user_info");
    m_serverManager.updateUserInfo(userInfo);
    reply["response"] = "ok";
    reply["result"] = {};
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = exceptionHandler("updateUserInfo");
  }
  
  return reply;
}

json DaqCmdServer::increaseRunNumber(json args) {
  json reply;

  try {
    m_serverManager.runNumber(m_serverManager.runNumber()+1);
    // INFO("Run number increased to ", m_serverManager.runNumber());
    reply["response"] = "ok";
    reply["result"] = {};
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = exceptionHandler("increaseRunNumber");
  }
  
  return reply;
}

json DaqCmdServer::startDaq(json args) {
  json reply;

  try {
    auto overwrite = args.value("overwrite", false);
    auto result = m_serverManager.startDaq(overwrite);
    if (result == DaqServerManager::Running) {
      reply["response"] = "ok";
      reply["result"] = {};
    }
    else {
      reply["response"] = "err";
      reply["result"] = "DAQ was not started. Writer error?";
    }
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = exceptionHandler("startDaq");
  }
  
  return reply;
}

json DaqCmdServer::stopDaq(json args) {
  json reply;
  json result;

  try {
    m_serverManager.stopDaq();

    addJsonStatus(result);

    reply["response"] = "ok";
    reply["result"] = result;
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = exceptionHandler("stopDaq");
  }
  
  return reply;
}


json DaqCmdServer::getStatus(json args) {
  json reply;
  json result;

  try {
    addJsonStatus(result);

    reply["response"] = "ok";
    reply["result"] = result;
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = exceptionHandler("getStatus");
  }
  
  return reply;
}


json DaqCmdServer::getRecordedRuns(json args) {
  namespace fs = std::filesystem;
  json reply;

  try {
    std::vector<int> runs;
    for (const auto & entry : fs::directory_iterator(m_serverManager.dataPath())) {
      try {
        auto pathStr = entry.path().string();
        auto lenStr = pathStr.length();
        runs.push_back(std::stoi(pathStr.substr(lenStr-6)));
      }
      catch(...) {}
    }

    std::sort(runs.begin(), runs.end());
    
    reply["response"] = "ok";
    reply["result"] = runs;
  }
  catch (...) {
    reply["response"] = "err";
    reply["result"] = exceptionHandler("getRecordedRuns");
  }
  
  return reply;
}


void DaqCmdServer::addJsonStatus(json& value) {
  value["state"] = m_serverManager.daqState();
  value["run_number"] = m_serverManager.runNumber();
  value["daq_monitor"] = m_serverManager.daqMonitor()->toJson();
  const auto diskSpaceInfo = m_serverManager.diskSpaceInfo();
  value["disk_space_info"] = {{"capacity", diskSpaceInfo.capacity}, {"available_space", diskSpaceInfo.available}, {"current_run_size", m_serverManager.runSizeInfo()}};
}
