#pragma once

#include <cstdint>
#include <string>

#include <mutex>
#include <thread>
#include <vector>

#include "network/common_server.hpp"
#include "daq_cmd_server.hpp"
#include "storage/data_packet.hpp"
#include "storage/data_queue.hpp"

class DataServer {
public:
  DataServer(std::string serverPort, size_t acceptMax);
  ~DataServer();

  std::thread acceptConnection(DataPacketQueue& dataPacketQueue, DaqServerManager& serverManager); 
  
private:
  CommonServer m_server;
  static void connectionThread(Connection connection, DataPacketQueue& dataPacketQueue, DaqServerManager& serverManager);
};
