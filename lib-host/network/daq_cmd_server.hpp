#pragma once

#include <thread>

#include "network/command_server.hpp"
#include "daq/daq_server_manager.hpp"

class DaqCmdServer : public CommandServer {
public:
  DaqCmdServer(std::string port, DaqServerManager& serverManager);

  std::thread commandThread();

private:
  DaqServerManager& m_serverManager;

  static std::string exceptionHandler(std::string functionName);

  json setRunNumber(json args);
  json setBoardIds(json args);
  json setDaqServerSettings(json args);
  json setBoardMapping(json args);
  json setSystemConfiguration(json args);
  json setBoardsConfiguration(json args);
  json setUserInfo(json args);
  json updateUserInfo(json args);
  json increaseRunNumber(json args);
  json startDaq(json args);
  json stopDaq(json args);
  json getStatus(json args);
  json getRecordedRuns(json args);
  void addJsonStatus(json& value);
};