#ifdef USE_ROOT
#include "utils/logger.hpp"

#include <cstdlib>
#include <ctime>
#include <filesystem>

#include "data_monitoring_stream.hpp"

using json = nlohmann::json;
namespace fs = std::filesystem;

DataMonitoringStream::DataMonitoringStream(const std::string path, json settings)
: m_prescalerSetting{settings.value("prescaler_setting", 100.f)}
, m_maxDeltaT{settings.value("max_delta_t", 0.5f)}
, m_writer(initializeWriter(path, settings))
, m_lastEventTimestamp{0}
{
  std::srand(std::time(nullptr));
}

bool DataMonitoringStream::process(TofpetEvent& event) const {
  const auto randN = static_cast<float>(std::rand()) / RAND_MAX;
  if (randN <= 1. / m_prescalerSetting || (event.timestamp() - m_lastEventTimestamp) / 160000000. >= m_maxDeltaT) {
    m_writer.append(event);
    m_lastEventTimestamp = event.timestamp();
  }
  return true;
}

void DataMonitoringStream::finalize() {
  m_writer.finalize();
}

EventWriterRoot DataMonitoringStream::initializeWriter(const std::string path, nlohmann::json settings) {
  auto monitoringPath = fs::path(path);

  if (fs::exists(monitoringPath)) {
    INFO("Directory {} exists. Removing it.", monitoringPath);
    fs::remove_all(monitoringPath);
  }

  // create run monitoring folder
  INFO("Creating directory {}", monitoringPath);
  fs::create_directory(monitoringPath);

  // override any events_per_file setting to have a single file
  settings["events_per_file"] = 1000000000000;

  return EventWriterRoot(monitoringPath, settings);
}
#endif // USE_ROOT