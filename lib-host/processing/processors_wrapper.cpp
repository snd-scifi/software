#include <filesystem>

#include "processing/simple_noise_filter.hpp"
#include "processing/data_monitoring_stream.hpp"
#include "processing/snd_add_flags.hpp"
#include "processing/snd_fast_noise_filter.hpp"
#include "processing/snd_advanced_noise_filter.hpp"
#include "processing/tofpet_calibrator.hpp"
#include "utils/logger.hpp"

#include "processors_wrapper.hpp"

namespace fs = std::filesystem;
using json = nlohmann::json;

ProcessorsWrapper::ProcessorsWrapper(EventQueue& inputEventQueue, EventQueue& outputEventQueue, DaqServerManager& serverManager)
  : m_inputEvents{inputEventQueue}
  , m_outputEvents{outputEventQueue}
  , m_serverManager{serverManager}
  , m_stopRequested{false}
  , m_writeEventNumber{m_serverManager.eventProcessorsSettings().value("write_event_number", false)}
  , m_monitor{std::make_shared<EventProcessorsMonitor>()}
{ 
  try {
    for (const auto& processor : m_serverManager.eventProcessorsSettings().at("processors").get<std::vector<std::string>>()) {
      try {
        if (processor == "simple_noise_filter") {
          m_eventProcessors.push_back(std::make_unique<SimpleNoiseFilter>(m_serverManager.boardMapping(), m_serverManager.eventProcessorsSettings().at("simple_noise_filter")));
          INFO("Added 'simple_noise_filter' processor.");
        }
        else if (processor == "snd_fast_noise_filter") {
          m_eventProcessors.push_back(std::make_unique<SndFastNoiseFilter>(m_serverManager.boardMapping(), m_serverManager.eventProcessorsSettings().at("snd_fast_noise_filter")));
          INFO("Added 'snd_fast_noise_filter' processor.");
        }
        else if (processor == "snd_advanced_noise_filter") {
          m_eventProcessors.push_back(std::make_unique<SndAdvancedNoiseFilter>(m_serverManager.boardMapping(), m_serverManager.eventProcessorsSettings().at("snd_advanced_noise_filter")));
          INFO("Added 'snd_advanced_noise_filter' processor.");
        }
        else if (processor == "tofpet_calibrator") {
          m_eventProcessors.push_back(std::make_unique<TofpetCalibrator>(m_serverManager.runPath()));
          INFO("Added 'tofpet_calibrator' processor.");
        }
        else if (processor == "snd_add_flags") {
          m_eventProcessors.push_back(std::make_unique<SndAddFlags>(m_serverManager));
          INFO("Added 'snd_add_flags' processor.");
        }
        else if (processor == "data_monitoring_stream") {
#ifdef USE_ROOT
          auto monitoringPath = (m_serverManager.dataPath() / fmt::format("monitoring_run_{:06d}", m_serverManager.runNumber())).string();
          m_eventProcessors.push_back(std::make_unique<DataMonitoringStream>(monitoringPath, m_serverManager.eventProcessorsSettings().at("data_monitoring_stream")));
          INFO("Added data_monitoring_stream processor.");
#else
          WARN("Cannot add data_monitoring_stream processor. Software was compiled without ROOT enabled. Call cmake with option -DUSE_ROOT=ON");
#endif // USE_ROOT
        }
        else {
          WARN("Unknown processor '{}'.", processor);
        }
      }
      // these should be fired when adding single processors 
      catch (const std::runtime_error& e) {
        WARN("Processor '{}' not added. {}.", processor, e.what());
      }
      catch (const json::out_of_range& e) {
        WARN("Processor '{}' not added. Configuration not found.", processor);
      }
    }
  }
  // these should be fired when looking for the list of processors
  catch (const std::runtime_error& e) {
    WARN("No processors added. The configuration for event processors has not been provided.");
  }
  catch (const json::out_of_range& e) {
    WARN("No processors added. The processors configuration doesn't contain the list of processors.");
  }

  m_serverManager.daqMonitor()->addMonitor(m_monitor);
  
  m_processorThread = std::thread([this] { processorsLoop(); });
  NOTICE("Processors thread started.");
}

void ProcessorsWrapper::processorsLoop() {
  bool breakFlag{false};
  uint64_t eventCounter{0};
  while (!breakFlag) {
    // check if the event builder thread has finished
    breakFlag = m_stopRequested.load();
    // get all the events from the event queue
    auto evtQueue = m_inputEvents.takeAll(1000);

    m_serverManager.daqMonitor()->eventQueueLength(m_inputEvents.size());
    m_monitor->localEventQueueLength(evtQueue.size());
    // loop over them and put the good one in the processed queue
    while (!evtQueue.empty()) {
      // get the event and pop it from the front of the queue
      auto evt = std::move(evtQueue.front());
      evtQueue.pop_front();
      m_monitor->localEventQueueLength(evtQueue.size());

      // pass it though the processors. If they all return true, save it.
      bool saveEvent{true};
      for (const auto& processor : m_eventProcessors) {
        saveEvent = processor->process(evt);
        if (!saveEvent) {
          break;
        }
      }
      
      // put the event in the output queue for writing
      if (saveEvent) {
        if (m_writeEventNumber) {
          evt.evtNumber(eventCounter);
        }
        eventCounter++;
        m_monitor->increaseProcessedEvents();
        m_monitor->increaseProcessedHits(evt.nHits());
        m_outputEvents.emplace_back(std::move(evt));
        m_serverManager.daqMonitor()->processedEventQueueLength(m_outputEvents.size());
      }
      else {
        m_monitor->increaseDiscardedEvents(1);
        m_monitor->increaseDiscardedHits(evt.nHits());
      }
    }
  }
  for (const auto& processor : m_eventProcessors) {
    processor->finalize();
  }
}

void ProcessorsWrapper::finalize() {
  NOTICE("Stopping processors wrapper...");
  m_stopRequested = true;
  m_processorThread.join();
  NOTICE("Processors wrapper stopped.");
}