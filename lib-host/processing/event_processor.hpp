#pragma once

#include <string>

#include "storage/tofpet_event.hpp"

/**
 * Base event processor class.
 */
class EventProcessor {
public:
  virtual ~EventProcessor() = default;

  virtual std::string name() const = 0;

  /**
   * Processes the event and returns true if the event should be saved, false otherwise.
   */
  virtual bool process(TofpetEvent& event) const = 0;
  virtual void finalize() {};

};