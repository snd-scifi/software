#pragma once

#include <vector>
#include <map>

#include "mechanics/plane.hpp"

#include "json.hpp"

#include "processing/event_processor.hpp"

using SubsystemId = uint32_t; // ID of the subsystem within the noise filter
using BoardId = uint32_t;
using SubsystemMap = std::map<BoardId, SubsystemId>;
using SubsystemNameVector = std::vector<std::string>;
using SubsystemIdMap = std::map<std::string, SubsystemId>;
using CountVector = std::vector<uint64_t>;

class SimpleNoiseFilter : public EventProcessor {
public:
  SimpleNoiseFilter(const nlohmann::json& boardMapping, const nlohmann::json& settings);
  ~SimpleNoiseFilter() = default;

  virtual std::string name() const { return "SimpleNoiseFilter"; };

  virtual bool process(TofpetEvent& event) const override;

private:
  static SubsystemNameVector generateSubsystemNameVector(const nlohmann::json& boardMapping);
  SubsystemMap generateSubsystemMap(const nlohmann::json& boardMapping) const;
  
  CountVector generateSettingVector(const nlohmann::json& settings, const std::string& key) const;

  const SubsystemNameVector m_subsystemNameVector; // the index of this vector is used for the internal mapping as subsystem ID
  const SubsystemMap m_subsystemMap; // board ID to subsystem internal ID
  const CountVector m_boardsMinVector; // how many boards with at least one hit
  const CountVector m_hitsMinVector; // how many hits in at least one board
  const uint64_t m_totalHitsMin; // how many total hits minimum


};
