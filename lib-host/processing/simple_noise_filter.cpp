#include <set>
#include <algorithm>

#include "simple_noise_filter.hpp"

#include "utils/logger.hpp"
#include "utils/range.hpp"

SimpleNoiseFilter::SimpleNoiseFilter(const nlohmann::json& boardMapping, const nlohmann::json& settings) 
  : m_subsystemNameVector(generateSubsystemNameVector(boardMapping))
  , m_subsystemMap(generateSubsystemMap(boardMapping))
  , m_boardsMinVector(generateSettingVector(settings, "n_boards_min"))
  , m_hitsMinVector(generateSettingVector(settings, "n_hits_min"))
  , m_totalHitsMin{settings.value("n_total_hits_min", -1UL)}
{
  PRINT("m_subsystemNameVector: {}\n", m_subsystemNameVector);
  PRINT("m_subsystemMap: {}\n", m_subsystemMap);
  PRINT("m_boardsMinVector: {}\n", m_boardsMinVector);
  PRINT("m_hitsMinVector: {}\n", m_hitsMinVector);
  PRINT("m_totalHitsMin: {}\n", m_totalHitsMin);
}

bool SimpleNoiseFilter::process(TofpetEvent& event) const {
  if (event.nHits() > m_totalHitsMin) {
    return true;
  }

  CountVector boardCount(m_subsystemNameVector.size(), 0UL);
  CountVector hitCount(m_subsystemNameVector.size(), 0UL);
  std::set<BoardId> boardHitSet;

  for (const auto& hit : event) {
    const auto& subsystemId = m_subsystemMap.at(hit.boardId());
    hitCount.at(subsystemId)++;
    // if the boardId gets inserted in the set (insert(...).second is true), 
    // it was not previously there, so we can count an additional board for this subsystem
    if (boardHitSet.insert(hit.boardId()).second) boardCount.at(subsystemId)++;
  }

  for (auto i : indices(m_hitsMinVector)) {
    if (boardCount.at(i) >= m_boardsMinVector.at(i)) return true;
    if (hitCount.at(i) >= m_hitsMinVector.at(i)) return true;
  }

  return false;
}


SubsystemNameVector SimpleNoiseFilter::generateSubsystemNameVector(const nlohmann::json& boardMapping) {
  SubsystemNameVector retMap;
  for (const auto& subsystemMapping : boardMapping.items()) {
    const auto& subsystem = subsystemMapping.key();
    retMap.emplace_back(subsystem);
  }
  return retMap;
}

SubsystemMap SimpleNoiseFilter::generateSubsystemMap(const nlohmann::json& boardMapping) const {
  SubsystemIdMap subsystemIdMap;
  for (SubsystemId subsystemId{0}; subsystemId < m_subsystemNameVector.size(); subsystemId++) {
    subsystemIdMap.try_emplace(m_subsystemNameVector.at(subsystemId), subsystemId);
  }

  SubsystemMap retMap;
  for (const auto& subsystemMapping : boardMapping.items()) {
    const auto subsystemName = subsystemMapping.key();
    for (const auto& planeMapping : subsystemMapping.value().items()) {
      const auto planeName = planeMapping.key();
      const auto& planeMap = planeMapping.value();
      const auto planeClass = planeMap.at("class").get<std::string>();
      const auto planeType = planeMap.at("type").get<PlaneType>();

      if (planeType == PlaneType::Invalid) {
        THROW_RE("Invalid plane type: {}", planeMap.at("type").get<std::string>());
      }

      if (planeClass == "multiboard") {
        const auto boardIds = planeMap.at("boards").get<std::vector<uint32_t>>();
        INFO("{} {} - {}", subsystemName, planeName, boardIds);
        for (const auto boardId : boardIds) {
          retMap.try_emplace(boardId, subsystemIdMap.at(subsystemName));
        }
      }
      else if (planeClass == "multislot") {
        const auto boardId = planeMap.at("board").get<uint32_t>();
        const auto slots = planeMap.at("slots").get<std::vector<std::string>>();
        INFO("{} {} - {} {}", subsystemName, planeName, boardId, slots);
        retMap.try_emplace(boardId, subsystemIdMap.at(subsystemName));
      }
    }
  }
  return retMap;
}

CountVector SimpleNoiseFilter::generateSettingVector(const nlohmann::json& settings, const std::string& key) const {
  CountVector retMap;
  for (const auto& subsystemName : m_subsystemNameVector) {
    try {
      const auto& subSystemSettings = settings.at(subsystemName);
      retMap.push_back(subSystemSettings.value(key, -1UL));
    }
    catch (const nlohmann::json::out_of_range& e) {
      retMap.push_back(-1UL);
    }
  }
  return retMap;
}


