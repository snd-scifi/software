#pragma once

#include <vector>
#include <map>

#include "processing/event_processor.hpp"
#include "daq/daq_server_manager.hpp"

#include "json.hpp"

class SndAddFlags : public EventProcessor {
public:

  SndAddFlags(const DaqServerManager& serverManager);
  ~SndAddFlags() = default;

  virtual std::string name() const { return "SndAddFlags"; };

  virtual bool process(TofpetEvent& event) const override;

private:
  const DaqServerManager& m_serverManager;

};
