#include <set>

#include "utils/logger.hpp"
#include "utils/snd_evt_flags.hpp"

#include "snd_fast_noise_filter.hpp"

using json = nlohmann::json;

SndFastNoiseFilter::SndFastNoiseFilter(const json boardMapping, const json settings)
  : m_scifiNboardsMin{settings.at("scifi").value("n_boards_min", -1UL)}
  , m_usNhitsMin{settings.at("us").value("n_hits_min", -1UL)}
  , m_dsNhitsMin{settings.at("ds").value("n_hits_min", -1UL)}
  , m_scifiNtotalHitsMin{settings.at("scifi").value("n_total_hits_min", -1UL)}
  , m_usNtotalHitsMin{settings.at("us").value("n_total_hits_min", -1UL)}
  , m_dsNtotalHitsMin{settings.at("ds").value("n_total_hits_min", -1UL)}  
  , m_vetoNtotalHitsMin{settings.at("veto").value("n_total_hits_min", -1UL)}  
{
  for (const auto& subsystemMapping : boardMapping.items()) {
    const auto subsystem = subsystemMapping.key();
    for (const auto& planeMapping : subsystemMapping.value().items()) {
      const auto plane = planeMapping.key();
      const auto& planeMap = planeMapping.value();
      const auto planeClass = planeMap.at("class").get<std::string>();
      const auto planeType = planeMap.at("type").get<PlaneType>();

      if (planeType == PlaneType::Invalid) {
        THROW_RE("Invalid plane type: {}", planeMap.at("type").get<std::string>());
      }

      if (planeClass == "multiboard" && planeType == PlaneType::SndScifi) {
        const auto boardIds = planeMap.at("boards").get<std::vector<uint32_t>>();
        // INFO("snd_scifi {} - {}", plane, boardIds);
        for (const auto boardId : boardIds) {
          m_planeTypeMap.try_emplace(boardId, PlaneType::SndScifi);
        }
      }
      else if (planeClass == "multislot") {
        const auto boardId = planeMap.at("board").get<uint32_t>();
        const auto slots = planeMap.at("slots").get<std::vector<std::string>>();
        // NOTICE("{} {} - {} {}", planeType, plane, boardId, slots);
        m_planeTypeMap.try_emplace(boardId, planeType);
      }
    }
  }
  // NOTICE("SND Fast noise Filter. SciFi min N boards: {}", m_scifiNboardsMin);
  // NOTICE("SND Fast noise Filter. SciFi min N total hits: {}", m_scifiNtotalHitsMin);
  // NOTICE("SND Fast noise Filter. US min N hits: {}", m_usNhitsMin);
  // NOTICE("SND Fast noise Filter. US min N total hits: {}", m_usNtotalHitsMin);
  // NOTICE("SND Fast noise Filter. DS min N hits: {}", m_dsNhitsMin);
  // NOTICE("SND Fast noise Filter. DS min N total hits: {}", m_dsNtotalHitsMin);
}

/**
 * It checks the number of hits in each SND subsystem and decides wether an event passes the filter.
 * The conditions to accept an event are the following:
 * SciFi:
 * * there must be at least `n_boards_min` boards that see at least one hit OR
 * * there must be at least `n_total_hits_min` total hits in the whole subsystem
 * US:
 * * there must be at least `n_hits_min` recorded by any of the boards in US OR
 * * there must be at least `n_total_hits_min` total hits in the whole subsystem
 * DS: is the same as US
 * VETO: there must be at least `n_total_hits_min` total hits in the whole subsystem
 * 
 * If any of the subdetector contitions passes, the event is accepted (i.e. there is an OR between the subsystems).
 */
bool SndFastNoiseFilter::process(TofpetEvent& event) const {
  size_t scifiBoardsCounter{0};
  size_t scifiTotalHitsCounter{0};
  size_t usTotalHitsCounter{0};
  size_t dsTotalHitsCounter{0};
  size_t vetoTotalHitsCounter{0};
  uint8_t flags{0};

  std::map<uint32_t, size_t> hitCounts;

  for (const auto& hit : event) {
    const auto& planeType = m_planeTypeMap.at(hit.boardId());

    switch (planeType) {
    case PlaneType::SndScifi:
      // if this is the first time I see this board ID, count it
      if (hitCounts.insert({hit.boardId(), 1}).second) scifiBoardsCounter++;
      // else hitCounts.at(hit.boardId())++; // not used
      if (scifiBoardsCounter >= m_scifiNboardsMin) flags |= FAST_FILTER_SCIFI;
      // add 1 to the total SciFi hits
      scifiTotalHitsCounter++;
      if (scifiTotalHitsCounter >= m_scifiNtotalHitsMin) flags |= FAST_FILTER_SCIFI_TOTAL;
      break;

    case PlaneType::SndUs:
      // Insert 1 in the map for this boardId. If already present, increase by one
      if (!hitCounts.insert({hit.boardId(), 1}).second) {
        hitCounts.at(hit.boardId())++;
      }
      if (hitCounts.at(hit.boardId()) >= m_usNhitsMin) flags |= FAST_FILTER_US;
      
      usTotalHitsCounter++;
      if (usTotalHitsCounter >= m_usNtotalHitsMin) flags |= FAST_FILTER_US_TOTAL;
      break;

    case PlaneType::SndDsH:
    case PlaneType::SndDsV:
      if (!hitCounts.insert({hit.boardId(), 1}).second) {
        hitCounts.at(hit.boardId())++;
      }
      if (hitCounts.at(hit.boardId()) >= m_dsNhitsMin) flags |= FAST_FILTER_DS;
      
      dsTotalHitsCounter++;
      if (dsTotalHitsCounter >= m_dsNtotalHitsMin) flags |= FAST_FILTER_DS_TOTAL;
      break;

    case PlaneType::SndVeto:
      vetoTotalHitsCounter++;
      if (vetoTotalHitsCounter >= m_vetoNtotalHitsMin) flags |= FAST_FILTER_VETO_TOTAL;
      break;

    default:
      break;
    }
  }

  event.setFlags(FAST_FILTER_MASK, flags);
  return flags;
}
