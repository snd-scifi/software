#pragma once

#include <vector>
#include <map>

#include "mechanics/plane.hpp"
#include "processing/event_processor.hpp"

#include "json.hpp"

class SndFastNoiseFilter : public EventProcessor {
public:

  SndFastNoiseFilter(const nlohmann::json boardMapping, const nlohmann::json settings);
  ~SndFastNoiseFilter() = default;

  virtual std::string name() const { return "SndFastNoiseFilter"; };

  virtual bool process(TofpetEvent& event) const override;

private:
  std::map<uint32_t, PlaneType> m_planeTypeMap;

  size_t m_scifiNboardsMin;
  size_t m_usNhitsMin;
  size_t m_dsNhitsMin;

  size_t m_scifiNtotalHitsMin;
  size_t m_usNtotalHitsMin;
  size_t m_dsNtotalHitsMin;
  size_t m_vetoNtotalHitsMin;

};
