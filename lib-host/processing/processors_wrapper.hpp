#pragma once

#include <atomic>
#include <thread>

#include "daq/daq_server_manager.hpp"
#include "monitoring/event_processors_monitor.hpp"
#include "processing/event_processor.hpp"
#include "storage/evt_queue.hpp"

#include "json.hpp"

class ProcessorsWrapper {
public:
  ProcessorsWrapper(EventQueue& inputEventQueue, EventQueue& outputEventQueue, DaqServerManager& serverManager);
  ~ProcessorsWrapper() = default;
  void finalize();

private:
  void processorsLoop();
  EventQueue& m_inputEvents;
  EventQueue& m_outputEvents;
  DaqServerManager& m_serverManager;
  std::atomic_bool m_stopRequested;

  const bool m_writeEventNumber;

  EventProcessorsMonitorPtr m_monitor;

  std::thread m_processorThread;
  std::vector<std::unique_ptr<EventProcessor>> m_eventProcessors;
};