#include <string>

#include "utils/logger.hpp"
#include "utils/snd_evt_flags.hpp"

#include "snd_add_flags.hpp"

using json = nlohmann::json;

SndAddFlags::SndAddFlags(const DaqServerManager& serverManager) : m_serverManager(serverManager) {}


bool SndAddFlags::process(TofpetEvent& event) const {
  auto info = m_serverManager.userInfo();
  try {
    event.setFlags(FILL_NUMBER_MASK, info.value("fill_number", 0U));
    
    auto acceleratorMode = info.value("accelerator_mode", LhcAcceleratorMode::Null);
    // if (acceleratorMode == LhcAcceleratorMode::Unknown) {
    //   WARN("Unknown accelerator mode: {}", info["accelerator_mode"].get<std::string>());
    // }
    event.setFlags(ACCELERATOR_MODE_MASK, static_cast<uint8_t>(acceleratorMode));
    
    auto beamMode = info.value("beam_mode", LhcBeamMode::Null);
    // if (beamMode == LhcBeamMode::Unknown) {
    //   WARN("Unknown beam mode: {}", info["beam_mode"].get<std::string>());
    // }
    event.setFlags(BEAM_MODE_MASK, static_cast<uint8_t>(beamMode));
  }
  catch (const nlohmann::detail::type_error& e) {}
  
  return true;
}
