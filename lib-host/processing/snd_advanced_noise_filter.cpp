#include "utils/logger.hpp"
#include "utils/snd_evt_flags.hpp"

#include "snd_advanced_noise_filter.hpp"

using json = nlohmann::json;

SndAdvancedNoiseFilter::SndAdvancedNoiseFilter(const json boardMapping, const json settings)
  : m_detector{boardMapping}
  , m_scifiNplanesMin{settings.value("n_scifi_planes_min", -1UL)}
  , m_scifiNhitsPerPlaneMin{settings.value("n_scifi_hits_per_plane_min", -1UL)}
  , m_usNplanesMin{settings.value("n_us_planes_min", -1UL)}
  , m_usNhitsPerPlaneMin{settings.value("n_us_hits_per_plane_min", -1UL)}
  , m_dshNplanesMin{settings.value("n_dsh_planes_min", -1UL)}
  , m_dshNhitsPerPlaneMin{settings.value("n_dsh_hits_per_plane_min", -1UL)}
  , m_dsvNplanesMin{settings.value("n_dsv_planes_min", -1UL)}
  , m_dsvNhitsPerPlaneMin{settings.value("n_dsv_hits_per_plane_min", -1UL)}
  , m_dsNplanesMin{settings.value("n_ds_planes_min", -1UL)}
  , m_vetoNplanesMin{settings.value("n_veto_planes_min", -1UL)}
  , m_vetoNhitsPerPlaneMin{settings.value("n_veto_hits_per_plane_min", -1UL)}
  , m_globalNplanesMin{settings.value("n_global_planes_min", -1UL)}
{
  
}


bool SndAdvancedNoiseFilter::process(TofpetEvent& event) const {
  //make a copy, otherwise the method cannot be const
  auto detector = m_detector;
  uint16_t flags{0};
  for (const auto& hit : event) {
      detector.addHit(hit);
  }

  if (m_scifiNplanesMin <= detector.nPlanesWithHits(PlaneType::SndScifi)) flags |= ADVANCED_FILTER_SCIFI_PLANES;
  if (m_usNplanesMin <= detector.nPlanesWithHits(PlaneType::SndUs)) flags |= ADVANCED_FILTER_US_PLANES;

  auto nPlanesDsh = detector.nPlanesWithHits(PlaneType::SndDsH);
  auto nPlanesDsv = detector.nPlanesWithHits(PlaneType::SndDsV);
  if (m_dshNplanesMin <= nPlanesDsh) flags |= ADVANCED_FILTER_DSH_PLANES;
  if (m_dshNplanesMin <= nPlanesDsv) flags |= ADVANCED_FILTER_DSV_PLANES;
  if (m_dsNplanesMin <= (nPlanesDsh + nPlanesDsv)) flags |= ADVANCED_FILTER_DS_PLANES;
  
  if (m_vetoNplanesMin <= detector.nPlanesWithHits(PlaneType::SndVeto)) flags |= ADVANCED_FILTER_VETO_PLANES;
  if (m_globalNplanesMin <= detector.nPlanesWithHits()) flags |= ADVANCED_FILTER_GLOBAL_PLANES;

  if (m_scifiNhitsPerPlaneMin <= detector.nMaxHitsPerPlaneType(PlaneType::SndScifi)) flags |= ADVANCED_FILTER_SCIFI_HITS;
  if (m_usNhitsPerPlaneMin <= detector.nMaxHitsPerPlaneType(PlaneType::SndUs)) flags |= ADVANCED_FILTER_US_HITS;
  if (m_dshNhitsPerPlaneMin <= detector.nMaxHitsPerPlaneType(PlaneType::SndDsH)) flags |= ADVANCED_FILTER_DSH_HITS;
  if (m_dsvNhitsPerPlaneMin <= detector.nMaxHitsPerPlaneType(PlaneType::SndDsV)) flags |= ADVANCED_FILTER_DSV_HITS;
  if (m_vetoNhitsPerPlaneMin <= detector.nMaxHitsPerPlaneType(PlaneType::SndVeto)) flags |= ADVANCED_FILTER_VETO_HITS;

  event.setFlags(ADVANCED_FILTER_MASK, flags);
  return flags;
}
