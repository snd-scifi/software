#include <string>
#include <filesystem>

#include "csv.hpp"

#include "utils/calibration.hpp"
#include "utils/logger.hpp"

#include "tofpet_calibrator.hpp"

namespace fs = std::filesystem;

TofpetCalibrator::TofpetCalibrator(std::string path, const nlohmann::json settings) 
  : m_timestampCorrectDoT1delay{settings.value("timestamp_correct_do_t1_delay", false)}
  , m_qdcCorrectVcoarse{settings.value("qdc_correct_v_coarse", true)}
  , m_qdcCorrectDoT1delay{settings.value("qdc_correct_do_t1_delay", true)}
  , m_qdcCorrectGain{settings.value("qdc_correct_gain", false)}
  , m_totVcoarseMax{settings.value<uint16_t>("tot_v_coarse_max", 1023)}
  , m_totCorrectDoT1delay{settings.value("tot_correct_do_t1_delay", true)}
{
  auto channelConfPath = fs::path(path) / "channels.csv";
  auto tdcCalPath = fs::path(path) / "tdc_cal.csv";
  auto qdcCalPath = fs::path(path) / "qdc_cal.csv";

  static const std::map<std::string, float> qdcGain = {
    {"1_00", 1.00},
    {"1_39", 1.39},
    {"1_70", 1.70},
    {"2_50", 2.50},
    {"3_65", 3.65} 
  };

  static const std::map<std::string, float> doT1Delay = {
    {"off", 0.0},
    {"3_ns", 3/6.23768058085},
    {"6_ns", 6/6.23768058085},
    {"9_ns", 9/6.23768058085}
  };
  
  INFO("Reading channels configuration...");
  csv::CSVReader channelsReader(channelConfPath.string());
  for (csv::CSVRow& row: channelsReader) {
      //if (!row["masked"].get<bool>()) {
      try {
        m_channelSettings[getIndex(row["board_id"].get<uint32_t>(), row["fe_id"].get<uint8_t>(), row["channel"].get<uint8_t>())] 
          = {row["masked"].get<bool>(),
          row["meas_mode"].get<>() == "qdc",
          qdcGain.at(row["g_q"].get<>()),
          doT1Delay.at(row["do_t1_delay"].get<>()),
          integrationTimeClockCycles(row["qdc_min_intg_t"].get<uint16_t>()),
          integrationTimeClockCycles(row["qdc_max_intg_t"].get<uint16_t>())
        };
      }
      catch (const std::runtime_error& e) {}
  }

  INFO("Reading TDC calibration...");
  csv::CSVReader tdcReader(tdcCalPath.string());
  for (csv::CSVRow& row: tdcReader) {
      m_tdcCal[getIndex(
        row["board_id"].get<uint32_t>(),
        row["fe_id"].get<uint8_t>(),
        row["channel"].get<uint8_t>(),
        row["tac"].get<uint8_t>(),
        row["tdc"].get<bool>())] 
        = {
          row["a"].get<float>(),
          row["b"].get<float>(),
          row["c"].get<float>(),
          row["d"].get<float>(),
          row["chi2"].get<float>(),
          row["dof"].get<float>()
        };
  }

  INFO("Reading QDC calibration...");
  csv::CSVReader qdcReader(qdcCalPath.string());
  for (csv::CSVRow& row: qdcReader) {
      m_qdcCal[getIndex(
        row["board_id"].get<uint32_t>(),
        row["fe_id"].get<uint8_t>(),
        row["channel"].get<uint8_t>(),
        row["tac"].get<uint8_t>())] 
        = {
          row["a"].get<float>(),
          row["b"].get<float>(),
          row["c"].get<float>(),
          row["d"].get<float>(),
          row["e"].get<float>(),
          row["chi2"].get<float>(),
          row["dof"].get<float>()
        };
  }

}

void TofpetCalibrator::calibrate(TofpetHit& hit) const {
  try {
    auto chsetting = m_channelSettings.at(getIndex(hit.boardId(), hit.tofpetId(), hit.tofpetChannel()));
    auto tsDoT1del = m_timestampCorrectDoT1delay ? chsetting.doT1delay : 0.0;
    hit.calculateTimestamp(m_tdcCal.at(getIndex(hit.boardId(), hit.tofpetId(), hit.tofpetChannel(), hit.tac(), 0)), tsDoT1del);
    if (chsetting.qdc) {

      if (!m_qdcCorrectVcoarse) {
        chsetting.intgTimeMin = 0;
      }
      if (!m_qdcCorrectDoT1delay) {
        chsetting.doT1delay = 0.0;
      }
      if (!m_qdcCorrectGain) {
        chsetting.qdcGain = 1.0;
      }

      hit.calculateValueQdc(
        m_qdcCal.at(getIndex(hit.boardId(), hit.tofpetId(), hit.tofpetChannel(), hit.tac())),
        chsetting.intgTimeMin,
        chsetting.intgTimeMax,
        chsetting.doT1delay,
        chsetting.qdcGain
        );
    }
    else {
      if (!m_totCorrectDoT1delay) {
        chsetting.doT1delay = 0.0;
      }
      hit.calculateValueTot(m_tdcCal.at(getIndex(hit.boardId(), hit.tofpetId(), hit.tofpetChannel(), hit.tac(), 1)), chsetting.doT1delay, m_totVcoarseMax);
    }
  }
  catch (const std::out_of_range& e) {
    WARN("Calibration data for board {}, TOFPET {}, channel {} not found.", hit.boardId(), hit.tofpetId(), hit.tofpetChannel());
  }
}


void TofpetCalibrator::calibrate(TofpetEvent& event) const {
  for (auto& hit : event) {
    calibrate(hit);
  }
}
