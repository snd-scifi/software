#pragma once

#include <map>
#include <string>
#include <tuple>

#include "processing/event_processor.hpp"
#include "storage/tofpet_hit.hpp"
#include "storage/tofpet_event.hpp"
#include "json.hpp"

class TofpetCalibrator : public EventProcessor {
public:
  TofpetCalibrator(std::string path, const nlohmann::json settings = nlohmann::json({}));
  ~TofpetCalibrator() = default;


  virtual std::string name() const { return "TofpetCalibrator"; };

  virtual bool process(TofpetEvent& event) const override { calibrate(event); return true; }

  void calibrate(TofpetHit& hit) const;
  void calibrate(TofpetEvent& event) const;

private:
  uint32_t getIndex(uint32_t boardId, uint8_t tofpetId, uint32_t tofpetChannel, uint8_t tac=0, bool tdc=false) const
    { return tdc + 10 * tac + 100 * tofpetChannel + 10000 * tofpetId + 100000 * boardId; }

  struct ChannelSettings {
    bool masked;
    bool qdc;
    float qdcGain;
    float doT1delay;
    uint16_t intgTimeMin;
    uint16_t intgTimeMax;
  };

  const bool m_timestampCorrectDoT1delay;

  const bool m_qdcCorrectVcoarse;
  const bool m_qdcCorrectDoT1delay;
  const bool m_qdcCorrectGain;

  const uint16_t m_totVcoarseMax;
  const bool m_totCorrectDoT1delay;

  std::map<uint32_t, ChannelSettings> m_channelSettings;
  std::map<uint32_t, std::array<float, 6>> m_tdcCal; //a, b, c, d, chi2, dof
  std::map<uint32_t, std::array<float, 7>> m_qdcCal; //a, b, c, d, e, chi2, dof

};