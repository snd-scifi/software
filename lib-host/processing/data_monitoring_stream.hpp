#pragma once
#ifdef USE_ROOT

#include <string>
#include <vector>

#include "io/event_writer_root.hpp"
#include "processing/event_processor.hpp"
#include "utils/timer.hpp"

#include "json.hpp"

class DataMonitoringStream : public EventProcessor {
public:
  DataMonitoringStream(const std::string path, nlohmann::json settings);
  ~DataMonitoringStream() = default;

  virtual std::string name() const { return "DataMonitoringStream"; };

  virtual bool process(TofpetEvent& event) const override;
  void finalize() override;

private:
  EventWriterRoot initializeWriter(const std::string path, nlohmann::json settings);
  float m_prescalerSetting;
  float m_maxDeltaT;
  mutable EventWriterRoot m_writer;
  mutable int64_t m_lastEventTimestamp;

};

#endif // USE_ROOT