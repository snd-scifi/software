#pragma once

#include <vector>
#include <map>

#include "mechanics/detector.hpp"
#include "processing/event_processor.hpp"

#include "json.hpp"

class SndAdvancedNoiseFilter : public EventProcessor {
public:

  SndAdvancedNoiseFilter(const nlohmann::json boardMapping, const nlohmann::json settings);
  ~SndAdvancedNoiseFilter() = default;

  virtual std::string name() const { return "SndAdvancedNoiseFilter"; };

  virtual bool process(TofpetEvent& event) const override;

private:
  Detector m_detector;

  size_t m_scifiNplanesMin;
  size_t m_scifiNhitsPerPlaneMin;
  size_t m_usNplanesMin;
  size_t m_usNhitsPerPlaneMin;
  size_t m_dshNplanesMin;
  size_t m_dshNhitsPerPlaneMin;
  size_t m_dsvNplanesMin;
  size_t m_dsvNhitsPerPlaneMin;
  size_t m_dsNplanesMin;
  size_t m_vetoNplanesMin;
  size_t m_vetoNhitsPerPlaneMin;
  size_t m_globalNplanesMin;

};
