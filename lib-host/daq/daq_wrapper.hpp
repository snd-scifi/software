#pragma once

#include <string>
#include <cstdint>
#include <thread>

#include "storage/data_queue.hpp"
#include "storage/evt_queue.hpp"

class DaqServerManager;
class EventBuilder;

/**
 * Class used to read data from the DataPacketQueue filled by the DAQ threads and write it to disk.
 */
class DaqWrapper {
public:
  DaqWrapper(DataPacketQueue& dataPacketQueue, DaqServerManager& serverManager);

  /**
   * Starts the writer thread and returns it.
   */
  std::thread daqLoopThread() { return std::thread([this] () { this->daqLoop(); } ); }

private:
  // std::string m_filename;
  DataPacketQueue& m_dataPacketQueue;
  DaqServerManager& m_serverManager;
  // WriterType m_writerType;

  void daqLoop();
  std::unique_ptr<EventBuilder> getEventBuilder(EventQueue& eventQueue);
  // std::unique_ptr<EventWriter> getEventWriter();
  // std::unique_ptr<PacketWriter> getPacketWriter();

  void saveConfiguration();

};
