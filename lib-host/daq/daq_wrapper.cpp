#include <filesystem>
#include <string>
#include <set>
#include <fstream>
#include <iomanip>

#ifdef USE_ROOT
#include "TROOT.h"
#endif

#include "storage/tofpet_hit.hpp"
#include "storage/tofpet_event.hpp"
#include "io/packet_writer.hpp"
#include "io/packet_writer_bin.hpp"
#include "daq/daq_server_manager.hpp"
#include "event_building/event_builder.hpp"
#include "event_building/event_builder_trigless.hpp"
#include "event_building/event_builder_trigger.hpp"
#include "processing/processors_wrapper.hpp"
#include "io/event_writer_wrapper.hpp"
#include "utils/logger.hpp"
#include "utils/root.hpp"

#include "daq_wrapper.hpp"

using json = nlohmann::json;
namespace fs = std::filesystem;

/**
 * The constructor for DaqWrapper will:
 *  * create the data directory if it doesn't already exist, and throws an exception of this operation fails;
 *  * create the run directory, removing the old one if it already exists.
 */
DaqWrapper::DaqWrapper(DataPacketQueue& dataPacketQueue, DaqServerManager& serverManager)
  : m_dataPacketQueue(dataPacketQueue), m_serverManager(serverManager)
  {
  auto dataPath = m_serverManager.dataPath();

  //create data path, if it doesn't exist
  if (!fs::exists(dataPath)) {
    INFO("Creating runs directory {}", dataPath);
    fs::create_directories(dataPath);
  }

  if (!fs::is_directory(dataPath)) {
    ERROR("{} is not a directory.", dataPath.string());
    THROW_RE("{} is not a directory.", dataPath.string());
  }

  auto runPath = m_serverManager.runPath();

  if (fs::exists(runPath)) {
    if (m_serverManager.overwriteData()) {
      WARN("Directory {} exists. Removing it.", runPath);
      fs::remove_all(runPath);
    }
    else {
      ERROR("Directory {} exists. Cannot overwrite it.", runPath);
      THROW_RE("Directory {} exists. Cannot overwrite it.", runPath);
    }
  }

  // create run folder
  INFO("Creating directory {}", runPath);
  fs::create_directory(runPath);

#if USE_ROOT
  ROOT::EnableThreadSafety();
#endif

}

/**
 * The main daq loop.
 */
void DaqWrapper::daqLoop() {
  saveConfiguration();

  if ( m_serverManager.daqServerSettings().value("build_events", false) ){
    EventQueue eventQueue;
    EventQueue processedEventQueue;
    DaqMonitorPtr daqMonitor = std::make_shared<DaqMonitor>(m_serverManager.getBoardIds());
    m_serverManager.daqMonitor(daqMonitor);

    auto builder = getEventBuilder(eventQueue);

    ProcessorsWrapper processors(eventQueue, processedEventQueue, m_serverManager);
    EventWriterWrapper writer(processedEventQueue, m_serverManager);

    m_serverManager.daqState(DaqServerManager::Running);
    m_serverManager.daqInitializationComplete();

    while (m_dataPacketQueue.size() > 0 || !m_serverManager.daqLoopStopRequested()) { 
      auto tmpQueue = m_dataPacketQueue.takeAll();

      // takeAll blocks for at most 1 s, so if no data is present, continue.
      if (tmpQueue.empty()) {
        continue;
      }

      builder->append(tmpQueue);
    }
    
    builder->finalize();
    processors.finalize();
    writer.finalize();
  }
  else {
    DaqMonitorPtr daqMonitor = std::make_shared<DaqMonitor>(m_serverManager.getBoardIds());
    m_serverManager.daqMonitor(daqMonitor);
    
    std::unique_ptr<PacketWriter> writer = std::make_unique<PacketWriterBin>(m_serverManager.runPath(), m_serverManager.packetWriterSettings());
    m_serverManager.daqState(DaqServerManager::Running);
    m_serverManager.daqInitializationComplete();

    while (m_dataPacketQueue.size() > 0 || !m_serverManager.daqLoopStopRequested()) { 
      auto tmpQueue = m_dataPacketQueue.takeAll();

      // takeAll blocks for at most 1 s, so if no data is present, continue.
      if (tmpQueue.empty()) {
        continue;
      }

      for (size_t i = 0; i < tmpQueue.size(); i++) {
        writer->append(tmpQueue.front());
        tmpQueue.pop_front();
      }
    }
  }

}


std::unique_ptr<EventBuilder> DaqWrapper::getEventBuilder(EventQueue& eventQueue) {
  std::string eventBuilderName;
  try {
    eventBuilderName = m_serverManager.eventBuilderSettings().at("builder").get<std::string>();
  }
  catch (const json::out_of_range& e) {
    THROW_RE("Event builder was not specified in the configuration.");
  }

  if (eventBuilderName == "trigless") {
    return std::make_unique<TriglessEventBuilder>(std::ref(eventQueue), std::ref(m_serverManager));
  }
  else if (eventBuilderName == "trigger") {
    return std::make_unique<TriggerEventBuilder>(std::ref(eventQueue), std::ref(m_serverManager));
  }
  else {
    THROW_RE("Unknown event builder '{}'", eventBuilderName);
  }
}

void DaqWrapper::saveConfiguration() {
  // function to retrieve all the keys in a json object
  auto getKeys = [] (const json& j) {
    std::set<std::string> ret;
    for (auto it = j.begin(); it != j.end(); ++it) {
      ret.insert(it.key());
    }
    return ret;
  };

  auto buildChannelKeys = [getKeys] (json conf, std::set<std::string>& keys) {
    for (auto it = conf.begin(); it != conf.end(); ++it) {
      for (auto it_c = (*it).begin(); it_c != (*it).end(); ++it_c) {
        auto channelsKeys = getKeys(*(it_c));
        keys.insert(channelsKeys.begin(), channelsKeys.end());
      }
    }
  };

  auto buildTdcConfKeys = [getKeys] (json conf, std::set<std::string>& keys) {
    for (auto it = conf.begin(); it != conf.end(); ++it) {
      for (auto it_c = (*it).begin(); it_c != (*it).end(); ++it_c) {
         for (auto it_tac = (*it_c).begin(); it_tac != (*it_c).end(); ++it_tac) {
          for (auto it_tdc = (*it_tac).begin(); it_tdc != (*it_tac).end(); ++it_tdc) {
            auto tdcKeys = getKeys(*(it_tdc));
            keys.insert(tdcKeys.begin(), tdcKeys.end());
          }
        }
      }
    }
  };

  auto buildQdcConfKeys = [getKeys] (json conf, std::set<std::string>& keys) {
    for (auto it = conf.begin(); it != conf.end(); ++it) {
      for (auto it_c = (*it).begin(); it_c != (*it).end(); ++it_c) {
         for (auto it_tac = (*it_c).begin(); it_tac != (*it_c).end(); ++it_tac) {
          auto tacsKeys = getKeys(*(it_tac));
          keys.insert(tacsKeys.begin(), tacsKeys.end());
        }
      }
    }
  };

  auto confPath = m_serverManager.runPath();

  if (m_serverManager.boardMapping().is_null()) {
    WARN("No board mapping has been provided");
  }
  else {
    INFO("Saving board mapping");
    auto boardMappingPath = confPath / "board_mapping.json";
    std::ofstream boardMappingF(boardMappingPath);
    if (!boardMappingF) {
      THROW_SE(errno, "Failed to open file {}", boardMappingPath);
    }
    fmt::print(boardMappingF, "{}", m_serverManager.boardMapping());
  }

  if (m_serverManager.systemConfiguration().is_null()) {
    WARN("No system configuration has been provided");
  }
  else {
    INFO("Saving system configuration");
    auto systemConfigurationPath = confPath / "configuration.json";
    std::ofstream systemConfigurationF(systemConfigurationPath);
    if (!systemConfigurationF) {
      THROW_SE(errno, "Failed to open file {}", systemConfigurationPath);
    }
    fmt::print(systemConfigurationF, "{}", m_serverManager.systemConfiguration());
  }

  const auto& boardsConf = m_serverManager.boardsConfiguration();

  if (boardsConf.is_null()) {
    WARN("No boards configuration has been provided");
    return;
  }
  
  INFO("Saving boards configuration");


  std::set<std::string> boardParameters;
  std::set<std::string> feParameters;
  std::set<std::string> channelsParameters;
  std::set<std::string> tdcCalParameters;
  std::set<std::string> qdcCalParameters;

  //TODO put checks that the files are correctly open

  // first loop, to determine the parameters that will be saved
  for (auto it_b = boardsConf.begin(); it_b != boardsConf.end(); ++it_b) {
    auto boardConf = (*it_b).at("board").get<json>();
    auto feConf = (*it_b).at("fe").get<json>();
    auto channelsConf = (*it_b).at("channels").get<json>();
    auto tdcCal = (*it_b).at("tdc_cal").get<json>();
    auto qdcCal = (*it_b).at("qdc_cal").get<json>();

    // find all keys in boards configuration
    auto boardKeys = getKeys(boardConf);
    boardParameters.insert(boardKeys.begin(), boardKeys.end());

    // find all the keys in the fe configurations (in principle they can vary between frontends)
    for (auto it_f = feConf.begin(); it_f != feConf.end(); ++it_f) {
      auto feKeys = getKeys(*(it_f));
      feParameters.insert(feKeys.begin(), feKeys.end());
    }

    // find all the keys in the channels configurations
    // for (auto it_f = channelsConf.begin(); it_f != channelsConf.end(); ++it_f) {
    //   for (auto it_c = (*it_f).begin(); it_c != (*it_f).end(); ++it_c) {
    //     auto channelsKeys = getKeys(*(it_c));
    //     channelsParameters.insert(channelsKeys.begin(), channelsKeys.end());
    //   }
    // }
    buildChannelKeys(channelsConf, channelsParameters);
    buildTdcConfKeys(tdcCal, tdcCalParameters);
    buildQdcConfKeys(qdcCal, qdcCalParameters);
  }

  // open files and write the headers
  auto boardConfPath = confPath / "boards.csv";
  auto feConfPath = confPath / "fe.csv";
  auto channelsConfPath = confPath / "channels.csv";
  auto tdcCalPath = confPath / "tdc_cal.csv";
  auto qdcCalPath = confPath / "qdc_cal.csv";

  std::ofstream boardConfF(boardConfPath);
  if (!boardConfF) {
    THROW_SE(errno, "Failed to open file {}", boardConfPath);
  }
  std::ofstream feConfF(feConfPath);
  if (!feConfF) {
    THROW_SE(errno, "Failed to open file {}", feConfPath);
  }
  std::ofstream channelsConfF(channelsConfPath);
  if (!channelsConfF) {
    THROW_SE(errno, "Failed to open file {}", channelsConfPath);
  }
  std::ofstream tdcCalF(tdcCalPath);
  if (!tdcCalF) {
    THROW_SE(errno, "Failed to open file {}", tdcCalPath);
  }
  std::ofstream qdcCalF(qdcCalPath);
  if (!qdcCalF) {
    THROW_SE(errno, "Failed to open file {}", qdcCalPath);
  }

  boardConfF << "board_id";
  for (const auto& k : boardParameters) {
    boardConfF << "," << k;
  }
  boardConfF << std::endl;

  feConfF << "board_id" << "," << "fe_id";
  for (const auto& k : feParameters) {
    feConfF << "," << k;
  }
  feConfF << std::endl;

  auto writeChannelsHeader = [] (std::ofstream& os, const std::set<std::string>& params) {
    os << "board_id" << "," << "fe_id" << "," << "channel";
    for (const auto& k : params) {
      os << "," << k;
    }
    os << std::endl;
  };

  auto writeQdcConfHeader = [] (std::ofstream& os, const std::set<std::string>& params) {
    os << "board_id" << "," << "fe_id" << "," << "channel" << "," << "tac";
    for (const auto& k : params) {
      os << "," << k;
    }
    os << std::endl;
  };

  auto writeTdcConfHeader = [] (std::ofstream& os, const std::set<std::string>& params) {
    os << "board_id" << "," << "fe_id" << "," << "channel" << "," << "tac" << "," << "tdc";
    for (const auto& k : params) {
      os << "," << k;
    }
    os << std::endl;
  };

  writeChannelsHeader(channelsConfF, channelsParameters);
  writeTdcConfHeader(tdcCalF, tdcCalParameters);
  writeQdcConfHeader(qdcCalF, qdcCalParameters);

  // channelsConfF << "board_id" << "," << "fe_id" << "," << "channel";
  // for (const auto& k : channelsParameters) {
  //   channelsConfF << "," << k;
  // }
  // channelsConfF << std::endl;

  // loop over the boards and write the configurations
  for (auto it_b = boardsConf.begin(); it_b != boardsConf.end(); ++it_b) {
    auto boardConf = (*it_b).at("board").get<json>();
    auto feConf = (*it_b).at("fe").get<json>();
    auto channelsConf = (*it_b).at("channels").get<json>();
    auto tdcCal = (*it_b).at("tdc_cal").get<json>();
    auto qdcCal = (*it_b).at("qdc_cal").get<json>();

    auto boardId = it_b.key();

    // write boards configuration
    {
      std::stringstream ss;
      ss << boardId;
      for (const auto& k : boardParameters) {
        ss << ",";
        try {
          ss << boardConf.at(k);
        }
        catch (const json::out_of_range& e) {}
      }
      boardConfF << ss.str() << std::endl;
    }

    // write FEs configuration
    for (auto it_f = feConf.begin(); it_f != feConf.end(); ++it_f) {
      auto feId = it_f.key();
      std::stringstream ss;
      ss << boardId << "," << feId;
      for (const auto& k : feParameters) {
        ss << ",";
        try {
          ss << (*it_f).at(k);
        }
        catch (const json::out_of_range& e) {}
      }
      feConfF << ss.str() << std::endl;
    }

    auto writeChannelsConf = [boardId] (std::ofstream& os, const std::set<std::string>& params, const json& conf) {
      for (auto it_f = conf.begin(); it_f != conf.end(); ++it_f) {
        auto feId = it_f.key();
        for (auto it_c = (*it_f).begin(); it_c != (*it_f).end(); ++it_c) {
          auto ch = it_c.key();
          std::stringstream ss;
          ss << boardId << "," << feId << "," << ch;
          for (const auto& k : params) {
          ss << ",";
            try {
              ss << (*it_c).at(k);
            }
            catch (const json::out_of_range& e) {}
          }
          os << ss.str() << std::endl;
        }
      }
    };

    auto writeTdcCal = [boardId] (std::ofstream& os, const std::set<std::string>& params, const json& conf) {
      for (auto it_f = conf.begin(); it_f != conf.end(); ++it_f) {
        auto feId = it_f.key();
        for (auto it_c = (*it_f).begin(); it_c != (*it_f).end(); ++it_c) {
          auto ch = it_c.key();
          for (auto it_tac = (*it_c).begin(); it_tac != (*it_c).end(); ++it_tac) {
            auto tac = it_tac.key();
            for (auto it_tdc = (*it_tac).begin(); it_tdc != (*it_tac).end(); ++it_tdc) {
              auto tdc = it_tdc.key();
              std::stringstream ss;
              ss << boardId << "," << feId << "," << ch << "," << tac << "," << tdc ;
              for (const auto& k : params) {
              ss << ",";
                try {
                  ss << (*it_tdc).at(k);
                }
                catch (const json::out_of_range& e) {}
              }
              os << ss.str() << std::endl;
            }
          }
        }
      }
    };

    auto writeQdcCal = [boardId] (std::ofstream& os, const std::set<std::string>& params, const json& conf) {
      for (auto it_f = conf.begin(); it_f != conf.end(); ++it_f) {
        auto feId = it_f.key();
        for (auto it_c = (*it_f).begin(); it_c != (*it_f).end(); ++it_c) {
          auto ch = it_c.key();
          for (auto it_t = (*it_c).begin(); it_t != (*it_c).end(); ++it_t) {
            auto tac = it_t.key();
            std::stringstream ss;
            ss << boardId << "," << feId << "," << ch << "," << tac;
            for (const auto& k : params) {
            ss << ",";
              try {
                ss << (*it_t).at(k);
              }
              catch (const json::out_of_range& e) {}
            }
            os << ss.str() << std::endl;
          }
        }
      }
    };

    writeChannelsConf(channelsConfF, channelsParameters, channelsConf);
    writeTdcCal(tdcCalF, tdcCalParameters, tdcCal);
    writeQdcCal(qdcCalF, qdcCalParameters, qdcCal);

    // write channels configuration
    // for (auto it_f = channelsConf.begin(); it_f != channelsConf.end(); ++it_f) {
    //   auto feId = it_f.key();
    //   for (auto it_c = (*it_f).begin(); it_c != (*it_f).end(); ++it_c) {
    //     auto ch = it_c.key();
    //     std::stringstream ss;
    //     ss << boardId << "," << feId << "," << ch;
    //     for (const auto& k : channelsParameters) {
    //     ss << ",";
    //       try {
    //         ss << (*it_c).at(k);
    //       }
    //       catch (const json::out_of_range& e) {}
    //     }
    //     channelsConfF << ss.str() << std::endl;
    //   }
    // }
  }

}