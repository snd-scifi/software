#pragma once
#include <atomic>
#include <string>
#include <mutex>
#include <condition_variable>
#include <map>
#include <vector>
#include <filesystem>

#include "daq/daq_wrapper.hpp"
#include "monitoring/daq_monitor.hpp"

#include "fmt/core.h"
#include "fmt/format.h"

#include "json.hpp"

/**
 * Status of the DAQ thread of each board
 */
struct DaqThreadStatus {
  std::atomic_bool connected{false};
};

/**
 * Class to manage the DAQ server.
 * An instance of this class in created in the main and its reference is passed to all the threads of the DAQ server.
 */
class DaqServerManager {
public:
  enum DaqState {
    Idle,
    Starting,
    Running,
    Stopping,
    Error
  };
  DaqServerManager();

  // setup during Idle
  void setBoardIds(const std::vector<uint32_t> ids);
  std::vector<uint32_t> getBoardIds() const;
  bool validBoardId(uint32_t id) const { return m_connectedBoardsStatus.count(id); }

  std::filesystem::path dataPath() const;
  std::filesystem::path runPath() const { return dataPath() / fmt::format("run_{:06d}", runNumber()); }

  void runNumber(const uint32_t runNumber);
  uint32_t runNumber() const;

  void daqServerSettings(const nlohmann::json daqServerSettings);
  const nlohmann::json& daqServerSettings() const;
  const nlohmann::json& eventBuilderSettings() const;
  const nlohmann::json& eventProcessorsSettings() const;
  const nlohmann::json& eventWriterSettings() const;
  const nlohmann::json& packetWriterSettings() const;

  void boardMapping(const nlohmann::json boardMapping);
  const nlohmann::json& boardMapping() const;

  void systemConfiguration(const nlohmann::json systemConfiguration);
  const nlohmann::json& systemConfiguration() const;

  void boardsConfiguration(const nlohmann::json boardsConfiguration);
  const nlohmann::json& boardsConfiguration() const;

  void userInfo(const nlohmann::json userInfo);
  void updateUserInfo(const nlohmann::json userInfo);
  const nlohmann::json& userInfo() const;

  void daqState(const DaqState state);
  DaqState daqState() const;

  bool stopRequested() const;
  bool daqLoopStopRequested() const;
  bool overwriteData() const { return m_overwrite.load(); }

  DaqThreadStatus& daqThreadStatus(uint32_t id);

  DaqState startDaq(bool overwrite=false);
  void daqInitializationComplete();
  void startupComplete();
  void daqStopComplete();
  void stopDaq();
  void stopDaqLoop();

  void waitForDaqStart();
  void waitForDaqInitialization();

  void resetDaqFlags();

  void requestDaqServerStop();
  bool daqServerStopRequested();

  void setStopTime();
  void setEndTime();

  std::filesystem::space_info diskSpaceInfo() const;
  uint64_t runSizeInfo() const;

  DaqMonitorPtr daqMonitor();
  void daqMonitor(DaqMonitorPtr monitor);

  void addRunStatusElements(nlohmann::json elements);
  void writeRunStatus();

private:
  std::atomic_uint32_t m_runNumber;
  std::vector<uint32_t> m_boardIds;
  std::map<uint32_t, std::shared_ptr<DaqThreadStatus>> m_connectedBoardsStatus;

  std::atomic_bool m_startDaq;
  std::atomic_bool m_overwrite;
  std::atomic_bool m_daqLoopReady;
  std::atomic_bool m_stopDaq;
  std::atomic_bool m_stopDaqLoop;
  std::atomic_bool m_stopDaqServer;

  std::atomic<DaqState> m_state;
  
  mutable std::mutex m_mutex;
  std::condition_variable m_condvarStart;
  std::condition_variable m_condvarDaqLoop;
  std::condition_variable m_condvarStartupComplete;
  std::condition_variable m_condvarStop;

  nlohmann::json m_daqServerSettings;
  nlohmann::json m_boardMapping;
  nlohmann::json m_systemConfiguration;
  nlohmann::json m_boardsConfiguration;
  nlohmann::json m_userInfo;
  nlohmann::json m_runStatus;

  DaqMonitorPtr m_daqMonitor;
};

NLOHMANN_JSON_SERIALIZE_ENUM(DaqServerManager::DaqState, {
    {DaqServerManager::Idle, "idle"},
    {DaqServerManager::Starting, "starting"},
    {DaqServerManager::Running, "running"},
    {DaqServerManager::Stopping, "stopping"},
    {DaqServerManager::Error, "error"},
})