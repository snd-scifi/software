#include <fstream>

#include "utils/logger.hpp"
#include "utils/time.hpp"

#include "daq_server_manager.hpp"

using json = nlohmann::json;

namespace fs = std::filesystem;

DaqServerManager::DaqServerManager()
  : m_runNumber(0)
  , m_startDaq(false)
  , m_overwrite(false)
  , m_daqLoopReady(false)
  , m_stopDaq(false)
  , m_stopDaqLoop(false)
  , m_stopDaqServer(false)
  , m_state(Idle)
  , m_mutex()
  , m_condvarStart()
  , m_condvarDaqLoop()
  {
    // for some reason, using m_daqServerSettings{json({})} above doesn't work, it becomeas an array and not an object...
    m_daqServerSettings = json({});
  }

/**
 * Set the board IDs expected by the server.
 * Practically, it fills a map with the DaqThreadStatus corresponding to each board ID.
 * Can be changed only when the server is in idle state.
 */
void DaqServerManager::setBoardIds(const std::vector<uint32_t> ids) {
  if (m_state != Idle) {
    THROW_RE("Cannot change board ids when DAQ is running");
  }

  std::lock_guard<std::mutex> lock(m_mutex);
  m_boardIds = ids;
  m_connectedBoardsStatus.clear();
  for (auto id : ids) {
    m_connectedBoardsStatus.emplace(id, std::make_shared<DaqThreadStatus>());
  }
  INFO("Board IDs set to {}", ids);
}

/**
 * Returns the board IDs expected by the server.
 */
std::vector<uint32_t> DaqServerManager::getBoardIds() const {
  return m_boardIds;
}

/**
 * Returns the current data path.
 */
fs::path DaqServerManager::dataPath() const {
  std::lock_guard<std::mutex> lock(m_mutex);
  return m_daqServerSettings.at("data_path").get<fs::path>();
}

/**
 * Set the run number, used to identify the folder where data is saved.
 * Can be changed only when the server is in idle state.
 */
void DaqServerManager::runNumber(const uint32_t runNumber) {
  if (m_state != Idle) {
    THROW_RE("Cannot change run number when DAQ is running");
  }

  m_runNumber = runNumber;
  INFO("Set run number to {}", runNumber);
}

/**
 * Returns the run number.
 */
uint32_t DaqServerManager::runNumber() const {
  return m_runNumber;
}

/**
 * Sets the DAQ server settings.
 */
void DaqServerManager::daqServerSettings(const json daqServerSettings) {
  if (m_state != Idle) {
    THROW_RE("Cannot change DAQ server settings when DAQ is running");
  }

  std::lock_guard<std::mutex> lock(m_mutex);
  m_daqServerSettings = daqServerSettings;
  INFO("Set DAQ server settings to {}", m_daqServerSettings);
}


/**
 * Returns the current DAQ server settings.
 */
const json& DaqServerManager::daqServerSettings() const {
  std::lock_guard<std::mutex> lock(m_mutex);
  return m_daqServerSettings;
}

const json& DaqServerManager::eventBuilderSettings() const {
  std::lock_guard<std::mutex> lock(m_mutex);
  try {
    return m_daqServerSettings.at("event_builder");
  }
  catch (const json::out_of_range& e) {
    THROW_RE("Configuration for event builder has not been provided.");
  }
}

const json& DaqServerManager::eventProcessorsSettings() const {
  std::lock_guard<std::mutex> lock(m_mutex);
  try {
    return m_daqServerSettings.at("event_processors");
  }
  catch (const json::out_of_range& e) {
    THROW_RE("Configuration for event processors has not been provided.");
  }
}

const json& DaqServerManager::eventWriterSettings() const {
  std::lock_guard<std::mutex> lock(m_mutex);
  try {
    return m_daqServerSettings.at("event_writer");
  }
  catch (const json::out_of_range& e) {
    THROW_RE("Configuration for event writer has not been provided.");
  }
}

const json& DaqServerManager::packetWriterSettings() const {
  std::lock_guard<std::mutex> lock(m_mutex);
  try {
    return m_daqServerSettings.at("packet_writer");
  }
  catch (const json::out_of_range& e) {
    THROW_RE("Configuration for packet writer has not been provided.");
  }
}

/**
 * Sets the board mapping.
 */
void DaqServerManager::boardMapping(const json boardMapping) {
  if (m_state != Idle) {
    THROW_RE("Cannot change board mapping when DAQ is running");
  }

  std::lock_guard<std::mutex> lock(m_mutex);
  m_boardMapping = boardMapping;
  DEBUG("Set board mapping to {}", m_boardMapping);
}

/**
 * Returns the current board mapping.
 */
const json& DaqServerManager::boardMapping() const {
  std::lock_guard<std::mutex> lock(m_mutex);
  return m_boardMapping;
}

/**
 * Sets the system configuration.
 */
void DaqServerManager::systemConfiguration(const json systemConfiguration) {
  if (m_state != Idle) {
    THROW_RE("Cannot change system configuration when DAQ is running");
  }

  std::lock_guard<std::mutex> lock(m_mutex);
  m_systemConfiguration = systemConfiguration;
  DEBUG("Set system configuration to {}", m_systemConfiguration);
}

/**
 * Returns the current system configuration.
 */
const json& DaqServerManager::systemConfiguration() const {
  std::lock_guard<std::mutex> lock(m_mutex);
  return m_systemConfiguration;
}

/**
 * Sets the boards configuration.
 */
void DaqServerManager::boardsConfiguration(const json boardsConfiguration) {
  if (m_state != Idle) {
    THROW_RE("Cannot change boards configuration when DAQ is running");
  }

  std::lock_guard<std::mutex> lock(m_mutex);
  m_boardsConfiguration = boardsConfiguration;
  DEBUG("Set boards configuration to {}", m_boardsConfiguration);
}

/**
 * Returns the current boards configuration.
 */
const json& DaqServerManager::boardsConfiguration() const {
  std::lock_guard<std::mutex> lock(m_mutex);
  return m_boardsConfiguration;
}

/**
 * Sets the user info.
 */
void DaqServerManager::userInfo(const json userInfo) {
  std::lock_guard<std::mutex> lock(m_mutex);
  m_userInfo = userInfo;
  INFO("Set user info to {}", m_userInfo);
}

/**
 * Sets the user info.
 */
void DaqServerManager::updateUserInfo(const json userInfo) {
  std::lock_guard<std::mutex> lock(m_mutex);
  for (const auto& [key, val] : userInfo.items()) {
    m_userInfo[key] = val;
  }
  INFO("Set user info to {}", m_userInfo);
}

/**
 * Returns the current user info.
 */
const json& DaqServerManager::userInfo() const {
  std::lock_guard<std::mutex> lock(m_mutex);
  return m_userInfo;
}

/**
 * Sets the DAQ state.
 */
void DaqServerManager::daqState(const DaqServerManager::DaqState state) {
  m_state = state;
}

/**
 * Returns the current DAQ state.
 */
DaqServerManager::DaqState DaqServerManager::daqState() const {
  return m_state.load();
}

/**
 * Returns true if a DAQ stop has been requested.
 * It is an indication for the DAQ server to stop accepting connections from the daq boards.
 */
bool DaqServerManager::stopRequested() const {
  return m_stopDaq.load();
}

/**
 * Returns true if the DAQ loop needs to stop.
 * It is checked as a condition in the DAQ loop loop.
 */
bool DaqServerManager::daqLoopStopRequested() const {
  return m_stopDaqLoop.load();
}

/**
 * Returns a reference to the DaqThreadStatus of a given board.
 */
DaqThreadStatus& DaqServerManager::daqThreadStatus(uint32_t id) { 
  return *(m_connectedBoardsStatus.at(id));
}

/**
 * Called by the command thread to request the DAQ to start.
 */
DaqServerManager::DaqState DaqServerManager::startDaq(bool overwrite) {
  if (m_state != Idle) {
    THROW_RE("Cannot start DAQ when DAQ is running");
  }

  NOTICE("DAQ start requested.");
  m_startDaq = true;
  m_overwrite = overwrite;
  m_condvarStart.notify_all();
  daqState(DaqServerManager::Starting);


  const auto timestampsPath = runPath() / "run_timestamps.json";
  std::unique_lock<std::mutex> lock(m_mutex);
  m_condvarStartupComplete.wait(lock, [this] () { return (this->m_state).load() != Starting; });

  if (m_state == Running) {
    auto currentTime = getCurrentTime();
    m_daqMonitor->startTime(currentTime);
    json jsonTime;
    jsonTime["start_time"] = getTimeStringUTC(currentTime);
    std::ofstream of(timestampsPath);
    of << jsonTime << std::endl;
    INFO("Set run start time to {}.", jsonTime.at("start_time"));
  }
  
  
  return m_state;
}

/**
 * Called by the DAQ loop when it is ready to receive data or there has been an error in the initialization.
 * The DAQ loop must also change the DaqState to Running or Error accordingly.
 */
void DaqServerManager::daqInitializationComplete() {
  INFO("DAQ initialization completed.");
  m_daqLoopReady = true;
  m_condvarDaqLoop.notify_all();
}

/**
 * Called by the main loop when the startup is complete, either successful or not.
 * Used to notify the DaqCmdServer that the startup is finished and it can return from startDaq.
 */
void DaqServerManager::startupComplete() {
  m_condvarStartupComplete.notify_all();
}

/**
 * Called by the main loop when the DAQ is stopped.
 * Used to notify the DaqCmdServer that the stoppin is finished and it can return from stopDaq.
 */
void DaqServerManager::daqStopComplete() {
  m_condvarStop.notify_all();
  setEndTime();
}

/**
 * Blocks until startDaq() is called by the command thread, then returns.
 */
void DaqServerManager::waitForDaqStart() {
  NOTICE("Waiting for DAQ start command...");
  std::unique_lock<std::mutex> lock(m_mutex);
  m_condvarStart.wait(lock, [this] () { return (this->m_startDaq).load(); });
  NOTICE("DAQ starting...");
}

/**
 * Blocks until daqInitializationComplete() is called by the daq runner thread.
 */
void DaqServerManager::waitForDaqInitialization() {
  INFO("Waiting for DAQ initialization...");
  std::unique_lock<std::mutex> lock(m_mutex);
  m_condvarDaqLoop.wait(lock, [this] () { return (this->m_daqLoopReady).load(); });
}

/**
 * Called by the command thread to request the DAQ to stop.
 */
void DaqServerManager::stopDaq() {
  if (m_state == Idle) {
    THROW_RE("Cannot stop DAQ when DAQ is not running");
  }

  NOTICE("DAQ stop requested.");
  m_stopDaq = true;

  setStopTime();
  // TODO add the option not to block
  {
    std::unique_lock<std::mutex> lock(m_mutex);
    m_condvarStop.wait(lock, [this] () { return (this->m_state).load() != Running; });
  }
  
}

/**
 * Called by the main thread to request the DAQ loop to stop, after all the DAQ threads have finished.
 */
void DaqServerManager::stopDaqLoop() {
  m_stopDaqLoop = true;
}

/**
 * Used at the end of a run to reset the status of the DaqServerManager without changing data path, run number, etc.
 */
void DaqServerManager::resetDaqFlags() {
  m_startDaq = false;
  m_stopDaq = false;
  m_daqLoopReady = false;
  m_stopDaqLoop = false;
  daqState(Idle);
  std::unique_lock<std::mutex> lock(m_mutex);
  // m_daqServerSettings.clear();
  // m_boardMapping.clear();
  // m_systemConfiguration.clear();
  // m_boardsConfiguration.clear();
  // m_userInfo.clear();
  m_runStatus.clear();
}

void DaqServerManager::requestDaqServerStop() {
  m_stopDaqServer = true;
}

bool DaqServerManager::daqServerStopRequested() {
  return m_stopDaqServer.load();
}

void DaqServerManager::setStopTime() {
  const auto infoRunPath = runPath() / "run_timestamps.json";
  const auto currentTime = getCurrentTime();
  m_daqMonitor->stopTime(currentTime);
  std::unique_lock<std::mutex> lock(m_mutex);
  std::ifstream i(infoRunPath);
  auto jsonTime = json::parse(i);
  i.close();
  jsonTime["stop_time"] = getTimeStringUTC(currentTime);
  std::ofstream of(infoRunPath);
  of << jsonTime << std::endl;
  INFO("Set run stop time to {}.", jsonTime.at("stop_time"));
}

void DaqServerManager::setEndTime() {
  const auto infoRunPath = runPath() / "run_timestamps.json";
  const auto currentTime = getCurrentTime();
  m_daqMonitor->endTime(currentTime);
  std::unique_lock<std::mutex> lock(m_mutex);
  std::ifstream i(infoRunPath);
  auto jsonTime = json::parse(i);
  i.close();
  jsonTime["end_time"] = getTimeStringUTC(currentTime);
  std::ofstream of(infoRunPath);
  of << jsonTime << std::endl;
  INFO("Set run end time to {}.", jsonTime.at("end_time"));
}

fs::space_info DaqServerManager::diskSpaceInfo() const {
  try {
    return fs::space(dataPath());
  }
  catch (const fs::filesystem_error& e) {
    WARN("Could not get disk space info: {}", e.what());
    return {0, 0, 0};
  }
}

uint64_t DaqServerManager::runSizeInfo() const {
  try {
    uint64_t result = 0; 
    for (const auto& path : fs::directory_iterator(runPath())) {
        result += fs::file_size(path);
    }
    return result;
  }
  catch (const fs::filesystem_error& e) {
    WARN("Could not get run space info: {}", e.what());
    return 0;
  }
}

DaqMonitorPtr DaqServerManager::daqMonitor() {
  return m_daqMonitor;
}

void DaqServerManager::daqMonitor(DaqMonitorPtr monitor) {
  m_daqMonitor = monitor;
}

void DaqServerManager::addRunStatusElements(json elements) {
  // TODO check type?
  for (auto it = elements.begin(); it != elements.end(); ++it) {
    m_runStatus[it.key()] = it.value();
  }
}

void DaqServerManager::writeRunStatus() {
  if (!m_daqServerSettings.value("write_run_status", false)) {
    return;
  }

  m_runStatus["timestamp"] = getCurrentTimeUTC();
  if (m_daqMonitor) {
    m_runStatus["start_time"] = m_daqMonitor->toJson().at("start_time");
  }
  m_runStatus["state"] = daqState();
  m_runStatus["run_number"] = runNumber();

  std::ofstream runStatusFile(dataPath() / m_daqServerSettings.value("run_status_file_name", "run_status.json"));
  runStatusFile << m_runStatus << std::endl;
}
