#include "daq_mon_evt_trig_writer.hpp"

DaqMonitorEvtTrigWriter::DaqMonitorEvtTrigWriter(const std::vector<uint32_t> boardIds)
  : DaqMonitor(boardIds)
  // , m_processedTriggers{0}
  // , m_globalEventQueueLength{0}
  // , m_threadEventQueueLength{0}
  // , m_globalProcessedEventQueueLength{0}
  // , m_threadProcessedEventQueueLength{0}
  , m_writtenHits{0}
  , m_discardedHits{0}
  , m_writtenEvents{0}
  , m_discardedEvents{0}
  // , m_sync{true}
{
  // std::lock_guard<std::mutex> lock{m_mutex};
  // for (const auto bid : boardIds) {
  //   m_globalHitQueuesLength[std::to_string(bid)] = 0;
  //   m_threadHitQueuesLength[std::to_string(bid)] = 0;
  //   m_globalBoardEventQueuesLength[std::to_string(bid)] = 0;
  //   m_threadBoardEventQueuesLength[std::to_string(bid)] = 0;
  //   m_triggerQueuesLength[std::to_string(bid)] = 0;
  //   m_missedTriggers[std::to_string(bid)] = 0;
  // }
}

json DaqMonitorEvtTrigWriter::toJson() const {
  auto ret = DaqMonitor::toJson();
  std::lock_guard<std::mutex> lock{m_mutex};
  // ret["last_timestamp"] = m_lastTimestamp;
  // ret["global_hit_queues_length"] = m_globalHitQueuesLength;
  // ret["thread_hit_queues_length"] = m_threadHitQueuesLength;
  // ret["global_board_event_queues_length"] = m_globalBoardEventQueuesLength;
  // ret["thread_board_event_queues_length"] = m_threadBoardEventQueuesLength;
  // ret["trigger_queues_length"] = m_triggerQueuesLength;
  // ret["missed_triggers"] = m_missedTriggers;
  // ret["global_hit_queues_length"] = json();
  // for(const auto& [k, v] : m_globalHitQueuesLength) {
  //   ret["global_hit_queues_length"][k] = v.load();
  // }
  // ret["thread_hit_queues_length"] = json();
  // for(const auto& [k, v] : m_threadHitQueuesLength) {
  //   ret["thread_hit_queues_length"][k] = v.load();
  // }
  // ret["global_board_event_queues_length"] = json();
  // for(const auto& [k, v] : m_globalBoardEventQueuesLength) {
  //   ret["global_board_event_queues_length"][k] = v.load();
  // }
  // ret["thread_board_event_queues_length"] = json();
  // for(const auto& [k, v] : m_threadBoardEventQueuesLength) {
  //   ret["thread_board_event_queues_length"][k] = v.load();
  // }
  // ret["trigger_queues_length"] = json();
  // for(const auto& [k, v] : m_triggerQueuesLength) {
  //   ret["trigger_queues_length"][k] = v.load();
  // }
  // ret["missed_triggers"] = json();
  // for(const auto& [k, v] : m_missedTriggers) {
  //   ret["missed_triggers"][k] = v.load();
  // }

  // ret["processed_triggers"] = m_processedTriggers.load();
  // ret["global_event_queue_length"] = m_globalEventQueueLength.load();
  // ret["thread_event_queue_length"] = m_threadEventQueueLength.load();
  // ret["global_processed_event_queue_length"] = m_globalProcessedEventQueueLength.load();
  // ret["thread_processed_event_queue_length"] = m_threadProcessedEventQueueLength.load();
  ret["written_hits"] = m_writtenHits.load();
  ret["discarded_hits"] = m_discardedHits.load();
  ret["written_events"] = m_writtenEvents.load();
  ret["discarded_events"] = m_discardedEvents.load();
  // ret["sync"] = m_sync;
  // if (!m_sync) {
  //   ret["desync_board_ids"] = m_desyncBoardIds;
  //   ret["desync_info"] = m_desyncInfo;
  // }
  return ret;
}


// void DaqMonitorEvtTrigWriter::globalHitQueueLength(uint32_t id, size_t length) {
//   // std::lock_guard<std::mutex> lock{m_mutex};
//   m_globalHitQueuesLength.at(std::to_string(id)) = length;
// }

// void DaqMonitorEvtTrigWriter::threadHitQueueLength(uint32_t id, size_t length) {
//   // std::lock_guard<std::mutex> lock{m_mutex};
//   m_threadHitQueuesLength.at(std::to_string(id)) = length;
// }

// void DaqMonitorEvtTrigWriter::globalBoardEventQueueLength(uint32_t id, size_t length) {
//   // std::lock_guard<std::mutex> lock{m_mutex};
//   m_globalBoardEventQueuesLength.at(std::to_string(id)) = length;
// }

// void DaqMonitorEvtTrigWriter::threadBoardEventQueueLength(uint32_t id, size_t length) {
//   // std::lock_guard<std::mutex> lock{m_mutex};
//   m_threadBoardEventQueuesLength.at(std::to_string(id)) = length;
// }

// void DaqMonitorEvtTrigWriter::triggerQueueLength(uint32_t id, size_t length) {
//   // std::lock_guard<std::mutex> lock{m_mutex};
//   m_triggerQueuesLength.at(std::to_string(id)) = length;
// }

// void DaqMonitorEvtTrigWriter::increaseMissedTriggers(uint32_t id, size_t n) {
//   // std::lock_guard<std::mutex> lock{m_mutex};
//   m_missedTriggers.at(std::to_string(id)) += n;
// }

// void DaqMonitorEvtTrigWriter::increaseProcessedTriggers(size_t n) {
//   // std::lock_guard<std::mutex> lock{m_mutex};
//   m_processedTriggers += n;
// }

// void DaqMonitorEvtTrigWriter::globalEventQueueLength(size_t length) {
//   // std::lock_guard<std::mutex> lock{m_mutex};
//   m_globalEventQueueLength = length;
// }

// void DaqMonitorEvtTrigWriter::threadEventQueueLength(size_t length) {
//   // std::lock_guard<std::mutex> lock{m_mutex};
//   m_threadEventQueueLength = length;
// }

// void DaqMonitorEvtTrigWriter::globalProcessedEventQueueLength(size_t length) {
//   // std::lock_guard<std::mutex> lock{m_mutex};
//   m_globalProcessedEventQueueLength = length;
// }

// void DaqMonitorEvtTrigWriter::threadProcessedEventQueueLength(size_t length) {
//   // std::lock_guard<std::mutex> lock{m_mutex};
//   m_threadProcessedEventQueueLength = length;
// }

void DaqMonitorEvtTrigWriter::increaseWrittenHits(size_t n) {
  // std::lock_guard<std::mutex> lock{m_mutex};
  m_writtenHits += n;
}

void DaqMonitorEvtTrigWriter::increaseDiscardedHits(size_t n) {
  // std::lock_guard<std::mutex> lock{m_mutex};
  m_discardedHits += n;
}

void DaqMonitorEvtTrigWriter::increaseWrittenEvents(size_t n) {
  // std::lock_guard<std::mutex> lock{m_mutex};
  m_writtenEvents += n;
}

void DaqMonitorEvtTrigWriter::increaseDiscardedEvents(size_t n) {
  // std::lock_guard<std::mutex> lock{m_mutex};
  m_discardedEvents += n;
}

// void DaqMonitorEvtTrigWriter::syncStatus(bool sync, std::vector<uint32_t> desyncBoardIds, TrigMap map) {
//   std::lock_guard<std::mutex> lock{m_mutex};
//   m_sync = sync;
//   m_desyncBoardIds = desyncBoardIds;
//   m_desyncInfo = map;
// }
