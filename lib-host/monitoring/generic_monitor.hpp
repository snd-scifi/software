#pragma once

#include <mutex>
#include <string>
#include <vector>

#include "json.hpp"

class GenericMonitor {
public:
  GenericMonitor(std::string name = "generic_monitor");
  nlohmann::json toJson() const;
  std::string name() const { return m_name; }
  void addMonitor(std::shared_ptr<GenericMonitor> monitor);

protected:
  virtual nlohmann::json thisToJson() const =0;
  const std::string m_name;
  mutable std::mutex m_mutex;
  std::vector<std::shared_ptr<GenericMonitor>> m_monitors;
};

using GenericMonitorPtr = std::shared_ptr<GenericMonitor>;
