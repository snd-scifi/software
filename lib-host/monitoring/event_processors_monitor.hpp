#pragma once

#include <atomic>
#include <string>
#include <vector>

#include "monitoring/generic_monitor.hpp"
#include "utils/timer.hpp"

using TrigMap = std::map<uint32_t, std::pair<uint64_t, int64_t>>;

class EventProcessorsMonitor : public GenericMonitor
{
public:
  EventProcessorsMonitor(const std::string name = "event_processors_monitor");

  void localEventQueueLength(size_t length) { m_localEventQueueLength = length; }

  void increaseProcessedHits(size_t n = 1);
  void increaseDiscardedHits(size_t n = 1);
  
  void increaseProcessedEvents(size_t n = 1);
  void increaseDiscardedEvents(size_t n = 1);
  

private:
  nlohmann::json thisToJson() const override;

  std::atomic_size_t m_localEventQueueLength;

  std::atomic_size_t m_processedHits;
  std::atomic_size_t m_discardedHits;

  std::atomic_size_t m_processedEvents;
  std::atomic_size_t m_discardedEvents;

  std::atomic<float> m_processedHitRate;
  std::atomic_size_t m_lastProcessedHits;
  Timer m_processedHitRateTimer;

  std::atomic<float> m_processedEventRate;
  std::atomic_size_t m_lastProcessedEvents;
  Timer m_processedEventRateTimer;

  std::atomic<float> m_discardedHitRate;
  std::atomic_size_t m_lastDiscardedHits;
  Timer m_discardedHitRateTimer;

  std::atomic<float> m_discardedEventRate;
  std::atomic_size_t m_lastDiscardedEvents;
  Timer m_discardedEventRateTimer;

};

using EventProcessorsMonitorPtr = std::shared_ptr<EventProcessorsMonitor>;
