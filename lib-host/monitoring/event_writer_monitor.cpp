#include "monitoring/utils.hpp"

#include "event_writer_monitor.hpp"

using json = nlohmann::json;

EventWriterMonitor::EventWriterMonitor(const std::string name)
  : GenericMonitor(name)
  , m_localEventQueueLength{0}
  , m_writtenHits{0}
  , m_writtenEvents{0}
  , m_writtenHitRate{0}
  , m_lastWrittenHits{0}
  , m_writtenEventRate{0}
  , m_lastWrittenEvents{0}
{}

json EventWriterMonitor::thisToJson() const {
  auto ret = json();

  ret["local_event_queue_length"] = m_localEventQueueLength.load();
  ret["written_hits"] = m_writtenHits.load();
  ret["written_events"] = m_writtenEvents.load();
  ret["written_hits_rate"] = m_writtenHitRate.load();
  ret["written_events_rate"] = m_writtenEventRate.load();
  
  return ret;
}

void EventWriterMonitor::increaseWrittenHits(size_t n) {
  m_writtenHits += n;
  calculateRate(m_writtenHits, m_lastWrittenHits, m_writtenHitRate, m_writtenHitRateTimer);
}

void EventWriterMonitor::increaseWrittenEvents(size_t n) {
  m_writtenEvents += n;
  calculateRate(m_writtenEvents, m_lastWrittenEvents, m_writtenEventRate, m_writtenEventRateTimer);
}
