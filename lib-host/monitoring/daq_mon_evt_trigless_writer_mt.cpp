#include "daq_mon_evt_trigless_writer_mt.hpp"

DaqMonitorEvtTriglessWriterMt::DaqMonitorEvtTriglessWriterMt(const std::vector<uint32_t> boardIds)
  : DaqMonitor(boardIds)
  , m_processedTriggers{0}
  , m_globalEventQueueLength{0}
  , m_threadEventQueueLength{0}
  , m_globalProcessedEventQueueLength{0}
  , m_threadProcessedEventQueueLength{0}
  , m_totalHits{0}
  , m_processedHits{0}
  , m_writtenHits{0}
  , m_discardedHits{0}
  , m_totalEvents{0}
  , m_processedEvents{0}
  , m_writtenEvents{0}
  , m_discardedEvents{0}
  , m_totalHitRate{0.}
  , m_lastTotalHits{0}
  , m_processedHitRate{0.}
  , m_lastProcessedHits{0}
  , m_writtenHitRate{0.}
  , m_lastWrittenHits{0}
  , m_discardedHitRate{0.}
  , m_lastDiscardedHits{0}
  , m_totalEventRate{0.}
  , m_lastTotalEvents{0}
  , m_processedEventRate{0.}
  , m_lastProcessedEvents{0}
  , m_writtenEventRate{0.}
  , m_lastWrittenEvents{0}
  , m_discardedEventRate{0.}
  , m_lastDiscardedEvents{0}
  , m_sync{true}
{
  std::lock_guard<std::mutex> lock{m_mutex};
  for (const auto bid : boardIds) {
    m_globalHitQueuesLength[std::to_string(bid)] = 0;
    m_threadHitQueuesLength[std::to_string(bid)] = 0;
    m_globalBoardEventQueuesLength[std::to_string(bid)] = 0;
    m_threadBoardEventQueuesLength[std::to_string(bid)] = 0;
    m_triggerQueuesLength[std::to_string(bid)] = 0;
    m_missedTriggers[std::to_string(bid)] = 0;
  }
}

json DaqMonitorEvtTriglessWriterMt::toJson() const {
  auto ret = DaqMonitor::toJson();
  std::lock_guard<std::mutex> lock{m_mutex};
  // ret["last_timestamp"] = m_lastTimestamp;
  // ret["global_hit_queues_length"] = m_globalHitQueuesLength;
  // ret["thread_hit_queues_length"] = m_threadHitQueuesLength;
  // ret["global_board_event_queues_length"] = m_globalBoardEventQueuesLength;
  // ret["thread_board_event_queues_length"] = m_threadBoardEventQueuesLength;
  // ret["trigger_queues_length"] = m_triggerQueuesLength;
  // ret["missed_triggers"] = m_missedTriggers;
  ret["global_hit_queues_length"] = json();
  for(const auto& [k, v] : m_globalHitQueuesLength) {
    ret["global_hit_queues_length"][k] = v.load();
  }
  ret["thread_hit_queues_length"] = json();
  for(const auto& [k, v] : m_threadHitQueuesLength) {
    ret["thread_hit_queues_length"][k] = v.load();
  }
  ret["global_board_event_queues_length"] = json();
  for(const auto& [k, v] : m_globalBoardEventQueuesLength) {
    ret["global_board_event_queues_length"][k] = v.load();
  }
  ret["thread_board_event_queues_length"] = json();
  for(const auto& [k, v] : m_threadBoardEventQueuesLength) {
    ret["thread_board_event_queues_length"][k] = v.load();
  }
  ret["trigger_queues_length"] = json();
  for(const auto& [k, v] : m_triggerQueuesLength) {
    ret["trigger_queues_length"][k] = v.load();
  }
  ret["missed_triggers"] = json();
  for(const auto& [k, v] : m_missedTriggers) {
    ret["missed_triggers"][k] = v.load();
  }

  ret["processed_triggers"] = m_processedTriggers.load();
  ret["global_event_queue_length"] = m_globalEventQueueLength.load();
  ret["thread_event_queue_length"] = m_threadEventQueueLength.load();
  ret["global_processed_event_queue_length"] = m_globalProcessedEventQueueLength.load();
  ret["thread_processed_event_queue_length"] = m_threadProcessedEventQueueLength.load();
  
  ret["total_hits"] = m_totalHits.load();
  ret["total_hit_rate"] = m_totalHitRate.load();
  ret["processed_hits"] = m_processedHits.load();
  ret["processed_hit_rate"] = m_processedHitRate.load();
  ret["written_hits"] = m_writtenHits.load();
  ret["written_hit_rate"] = m_writtenHitRate.load();
  ret["discarded_hits"] = m_discardedHits.load();
  ret["discarded_hit_rate"] = m_discardedHitRate.load();
  ret["total_events"] = m_totalEvents.load();
  ret["total_event_rate"] = m_totalEventRate.load();
  ret["processed_events"] = m_processedEvents.load();
  ret["processed_event_rate"] = m_processedEventRate.load();
  ret["written_events"] = m_writtenEvents.load();
  ret["written_event_rate"] = m_writtenEventRate.load();
  ret["discarded_events"] = m_discardedEvents.load();
  ret["discarded_event_rate"] = m_discardedEventRate.load();
  ret["sync"] = m_sync;
  if (!m_sync) {
    ret["desync_board_ids"] = m_desyncBoardIds;
    ret["desync_info"] = m_desyncInfo;
  }
  return ret;
}


void DaqMonitorEvtTriglessWriterMt::globalHitQueueLength(uint32_t id, size_t length) {
  m_globalHitQueuesLength.at(std::to_string(id)) = length;
}

void DaqMonitorEvtTriglessWriterMt::threadHitQueueLength(uint32_t id, size_t length) {
  m_threadHitQueuesLength.at(std::to_string(id)) = length;
}

void DaqMonitorEvtTriglessWriterMt::globalBoardEventQueueLength(uint32_t id, size_t length) {
  m_globalBoardEventQueuesLength.at(std::to_string(id)) = length;
}

void DaqMonitorEvtTriglessWriterMt::threadBoardEventQueueLength(uint32_t id, size_t length) {
  m_threadBoardEventQueuesLength.at(std::to_string(id)) = length;
}

void DaqMonitorEvtTriglessWriterMt::triggerQueueLength(uint32_t id, size_t length) {
  m_triggerQueuesLength.at(std::to_string(id)) = length;
}

void DaqMonitorEvtTriglessWriterMt::increaseMissedTriggers(uint32_t id, size_t n) {
  m_missedTriggers.at(std::to_string(id)) += n;
}

void DaqMonitorEvtTriglessWriterMt::increaseProcessedTriggers(size_t n) {
  m_processedTriggers += n;
}

void DaqMonitorEvtTriglessWriterMt::globalEventQueueLength(size_t length) {
  m_globalEventQueueLength = length;
}

void DaqMonitorEvtTriglessWriterMt::threadEventQueueLength(size_t length) {
  m_threadEventQueueLength = length;
}

void DaqMonitorEvtTriglessWriterMt::globalProcessedEventQueueLength(size_t length) {
  m_globalProcessedEventQueueLength = length;
}

void DaqMonitorEvtTriglessWriterMt::threadProcessedEventQueueLength(size_t length) {
  m_threadProcessedEventQueueLength = length;
}

void DaqMonitorEvtTriglessWriterMt::increaseTotalHits(size_t n) {
  m_totalHits += n;
  calculateRate(m_totalHits, m_lastTotalHits, m_totalHitRate, m_totalHitRateTimer);
}

void DaqMonitorEvtTriglessWriterMt::increaseProcessedHits(size_t n) {
  m_processedHits += n;
  calculateRate(m_processedHits, m_lastProcessedHits, m_processedHitRate, m_processedHitRateTimer);
}

void DaqMonitorEvtTriglessWriterMt::increaseWrittenHits(size_t n) {
  m_writtenHits += n;
  calculateRate(m_writtenHits, m_lastWrittenHits, m_writtenHitRate, m_writtenHitRateTimer);
}

void DaqMonitorEvtTriglessWriterMt::increaseDiscardedHits(size_t n) {
  m_discardedHits += n;
  calculateRate(m_discardedHits, m_lastDiscardedHits, m_discardedHitRate, m_discardedHitRateTimer);
}

void DaqMonitorEvtTriglessWriterMt::increaseTotalEvents(size_t n) {
  m_totalEvents += n;
  calculateRate(m_totalEvents, m_lastTotalEvents, m_totalEventRate, m_totalEventRateTimer);
}

void DaqMonitorEvtTriglessWriterMt::increaseProcessedEvents(size_t n) {
  m_processedEvents += n;
  calculateRate(m_processedEvents, m_lastProcessedEvents, m_processedEventRate, m_processedEventRateTimer);
}

void DaqMonitorEvtTriglessWriterMt::increaseWrittenEvents(size_t n) {
  m_writtenEvents += n;
  calculateRate(m_writtenEvents, m_lastWrittenEvents, m_writtenEventRate, m_writtenEventRateTimer);
}

void DaqMonitorEvtTriglessWriterMt::increaseDiscardedEvents(size_t n) {
  m_discardedEvents += n;
  calculateRate(m_discardedEvents, m_lastDiscardedEvents, m_discardedEventRate, m_discardedEventRateTimer);
}

void DaqMonitorEvtTriglessWriterMt::syncStatus(bool sync, std::vector<uint32_t> desyncBoardIds, TrigMap map) {
  std::lock_guard<std::mutex> lock{m_mutex};
  m_sync = sync;
  m_desyncBoardIds = desyncBoardIds;
  m_desyncInfo = map;
}


void DaqMonitorEvtTriglessWriterMt::calculateRate(const size_t value, std::atomic<size_t>& lastValue, std::atomic<float>& rate, Timer& timer) {
  // Calculate rate if at least one second has passed from the last calculation AND there are at least 10 new entries. 
  // After 10 seconds at most calculate anyway
  auto timerValue = timer.elapsed();
  if (timerValue < 1. || (value - lastValue < 10 && timerValue < 10.)) {
    return;
  }

  rate = (value - lastValue) / timerValue;
  lastValue = value;
  timer.reset();

}
