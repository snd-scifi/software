#pragma once

#include <atomic>
#include <cstdint>
#include <map>
#include <string>
#include <vector>

#include "monitoring/daq_monitor.hpp"
#include "utils/timer.hpp"

using json = nlohmann::json;
using TrigMap = std::map<uint32_t, std::pair<uint64_t, int64_t>>; //<! [board_id, [trigger_counter, trigger_timestamp]]

class DaqMonitorEvtTriglessWriterMt : public DaqMonitor
{
public:
  DaqMonitorEvtTriglessWriterMt(const std::vector<uint32_t> boardIds);
  // void reset();

  json toJson() const;

  void globalHitQueueLength(uint32_t id, size_t length);
  void threadHitQueueLength(uint32_t id, size_t length);

  void globalBoardEventQueueLength(uint32_t id, size_t length);
  void threadBoardEventQueueLength(uint32_t id, size_t length);

  void triggerQueueLength(uint32_t id, size_t length);
  void increaseMissedTriggers(uint32_t id, size_t n = 1);
  void increaseProcessedTriggers(size_t n = 1);

  void globalEventQueueLength(size_t length);
  void threadEventQueueLength(size_t length);

  void globalProcessedEventQueueLength(size_t length);
  void threadProcessedEventQueueLength(size_t length);

  void increaseTotalHits(size_t n = 1);
  void increaseProcessedHits(size_t n = 1);
  void increaseWrittenHits(size_t n = 1);
  void increaseDiscardedHits(size_t n = 1);

  void increaseTotalEvents(size_t n = 1);
  void increaseProcessedEvents(size_t n = 1);
  void increaseWrittenEvents(size_t n = 1);
  void increaseDiscardedEvents(size_t n = 1);

  void syncStatus(bool sync, std::vector<uint32_t> desyncBoardIds, TrigMap map = {});

private:
  void calculateRate(const size_t value, std::atomic<size_t>& lastValue, std::atomic<float>& rate, Timer& timer);

  std::map<std::string, std::atomic<size_t>> m_globalHitQueuesLength;
  std::map<std::string, std::atomic<size_t>> m_threadHitQueuesLength;

  std::map<std::string, std::atomic<size_t>> m_globalBoardEventQueuesLength;
  std::map<std::string, std::atomic<size_t>> m_threadBoardEventQueuesLength;

  std::map<std::string, std::atomic<size_t>> m_triggerQueuesLength;
  std::map<std::string, std::atomic<size_t>> m_missedTriggers;
  std::atomic<size_t> m_processedTriggers;

  std::atomic<size_t> m_globalEventQueueLength;
  std::atomic<size_t> m_threadEventQueueLength;
  std::atomic<size_t> m_globalProcessedEventQueueLength;
  std::atomic<size_t> m_threadProcessedEventQueueLength;

  std::atomic<size_t> m_totalHits;
  std::atomic<size_t> m_processedHits;
  std::atomic<size_t> m_writtenHits;
  std::atomic<size_t> m_discardedHits;

  std::atomic<size_t> m_totalEvents;
  std::atomic<size_t> m_processedEvents;
  std::atomic<size_t> m_writtenEvents;
  std::atomic<size_t> m_discardedEvents;

  std::atomic<float> m_totalHitRate;
  std::atomic<size_t> m_lastTotalHits;
  Timer m_totalHitRateTimer;

  std::atomic<float> m_processedHitRate;
  std::atomic<size_t> m_lastProcessedHits;
  Timer m_processedHitRateTimer;

  std::atomic<float> m_writtenHitRate;
  std::atomic<size_t> m_lastWrittenHits;
  Timer m_writtenHitRateTimer;

  std::atomic<float> m_discardedHitRate;
  std::atomic<size_t> m_lastDiscardedHits;
  Timer m_discardedHitRateTimer;


  std::atomic<float> m_totalEventRate;
  std::atomic<size_t> m_lastTotalEvents;
  Timer m_totalEventRateTimer;

  std::atomic<float> m_processedEventRate;
  std::atomic<size_t> m_lastProcessedEvents;
  Timer m_processedEventRateTimer;

  std::atomic<float> m_writtenEventRate;
  std::atomic<size_t> m_lastWrittenEvents;
  Timer m_writtenEventRateTimer;

  std::atomic<float> m_discardedEventRate;
  std::atomic<size_t> m_lastDiscardedEvents;
  Timer m_discardedEventRateTimer;

  bool m_sync;
  std::vector<uint32_t> m_desyncBoardIds;
  TrigMap m_desyncInfo;
};

using DaqMonitorEvtTriglessWriterMtPtr = std::shared_ptr<DaqMonitorEvtTriglessWriterMt>;

// namespace nlohmann {
//   template <typename T>
//   struct adl_serializer<std::map<std::string, std::atomic<T>>> {
//     void to_json(json &j, const std::map<std::string, std::atomic<T>> &p) {
//       // j = json;
//       for (const auto &[k, v] : p) {
//         j[k] = v.load();
//       }
//     }
//   };

//   template <typename T>
//   struct adl_serializer<std::atomic<T>> {
//       void to_json(json & j, const std::atomic<T> &p) {
//         j = json{p.load()};
//       }
//   };
// }
