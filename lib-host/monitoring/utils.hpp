#include <atomic>
#include <cstdint>

#include "utils/timer.hpp"

void calculateRate(const size_t value, std::atomic<size_t>& lastValue, std::atomic<float>& rate, Timer& timer);