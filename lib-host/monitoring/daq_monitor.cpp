#include "utils/time.hpp"
#include "daq_monitor.hpp"

using json = nlohmann::json;

DaqMonitor::DaqMonitor(const std::vector<uint32_t> boardIds, const std::string name)
  : GenericMonitor(name)
  , m_serverPacketQueueLength{0}
  , m_eventQueueLength{0}
  , m_processedEventQueueLength{0}
  , m_startTime{0}
  , m_stopTime{0}
  , m_endTime{0}
{
  for (const auto bid : boardIds) {
    m_threadPacketQueuesLength[std::to_string(bid)] = 0;
  }
}

json DaqMonitor::thisToJson() const {
  json ret;
  if (m_startTime > 0) {
    ret["start_time"] = getTimeStringUTC(m_startTime.load());
    if (m_stopTime > 0) {
      ret["elapsed_seconds"] = m_stopTime.load() - m_startTime.load();
    }
    else {
      ret["elapsed_seconds"] = getCurrentTime() - m_startTime.load();
    }
  }
  if (m_stopTime > 0) {
    ret["stop_time"] = getTimeStringUTC(m_stopTime.load());
  }
  if (m_endTime > 0) {
    ret["end_time"] = getTimeStringUTC(m_endTime.load());
  }
  ret["server_packet_queue_length"] = m_serverPacketQueueLength.load();
  ret["event_queue_length"] = m_eventQueueLength.load();
  ret["processed_event_queue_length"] = m_processedEventQueueLength.load();
  std::lock_guard<std::mutex> lock{m_mutex};
  ret["thread_packet_queues_length"] = m_threadPacketQueuesLength;
  return ret;
}


void DaqMonitor::threadPacketQueueLength(uint32_t id, size_t length) {
  std::lock_guard<std::mutex> lock{m_mutex};
  m_threadPacketQueuesLength.at(std::to_string(id)) = length;
}
