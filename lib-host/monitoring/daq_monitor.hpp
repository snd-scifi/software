#pragma once

#include <atomic>
#include <cstdint>
#include <map>
#include <mutex>
#include <string>

#include "monitoring/generic_monitor.hpp"

#include "json.hpp"

// using TrigMap = std::map<uint32_t, std::pair<uint64_t, int64_t>>;  //<! [board_id, [trigger_counter, trigger_timestamp]]

class DaqMonitor : public GenericMonitor {
public:
  DaqMonitor(const std::vector<uint32_t> boardIds, const std::string name = "daq_monitor");

  void serverPacketQueueLength(size_t length) { m_serverPacketQueueLength = length; }
  void eventQueueLength(size_t length) { m_eventQueueLength = length; }
  void processedEventQueueLength(size_t length) { m_processedEventQueueLength = length; }
  void threadPacketQueueLength(uint32_t id, size_t length);
  void startTime(std::time_t startTime) { m_startTime = startTime; }
  void stopTime(std::time_t stopTime) { m_stopTime = stopTime; }
  void endTime(std::time_t endTime) { m_endTime = endTime; }

private:
  nlohmann::json thisToJson() const override;
  std::atomic_size_t m_serverPacketQueueLength;
  std::atomic_size_t m_eventQueueLength;
  std::atomic_size_t m_processedEventQueueLength;
  std::map<std::string, size_t> m_threadPacketQueuesLength;
  std::atomic<std::time_t> m_startTime;
  std::atomic<std::time_t> m_stopTime;
  std::atomic<std::time_t> m_endTime;
};

using DaqMonitorPtr = std::shared_ptr<DaqMonitor>;


// void to_json(json& j, DaqMonitor& i);