#include "monitoring/utils.hpp"

#include "event_processors_monitor.hpp"

using json = nlohmann::json;

EventProcessorsMonitor::EventProcessorsMonitor(const std::string name)
  : GenericMonitor(name)
  , m_localEventQueueLength{0}
  , m_processedHits{0}
  , m_discardedHits{0}
  , m_processedEvents{0}
  , m_discardedEvents{0}
  , m_processedHitRate{0}
  , m_lastProcessedHits{0}
  , m_processedEventRate{0}
  , m_lastProcessedEvents{0}
  , m_discardedHitRate{0}
  , m_lastDiscardedHits{0}
  , m_discardedEventRate{0}
  , m_lastDiscardedEvents{0}
{}

json EventProcessorsMonitor::thisToJson() const {
  auto ret = json();

  ret["local_event_queue_length"] = m_localEventQueueLength.load();

  ret["processed_hits"] = m_processedHits.load();
  ret["discarded_hits"] = m_discardedHits.load();

  ret["processed_events"] = m_processedEvents.load();
  ret["discarded_events"] = m_discardedEvents.load();

  ret["processed_hits_rate"] = m_processedHitRate.load();
  ret["discarded_hits_rate"] = m_discardedHitRate.load();

  ret["processed_events_rate"] = m_processedEventRate.load();
  ret["discarded_events_rate"] = m_discardedEventRate.load();
  
  return ret;
}

void EventProcessorsMonitor::increaseProcessedHits(size_t n) {
  m_processedHits += n;
  calculateRate(m_processedHits, m_lastProcessedHits, m_processedHitRate, m_processedHitRateTimer);
}

void EventProcessorsMonitor::increaseDiscardedHits(size_t n) {
  m_discardedHits += n;
  calculateRate(m_discardedHits, m_lastDiscardedHits, m_discardedHitRate, m_discardedHitRateTimer);
}

void EventProcessorsMonitor::increaseProcessedEvents(size_t n) {
  m_processedEvents += n;
  calculateRate(m_processedEvents, m_lastProcessedEvents, m_processedEventRate, m_processedEventRateTimer);
}

void EventProcessorsMonitor::increaseDiscardedEvents(size_t n) {
  m_discardedEvents += n;
  calculateRate(m_discardedEvents, m_lastDiscardedEvents, m_discardedEventRate, m_discardedEventRateTimer);
}
