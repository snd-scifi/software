#pragma once

#include <atomic>
#include <cstdint>
#include <map>
#include <string>
#include <vector>

#include "monitoring/generic_monitor.hpp"
#include "utils/timer.hpp"

using TrigMap = std::map<uint32_t, std::pair<uint64_t, int64_t>>;

class EventBuilderTriglessMonitor : public GenericMonitor
{
public:
  EventBuilderTriglessMonitor(const std::vector<uint32_t> boardIds, const std::string name = "event_builder_trigless_monitor");
  // void reset();

  void localEventQueueLength(size_t length){ m_localEventQueueLength = length; }
  void hitQueueLength(size_t length) { m_hitQueueLength = length; }
  void localHitQueueLength(size_t length) { m_localHitQueueLength = length; }

  void triggerQueueLength(uint32_t id, size_t length) { m_triggerQueuesLength.at(std::to_string(id)) = length; }
  void increaseMissedTriggers(uint32_t id, size_t n = 1) { m_missedTriggers.at(std::to_string(id)) += n; }
  void increaseProcessedTriggers(size_t n = 1) { m_processedTriggers += n; }

  void increaseTotalHits(size_t n = 1);
  void increaseTotalEvents(size_t n = 1);

  void syncStatus(bool sync, std::vector<uint32_t> desyncBoardIds, TrigMap map = {});

private:
  nlohmann::json thisToJson() const override;

  std::atomic_size_t m_localEventQueueLength;
  std::atomic_size_t m_hitQueueLength;
  std::atomic_size_t m_localHitQueueLength;

  std::map<std::string, std::atomic_size_t> m_triggerQueuesLength;
  std::map<std::string, std::atomic_size_t> m_missedTriggers;
  std::atomic_size_t m_processedTriggers;

  std::atomic_size_t m_totalHits;
  std::atomic_size_t m_totalEvents;

  std::atomic<float> m_totalHitRate;
  std::atomic_size_t m_lastTotalHits;
  Timer m_totalHitRateTimer;

  std::atomic<float> m_totalEventRate;
  std::atomic_size_t m_lastTotalEvents;
  Timer m_totalEventRateTimer;

  bool m_sync;
  std::vector<uint32_t> m_desyncBoardIds;
  TrigMap m_desyncInfo;
};

using EventBuilderTriglessMonitorPtr = std::shared_ptr<EventBuilderTriglessMonitor>;
