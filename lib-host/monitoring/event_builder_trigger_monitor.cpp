#include "monitoring/utils.hpp"

#include "event_builder_trigger_monitor.hpp"

using json = nlohmann::json;

EventBuilderTriggerMonitor::EventBuilderTriggerMonitor(const std::vector<uint32_t> boardIds, const std::string name)
  : GenericMonitor(name)
  , m_localEventQueueLength{0}
  , m_hitQueueLength{0}
  , m_localHitQueueLength{0}
  , m_triggerQueueLength{0}
  , m_localTriggerQueueLength{0}
  , m_processedTriggers{0}
  , m_acceptedHits{0}
  , m_discardedHits{0}
  , m_totalEvents{0}
  , m_acceptedHitRate{0}
  , m_lastAcceptedHits{0}
  , m_discardedHitRate{0}
  , m_lastDiscardedHits{0}
  , m_totalEventRate{0}
  , m_lastTotalEvents{0}
  , m_sync{true}
{
  std::lock_guard<std::mutex> lock{m_mutex};
  for (const auto bid : boardIds) {
    m_triggerQueuesLength[std::to_string(bid)] = 0;
    m_missedTriggers[std::to_string(bid)] = 0;
  }
}

json EventBuilderTriggerMonitor::thisToJson() const {
  auto ret = json();
  std::lock_guard<std::mutex> lock{m_mutex};

  ret["local_event_queue_length"] = m_localEventQueueLength.load();
  ret["hit_queue_length"] = m_hitQueueLength.load();
  ret["local_hit_queue_length"] = m_localHitQueueLength.load();
  ret["trigger_queue_length"] = m_triggerQueueLength.load();
  ret["local_trigger_queue_length"] = m_localTriggerQueueLength.load();

  ret["trigger_queues_length"] = json();
  for(const auto& [k, v] : m_triggerQueuesLength) {
    ret["trigger_queues_length"][k] = v.load();
  }

  ret["missed_triggers"] = json();
  for(const auto& [k, v] : m_missedTriggers) {
    ret["missed_triggers"][k] = v.load();
  }

  ret["processed_triggers"] = m_processedTriggers.load();
  ret["lost_triggers"] = m_lostTriggers.load();
  
  ret["accepted_hits"] = m_acceptedHits.load();
  ret["accepted_hit_rate"] = m_acceptedHitRate.load();
  ret["discarded_hits"] = m_discardedHits.load();
  ret["discarded_hit_rate"] = m_discardedHitRate.load();
  ret["total_events"] = m_totalEvents.load();
  ret["total_event_rate"] = m_totalEventRate.load();
  ret["sync"] = m_sync;
  if (!m_sync) {
    ret["desync_board_ids"] = m_desyncBoardIds;
    ret["desync_info"] = m_desyncInfo;
  }
  return ret;
}

void EventBuilderTriggerMonitor::increaseAcceptedHits(size_t n) {
  m_acceptedHits += n;
  calculateRate(m_acceptedHits, m_lastAcceptedHits, m_acceptedHitRate, m_acceptedHitRateTimer);
}

void EventBuilderTriggerMonitor::increaseDiscardedHits(size_t n) {
  m_discardedHits += n;
  calculateRate(m_discardedHits, m_lastDiscardedHits, m_discardedHitRate, m_discardedHitRateTimer);
}

void EventBuilderTriggerMonitor::increaseTotalEvents(size_t n) {
  m_totalEvents += n;
  calculateRate(m_totalEvents, m_lastTotalEvents, m_totalEventRate, m_totalEventRateTimer);
}

void EventBuilderTriggerMonitor::syncStatus(bool sync, std::vector<uint32_t> desyncBoardIds, TrigMap map) {
  std::lock_guard<std::mutex> lock{m_mutex};
  m_sync = sync;
  m_desyncBoardIds = desyncBoardIds;
  m_desyncInfo = map;
}
