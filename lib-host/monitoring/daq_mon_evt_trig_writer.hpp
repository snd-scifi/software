#pragma once

#include <atomic>
#include <cstdint>
#include <map>
#include <string>
#include <vector>

#include "monitoring/daq_monitor.hpp"

using json = nlohmann::json;
using TrigMap = std::map<uint32_t, std::pair<uint64_t, int64_t>>;  //<! [board_id, [trigger_counter, trigger_timestamp]]

class DaqMonitorEvtTrigWriter : public DaqMonitor {
public:
  DaqMonitorEvtTrigWriter(const std::vector<uint32_t> boardIds);
  // void reset();

  json toJson() const;

  // void globalHitQueueLength(uint32_t id, size_t length);
  // void threadHitQueueLength(uint32_t id, size_t length);

  // void globalBoardEventQueueLength(uint32_t id, size_t length);
  // void threadBoardEventQueueLength(uint32_t id, size_t length);

  // void triggerQueueLength(uint32_t id, size_t length);
  // void increaseMissedTriggers(uint32_t id, size_t n=1);
  // void increaseProcessedTriggers(size_t n=1);

  // void globalEventQueueLength(size_t length);
  // void threadEventQueueLength(size_t length);

  // void globalProcessedEventQueueLength(size_t length);
  // void threadProcessedEventQueueLength(size_t length);

  void increaseWrittenHits(size_t n=1);
  void increaseDiscardedHits(size_t n=1);

  void increaseWrittenEvents(size_t n=1);
  void increaseDiscardedEvents(size_t n=1);

  // void syncStatus(bool sync, std::vector<uint32_t> desyncBoardIds, TrigMap map={});

private:
  // std::map<std::string, std::atomic<size_t>> m_globalHitQueuesLength;
  // std::map<std::string, std::atomic<size_t>> m_threadHitQueuesLength;

  // std::map<std::string, std::atomic<size_t>> m_globalBoardEventQueuesLength;
  // std::map<std::string, std::atomic<size_t>> m_threadBoardEventQueuesLength;

  // std::map<std::string, std::atomic<size_t>> m_triggerQueuesLength;
  // std::map<std::string, std::atomic<size_t>> m_missedTriggers;
  // std::atomic<size_t> m_processedTriggers;

  // std::atomic<size_t> m_globalEventQueueLength;
  // std::atomic<size_t> m_threadEventQueueLength;
  // std::atomic<size_t> m_globalProcessedEventQueueLength;
  // std::atomic<size_t> m_threadProcessedEventQueueLength;

  std::atomic<size_t> m_writtenHits;
  std::atomic<size_t> m_discardedHits;

  std::atomic<size_t> m_writtenEvents;
  std::atomic<size_t> m_discardedEvents;

  // bool m_sync;
  // std::vector<uint32_t> m_desyncBoardIds;
  // TrigMap m_desyncInfo;

};

using DaqMonitorEvtTrigWriterPtr = std::shared_ptr<DaqMonitorEvtTrigWriter>;
