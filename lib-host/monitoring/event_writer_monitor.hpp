#pragma once

#include <atomic>
#include <string>
#include <vector>

#include "monitoring/generic_monitor.hpp"
#include "utils/timer.hpp"

using TrigMap = std::map<uint32_t, std::pair<uint64_t, int64_t>>;

class EventWriterMonitor : public GenericMonitor
{
public:
  EventWriterMonitor(const std::string name = "event_writer_monitor");
  // void reset();

  void localEventQueueLength(size_t length) { m_localEventQueueLength = length; }

  void increaseWrittenHits(size_t n = 1);
  
  void increaseWrittenEvents(size_t n = 1);
  

private:
  nlohmann::json thisToJson() const override;

  std::atomic_size_t m_localEventQueueLength;

  std::atomic_size_t m_writtenHits;

  std::atomic_size_t m_writtenEvents;

  std::atomic<float> m_writtenHitRate;
  std::atomic_size_t m_lastWrittenHits;
  Timer m_writtenHitRateTimer;

  std::atomic<float> m_writtenEventRate;
  std::atomic_size_t m_lastWrittenEvents;
  Timer m_writtenEventRateTimer;

};

using EventWriterMonitorPtr = std::shared_ptr<EventWriterMonitor>;
