#include "utils.hpp"

void calculateRate(const size_t value, std::atomic<size_t>& lastValue, std::atomic<float>& rate, Timer& timer) {
  // Calculate rate if at least one second has passed from the last calculation AND there are at least 10 new entries. 
  // After 10 seconds at most calculate anyway
  auto timerValue = timer.elapsed();
  if (timerValue < 1. || (value - lastValue < 10 && timerValue < 10.)) {
    return;
  }

  rate = (value - lastValue) / timerValue;
  lastValue = value;
  timer.reset();
}