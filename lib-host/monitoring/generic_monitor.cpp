#include "generic_monitor.hpp"

using json = nlohmann::json;

GenericMonitor::GenericMonitor(const std::string name)
  : m_name{name}
{}

json GenericMonitor::toJson() const {
  json ret = thisToJson();
  std::lock_guard<std::mutex> l{m_mutex};
  for (const auto& mon : m_monitors) {
    ret[mon->name()] = mon->toJson();
  }
  return ret;
}

void GenericMonitor::addMonitor(GenericMonitorPtr monitor) {
  std::lock_guard<std::mutex> l{m_mutex};
  m_monitors.push_back(monitor);
}
