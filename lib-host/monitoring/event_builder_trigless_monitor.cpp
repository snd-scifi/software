#include "monitoring/utils.hpp"

#include "event_builder_trigless_monitor.hpp"

using json = nlohmann::json;

EventBuilderTriglessMonitor::EventBuilderTriglessMonitor(const std::vector<uint32_t> boardIds, const std::string name)
  : GenericMonitor(name)
  , m_localEventQueueLength{0}
  , m_hitQueueLength{0}
  , m_localHitQueueLength{0}
  , m_processedTriggers{0}
  , m_totalHits{0}
  , m_totalEvents{0}
  , m_totalHitRate{0}
  , m_lastTotalHits{0}
  , m_totalEventRate{0}
  , m_lastTotalEvents{0}
  , m_sync{true}
{
  std::lock_guard<std::mutex> lock{m_mutex};
  for (const auto bid : boardIds) {
    m_triggerQueuesLength[std::to_string(bid)] = 0;
    m_missedTriggers[std::to_string(bid)] = 0;
  }
}

json EventBuilderTriglessMonitor::thisToJson() const {
  auto ret = json();
  std::lock_guard<std::mutex> lock{m_mutex};

  ret["local_event_queue_length"] = m_localEventQueueLength.load();
  ret["hit_queue_length"] = m_hitQueueLength.load();
  ret["local_hit_queue_length"] = m_localHitQueueLength.load();

  ret["trigger_queues_length"] = json();
  for(const auto& [k, v] : m_triggerQueuesLength) {
    ret["trigger_queues_length"][k] = v.load();
  }

  ret["missed_triggers"] = json();
  for(const auto& [k, v] : m_missedTriggers) {
    ret["missed_triggers"][k] = v.load();
  }

  ret["processed_triggers"] = m_processedTriggers.load();
  
  ret["total_hits"] = m_totalHits.load();
  ret["total_hit_rate"] = m_totalHitRate.load();
  ret["total_events"] = m_totalEvents.load();
  ret["total_event_rate"] = m_totalEventRate.load();
  ret["sync"] = m_sync;
  if (!m_sync) {
    ret["desync_board_ids"] = m_desyncBoardIds;
    ret["desync_info"] = m_desyncInfo;
  }
  return ret;
}

void EventBuilderTriglessMonitor::increaseTotalHits(size_t n) {
  m_totalHits += n;
  calculateRate(m_totalHits, m_lastTotalHits, m_totalHitRate, m_totalHitRateTimer);
}

void EventBuilderTriglessMonitor::increaseTotalEvents(size_t n) {
  m_totalEvents += n;
  calculateRate(m_totalEvents, m_lastTotalEvents, m_totalEventRate, m_totalEventRateTimer);
}

void EventBuilderTriglessMonitor::syncStatus(bool sync, std::vector<uint32_t> desyncBoardIds, TrigMap map) {
  std::lock_guard<std::mutex> lock{m_mutex};
  m_sync = sync;
  m_desyncBoardIds = desyncBoardIds;
  m_desyncInfo = map;
}
