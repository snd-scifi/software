#pragma once

#include <atomic>
#include <thread>

#include "event_building/event_builder.hpp"
#include "monitoring/event_builder_trigless_monitor.hpp"
#include "storage/hit_queue.hpp"


class TriglessEventBuilder : public EventBuilder {
public:
  TriglessEventBuilder(EventQueue& eventQueue, DaqServerManager& serverManager);
  ~TriglessEventBuilder() = default;
  std::string name() const override { return "TriglessEventBuilder"; }
  void append(std::deque<DataPacket>& packets) override;
  void finalize() override;

private:
  void checkTriggersConsistency();
  void determineBoardDelays(const TrigMap& trigmap);
  std::vector<uint32_t> findDesyncedBoardIds(const std::map<uint32_t, std::pair<uint64_t, int64_t>> map) const;
  void buildEvents();

  std::thread m_eventBuildingThread;
  HitQueue m_hitQueue;
  std::map<uint32_t, std::deque<TriggerPacket>> m_trigMap;
  std::map<uint32_t, int64_t> m_delayMap;

  std::atomic_int64_t m_lastGoodTimestamp; // hits up to this timestamp are grouped into Events
  std::atomic_int64_t m_lastSyncTimestamp; // hits up to this timestamp are guaranteed to come from a synched board

  std::atomic_bool m_stopRequested{false};
  std::atomic_bool m_buildEvtsThreadFinished{false};
  std::atomic_bool m_processEvtsThreadFinished{false};

  EventBuilderTriglessMonitorPtr m_monitor;
  const bool m_alignWithClock;
  const unsigned int m_clockAlignPeriod;
  const unsigned int m_clockAlignPhase;
  const unsigned int m_minimumEventDuration;
  const unsigned int m_maximumEventDuration;
  const unsigned int m_maximumHitsGap;
  const bool m_fixDesync;

};