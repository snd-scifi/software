#pragma once

#include <atomic>
#include <thread>

#include "event_building/event_builder.hpp"
#include "monitoring/event_builder_trigger_monitor.hpp"
#include "storage/hit_queue.hpp"


class TriggerEventBuilder : public EventBuilder {
public:
  TriggerEventBuilder(EventQueue& eventQueue, DaqServerManager& serverManager);
  ~TriggerEventBuilder() = default;
  std::string name() const override { return "TriggerEventBuilder"; }
  void append(std::deque<DataPacket>& packets) override;
  void finalize() override;

private:
  void checkTriggers();
  std::vector<uint32_t> findDesyncedBoardIds(const std::map<uint32_t, std::pair<uint64_t, int64_t>> map) const;
  void buildEvents();

  std::thread m_eventBuildingThread;
  HitQueue m_hitQueue;
  std::map<uint32_t, std::deque<TriggerPacket>> m_trigMap; // trigger packets, as received by the boards
  DequeMt<std::pair<uint64_t, int64_t>> m_trigQueue; // trigger (counter, timestamp) to be used for event building
  int64_t m_nextTrigCounter;

  std::atomic_bool m_stopRequested{false};

  EventBuilderTriggerMonitorPtr m_monitor;
  const int64_t m_triggerLatency;
  const int64_t m_triggerWindow;
  const int64_t m_hitRetentionTime;
  const bool m_evtTsSubtractTrigLatency;

};