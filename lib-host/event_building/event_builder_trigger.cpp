#include "event_builder_trigger.hpp"

#include "utils/logger.hpp"

using TriggerQueue = std::deque<TriggerPacket>;
using TriggerMap = std::map<uint32_t, TriggerQueue>;
using TrigMap = std::map<uint32_t, std::pair<uint64_t, int64_t>>; //<! [board_id, [trigger_counter, trigger_timestamp]]

TriggerEventBuilder::TriggerEventBuilder(EventQueue& eventQueue, DaqServerManager& serverManager) 
  : EventBuilder(eventQueue, serverManager)
  , m_nextTrigCounter{0} // TODO check if triggers count from 0 or 1
  , m_monitor{std::make_shared<EventBuilderTriggerMonitor>(m_serverManager.getBoardIds())}
  , m_triggerLatency{m_serverManager.eventBuilderSettings().value("trigger_latency", 0L)}
  , m_triggerWindow{m_serverManager.eventBuilderSettings().value("trigger_window", 4L)}
  , m_hitRetentionTime{m_serverManager.eventBuilderSettings().value("hit_retention_time", 10000000000L)}
  , m_evtTsSubtractTrigLatency{m_serverManager.eventBuilderSettings().value("evt_ts_subtract_trig_latency", true)}
  { 
    for (const auto& boardId : m_serverManager.getBoardIds()) {
      m_trigMap.emplace(boardId, TriggerQueue());
    }
    m_serverManager.daqMonitor()->addMonitor(m_monitor);
    m_eventBuildingThread = std::thread([this] { buildEvents(); });
  }

void TriggerEventBuilder::append(std::deque<DataPacket>& packets) {
  for (const auto& packet : packets) {
    switch (packet.type()) {
    case DataPacket::Hit:
      {
        auto p = static_cast<const HitPacket&>(packet);
        m_hitQueue.put(TofpetHit(std::move(p)));
      }
      break;
    case DataPacket::Trigger:
      {
        auto p = static_cast<const TriggerPacket&>(packet);
        m_trigMap.at(p.boardId()).push_back(p);
      }
      break;
    default:
      break;
    }
  }

  m_monitor->hitQueueLength(m_hitQueue.size());
  for (const auto boardId : m_serverManager.getBoardIds()) {
    m_monitor->triggerQueueLength(boardId, m_trigMap[boardId].size());
  }

  checkTriggers();
}

void TriggerEventBuilder::finalize() {
  NOTICE("Stopping event builder...");
  m_stopRequested = true;
  m_eventBuildingThread.join();
  NOTICE("Event builder stopped.");
}

/**
 * 
 */
void TriggerEventBuilder::checkTriggers() {
  // find the shortest trigger queue
  size_t nMin = std::numeric_limits<size_t>::max();
  for (auto& [id, trigq] : m_trigMap) {
    nMin = std::min(nMin, trigq.size());
  }
   
  // if the shortes trigger queue is empty or with just one trigger, do nothing
  if (nMin < 2) {
    return;
  }
  // we want to leave the last trigger in each queue to be used as a check for event building
  nMin--;

  for (size_t i = 0; i < nMin; i++) {

    // iterator to the queue with the first packet with a valid counter (1 + the previous)
    const auto nextCounterPacketIt = std::find_if(m_trigMap.begin(), m_trigMap.end(), [this] (auto& a) {
      return a.second.front().counter() == m_nextTrigCounter;
    });
    
    // if none of them had the valid counter value, skip it and warn
    if (nextCounterPacketIt == m_trigMap.end()) {
      m_monitor->increaseLostTriggers();
      WARN("Trigger {} was lost by all boards!", m_nextTrigCounter);
      m_nextTrigCounter++;
      continue;
    }

    // if we arrive here, at least one queue has the next trigger, let's save its timestamp
    const auto candidateTriggerTs = (*nextCounterPacketIt).second.front().timestamp();
    auto consistent = true;

    for (auto& [boardId, trigQueue] : m_trigMap) {
      auto& queueFront = trigQueue.front();

      // if counter and timestamp are ok, we can pop the packet
      if (queueFront.counter() == m_nextTrigCounter && queueFront.timestamp() == candidateTriggerTs) {
        trigQueue.pop_front();
      }
      // if the counter is different, some trigger was lost
      else if (queueFront.counter() != m_nextTrigCounter) {
        INFO("Trigger {} was lost by board {}", m_nextTrigCounter, boardId);
        m_monitor->increaseMissedTriggers(boardId);
      }
      // if the counter is the same, but the timestamp is different, there was a desync
      else {
        WARN("Inconsistency found!");
        consistent = false;
        break;
      }
    }

    if (consistent) {
      m_trigQueue.emplace_back(std::make_pair(m_nextTrigCounter, candidateTriggerTs));
      m_monitor->triggerQueueLength(m_trigQueue.size());
      m_nextTrigCounter++;
      // sometimes the trigger timestamp can be off by one, for some reason, so we need to restore this.
      m_monitor->syncStatus(true);
    }
    else {
      TrigMap map;
      for (auto& [id, q] : m_trigMap) {
        map[id] = std::make_pair(q.front().counter(), q.front().timestamp());
      }
      m_monitor->syncStatus(false, findDesyncedBoardIds(map), map);
    }

    m_monitor->increaseProcessedTriggers();
  }

  for (const auto boardId : m_serverManager.getBoardIds()) {
    m_monitor->triggerQueueLength(boardId, m_trigMap[boardId].size());
  }

}

//TODO check if this works
std::vector<uint32_t> TriggerEventBuilder::findDesyncedBoardIds(const TrigMap map) const {
  std::vector<uint32_t> ret;
  std::map<int64_t, size_t> elementCount;

  // count the occurrences of each trigger timestamp
  for (const auto& [bid, trigInfo] : map) {
    auto trigTs = trigInfo.second;
    try {
      elementCount.at(trigTs)++;
    }
    catch (const std::out_of_range&) {
      elementCount[trigTs] = 1;
    }
  }

  // find the most frequesnt timestamp. If this appears only once, all boards are not synchronized.
  auto mostFrequentTs = (*std::max_element(elementCount.begin(), elementCount.end(), [] (const auto& a, const auto& b) { return a.second < b.second; })).first;
  if (elementCount.at(mostFrequentTs) == 1) {
    for (const auto& [bid, trigInfo] : map) {
      ret.push_back(bid);
    }
  }

  // find all boards that have a different timestamp
  for (const auto& [bid, trigInfo] : map) {
    auto trigTs = trigInfo.second;
    if (trigTs != mostFrequentTs) {
      ret.push_back(bid);
    }
  }

  return ret;
}

/**
 * Performs event building.
 */
void TriggerEventBuilder::buildEvents() {
  // define local buffer to store hits
  std::deque<TofpetHit> hitQueue;
  std::deque<std::pair<uint64_t, int64_t>> trigQueue;

  bool breakFlag{false};


  // function to remove hits that are older than max_hit_retention_time, relative to the latest hit
  auto pruneHits = [this, &hitQueue] () {
    size_t discardedHits{0};
    while (!hitQueue.empty()) {
      // if the first hit has a timestamp smaller than the last hit - the retention time, drop it
      if (hitQueue.back().tCoarse() - m_hitRetentionTime < hitQueue.front().tCoarse()) {
        break;
      }
      hitQueue.pop_front();
      discardedHits++;
    }
    m_monitor->increaseDiscardedHits(discardedHits);
    if (discardedHits != 0) {
      DEBUG("Pruned {} hits.", discardedHits);
    }
  };

  auto assembleEvent = [this, &hitQueue, &trigQueue] (const int64_t triggerTimestamp, const uint64_t triggerCounter, const int64_t nextTriggerTimestamp) {
    const auto evtTs = triggerTimestamp - (m_evtTsSubtractTrigLatency ? m_triggerLatency : 0);
      TofpetEvent tmpEvt(evtTs);
      tmpEvt.evtNumber(triggerCounter);
      // PRINT("Event {}\n", evtTs);
      while (!hitQueue.empty()) {
        const auto& frontHit = hitQueue.front();
        const auto frontHitTs = frontHit.tCoarse();
        // if the hit comes before the trigger, drop it
        const auto trigTsCorrected = triggerTimestamp - m_triggerLatency;
        const auto nextTrigTsCorrected = nextTriggerTimestamp - m_triggerLatency;
        if (frontHitTs < trigTsCorrected) {
          hitQueue.pop_front();
          m_monitor->increaseDiscardedHits();
        }
        // if it is in the trigger window, save it
        else if (frontHitTs >= trigTsCorrected && frontHitTs < trigTsCorrected + m_triggerWindow) {
          if (frontHitTs >= nextTrigTsCorrected && frontHitTs < nextTrigTsCorrected + m_triggerWindow) {
            WARN("Hit should be in the next trigger");
            // TODO add flag in event?
            break;
          }
          tmpEvt.emplaceHitRelative(frontHit);
          hitQueue.pop_front();
        }
        // with the first hit outside of the window, break. The event is complete.
        else {
          break;
        }
      }
      m_monitor->increaseTotalEvents();
      m_monitor->increaseAcceptedHits(tmpEvt.nHits());
      
      // once we are done building the event, we can add it to the events queue
      m_eventQueue.emplace_back(std::move(tmpEvt)); // TODO use local queue?
      m_serverManager.daqMonitor()->eventQueueLength(m_eventQueue.size());
      m_monitor->localHitQueueLength(hitQueue.size());
      m_monitor->localTriggerQueueLength(trigQueue.size());

      trigQueue.pop_front();
  };


  // loop where hits are taken from the buffers, stored locally and board events are built.
  // it will stop after finalize is called and all the hit buffers are emptied.
  while(!breakFlag) {
    // check if DAQ is stopping (m_stopRequested is set in finalize())
    breakFlag = m_stopRequested.load();

    // take all hits for the current board ID from the hit buffer and insert them in the local buffer. Blocks 100 ms if no hits are present.
    auto tmpQueue = m_hitQueue.takeAll(100);
    if (!tmpQueue.empty()) {
      hitQueue.insert(hitQueue.end(), std::make_move_iterator(tmpQueue.begin()), std::make_move_iterator(tmpQueue.end()));
      // we need to guarantee that the hits are sorted
      std::sort(hitQueue.begin(), hitQueue.end(), [] (const auto& a, const auto& b) { return a.tCoarse() < b.tCoarse(); });
    }
    tmpQueue.clear();

    m_monitor->hitQueueLength(m_hitQueue.size());
    m_monitor->localHitQueueLength(hitQueue.size());

    // check if triggers are available in the queue, if not restart the loop
    auto trigQueueTmp = m_trigQueue.takeAll(0);
    if (trigQueueTmp.empty()) {
      pruneHits();
      continue;
    }
    trigQueue.insert(trigQueue.end(), std::make_move_iterator(trigQueueTmp.begin()), std::make_move_iterator(trigQueueTmp.end()));

    m_monitor->triggerQueueLength(m_trigQueue.size());
    m_monitor->localTriggerQueueLength(trigQueue.size());

    for (size_t i{0}; i < trigQueue.size() - 1; i++){
      const auto& trigPacket = trigQueue.at(0);
      const auto& nextTrigPacket = trigQueue.at(1);
      const auto trigTs = trigPacket.second;
      const auto trigCnt = trigPacket.first;
      const auto nextTrigTs = nextTrigPacket.second;

      assembleEvent(trigTs, trigCnt, nextTrigTs);
    }

    pruneHits();

  }

  for (size_t i{0}; i < trigQueue.size(); i++){
      const auto trigTs = trigQueue.at(0).second;
      const auto trigCnt = trigQueue.at(0).first;
      int64_t nextTrigTs{std::numeric_limits<int64_t>::max()};
      try {
        nextTrigTs = trigQueue.at(1).second;
      }
      catch (const std::out_of_range& e) {}

      assembleEvent(trigTs, trigCnt, nextTrigTs);
    }
}