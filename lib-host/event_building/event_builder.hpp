#pragma once
#include <deque>
#include <mutex>

#include "storage/data_packet.hpp"
#include "storage/hit_queue.hpp"
#include "storage/evt_queue.hpp"
#include "daq/daq_server_manager.hpp"

class EventBuilder {
public:
  EventBuilder(EventQueue& eventQueue, DaqServerManager& serverManager) : m_eventQueue{eventQueue}, m_serverManager{serverManager} {}
  virtual ~EventBuilder() = default;
  virtual std::string name() const = 0;
  virtual void append(std::deque<DataPacket>& packets) = 0;
  virtual void finalize() = 0;
  EventQueue& eventQueue() { return m_eventQueue; }

protected:
  EventQueue& m_eventQueue;
  const nlohmann::json m_settings;
  mutable std::mutex m_statusMutex;
  DaqServerManager& m_serverManager;
};