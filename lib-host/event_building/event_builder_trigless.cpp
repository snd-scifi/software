#include "event_builder_trigless.hpp"

#include "utils/logger.hpp"

using TriggerQueue = std::deque<TriggerPacket>;
using TriggerMap = std::map<uint32_t, TriggerQueue>;
using TrigMap = std::map<uint32_t, std::pair<uint64_t, int64_t>>; //<! [board_id, [trigger_counter, trigger_timestamp]]

TriglessEventBuilder::TriglessEventBuilder(EventQueue& eventQueue, DaqServerManager& serverManager) 
  : EventBuilder(eventQueue, serverManager)
  , m_lastGoodTimestamp{0}
  , m_lastSyncTimestamp{0}
  , m_monitor{std::make_shared<EventBuilderTriglessMonitor>(m_serverManager.getBoardIds())}
  , m_alignWithClock{m_serverManager.eventBuilderSettings().value("align_with_clock", false)}
  , m_clockAlignPeriod{m_serverManager.eventBuilderSettings().value("clock_align_period", 4U)}
  , m_clockAlignPhase{m_serverManager.eventBuilderSettings().value("clock_align_phase", 0U)}
  , m_minimumEventDuration{m_serverManager.eventBuilderSettings().value("minimum_event_duration", 4U)}
  , m_maximumEventDuration{m_serverManager.eventBuilderSettings().value("maximum_event_duration", 4U)}
  , m_maximumHitsGap{m_serverManager.eventBuilderSettings().value("maximum_hits_gap", 0U)}
  , m_fixDesync{m_serverManager.eventBuilderSettings().value("fix_desync", false)}
  { 
    for (const auto& boardId : m_serverManager.getBoardIds()) {
      m_trigMap.emplace(boardId, TriggerQueue());
      m_delayMap.emplace(boardId, 0);
    }
    m_serverManager.daqMonitor()->addMonitor(m_monitor);
    m_eventBuildingThread = std::thread([this] { buildEvents(); });
  }

void TriglessEventBuilder::append(std::deque<DataPacket>& packets) {
  for (const auto& packet : packets) {
    switch (packet.type()) {
    case DataPacket::Hit:
      {
        auto p = static_cast<const HitPacket&>(packet);
        m_hitQueue.put(TofpetHit(std::move(p)));
      }
      break;
    case DataPacket::Trigger:
      {
        auto p = static_cast<const TriggerPacket&>(packet);
        m_trigMap.at(p.boardId()).push_back(p);
      }
      break;
    default:
      break;
    }
  }

  m_monitor->hitQueueLength(m_hitQueue.size());
  for (const auto& boardId : m_serverManager.getBoardIds()) {
    m_monitor->triggerQueueLength(boardId, m_trigMap[boardId].size());
  }

  checkTriggersConsistency();
}

void TriglessEventBuilder::finalize() {
  NOTICE("Stopping event builder...");
  m_stopRequested = true;
  m_eventBuildingThread.join();
  NOTICE("Event builder stopped.");
}

/**
 * Verifies triggers are consistent, i.e. triggers with the same counter value have the same timestamp.
 * This function assumes that sometimes a trigger can be missing, but the counter will always be consistent.
 * It also assumes that all boards are correctly transmitting.
 * It assignes the value to m_lastGoodTimestamp, based on the penultimate trigger detected.
 */
void TriglessEventBuilder::checkTriggersConsistency() {
  // find the shortest trigger queue
  size_t nMin = std::numeric_limits<size_t>::max();
  for (auto& [id, trigq] : m_trigMap) {
    nMin = std::min(nMin, trigq.size());
  }
   
  // if the shortes trigger queue is empty or with just one trigger, do nothing
  if (nMin < 2) {
    return;
  }
  // we want to leave the last trigger in each queue to be used as a check for event building
  nMin--;

  auto lastGoodTs = m_lastGoodTimestamp.load();
  for (size_t i = 0; i < nMin; i++) {
    // check if the trigger counter is the same for all first packets
    uint32_t cntMin = std::numeric_limits<uint32_t>::max();
    uint32_t cntMax = 0;
    for (const auto& [id, trigq] : m_trigMap) {
      cntMin = std::min(cntMin, trigq.front().counter());
      cntMax = std::max(cntMax, trigq.front().counter());
    }

    // if the counters are not the same, remove the ones smaller than the max and continue
    // we allow some triggers to be lost, at the moment
    if (cntMin != cntMax) {
      WARN("Some trigger was lost: {} != {}", cntMin, cntMax);
      // removes all trigger packets with counter < cntMax (at most one per queue)
      std::for_each(m_trigMap.begin(), m_trigMap.end(), [this, cntMax] (auto& a) {
        if (a.second.front().counter() < cntMax) {
          // if the counter is not at the max, needs to be removed
          a.second.pop_front();
        }
        else {
          // if the counter is at the max, it means at least one of the previous triggers was lost
          m_monitor->increaseMissedTriggers(a.first);
        }
      });
      m_monitor->increaseProcessedTriggers();
      // once this "cleanup" is done, it needs to be checked again
      continue;
    }

    // determines if the first trigger packet of each queue has the same timestamp
    auto consistent = std::adjacent_find(m_trigMap.begin(), m_trigMap.end(), [this] (const auto& a, const auto& b) {
      return a.second.front().timestamp() - m_delayMap.at(a.first) != b.second.front().timestamp() - m_delayMap.at(b.first);
    }) == m_trigMap.end();

    // store the previous last good timestamp
    auto lastGoodTsTmp = m_lastSyncTimestamp.load();

    // determines the "new" last good timestamp
    lastGoodTs = (*std::min_element(m_trigMap.begin(), m_trigMap.end(),
      [] (const auto& a, const auto& b) {
        return a.second.front().timestamp() < b.second.front().timestamp();
      })).second.front().timestamp();
    
    // if an inconsistency is detected, 
    if (consistent) {
      // we know we are consistent up to the "new" last good timestamp, as this group of triggers passed the check
      m_lastSyncTimestamp = lastGoodTs;
    }
    else {
      WARN("inconsistency found!");
      TrigMap map;
      for (const auto& [id, q] : m_trigMap) {
        map[id] = std::make_pair(q.front().counter(), q.front().timestamp());
        // WARN(id, ": ", q.front().timestamp(), " (", q.front().counter(), ")");
      }
      determineBoardDelays(map);
      m_monitor->syncStatus(false, findDesyncedBoardIds(map), map);
    }

    // m_lastGoodTimestamp is set AFTER m_lastSyncTimestamp, when everything is good, as we want the latter to be always bigger, for safety reasons
    m_lastGoodTimestamp = lastGoodTsTmp;
    
    // remove all processed trigger packets
    std::for_each(m_trigMap.begin(), m_trigMap.end(), [] (auto& a) { a.second.pop_front(); });
    m_monitor->increaseProcessedTriggers();
  }

  for (const auto& boardId : m_serverManager.getBoardIds()) {
    m_monitor->triggerQueueLength(boardId, m_trigMap[boardId].size());
  }

}

void TriglessEventBuilder::determineBoardDelays(const TrigMap& trigmap) {
  // if we don't want to fix the desync, we just don't determine the delays
  if (!m_fixDesync) {
    return;
  }

  // determine the lowest timestamp. This will be used as reference
  const auto minTs = (*std::min_element(trigmap.begin(), trigmap.end(), [] (const auto& a, const auto& b) {
      return a.second.second < b.second.second;
    }
  )).second.second;

  // determine the delay of each board wrt the earliest
  for (const auto& [boardId, trigInfo] : trigmap) {
    m_delayMap.at(boardId) = trigInfo.second - minTs;
  }
  
  INFO("Relative board delays: {}", m_delayMap);
}

//TODO check if this works
std::vector<uint32_t> TriglessEventBuilder::findDesyncedBoardIds(const TrigMap map) const {
  std::vector<uint32_t> ret;
  std::map<int64_t, size_t> elementCount;

  // count the occurrences of each trigger timestamp
  for (const auto& [bid, trigInfo] : map) {
    auto trigTs = trigInfo.second;
    try {
      elementCount.at(trigTs)++;
    }
    catch (const std::out_of_range&) {
      elementCount[trigTs] = 1;
    }
  }

  // find the most frequesnt timestamp. If this appears only once, all boards are not synchronized.
  auto mostFrequentTs = (*std::max_element(elementCount.begin(), elementCount.end(), [] (const auto& a, const auto& b) { return a.second < b.second; })).first;
  if (elementCount.at(mostFrequentTs) == 1) {
    for (const auto& [bid, trigInfo] : map) {
      ret.push_back(bid);
    }
  }

  // find all boards that have a different timestamp
  for (const auto& [bid, trigInfo] : map) {
    auto trigTs = trigInfo.second;
    if (trigTs != mostFrequentTs) {
      ret.push_back(bid);
    }
  }

  return ret;
}

/**
 * Performs event building.
 */
void TriglessEventBuilder::buildEvents() {
  // define local buffer to store hits
  std::deque<TofpetHit> hitQueue;

  bool breakFlag{false};

  // loop where hits are taken from the buffers, stored locally and board events are built.
  // it will stop after finalize is called and all the hit buffers are emptied.
  while(!breakFlag) {
    // check if DAQ is stopping (m_stopRequested is set in finalize())
    breakFlag = m_stopRequested.load();
    // gets the last good timestamp that can be processed (set in checkTriggerConsistency(), only hits with timestamp < lgt will be treated)
    auto lgt = m_lastGoodTimestamp.load();
    // if the DAQ is stopping, no more hits will arrive, therefore all hits can be treated.
    // we do this by changing lgt to the maximum possible value, so a hit timestamp is always < lgt
    if (breakFlag) {
      lgt = std::numeric_limits<int64_t>::max();
    }

    // take all hits for the current board ID from the hit buffer and insert them in the local buffer. Blocks 100 ms if no hits are present.
    auto tmpQueue = m_hitQueue.takeAll(100);
    if (!tmpQueue.empty()) {
      hitQueue.insert(hitQueue.end(), std::make_move_iterator(tmpQueue.begin()), std::make_move_iterator(tmpQueue.end()));
    }
    tmpQueue.clear();

    // we need to guarantee that the hits are sorted
    std::sort(hitQueue.begin(), hitQueue.end(), [] (const auto& a, const auto& b) { return a.tCoarse() < b.tCoarse(); });

    m_monitor->hitQueueLength(m_hitQueue.size());
    m_monitor->localHitQueueLength(hitQueue.size());

    // we build events until the queue is empty or the timestamp of the first hit is larger than the last good timestamp, determined at the beginning of the loop.
    auto firstHitTcoarse = [this, &hitQueue] () {
      try {
        return hitQueue.front().tCoarse() - m_delayMap.at(hitQueue.front().boardId());
      }
      catch (const std::out_of_range& e) {
        return 0L;
      }
    };

    for (int64_t firstHitTs = firstHitTcoarse(); !hitQueue.empty() && firstHitTs < lgt; firstHitTs = firstHitTcoarse()) {
      int64_t evtTimestamp;
      // algorithm to realign with a given clock
      if (m_alignWithClock) {
        auto correction = firstHitTs % m_clockAlignPeriod - m_clockAlignPhase;
        // the correction must be non-negative, otherwise the event timestamp comes after the hit timestamp
        correction = correction < 0 ? correction + m_clockAlignPeriod : correction;
        evtTimestamp = firstHitTs - correction;
      }
      else {
        evtTimestamp = firstHitTs;
      }
      // the first avalable hit is always added to the board event and its (corrected) timestamp determines the event timestamp
      TofpetEvent tmpEvt(evtTimestamp);
      tmpEvt.emplaceHitRelative(hitQueue.front());
      hitQueue.pop_front();
      
      // then others hits are added if they are within the right number of clock cycles from the event start
      int64_t lastHitTimestamp{firstHitTs};
      while (!hitQueue.empty() && ( // the queue must contain something
        ( firstHitTcoarse() < tmpEvt.timestamp() + m_minimumEventDuration ) || // always add a hit if it's within the minimum event duration
        ( firstHitTcoarse() < tmpEvt.timestamp() + m_maximumEventDuration && firstHitTcoarse() - lastHitTimestamp <= m_maximumHitsGap ) ) // add a hit if it's within the maximum event duration and close enough to the previous hit
        )
      {
        lastHitTimestamp = firstHitTcoarse();
        tmpEvt.emplaceHitRelative(hitQueue.front());
        hitQueue.pop_front();
      }

      m_monitor->increaseTotalEvents();
      m_monitor->increaseTotalHits(tmpEvt.nHits());
      
      // once we are done building the event, we can add it to the events queue
      m_eventQueue.emplace_back(std::move(tmpEvt)); // TODO use local queue?
      m_serverManager.daqMonitor()->eventQueueLength(m_eventQueue.size());
      m_monitor->localHitQueueLength(hitQueue.size());
    }
  }
}