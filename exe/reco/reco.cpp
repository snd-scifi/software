#include <fstream>
#include <filesystem>

#include "processing/event_loop.hpp"
#include "utils/logger.hpp"
#include "utils/build.hpp"
#include "utils/config.hpp"

#include "mechanics/geometry.hpp"
#include "mechanics/device.hpp"

#include "processing/tofpet_calibrator.hpp"

#include "tclap/CmdLine.h"

using json = nlohmann::json;
namespace fs = std::filesystem;

using namespace TCLAP;
using namespace reco;

int main(int argc, char** argv) {
  CmdLine cmd("Reco");
  UnlabeledValueArg<std::string> inputPathArg("input-path", "Path to the input data folder", true, "", "input folder");
  UnlabeledValueArg<std::string> outputPathArg("output-path", "Path to the output data folder", true, "", "output folder");
  ValueArg<std::string> configurationFileArg("c", "conf", "Configuration file path", false, "configuration.toml", "configuration file");
  ValueArg<std::string> geometryFileArg("g", "geo", "Geometry file path", false, "", "geometry file");
  ValueArg<std::string> deviceFileArg("d", "dev", "Device file path", false, "", "device file");
  ValueArg<std::string> confSubsectionArg("u", "subsection", "Use the given configuration subsection", false, "", "configuration subsection");
  ValueArg<uint64_t> numEventsArg("n", "num", "Number of events to process", false, -1, "number");
  ValueArg<uint64_t> skipEventsArg("s", "skip", "Number of events to skip", false, 0, "skip");
  SwitchArg noProgressArg("p", "no-progress", "Do not show progress bar");
  SwitchArg noColorsArg("k", "no-colors", "Do not use colors in the log");
  ValueArg<Logger::Level> loggerLevelArg("l", "log-level", "Set logger level", false, Logger::Level::Info, "logger level");
  ValueArg<EventLoop::InputDataType> inputDataTypeArg("", "input-data-type", "Specify input data type", false, EventLoop::InputDataType::RawRoot, "input data type");
  
  cmd.add(inputPathArg);
  cmd.add(outputPathArg);
  cmd.add(configurationFileArg);
  cmd.add(geometryFileArg);
  cmd.add(deviceFileArg);
  cmd.add(confSubsectionArg);
  cmd.add(numEventsArg);
  cmd.add(skipEventsArg);
  cmd.add(noProgressArg);
  cmd.add(inputDataTypeArg);
  cmd.add(noColorsArg);
  cmd.add(loggerLevelArg);
  
  cmd.parse(argc, argv);
  auto inputPath = inputPathArg.getValue();
  auto outputPath = outputPathArg.getValue();
  auto configurationFilePath = configurationFileArg.getValue();
  auto geometryFilePath = geometryFileArg.getValue();
  auto deviceFilePath = deviceFileArg.getValue();
  auto confSubsection = confSubsectionArg.getValue();
  auto numEvens = numEventsArg.getValue();
  auto inputDataType = inputDataTypeArg.getValue();
  auto skipEvens = skipEventsArg.getValue();
  auto noProgress = noProgressArg.getValue();

  globalLogger().setMinimalLevel(loggerLevelArg.getValue());
  globalLogger().setColors(!noColorsArg.getValue());
  NOTICE("Reconstruction");
  NOTICE("Build date: {}", SOFTWARE_BUILD_DATE);
  NOTICE("Build time: {}", SOFTWARE_BUILD_TIME);

  auto configuration = reco::tomlToJson(reco::configReadToml(configurationFilePath));

  if (deviceFilePath != "") {
    configuration["device_file"] = deviceFilePath;
  }

  if (geometryFilePath != "") {
    configuration["geometry_file"] = geometryFilePath;
  }

  if (confSubsection != "") {
    for (const auto& [k, v] : configuration.at(confSubsection).items()) {
      configuration[k] = v;
    }
  }

  //create data path if it doesn't exist. Delete and recreate otherwise.
  if (!fs::exists(outputPath)) {
    INFO("Creating output directory {}", outputPath);
    fs::create_directories(outputPath);
  }
  else {
    WARN("Output directory {} exists. Removing and recreating it.", outputPath);
    fs::remove_all(outputPath);
    fs::create_directories(outputPath);
  }

  EventLoop loop(configuration, inputPath, outputPath, numEvens, skipEvens, noProgress, inputDataType);

  loop.loop();

  return 0;
}