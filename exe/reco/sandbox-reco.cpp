#include <fstream>
#include <filesystem>

#include "io/reco_event_reader_root.hpp"
#include "io/reco_event_writer_root.hpp"
#include "processing/event_loop.hpp"
#include "utils/logger.hpp"
#include "utils/build.hpp"
#include "utils/config.hpp"

#include "mechanics/geometry.hpp"
#include "mechanics/device.hpp"

#include "processing/tofpet_calibrator.hpp"

#include "tclap/CmdLine.h"

using json = nlohmann::json;
namespace fs = std::filesystem;

using namespace TCLAP;
using namespace reco;

int main(int argc, char** argv) {
  CmdLine cmd("Reco");
  UnlabeledValueArg<std::string> inputPathArg("input-path", "Path to the input data folder", true, "", "input folder");
  UnlabeledValueArg<std::string> outputPathArg("output-path", "Path to the output data folder", true, "", "output folder");
  ValueArg<std::string> configurationFileArg("c", "conf", "Configuration file path", false, "configuration.toml", "configuration file");
  ValueArg<std::string> geometryFileArg("g", "geo", "Geometry file path", false, "", "geometry file");
  ValueArg<std::string> deviceFileArg("d", "dev", "Device file path", false, "", "device file");
  
  cmd.add(inputPathArg);
  cmd.add(outputPathArg);
  cmd.add(configurationFileArg);
  cmd.add(geometryFileArg);
  cmd.add(deviceFileArg);
  
  cmd.parse(argc, argv);
  auto inputPath = inputPathArg.getValue();
  auto outputPath = outputPathArg.getValue();
  auto configurationFilePath = configurationFileArg.getValue();
  auto geometryFilePath = geometryFileArg.getValue();
  auto deviceFilePath = deviceFileArg.getValue();

  globalLogger().setMinimalLevel(Logger::Level::Info);
  NOTICE("Reconstruction sandbox");
  NOTICE("Build date: {}", SOFTWARE_BUILD_DATE);
  NOTICE("Build time: {}", SOFTWARE_BUILD_TIME);

  auto configuration = reco::tomlToJson(reco::configReadToml(configurationFilePath));

  if (deviceFilePath != "") {
    configuration["device_file"] = deviceFilePath;
  }

  if (geometryFilePath != "") {
    configuration["geometry_file"] = geometryFilePath;
  }


  //create data path if it doesn't exist. Delete and recreate otherwise.
  if (!fs::exists(outputPath)) {
    INFO("Creating output directory {}", outputPath);
    fs::create_directories(outputPath);
  }
  else {
    WARN("Output directory {} exists. Removing and recreating it.", outputPath);
    fs::remove_all(outputPath);
    fs::create_directories(outputPath);
  }

  auto dev = Device::fromFile(configuration.at("device_file").get<std::string>(), configuration.at("geometry_file").get<std::string>());

#ifdef USE_ROOT

  RecoEventReaderRoot r(inputPath, dev);
  NOTICE("Total events: {}", r.numEvents());
  // RecoEventWriterRoot w(outputPath, dev, configuration.at("event_writer"));
  RecoEvent e(dev);
  r.skip(9995);
  while (r.read(e)) {
    // w.append(e);
    NOTICE("Event {}", e.evtNumber());
    // r.skip(1500);
  }
  // w.finalize();

  // EventLoop loop(configuration, inputPath, outputPath, configuration.at("event_writer"));

  // loop.loop();

#endif // USE_ROOT
  return 0;
}