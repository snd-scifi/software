#include <fstream>
#include <filesystem>
#include <map>
#include <set>

#include "io/event_reader_root_multi_tree.hpp"
#include "io/event_reader_root.hpp"
#include "io/event_writer_root.hpp"
#include "utils/logger.hpp"
#include "utils/build.hpp"
#include "utils/time.hpp"
#include "utils/timer.hpp"

#include "processing/tofpet_calibrator.hpp"
#include "processing/snd_add_flags.hpp"
#include "processing/snd_fast_noise_filter.hpp"
#include "processing/snd_advanced_noise_filter.hpp"
#include "utils/snd_evt_flags.hpp"

#include "tclap/CmdLine.h"
#include "json.hpp"

using namespace TCLAP;
namespace fs = std::filesystem;

using json = nlohmann::json;

json loadJsonFile(std::string filename);

int main(int argc, char** argv) {
#ifdef USE_ROOT
  CmdLine cmd("Data reprocessing (simple)");
  UnlabeledValueArg<std::string> inputPathArg("input-path", "Path to the input data folder", true, "", "input folder");
  UnlabeledValueArg<std::string> outputPathArg("output-path", "Path to the output data folder", true, "", "output folder");
  ValueArg<std::string> compressionAlgorithmArg("z", "compression-algorithm", "Override compression algorithm", false, "", "compression algorithm");
  ValueArg<int> compressionLevelArg("y", "compression-level", "Override compression level", false, -1, "compression level");
  ValueArg<Logger::Level> loggerLevelArg("l", "log-level", "Set logger level", false, Logger::Level::Info, "logger level");
  SwitchArg noColorsArg("k", "no-colors", "Do not use colors in the log");

  cmd.add(inputPathArg);
  cmd.add(outputPathArg);
  cmd.add(compressionAlgorithmArg);
  cmd.add(compressionLevelArg);
  cmd.add(loggerLevelArg);
  cmd.add(noColorsArg);
  cmd.parse(argc, argv);

  auto inputPath = inputPathArg.getValue();
  auto outputPath = outputPathArg.getValue();
  auto compressionAlgorithm = compressionAlgorithmArg.getValue();
  auto compressionLevel = compressionLevelArg.getValue();

  globalLogger().setMinimalLevel(loggerLevelArg.getValue());
  globalLogger().setColors(!noColorsArg.getValue());
  NOTICE("SND@LHC data reprocessing");
  NOTICE("Build date: {}", SOFTWARE_BUILD_DATE);
  NOTICE("Build time: {}", SOFTWARE_BUILD_TIME);

  
  auto conf = loadJsonFile(fs::path(inputPath) / "configuration.json");
  auto boardMapping = loadJsonFile(fs::path(inputPath) / "board_mapping.json");
  auto runTimestamps = loadJsonFile(fs::path(inputPath) / "run_timestamps.json");

  TofpetCalibrator calibrator(inputPath);
  json confFastNoiseFilter, confAdvancedNoiseFilter, writerSettings;
  if (conf.contains("daq_server")) {
    // new conf
    confFastNoiseFilter = conf.at("daq_server").at("event_processors").at("snd_fast_noise_filter");
    confAdvancedNoiseFilter = conf.at("daq_server").at("event_processors").at("snd_advanced_noise_filter");
    writerSettings = conf.at("daq_server").at("event_writer");
  }
  else {
    // old conf
    confFastNoiseFilter = conf.at("daq").at("writer_settings").at("processors_settings").at("snd_fast_noise_filter");
    confAdvancedNoiseFilter = conf.at("daq").at("writer_settings").at("processors_settings").at("snd_advanced_noise_filter");
    writerSettings = conf.at("daq").at("writer_settings");
  }

  if (compressionAlgorithm != "") {
    NOTICE("Overriding compression algorithm with {}", compressionAlgorithm);
    writerSettings["compression_algorithm"] = compressionAlgorithm;
  }
  
  if (compressionLevel > 0) {
    NOTICE("Overriding compression level with {}", compressionLevel);
    writerSettings["compression_level"] = compressionLevel;
  }
  SndFastNoiseFilter sndFastNoiseFilter(boardMapping, confFastNoiseFilter);
  SndAdvancedNoiseFilter sndAdvancedNoiseFilter(boardMapping, confAdvancedNoiseFilter);

  std::unique_ptr<EventReader> r;
  r = std::make_unique<EventReaderRoot>(inputPath);
  EventWriterRoot w(outputPathArg, writerSettings);
  TofpetEvent e;

  auto didntPassFastNoiseFilter{0};
  auto didntPassAdvancedNoiseFilter{0};
  std::size_t eventCounter{0};
  Timer t;
  std::map<unsigned int, size_t> fillNumberCounter;
  std::map<unsigned int, size_t> trueFillNumberCounter;
  try {
  while (r->read(e)) {
    // pass it through the noise filter again, to set the correct flags
    if (!sndFastNoiseFilter.process(e)) {
      didntPassFastNoiseFilter++;
    }
    if (!sndAdvancedNoiseFilter.process(e)) {
      didntPassAdvancedNoiseFilter++;
    }

    //apply calibration
    calibrator.process(e);

    // write
    w.append(e);
    
    eventCounter++;
    if (eventCounter % 1000000 == 0) {
      NOTICE("Processed {} events", eventCounter);
    }
  }
  }
  catch (const std::runtime_error& e) {
    ERROR("Error while processing the data: {}", e.what());
  }
  w.finalize();
  auto elapsedSeconds = t.elapsed();
  NOTICE("Processed {} events in {} s ({:.0f} evts/s)", eventCounter, elapsedSeconds, eventCounter/elapsedSeconds);
  NOTICE("{} events didn't pass the fast noise filter", didntPassFastNoiseFilter);
  NOTICE("{} events didn't pass the advanced noise filter", didntPassAdvancedNoiseFilter);
  for (const auto& [fillN, count] : fillNumberCounter) {
    NOTICE("Fill {}: {} events", fillN, count);
  }
  for (const auto& [fillN, count] : trueFillNumberCounter) {
    NOTICE("Fill (true) {}: {} events", fillN, count);
  }
#else
  ERROR("Software was compiled without ROOT enabled. Call cmake with option -DUSE_ROOT=ON");
#endif //USE_ROOT
  
  return 0;
}

json loadJsonFile(std::string filename) {
  std::ifstream ifs(filename);
  return json::parse(ifs);
}
