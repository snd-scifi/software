#include <map>

#include "network/data_server.hpp"
#include "network/daq_cmd_server.hpp"
#include "utils/logger.hpp"
#include "storage/data_queue.hpp"
#include "daq/daq_wrapper.hpp"
#include "utils/build.hpp"

#include "tclap/CmdLine.h"

using namespace TCLAP;

int main(int argc, char** argv) {
  CmdLine cmd("DAQ server");
  ValueArg<std::string> portDataArg("p", "port-data", "Change default port for data", false, "42069", "Port number");
  ValueArg<std::string> portCmdArg("c", "port-commands", "Change default port for commands", false, "42070", "Port number");
  ValueArg<Logger::Level> loggerLevelArg("l", "log-level", "Set logger level", false, Logger::Level::Info, "logger level");
  SwitchArg noColorsArg("k", "no-colors", "Do not use colors in the log");

  cmd.add(portDataArg);
  cmd.add(portCmdArg);
  cmd.add(loggerLevelArg);
  cmd.add(noColorsArg);
  cmd.parse(argc, argv);

  auto portData = portDataArg.getValue();
  auto portCmd = portCmdArg.getValue();

  globalLogger().setMinimalLevel(loggerLevelArg.getValue());
  globalLogger().setColors(!noColorsArg.getValue());

  NOTICE("DAQ server - version {}", VERSION);
  INFO("Built on the {} at {}", SOFTWARE_BUILD_DATE, SOFTWARE_BUILD_TIME);

  DaqServerManager serverManager;

  DaqCmdServer cmdServer(portCmd, serverManager);
  auto cmdThread = cmdServer.commandThread();
  DataServer dataServer(portData, 100);


  while (true) { // TODO stops if daq server shutdown
    // blocks until startDaq is called
    // all configuration must happen here
    DataPacketQueue dataPacketQueue;
    std::vector<std::thread> boardConnectionThreads;
    serverManager.writeRunStatus();
    serverManager.waitForDaqStart();

    // once sartDaq is called, starts the writer
    std::thread daqLoopThread;
    std::unique_ptr<DaqWrapper> daqWrapper = nullptr;
    try {
      daqWrapper = std::make_unique<DaqWrapper>(std::ref(dataPacketQueue), std::ref(serverManager));
      daqLoopThread = daqWrapper->daqLoopThread(); 
    }
    catch (const std::exception& e) {
      ERROR("Error starting writer: {}", e.what());

      serverManager.daqState(DaqServerManager::Error);
      if (daqLoopThread.joinable()) {
        daqLoopThread.join();
      }
      serverManager.startupComplete();
      serverManager.resetDaqFlags();
      continue;
    }

    // waits for the writer to be ready
    serverManager.waitForDaqInitialization();

    // tell to the command thread that the startup is complete
    serverManager.startupComplete();
    
    if (serverManager.daqState() == DaqServerManager::Error) {
      if (daqLoopThread.joinable()) {
        daqLoopThread.join();
      }
      serverManager.resetDaqFlags();
      continue;
    }

    // main loop that accepts connections from the boards
    while(!serverManager.stopRequested()) {
      try {
        boardConnectionThreads.push_back(dataServer.acceptConnection(std::ref(dataPacketQueue), std::ref(serverManager)));
        VERBOSE("daq-server: connection accepted.");
      }
      catch (const connection_timeout& e) {} //do nothing

    } //TODO use futures for daq loop. if it fails, it should instruct all the threads to stop?

    // wait for all daq threads to finish
    for (auto& t : boardConnectionThreads) {
      t.join();
    }
    boardConnectionThreads.clear();

    // request the writer to stop
    serverManager.stopDaqLoop();

    // wait for daq loop thread to finish
    daqLoopThread.join();
    NOTICE("DAQ loop finisehd.");

    serverManager.daqState(DaqServerManager::Idle);
    serverManager.daqStopComplete();
    serverManager.resetDaqFlags();

  }
  serverManager.requestDaqServerStop();

  cmdThread.join();

  return 0;
}