#include <fstream>
#include <map>
#include <set>

#include "network/data_client.hpp"
#include "utils/logger.hpp"
#include "utils/build.hpp"
#include "utils/timer.hpp"

#include "tclap/CmdLine.h"

using namespace TCLAP;

int main(int argc, char** argv) {
  CmdLine cmd("DAQ boards simulator");
  UnlabeledValueArg<std::string> fineNameArg("file-name", "Input data file name", true, "", "file name");
  UnlabeledValueArg<std::string> serverAddressArg("server-address", "Address of the DAQ server", true, "", "server address");
  ValueArg<std::string> serverPortArg("p", "server-port", "Change default port for data", false, "42069", "port number");
  SwitchArg findIdsArg("i", "find-ids", "Find the board IDS");
  MultiArg<uint32_t> boardIdsArg("b", "board-ids", "Set board IDs", true, "board ids");
  ValueArg<Logger::Level> loggerLevelArg("l", "log-level", "Set logger level", false, Logger::Level::Info, "logger level");

  cmd.add(fineNameArg);
  cmd.add(serverAddressArg);
  cmd.add(serverPortArg);
  cmd.add(findIdsArg);
  cmd.add(boardIdsArg);
  cmd.add(loggerLevelArg);
  cmd.parse(argc, argv);

  auto fineName = fineNameArg.getValue();
  auto serverAddress = serverAddressArg.getValue();
  auto serverPort = serverPortArg.getValue();
  auto boardIds = boardIdsArg.getValue();
  auto findIds = findIdsArg.getValue();

  globalLogger().setMinimalLevel(loggerLevelArg.getValue());

  NOTICE("DAQ boards simulator - version {}", VERSION);
  INFO("Built on the {} at {}", SOFTWARE_BUILD_DATE, SOFTWARE_BUILD_TIME);

  std::ifstream f(fineName);
  uint32_t id{0};
  uint32_t lastId{0};
  std::vector<uint32_t> data(4);

  if (findIds) {
    std::set<uint32_t> foundIds;
    while (f >> std::dec >> id >> std::hex >> data.at(0) >> data.at(1) >> data.at(2) >> data.at(3)) {
      foundIds.insert(id);
    }
    NOTICE("IDs found: {}", foundIds);
    return 0;
  }

  NOTICE("Creating clients...");
  std::map<uint32_t, DataClient> clientMap;
  for (const auto id : boardIds) {
    clientMap.try_emplace(id, serverAddress, serverPort, id, false);
  }

  NOTICE("Connecting...");
  for (auto& [id, client] : clientMap) {
    client.connect();
    client.beginCommunication();
  }

  NOTICE("Starting data transmission...");
  Timer t;
  while (f >> std::dec >> id >> std::hex >> data.at(0) >> data.at(1) >> data.at(2) >> data.at(3)) {
    if (id != lastId && lastId != 0) {
      clientMap.at(lastId).transmitQueue();
    }
    try {
      clientMap.at(id).addToQueue(data);
      lastId = id;
    }
    catch(const std::out_of_range&) {}
    
  }

  clientMap.at(lastId).transmitQueue();

  NOTICE("Closing communication...");
  for (auto& [id, client] : clientMap) {
    client.endCommunication();
  }
  NOTICE("Time elapsed: {}", t.elapsed());
  
  
  return 0;
}