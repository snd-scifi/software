#include "io/event_reader_root.hpp"
#include "io/event_writer_root.hpp"
#include "utils/logger.hpp"
#include "utils/build.hpp"

#include "processing/tofpet_calibrator.hpp"

#include "tclap/CmdLine.h"

using namespace TCLAP;

int main(int argc, char** argv) {
#ifdef USE_ROOT
  CmdLine cmd("Data calibrator");
  UnlabeledValueArg<std::string> inputPathArg("input-path", "Path to the input data folder", true, "", "input folder");
  UnlabeledValueArg<std::string> outputPathArg("output-path", "Path to the output data folder", true, "", "output folder");
  cmd.add(inputPathArg);
  cmd.add(outputPathArg);
  cmd.parse(argc, argv);
  auto inputPath = inputPathArg.getValue();
  auto outputPath = outputPathArg.getValue();

  globalLogger().setMinimalLevel(Logger::Level::Info);
  NOTICE("Data calibration");
  NOTICE("Build date: {}", SOFTWARE_BUILD_DATE);
  NOTICE("Build time: {}", SOFTWARE_BUILD_TIME);

  TofpetCalibrator c(inputPath, nlohmann::json::parse("{\"tot_v_coarse_max\": 900}"));
  EventReaderRoot r(inputPath, "event_data");
  EventWriterRoot w(outputPath, nlohmann::json::parse("{\"data_tree_name\": \"event_data\"}"));
  TofpetEvent e;
  while (r.read(e)) {
    c.calibrate(e);
    w.append(e);
  }
#else
  ERROR("Software was compiled without ROOT enabled. Call cmake with option -DUSE_ROOT=ON");
#endif //USE_ROOT
  
  return 0;
}