#include <fstream>
#include <filesystem>
#include <map>
#include <set>

#include "io/event_reader_root_multi_tree.hpp"
#include "io/event_reader_root.hpp"
#include "io/event_writer_root.hpp"
#include "utils/logger.hpp"
#include "utils/build.hpp"
#include "utils/time.hpp"
#include "utils/timer.hpp"

#include "processing/tofpet_calibrator.hpp"
#include "processing/snd_add_flags.hpp"
#include "processing/snd_fast_noise_filter.hpp"
#include "processing/snd_advanced_noise_filter.hpp"
#include "utils/snd_evt_flags.hpp"

#include "tclap/CmdLine.h"
#include "json.hpp"

using namespace TCLAP;
namespace fs = std::filesystem;

struct LhcInfo {
  unsigned int fillNumber;
  std::string beamMode;
  std::string acceleratorMode;
};
using LhcInfoTs = std::pair<double, LhcInfo>;
using LhcInfoMap = std::map<double, LhcInfo>;
using json = nlohmann::json;

json loadJsonFile(std::string filename);
LhcInfoMap generateLhcInfoMap(const json& fills, const json& acceleratorModes);
LhcInfoTs lastLhcInfo(const LhcInfoMap& map, const double timestamp);
LhcInfoTs nextLhcInfo(const LhcInfoMap& map, const double timestamp);
double calculateEventTime(const int64_t eventTimestamp, double runStartTimestamp);
json buildRunInfo(LhcInfo lhcInfo, unsigned int forceFillNumber = 0xFFFFFFFF);

int main(int argc, char** argv) {
#ifdef USE_ROOT
  CmdLine cmd("Data calibrator");
  UnlabeledValueArg<std::string> inputPathArg("input-path", "Path to the input data folder", true, "", "input folder");
  UnlabeledValueArg<std::string> outputPathArg("output-path", "Path to the output data folder", true, "", "output folder");
  ValueArg<std::string> fillPathArg("f", "fill-path", "Path to the fills.json file", false, "", "fill path");
  ValueArg<std::string> aModePathArg("a", "amode-path", "Path to the accelerator_modes.csv file", false, "", "amode path");
  SwitchArg multiTreeArg("m", "multi-tree", "Input file uses the old format with multiple TTrees");
  SwitchArg fixFillArg("b", "fix-fill", "Use the initial fill number for all events");
  ValueArg<Logger::Level> loggerLevelArg("l", "log-level", "Set logger level", false, Logger::Level::Info, "logger level");
  SwitchArg noColorsArg("k", "no-colors", "Do not use colors in the log");

  cmd.add(inputPathArg);
  cmd.add(outputPathArg);
  cmd.add(fillPathArg);
  cmd.add(aModePathArg);
  cmd.add(multiTreeArg);
  cmd.add(fixFillArg);
  cmd.add(loggerLevelArg);
  cmd.add(noColorsArg);
  cmd.parse(argc, argv);

  auto inputPath = inputPathArg.getValue();
  auto outputPath = outputPathArg.getValue();
  auto fillPath = fillPathArg.getValue();
  auto amodePath = aModePathArg.getValue();
  auto multiTree = multiTreeArg.getValue();
  auto fixFill = fixFillArg.getValue();

  globalLogger().setMinimalLevel(loggerLevelArg.getValue());
  globalLogger().setColors(!noColorsArg.getValue());
  NOTICE("SND@LHC data reprocessing");
  NOTICE("Build date: {}", SOFTWARE_BUILD_DATE);
  NOTICE("Build time: {}", SOFTWARE_BUILD_TIME);

  // load the fill json, if specified
  json fills;
  if (fillPath != ""){
    fills = loadJsonFile(fillPath);
  }

  // load the accelerator mode json, if specified
  json acceleratorModes;
  if (amodePath != ""){
    acceleratorModes = loadJsonFile(amodePath);
  }

  // generate the map of LHC info
  auto lhcInfoMap = generateLhcInfoMap(fills, acceleratorModes);

  auto conf = loadJsonFile(fs::path(inputPath) / "configuration.json");
  auto boardMapping = loadJsonFile(fs::path(inputPath) / "board_mapping.json");
  auto runTimestamps = loadJsonFile(fs::path(inputPath) / "run_timestamps.json");
  const auto startTimestamp = getTimestampUTC(runTimestamps.at("start_time"));

  DaqServerManager manager;
  manager.userInfo(buildRunInfo(lastLhcInfo(lhcInfoMap, startTimestamp).second));
  auto initialFillNumber = manager.userInfo().at("fill_number");
  auto trueFillNumber = initialFillNumber;
  double nextLhcInfoTimestamp = nextLhcInfo(lhcInfoMap, startTimestamp).first;

  TofpetCalibrator calibrator(inputPath);
  SndFastNoiseFilter sndFastNoiseFilter(boardMapping, conf["daq"]["writer_settings"]["processors_settings"]["snd_fast_noise_filter"]); //TODO fix
  SndAdvancedNoiseFilter sndAdvancedNoiseFilter(boardMapping, conf["daq"]["writer_settings"]["processors_settings"]["snd_advanced_noise_filter"]); //TODO fix
  SndAddFlags sndAddFlags(manager);

  std::unique_ptr<EventReader> r;
  if (multiTree) {
#ifdef USE_ROOTT
    r = std::make_unique<EventReaderRootMultiTree>(inputPath);
#else
    THROW_RE("broken for the moment");
#endif
  }
  else {
    r = std::make_unique<EventReaderRoot>(inputPath);
  }
  EventWriterRoot w(outputPathArg, conf["daq"]["writer_settings"]); //TODO fix
  TofpetEvent e;

  auto didntPassFastNoiseFilter{0};
  auto didntPassAdvancedNoiseFilter{0};
  std::size_t eventCounter{0};
  Timer t;
  std::map<unsigned int, size_t> fillNumberCounter;
  std::map<unsigned int, size_t> trueFillNumberCounter;
  try {
  while (r->read(e)) {
    // reset the noise filter flags
    e.setFlags(FAST_FILTER_MASK | ADVANCED_FILTER_MASK, 0);

    // pass it through the noise filter again, to set the correct flags
    if (!sndFastNoiseFilter.process(e)) {
      didntPassFastNoiseFilter++;
    }
    if (!sndAdvancedNoiseFilter.process(e)) {
      didntPassAdvancedNoiseFilter++;
    }

    //apply calibration
    calibrator.process(e);

    // retrieve the correct LHC flags and set them in the user info
    auto eventTime = calculateEventTime(e.timestamp(), startTimestamp);
    if (eventTime > nextLhcInfoTimestamp) {
      auto lhcInfo = lastLhcInfo(lhcInfoMap, eventTime);
      trueFillNumber = lhcInfo.second.fillNumber;
      if (fixFill) {
        manager.userInfo(buildRunInfo(lhcInfo.second, initialFillNumber));
      }
      // lastLhcInfoTimestamp = lhcInfo.first;
      nextLhcInfoTimestamp = nextLhcInfo(lhcInfoMap, eventTime).first;
    }

    try {
      fillNumberCounter.at(manager.userInfo().at("fill_number"))++;
    }
    catch (const std::out_of_range& e) {
      fillNumberCounter.emplace(manager.userInfo().at("fill_number"), 1);
    }

    try {
      trueFillNumberCounter.at(trueFillNumber)++;
    }
    catch (const std::out_of_range& e) {
      trueFillNumberCounter.emplace(trueFillNumber, 1);
    }

    // add the LHC flags
    sndAddFlags.process(e);

    // write
    w.append(e);
    
    eventCounter++;
    if (eventCounter % 1000000 == 0) {
      NOTICE("Processed {} events", eventCounter);
    }
  }
  }
  catch (const std::runtime_error& e) {
    ERROR("Error while processing the data: {}", e.what());
  }
  auto elapsedSeconds = t.elapsed();
  NOTICE("Processed {} events in {} s ({:.0f} evts/s)", eventCounter, elapsedSeconds, eventCounter/elapsedSeconds);
  NOTICE("{} events didn't pass the fast noise filter", didntPassFastNoiseFilter);
  NOTICE("{} events didn't pass the advanced noise filter", didntPassAdvancedNoiseFilter);
  for (const auto& [fillN, count] : fillNumberCounter) {
    NOTICE("Fill {}: {} events", fillN, count);
  }
  for (const auto& [fillN, count] : trueFillNumberCounter) {
    NOTICE("Fill (true) {}: {} events", fillN, count);
  }
#else
  ERROR("Software was compiled without ROOT enabled. Call cmake with option -DUSE_ROOT=ON");
#endif //USE_ROOT
  
  return 0;
}

json loadJsonFile(std::string filename) {
  std::ifstream ifs(filename);
  return json::parse(ifs);
}

LhcInfoMap generateLhcInfoMap(const json& fills, const json& acceleratorModes) {
  static const std::map<std::string, std::string> BEAM_MODE_MAP{
    {"ADJUST", "ADJUST"},
    {"BEAMDUMP", "BEAM DUMP"},
    {"CYCLING", "CYCLING"},
    {"FLATTOP", "FLAT TOP"},
    {"INJPHYS", "INJECTION PHYSICS BEAM"},
    {"INJPROB", "INJECTION PROBE BEAM"},
    {"INJSTUP", "INJECTION SETUP BEAM"},
    {"NOBEAM", "NO BEAM"},
    {"PRERAMP", "PREPARE RAMP"},
    {"RAMP", "RAMP"},
    {"RAMPDOWN", "RAMP DOWN"},
    {"SETUP", "SETUP"},
    {"SQUEEZE", "SQUEEZE"},
    {"STABLE", "STABLE BEAMS"}
  };
  std::string lastBeamMode;
  LhcInfoMap lhcInfoMap;
  std::map<double, std::string> accModeMap;

  // get a map with the accelerator modes
  for (const auto& amodeInfo : acceleratorModes) {
    accModeMap.emplace(amodeInfo.at("timestamp").get<double>()/1000000., amodeInfo.at("accelerator_mode").get<std::string>());
  }

  unsigned int lastFillNumber{0};
  double lastFillEndTimestamp{0.};
  // loop over the fills
  for (const auto& fillInfo : fills) {
    lastFillNumber = fillInfo.at("fillNumber").get<unsigned int>();
    auto fillStartTimestamp = fillInfo.at("startTime").get<double>();
    lastFillEndTimestamp = fillInfo.at("endTime").get<double>();

    std::string accMode;
    // in case the fill has no beam modes, we use the last one and the fill timestamp
    if (fillInfo.at("beamModes").size() == 0) {
      // this gets the last accelerator mode for a given timestamp (upper_bound finds the first with greater timestamp, so we decrement it by 1)
      accMode = (*(--accModeMap.upper_bound(fillStartTimestamp))).second;
      lhcInfoMap.try_emplace(fillStartTimestamp, LhcInfo({lastFillNumber, lastBeamMode, accMode}));
    }
    // otherwise we just take the timestamps from the beam modes
    else {
      for (const auto& beamModeInfo : fillInfo.at("beamModes")) {
        lastBeamMode = BEAM_MODE_MAP.at(beamModeInfo.at("mode"));
        auto accModeIt = accModeMap.upper_bound(beamModeInfo.at("startTime"));
        if (accModeIt != accModeMap.begin()) {
          accMode = (*(--accModeIt)).second;
        }
        else {
          accMode = "UNKNOWN";
        }
        lhcInfoMap.try_emplace(beamModeInfo.at("startTime"), LhcInfo({lastFillNumber, lastBeamMode, accMode}));
      }
    }
  }
  
  // finally, we add the last acc modes, manually adding the last fill
  for (auto accModeIt = accModeMap.upper_bound(lastFillEndTimestamp); accModeIt != accModeMap.end(); accModeIt++) {
    lhcInfoMap.try_emplace(lastFillEndTimestamp, LhcInfo({lastFillNumber+1, lastBeamMode, (*accModeIt).second}));
  }

  return lhcInfoMap;
}

LhcInfoTs lastLhcInfo(const LhcInfoMap& map, const double timestamp) {
  return *(--map.upper_bound(timestamp));
}

LhcInfoTs nextLhcInfo(const LhcInfoMap& map, const double timestamp) {
  if (map.upper_bound(timestamp) == map.end()) {
    return std::make_pair(1e30, LhcInfo({-1U, "", ""}));
  }
  else {
    return *(map.upper_bound(timestamp));
  }
}

double calculateEventTime(const int64_t eventTimestamp, double runStartTimestamp) {
  return runStartTimestamp + eventTimestamp * 6.23768058085e-9;
}

json buildRunInfo(LhcInfo lhcInfo, unsigned int forceFillNumber) {
  json ret;
  if (forceFillNumber == 0xFFFFFFFF){
    ret["fill_number"] = lhcInfo.fillNumber;
  }
  else {
    ret["fill_number"] = forceFillNumber;
  }
  ret["beam_mode"] = lhcInfo.beamMode;
  ret["accelerator_mode"] = lhcInfo.acceleratorMode;
  return ret;
}