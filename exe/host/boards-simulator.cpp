#include <fstream>
#include <map>
#include <set>

#include "network/data_client.hpp"
#include "utils/delay.hpp"
#include "utils/logger.hpp"
#include "utils/build.hpp"
#include "utils/timer.hpp"
#include "io/packet_reader_bin.hpp"

#include "tclap/CmdLine.h"

using namespace TCLAP;

int main(int argc, char** argv) {
  CmdLine cmd("DAQ boards simulator");
  UnlabeledValueArg<std::string> inputPathArg("input-path", "Input data folder", true, "", "input path");
  UnlabeledValueArg<std::string> serverAddressArg("server-address", "Address of the DAQ server", true, "", "server address");
  ValueArg<std::string> serverPortArg("p", "server-port", "Change default port for data", false, "42069", "port number");
  // SwitchArg findIdsArg("i", "find-ids", "Find the board IDS");
  MultiArg<uint32_t> boardIdsArg("b", "board-ids", "Set board IDs", true, "board ids");
  MultiArg<uint32_t> excludeBoardIdsArg("x", "exclude-board-ids", "Set excluded board IDs", false, "excluded board ids");
  ValueArg<Logger::Level> loggerLevelArg("l", "log-level", "Set logger level", false, Logger::Level::Info, "logger level");
  ValueArg<unsigned int> speedLimitArg("s", "speed-limit", "Set transmission speed. 0 for no limit.", false, 0, "speed limit");

  cmd.add(inputPathArg);
  cmd.add(serverAddressArg);
  cmd.add(serverPortArg);
  // cmd.add(findIdsArg);
  cmd.add(boardIdsArg);
  cmd.add(excludeBoardIdsArg);
  cmd.add(loggerLevelArg);
  cmd.add(speedLimitArg);
  cmd.parse(argc, argv);

  auto inputPath = inputPathArg.getValue();
  auto serverAddress = serverAddressArg.getValue();
  auto serverPort = serverPortArg.getValue();
  auto boardIds = boardIdsArg.getValue();
  auto excludeBoardIds = excludeBoardIdsArg.getValue();
  auto speedLimit = speedLimitArg.getValue();
  // auto findIds = findIdsArg.getValue();

  globalLogger().setMinimalLevel(loggerLevelArg.getValue());

  NOTICE("DAQ boards simulator - version {}", VERSION);
  INFO("Built on the {} at {}", SOFTWARE_BUILD_DATE, SOFTWARE_BUILD_TIME);

  

  // if (findIds) {
  //   std::set<uint32_t> foundIds;
  //   while (f >> std::dec >> id >> std::hex >> data.at(0) >> data.at(1) >> data.at(2) >> data.at(3)) {
  //     foundIds.insert(id);
  //   }
  //   NOTICE("IDs found: {}", foundIds);
  //   return 0;
  // }

  NOTICE("Creating clients...");
  std::map<uint32_t, DataClient> clientMap;
  std::map<uint32_t, int64_t> lastSeenTimestamp;
  std::map<uint32_t, int64_t> lastTransmittedTimestamp;
  std::map<uint32_t, Timer> transmissionTimers;
  for (const auto id : boardIds) {
    clientMap.try_emplace(id, serverAddress, serverPort, id, false);
    lastSeenTimestamp.try_emplace(id, 0);
    lastTransmittedTimestamp.try_emplace(id, 0);
    transmissionTimers.try_emplace(id);
  }

  auto determineDelay = [&] (uint32_t id) {
    if (speedLimit == 0) {
      return -1;
    }
    auto triggerDeltaTs = (lastSeenTimestamp.at(id) - lastTransmittedTimestamp.at(id)) / 160000;
    return static_cast<int>( (triggerDeltaTs - transmissionTimers.at(id).elapsed() * 1000)  / speedLimit );
  };

  NOTICE("Connecting...");
  for (auto& [id, client] : clientMap) {
    client.connect();
    client.beginCommunication();
  }

  PacketReaderBin reader(inputPath);
  uint32_t lastId{0};
  std::vector<uint32_t> data(4);

  NOTICE("Starting data transmission...");
  Timer t;
  DataPacket p;
  while (reader.read(p)) {
    if (std::find(excludeBoardIds.begin(), excludeBoardIds.end(), p.boardId()) != excludeBoardIds.end()) {
      continue;
    }
    // determine the timestamp of the packet
    if (p.type() == DataPacket::Trigger) {
      const auto& tp = static_cast<const TriggerPacket&>(p);
      lastSeenTimestamp.at(p.boardId()) = tp.timestamp();
    }
    else if (p.type() == DataPacket::Hit) {
      const auto& tp = static_cast<const HitPacket&>(p);
      lastSeenTimestamp.at(p.boardId()) = tp.timestampCoarse();
    }

    // extract data from the packet
    data = std::vector<uint32_t>(p.data().begin(), p.data().end());

    // trasmit only when the new packet is from a different board
    if (p.boardId() != lastId && lastId != 0) {
      if (int sleepTime = determineDelay(lastId); sleepTime > 0) {
        delayMilliseconds(sleepTime);
      }
        
      clientMap.at(lastId).transmitQueue();
      lastTransmittedTimestamp.at(lastId) = lastSeenTimestamp.at(lastId);
      transmissionTimers.at(lastId).reset();
    }

    // add the packet to the queue
    try {
      clientMap.at(p.boardId()).addToQueue(data);
      lastId = p.boardId();
    }
    catch(const std::out_of_range&) {}
    
  }

  clientMap.at(lastId).transmitQueue();

  NOTICE("Closing communication...");
  for (auto& [id, client] : clientMap) {
    client.endCommunication();
  }
  NOTICE("Time elapsed: {}", t.elapsed());
  
  
  return 0;
}