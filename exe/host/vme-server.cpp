#include <sstream>

#include "network/vme_server.hpp"
#include "utils/logger.hpp"
#include "utils/build.hpp"

#include "tclap/CmdLine.h"

using namespace TCLAP;

int main(int argc, char** argv) {
  CmdLine cmd("VME server");
  ValueArg<VmeServer::VmeInterface> vmeInterfaceArg("i", "interface", "Select VME interface (vm_usb, caen_v1718, caen_v2718, caen_v3718_usb)", false, VmeServer::VmeInterface::VmUsb, "interface");
  ValueArg<std::string> ttcviAddressArg("a", "ttcvi-address", "TTCvi VME address (A23-A8) in hex, as set by the four rotary switches on the PCB.", false, "FF10", "ttcvi address");
  ValueArg<Logger::Level> loggerLevelArg("l", "log-level", "Set logger level", false, Logger::Level::Info, "logger level");
  SwitchArg noColorsArg("k", "no-colors", "Do not use colors in the log");

  cmd.add(vmeInterfaceArg);
  cmd.add(ttcviAddressArg);
  cmd.add(loggerLevelArg);
  cmd.add(noColorsArg);
  cmd.parse(argc, argv);

  globalLogger().setMinimalLevel(loggerLevelArg.getValue());
  globalLogger().setColors(!noColorsArg.getValue());

  NOTICE("VME server - version {}", VERSION);
  INFO("Built on the {} at {}", SOFTWARE_BUILD_DATE, SOFTWARE_BUILD_TIME);

  auto ttcviAddress = std::stol(ttcviAddressArg.getValue(), nullptr, 16);
  ttcviAddress <<= 8;

  INFO("TTCvi address: {:#08X}", ttcviAddress);

  VmeServer vmeServer("41867", 1, 0x39, ttcviAddress, vmeInterfaceArg.getValue());
  NOTICE("Starting VME server...");
  vmeServer.start();
  while (1) {
    try {
      auto connection = vmeServer.acceptConnection();
      vmeServer.connectionLoop(connection);
    }
    catch (const connection_timeout& e) {}
  }
  
  return 0;
}