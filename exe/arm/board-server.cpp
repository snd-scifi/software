#include "tclap/CmdLine.h"

#include "utils/logger.hpp"
#include "utils/build.hpp"
#include "network/board_server.hpp"
#include "tofpet/tofpet.hpp"
#include "fpga/fpga.hpp"
#include "fpga/ttcrx.hpp"
#include "fpga/registers.hpp"
#include "board/board_startup_status.hpp"
#include "fpga/temperature.hpp"

using namespace TCLAP;

int main(int argc, char** argv) {

  CmdLine cmd("Board server");
  SwitchArg daemonArg("d", "daemon", "Run as daemon");
  ValueArg<Logger::Level> loggerLevelArg("l", "log-level", "Set logger level", false, Logger::Level::Info, "logger level");
  SwitchArg noColorsArg("k", "no-colors", "Do not use colors in the log");

  cmd.add(daemonArg);
  cmd.add(loggerLevelArg);
  cmd.add(noColorsArg);
  cmd.parse(argc, argv);

  auto daemon = daemonArg.getValue();
  
  globalLogger().setMinimalLevel(loggerLevelArg.getValue());
  globalLogger().setColors(!noColorsArg.getValue());
  if (daemon) {
    globalLogger().setLevelPrint(Logger::LevelPrint::Short);
    globalLogger().setTimePrint(Logger::TimePrint::None);
  }
  globalLogger().setUseSyslog(daemon);
  
  NOTICE("DAQ board server - version {}", VERSION);
  INFO("Built on the {} at {}", SOFTWARE_BUILD_DATE, SOFTWARE_BUILD_TIME);

  auto fw = fwInfo();
  NOTICE("FW date: {}", fw.buildDate);
  NOTICE("FW time: {}", fw.buildTime);
  NOTICE("FW version: {}", fw.buildNumber);
  NOTICE("Initializing DAQ board");
  initializeBoard();

  NOTICE("Board ID: {}", boardId());
  NOTICE("Board revision: {}", boardRevision());

  NOTICE("FE boards IDs: {}", BoardStartupStatus::getInstance().feBoardIds());
  NOTICE("Detected TOFPETs: {}", BoardStartupStatus::getInstance().tofpetsAvailable());

  BoardServer boardServer("41968", 1);
  NOTICE("Starting board server...");
  boardServer.start();
  while (1) {
    try {
      auto connection = boardServer.acceptConnection();
      boardServer.connectionLoop(connection);
    }
    catch (const connection_timeout& e) {}
  }
  

  return 0;
}