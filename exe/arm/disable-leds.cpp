#include "tclap/CmdLine.h"

#include "fpga/fpga.hpp"

using namespace TCLAP;

int main(int argc, char** argv) {
  CmdLine cmd("Disable leds");
  SwitchArg enableArg("e", "enable", "Enable the LEDs");

  cmd.add(enableArg);
  cmd.parse(argc, argv);

  disableLeds(!enableArg.getValue());

  
  return 0;
}