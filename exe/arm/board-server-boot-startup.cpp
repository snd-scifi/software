#include <string>

#include "tclap/CmdLine.h"

#include "board/board.hpp"
#include "utils/logger.hpp"

using namespace TCLAP;

int main(int argc, char** argv) {
  CmdLine cmd("Board server boot startup");
  UnlabeledValueArg<std::string> optionArg("option", "enable or disable", true, "enable", "string");
  globalLogger().setMinimalLevel(Logger::Level::Info);

  cmd.add(optionArg);
  cmd.parse(argc, argv);

  if (optionArg.getValue() == "enable" || optionArg.getValue() == "ENABLE") {
    board::setBootStartup(true);
  }
  else if (optionArg.getValue() == "disable" || optionArg.getValue() == "DISABLE") {
    board::setBootStartup(false);
  }
  else {
    ERROR("Unknown option {}. Valid options are enable and disable.", optionArg.getValue());
    return 1;
  }

  return 0;
}