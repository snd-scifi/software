#include <string>

#include "tclap/CmdLine.h"

#include "fpga/fpga.hpp"
#include "utils/logger.hpp"

using namespace TCLAP;

int main(int argc, char** argv) {
  CmdLine cmd("Clock mux");
  UnlabeledValueArg<std::string> optionArg("option", "qpll or no-qpll", true, "qpll", "option");
  globalLogger().setMinimalLevel(Logger::Level::Info);

  cmd.add(optionArg);
  cmd.parse(argc, argv);

  if (optionArg.getValue() == "qpll" || optionArg.getValue() == "QPLL") {
    clock40UseQpll(true);
  }
  else if (optionArg.getValue() == "no-qpll" || optionArg.getValue() == "NO-QPLL") {
    clock40UseQpll(false);
  }
  else {
    ERROR("Unknown option {}. Valid options are qpll and no-qpll.", optionArg.getValue());
    return 1;
  }

  return 0;
}