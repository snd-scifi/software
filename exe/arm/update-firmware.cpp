#include "tclap/CmdLine.h"

#include "board/board.hpp"
#include "fpga/fpga.hpp"
#include "utils/logger.hpp"
#include "utils/build.hpp"

using namespace TCLAP;

int main(int argc, char** argv) {
  CmdLine cmd("Firmware update");
  ValueArg<std::string> fileArg("f", "file", "Path to the new rbf file", false, "/opt/firmware-update/fpga.rbf", "File name");
  ValueArg<Logger::Level> loggerLevelArg("l", "log-level", "Set logger level", false, Logger::Level::Info, "logger level");
  SwitchArg noColorsArg("k", "no-colors", "Do not use colors in the log.");
  SwitchArg rebootArg("r", "reboot", "Reboot after updating the firmware.");

  cmd.add(fileArg);
  cmd.add(loggerLevelArg);
  cmd.add(noColorsArg);
  cmd.add(rebootArg);
  cmd.parse(argc, argv);

  auto filename = fileArg.getValue();
  
  globalLogger().setMinimalLevel(loggerLevelArg.getValue());
  globalLogger().setColors(!noColorsArg.getValue());

  NOTICE("Firmware updater - version {}", VERSION);
  INFO("Built on the {} at {}", SOFTWARE_BUILD_DATE, SOFTWARE_BUILD_TIME);

  NOTICE("Updating firmware...");
  updateFw(filename);

  if (rebootArg.getValue()) {
    NOTICE("Rebooting...");
    board::rebootBoard();
  }
  
  return 0;
}