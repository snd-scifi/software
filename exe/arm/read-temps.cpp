#include "board/board_startup_status.hpp"
#include "fpga/temperature.hpp"
#include "utils/logger.hpp"

int main(int argc, char** argv) {
  // globalLogger().setColors(false);
  // globalLogger().setLevelPrint(Logger::LevelPrint::None);
  // globalLogger().setTimePrint(Logger::TimePrint::None);
  globalLogger().setMinimalLevel(Logger::Level::Alert);
  for (int i = 0; i < 4; i++) {
    PRINT("FE {}: {:.1f} °C\n", i, feTemperature(i));
  }
  for (int i = 0; i < 4; i++) {
    PRINT("SiPM {}: {:.1f} °C\n", i, sipmTemperature(i));
  }

  if (BoardStartupStatus::getInstance().boardVersion() > 4) {
    PRINT("Board: {:.1f} °C\n", boardTemperature());
  }
  
  return 0;
}