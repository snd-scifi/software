# DAQ software
The DAQ software is written in C++ 17 and Python 3.
It has been developed and tested on Ubuntu 20.04 and to make your life easier I suggest you use it as well.
It is possible to make it work on older versions of Ubuntu (I tested 18.04), see below.

## Prerequisites
The bare minimum to compile and use the DAQ software is:
* A C++17 compiler for the host system (tested with `g++-9`, later versions should work);
* CMake version >3.14 (>3.7 would be enough for this code, but 3.14 is better for ROOT compilation). On ubuntu, `sudo apt install cmake`.
* make (`sudo apt install make`);
* git (`sudo apt install git`);
* ROOT compiled with the C++17 standard (I suggest you compile it from source, see below);

If you need to use the `vme-server`:
* `libxxusb` (also requires `libusb`) to be able to control the TTCVi module in a VME crate with the Wiener VM-USB module (see below);
* the CAENVMELib to be able to control the TTCVi module in a VME crate with a CAEN VME module vX718 (see below).

If you need to compile the software that runs on the DAQ boards:
* A C++17 compiler for the boards `arm-linux-gnueabihf-g++-8` (version 9 will not work, other compilers not tested). On ubuntu 20.04 do `sudo apt install g++-8-arm-linux-gnueabihf gcc-8-arm-linux-gnueabihf`.


### Older Ubuntu versions
To get `gcc-9` and `g++-9` on ubuntu 18.04 (and probably even earlier versions), do:
```
sudo add-apt-repository ppa:ubuntu-toolchain-r/test
sudo apt update
sudo apt install gcc-9 g++9
sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-9 30
sudo update-alternatives --install /usr/bin/g++ g++ /usr/bin/g++-9 30
```
The last two lines should be done for each version of the compiles you want to be able to switch between using `update-alternatives`, more info [here](https://askubuntu.com/questions/26498/how-to-choose-the-default-gcc-and-g-version)

### ROOT
There is the option to compile the code with ROOT, if it is desired to write the data to ROOT files. To allow this, ROOT must be compiled with the same C++ standard as the software (C++17).

To do so, cmake must be invoked with the `-DCMAKE_CXX_STANDARD=17` option when compiling ROOT. Instructions to compile ROOT from sources can be found [here](https://root.cern/install/build_from_source/)

### libxxusb
Details on how to install `libxxusb` can be found [here](https://gitlab.cern.ch/snd-scifi/software/-/wikis/libxxusb-installation)

### CAENVMELib and drivers
Details on how to install the CAEN VME library and drivers can be found [here](https://gitlab.cern.ch/snd-scifi/software/-/wikis/CAENVMELib-and-driver-installation)


## Get the code
* Clone the git repo: `git clone ssh://git@gitlab.cern.ch:7999/snd-scifi/software.git` or `git clone https://gitlab.cern.ch/snd-scifi/software.git`.
* Enter the repo and fetch the submodules by doing:
```
git submodule init
git submodule update
```

## Compile the code
You can compile for the host system or for the ARM boards. I suggest to have two folders, `build-host` and `build-arm`.
For example, let's compile for ARM:
```
cd path/to/software
mkdir build-arm
cd build-arm
cmake -DARCH=arm -DCMAKE_BUILD_TYPE=Release ..
make -j8
```

Similarly, for host it will be:
```
cd path/to/software
mkdir build-host
cd build-host
cmake -DUSE_ROOT=ON -DUSE_VMUSB=OFF -DUSE_CAENVMELIB=OFF -DCMAKE_BUILD_TYPE=Release ..
make -j8
```
In case ROOT is not available, remove `-DUSE_ROOT=ON`. If you want to enable the VME libraries, change the relevant one to `ON`

In all cases, the binary will be available in the `bin` folder.

### Compilation options
To be added as `-D<option>=<value>` when invoking CMake.
| Option | Values | Meaning |
| ------ | ------ | ------- |
| `ARCH` | `arm/host` | Compile the code for the boards or for the host system. Default `host`. |
| `USE_ROOT` | `ON/OFF` | Compile the code with ROOT. `ARCH` must be `host`. Default `OFF`.|
| `USE_VMUSB` | `ON/OFF` | Compile the code linking `libxxusb`, to e able to control the VM-USB module. `ARCH` must be `host`. Default `OFF`. |
| `USE_CAENVMELIB` | `ON/OFF` | Compile the code linking `CAENVMELib`, to e able to control the CAEN VME modules. `ARCH` must be `host`. Default `OFF`. |
| `CMAKE_BUILD_TYPE` | `Debug/Release` | Compile optimized for debug or for performance. `Release` is recommended for data taking, `Debug` for development.  Default `Debug`. |

## Python module
Python modules are available to control the various components of the DAQ system.
To use them, you first need to create a virtual environment (in this example, we will create it in the `~/.venv directory`, create it if it doesn't exist).
```bash
cd
python3 -m venv ~/.venv/my-venv
source ~/.venv/my-venv/bin/activate
```

Now the command `which python` should return a path in `~/.venv/my-venv/...`. If this is not the case, something went wrong when creating the virtual environment.

The virtual environment is needed because there is a part of the python module written in C++ that fails to compile when using the system python installation.
You also need to make sure that the python header is installed, normally it comes with the `python3-dev` package in Ubuntu.

Once this is done, the module itself can be installed:

```bash
cd path/to/software/python
pip install -e .
```
The `-e` installs the package in editable mode, i.e. links to the files in the repository are created, so when it is updated, the package is automatically up to date, without needing to reinstall it.
Of course the command needs to be run again if thr C++ part is updated (in case of doubt, run it after updating the repository).

## Running on lxplus
Connect to lxplus via 
```bash
ssh user_name@lxplus.cern.ch
```

Git clone and get the submodules as described above in 'Get the code'. If working in a particular branch, enable it via:
```bash
git checkout branch-name
```
with the name of your branch.

 Next, cd into the cvmfs folder:
```bash
cd /cvmfs/
```
Use the following two commands to check Linux version adn CentOS version. 
```bash
uname -a
cat /etc/centos-release
```
Now, find the appropriate folder from the list shown by:
```bash
ls sft.cern.ch/lcg/views/LCG_101
```
Note that LCG_102 may give the following error
```
This application failed to start because it could not find or load the Qt platform plugin "xcb"
```
which can be fixed by adding 
```
import matplotlib
matplotlib.use('TkAgg')
``` to your files. However, simply using LCG_101 is recommended. 

Source lcg via:
```bash
source sft.cern.ch/lcg/views/LCG_101/chosen_folder/setup.sh
```
with your chosen folder. For example, with an x86_64 system and CentOS version 7, you would run:
```bash
source /cvmfs/sft.cern.ch/lcg/views/LCG_101/x86_64-centos7-gcc11-opt/setup.sh
```
Now, create a new virtual environment as above:
```bash
cd
python3 -m venv ~/.venv/my-venv
source ~/.venv/my-venv/bin/activate
```
Check where python3 is located and ensure its in your new virtual environment. Also check that it is the latest version.
```bash
which python3
python3 --version
```
Now, install the module:
```bash
cd path/to/software/python
pip install -e .
```
To activate your venv after the initial setup, follow these steps:

```bash
ssh user_name@lxplus.extension
source /cvmfs/sft.cern.ch/lcg/views/LCG_101/chosen_folder/setup.sh
cd
source ~/.venv/my-venv/bin/activate
```
