# Docker

This directory is to store Docker files to automate building images. The current requirement for the Host and DAQ ARM boards are described below

## Host 
- [Alma linux 8](https://hub.docker.com/r/cern/alma8-base)
- [gcc 9](https://www.appsloveworld.com/cplus/100/246/how-to-install-gcc-g-9-on-centos-8-docker-centoslatest)
- cmake/make
- ROOT (optional) 

## ARM board [Ref. https://pkgs.org/download/gcc-8-arm-linux-gnueabihf]
- Ubuntu 20.04 (gcc-arm with gnueabihf does not exist in CentOS - install/compile from source?)
- g++-8-arm-linux-gnueabihf
- cmake/make

Before building on your computer, open docker desktop and a terminal.
Leave docker open and work in the terminal.

First you need to login (CERN user and password):
```bash
docker login gitlab-registry.cern.ch
```

To build the [Docker](https://www.docker.com/) images on UNIX terminal, you may do:
```bash
docker build -t gitlab-registry.cern.ch/snd-scifi/software/images/host_root:test -f Dockerfile.HOST --build-arg root=ON ../
docker build -t gitlab-registry.cern.ch/snd-scifi/software/images/host:test -f Dockerfile.HOST ../
docker build -t gitlab-registry.cern.ch/snd-scifi/software/images/arm:test -f Dockerfile.ARM  ../
```
Here we are building to registry `gitlab-registry.cern.ch/snd-scifi/software/images/NAME:TAG`.  

**It is recommended to update the existing images rather than building from scratch. Please go to [Update the available images](#update-the-available-images)**

After building the image, you need to push it to the CERN gitlab registry:
```bash
docker push gitlab-registry.cern.ch/snd-scifi/software/images/xxx
```

## For testing with building process

If you want to test a Dockerfile to see if the current built image is able to host this application, simply copy and paste the codes below 


For both files 
```docker 
# Prepare directory 
RUN mkdir -p /home/software/
COPY . /home/software/
WORKDIR /home/software/
```

HOST
```docker 
RUN mkdir build-host \
    && cd build-host \ 
    && cmake DUSE_VMUSB=ON -DUSE_ROOT=$root -DCMAKE_BUILD_TYPE=Release .. \ 
    && make -j2
```

DAQ ARM Board
```docker 
RUN mkdir build-arm \
    && cd build-arm \ 
    && cmake -DARCH=arm -DUSE_ROOT=OFF -DCMAKE_BUILD_TYPE=Release .. \ 
    && make -j2

```

## Update the available images

Please familiarise yourself with [Docker command](https://docs.docker.com/engine/reference/commandline/cli/). Usually, we need only `image`, `container`, `run`, `exec`, `stats`, `ps`, `pull`, `commit`, and `push` commands. 

To manually update the image, you may follow the following steps

1. Make sure Docker is installed and Docker Daemon is running. 
2. Pull your prefered base image and tag by 

```bash
    docker pull NAME:TAG
    # Example 
    docker pull gitlab-registry.cern.ch/snd-scifi/software/images/arm:base
```

3. Run the image in a container with iteractive mode (`-it`) and bash shell (`bash`)

```bash
    docker run -it NAME:TAG bash
```

4. You will be inside a container which look like when you ssh to different computer. Then You may update packages inside the container. Then when you are done. You can exit the container by type `exit`. 

5. To commit and push the version. First, check the `CONTAINER ID` with 

```bash 
    docker ps -a
```

6. Take the ID and choose your preferred NEW_TAG. Then commit the image with 

```bash 
    docker commit CONTAINER_ID NAME:NEW_TAG
```

7. Push to the repository by

```bash 
    docker push NAME:NEW_TAG
```

## Tagging 

Please check the available tags for each architect and tags in (the Container Registry)[https://gitlab.cern.ch/snd-scifi/software/container_registry]. Each tag is described below. Note that, not every `ARCH` has all of the listing tags.


### [ARM](https://gitlab.cern.ch/snd-scifi/software/container_registry/14402) 
| Tag | comment |
|-----|---------|
| test | for personal testing | 
| base | base images  |

### [Host](https://gitlab.cern.ch/snd-scifi/software/container_registry/14376)

| Tag | comment |
|-----|---------|
| test | for personal testing | 
| base | base images | 


### [Host with ROOT](https://gitlab.cern.ch/snd-scifi/software/container_registry/14378)

| Tag | comment |
|-----|---------|
| test | for personal testing | 
| base | base images |



