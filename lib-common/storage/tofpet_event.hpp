#pragma once

#include <cstdint>
#include <vector>
#include <map>

#include "storage/tofpet_hit.hpp"

// class TofpetBoardEvent {
// public:
//   TofpetBoardEvent(const int64_t timestamp, const bool desync=false);

//   void clear(const int64_t timestamp);

//   size_t nHits() const { return m_hits.size(); }
//   int64_t timestamp() const { return m_timestamp; }
//   bool desync() const { return m_desync; }
//   bool ttcrxReadyLost() const { return m_ttcrxReadyLost; }
//   bool qpllLockedLost() const { return m_qpllLockedLost; }
//   const TofpetHit& hit(size_t i) const { return m_hits.at(i); }
//   void newTimestamp(int64_t timestamp);

//   auto begin() { return m_hits.begin(); }
//   auto end() { return m_hits.end(); }
//   auto cbegin() const { return m_hits.cbegin(); }
//   auto cend() const { return m_hits.cend(); }
//   auto begin() const { return m_hits.begin(); }
//   auto end() const { return m_hits.end(); }

//   template <typename... Params>
//   void emplaceHit(Params&&... params);

//   template <typename... Params>
//   void emplaceHitRelative(Params&&... params);

// private:
//   int64_t m_timestamp;
//   bool m_desync;
//   bool m_ttcrxReadyLost;
//   bool m_qpllLockedLost;
//   // uint8_t m_evtType;
//   // bool m_vetoOn;
//   // bool m_scifiOn;
//   // bool m_usMuonOn;
//   // bool m_dsMuonOn;

//   std::vector<TofpetHit> m_hits;


// };


class TofpetEvent {
public:
  // TofpetEvent(const int64_t timestamp = 0, const uint64_t evtNumber = 0, const uint64_t flags = 0);
  TofpetEvent(const int64_t timestamp = 0, const uint64_t evtNumber = 0, const uint64_t flags = 0);

  void clear(const int64_t timestamp, const uint64_t evtNumber = 0, const uint64_t flags = 0);

  // void setBoardData(const uint32_t boardId, TofpetBoardEvent&& boardEvent);
  // void emplaceBoardEvent(const uint32_t boardId, TofpetBoardEvent&& boardEvent);
  // void fillMissingBoardEvents(const std::vector<uint32_t>& boardIds);

  template <typename... Params>
  void emplaceHit(Params&&... params);

  template <typename... Params>
  void emplaceHitRelative(Params&&... params);


  int64_t timestamp() const { return m_timestamp; }
  uint64_t evtNumber() const { return m_evtNumber; }
  void evtNumber(uint64_t evtNumber) { m_evtNumber = evtNumber; }
  uint64_t flags() const { return m_flags; }
  void setFlags(const uint64_t mask, const uint64_t value);
  uint64_t getFlags(const uint64_t mask) const;
  bool desync() const { return m_desync; }
  bool ttcrxReadyLost() const { return m_ttcrxReadyLost; }
  bool qpllLockedLost() const { return m_qpllLockedLost; }
  // const TofpetBoardEvent& boardEvent(uint32_t boardId) const { return m_boardEvents.at(boardId); }
  // bool hasBoardEvent(uint32_t boardId) const { return m_boardEvents.count(boardId); }
  size_t nHits() const;

  auto begin() { return m_hits.begin(); }
  auto end() { return m_hits.end(); }
  auto cbegin() const { return m_hits.cbegin(); }
  auto cend() const { return m_hits.cend(); }
  auto begin() const { return m_hits.begin(); }
  auto end() const { return m_hits.end(); }

private:
  int64_t m_timestamp;
  uint64_t m_evtNumber;
  uint64_t m_flags;
  bool m_desync;
  bool m_ttcrxReadyLost;
  bool m_qpllLockedLost;
  // uint8_t m_evtType;
  // bool m_vetoOn;
  // bool m_scifiOn;
  // bool m_usMuonOn;
  // bool m_dsMuonOn;

  // std::map<uint32_t, TofpetBoardEvent> m_boardEvents;

  std::vector<TofpetHit> m_hits;

};

// template <typename... Params>
// void TofpetEvent::emplaceHit(uint32_t id, Params&&... params) {
//   m_hitMap[id].emplace_back(std::forward(params)...);
// }


template <typename... Params>
/**
 * Emplaces a hit in the hit vector.
*/
void TofpetEvent::emplaceHit(Params&&... params) {
  m_hits.emplace_back(std::forward<Params>(params)...);
  if (m_hits.back().ttcrxReadyLost()) {
    m_ttcrxReadyLost = true;
  }
  if (m_hits.back().qpllLockedLost()) {
    m_qpllLockedLost = true;
  }
}

template <typename... Params>
/**
 * Emplaces a hit in the hit vector rescaling its timestamp relative to the event timestamp.
*/
void TofpetEvent::emplaceHitRelative(Params&&... params) {
  m_hits.emplace_back(std::forward<Params>(params)...);
  m_hits.back().relativeTCoarse(m_timestamp);
  if (m_hits.back().ttcrxReadyLost()) {
    m_ttcrxReadyLost = true;
  }
  if (m_hits.back().qpllLockedLost()) {
    m_qpllLockedLost = true;
  }
}
