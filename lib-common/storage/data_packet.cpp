#include <endian.h>

#include "utils/logger.hpp"

#include "data_packet.hpp"

using json = nlohmann::json;

/**
 * Accepts an iterator to a std::vector containing data to be converted to packets and the iterator pointing to the end of said vector.
 * It increases the iterator it until a valid packet is found or the end is reached.
 */
DataPacket DataPacket::createPacket(uint32_t boardId, std::vector<uint32_t>::const_iterator& it, std::vector<uint32_t>::const_iterator end) {
  std::array<uint8_t, 4> headers;
  std::array<uint32_t, 4> packetData;

  // increases iterator until it finds a valid first packet header (0, 4, 8 or C)
  auto searchPacketBeginning = [&it, &end] () {
    while ((*it & 0xF0000000) % 0x40000000 != 0) {
      WARN("invalid first packet header '{:#x}', searching valid packet.", (*it & 0xF0000000)>>28);
      it++;
      if (it == end) {
        return;
      }
    }
  };

  // put the iterator at the beginning of the first packet (ideally this should do nothing, if data is ok)
  searchPacketBeginning();

  // read 
  for (int i = 0; i < 4; i++) {
    if (it == end) {
      // this should happen only if there are partial packets in the data stream, assuming that data is read from the xillybus in multiples of 4
      WARN("data ended before a packet could be completed. Returning invalid packet.");
      // roll back the iterator to the beginning of the packet
      it -= i;
      return InvalidPacket(boardId, {0, 0, 0, 0});
    }
    // read the packet header
    headers[i] = (*it & 0xF0000000) >> 28;
    // verify that the packet headers are consistent
    if (i != 0 && headers[i] != headers[i-1]+1) {
      WARN("Inconsistent packet header. {} {}", headers[i-1], headers[i]);
      // if not, search for another baginning of packet
      searchPacketBeginning();
      // and set i to -1 to restart (it will be increased to 0 with continue)
      i = -1;
      continue;
    }
    // if everything is fine, store the data and increase the iterator
    packetData[i] = *it;
    it++;
  }

  // determine the packet type
  switch (headers[0]) {
  case 0x0:
    if ((packetData[3] & 0xF0CC0000) == 0x30080000) {
      return HitPacket(boardId, packetData);
    }
    else {
      WARN("data headers valid (hit packet), but data content is inconsistent.");
      return DataPacket(boardId, packetData, None);
    }
  case 0x4:
    if ((packetData[2] & 0xFFC00000) == 0x60000000) {
      return TriggerPacket(boardId, packetData);
    }
    else {
      WARN("data headers valid (trigger packet), but data content is inconsistent.");
      return DataPacket(boardId, packetData, None);
    }
  case 0x8:
    if ((packetData[1] & 0x0FF00000) == 0x00100000) {
      return CounterPacket(boardId, packetData);
    }
    else if ((packetData[1] & 0x0C000000) == 0x08000000) {
      return RawHitPacket(boardId, packetData);
    }
    else if (packetData[0] == 0x8ABBCCDD && packetData[1] == 0x9000000A) {
      return HeartbeatPacket(boardId, packetData);
    }
    else {
      WARN("data headers valid (raw packet), but could not determine type.");
      return RawPacket(boardId, packetData);
    }
  case 0xC:
    if ((packetData[2] & 0x0F000000) == 0 && (packetData[3] & 0x0FFFFFFF) == 0) {
      return BchannelPacket(boardId, packetData);
    }
    else {
      WARN("data headers valid (B-channel packet), but data content is inconsistent.");
      return DataPacket(boardId, packetData, None);
    }
  default:
    ERROR("Packed data is invalid. This should never be called.");
    return InvalidPacket(boardId, packetData);
  }
}

void DataPacket::serializePacket(char* const dest) const {
  dest[0] = static_cast<uint8_t>(m_boardId);
  for (int i = 0; i < 4; i++) {
    // make data little endian
    auto d = htole32(m_data[i]);
    // store the 32 bit value in 4x 8-bit numbers in dest
    for (int j = 0; j < 4; j++) {
      dest[i*4 + j + 1] = static_cast<char>(d & 0xFF);
      d >>= 8;
    }
  }
}

DataPacket DataPacket::deserializePacket(const char* const data) {
  std::vector<uint32_t> packetData(4, 0);
  for (int i = 0; i < 16; i++) {
    packetData.at(i/4) |= static_cast<uint32_t>(static_cast<unsigned char>(data[i+1])) << ((i%4) * 8);
  }
  for (auto& d : packetData) {
    d = le32toh(d);
  }
  auto it = packetData.cbegin();
  return createPacket(data[0], it, packetData.end());
}


int64_t HitPacket::timestampCoarse() const {
  return ((m_data[2] & 0xFFull) << 56) + ((m_data[1] & 0x0FFFFFFFull) << 28) + (m_data[0] & 0x0FFFFFFFull);
}

uint16_t HitPacket::timestampFine() const {
  return (m_data[2] & 0x3FF00) >> 8;
}

uint16_t HitPacket::valueCoarse() const {
  return (m_data[2] & 0x0FFC0000) >> 18;
}

uint16_t HitPacket::valueFine() const {
  return m_data[3] & 0x3FF;
}

uint8_t HitPacket::tofpetChannel() const {
  return (m_data[3] & 0x0000FC00) >> 10;
}

uint8_t HitPacket::tac() const {
  return (m_data[3] & 0x00030000) >> 16;
}

// uint8_t HitPacket::flags() const {
//   return (m_data[3] & 0xF800000) >> 23;
// }

uint8_t HitPacket::tofpetId() const {
  return (m_data[3] & 0x07000000) >> 24;
}

bool HitPacket::ttcrxReadyLost() const {
  return m_data[3] & 0x00100000;
}

bool HitPacket::qpllLockedLost() const {
  return m_data[3] & 0x00200000;
}



int64_t TriggerPacket::timestamp() const {
  return ((m_data[2] & 0xFFull) << 56) + ((m_data[1] & 0x0FFFFFFFull) << 28) +  (m_data[0] & 0x0FFFFFFFull);
}

uint8_t TriggerPacket::triggerType() const {
  return (m_data[2] & 0xF00) >> 8;
}

uint32_t TriggerPacket::counter() const {
  return m_data[3] & 0x0FFFFFFF;
}

uint16_t TriggerPacket::flags() const {
  return (m_data[2] & 0xFF00000) >> 20;
}

uint8_t TriggerPacket::boardIdTrigger() const {
  return (m_data[2] & 0xFF000) >> 12;
}

bool TriggerPacket::ttcrxReadyLost() const {
  return m_data[2] & 0x00100000;
}

bool TriggerPacket::qpllLockedLost() const {
  return m_data[2] & 0x00200000;
}



uint8_t CounterPacket::channel() const {
  return ((m_data[0] & 0x0F000000) >> 24) + ((m_data[1] & 0x3) << 4);
}

uint32_t CounterPacket::value() const {
  return m_data[0] & 0x00FFFFFF;
}

int64_t CounterPacket::timestamp() const {
  return ((m_data[3] & 0x00FFFFFFull) << 28) +  (m_data[2] & 0x0FFFFFFFull);
}

uint8_t CounterPacket::tofpetId() const {
  return (m_data[3] & 0x07000000) >> 24;
}



int64_t RawHitPacket::timestampCoarse() const {
  uint64_t ts_160 = ((m_data[3] & 0x00FFFFFFull) << 28) +  (m_data[2] & 0x0FFFFFFFull);
  uint64_t t_coarse = (m_data[1] & 0x0003FFFC) >> 2;
  if (t_coarse <= (ts_160 & 0xFFFF)) { //TODO check
    return (ts_160 & 0xFFFFFFFFFFFF0000) | t_coarse;
  }
  else {
    return ((ts_160 - 0x10000) & 0xFFFFFFFFFFFF0000) | t_coarse;
  }
}

uint16_t RawHitPacket::timestampFine() const {
  return (m_data[0] & 0xFFC00) >> 10;
}

uint16_t RawHitPacket::valueCoarse() const {
  return ((m_data[0] & 0x0FF00000) >> 20) + ((m_data[1] & 0x3) << 8);
}

uint16_t RawHitPacket::valueFine() const {
  return m_data[0] & 0x3FF;
}

uint8_t RawHitPacket::tofpetChannel() const {
  return (m_data[1] & 0x03F00000) >> 20;
}

uint8_t RawHitPacket::tac() const {
  return (m_data[1] & 0x000C0000) >> 18;
}

uint8_t RawHitPacket::tofpetId() const {
  return (m_data[3] & 0x07000000) >> 24;
}



uint64_t RawPacket::dataWord() const {
  return ((m_data[1] & 0x0FFFFFFFull) << 28) +  (m_data[0] & 0x0FFFFFFFull);
}

int64_t RawPacket::timestamp() const {
  return ((m_data[3] & 0x00FFFFFFull) << 28) +  (m_data[2] & 0x0FFFFFFFull);
}


int64_t BchannelPacket::timestamp() const {
  return ((m_data[2] & 0xFFull) << 56) + ((m_data[1] & 0x0FFFFFFFull) << 28) + (m_data[0] & 0x0FFFFFFFull);
}

uint8_t BchannelPacket::dout() const {
  return (m_data[2] & 0xFF00) >> 8;
}

uint8_t BchannelPacket::subaddr() const {
  return (m_data[2] & 0xFF0000) >> 16;
}

uint16_t BchannelPacket::subaddrDout() const {
  return (m_data[2] & 0xFFFF00) >> 8;
}


std::ostream& operator<<(std::ostream& out, const DataPacket packet) {
  return out << "DATA PACKET [" << std::hex 
    << packet.data()[0] << ", "
    << packet.data()[1] << ", "
    << packet.data()[2] << ", "
    << packet.data()[3] << "]" << std::dec;
}

std::ostream& operator<<(std::ostream& out, const HitPacket packet){
  return out << "HIT PACKET";
}

std::ostream& operator<<(std::ostream& out, const TriggerPacket packet){
  return out << "TRIGGER PACKET";
}

std::ostream& operator<<(std::ostream& out, const CounterPacket packet) {
  return out << "COUNTER PACKET";
}

std::ostream& operator<<(std::ostream& out, const HeartbeatPacket packet){
  return out << "HEARTBEAT PACKET";
}

std::ostream& operator<<(std::ostream& out, const RawPacket packet){
  return out << "RAW PACKET";
}

std::ostream& operator<<(std::ostream& out, const RawHitPacket packet){
  return out << "RAW HIT PACKET";
}

std::ostream& operator<<(std::ostream& out, const BchannelPacket packet){
  return out << "B_CHANNEL PACKET";
}

std::ostream& operator<<(std::ostream& out, const InvalidPacket packet){
  return out << "INVALID PACKET";
}

std::ostream& operator<<(std::ostream& out, const DataPacket::DataPacketType type) {
  switch (type)
  {
  case DataPacket::None:
    return out << "NONE";
  case DataPacket::Hit:
    return out << "HIT";
  case DataPacket::Trigger:
    return out << "TRIGGER";
  case DataPacket::Counter:
    return out << "COUNTER";
  case DataPacket::Heartbeat:
    return out << "HEARTBEAT";
  case DataPacket::Raw:
    return out << "RAW";
  case DataPacket::RawHit:
    return out << "RAW_HIT";
  case DataPacket::Bchannel:
    return out << "B_CHANNEL";
  case DataPacket::Invalid:
    return out << "INVALID";
  default:
    return out << "***";
  }
}

void to_json(json& j, const DataPacket& p) {
  j = json(p.data());
}

void from_json(const json& j, DataPacket& p) {
  // p = DataPacket::createPacket(j.get<std::array<uint32_t, 4>>().data()); 
  auto arr = j.get<std::vector<uint32_t>>();
  auto it = arr.cbegin();
  p = DataPacket::createPacket(0, it, arr.cend()); // TODO see if this is used somewhere...
}