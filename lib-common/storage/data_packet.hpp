#pragma once

#include <cstdint>
#include <array>
#include <vector>
#include <iostream>

#include "json.hpp"

class DataPacket {
public:
  enum DataPacketType {
    None,
    Hit,
    Trigger,
    Counter,
    Heartbeat,
    Raw,
    RawHit,
    Bchannel,
    Invalid
  };
  DataPacket() : m_boardId{0}, m_data({0, 0, 0, 0}), m_type(Invalid) {}
  DataPacket(uint32_t boardId, std::array<uint32_t, 4> data, DataPacketType type) : m_boardId{boardId}, m_data(data), m_type(type) {}
  virtual ~DataPacket() = default;
  uint32_t boardId() const { return m_boardId; }
  const std::array<uint32_t, 4>& data() const { return m_data; }
  DataPacketType type() const { return m_type; }

  // static DataPacket createPacket(const uint32_t* data);
  static DataPacket createPacket(uint32_t boardId, std::vector<uint32_t>::const_iterator& it, std::vector<uint32_t>::const_iterator end);

  void serializePacket(char* const dest) const;
  static DataPacket deserializePacket(const char* const data);

protected:
  uint32_t m_boardId;
  std::array<uint32_t, 4> m_data;
  DataPacketType m_type;

};

// using DataPacketId = std::pair<uint32_t, DataPacket>; // TODO remove

class HitPacket : public DataPacket {
public:
  HitPacket(uint32_t boardId, std::array<uint32_t, 4> data) : DataPacket(boardId, data, DataPacketType::Hit) {}
  
  // uint16_t channel() const;
  int64_t timestampCoarse() const;
  uint16_t timestampFine() const;
  uint16_t valueCoarse() const;
  uint16_t valueFine() const;
  uint8_t tofpetChannel() const;
  uint8_t tofpetId() const;
  uint8_t tac() const;
  bool ttcrxReadyLost() const;
  bool qpllLockedLost() const;
  // uint8_t flags() const;
};

class TriggerPacket : public DataPacket {
public:
  TriggerPacket(uint32_t boardId, std::array<uint32_t, 4> data) : DataPacket(boardId, data, DataPacketType::Trigger) {}

  int64_t timestamp() const;
  uint8_t triggerType() const;
  uint32_t counter() const;
  uint8_t boardIdTrigger() const;
  uint16_t flags() const;
  bool ttcrxReadyLost() const;
  bool qpllLockedLost() const;
};

class CounterPacket : public DataPacket {
public:
  CounterPacket(uint32_t boardId, std::array<uint32_t, 4> data) : DataPacket(boardId, data, DataPacketType::Counter) {}

  uint8_t channel() const;
  uint32_t value() const;
  int64_t timestamp() const;
  uint8_t tofpetId() const;
};

class HeartbeatPacket : public DataPacket {
public:
  HeartbeatPacket(uint32_t boardId, std::array<uint32_t, 4> data) : DataPacket(boardId, data, DataPacketType::Heartbeat) {}

  uint8_t tofpetId() const;
};

class RawHitPacket : public DataPacket {
public:
  RawHitPacket(uint32_t boardId, std::array<uint32_t, 4> data) : DataPacket(boardId, data, DataPacketType::RawHit) {}
  
  // uint16_t channel() const;
  int64_t timestampCoarse() const;
  uint16_t timestampFine() const;
  uint16_t valueCoarse() const;
  uint16_t valueFine() const;
  uint8_t tofpetChannel() const;
  uint8_t tofpetId() const;
  uint8_t tac() const;
  // uint8_t flags() const;
};

class RawPacket : public DataPacket {
public:
  RawPacket(uint32_t boardId, std::array<uint32_t, 4> data) : DataPacket(boardId, data, DataPacketType::Raw) {}

  uint64_t dataWord() const;
  int64_t timestamp() const;
};

class BchannelPacket : public DataPacket {
public:
  BchannelPacket(uint32_t boardId, std::array<uint32_t, 4> data) : DataPacket(boardId, data, DataPacketType::Bchannel) {}

  int64_t timestamp() const;
  uint8_t dout() const;
  uint8_t subaddr() const;
  uint16_t subaddrDout() const;
};

class InvalidPacket : public DataPacket {
public:
  InvalidPacket(uint32_t boardId, std::array<uint32_t, 4> data) : DataPacket(boardId, data, DataPacketType::Invalid) {}
};

std::ostream& operator<<(std::ostream& out, const DataPacket packet);
std::ostream& operator<<(std::ostream& out, const HitPacket packet);
std::ostream& operator<<(std::ostream& out, const TriggerPacket packet);
std::ostream& operator<<(std::ostream& out, const CounterPacket packet);
std::ostream& operator<<(std::ostream& out, const HeartbeatPacket packet);
std::ostream& operator<<(std::ostream& out, const RawPacket packet);
std::ostream& operator<<(std::ostream& out, const RawHitPacket packet);
std::ostream& operator<<(std::ostream& out, const BchannelPacket packet);
std::ostream& operator<<(std::ostream& out, const InvalidPacket packet);

std::ostream& operator<<(std::ostream& out, const DataPacket::DataPacketType type);

void to_json(nlohmann::json& j, const DataPacket& p); 
void from_json(const nlohmann::json& j, DataPacket& p);