#include "tofpet_event.hpp"

/**
 * Returns the left shift coresponding to a mask, i.e. the number of zeros on the right.
 * E.g. a mask of 0b00111000 would return 3.
 */
constexpr int maskToShift(uint64_t mask) {
    int c = 64; // c will be the number of zero bits on the right
    mask &= -mask;
    if (mask) c--;
    if (mask & 0x00000000FFFFFFFF) c -= 32;
    if (mask & 0x0000FFFF0000FFFF) c -= 16;
    if (mask & 0x00FF00FF00FF00FF) c -= 8;
    if (mask & 0x0F0F0F0F0F0F0F0F) c -= 4;
    if (mask & 0x3333333333333333) c -= 2;
    if (mask & 0x5555555555555555) c -= 1;

    return c;
}

// TofpetBoardEvent::TofpetBoardEvent(const int64_t timestamp, const bool desync)
//   : m_timestamp(timestamp)
//   , m_desync{desync}
//   , m_ttcrxReadyLost{false}
//   , m_qpllLockedLost{false}
// {}

// void TofpetBoardEvent::clear(const int64_t timestamp) {
//   m_timestamp = timestamp;
//   m_desync = false;
//   m_ttcrxReadyLost = false;
//   m_qpllLockedLost = false;
//   m_hits.clear();
// }


// void TofpetBoardEvent::newTimestamp(int64_t timestamp) {
//   for (auto& hit : m_hits) {
//     hit.relativeTCoarse(timestamp - m_timestamp);
//   }
//   m_timestamp = timestamp;
// }



// TofpetEvent::TofpetEvent(const int64_t timestamp, const uint64_t evtNumber, const uint64_t flags)
//   : m_timestamp{timestamp}
//   , m_evtNumber{evtNumber}
//   , m_flags{flags}
//   , m_desync{false}
//   , m_ttcrxReadyLost{false}
//   , m_qpllLockedLost{false}
// {}

TofpetEvent::TofpetEvent(const int64_t timestamp, const uint64_t evtNumber, const uint64_t flags)
  : m_timestamp{timestamp}
  , m_evtNumber{evtNumber}
  , m_flags{flags}
  , m_desync{false}
  , m_ttcrxReadyLost{false}
  , m_qpllLockedLost{false}
{
  // for (const auto boardId : boardIds) {
  //   m_boardEvents.emplace(boardId, timestamp);
  // }
}

void TofpetEvent::clear(const int64_t timestamp, const uint64_t evtNumber, const uint64_t flags) {
  m_timestamp = timestamp;
  m_evtNumber = evtNumber;
  m_flags = flags;
  m_desync = false;
  m_ttcrxReadyLost = false;
  m_qpllLockedLost = false;
  // for (auto& [id, evt] : m_boardEvents) {
  //   evt.clear(timestamp);
  // }
  m_hits.clear();
}


void TofpetEvent::setFlags(const uint64_t mask, const uint64_t value) {
  // clear the bits identified by the mask
  m_flags &= ~mask;
  // shift the value to the correct position
  // AND with the mask, to make sure we modify the bits outside the mask
  // OR with the flags, to write the bits there
  m_flags |= (value << maskToShift(mask)) & mask;
}


uint64_t TofpetEvent::getFlags(const uint64_t mask) const {
  return (m_flags & mask) >> maskToShift(mask);
}

// void TofpetEvent::setBoardData(const uint32_t boardId, TofpetBoardEvent&& boardEvent) {
//   m_boardEvents.at(boardId) = std::move(boardEvent);
//   if (m_boardEvents.at(boardId).desync()) {
//     m_desync = true;
//   }
//   if (m_boardEvents.at(boardId).ttcrxReadyLost()) {
//     m_ttcrxReadyLost = true;
//   }
//   if (m_boardEvents.at(boardId).qpllLockedLost()) {
//     m_qpllLockedLost = true;
//   }
// }

// void TofpetEvent::emplaceBoardEvent(const uint32_t boardId, TofpetBoardEvent&& boardEvent) {
//   m_boardEvents.try_emplace(boardId, boardEvent);
//   m_boardEvents.at(boardId).newTimestamp(m_timestamp);
// }

// void TofpetEvent::fillMissingBoardEvents(const std::vector<uint32_t>& boardIds) {
//   for (const auto boardId : boardIds) {
//     m_boardEvents.try_emplace(boardId, m_timestamp);
//   }
// }

size_t TofpetEvent::nHits() const {
  return m_hits.size();
}