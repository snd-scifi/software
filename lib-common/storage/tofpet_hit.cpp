#include "utils/calibration.hpp"
#include "utils/logger.hpp"

#include "tofpet_hit.hpp"

TofpetHit::TofpetHit(const HitPacket&& packet) 
  : m_boardId{packet.boardId()}
  , m_tofpetId{packet.tofpetId()}
  , m_tofpetChannel{packet.tofpetChannel()}
  , m_tac{packet.tac()}
  , m_tCoarse{packet.timestampCoarse()}
  , m_tFine{packet.timestampFine()}
  , m_timestamp{std::numeric_limits<double>::quiet_NaN()}
  , m_timestampCalChi2{std::numeric_limits<float>::quiet_NaN()}
  , m_timestampCalDof{std::numeric_limits<float>::quiet_NaN()}
  , m_vCoarse{packet.valueCoarse()}
  , m_vFine{packet.valueFine()}
  , m_value{std::numeric_limits<double>::quiet_NaN()}
  , m_valueCalChi2{std::numeric_limits<float>::quiet_NaN()}
  , m_valueCalDof{std::numeric_limits<float>::quiet_NaN()}
  , m_valueSaturation{std::numeric_limits<float>::quiet_NaN()}
  , m_ttcrxReadyLost{packet.ttcrxReadyLost()}
  , m_qpllLockedLost{packet.qpllLockedLost()}
  {}

TofpetHit::TofpetHit(uint32_t boardId, uint8_t tofpetId, uint8_t tofpetChannel, uint8_t tac, int64_t tCoarse, uint16_t tFine, uint16_t vCoarse, uint16_t vFine, double timestamp, float timestampCalChi2, float timestampCalDof, double value, float valueCalChi2, float valueCalDof, float valueSaturation) 
  : m_boardId{boardId}
  , m_tofpetId{tofpetId}
  , m_tofpetChannel{tofpetChannel}
  , m_tac{tac}
  , m_tCoarse{tCoarse}
  , m_tFine{tFine}
  , m_timestamp{timestamp}
  , m_timestampCalChi2{timestampCalChi2}
  , m_timestampCalDof{timestampCalDof}
  , m_vCoarse{vCoarse}
  , m_vFine{vFine}
  , m_value{value}
  , m_valueCalChi2{valueCalChi2}
  , m_valueCalDof{valueCalDof}
  , m_valueSaturation{valueSaturation}
  , m_ttcrxReadyLost{false}
  , m_qpllLockedLost{false}
{}


void TofpetHit::calculateTimestamp(double a, double b, double c, double d, float chi2, float dof, double doT1delay) {
  m_timestamp = m_tCoarse + tdcCalibration(m_tFine, a, b, c, d) - doT1delay;
  m_timestampCalChi2 = chi2;
  m_timestampCalDof = dof;
}


void TofpetHit::calculateValueQdc(double a, double b, double c, double d, double e, float chi2, float dof, uint16_t intgTimeMin, uint16_t intgTimeMax, double doT1delay, float gain) {
  if (!hasTimestamp()) {
    THROW_RE("TofpetHit::calculateValueQdc: timestamp not available. Run calculateTimestamp before calculateValueQdc.");
  }
  // this is to fix a bug present until July 2022, where it could happen that the minimum integration time was larger than the maximum.
  // According to the datasheet, in such case the integration time is actually driven by the maximum.
  if (intgTimeMax < intgTimeMin) {
    intgTimeMin = intgTimeMax;
  }

  // this corrects for an effect observed with SciFi at low light intensity, where the reported v_coarse was shorter than what is set by intgTimeMin
  uint16_t vCoarseCorrected = static_cast<int>(m_vCoarse) - intgTimeMin < 1 ? intgTimeMin + 1 : m_vCoarse;
  
  m_value = (m_vFine - qdcCalibration(vCoarseCorrected - calibratedTimestampFine() + doT1delay, a, b, c, d, e)) / gain;
  m_valueCalChi2 = chi2;
  m_valueCalDof = dof;
  m_valueSaturation = m_vFine / d;
}


void TofpetHit::calculateValueTot(double a, double b, double c, double d, float chi2, float dof, double doT1delay, uint16_t vCoarseMax) {
  if (!hasTimestamp()) {
    THROW_RE("TofpetHit::calculateValueTot: timestamp not available. Run calculateTimestamp before calculateValueTot.");
  }
  int16_t vCoarseCorrected = vCoarseMax < m_vCoarse ? m_vCoarse - 1024 : m_vCoarse;
  m_value = vCoarseCorrected + tdcCalibration(m_vFine, a, b, c, d) - calibratedTimestampFine() + doT1delay;
  m_valueCalChi2 = chi2;
  m_valueCalDof = dof;
}