#pragma once

#include <deque>
#include <cassert>
#include <mutex>
#include <condition_variable>
#include <thread>
#include <chrono>

#include "storage/deque_mt.hpp"
#include "storage/tofpet_hit.hpp"


class HitQueue : public DequeMt<TofpetHit> {
public:
  HitQueue() : DequeMt<TofpetHit>() {}
  HitQueue(const HitQueue& rhs) = delete;
  HitQueue &operator=(const HitQueue &rhs) = delete;

  void insertSorted(TofpetHit&& hit) {
    {
      std::lock_guard<std::mutex> lock(m_mutex);
      // start looking from the end (crbegin is the const revere iterator)
      for (auto it = m_queue.crbegin(); ; it++) {
        // insert the new hit right after the first hit with a greater or equal timestamp (or at the beginning) 
        if (it == m_queue.crend() || (*it).tCoarse() <= hit.tCoarse()) {
          m_queue.emplace(it.base(), hit);
          break;
        }
      }
    }
    m_condvar.notify_all();
  }

};