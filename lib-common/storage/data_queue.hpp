#pragma once

#include "storage/deque_mt.hpp"
#include "storage/data_packet.hpp"

using DataPacketQueue = DequeMt<DataPacket>;