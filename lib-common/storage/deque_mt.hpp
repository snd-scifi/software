#pragma once

#include <deque>
#include <cassert>
#include <mutex>
#include <condition_variable>
#include <thread>
#include <chrono>

template <typename T>
class DequeMt {
public:
  DequeMt() : m_mutex(), m_condvar(), m_queue() {}
  DequeMt(const DequeMt& rhs) = delete;
  DequeMt &operator=(const DequeMt &rhs) = delete;

  void put(const T& element) {
    {
      std::lock_guard<std::mutex> lock(m_mutex);
      m_queue.push_back(element);
    }
    m_condvar.notify_all();
  }

  void emplace_back(T&& element) {
    {
      std::lock_guard<std::mutex> lock(m_mutex);
      m_queue.emplace_back(std::forward<T>(element));
    }
    m_condvar.notify_all();
  }

  void putBlock(std::deque<T>& block) {
    {
      std::lock_guard<std::mutex> lock(m_mutex);
      m_queue.insert(m_queue.end(), std::make_move_iterator(block.begin()), std::make_move_iterator(block.end()));
    }
    m_condvar.notify_all();
    block.clear();
  }

  T take() {
    using namespace std::chrono_literals;
    std::unique_lock<std::mutex> lock(m_mutex);
    m_condvar.wait(lock, [this] { return !m_queue.empty(); });
    
    T front(std::move(m_queue.front()));
    m_queue.pop_front();

    return front;
  }

/**
 * Retuns all the data contained in the queue and clears it.
 * Blocks for 1 second if no data is present.
 */
  std::deque<T> takeAll(unsigned int timeout = 1000) {
    std::unique_lock<std::mutex> lock(m_mutex);
    m_condvar.wait_for(lock, std::chrono::milliseconds(timeout), [this] { return !m_queue.empty(); });
    
    std::deque<T> retQueue(std::move(m_queue));
    m_queue.clear();

    return retQueue;
  }

  bool empty() {
    std::lock_guard<std::mutex> lock(m_mutex);
    return m_queue.empty();
  }

  size_t size() {
    std::lock_guard<std::mutex> lock(m_mutex);
    return m_queue.size();
  }


protected:
  std::mutex m_mutex;
  std::condition_variable m_condvar;
  std::deque<T> m_queue;
};