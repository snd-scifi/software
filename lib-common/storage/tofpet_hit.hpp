#pragma once

#include <cstdint>
#include "json.hpp"
#include "storage/data_packet.hpp"

class TofpetHit {
public:
  TofpetHit(const HitPacket&& packet);
  TofpetHit(
    uint32_t boardId,
    uint8_t tofpetId,
    uint8_t tofpetChannel,
    uint8_t tac,
    int64_t tCoarse,
    uint16_t tFine,
    uint16_t m_vCoarse,
    uint16_t vFine,
    double timestamp=std::numeric_limits<double>::quiet_NaN(),
    float timestampCalChi2=std::numeric_limits<float>::quiet_NaN(),
    float timestampCalDof=std::numeric_limits<float>::quiet_NaN(),
    double value=std::numeric_limits<double>::quiet_NaN(),
    float valueCalChi2=std::numeric_limits<float>::quiet_NaN(),
    float valueCalDof=std::numeric_limits<float>::quiet_NaN(),
    float valueSaturation=std::numeric_limits<float>::quiet_NaN()
  );

  enum class Slot { A, B, C, D, Invalid = -1 };

  auto boardId() const { return m_boardId; }
  auto slotId() const { return static_cast<Slot>(m_tofpetId >> 1); }
  auto tofpetId() const { return m_tofpetId; }
  auto tofpetChannel() const { return m_tofpetChannel; }
  auto tac() const { return m_tac; }

  auto tCoarse() const { return m_tCoarse; }
  void relativeTCoarse(int64_t eventTimestamp) { m_tCoarse -= eventTimestamp; }
  auto tFine() const { return m_tFine; }
  auto timestamp() const { return m_timestamp; }
  auto hasTimestamp() const { return m_timestamp != std::numeric_limits<double>::quiet_NaN(); }
  auto timestampCalChi2() const { return m_timestampCalChi2; }
  auto timestampCalDof() const { return m_timestampCalDof; }

  void calculateTimestamp(std::array<float, 6> parameters, double doT1delay=0.0) { calculateTimestamp(parameters[0], parameters[1], parameters[2], parameters[3], parameters[4], parameters[5], doT1delay); }
  void calculateTimestamp(double a, double b, double c, double d, float chi2, float dof, double doT1delay=0.0);

  auto vCoarse() const { return m_vCoarse; }
  auto vFine() const { return m_vFine; }
  auto value() const { return m_value; }
  auto hasValue() const { return m_value != std::numeric_limits<double>::quiet_NaN(); }
  auto valueCalChi2() const { return m_valueCalChi2; }
  auto valueCalDof() const { return m_valueCalDof; }
  auto valueSaturation() const { return m_valueSaturation; }

  void calculateValueQdc(std::array<float, 7> parameters, uint16_t intgTimeMin, uint16_t intgTimeMax, double doT1delay=0.0, float gain=1.0) { calculateValueQdc(parameters[0], parameters[1], parameters[2], parameters[3], parameters[4], parameters[5], parameters[6], intgTimeMin, intgTimeMax, doT1delay, gain); }
  void calculateValueQdc(double a, double b, double c, double d, double e, float chi2, float dof, uint16_t intgTimeMin, uint16_t intgTimeMax, double doT1delay=0.0, float gain=1.0);

  void calculateValueTot(std::array<float, 6> parameters, double doT1delay=0.0, uint16_t vCoarseMax=1023) { calculateValueTot(parameters[0], parameters[1], parameters[2], parameters[3], parameters[4], parameters[5], doT1delay, vCoarseMax); }
  void calculateValueTot(double a, double b, double c, double d, float chi2, float dof, double doT1delay=0.0, uint16_t vCoarseMax=1023);

  bool ttcrxReadyLost() const { return m_ttcrxReadyLost; }
  bool qpllLockedLost() const { return m_qpllLockedLost; }

protected:
  uint32_t m_boardId;
  uint8_t m_tofpetId;
  uint8_t m_tofpetChannel;
  uint8_t m_tac;
  int64_t m_tCoarse;
  uint16_t m_tFine;
  double m_timestamp;
  float m_timestampCalChi2;
  float m_timestampCalDof;
  uint16_t m_vCoarse;
  uint16_t m_vFine;
  double m_value;
  float m_valueCalChi2;
  float m_valueCalDof;
  float m_valueSaturation;

  bool m_ttcrxReadyLost;
  bool m_qpllLockedLost;

  double calibratedTimestampFine() const { return m_timestamp - m_tCoarse; }
};

NLOHMANN_JSON_SERIALIZE_ENUM(TofpetHit::Slot, {
    {TofpetHit::Slot::Invalid, "invalid"},
    {TofpetHit::Slot::A, "A"},
    {TofpetHit::Slot::B, "B"},
    {TofpetHit::Slot::C, "C"},
    {TofpetHit::Slot::D, "D"},
})