#pragma once

#include <deque>
#include <cassert>
#include <mutex>
#include <condition_variable>
#include <thread>
#include <chrono>

#include "storage/deque_mt.hpp"
#include "storage/tofpet_event.hpp"


class EventQueue : public DequeMt<TofpetEvent> {
public:
  EventQueue() : DequeMt<TofpetEvent>() {}
  EventQueue(const EventQueue& rhs) = delete;
  EventQueue &operator=(const EventQueue &rhs) = delete;

  void insertSorted(TofpetEvent&& event) {
    {
      std::lock_guard<std::mutex> lock(m_mutex);
      // start looking from the end (crbegin is the const revere iterator)
      for (auto it = m_queue.crbegin(); ; it++) {
        // insert the new event right after the first event with a greater or equal timestamp (or at the beginning) 
        if (it == m_queue.crend() || (*it).timestamp() <= event.timestamp()) {
          m_queue.emplace(it.base(), event);
          break;
        }
      }
    }
    m_condvar.notify_all();
  }

};