#pragma once

#include <cstdint>
#include <string>
#include <deque>
#include <vector>

#include "network/common.hpp"
#include "network/common_client.hpp"
#include "network/connection.hpp"
#include "storage/data_packet.hpp"

// TODO double buffer vector
// TODO trasmission in separate thread, alternates between buffers
// TODO reserve buffers from the beginning
class DataClient {
public:
  DataClient(std::string serverAddress, std::string serverPort, uint32_t boardId, bool buildPackets);
  ~DataClient();

  void connect();
  void disconnect();
  //TODO bool isConnected();

  void beginCommunication();
  void endCommunication();

  void addToQueue(DataPacket&& packet);
  void addToQueue(const std::vector<uint32_t>& data);
  /**
   * Length of the queue in packets.
   */
  size_t queueSize() { return m_buildPackets ? m_packetQueue.size() : m_dataVector.size() / 4; }

  void transmitBeginData();
  uint32_t transmitQueue();
  //TODO void saveToDisk();
  //TODO void loadFromDisk();

private:

  std::vector<uint32_t> serializeQueue(uint32_t id);
  CommonClient m_client;
  Connection m_connection;
  uint32_t m_boardId;
  bool m_buildPackets;
  
  std::deque<DataPacket> m_packetQueue;
  std::vector<uint32_t> m_dataVector;

};
