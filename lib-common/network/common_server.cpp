#include <iostream>
#include <cstdlib>
#include <cerrno>
#include <cstring>

#include <unistd.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <poll.h>

#include <arpa/inet.h>

#include "utils/logger.hpp"

#include "common_server.hpp"

CommonServer::CommonServer(std::string serverName, std::string serverPort, size_t maxConnections)
        : m_name(serverName)
        , m_port(serverPort)
        , m_acceptMax(maxConnections)
        , m_socketFd(-1) {}

CommonServer::~CommonServer() {
  stop();
}

/**
 * Starts the server:
 * - creates the socket
 * - binds the socket
 * - listens for connections
 */
void CommonServer::start() {
  struct addrinfo hints, *servinfo, *p;
  // struct sigaction sa;
  int yes=1;
  int rv;

  memset(&hints, 0, sizeof hints);
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  hints.ai_flags = AI_PASSIVE; // use my IP

  if ((rv = getaddrinfo(NULL, m_port.c_str(), &hints, &servinfo)) != 0) {
    THROW_RE("getaddrinfo: {}", gai_strerror(rv));
  }

  // loop through all the results and bind to the first we can
  for(p = servinfo; p != NULL; p = p->ai_next) {
    if ((m_socketFd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) == -1) {
      DEBUG("server: socket. {}", std::strerror(errno));
      continue;
    }

    if (setsockopt(m_socketFd, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int)) == -1) {
      THROW_SE(errno, "setsockopt");
    }

    if (bind(m_socketFd, p->ai_addr, p->ai_addrlen) == -1) {
      close(m_socketFd);
      DEBUG("server: bind. {}", std::strerror(errno));
      continue;
    }

    break;
  }

  freeaddrinfo(servinfo); // all done with this structure

  if (p == NULL)  {
    THROW_RE("server failed to bind");
  }

  if (listen(m_socketFd, m_acceptMax) == -1) {
    THROW_RE("server: listen {}", std::strerror(errno));
  }

  // TODO needed?
  // sa.sa_handler = sigchld_handler; // reap all dead processes
  // sigemptyset(&sa.sa_mask);
  // sa.sa_flags = SA_RESTART;
  // if (sigaction(SIGCHLD, &sa, NULL) == -1) {
  //   perror("sigaction");
  //   exit(1);
  // }

  NOTICE("server started");
}

/**
 * Closes the socket and invalidates the socket file descriptor.
 */
void CommonServer::stop() {
  close(m_socketFd);
  m_socketFd = -1;
}

/**
 * Blocks until a connection is received, accepts and retuns it, unless an error occurs.
 */
Connection CommonServer::acceptConnection() {
  char s[INET6_ADDRSTRLEN];
  struct sockaddr_storage their_addr; // connector's address information
  socklen_t sin_size = sizeof(their_addr);

  struct pollfd pfd;
  pfd.fd = m_socketFd;
  pfd.events = POLLIN;

  int new_fd;

  while (true){
    int pollCount = poll(&pfd, 1, 1000);
    
    if (pollCount == -1) {
      if (errno == EINTR) {
        continue;
      }
      THROW_SE(errno, "poll");
    }

    if (pollCount == 1 && (pfd.revents & POLLIN)) {
      DEBUG("CommonServer.acceptConnection: accept");
      new_fd = accept(m_socketFd, (struct sockaddr *)&their_addr, &sin_size);
      DEBUG("CommonServer.acceptConnection: accepted");
      break;
    }
    else if (pollCount == 0) {
      THROWX(connection_timeout, "CommonServer.acceptConnection: poll timeout");
    }

  }
  
  if (new_fd == -1) {
    THROW_SE(errno, "accept");
  }

  inet_ntop(their_addr.ss_family, getInAddress((struct sockaddr *)&their_addr), s, sizeof(s));
  VERBOSE("CommonServer: got connection from {}", s);

  return Connection(new_fd, s);
}