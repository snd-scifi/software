#include "utils/logger.hpp"

#include "command_server.hpp"

CommandServer::~CommandServer() {
  m_server.stop();
}

Connection CommandServer::acceptConnection() {
  return m_server.acceptConnection();
}

void CommandServer::connectionLoop(Connection connection) {
  Header header;
  std::string data;
  std::string command;
  
  while(1) {
    //TODO error checking
    try {
      header = connection.recvHeader();
    }
    catch (connection_closed& cc) {
      NOTICE("CommandServer: Connection from {} closed.", connection.address());
      return;
    }

    switch (header.type) {
    case BEGIN_COMMUNICATION: //TODO need to distinguish between a data connection and a control connection
      {
        try {
          data = connection.recvString(header.length);
        }
        catch (connection_closed& cc) {
          NOTICE("CommandServer: Connection from {} closed.", connection.address());
          return;
        }

        //TODO initialize
      }
    case COMMAND:
      {
        try {
          command = connection.recvString(header.length);
        }
        catch (connection_closed& cc) {
          NOTICE("CommandServer: Connection from {} closed.", connection.address());
          return;
        }
        auto reply = processJsonCommand(command).dump();
        connection.sendHeader({HeaderType::COMMAND_REPLY, static_cast<uint32_t>(reply.size()), header.id});
        connection.sendString(reply);
        break;
      }
    case RAW_COMMAND:
      {
        try {
          command = connection.recvString(header.length);
        }
        catch (connection_closed& cc) {
          NOTICE("CommandServer: Connection from {} closed.", connection.address());
          return;
        }
        auto reply = processRawCommand(command);
        auto replyJson = reply.first.dump();
        auto replyData = reply.second;
        connection.sendHeader({HeaderType::COMMAND_REPLY, static_cast<uint32_t>(replyJson.size()), header.id});
        connection.sendString(replyJson);
        if (reply.first.at("response") == "ok" && reply.first.at("result") == "raw_data_follows") {
          connection.sendData(replyData, header.id);
        }
        break;
      }
    case END_COMMUNICATION:
      {
        try {
          data = connection.recvString(header.length);
        }
        catch (connection_closed& cc) {
          NOTICE("CommandServer: Connection from {} closed.", connection.address());
          return;
        }
        return;
      }
    default:
      WARN("Unknown header: {}", header.type);
      break;
    }
    
  }
}

json CommandServer::processJsonCommand(std::string command) {
  auto j = json::parse(command);
  std::string cmd = j.at("command");
  json args;

  try {
    args = j.at("args");
  }
  catch (nlohmann::detail::out_of_range& oor) {
    DEBUG("processJsonCommand: no arguments given");
    args["args"] = json::value_t::object;
  }

  try {
    return m_jsonCommands.at(cmd)(args);
  }
  catch (std::out_of_range& oor) {
    DEBUG("processJsonCommand: unknown command");
    json response;
    response["response"] = "err";
    response["result"] = "unknown command";
    return response;
  }
}


std::pair<json, std::vector<DataPacket>> CommandServer::processRawCommand(std::string command) {
  auto j = json::parse(command);
  std::string cmd = j.at("command");
  json args;
  try {
    args = j.at("args");
  }
  catch (nlohmann::detail::out_of_range& oor) {
    DEBUG("processRawCommand: no arguments given");
    args["args"] = json::value_t::object;
  }

  try {
    return m_rawCommands.at(cmd)(args);
  }
  catch (std::out_of_range& oor) {
    WARN("processRawCommand: unknown command: {}", cmd);
    json response;
    response["response"] = "err";
    response["result"] = fmt::format("unknown command: {}", cmd);
    return std::make_pair(response, std::vector<DataPacket>());
  }
}

void CommandServer::registerJsonCommand(std::string command, JsonCommandCallback_t callback) {
  m_jsonCommands[command] = callback;
}

void CommandServer::registerRawCommand(std::string command, RawCommandCallback_t callback) {
  m_rawCommands[command] = callback;
}
