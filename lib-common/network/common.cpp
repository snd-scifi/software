
#include <cstring>
//#include <cerrno>
#include <sys/socket.h>

#include "utils/logger.hpp"
#include "common.hpp"

//TODO implement timeout https://stackoverflow.com/questions/2876024/linux-is-there-a-read-or-recv-from-socket-with-timeout

void* getInAddress(struct sockaddr *sa) {
  if (sa->sa_family == AF_INET) {
    return &(((struct sockaddr_in*)sa)->sin_addr);
  }

  return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

