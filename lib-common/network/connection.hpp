#pragma once

#include <vector>
#include <string>
#include "storage/data_packet.hpp"
#include "network/common.hpp"

/**
 * Class representing a connection that allows to send and receive data in various formats (std::string and uint32_t).
 * It is returned by a CommonClient object when connects to a server and by a CommonServer object when it accepts a connection.
 */
class Connection {
public:
  Connection() : m_fd(-1), m_address("") {}
  Connection(int fileDescriptor, std::string address) : m_fd(fileDescriptor), m_address(address) {}
  ~Connection();

  /**
   * Returns the address of this connection.
   */
  std::string address() const { return m_address; }
  void close();

  void recvAll(void* buf, int len) const;
  void sendAll(void* buf, int len) const;

  std::vector<uint32_t> recv32(size_t length) const;
  void send32(std::vector<uint32_t> data) const;

  std::string recvString(size_t length) const;
  void sendString(std::string string) const;

  void beginCommunication(uint32_t boardId) const;
  void endCommunication(uint32_t boardId) const;

  void replyOk() const;
  void replyErr() const;

  Header recvHeader() const;
  void sendHeader(Header header) const;
  void sendHeader(HeaderType type, uint32_t length, uint32_t id) const;

  void sendData(std::vector<DataPacket> data, uint32_t id);
  std::vector<DataPacket> recvData();

  void sendRawData(const std::vector<uint32_t>& data, uint32_t id);
  

private:
  int m_fd;
  std::string m_address;
};