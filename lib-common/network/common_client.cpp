#include <iostream>
#include <cstdlib>
#include <cerrno>
#include <cstring>


#include <unistd.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>

#include <arpa/inet.h>

#include "utils/logger.hpp"

#include "common_client.hpp"

/**
 * The constructor only sets up the address, without actually connecting.
 * 
 * \param clientName Name of the client. Can be any string.
 * \param serverAddress Address of the server the client will connect to, e.g. "192.168.0.1"
 * \param serverPort Port of the server the client will connect to, e.g. "42069"
 */
CommonClient::CommonClient(std::string clientName, std::string serverAddress, std::string serverPort) 
  : m_name(clientName)
  , m_serverAddress(serverAddress)
  , m_serverPort(serverPort)
  , m_socketFd(-1)
  {}

CommonClient::~CommonClient() {
  disconnect();
}

/**
 * Connects to the server and returns a Connection object.
 * In case of failure, throws an exception.
 */
Connection CommonClient::connect() {
  struct addrinfo hints, *servinfo, *p;
  int rv;
  //char s[INET6_ADDRSTRLEN];

  //printf("clear hints\n");
  std::memset(&hints, 0, sizeof hints);
  hints.ai_family = AF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;

  DEBUG("Getting address info");
  if ((rv = getaddrinfo(m_serverAddress.c_str(), m_serverPort.c_str(), &hints, &servinfo)) != 0) {
    THROW_RE("getaddrinfo: {}", gai_strerror(rv));
  }

  // loop through all the results and connect to the first we can
  DEBUG("Looping over servinfo results");
  for(p = servinfo; p != NULL; p = p->ai_next) {
    if ((m_socketFd = socket(p->ai_family, p->ai_socktype,
        p->ai_protocol)) == -1) {
      DEBUG("client: socket - {}", std::strerror(errno));
      continue;
    }

    if (::connect(m_socketFd, p->ai_addr, p->ai_addrlen) == -1) {
      close(m_socketFd);
      DEBUG("client: connect - {}", std::strerror(errno));
      continue;
    }

    break;
  }

  if (p == NULL) {
    THROW_RE("failed to connect");
  }
  DEBUG("Free server info");
  freeaddrinfo(servinfo);

  return Connection(m_socketFd, m_serverAddress);
}

/**
 * Disconnects from the server.
 * At this point the connection object returned by connect() is no longer valid.
 */
void CommonClient::disconnect() {
  DEBUG("Closing socket");
  ::close(m_socketFd);
  m_socketFd = -1;
}