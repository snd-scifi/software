#pragma once

#include <cstdint>
#include <string>

#include "network/common.hpp"
#include "network/connection.hpp"

/**
 * Class containing the basic utilities to create a TCP server.
 */
class CommonServer {
public:
  CommonServer(std::string serverName, std::string serverPort, size_t maxConnections);
  ~CommonServer();

  void start();
  void stop();

  Connection acceptConnection();

  /**
   * Return the server name, given in the constructor.
   */
  std::string name() const { return m_name; }

  CommonServer(CommonServer const&) = delete;             // Copy construct
  CommonServer(CommonServer&&) = delete;                  // Move construct
  CommonServer& operator=(CommonServer const&) = delete;  // Copy assign
  CommonServer& operator=(CommonServer &&) = delete;      // Move assign

private:
  std::string m_name;
  std::string m_port;
  size_t m_acceptMax;
  int m_socketFd;
};