#include "data_client.hpp"
#include "utils/logger.hpp"
#include "utils/crc.hpp"

DataClient::DataClient(std::string serverAddress, std::string serverPort, uint32_t boardId, bool buildPackets)
  : m_client("DataClient", serverAddress, serverPort)
  , m_boardId(boardId)
  , m_buildPackets(buildPackets)
  {}

DataClient::~DataClient() {
  disconnect();
}

void DataClient::connect() {
  m_connection = m_client.connect();
}

void DataClient::beginCommunication() {
  m_connection.beginCommunication(m_boardId);
}

void DataClient::endCommunication() {
  m_connection.endCommunication(m_boardId);
}

void DataClient::disconnect() {
  m_client.disconnect(); // TODO only one of these should be used. need to better structure the network stuff.
  m_connection.close();
}

void DataClient::addToQueue(DataPacket&& packet) {
  m_packetQueue.emplace_back(packet);
}

void DataClient::addToQueue(const std::vector<uint32_t>& data) {
  if (!m_buildPackets) {
    m_dataVector.insert(m_dataVector.end(), data.begin(), data.end());
    DEBUG("Added {} elements to the queue.", data.size());
    DEBUG("The data queue now contains {} elements.", m_dataVector.size());
  }
  else {
    if (data.size() % 4 != 0) {
      THROW_RE("vector length must be a multiple of 4");
    }

    for (auto it = data.cbegin(); it != data.cend();) {
      m_packetQueue.emplace_back(DataPacket::createPacket(0, it, data.cend()));
    }

    // for (size_t i = 0; i < data.size()/4; i++) {
    //   // addToQueue(DataPacket::createPacket(data.data()+4*i));
    //   m_packetQueue.emplace_back(DataPacket::createPacket(data.data()+4*i));
    // }

    DEBUG("Added {} elements to the packet queue.", data.size()/4);
    DEBUG("The packet queue now contains {} elements.", m_packetQueue.size());
  }
  
}

std::vector<uint32_t> DataClient::serializeQueue(uint32_t id) { //TODO the header should be added by Connection, using sendData
  std::vector<uint32_t> serQueue;
  // each packet contains 4 uint32_t, then 3 for header (header-size-id)
  serQueue.reserve(m_packetQueue.size()*4+3);
  
  //add the header 
  serQueue.push_back(DATA);
  serQueue.push_back(m_packetQueue.size()*4);
  serQueue.push_back(id);

  //add the data
  for (auto& packet : m_packetQueue) {
    const auto& serPacket = packet.data();
    serQueue.insert(std::end(serQueue), std::begin(serPacket), std::end(serPacket));
  }

  return serQueue;
}

uint32_t DataClient::transmitQueue() {
  if (!m_buildPackets) {
    DEBUG("Transmitting raw queue ({} elements).", queueSize());
    uint32_t retVal = queueSize();
    m_connection.sendRawData(m_dataVector, 0);

    //TODO has to manage disconnection
    //TODO if disconnected, save locally

    auto response = m_connection.recvHeader();

    //TODO check response
    
    m_dataVector.clear();
    return retVal;
  }
  else {
    DEBUG("Transmitting queue ({} elements).", queueSize());
    uint32_t retVal = queueSize();
    auto dataToSend = serializeQueue(0);

    m_connection.send32(dataToSend);
    auto response = m_connection.recv32(3);

    //TODO has to manage disconnection
    //TODO if disconnected, save locally

    DEBUG("response: {}", response);

    //TODO check response
    m_packetQueue.clear();
    
    return retVal;
  }
  
}

