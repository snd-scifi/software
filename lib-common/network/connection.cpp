#include <cstring>
#include <unistd.h>
#include <sys/socket.h>

#include "network/common.hpp"
#include "utils/logger.hpp"

#include "connection.hpp"


Connection::~Connection() {
  // you cannot close the connection here otherwise when an instance gets copied, the socket is closed
  // ::close(m_fd);
}

void Connection::close() {
  ::close(m_fd);
  m_fd = -1;
  m_address = "";
  VERBOSE("Connection to {} closed.", m_address);
}

/**
 * A wrapper to recv that iterates until it receives exactly len bytes.
 */
void Connection::recvAll(void* buf, int len) const {
  int total = 0;        // how many bytes we've received
  int bytesleft = len; // how many we want to receive
  int n;

  while(total < len) {
    n = ::recv(m_fd, (char*) buf + total, bytesleft, 0);
    if (n < 0) {
      THROWX(network_error, "failed to receive: {}", std::strerror(errno));
    }
    else if (n == 0) {
      THROWX(connection_closed, "recv");
    }
    DEBUG("recvAll: received {} bytes", n);
    total += n;
    bytesleft -= n;
  }

}

/**
 * A wrapper to send that iterates until it sends exactly len bytes.
 */
void Connection::sendAll(void* buf, int len) const {
  int total = 0;        // how many bytes we've sent
  int bytesleft = len; // how many we have left to send
  int n;

  while(total < len) {
    n = ::send(m_fd, (char*) buf + total, bytesleft, MSG_NOSIGNAL);
    if (n < 0) {
      THROWX(network_error, "failed to send: {}", std::strerror(errno));
    }
    DEBUG("sendAll: sent {} bytes", n);
    total += n;
    bytesleft -= n;
  }
}

/**
 * Receives a vector of uint32_t with a fixed length and perfoms networ-to-host endianness conversion.
 * 
 * Note about endianness: network communications use big-endian order, while most computers (but not all) use little-endian.
 * To be sure things don't get messed up, we need to convert to big-endian before transmitting and reconverting to whatever endianness is used on the system after receiving.
 * 
 * \param length number of uint32_t to be received
 */
std::vector<uint32_t> Connection::recv32(size_t length) const {
  std::vector<uint32_t> data(length);
  recvAll(data.data(), data.size()*sizeof(uint32_t));
  // restore endianness
  for (auto& b : data) {
    b = be32toh(b);
  }
  return data;
}

/**
 * Sends a vector of uint32_t after performing host-to-network endianness conversion.
 * 
 * see recv32 for a note about endianness.
 */
void Connection::send32(std::vector<uint32_t> data) const {
  // set correct endianness for transmission (network works big-endian)
  for (auto& b : data) {
    b = htobe32(b);
  }
  sendAll(data.data(), data.size()*sizeof(uint32_t));
}

/**
 * Receives a string with a fixed length.
 */
std::string Connection::recvString(size_t length) const {
  std::vector<char> buffer(length);
  recvAll(buffer.data(), buffer.size());
  //ensure there is a null character at the end
  buffer.push_back(0);
  return std::string(buffer.data());
}

/**
 * Sends a string with a fixed length.
 */
void Connection::sendString(std::string string) const {
  sendAll(string.data(), string.length());
}

void Connection::beginCommunication(uint32_t boardId) const {
  sendHeader(BEGIN_COMMUNICATION, 0, boardId);

  auto response = recvHeader();
  if (response.type != REPLY_OK) {
    THROW_RE("Connection not accepted from server");
  }
}

void Connection::endCommunication(uint32_t boardId) const {
  sendHeader(END_COMMUNICATION, 0, boardId);
  
  auto response = recvHeader();
  if (response.type != REPLY_OK) {
    THROW_RE("Error during disconnection");
  }
}

void Connection::replyOk() const {
  sendHeader(REPLY_OK, 0, 0);
}

void Connection::replyErr() const {
  sendHeader(REPLY_ERR, 0, 0);
}

Header Connection::recvHeader() const {
  return Header(recv32(3));
}

void Connection::sendHeader(Header header) const {
  std::vector<uint32_t> hd{static_cast<uint32_t>(header.type), header.length, header.id};
  send32(hd);
}

void Connection::sendHeader(HeaderType type, uint32_t length, uint32_t id) const {
  sendHeader(Header{type, length, id});
}

void Connection::sendData(std::vector<DataPacket> data, uint32_t id) {
  std::vector<uint32_t> serializedData;
  // each packet contains 4 uint32_t
  serializedData.reserve(data.size()*4);

  //add the data
  for (auto& packet : data) {
    const auto& serPacket = packet.data();
    serializedData.insert(std::end(serializedData), std::begin(serPacket), std::end(serPacket));
  }

  sendHeader(DATA, serializedData.size()*4, id);
  send32(serializedData);

}

void Connection::sendRawData(const std::vector<uint32_t>& data, uint32_t id) {
  sendHeader(DATA, data.size(), id);
  send32(data);
}
