#pragma once

#include <stdexcept>
#include <vector>
#include <netinet/in.h>

/**
 * HeaderType used for communication between Data Server and Data Client
 */
enum HeaderType {
  NOP = 0x00000000,
  BEGIN_COMMUNICATION = 0x00001111, ///< Packet containing beginning of communication info, C-to-S.
  END_COMMUNICATION = 0x00002222, ///< Packet containing end of communication info, C-to-S.
  DATA = 0x00003333, ///< Packet containing data, C-to-S
  DATA_OK = 0x00004444, ///< Server response to data ok, S-to-C.
  DATA_ERR = 0x00005555, ///< Server response to data error (bad CRC), S-to-C.
  COMMAND = 0x00006666,
  COMMAND_REPLY = 0x00007777,
  RAW_COMMAND = 0x00008888,
  HEARTBEAT = 0x00009999,
  HEARTBEAT_REPLY = 0x0000AAAA,
  REPLY_OK = 0x0000BBBB,
  REPLY_ERR = 0x0000CCCC,
};

struct Header {
  HeaderType type;
  uint32_t length;
  uint32_t id;

  Header() : type(NOP), length(0), id(0) {}
  Header(std::vector<uint32_t> v) : type(static_cast<HeaderType>(v[0])), length(v[1]), id(v[2]) {}
  Header(HeaderType type, uint32_t length, uint32_t id) : type(type), length(length), id(id) {}
};

/**
 * Exception that indicates a generic network error.
 */
class network_error : public std::runtime_error {
public:
  network_error(const std::string& what_arg) : std::runtime_error(what_arg) {}
  network_error(const char* what_arg) : std::runtime_error(what_arg) {}
  network_error(const network_error& other) : std::runtime_error(other) {}
};

/**
 * Exception that indicates a closed connection.
 */
class connection_closed : public network_error {
public:
  connection_closed(const std::string& what_arg) : network_error(what_arg) {}
  connection_closed(const char* what_arg) : network_error(what_arg) {}
  connection_closed(const connection_closed& other) : network_error(other) {}
};

/**
 * Exception that indicates a timeout.
 */
class connection_timeout : public network_error {
public:
  connection_timeout(const std::string& what_arg) : network_error(what_arg) {}
  connection_timeout(const char* what_arg) : network_error(what_arg) {}
  connection_timeout(const connection_timeout& other) : network_error(other) {}
};

void* getInAddress(struct sockaddr *sa);
