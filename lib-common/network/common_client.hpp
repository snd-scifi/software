#pragma once


#include "network/common.hpp"
#include "network/connection.hpp"

class CommonClient {
public:
  CommonClient(std::string clientName, std::string serverAddress, std::string serverPort);
  ~CommonClient();

  /**
   * Returns the client name given at construction.
   */
  std::string name() const { return m_name; }

  Connection connect(); //TODO keep connection object and return reference to it?
  void disconnect();

private:
  std::string m_name;
  std::string m_serverAddress;
  std::string m_serverPort;
  int m_socketFd;
};