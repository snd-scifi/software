#pragma once

#include <string>

#include "json.hpp"

#include "network/common_server.hpp"

using json = nlohmann::json;
using JsonObject = json::object_t;
using JsonCommandCallback_t = std::function<JsonObject(JsonObject)>;
using RawCommandCallback_t = std::function<std::pair<json, std::vector<DataPacket>>(JsonObject)>;
using JsonCommandMap = std::unordered_map<std::string, JsonCommandCallback_t>;
using RawCommandMap = std::unordered_map<std::string, RawCommandCallback_t>;

class CommandServer {
public:
  CommandServer(std::string serverPort, size_t acceptMax) : m_server("CommandServer", serverPort, acceptMax) {};
  ~CommandServer();

  void start() { m_server.start(); }

  Connection acceptConnection();
  void connectionLoop(Connection connection);
  void registerJsonCommand(std::string command, JsonCommandCallback_t callback);
  void registerRawCommand(std::string command, RawCommandCallback_t callback);
  json processJsonCommand(std::string command);
  std::pair<json, std::vector<DataPacket>> processRawCommand(std::string command);

private:
  CommonServer m_server;
  JsonCommandMap m_jsonCommands;
  RawCommandMap m_rawCommands;

};