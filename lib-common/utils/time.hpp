#pragma once

#include <ctime>
#include <string>

std::time_t getCurrentTime();

std::string getTimeStringUTC(std::time_t time);

std::string getTimeStringLocaltime(std::time_t time);

std::string getCurrentTimeUTC();

std::string getCurrentLocaltime();

std::time_t getTimestampUTC(std::string time, std::string format = "%FT%TZ");

// std::time_t getTimestampLocaltime(std::string time, std::string format = "%FT%T%z");
