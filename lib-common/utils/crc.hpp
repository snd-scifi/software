#pragma once

#include <vector>
#include <cstdint>
#include <bitset>

#include "utils/logger.hpp"

class crc_error : public std::runtime_error {
  public:
  crc_error(const std::string& what_arg) : std::runtime_error(what_arg) {}
  crc_error(const char* what_arg) : std::runtime_error(what_arg) {}
  crc_error(const crc_error& other) : std::runtime_error(other) {}
};


/**
 * Calculate the 8-bit CRC for the TOFPET configuration.
 * It uses 0x07 as generator and 0xEC as initial value.
 */
template<size_t N>
uint8_t crcTofpet(const std::bitset<N> bs) {
  constexpr size_t NNEW = N+8;
  constexpr uint8_t generator = 0x07;
  uint8_t crc = 0xEC;
  
  std::bitset<NNEW> dividend(bs.to_string() + "00000000");

  for(size_t i = 0; i < NNEW; i++) {
    if ((crc & 0x80) != 0) { // if MSB of the CRC is 1, shift in the next bit from the dividend and XOR with the generator
      crc <<= 1;
      crc |= dividend[NNEW-1-i];
      crc ^= generator;
    }
    else { // otherwise just shift in the next bit
      crc <<= 1;
      crc |= dividend[NNEW-1-i];
    }
  }

  return crc;
}