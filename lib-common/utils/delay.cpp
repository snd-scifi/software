#include "delay.hpp"

#include <chrono>
#include <thread>

void delayMilliseconds(int duration) {
  std::this_thread::sleep_for(std::chrono::milliseconds(duration));
}