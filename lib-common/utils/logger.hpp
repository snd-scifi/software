// Copyright (c) 2014-2019 The Proteus authors
// Copyright (c) 2020 The SciFi reconstruction authors
// SPDX-License-Identifier: MIT

#pragma once

#include <mutex>

#include <syslog.h>

#include "utils/macros.hpp"

#include "fmt/chrono.h"
#include "fmt/color.h"
#include "fmt/core.h"
#include "fmt/format.h"
#include "fmt/ostream.h"
#include "fmt/ranges.h"

#include "json.hpp"

// see here for more info: https://fmt.dev/latest/api.html#format-api
template <> struct fmt::formatter<nlohmann::json> {
  // Parses empty specifications of the form.
  constexpr auto parse(format_parse_context& ctx) -> decltype(ctx.begin()) {
    auto it = ctx.begin(), end = ctx.end();

    // Check if reached the end of the range:
    if (it != end && *it != '}')
      throw format_error("nlohomann::json objects must have an empty format specification.");

    // Return an iterator past the end of the parsed range:
    return it;
  }

  // Formats the json object using the parsed format specification (presentation) stored in this formatter.
  template <typename FormatContext>
  auto format(const nlohmann::json& j, FormatContext& ctx) -> decltype(ctx.out()) {
    // ctx.out() is an output iterator to write to.
    return format_to(ctx.out(), "{}", j.dump());
  }
};

namespace detail {

// helper function to suppress unused variables warnings in non-debug builds
template <typename... Ts>
inline void unreferenced(Ts&&...)
{
}

} // namespace detail

/** A logger object with adjustable log level.
 *
 * The logger provides a `.log(...)` function that can be called with a
 * variable number of arguments.
 */
class Logger {
public:
  enum class Level {
    //Emergency = 0, // Basically a kernel panic, do not use
    Alert = LOG_ALERT,
    Critical = LOG_CRIT, // Critical conditions	Hard device errors.
    Error = LOG_ERR,
    Warning = LOG_WARNING,
    Notice = LOG_NOTICE, // Normal but significant conditions
    Info = LOG_INFO,
    Debug = LOG_DEBUG,
  };
  enum class LevelPrint { None, Short, Full, FullCaps };
  enum class TimePrint { None, Time, DateTime };

  Logger(Level level = Level::Notice);

  /** Set the minimal logging level for which messages are shown. */
  void setMinimalLevel(Level level) { m_level = level; }
  void setLevelPrint(LevelPrint setting) { m_levelPrint = setting; }
  void setTimePrint(TimePrint setting) { m_timePrint = setting; }
  void setUseSyslog(bool useSyslog);
  void setPrintFunctionName(bool printFunctionName) { m_printFunctionName = printFunctionName; }
  void setColors(bool colors) { m_colors = colors; }

  /** Log information with a given log level. */
  template <typename... Ts>
  void log(Level lvl, std::string_view functionName, std::string fmt, const Ts&... things) {
    std::lock_guard<std::mutex> lock(m_mutex);
    if (isActive(lvl)) {
      std::string message;
      print_level(message, lvl);
      print_time(message);
      print_function(message, functionName);
      message += fmt + "\n";
      message = fmt::format(message, things...);
  
      if (m_useSyslog) {
        syslog(static_cast<int>(lvl), "%s", message.c_str());
      }
      else {
        fmt::print(m_streams[static_cast<size_t>(lvl)],
        m_colors ? m_styles[static_cast<size_t>(lvl)] : fmt::emphasis(),
        "{}", message);
        fflush(m_streams[static_cast<size_t>(lvl)]); // needed when redirecting the output, otherwise it seems there is no output
      }
      
    }
  }

private:
  /** Check whether messages at the give loggging level are active. */
  bool isActive(Level level) const { return (level <= m_level); }
  void print_level(std::string& message, Level level) const {
    switch (m_levelPrint) {
    case LevelPrint::None:
      break;
    case LevelPrint::Short:
      message = fmt::format(FMT_STRING("{}{} "), message, m_prefixes[static_cast<int>(level)][0]);
      break;
    case LevelPrint::Full:
      message = fmt::format(FMT_STRING("{}{:<8} "), message, m_prefixes[static_cast<int>(level)]);
      break;
    default:
      break;
    }
  }

  void print_time(std::string& message) const {
    auto now = fmt::localtime(std::time(nullptr));
    switch (m_timePrint) {
    case TimePrint::None:
      break;
    case TimePrint::Time:
      message = fmt::format("{}|{%H:%M:%S}| ", message, now);
      break;
    case TimePrint::DateTime:
      message = fmt::format("{}|{:%Y-%m-%d %H:%M:%S}| ", message, now);
      break;
    default:
      break;
    }
  }

  void print_function(std::string& message, const std::string_view& functionName) const
  {
    if (m_printFunctionName) {
      message = fmt::format(FMT_STRING("{}{}: "), message, functionName);
    }
  }


  Level m_level;
  LevelPrint m_levelPrint;
  TimePrint m_timePrint;
  bool m_useSyslog;
  bool m_printFunctionName;
  bool m_colors;
  std::FILE* const m_streams[8];
  const char* const m_prefixes[8];
  const fmt::text_style m_styles[8];
  const char* const m_reset;
  std::mutex m_mutex;
};

/** Return the global logger. */
Logger& globalLogger();

std::istream& operator>> (std::istream& is, Logger::Level& level);
std::ostream& operator<< (std::ostream& os, const Logger::Level& level);

/* Convenience macros to log a message via the logger.
 *
 * The macros should be used to log a single message. The message **must not**
 * end in a newline. These macros expect a `globalLogger()` function to be
 * available that returns a reference to a `Logger` object.
 */
#define PRINT(...)                                                                             \
  do {                                                                                         \
    fmt::print(__VA_ARGS__); \
  } while (false)

#define ALERT(...)                                                                             \
  do {                                                                                         \
    constexpr auto cpf = __COMPACT_PRETTY_FUNCTION__;\
    ::globalLogger().log(::Logger::Level::Alert, cpf, __VA_ARGS__);    \
  } while (false)

#define CRITICAL(...)                                                                          \
  do {                                                                                         \
    constexpr auto cpf = __COMPACT_PRETTY_FUNCTION__;\
    ::globalLogger().log(::Logger::Level::Critical, cpf, __VA_ARGS__); \
  } while (false)

#define ERROR(...)                                                                             \
  do {                                                                                         \
    constexpr auto cpf = __COMPACT_PRETTY_FUNCTION__;\
    ::globalLogger().log(::Logger::Level::Error, cpf, __VA_ARGS__);    \
  } while (false)

#define WARN(...)                                                                              \
  do {                                                                                         \
    constexpr auto cpf = __COMPACT_PRETTY_FUNCTION__;\
    ::globalLogger().log(::Logger::Level::Warning, cpf, __VA_ARGS__);  \
  } while (false)

#define NOTICE(...)                                                                            \
  do {                                                                                         \
    constexpr auto cpf = __COMPACT_PRETTY_FUNCTION__;\
    ::globalLogger().log(::Logger::Level::Notice, cpf, __VA_ARGS__);   \
  } while (false)

#define INFO(...)                                                                              \
  do {                                                                                         \
    constexpr auto cpf = __COMPACT_PRETTY_FUNCTION__;\
    ::globalLogger().log(::Logger::Level::Info, cpf, __VA_ARGS__);     \
  } while (false)

#define VERBOSE(...)                                                                              \
  do {                                                                                         \
    constexpr auto cpf = __COMPACT_PRETTY_FUNCTION__;\
    ::globalLogger().log(::Logger::Level::Debug, cpf, __VA_ARGS__);     \
  } while (false)


/* Debug messages are logged with the verbose level but are only shown in
 * a debug build. They become noops in a release build. In a release build
 * the arguments should not be evaluated and no warning for unused variables
 * should be emitted.
 */
#ifdef NDEBUG
#define DEBUG(...)                                                                          \
  do {                                                                                      \
    (decltype(::detail::unreferenced(__VA_ARGS__)))0;                                       \
  } while (false)
#else
#define DEBUG(...)                                                                          \
  do {                                                                                      \
    constexpr auto cpf = __COMPACT_PRETTY_FUNCTION__;\
    ::globalLogger().log(::Logger::Level::Debug, cpf, __VA_ARGS__); \
  } while (false)
#endif

/** Throw an exception of the given type with a custom error message. */
#define THROWX(ExceptionType, fmtString, ...)                                            \
  do {                                                                                   \
    constexpr auto fn = __FILENAME__;\
    constexpr auto cpf = __COMPACT_PRETTY_FUNCTION__;\
    std::string message = fmt::format("{}: {}:{} {}", fn, cpf, __LINE__, fmtString); \
    throw ExceptionType(fmt::format(message, ##__VA_ARGS__));                            \
  } while (false)
// todo-c++20
// The ## is needed because, if no arguments are present, it removes the comma after message, which would otherwise give an error.
// See https://gcc.gnu.org/onlinedocs/cpp/Variadic-Macros.html
// in C++20 ", ##" can be replaced with " __VA_OPT__(,) "
// in addition in C++20 it's possible to force __FILENAME__ to be evaluated at compile time without the additional line

#define THROW_OOR(...) THROWX(std::out_of_range, __VA_ARGS__)
#define THROW_RE(...) THROWX(std::runtime_error, __VA_ARGS__)
#define THROW_IA(...) THROWX(std::invalid_argument, __VA_ARGS__)
#define THROW_SE(errNo, fmtString, ...)                                            \
  do {                                                                                   \
    constexpr auto fn = __FILENAME__;\
    constexpr auto cpf = __COMPACT_PRETTY_FUNCTION__;\
    std::string message = fmt::format("{}: {}:{} {}", fn, cpf, __LINE__, fmtString); \
    throw fmt::system_error(errNo, message,  ##__VA_ARGS__); \
  } while (false)
