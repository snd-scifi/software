#pragma once

// Example of __DATE__ string: "Jul 27 2012"
//                              01234567890

#define _BUILD_YEAR_CH0_ (__DATE__[ 7])
#define _BUILD_YEAR_CH1_ (__DATE__[ 8])
#define _BUILD_YEAR_CH2_ (__DATE__[ 9])
#define _BUILD_YEAR_CH3_ (__DATE__[10])


#define _BUILD_MONTH_IS_JAN_ (__DATE__[0] == 'J' && __DATE__[1] == 'a' && __DATE__[2] == 'n')
#define _BUILD_MONTH_IS_FEB_ (__DATE__[0] == 'F')
#define _BUILD_MONTH_IS_MAR_ (__DATE__[0] == 'M' && __DATE__[1] == 'a' && __DATE__[2] == 'r')
#define _BUILD_MONTH_IS_APR_ (__DATE__[0] == 'A' && __DATE__[1] == 'p')
#define _BUILD_MONTH_IS_MAY_ (__DATE__[0] == 'M' && __DATE__[1] == 'a' && __DATE__[2] == 'y')
#define _BUILD_MONTH_IS_JUN_ (__DATE__[0] == 'J' && __DATE__[1] == 'u' && __DATE__[2] == 'n')
#define _BUILD_MONTH_IS_JUL_ (__DATE__[0] == 'J' && __DATE__[1] == 'u' && __DATE__[2] == 'l')
#define _BUILD_MONTH_IS_AUG_ (__DATE__[0] == 'A' && __DATE__[1] == 'u')
#define _BUILD_MONTH_IS_SEP_ (__DATE__[0] == 'S')
#define _BUILD_MONTH_IS_OCT_ (__DATE__[0] == 'O')
#define _BUILD_MONTH_IS_NOV_ (__DATE__[0] == 'N')
#define _BUILD_MONTH_IS_DEC_ (__DATE__[0] == 'D')


#define _BUILD_MONTH_CH0_ \
    ((_BUILD_MONTH_IS_OCT_ || _BUILD_MONTH_IS_NOV_ || _BUILD_MONTH_IS_DEC_) ? '1' : '0')

#define _BUILD_MONTH_CH1_ \
    ( \
        (_BUILD_MONTH_IS_JAN_) ? '1' : \
        (_BUILD_MONTH_IS_FEB_) ? '2' : \
        (_BUILD_MONTH_IS_MAR_) ? '3' : \
        (_BUILD_MONTH_IS_APR_) ? '4' : \
        (_BUILD_MONTH_IS_MAY_) ? '5' : \
        (_BUILD_MONTH_IS_JUN_) ? '6' : \
        (_BUILD_MONTH_IS_JUL_) ? '7' : \
        (_BUILD_MONTH_IS_AUG_) ? '8' : \
        (_BUILD_MONTH_IS_SEP_) ? '9' : \
        (_BUILD_MONTH_IS_OCT_) ? '0' : \
        (_BUILD_MONTH_IS_NOV_) ? '1' : \
        (_BUILD_MONTH_IS_DEC_) ? '2' : \
        /* error default */    '?' \
    )

#define _BUILD_DAY_CH0_ ((__DATE__[4] >= '0') ? (__DATE__[4]) : '0')
#define _BUILD_DAY_CH1_ (__DATE__[ 5])



// Example of __TIME__ string: "21:06:19"
//                              01234567

#define _BUILD_HOUR_CH0_ (__TIME__[0])
#define _BUILD_HOUR_CH1_ (__TIME__[1])

#define _BUILD_MIN_CH0_ (__TIME__[3])
#define _BUILD_MIN_CH1_ (__TIME__[4])

#define _BUILD_SEC_CH0_ (__TIME__[6])
#define _BUILD_SEC_CH1_ (__TIME__[7])

static constexpr char SOFTWARE_BUILD_DATE[] = {_BUILD_YEAR_CH0_, _BUILD_YEAR_CH1_, _BUILD_YEAR_CH2_, _BUILD_YEAR_CH3_, '-', _BUILD_MONTH_CH0_, _BUILD_MONTH_CH1_, '-', _BUILD_DAY_CH0_, _BUILD_DAY_CH1_, '\0'};
static constexpr char SOFTWARE_BUILD_TIME[] = {_BUILD_HOUR_CH0_, _BUILD_HOUR_CH1_, ':', _BUILD_MIN_CH0_, _BUILD_MIN_CH1_, ':', _BUILD_SEC_CH0_, _BUILD_SEC_CH1_, '\0'};
