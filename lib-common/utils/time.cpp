#include <time.h>

#include "time.hpp"
#include "utils/logger.hpp"

std::time_t getCurrentTime() {
  const auto time = std::time(nullptr);
  if (time == -1) {
    WARN("Could not get current time");
    return 0;
  }
  return time;
}

std::string getTimeStringUTC(std::time_t time) {
  char buf[30];
  std::strftime(buf, sizeof(buf), "%FT%TZ", std::gmtime(&time));
  return std::string(buf);
}

std::string getTimeStringLocaltime(std::time_t time) {
  char buf[30];
  std::strftime(buf, sizeof(buf), "%FT%T%z", std::localtime(&time));
  return std::string(buf);
}

std::string getCurrentTimeUTC() {
  return getTimeStringUTC(getCurrentTime());
}

std::string getCurrentLocaltime() {
  return getTimeStringLocaltime(getCurrentTime());
}

std::time_t getTimestampUTC(std::string time, std::string format) {
  std::tm tm = {};
  strptime(time.c_str(), format.c_str(), &tm);
  auto t = std::mktime(&tm);
  return t + (std::mktime(std::localtime(&t)) - std::mktime(std::gmtime(&t)));
}

//off by one hour
// std::time_t getTimestampLocaltime(std::string time, std::string format) {
//   std::tm tm = {};
//   strptime(time.c_str(), format.c_str(), &tm);
//   auto t = std::mktime(&tm);
//   return std::mktime(std::localtime(&t));
// }
