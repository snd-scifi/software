// taken from https://stackoverflow.com/questions/23230003/something-between-func-and-pretty-function

#include <string>
#include <algorithm>

#include "macros.hpp"

// std::string computeMethodName(const std::string& function, const std::string& prettyFunction) {
//   size_t locFunName = prettyFunction.find(function); //If the input is a constructor, it gets the beginning of the class name, not of the method. That's why later on we have to search for the first parenthesys
//   size_t begin = prettyFunction.rfind(" ",locFunName) + 1;
//   size_t end = prettyFunction.find("(",locFunName + function.length()); //Adding function.length() make this faster and also allows to handle operator parenthesys!
//   if (prettyFunction[end + 1] == ')')
//     return (prettyFunction.substr(begin,end - begin) + "()");
//   else
//     return (prettyFunction.substr(begin,end - begin) + "(...)");
// }

// std::string removeSubstring(std::string source, const std::string& substring) {
//   size_t pos = source.find(substring);
//   if (pos != std::string::npos) {
//     return source.erase(pos, substring.length());
//   }
//   else {
//     return source;
//   }
// }