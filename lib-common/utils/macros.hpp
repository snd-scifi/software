#pragma once
// taken from https://stackoverflow.com/questions/23230003/something-between-func-and-pretty-function
#include <string_view>

// std::string computeMethodName(const std::string& function, const std::string& prettyFunction);
constexpr std::string_view computeMethodName(std::string_view function, std::string_view prettyFunction) {
  // if (!__builtin_is_constant_evaluated()) {
  //   return "AAAAAAA";
  // }
  size_t locFunName = prettyFunction.find(function); //If the input is a constructor, it gets the beginning of the class name, not of the method. That's why later on we have to search for the first parenthesys
  size_t begin = prettyFunction.rfind(" ",locFunName) + 1;
  size_t end = prettyFunction.find("(",locFunName + function.length()); //Adding function.length() make this faster and also allows to handle operator parenthesys!
  return prettyFunction.substr(begin,end - begin);
}
// std::string removeSubstring(std::string source, const std::string& substring);
constexpr std::string_view removePrefix(std::string_view source, std::string_view prefix) {
  size_t pos = source.find(prefix);
  // if (!__builtin_is_constant_evaluated()) {
  //   return "AAAAAAA";
  // }
  if (pos != std::string_view::npos) {
    source.remove_prefix(pos + prefix.length());
    return source;
  }
  else {
    return source;
  }
}

#define __COMPACT_PRETTY_FUNCTION__ computeMethodName(__FUNCTION__, __PRETTY_FUNCTION__)

#ifdef SOURCE_DIR
#define __FILENAME__ removePrefix(__FILE__, SOURCE_DIR"/")
#else
#define __FILENAME__ __FILE__
#endif