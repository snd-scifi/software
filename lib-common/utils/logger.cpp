// Copyright (c) 2014-2019 The Proteus authors
// Copyright (c) 2020 The SciFi reconstruction authors
// SPDX-License-Identifier: MIT

#include <cctype>
#include <algorithm>
#include <iostream>
#include <unordered_map>

#include "logger.hpp"

#define ANSI_RESET "\x1B[0m"
#define ANSI_BOLD "\x1B[1m"
#define ANSI_ITALIC "\x1B[3m"
#define ANSI_RED "\x1B[31m"
#define ANSI_YELLOW "\x1B[33m"

Logger::Logger(Level level)
    : m_level(level)
    , m_levelPrint{LevelPrint::Full}
    , m_timePrint{TimePrint::DateTime}
    , m_useSyslog{false}
    , m_printFunctionName{false}
    , m_colors{true}
    , m_streams{stderr, stderr, stderr, stderr, stderr, stdout, stdout, stdout}
    , m_prefixes{"",
                "Alert",
                "Critical",
                "Error",
                "Warning",
                "Notice",
                "Info",
                "Debug"}
    , m_styles{
      fmt::emphasis(),
      fmt::emphasis::bold | fmt::fg(fmt::color::red),
      fmt::emphasis::bold | fmt::fg(fmt::color::red),
      fmt::emphasis::bold | fmt::fg(fmt::color::red),
      fmt::emphasis::bold | fmt::fg(fmt::color::yellow),
      fmt::emphasis::bold | fmt::fg(fmt::color::light_green),
      fmt::emphasis::bold | fmt::fg(fmt::color::light_blue),
      fmt::emphasis::italic,
    }
    , m_reset{ANSI_RESET}
{}


void Logger::setUseSyslog(bool useSyslog) {
  m_useSyslog = useSyslog;
  if (m_useSyslog) {
    openlog(NULL, LOG_PID, LOG_USER);
  }
  else {
    closelog();
  }
}

Logger& globalLogger()
{
  static Logger log;
  return log;
}

std::istream& operator>> (std::istream& is, Logger::Level& level) {
  std::string tmp;
  is >> tmp;

  //make tmp lowercase
  std::transform(tmp.begin(), tmp.end(), tmp.begin(),
    [](unsigned char c){ return std::tolower(c); });
  
  std::unordered_map<std::string, Logger::Level> map{
    {"alert", Logger::Level::Alert},
    {"critical", Logger::Level::Critical},
    {"error", Logger::Level::Error},
    {"warning", Logger::Level::Warning},
    {"notice", Logger::Level::Notice},
    {"info", Logger::Level::Info},
    {"debug", Logger::Level::Debug},
    {"a", Logger::Level::Alert},
    {"c", Logger::Level::Critical},
    {"e", Logger::Level::Error},
    {"w", Logger::Level::Warning},
    {"n", Logger::Level::Notice},
    {"i", Logger::Level::Info},
    {"d", Logger::Level::Debug},
  };

  auto it = map.find(tmp);
  if (it != map.end()) {
      level = it->second;
  }
  else {
      THROW_IA("unknown logger level: {}", tmp);
  }

  return is;
}

std::ostream& operator<< (std::ostream& os, const Logger::Level& level) {
  switch (level) {
  case Logger::Level::Alert:
    return os << "alert";
  case Logger::Level::Critical:
    return os << "critical";
  case Logger::Level::Error:
    return os << "error";
  case Logger::Level::Warning:
    return os << "warning";
  case Logger::Level::Notice:
    return os << "notice";
  case Logger::Level::Info:
    return os << "info";
  case Logger::Level::Debug:
    return os << "debug";
  default:
    return os;
  }
}

