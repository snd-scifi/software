#pragma once

#include <iostream>

namespace print_u8 {
    // inline std::ostream &operator<<(std::ostream &os, const char c) {
    //     return os << (std::is_signed<char>::value
    //             ? static_cast<int>(c)
    //             : static_cast<unsigned int>(c));
    // }
 
    // inline std::ostream &operator<<(std::ostream &os, const signed char c) {
    //     return os << static_cast<int>(c);
    // }
 
    inline std::ostream &operator<<(std::ostream &os, const unsigned char c) {
        return os << +c;
    }
}