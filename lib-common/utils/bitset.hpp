#pragma once

#include <bitset>

/**
 * Returns the subset starting from first and of length M of the bitset bs.
 */
template <std::size_t M, std::size_t N>
std::bitset<M> bitSubSet(const std::bitset<N>& bs, const size_t first) {
  if (first >= bs.size() || first + M > bs.size()) {
    THROW_IA("bitsubSet: subset must be contained in the original bitset");
  }

  std::bitset<M> ret;
  for (size_t i = 0; i < M; i++) {
    ret[i] = bs[i+first];
  }
  return ret;
}

/**
 * Sets the subset starting from  first and of length M of the bitset bs with the content of subset.
 */
template <std::size_t M, std::size_t N>
void setBitSubSet(std::bitset<N>& bs, const size_t first, const std::bitset<M> subset) {
  if (first >= bs.size() || first + M > bs.size()) {
    THROW_IA("setBitSubSet: subset must be contained in the original bitset");
  }

  for (size_t i = 0; i < M; i++) {
    bs[i+first] = subset[i];
  }
}

/**
 * Reverses the order of the bits in a bitset, e.g. 001011 becomes 110100.
 */
template <std::size_t N>
void reverseBitset(std::bitset<N>& bs) {
  for (std::size_t i = 0; i < N/2; i++) {
    bool t = bs[i];
    bs[i] = bs[N-i-1];
    bs[N-i-1] = t;
  }
}
