#pragma once

#include <cstdint>

double tdcCalibration(uint16_t tf, double a, double b, double c, double d);


double qdcCalibration(double x, double a, double b, double c, double d, double e);


uint16_t integrationTimeClockCycles(uint16_t setting);


uint16_t integrationTimeSetting(uint16_t clockCycles);


