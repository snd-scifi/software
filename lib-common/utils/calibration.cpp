#include <stdexcept>
#include <cmath>

#include "utils/logger.hpp"

double tdcCalibration(uint16_t tf, double a, double b, double c, double d) {
  return (-b - std::sqrt(b*b - 4*a*(c-tf)))/(2*a);
}


double qdcCalibration(double x, double a, double b, double c, double d, double e) {
  return - c * std::log(1 + std::exp(a*(x-e)*(x-e) - b*(x-e))) + d;
}


uint16_t integrationTimeClockCycles(uint16_t setting) {
  if (setting < 16) {
    return setting;
  }
  else if (16 <= setting && setting < 32) {
    return 2 * setting - 16;
  }
  else if (32 <= setting && setting < 128) {
    return 4 * setting - 78;
  }
  else {
    THROW_IA("setting must be between 0 and 127.");
  }
}


uint16_t integrationTimeSetting(uint16_t clockCycles) {
  if (clockCycles < 16) {
    return clockCycles;
  }
  else if (16 <= clockCycles && clockCycles < 46) {
    return (clockCycles + 16) / 2;
  }
  else if (46 <= clockCycles && clockCycles < 430) {
    return (clockCycles + 78) / 4;
  }
  else {
    THROW_IA("clockCycles must be between 0 and 429.");
  }
}


