
#pragma once

#include "storage/track.hpp"
#include "utils/definitions.hpp"
#include "processing/scifi_tracker.hpp"

namespace reco {

    void correctClustersTime(RecoEvent& event, const Track& track, const Device& device);

    void addLocalStates(RecoEvent& event, const Device& m_device, Track& track, const Index& trId);

    void matchClusters(const std::vector<Index>& matchingSensorIds, RecoEvent& event, const Index& trId);

} // namespace reco
