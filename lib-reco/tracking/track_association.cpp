#include "processing/cluster_track_update.hpp"
#include "track_association.hpp"
#include "processing/scifi_tracker.hpp"
#include "propagation.hpp"
#include "mechanics/geometry.hpp"
#include "utils/logger.hpp"

namespace reco {

  /**
   * Corrects the timestamp and the v position of the clusters from a track having a valid trackstate
  */
  void correctClustersTime(RecoEvent& event, const Track& track, const Device& device) {
    for (Index i = 0; i < event.numSensorEvents(); i++) {
      if (!track.hasClusterOn(i)) {
      continue;
      }
       
      // get a reference to the cluster that is part of the track
      auto& cluster = event.getSensorEvent(i).getCluster(track.getClusterOn(i));
      const auto& plane = device.geometry().getPlane(i);

      // extrapolate the track on the plane of this cluster
      auto trackState = propagateTo(track.globalState(), Plane(), plane);

      // correct timestamp and add v position
      updateCluster(cluster, trackState);

    }
  }

  /**
   * Adds the the local state of a track indexed by trId in each plane
  */
  void addLocalStates(RecoEvent& event, const Device& m_device, Track& track, const Index& trId) {

    for (Index iSensor = 0; iSensor < event.numSensorEvents(); iSensor++) {
      auto& sensorEvent = event.getSensorEvent(iSensor);
      const auto& plane = m_device.geometry().getPlane(iSensor);
    
     sensorEvent.setLocalState(trId, propagateTo(track.globalState(), Plane(), plane));
    }
  }

  /**
   * matches the clostest cluster from a track of index trId of the matching sensors
  */
  void matchClusters(const std::vector<Index>& matchingSensorIds, RecoEvent& event, const Index& trId) {

    for (const auto sensorId : matchingSensorIds) {
      auto& sensorEvent = event.getSensorEvent(sensorId);
      const auto& trackState = sensorEvent.getLocalState(trId); 
      auto trackU = trackState.loc0();
      std::vector<double> trkCluDist;
      for (unsigned int i{0}; i < sensorEvent.numClusters(); i++) {
        trkCluDist.push_back(std::abs(trackU - sensorEvent.getCluster(i).position()[kU]));
      }

      if (trkCluDist.empty()) {
      continue;
      }

      auto idxMin = std::distance(std::begin(trkCluDist), std::min_element(std::begin(trkCluDist), std::end(trkCluDist)));
      updateCluster(sensorEvent.getCluster(idxMin), trackState);
      sensorEvent.addMatch(idxMin, trId);
    }

  }


} // namespace reco

