#pragma once

#include <string>
#include <filesystem>

#include "storage/reco_event.hpp"

namespace reco {

class RecoEventReader {
public:
  RecoEventReader(std::string path)
  : m_runDirectoryPath{path} {}
  virtual ~RecoEventReader() = default;
  virtual std::string name() const = 0;

  /** Return the (minimum) number of available events.
   *
   * \returns UINT64_MAX if the number of events is unknown.
   *
   * Calling `read` the given number of times must succeed. Additional
   * calls could still succeed.
   */
  virtual uint64_t numEvents() const = 0;
  
  /** Skip the next n events.
   *
   * If the call would seek beyond the range of available events it should
   * not throw and error. Instead, the next `readNext` call should fail.
   */
  virtual void skip(uint64_t n) = 0;
  
  /** Read the next event from the underlying device into the given object.
   * 
   * The Reader implementation is responsible for ensuring consistent events and
   * clearing previous contents. Errors must be handled by throwing an
   * appropriate exception.
   * 
   * \param[out] event Output event.
   * \returns true if an event was read, false if no event was read because no more events are available
   *
   */
  virtual bool read(RecoEvent& event) = 0;

protected:
  const std::filesystem::path m_runDirectoryPath;
  std::filesystem::path m_filePath;

};

} // namespace reco