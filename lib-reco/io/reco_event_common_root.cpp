#ifdef USE_ROOT

#include "reco_event_common_root.hpp"

namespace reco {

RecoEventCommonRoot::RecoEventCommonRoot(const Device& device) 
  : m_device(device)
  , m_fileEventCounter{0}
  , m_fileCounter{0}
  , m_f(uninitializedRootFilePtr())
  {}

}

#endif // USE_ROOT
