#pragma once

#ifdef USE_ROOT

#include <string>

#include "io/reco_event_reader.hpp"
#include "io/event_reader_root.hpp"

namespace reco {

class RecoTofpetEventReaderRoot : public RecoEventReader {
public:
  RecoTofpetEventReaderRoot(std::string path, const Device& device, std::string dataTreeName = "event_data");
  virtual ~RecoTofpetEventReaderRoot() = default;
  std::string name() const override { return "RecoTofpetEventReaderRoot";};

  uint64_t numEvents() const;
  
  void skip(uint64_t n);

  bool read(RecoEvent& event);

private:
  const Device& m_device;
  EventReaderRoot m_reader;

};

} // namespace reco

#endif // USE_ROOT
