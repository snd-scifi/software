#ifdef USE_ROOT

#include <algorithm>
#include <filesystem>

#include "utils/logger.hpp"

#include "reco_event_reader_root.hpp"

namespace reco {

namespace fs = std::filesystem;

RecoEventReaderRoot::RecoEventReaderRoot(std::string path, const Device& device) 
  : RecoEventReader(path)
  , RecoEventCommonRoot(device)
  , m_numberOfDataFiles{0}
  , m_totalEvents{0}
  , m_eventCounter{0}
  , m_hitLocalCoordinatesAvailable{false} {
  for (const auto& entry : fs::directory_iterator(m_runDirectoryPath)) {
    auto filename = entry.path().filename().string();
    if (filename.substr(0, 5) == "data_" && filename.substr(9, 5) == ".root") {
      m_numberOfDataFiles = std::max(m_numberOfDataFiles, static_cast<unsigned int>(std::stoi(filename.substr(5, 4)) + 1));
    }
  }
  DEBUG("Found {} data_XXXX.root files.", m_numberOfDataFiles);
  determineNumberOfEvents();
  openNextFile();
  WARN("RecoEventReaderRoot needs to be properly debugged. Use at your own risk.");
}


uint64_t RecoEventReaderRoot::numEvents() const {
  return m_totalEvents;
}


void RecoEventReaderRoot::skip(uint64_t n) {
  while (m_eventsInFile <= m_eventIdx + n ) {
    n -= m_eventsInFile - m_eventIdx;
    m_eventCounter += m_eventsInFile - m_eventIdx;
    if (!openNextFile()) {
      WARN("Skipping more events than actually available.");
      m_eventIdx = m_eventsInFile;
      break;
    }
  }
  m_eventIdx += n;
}

bool RecoEventReaderRoot::read(RecoEvent& event) {
  if (m_eventsInFile <= m_eventIdx) {
    if (!openNextFile()) {
      return false;
    }
  }

  if(m_eventTree->GetEntry(m_eventIdx) <= 0) {
    THROW_RE("Could not read entry {} from the event tree", m_eventIdx);
  }

  if(m_hitTree->GetEntry(m_eventIdx) <= 0) {
    THROW_RE("Could not read entry {} from the hit tree", m_eventIdx);
  }

  // INFO("Event timestamp {}, n hits {}", evtTimestamp, nHits);

  event.clear(evtTimestamp, evtNumber, evtFlags);

  
  if (m_clusterTree) {
     // tracks and intercepts make sense only in presence of clusters
    if (m_trackTree) {
      if(m_trackTree->GetEntry(m_eventIdx) <= 0) {
        THROW_RE("Could not read entry {} from the track tree", m_eventIdx);
      }
      for (unsigned int i = 0; i < nTracks; i++) {
        TrackState globalTrack(Vector6(trkX[i], trkY[i], trkT[i], trkSlopeX[i], trkSlopeY[i], trkSlopeT[i]), SymMatrix6::Zero());
        globalTrack.setCovPacked(trkCov[i]);
        event.addTrack(Track(globalTrack, trkChi2[i], trkDof[i]));
      }
    }

    if (m_interceptsTree) {
      if(m_interceptsTree->GetEntry(m_eventIdx) <= 0) {
        THROW_RE("Could not read entry {} from the intecepts tree", m_eventIdx);
      }
      for (unsigned int i = 0; i < nIntercepts; i++) {
        auto& sensorEvent = event.getSensorEvent(intSensorId[i]);
        TrackState localTrack(Vector6(intU[i], intV[i], intS[i], intSlopeU[i], intSlopeV[i], intSlopeS[i]), SymMatrix6::Zero());
        localTrack.setCovPacked(intCov[i]);
        sensorEvent.setLocalState(intTrackIndex[i], localTrack);
      }
    }

    if(m_clusterTree->GetEntry(m_eventIdx) <= 0) {
      THROW_RE("Could not read entry {} from the cluster tree", m_eventIdx);
    }
    for (unsigned int i = 0; i < nClusters; i++) {
      auto& sensorEvent = event.getSensorEvent(cluSensorId[i]);
      auto& cluster = sensorEvent.addCluster(
        Vector4(cluU[i], cluV[i], cluW[i], cluS[i]),
        SymMatrix4::Zero(),
        cluValue[i],
        cluTrackIndex[i],
        cluMatchIndex[i]
      );
      auto clusterIndex = cluster.getClusterIndex();
      cluster.setCovPacked(cluCov[i]);

      // if the track index is valid, we can add the last cluster inserted to the corresponding track
      if (cluTrackIndex[i] != kInvalidIndex) {
        event.getTrack(cluTrackIndex[i]).addCluster(cluSensorId[i], clusterIndex);
        // sensorEvent.getLocalState(cluTrackIndex[i]).
      }
      if (cluMatchIndex[i] != kInvalidIndex) {
        sensorEvent.addMatch(clusterIndex, cluMatchIndex[i]);
      }
      
    }

  }

  if (m_hitLocalCoordinatesAvailable) {
    for (unsigned int i = 0; i < nHits; i++) {
      auto& sensorEvent = event.getSensorEvent(hitSensorId[i]);
      auto& hit = sensorEvent.addHit(
        hitChannel[i],
        hitTimestamp[i],
        hitValue[i],
        hitU[i],
        hitV[i],
        hitW[i],
        hitS[i],
        hitClusterIndex[i]
      );
      hit.setCovPacked(hitCov[i]);

      if (hitClusterIndex[i] != kInvalidIndex) {
        sensorEvent.getCluster(hitClusterIndex[i]).addHit(hit);
      }
    }
  }
  else { // TODO check if this works
    for (unsigned int i = 0; i < nHits; i++) {
      const auto& sensor = m_device.getSensor(hitSensorId[i]);
      auto& sensorEvent = event.getSensorEvent(hitSensorId[i]);
      auto& hit = sensorEvent.addHit(
        hitChannel[i],
        hitTimestamp[i],
        hitValue[i],
        sensor.transformChannelToLocal(hitChannel[i], hitTimestamp[i]),
        hitClusterIndex[i]
      );

      if (hitClusterIndex[i] != kInvalidIndex) {
        sensorEvent.getCluster(hitClusterIndex[i]).addHit(hit);
      }
    }
  }

  
  m_eventIdx++;
  m_eventCounter++;
  return true;
}



bool RecoEventReaderRoot::openNextFile() {
  if (m_numberOfDataFiles <= m_fileCounter) {
    return false;
  }
  
  m_filePath = m_runDirectoryPath / fmt::format("data_{:04d}.root", m_fileCounter);
  DEBUG("Opening {}", m_filePath);
  m_f = openRootRead(m_filePath);
  m_fileCounter++;

  m_f->GetObject(kEventsTreeName, m_eventTree);
  if (!m_eventTree) {
    THROW_RE("TTree '{}' is not present in file {}", kEventsTreeName, m_filePath);
  }

  m_f->GetObject(kHitsTreeName, m_hitTree);
  if (!m_hitTree) {
    THROW_RE("TTree '{}' is not present in file {}", kHitsTreeName, m_filePath);
  }

  m_f->GetObject(kClustersTreeName, m_clusterTree);
  if (!m_clusterTree) {
    INFO("TTree '{}' is not present in file {}", kClustersTreeName, m_filePath);
  }

  m_f->GetObject(kTracksTreeName, m_trackTree);
  if (!m_trackTree) {
    INFO("TTree '{}' is not present in file {}", kTracksTreeName, m_filePath);
  }

  m_f->GetObject(kInterceptsTreeName, m_interceptsTree);
  if (!m_interceptsTree) {
    INFO("TTree '{}' is not present in file {}", kInterceptsTreeName, m_filePath);
  }

  m_eventsInFile = m_eventTree->GetEntriesFast();
  m_eventIdx = 0;

  m_eventTree->SetBranchAddress("timestamp", &evtTimestamp);
  m_eventTree->SetBranchAddress("number", &evtNumber);
  m_eventTree->SetBranchAddress("flags", &evtFlags);

  m_hitTree->SetBranchAddress("n_hits", &nHits);
  m_hitTree->SetBranchAddress("sensor_id", hitSensorId);
  m_hitTree->SetBranchAddress("channel", hitChannel);
  m_hitTree->SetBranchAddress("timestamp", hitTimestamp);
  m_hitTree->SetBranchAddress("value", hitValue);
  if (!m_hitTree->GetBranch("u") || !m_hitTree->GetBranch("v") || !m_hitTree->GetBranch("w") || !m_hitTree->GetBranch("s") || !m_hitTree->GetBranch("cov")) {
    m_hitLocalCoordinatesAvailable = false;
  }
  else {
    m_hitLocalCoordinatesAvailable = true;
    m_hitTree->SetBranchAddress("u", hitU);
    m_hitTree->SetBranchAddress("v", hitV);
    m_hitTree->SetBranchAddress("w", hitW);
    m_hitTree->SetBranchAddress("s", hitS);
    m_hitTree->SetBranchAddress("cov", hitCov);
  }
  m_hitTree->SetBranchAddress("cluster_idx", hitClusterIndex);

  if (m_clusterTree) {
    m_clusterTree->SetBranchAddress("n_clusters", &nClusters);
    m_clusterTree->SetBranchAddress("sensor_id", cluSensorId);
    m_clusterTree->SetBranchAddress("size", cluSize);
    m_clusterTree->SetBranchAddress("u", cluU);
    m_clusterTree->SetBranchAddress("v", cluV);
    m_clusterTree->SetBranchAddress("w", cluW);
    m_clusterTree->SetBranchAddress("s", cluS);
    m_clusterTree->SetBranchAddress("cov", cluCov);
    m_clusterTree->SetBranchAddress("value", cluValue);
    m_clusterTree->SetBranchAddress("track_idx", cluTrackIndex);
    m_clusterTree->SetBranchAddress("match_idx", cluMatchIndex);
  }
  
  if (m_trackTree) {
    m_trackTree->SetBranchAddress("n_tracks", &nTracks);
    m_trackTree->SetBranchAddress("x", trkX);
    m_trackTree->SetBranchAddress("y", trkY);
    m_trackTree->SetBranchAddress("t", trkT);
    m_trackTree->SetBranchAddress("slope_x", trkSlopeX);
    m_trackTree->SetBranchAddress("slope_y", trkSlopeY);
    m_trackTree->SetBranchAddress("slope_t", trkSlopeT);
    m_trackTree->SetBranchAddress("cov", trkCov);
    m_trackTree->SetBranchAddress("chi2", trkChi2);
    m_trackTree->SetBranchAddress("dof", trkDof);
  }
  
  if (m_interceptsTree) {
    m_interceptsTree->SetBranchAddress("n_intercepts", &nIntercepts);
    m_interceptsTree->SetBranchAddress("sensor_id", intSensorId);
    m_interceptsTree->SetBranchAddress("u", intU);
    m_interceptsTree->SetBranchAddress("v", intV);
    m_interceptsTree->SetBranchAddress("s", intS);
    m_interceptsTree->SetBranchAddress("slope_u", intSlopeU);
    m_interceptsTree->SetBranchAddress("slope_v", intSlopeV);
    m_interceptsTree->SetBranchAddress("slope_s", intSlopeS);
    m_interceptsTree->SetBranchAddress("cov", intCov);
    m_interceptsTree->SetBranchAddress("track_idx", intTrackIndex);
  }
  

  return true;
}


void RecoEventReaderRoot::determineNumberOfEvents() {
  for (unsigned int i{0}; i < m_numberOfDataFiles; i++) {
    auto filePath = m_runDirectoryPath / fmt::format("data_{:04d}.root", i);
    auto f = openRootRead(filePath);
    TTree* tree;
    f->GetObject(kEventsTreeName, tree);
    if (!tree) {
      THROW_RE("TTree '{}' is not present in file {}", kEventsTreeName, filePath);
    }
    m_totalEvents += tree->GetEntriesFast();
  }
}


} // namespace reco

#endif // USE_ROOT
