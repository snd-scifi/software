#pragma once

#include <string>
#include <vector>

// #include "daq/daq_server_manager.hpp"
#include "storage/reco_event.hpp"

#include "json.hpp"

namespace reco {

class RecoEventWriter {
public:
  // RecoEventWriter(DaqServerManager& daqServerManager) : m_serverManager{daqServerManager} {}
  RecoEventWriter(std::string path, nlohmann::json writerSettings)
    : m_writerSettings(writerSettings)
    , m_runDirectoryPath{path} {}
  virtual ~RecoEventWriter() = default;
  virtual std::string name() const = 0;

  /** Add the event to the writer.
   *
   * The reference to the event is only valid for the duration of the call.
   * Errors must be handled by throwing an appropriate exception.
   */
  virtual void append(const RecoEvent& event) = 0;
  virtual void finalize() {};
  virtual std::string currentFilePath() const { return m_filePath; };

protected:
  // DaqServerManager& m_serverManager;
  const nlohmann::json m_writerSettings;

  const std::filesystem::path m_runDirectoryPath;
  std::filesystem::path m_filePath;

};

} // namespace reco