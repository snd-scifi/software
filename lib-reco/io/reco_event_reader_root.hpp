#pragma once

#ifdef USE_ROOT

#include <string>

#include "io/reco_event_reader.hpp"
#include "io/reco_event_common_root.hpp"

namespace reco {

class RecoEventReaderRoot : public RecoEventReader, public RecoEventCommonRoot {
public:
  RecoEventReaderRoot(std::string path, const Device& device);
  virtual ~RecoEventReaderRoot() = default;
  std::string name() const override { return "RecoEventReaderRoot";};

  uint64_t numEvents() const;
  
  void skip(uint64_t n);

  bool read(RecoEvent& event);

private:
  bool openNextFile();
  void determineNumberOfEvents();
  unsigned int m_numberOfDataFiles;
  size_t m_totalEvents;
  size_t m_eventCounter;
  size_t m_eventsInFile;
  size_t m_eventIdx;
  bool m_hitLocalCoordinatesAvailable;

};

} // namespace reco

#endif // USE_ROOT
