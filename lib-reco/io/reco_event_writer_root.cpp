#ifdef USE_ROOT

#include <algorithm>
#include <filesystem>

#include "utils/logger.hpp"

#include "reco_event_writer_root.hpp"

namespace reco {

namespace fs = std::filesystem;

RecoEventWriterRoot::RecoEventWriterRoot(std::string path, const Device& device, nlohmann::json writerSettings) 
  : RecoEventWriter(path, writerSettings)
  , RecoEventCommonRoot(device)
  , m_fileEventCounter {0}
  , m_autoSaveEventCounter {0}
  , m_fileEventLimit{m_writerSettings.value("events_per_file", 1000000UL)}
  , m_autoFlush{m_writerSettings.value("auto_flush", -30000000L)}
  , m_autoSave{m_writerSettings.value("auto_save", -300000000L)}
  , m_autoSaveNevents{m_writerSettings.value("auto_save_n_events", 1000UL)}
  , m_autoSaveDelayMin{m_writerSettings.value("auto_save_delay_min", 5.0)}
  , m_autoSaveDelayMax{m_writerSettings.value("auto_save_delay_max", 5.0)}
  , m_saveGlobalCoordinates{m_writerSettings.value("save_global_coordinates", false)}
{
  
  if (m_autoSaveDelayMax < m_autoSaveDelayMin) {
    WARN("Setting `auto_save_delay_min` ({}) must be <= `auto_save_delay_max` ({}). auto_save_delay_max takes priority.", m_autoSaveDelayMin, m_autoSaveDelayMax);
  }
  NOTICE("Open next file");
  openNextFile();
}

void RecoEventWriterRoot::append(const RecoEvent& event) {
  if (m_fileEventLimit <= m_fileEventCounter) {
    openNextFile();
  }

  evtTimestamp = event.timestamp();
  evtNumber = event.evtNumber();
  evtFlags = event.flags();

  m_eventTree->Fill();


  nTracks = event.numTracks();
  if (kMaxTracks < static_cast<Index>(nTracks)) {
    THROW_RE("tracks exceed MAX_TRACKS ({} > {})", nTracks,  kMaxTracks);
  }

  for (unsigned int i = 0; i < nTracks; i++) {
    const auto& track = event.getTrack(i);
    const auto& state = track.globalState();
    trkX[i] = state.loc0();
    trkY[i] = state.loc1();
    trkT[i] = state.time();
    trkSlopeX[i] = state.slopeLoc0();
    trkSlopeY[i] = state.slopeLoc1();
    trkSlopeT[i] = state.slopeTime();
    trkChi2[i] = track.chi2();
    trkDof[i] = track.degreesOfFreedom();
    state.getCovPacked(trkCov[i]);
  }
  m_trackTree->Fill();


  nCaloTracks = event.numCaloTracks();
  if (kMaxCaloTracks < static_cast<Index>(nCaloTracks)) {
    THROW_RE("caloTracks exceed MAX_CALO_TRACKS ({} > {})", nCaloTracks, kMaxCaloTracks);
  }

  for (unsigned int i = 0; i < nCaloTracks; i++) {
    const auto& caloTrack = event.getCaloTrack(i);
    const auto& state = caloTrack.globalState();
    caloTrkX[i] = state.loc0();
    caloTrkY[i] = state.loc1();
    caloTrkT[i] = state.time();
    caloTrkSlopeX[i] = state.slopeLoc0();
    caloTrkSlopeY[i] = state.slopeLoc1();
    caloTrkSlopeT[i] = state.slopeTime();
    caloTrkChi2[i] = caloTrack.chi2();
    caloTrkDof[i] = caloTrack.degreesOfFreedom();
    state.getCovPacked(caloTrkCov[i]);
  }
  m_caloTrackTree->Fill();


  unsigned int sensorIndex{0};
  nHits = 0;
  nClusters = 0;
  nIntercepts = 0;
  for (const auto& sensorEvent : event) {
    auto nSensorHits = sensorEvent.numHits();
    if (kMaxHits < static_cast<Index>(nSensorHits)) {
      THROW_RE("hits exceed MAX_HITS ({})", kMaxHits);
    }
    auto nSensorClusters = sensorEvent.numClusters();
    if (kMaxHits < static_cast<Index>(nSensorClusters)) {
      THROW_RE("clusters exceed MAX_HITS ({})", kMaxHits);
    }
    // nHits += nSensorHits;
    // nClusters += nSensorClusters;

    for (unsigned int j = 0; j < nSensorHits; j++, nHits++) {
      const auto& hit = sensorEvent.getHit(j);
      hitSensorId[nHits] = sensorIndex;
      hitChannel[nHits] = hit.channel();
      hitTimestamp[nHits] = hit.timestamp();
      const auto& localPosition = hit.position();
      hitU[nHits] = localPosition[0];
      hitV[nHits] = localPosition[1];
      hitW[nHits] = localPosition[2];
      hitS[nHits] = localPosition[3];
      hit.getCovPacked(hitCov[nHits]);
      if (m_saveGlobalCoordinates) {
        const auto& plane = m_device.geometry().getPlane(sensorIndex);
        const auto& globalPosition = plane.toGlobal(localPosition);
        hitX[nHits] = globalPosition[0];
        hitY[nHits] = globalPosition[1];
        hitZ[nHits] = globalPosition[2];
        hitT[nHits] = globalPosition[3];
        hit.getCovPackedTransformed(hitCovGlobal[nHits], plane.linearToGlobal());
      }
      hitValue[nHits] = hit.value();
      hitClusterIndex[nHits] = hit.clusterIndex();

      // m_boardId[i] = hit.boardId();
      // m_tofpetId[i] = hit.tofpetId();
      // m_tofpetChannel[i] = hit.tofpetChannel();
      // m_tac[i] = hit.tac();
      // m_tCoarse[i] = hit.tCoarse();
      // m_tFine[i] = hit.tFine();
      // m_timestamp[i] = hit.timestamp();
      // m_vCoarse[i] = hit.vCoarse();
      // m_vFine[i] = hit.vFine();
      // m_value[i] = hit.value();

      // m_timestampCalChi2[i] = hit.timestampCalChi2();
      // m_timestampCalDof[i] = hit.timestampCalDof();
      // m_valueCalChi2[i] = hit.valueCalChi2();
      // m_valueCalDof[i] = hit.valueCalDof();
      // m_valueSaturation[i] = hit.valueSaturation();
    }

    for (unsigned int j = 0; j < nSensorClusters; j++, nClusters++) {
      const auto& clu = sensorEvent.getCluster(j);
      cluSensorId[nClusters] = sensorIndex;
      cluSize[nClusters] = clu.size();
      const auto& localPosition = clu.position();
      cluU[nClusters] = localPosition[0];
      cluV[nClusters] = localPosition[1];
      cluW[nClusters] = localPosition[2];
      cluS[nClusters] = localPosition[3];
      clu.getCovPacked(cluCov[nClusters]);
      if (m_saveGlobalCoordinates) {
        const auto& plane = m_device.geometry().getPlane(sensorIndex);
        const auto& globalPosition = plane.toGlobal(localPosition);
        cluX[nClusters] = globalPosition[0];
        cluY[nClusters] = globalPosition[1];
        cluZ[nClusters] = globalPosition[2];
        cluT[nClusters] = globalPosition[3];
        clu.getCovPackedTransformed(cluCovGlobal[nClusters], plane.linearToGlobal());
      }
      cluValue[nClusters] = clu.value();
      cluTrackIndex[nClusters] = clu.track();
      cluMatchIndex[nClusters] = clu.matchedState();
    }

    for (const auto& locState : sensorEvent.localStates()) {
      if (kMaxTracks < static_cast<Index>(nIntercepts + 1)) {
        THROW_RE("intercepts exceed MAX_TRACKS ({})", kMaxTracks);
      }
      intSensorId[nIntercepts] = sensorIndex;
      intU[nIntercepts] = locState.loc0();
      intV[nIntercepts] = locState.loc1();
      intS[nIntercepts] = locState.time();
      intSlopeU[nIntercepts] = locState.slopeLoc0();
      intSlopeV[nIntercepts] = locState.slopeLoc1();
      intSlopeS[nIntercepts] = locState.slopeTime();
      locState.getCovPacked(intCov[nIntercepts]);
      intTrackIndex[nIntercepts] = locState.track();
      nIntercepts++;
    }
    sensorIndex++;

  }

  m_hitTree->Fill();
  m_clusterTree->Fill();
  m_interceptsTree->Fill();

  m_fileEventCounter++;
  m_autoSaveEventCounter++;

  const auto elapsed = m_autoSaveTimer.elapsed();
  // call the autosave if the minimum time has passed AND the maximum number or events is reached OR if the maximum time has passed
  // we also write the first event, to have the trees immediately accessible
  if ((m_autoSaveDelayMin < elapsed && m_autoSaveNevents < m_autoSaveEventCounter) || m_autoSaveDelayMax < elapsed || m_fileEventCounter == 1) {
    // m_dataTree->AutoSave("SaveSelf");
    m_autoSaveEventCounter = 0;
    m_autoSaveTimer.reset();
  }
}


void RecoEventWriterRoot::openNextFile() {
  m_filePath = m_runDirectoryPath / fmt::format("data_{:04d}.root", m_fileCounter);
  DEBUG("Opening {}", m_filePath);
  m_f = openRootWrite(m_filePath,
    m_writerSettings.value("compression_algorithm", ROOT::kZSTD),
    m_writerSettings.value("compression_level", 0)
  );
  m_fileCounter++;
  m_fileEventCounter = 0;
  m_autoSaveEventCounter = 0;

  m_eventTree = new TTree(kEventsTreeName, "Events");
  m_eventTree->SetDirectory(m_f.get());

  m_eventTree->Branch("timestamp", &evtTimestamp, "timestamp/L");
  m_eventTree->Branch("number", &evtNumber, "evt_number/l");
  m_eventTree->Branch("flags", &evtFlags, "evt_flags/l");


  m_trackTree = new TTree(kTracksTreeName, "Tracks");
  m_trackTree->SetDirectory(m_f.get());

  m_trackTree->Branch("n_tracks", &nTracks, "n_tracks/i");
  m_trackTree->Branch("x", trkX, "x[n_tracks]/D");
  m_trackTree->Branch("y", trkY, "y[n_tracks]/D");
  m_trackTree->Branch("t", trkT, "t[n_tracks]/D");
  m_trackTree->Branch("slope_x", trkSlopeX, "slope_x[n_tracks]/D");
  m_trackTree->Branch("slope_y", trkSlopeY, "slope_y[n_tracks]/D");
  m_trackTree->Branch("slope_t", trkSlopeT, "slope_t[n_tracks]/D");
  m_trackTree->Branch("cov", trkCov, "cov[n_tracks][21]/D");
  m_trackTree->Branch("chi2", trkChi2, "chi2[n_tracks]/D");
  m_trackTree->Branch("dof", trkDof, "dof[n_tracks]/I");


  m_caloTrackTree = new TTree(kCaloTracksTreeName, "CaloTracks");
  m_caloTrackTree->SetDirectory(m_f.get());

  m_caloTrackTree->Branch("n_calo_tracks", &nCaloTracks, "n_calo_tracks/i");
  m_caloTrackTree->Branch("x", caloTrkX, "x[n_calo_tracks]/D");
  m_caloTrackTree->Branch("y", caloTrkY, "y[n_calo_tracks]/D");
  m_caloTrackTree->Branch("t", caloTrkT, "t[n_calo_tracks]/D");
  m_caloTrackTree->Branch("slope_x", caloTrkSlopeX, "slope_x[n_calo_tracks]/D");
  m_caloTrackTree->Branch("slope_y", caloTrkSlopeY, "slope_y[n_calo_tracks]/D");
  m_caloTrackTree->Branch("slope_t", caloTrkSlopeT, "slope_t[n_calo_tracks]/D");
  m_caloTrackTree->Branch("cov", caloTrkCov, "cov[n_calo_tracks][21]/D");
  m_caloTrackTree->Branch("chi2", caloTrkChi2, "chi2[n_calo_tracks]/D");
  m_caloTrackTree->Branch("dof", caloTrkDof, "dof[n_calo_tracks]/I");


  m_hitTree = new TTree(kHitsTreeName, "Hits");
  m_hitTree->SetDirectory(m_f.get());

  m_hitTree->Branch("n_hits", &nHits, "n_hits/i");
  m_hitTree->Branch("sensor_id", hitSensorId, "sensor_id[n_hits]/i");
  m_hitTree->Branch("channel", hitChannel, "channel[n_hits]/I");
  m_hitTree->Branch("timestamp", hitTimestamp, "timestamp[n_hits]/D");
  m_hitTree->Branch("value", hitValue, "value[n_hits]/D");
  m_hitTree->Branch("u", hitU, "u[n_hits]/D");
  m_hitTree->Branch("v", hitV, "v[n_hits]/D");
  m_hitTree->Branch("w", hitW, "w[n_hits]/D");
  m_hitTree->Branch("s", hitS, "s[n_hits]/D");
  m_hitTree->Branch("cov", hitCov, "cov[n_hits][10]/D");
  if (m_saveGlobalCoordinates) {
    m_hitTree->Branch("x", hitX, "x[n_hits]/D");
    m_hitTree->Branch("y", hitY, "y[n_hits]/D");
    m_hitTree->Branch("z", hitZ, "z[n_hits]/D");
    m_hitTree->Branch("t", hitT, "t[n_hits]/D");
    m_hitTree->Branch("cov_global", hitCovGlobal, "cov_global[n_hits][10]/D");
  }
  m_hitTree->Branch("cluster_idx", hitClusterIndex, "cluster_idx[n_hits]/i");


  m_clusterTree = new TTree(kClustersTreeName, "Clusters");
  m_clusterTree->SetDirectory(m_f.get());

  m_clusterTree->Branch("n_clusters", &nClusters, "n_clusters/i");
  m_clusterTree->Branch("sensor_id", cluSensorId, "sensor_id[n_clusters]/i");
  m_clusterTree->Branch("size", cluSize, "size[n_clusters]/i");
  m_clusterTree->Branch("u", cluU, "u[n_clusters]/D");
  m_clusterTree->Branch("v", cluV, "v[n_clusters]/D");
  m_clusterTree->Branch("w", cluW, "w[n_clusters]/D");
  m_clusterTree->Branch("s", cluS, "s[n_clusters]/D");
  m_clusterTree->Branch("cov", cluCov, "cov[n_clusters][10]/D");
  if (m_saveGlobalCoordinates) {
    m_clusterTree->Branch("x", cluX, "x[n_clusters]/D");
    m_clusterTree->Branch("y", cluY, "y[n_clusters]/D");
    m_clusterTree->Branch("z", cluZ, "z[n_clusters]/D");
    m_clusterTree->Branch("t", cluT, "t[n_clusters]/D");
    m_clusterTree->Branch("cov_global", cluCovGlobal, "cov_global[n_clusters][10]/D");
  }
  m_clusterTree->Branch("value", cluValue, "value[n_clusters]/D");
  m_clusterTree->Branch("track_idx", cluTrackIndex, "track_idx[n_clusters]/i");
  m_clusterTree->Branch("match_idx", cluMatchIndex, "match_idx[n_clusters]/i");


  m_interceptsTree = new TTree(kInterceptsTreeName, "Intercepts");
  m_interceptsTree->SetDirectory(m_f.get());

  m_interceptsTree->Branch("n_intercepts", &nIntercepts, "n_intercepts/i");
  m_interceptsTree->Branch("sensor_id", intSensorId, "sensor_id[n_intercepts]/i");
  m_interceptsTree->Branch("u", intU, "u[n_intercepts]/D");
  m_interceptsTree->Branch("v", intV, "v[n_intercepts]/D");
  m_interceptsTree->Branch("s", intS, "s[n_intercepts]/D");
  m_interceptsTree->Branch("slope_u", intSlopeU, "slope_u[n_intercepts]/D");
  m_interceptsTree->Branch("slope_v", intSlopeV, "slope_v[n_intercepts]/D");
  m_interceptsTree->Branch("slope_s", intSlopeS, "slope_s[n_intercepts]/D");
  m_interceptsTree->Branch("cov", intCov, "cov[n_intercepts][21]/D");
  m_interceptsTree->Branch("track_idx", intTrackIndex, "track_idx[n_intercepts]/i");

  // m_dataTree->Branch("board_id", m_boardId, "boardId[nHits]/i");
  // m_dataTree->Branch("tofpet_id", m_tofpetId, "tofpetId[nHits]/b");
  // m_dataTree->Branch("tofpet_channel", m_tofpetChannel, "tofpetChannel[nHits]/b");
  // m_dataTree->Branch("tac", m_tac, "tac[nHits]/b");
  // m_dataTree->Branch("t_coarse", m_tCoarse, "tCoarse[nHits]/L");
  // m_dataTree->Branch("t_fine", m_tFine, "tFine[nHits]/s");
  // m_dataTree->Branch("timestamp", m_timestamp, "timestamp[nHits]/F");
  // m_dataTree->Branch("v_coarse", m_vCoarse, "vCoarse[nHits]/s");
  // m_dataTree->Branch("v_fine", m_vFine, "vFine[nHits]/s");
  // m_dataTree->Branch("value", m_value, "value[nHits]/F");

  // m_dataTree->Branch("timestamp_cal_chi2", m_timestampCalChi2, "timestampCalChi2[nHits]/F");
  // m_dataTree->Branch("timestamp_cal_dof", m_timestampCalDof, "timestampCalDof[nHits]/F");
  // m_dataTree->Branch("value_cal_chi2", m_valueCalChi2, "valueCalChi2[nHits]/F");
  // m_dataTree->Branch("value_cal_dof", m_valueCalDof, "valueCalDof[nHits]/F");
  // m_dataTree->Branch("value_saturation", m_valueSaturation, "valueSaturation[nHits]/F");


  m_autoSaveTimer.reset();

}

void RecoEventWriterRoot::finalize() {
  // this prevents ROOT from hanging when writing the last file. If this happens in the destructor, somehow it doesn't work...
  m_f = uninitializedRootFilePtr();
}

} // namespace reco

#endif // USE_ROOT
