#pragma once

#ifdef USE_ROOT

#include <string>

#include "io/reco_event_writer.hpp"
#include "io/reco_event_common_root.hpp"

#include "utils/timer.hpp"


namespace reco {

class RecoEventWriterRoot : public RecoEventWriter, public RecoEventCommonRoot {
public:
  RecoEventWriterRoot(std::string path, const Device& device, nlohmann::json writerSettings = nlohmann::json({}));
  virtual ~RecoEventWriterRoot() = default;
  std::string name() const override { return "RecoEventWriterRoot"; }

  /** Add the event to the underlying device.
   *
   * The reference to the event is only valid for the duration of the call.
   * Errors must be handled by throwing an appropriate exception.
   */
  void append(const RecoEvent& event) override;
  void finalize() override;

private:
  void openNextFile();

  size_t m_fileEventCounter;
  size_t m_autoSaveEventCounter;

  Timer m_autoSaveTimer;

  const unsigned long m_fileEventLimit;
  const long m_autoFlush;
  const long m_autoSave;
  const unsigned long m_autoSaveNevents;
  const double m_autoSaveDelayMin;
  const double m_autoSaveDelayMax;
  const bool m_saveGlobalCoordinates;
};

} // namespace reco

#endif // USE_ROOT
