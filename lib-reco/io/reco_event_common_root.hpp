#pragma once

#ifdef USE_ROOT

#include <cstdint>
#include <cstddef>
#include <filesystem>
#include <map>
#include <string>
#include <vector>

#include "mechanics/device.hpp"
#include "utils/root.hpp"

#include "TTree.h"

namespace reco {

class RecoEventCommonRoot {
protected:
  RecoEventCommonRoot(const Device& device);
  virtual ~RecoEventCommonRoot() = default;

  const Device& m_device;

  unsigned int m_fileEventCounter;
  unsigned int m_fileCounter;

  static constexpr size_t kMaxHits = 1 << 15;
  static constexpr size_t kMaxTracks = 1 << 10;
  static constexpr size_t kMaxCaloTracks = 1 << 3;

  static constexpr const char* kEventsTreeName = "events";
  static constexpr const char* kHitsTreeName = "hits";
  static constexpr const char* kClustersTreeName = "clusters";
  static constexpr const char* kTracksTreeName = "tracks";
  static constexpr const char* kCaloTracksTreeName = "calo_tracks";
  static constexpr const char* kInterceptsTreeName = "intercepts";

  // int64_t evtTimestamp;
  long long  evtTimestamp;
  unsigned long long  evtNumber;
  unsigned long long  evtFlags;


  UInt_t nTracks;
  double trkX[kMaxTracks];
  double trkY[kMaxTracks];
  double trkT[kMaxTracks];
  double trkSlopeX[kMaxTracks];
  double trkSlopeY[kMaxTracks];
  double trkSlopeT[kMaxTracks];
  double trkChi2[kMaxTracks];
  int trkDof[kMaxTracks];
  double trkCov[kMaxTracks][21];
  
  UInt_t nCaloTracks;
  double caloTrkX[kMaxCaloTracks];
  double caloTrkY[kMaxCaloTracks];
  double caloTrkT[kMaxCaloTracks];
  double caloTrkSlopeX[kMaxCaloTracks];
  double caloTrkSlopeY[kMaxCaloTracks];
  double caloTrkSlopeT[kMaxCaloTracks];
  double caloTrkChi2[kMaxCaloTracks];
  int caloTrkDof[kMaxCaloTracks];
  double caloTrkCov[kMaxCaloTracks][21];
  
  UInt_t nHits;
  UInt_t hitSensorId[kMaxHits];
  Int_t hitChannel[kMaxHits];
  double hitTimestamp[kMaxHits];
  double hitValue[kMaxHits];
  double hitU[kMaxHits];
  double hitV[kMaxHits];
  double hitW[kMaxHits];
  double hitS[kMaxHits];
  double hitCov[kMaxHits][10];
  double hitX[kMaxHits];
  double hitY[kMaxHits];
  double hitZ[kMaxHits];
  double hitT[kMaxHits];
  double hitCovGlobal[kMaxHits][10];
  Index hitClusterIndex[kMaxHits];

  UInt_t nClusters;
  UInt_t cluSensorId[kMaxHits];
  UInt_t cluSize[kMaxHits];
  double cluValue[kMaxHits];
  double cluU[kMaxHits];
  double cluV[kMaxHits];
  double cluW[kMaxHits];
  double cluS[kMaxHits];
  double cluCov[kMaxHits][10];
  double cluX[kMaxHits];
  double cluY[kMaxHits];
  double cluZ[kMaxHits];
  double cluT[kMaxHits];
  double cluCovGlobal[kMaxHits][10];
  Index cluTrackIndex[kMaxHits];
  Index cluMatchIndex[kMaxHits];

  UInt_t nIntercepts;
  UInt_t intSensorId[kMaxTracks];
  double intU[kMaxTracks];
  double intV[kMaxTracks];
  double intS[kMaxTracks];
  double intSlopeU[kMaxTracks];
  double intSlopeV[kMaxTracks];
  double intSlopeS[kMaxTracks];
  double intCov[kMaxHits][21];
  Index intTrackIndex[kMaxTracks];

  // uint32_t m_boardId[kMaxHits];
  // uint8_t m_tofpetId[kMaxHits];
  // uint8_t m_tofpetChannel[kMaxHits];
  // uint8_t m_tac[kMaxHits];
  // long long m_tCoarse[kMaxHits];
  // uint16_t m_tFine[kMaxHits];
  // float m_timestamp[kMaxHits];
  // uint16_t m_vCoarse[kMaxHits];
  // uint16_t m_vFine[kMaxHits];
  // float m_value[kMaxHits];
  // float m_timestampCalChi2[kMaxHits];
  // float m_timestampCalDof[kMaxHits];
  // float m_valueCalChi2[kMaxHits];
  // float m_valueCalDof[kMaxHits];
  // float m_valueSaturation[kMaxHits];

  TTree* m_eventTree;
  TTree* m_trackTree;
  TTree* m_caloTrackTree;
  TTree* m_hitTree;
  TTree* m_clusterTree;
  TTree* m_interceptsTree;
  RootFilePtr m_f;

};

}

#endif // USE_ROOT
