#ifdef USE_ROOT

#include <algorithm>
#include <filesystem>

#include "utils/logger.hpp"

#include "reco_tofpet_event_reader_root.hpp"

namespace reco {

namespace fs = std::filesystem;

RecoTofpetEventReaderRoot::RecoTofpetEventReaderRoot(std::string path, const Device& device, std::string dataTreeName) 
  : RecoEventReader(path)
  , m_device(device)
  , m_reader(path, dataTreeName)
{}


uint64_t RecoTofpetEventReaderRoot::numEvents() const {
  return m_reader.numEvents();
}


void RecoTofpetEventReaderRoot::skip(uint64_t n) {
  m_reader.skip(n);
}

bool RecoTofpetEventReaderRoot::read(RecoEvent& event) {
  TofpetEvent te;
  if(!m_reader.read(te)) {
    return false;
  }
  event = RecoEvent(te, m_device);
  return true;
}

} // namespace reco

#endif // USE_ROOT
