// Copyright (c) 2014-2019 The Proteus authors
// SPDX-License-Identifier: MIT
/**
 * \file
 * \author Moritz Kiehn (msmk@cern.ch)
 * \date 2016-09
 */

#include "config.hpp"

#include <algorithm>
#include <fstream>
#include <stdexcept>
#include <sstream>

#include "utils/logger.hpp"

namespace reco {

// TODO use filesystem library
bool pathIsAbsolute(const std::string& path)
{
  return !path.empty() && (path.front() == '/');
}

std::string pathDirname(const std::string& path)
{
  auto pos = path.find_last_of('/');
  // no slash means the path contains only a filename
  if (path.empty() || (pos == std::string::npos))
    return ".";
  // remove possible duplicates slashes at the end
  return path.substr(0, path.find_last_not_of('/', pos) + 1);
}

std::string pathExtension(const std::string& path)
{
  auto pos = path.find_last_of('.');
  return ((pos != std::string::npos) ? path.substr(pos + 1) : std::string());
}

std::string pathRebaseIfRelative(const std::string& path,
                                 const std::string& dir)
{
  if (pathIsAbsolute(path))
    return path;
  std::string full;
  if (!dir.empty()) {
    full = dir;
    full += '/';
  }
  full += path;
  return full;
}

toml::table configReadToml(const std::string& path) {
  toml::table tbl;
  try
  {
    tbl = toml::parse_file(path);
  }
  catch (const toml::parse_error& err)
  {
    THROW_RE("Could not parse {}: {}", path, err.what());
  }
  return tbl;
}

nlohmann::json configReadJson(const std::string& path) {
  std::ifstream f(path);
  if (!f) {
    THROW_RE("Could not read {}.", path);
  }
  return nlohmann::json::parse(f);
}

void configWriteToml(const toml::table& cfg, const std::string& path)
{
  std::ofstream file(path, std::ofstream::out);
  if (!file.good())
    THROW_RE("Could not open file '{}' to write.", path);

  file << cfg << "\n\n";
}

// toml::table configWithDefaults(const toml::table& cfg,
//                                const toml::table& defaults)
// {
//   toml::table combined(defaults);
//   if (!combined.merge(cfg)) {
//     throw std::runtime_error("Config: could not merge config w/ defaults");
//   }
//   return combined;
// }

// std::vector<toml::table> configPerSensor(const toml::table& cfg,
//                                          const toml::table& defaults)
// {
//   std::vector<toml::table> sensors =
//       cfg.get<std::vector<toml::table>>("sensors");
//   toml::table globals(cfg);
//   globals.eraseChild("sensors");

//   for (auto sensor = sensors.begin(); sensor != sensors.end(); ++sensor) {
//     toml::table combined(defaults);
//     if (!combined.merge(globals))
//       throw std::runtime_error("Config: could not merge globals");
//     if (!combined.merge(*sensor))
//       throw std::runtime_error("Config: could not merge sensor config");
//     std::swap(combined, *sensor);
//   }
//   return sensors;
// }

nlohmann::json tomlToJson(const toml::table& cfg) {
  std::stringstream ss;
  ss << toml::json_formatter(cfg);
  return nlohmann::json::parse(ss);
}

} // namespace reco
