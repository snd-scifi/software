// Copyright (c) 2014-2019 The Proteus authors
// SPDX-License-Identifier: MIT

#include "calo_track.hpp"

#include <ostream>
#include <stdexcept>

// #include <Math/ProbFunc.h>

namespace reco {

CaloTrack::CaloTrack(const TrackState& global, Scalar chi2, int dof)
    : m_state(global), m_chi2(chi2), m_dof(dof)
{
}

// Scalar Track::probability() const
// {
//   return ((0 < m_dof) and (0 <= m_chi2))
//              ? ROOT::Math::chisquared_cdf_c(m_chi2, m_dof)
//              : std::numeric_limits<Scalar>::quiet_NaN();
// }

namespace {
/** Helper functor struct to find a track cluster on a specific sensor. */
struct OnSensor {
  Index sensorId;

  bool operator()(const CaloTrack::TrackCluster& tc) const
  {
    return tc.sensor == sensorId;
  }
};
} // namespace

void CaloTrack::addCluster(Index sensor, Index cluster)
{
  m_clusters.push_back({sensor, cluster});
}

bool CaloTrack::hasClusterOn(Index sensor) const
{
  auto c = std::find_if(m_clusters.begin(), m_clusters.end(), OnSensor{sensor});
  return (c != m_clusters.end());
}

std::vector<Index> CaloTrack::getClustersOn(Index sensor) const
{
  std::vector<Index> ret;
  for (const auto& tc : m_clusters) {
    if (tc.sensor == sensor) {
      ret.push_back(tc.cluster);
    }
  }
  return ret;
}

std::ostream& operator<<(std::ostream& os, const CaloTrack& track)
{
  os << "chi2/dof=" << track.chi2() << "/" << track.degreesOfFreedom();
  // os << " prob=" << track.probability();
  os << " size=" << track.size();
  return os;
}

} // namespace reco
