#include "utils/logger.hpp"

#include "reco_hit.hpp"

namespace reco {

RecoHit::RecoHit(const int32_t channel, const double timestamp, const double value, const Vector4 position, const Index clusterIndex) 
  : TofpetHit(0xFFFFFFFF, 0xFF, 0xFF, 0xFF, 0xFFFFFFFFFFFFFFFF, 0xFFFF, 0xFFFF, 0xFFFF,
      timestamp,
      std::numeric_limits<float>::quiet_NaN(),
      std::numeric_limits<float>::quiet_NaN(),
      value,
      std::numeric_limits<float>::quiet_NaN(),
      std::numeric_limits<float>::quiet_NaN()
    )
  , m_channel{channel}
  , m_pos{position} // TODO covariance
  , m_clusterIndex{clusterIndex}
{}

RecoHit::RecoHit(const int32_t channel, const double timestamp, const double value, const double u, const double v, const double w, const double s, const Index clusterIndex) 
  : RecoHit(channel, timestamp, value, Vector4(u, v, w, s), clusterIndex)
{}

RecoHit::RecoHit(const TofpetHit& tofpetHit, const Sensor& sensor, const int index)
  : TofpetHit(tofpetHit) 
  , m_channel{sensor.getRecoChannel(m_tofpetId, m_tofpetChannel, index)} 
  , m_pos(sensor.transformChannelToLocal(m_channel, m_timestamp))
  , m_cov(sensor.getHitCovariance())
  , m_clusterIndex{kInvalidIndex}
  {}

} // namespace reco