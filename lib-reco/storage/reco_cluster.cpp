#include "utils/logger.hpp"

#include "reco_cluster.hpp"

namespace reco {

RecoCluster::RecoCluster(Vector4 position, SymMatrix4 covariance, double value, Index trackIndex, Index caloTrackIndex, Index matchIndex) 
  : m_pos(position)
  , m_posCov(covariance)
  , m_value{value}
  , m_index{kInvalidIndex}
  , m_track{trackIndex}
  , m_caloTrack{caloTrackIndex}
  , m_matchedState{matchIndex}
  {}

void RecoCluster::addHit(RecoHit& hit) {
  hit.setClusterIndex(m_index);
  m_hits.push_back(std::ref(hit));
}

void RecoCluster::setTrack(Index track) {
  m_track = track;
}

void RecoCluster::setCaloTrack(Index caloTrack) {
  m_caloTrack = caloTrack;
}

} // namespace reco