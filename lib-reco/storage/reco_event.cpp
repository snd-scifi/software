#include "utils/logger.hpp"

#include "reco_event.hpp"

namespace reco {

// SciFiEvent::SciFiEvent(const nlohmann::json& boardMapping) {
//   for (const auto& [planeName, planeSettings] : boardMapping.at("scifi").items()) {
//     // NOTICE("{}: {}", planeName, planeSettings);
//     if (planeSettings.at("type") != "snd_scifi" || planeSettings.at("class") != "multiboard") {
//       WARN("Found plane of type {} ({}). Ignoring it.", planeSettings.at("type"), planeSettings.at("class"));
//       continue;
//     }
//     m_sensorEvents.try_emplace(planeName, RecoSensorEvent{});

//     for (size_t i = 0; i < planeSettings.at("boards").get<std::vector<uint32_t>>().size(); i++) {
//       m_boardIdMap.try_emplace(planeSettings.at("boards").get<std::vector<uint32_t>>().at(i), planeName, i);
//     }
//   }
// }

RecoEvent::RecoEvent(const Device& device)
  : m_timestamp{std::numeric_limits<int64_t>::max()}
  , m_flags{0}
  , m_eventNumber{std::numeric_limits<uint64_t>::max()}
{
  for (unsigned int i = 0; i < device.numSensors(); i++) {
    m_sensorEvents.emplace_back(RecoSensorEvent{});
  }
}

RecoEvent::RecoEvent(const TofpetEvent& tofpetEvent, const Device& device) : RecoEvent(device) {
  m_timestamp = tofpetEvent.timestamp();
  m_flags = tofpetEvent.flags();
  m_eventNumber = tofpetEvent.evtNumber();

  for (const auto& hit : tofpetEvent) {
    // find the (sensorId, index) pair corresponding to the (boardId, slotId) pair and add the corresponding SensorEvent
    if (const auto it = device.boardSlotIdMap().find({hit.boardId(), static_cast<uint8_t>(hit.slotId())}); it != device.boardSlotIdMap().end()) {
      // *it is a key-value pair, so it->second is the (boardId, slotId) pair
      const auto& sensorId = it->second.first;
      const auto& index = it->second.second;
      m_sensorEvents.at(sensorId).addHit(hit, device.getSensor(sensorId), index);
    }
  }
}

void RecoEvent::clear(int64_t timestamp, uint64_t eventNumber, uint64_t flags) {
  m_timestamp = timestamp;
  m_eventNumber = eventNumber;
  m_flags = flags;

  for (auto& sensorEvent : m_sensorEvents) {
    sensorEvent.clear();
  }

  m_tracks.clear();
}

// void SciFiEvent::addTofpetEvent(const TofpetEvent& tofpetEvent) {
  
// }


void RecoEvent::addTrack(const Track& track)
{
  Index trackId = static_cast<Index>(m_tracks.size());
  m_tracks.push_back(track);
  // freeze cluster-to-track association
  for (const auto& c : m_tracks.back().m_clusters) {
    getSensorEvent(c.sensor).getCluster(c.cluster).setTrack(trackId);
  }
}

void RecoEvent::addCaloTrack(const CaloTrack& caloTrack)
{
  Index trackId = static_cast<Index>(m_caloTracks.size());
  m_caloTracks.push_back(caloTrack);
  // freeze cluster-to-track association
  for (const auto& c : m_caloTracks.back().m_clusters) {
    getSensorEvent(c.sensor).getCluster(c.cluster).setCaloTrack(trackId);
  }
}

RecoEvent RecoEvent::merge(const RecoEvent& event) {
  RecoEvent newEvent(*this);
  for (const auto& sensorEvent : event) {
    newEvent.m_sensorEvents.push_back(sensorEvent);
  }
  return newEvent;
}


} // namespace reco