// Copyright (c) 2014-2019 The Proteus authors
// SPDX-License-Identifier: MIT
/**
 * \file
 * \author Moritz Kiehn (msmk@cern.ch)
 * \date 2016-12
 */

#pragma once

#include <iosfwd>
#include <limits>

#include "utils/definitions.hpp"

namespace reco {

/** Track state on a plane.
 *
 * If the plane is the global xy-plane, the track description is identical
 * to the usual global description, i.e. global position and slopes along the
 * global z-axis.
 */
class TrackState {
public:
  /** Construct invalid state; only public for container support. */
  TrackState();
  /** Construct from scalar spatial parameters. */
  TrackState(Scalar location0, Scalar location1, Scalar slope0, Scalar slope1);
  /** Construct from position and slope
   *
   * \warning Assumes the position is on the plane and ignores normal component.
   */
  TrackState(const Vector4& position,
             const SymMatrix4& positionCov,
             const Vector2& slope,
             const SymMatrix2& slopeCov);
  /** Construct from position and slope
   *
   * \warning Assumes the position is on the plane and ignores normal component.
   */
  TrackState(const Vector4& position,
             const SymMatrix4& positionCov,
             const Vector3& slope,
             const SymMatrix3& slopeCov);
  /** Construct from full parameter vector. */
  template <typename Params, typename Covariance>
  TrackState(const Eigen::MatrixBase<Params>& params,
             const Eigen::MatrixBase<Covariance>& cov);

  /** Set the full covariance matrix. */
  template <typename Covariance>
  void setCov(const Eigen::MatrixBase<Covariance>& cov);

  /** Store the spatial covariance into packed storage.
   *
   * The iterator must point to a 10 element container that will be filled
   * with the lower triangular block of the symmetric covariance matrix for the
   * parameters [offset0, offset1, offsetT, slope0, slope1, slopeT] in compressed "diagonal-major"
   * layout, i.e.
   *
   *     | c[0]                               |
   *     | c[6]  c[1]                         |
   *     | c[11] c[7]  c[2]                   |
   *     | c[15] c[12] c[8]  c[3]             |
   *     | c[18] c[16] c[13] c[9]  c[4]       |
   *     | c[20] c[19] c[17] c[14] c[10] c[5] |
   *
   */
  template <typename OutputIterator>
  void getCovPacked(OutputIterator out) const;

  /** Set the spatial covariance matrix from packed storage.
   * Refer to TrackState::getCovPacked() for the format.
   */
  template <typename InputIterator>
  void setCovPacked(InputIterator in);

  /** Full parameter vector. */
  const Vector6& params() const { return m_params; }
  /** Covariance matrix of the full parameter vector. */
  const SymMatrix6& cov() const { return m_cov; }

  /** On-plane track first spatial dimension. */
  Scalar loc0() const { return m_params[kLoc0]; }
  /** On-plane track second spatial dimension. */
  Scalar loc1() const { return m_params[kLoc1]; }
  /** On-plane spatial track coordinates covariance. */
  auto loc01Cov() const { return m_cov.block<2, 2>(kLoc0, kLoc0); }
  /** Track time. */
  Scalar time() const { return m_params[kTime]; }
  /** Track time variance. */
  Scalar timeVar() const { return m_cov(kTime, kTime); }
  auto onPlane() const { return m_params.segment<3>(kOnPlane); }
  auto onPlaneCov() const { return m_cov.block<3, 3>(kOnPlane, kOnPlane); }
  /** Full track position. */
  Vector4 position() const;
  /** Full track position covariance. */
  SymMatrix4 positionCov() const;

  /** Track slope along the first spatial dimension. */
  Scalar slopeLoc0() const { return m_params[kSlopeLoc0]; }
  /** Track slope along the second spatial dimension. */
  Scalar slopeLoc1() const { return m_params[kSlopeLoc1]; }
  /** Track slope along the temporal dimension (inverse velocity). */
  Scalar slopeTime() const { return m_params[kSlopeTime]; }
  /** Full track tangent in slope parametrization. */
  Vector4 tangent() const;

  Index track() const { return m_track; }

  bool isMatched() const { return (m_matchedCluster != kInvalidIndex); }
  Index matchedCluster() const { return m_matchedCluster; }

private:
  Vector6 m_params;
  SymMatrix6 m_cov;
  Index m_track;
  Index m_matchedCluster;

  friend class RecoSensorEvent;
  friend class Track;

};

std::ostream& operator<<(std::ostream& os, const TrackState& state);

// inline implementations

inline TrackState::TrackState()
    : m_params(Vector6::Constant(std::numeric_limits<Scalar>::quiet_NaN()))
    , m_cov(SymMatrix6::Constant(std::numeric_limits<Scalar>::quiet_NaN()))
    , m_track(kInvalidIndex)
    , m_matchedCluster(kInvalidIndex)
{
}

template <typename Params, typename Covariance>
inline TrackState::TrackState(const Eigen::MatrixBase<Params>& params,
                              const Eigen::MatrixBase<Covariance>& cov)
    : m_params(params)
    , m_cov(cov.template selfadjointView<Eigen::Lower>())
    , m_track(kInvalidIndex)
    , m_matchedCluster(kInvalidIndex)
{
}

template <typename Covariance>
inline void TrackState::setCov(const Eigen::MatrixBase<Covariance>& cov)
{
  m_cov = cov.template selfadjointView<Eigen::Lower>();
}

template <typename OutputIterator>
inline void TrackState::getCovPacked(OutputIterator out) const
{
  // manual packing using symmetric, compressed, diagonal storage
  // clang-format off
  *(out++) = m_cov(kLoc0, kLoc0);
  *(out++) = m_cov(kLoc1, kLoc1);
  *(out++) = m_cov(kTime, kTime);
  *(out++) = m_cov(kSlopeLoc0, kSlopeLoc0);
  *(out++) = m_cov(kSlopeLoc1, kSlopeLoc1);
  *(out++) = m_cov(kSlopeTime, kSlopeTime);

  *(out++) = m_cov(kLoc0, kLoc1);
  *(out++) = m_cov(kLoc1, kTime);
  *(out++) = m_cov(kTime, kSlopeLoc0);
  *(out++) = m_cov(kSlopeLoc0, kSlopeLoc1);
  *(out++) = m_cov(kSlopeLoc1, kSlopeTime);
  *(out++) = m_cov(kLoc0, kTime);
  *(out++) = m_cov(kLoc1, kSlopeLoc0);
  *(out++) = m_cov(kTime, kSlopeLoc1);
  *(out++) = m_cov(kSlopeLoc0, kSlopeTime);
  *(out++) = m_cov(kLoc0, kSlopeLoc0);
  *(out++) = m_cov(kLoc1, kSlopeLoc1);
  *(out++) = m_cov(kTime, kSlopeTime);
  *(out++) = m_cov(kLoc0, kSlopeLoc1);
  *(out++) = m_cov(kLoc1, kSlopeTime);
  *(out++) = m_cov(kLoc0, kSlopeTime);
  // clang-format on
}

/**
 * Sets the covariance of the cluster from packed storage.
 * Refer to TrackState::getCovPacked() for the format.
*/
template <typename InputIterator>
inline void TrackState::setCovPacked(InputIterator in)  {
  m_cov(kLoc0, kLoc0) = *(in++);
  m_cov(kLoc1, kLoc1) = *(in++);
  m_cov(kTime, kTime) = *(in++);
  m_cov(kSlopeLoc0, kSlopeLoc0) = *(in++);
  m_cov(kSlopeLoc1, kSlopeLoc1) = *(in++);
  m_cov(kSlopeTime, kSlopeTime) = *(in++);

  m_cov(kLoc0, kLoc1) = m_cov(kLoc1, kLoc0) = *(in++);
  m_cov(kLoc1, kTime) = m_cov(kTime, kLoc1) = *(in++);
  m_cov(kTime, kSlopeLoc0) = m_cov(kSlopeLoc0, kTime) = *(in++);
  m_cov(kSlopeLoc0, kSlopeLoc1) = m_cov(kSlopeLoc1, kSlopeLoc0) = *(in++);
  m_cov(kSlopeLoc1, kSlopeTime) = m_cov(kSlopeTime, kSlopeLoc1) = *(in++);
  m_cov(kLoc0, kTime) = m_cov(kTime, kLoc0) = *(in++);
  m_cov(kLoc1, kSlopeLoc0) = m_cov(kSlopeLoc0, kLoc1) = *(in++);
  m_cov(kTime, kSlopeLoc1) = m_cov(kSlopeLoc1, kTime) = *(in++);
  m_cov(kSlopeLoc0, kSlopeTime) = m_cov(kSlopeTime, kSlopeLoc0) = *(in++);
  m_cov(kLoc0, kSlopeLoc0) = m_cov(kSlopeLoc0, kLoc0) = *(in++);
  m_cov(kLoc1, kSlopeLoc1) = m_cov(kSlopeLoc1, kLoc1) = *(in++);
  m_cov(kTime, kSlopeTime) = m_cov(kSlopeTime, kTime) = *(in++);
  m_cov(kLoc0, kSlopeLoc1) = m_cov(kSlopeLoc1, kLoc0) = *(in++);
  m_cov(kLoc1, kSlopeTime) = m_cov(kSlopeTime, kLoc1) = *(in++);
  m_cov(kLoc0, kSlopeTime) = m_cov(kSlopeTime, kLoc0) = *(in++);
}

inline Vector4 TrackState::position() const
{
  Vector4 pos;
  pos[kU] = m_params[kLoc0];
  pos[kV] = m_params[kLoc1];
  pos[kW] = 0;
  pos[kS] = m_params[kTime];
  return pos;
}

inline SymMatrix4 TrackState::positionCov() const
{
  SymMatrix4 cov = SymMatrix4::Zero();
  cov(kU, kU) = m_cov(kLoc0, kLoc0);
  cov(kV, kU) = m_cov(kLoc1, kLoc0);
  cov(kS, kU) = m_cov(kTime, kLoc0);
  cov(kU, kV) = m_cov(kLoc0, kLoc1);
  cov(kV, kV) = m_cov(kLoc1, kLoc1);
  cov(kS, kV) = m_cov(kTime, kLoc1);
  cov(kU, kS) = m_cov(kLoc0, kTime);
  cov(kV, kS) = m_cov(kLoc1, kTime);
  cov(kS, kS) = m_cov(kTime, kTime);
  // measurement is on the plane, i.e. w-components have no uncertainty
  return cov;
}

inline Vector4 TrackState::tangent() const
{
  Vector4 tgt;
  tgt[kU] = m_params[kSlopeLoc0];
  tgt[kV] = m_params[kSlopeLoc1];
  tgt[kW] = 1;
  tgt[kS] = m_params[kSlopeTime];
  return tgt;
}

} // namespace reco
