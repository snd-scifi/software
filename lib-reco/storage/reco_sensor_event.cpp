#include "utils/logger.hpp"

#include "reco_sensor_event.hpp"

namespace reco {

RecoSensorEvent::RecoSensorEvent(const RecoSensorEvent& other) {
    for (const auto& hit : other.m_hits) {
      m_hits.push_back(std::make_unique<RecoHit>(*hit));
    }

    for (const auto& cluster : other.m_clusters) {
      m_clusters.push_back(std::make_unique<RecoCluster>(*cluster));
    }

    m_trackStates = other.m_trackStates;
    m_caloTrackStates = other.m_caloTrackStates;
  }

RecoSensorEvent& RecoSensorEvent::operator=(const RecoSensorEvent& other) {
  if (this == &other) {
    return *this;
  }

  clear();
  m_hits.reserve(other.m_hits.size());
  m_clusters.reserve(other.m_clusters.size());

  for (const auto& hit : other.m_hits) {
    m_hits.push_back(std::make_unique<RecoHit>(*hit));
  }

  for (const auto& cluster : other.m_clusters) {
    m_clusters.push_back(std::make_unique<RecoCluster>(*cluster));
  }

  m_trackStates = other.m_trackStates;
  m_caloTrackStates = other.m_caloTrackStates;

  return *this;
}


bool RecoSensorEvent::hasLocalState(Index itrack) const {
  return (std::find_if(m_trackStates.begin(), m_trackStates.end(), [=] (const TrackState& state) {
                         return (state.track() == itrack);
                       }) != m_trackStates.end());
}

const TrackState& RecoSensorEvent::getLocalState(Index itrack) const {
  auto it = std::find_if(m_trackStates.begin(), m_trackStates.end(), [=] (const TrackState& state) { return (state.track() == itrack); });
  
  if (it == m_trackStates.end()) {
    THROW_OOR("Invalid track index.");
  }
  return *it;
}

bool RecoSensorEvent::hasCaloLocalState(Index iCaloTrack) const {
  return (std::find_if(m_caloTrackStates.begin(), m_caloTrackStates.end(), [=] (const TrackState& state) {
                         return (state.track() == iCaloTrack);
                       }) != m_caloTrackStates.end());
}

const TrackState& RecoSensorEvent::getCaloLocalState(Index iCaloTrack) const {
  auto it = std::find_if(m_caloTrackStates.begin(), m_caloTrackStates.end(), [=] (const TrackState& state) { return (state.track() == iCaloTrack); });
  
  if (it == m_caloTrackStates.end()) {
    THROW_OOR("Invalid cal track index.");
  }
  return *it;
}

void RecoSensorEvent::clear() {
  m_hits.clear();
  m_clusters.clear();
  m_trackStates.clear();
}

void RecoSensorEvent::addMatch(Index icluster, Index itrack) {
  auto isInTrack = [=](const TrackState& state) {
    return (state.track() == itrack);
  };
  auto& cluster = m_clusters.at(icluster);
  auto state = std::find_if(m_trackStates.begin(), m_trackStates.end(), isInTrack);
  if (state == m_trackStates.end()) {
    THROW_OOR("Invalid track index");
  }

  // remove previous associations
  if (cluster->isMatched()) {
    auto other = std::find_if(m_trackStates.begin(), m_trackStates.end(), isInTrack);
    if (other != m_trackStates.end()) {
      other->m_matchedCluster = kInvalidIndex;
    }
  }
  if (state->isMatched()) {
    m_clusters.at(state->matchedCluster())->m_matchedState = kInvalidIndex;
  }

  // set new association
  cluster->m_matchedState = itrack;
  state->m_matchedCluster = icluster;
}


} // namespace reco