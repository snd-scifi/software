#pragma once

#include <vector>

#include "storage/reco_hit.hpp"
#include "storage/reco_cluster.hpp"
#include "storage/track_state.hpp"

namespace reco {

class SciFiClusterizer;
class NonClusterizer;

class RecoSensorEvent {
public:
  RecoSensorEvent() = default;
  RecoSensorEvent(const RecoSensorEvent& other);
  RecoSensorEvent& operator=(const RecoSensorEvent& other);
  

  void clear();

  template <typename... Params>
  RecoHit& addHit(Params&&... params);
  std::size_t numHits() const { return m_hits.size(); }
  RecoHit& getHit(std::size_t ihit) { return *m_hits.at(ihit); }
  const RecoHit& getHit(std::size_t ihit) const { return *m_hits.at(ihit); }
  // void sortHits() { std::sort(m_hits.begin(), m_hits.end(), 
  //   [] (const auto* a, const auto* b) { return a->channel() < b->channel(); }
  // ); }

  template <typename... Params>
  RecoCluster& addCluster(Params&&... params);
  std::size_t numClusters() const { return m_clusters.size(); }
  RecoCluster& getCluster(std::size_t icluster) { return *m_clusters.at(icluster); }
  const RecoCluster& getCluster(std::size_t icluster) const { return *m_clusters.at(icluster); }

  template <typename... Params>
  void setLocalState(Index itrack, Params&&... params);
  /** Check if a local state is available for a specific track. */
  bool hasLocalState(Index itrack) const;
  const TrackState& getLocalState(Index itrack) const;
  const std::vector<TrackState>& localStates() const { return m_trackStates; }

  template <typename... Params>
  void setCaloLocalState(Index iCaloTrack, Params&&... params);
  /** Check if a local state is available for a specific track. */
  bool hasCaloLocalState(Index iCaloTrack) const;
  const TrackState& getCaloLocalState(Index iCaloTrack) const;
  const std::vector<TrackState>& localCaloStates() const { return m_caloTrackStates; }

  /** Associate one cluster to one track state.
   *
   * Any previously existing association for either the cluster or the track
   * will be overwritten.
   */
  void addMatch(Index icluster, Index itrack);

private:
  std::vector<std::unique_ptr<RecoHit>> m_hits;
  std::vector<std::unique_ptr<RecoCluster>> m_clusters;
  std::vector<TrackState> m_trackStates;
  std::vector<TrackState> m_caloTrackStates;

  friend class SciFiClusterizer;  
  friend class NonClusterizer;  
};


template <typename... HitParams>
inline RecoHit& RecoSensorEvent::addHit(HitParams&&... params)
{
  m_hits.emplace_back(
      std::make_unique<RecoHit>(std::forward<HitParams>(params)...));
  return *m_hits.back();
  
}

template <typename... Params>
inline RecoCluster& RecoSensorEvent::addCluster(Params&&... params)
{
  m_clusters.emplace_back(
      std::make_unique<RecoCluster>(std::forward<Params>(params)...));
  m_clusters.back()->m_index = m_clusters.size() - 1;
  return *m_clusters.back();
}

template <typename... Params>
inline void RecoSensorEvent::setLocalState(Index itrack, Params&&... params) {
  auto it = std::find_if(m_trackStates.begin(), m_trackStates.end(), [=] (const TrackState& state) { return (state.track() == itrack); });
  
  if (it != m_trackStates.end()) {
    *it = TrackState(std::forward<Params>(params)...);
    it->m_track = itrack;
  }
  else {
    m_trackStates.emplace_back(std::forward<Params>(params)...);
    m_trackStates.back().m_track = itrack;
  }
}

template <typename... Params>
inline void RecoSensorEvent::setCaloLocalState(Index iCaloTrack, Params&&... params) {
  auto it = std::find_if(m_caloTrackStates.begin(), m_caloTrackStates.end(), [=] (const TrackState& state) { return (state.track() == iCaloTrack); });
  
  if (it != m_caloTrackStates.end()) {
    *it = TrackState(std::forward<Params>(params)...);
    it->m_track = iCaloTrack;
  }
  else {
    m_caloTrackStates.emplace_back(std::forward<Params>(params)...);
    m_caloTrackStates.back().m_track = iCaloTrack;
  }
}


} // namespace reco
