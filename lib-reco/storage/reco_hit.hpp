#pragma once

#include <cstdint>

#include "mechanics/sensor.hpp"
#include "storage/tofpet_hit.hpp"
#include "utils/definitions.hpp"

namespace reco {

class RecoHit : public TofpetHit {
public:
  RecoHit(const int32_t channel, const double timestamp, const double value, const Vector4 position, const Index clusterIndex);
  RecoHit(const int32_t channel, const double timestamp, const double value, const double u, const double v, const double w, const double s, const Index clusterIndex);
  // RecoHit(const TofpetHit& tofpetHit, const int32_t channel, const double timestamp, const double value, const Vector4 position, const Index clusterIndex);
  RecoHit(const TofpetHit& tofpetHit, const Sensor& sensor, const int index); // used in the RecoEvent constructor

  int32_t channel() const { return m_channel; }
  const Vector4& position() const { return m_pos; }
  const SymMatrix4& covariance() const { return m_cov; }

  void setPosition(Vector4 position) { m_pos = position; }
  void setCovariance(SymMatrix4 cov) { m_cov = cov; }
  
  template <typename OutputIterator>
  void getCovPacked(OutputIterator out) const;
  template <typename OutputIterator>
  void getCovPackedTransformed(OutputIterator out, Matrix4 jacobian) const;
  template <typename InputIterator>
  void setCovPacked(InputIterator in);
  
  Index clusterIndex() const { return m_clusterIndex; }
  void setClusterIndex(Index index) { m_clusterIndex = index; }

private:
  const int32_t m_channel;
  Vector4 m_pos;
  SymMatrix4 m_cov;
  Index m_clusterIndex;

};


template <typename OutputIterator>
inline void RecoHit::getCovPacked(OutputIterator out) const
{
  // manual packing using symmetric, compressed, diagonal storage
  // clang-format off
  *(out++) = m_cov(kU, kU);
  *(out++) = m_cov(kV, kV);
  *(out++) = m_cov(kW, kW);
  *(out++) = m_cov(kS, kS);
  *(out++) = m_cov(kU, kV);
  *(out++) = m_cov(kV, kW);
  *(out++) = m_cov(kW, kS);
  *(out++) = m_cov(kU, kW);
  *(out++) = m_cov(kV, kS);
  *(out++) = m_cov(kU, kS);
  // clang-format on
}


template <typename OutputIterator>
inline void RecoHit::getCovPackedTransformed(OutputIterator out, Matrix4 jacobian) const
{ 
  auto cov = transformCovariance(jacobian, m_cov);
  // manual packing using symmetric, compressed, diagonal storage
  // clang-format off
  *(out++) = cov(kU, kU);
  *(out++) = cov(kV, kV);
  *(out++) = cov(kW, kW);
  *(out++) = cov(kS, kS);
  *(out++) = cov(kU, kV);
  *(out++) = cov(kV, kW);
  *(out++) = cov(kW, kS);
  *(out++) = cov(kU, kW);
  *(out++) = cov(kV, kS);
  *(out++) = cov(kU, kS);
  // clang-format on
}


template <typename InputIterator>
inline void RecoHit::setCovPacked(InputIterator in)  {
  m_cov(kU, kU) = *(in++);
  m_cov(kV, kV) = *(in++);
  m_cov(kW, kW) = *(in++);
  m_cov(kS, kS) = *(in++);
  m_cov(kU, kV) = m_cov(kV, kU) = *(in++);
  m_cov(kV, kW) = m_cov(kW, kV) = *(in++);
  m_cov(kW, kS) = m_cov(kS, kW) = *(in++);
  m_cov(kU, kW) = m_cov(kW, kU) = *(in++);
  m_cov(kV, kS) = m_cov(kS, kV) = *(in++);
  m_cov(kU, kS) = m_cov(kS, kU) = *(in++);
}

} // namespace reco