#pragma once

#include <map>
#include <string>
#include <vector>

#include "mechanics/device.hpp"
#include "storage/calo_track.hpp"
#include "storage/reco_sensor_event.hpp"
#include "storage/tofpet_event.hpp"
#include "storage/track.hpp"

#include "json.hpp"

namespace reco {

class RecoEvent {
public:
  RecoEvent(const Device& device);
  RecoEvent(const TofpetEvent& tofpetEvent, const Device& device);

  void addTofpetEvent(const TofpetEvent& tofpetEvent);

  void clear(int64_t timestamp=std::numeric_limits<int64_t>::max(), uint64_t eventNumber=std::numeric_limits<uint64_t>::max(), uint64_t flags=0);

  int64_t timestamp() const { return m_timestamp; }
  uint64_t evtNumber() const { return m_eventNumber; }
  uint64_t flags() const { return m_flags; }

  Index numSensorEvents() const { return static_cast<Index>(m_sensorEvents.size()); }
  RecoSensorEvent& getSensorEvent(Index i) { return m_sensorEvents.at(i); }
  const RecoSensorEvent& getSensorEvent(Index i) const { return m_sensorEvents.at(i); }

  /** Add track to the event and fix the cluster to track association. */
  void addTrack(const Track& track);
  Index numTracks() const { return static_cast<Index>(m_tracks.size()); }
  Track& getTrack(Index i) { return m_tracks.at(i); }
  const Track& getTrack(Index i) const { return m_tracks.at(i); }

  /** Add calo track to the event and fix the cluster to track association. */
  void addCaloTrack(const CaloTrack& track);
  Index numCaloTracks() const { return static_cast<Index>(m_caloTracks.size()); }
  CaloTrack& getCaloTrack(Index i) { return m_caloTracks.at(i); }
  const CaloTrack& getCaloTrack(Index i) const { return m_caloTracks.at(i); }

  auto begin() { return m_sensorEvents.begin(); }
  auto end() { return m_sensorEvents.end(); }
  auto cbegin() const { return m_sensorEvents.cbegin(); }
  auto cend() const { return m_sensorEvents.cend(); }
  auto begin() const { return m_sensorEvents.begin(); }
  auto end() const { return m_sensorEvents.end(); }

  RecoEvent merge(const RecoEvent& event);


private:
  int64_t m_timestamp;
  uint64_t m_flags;
  uint64_t m_eventNumber;
  
  std::vector<RecoSensorEvent> m_sensorEvents;
  std::vector<Track> m_tracks;
  std::vector<CaloTrack> m_caloTracks;
  
};

} // namespace reco