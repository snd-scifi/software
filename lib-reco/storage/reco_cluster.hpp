#pragma once

#include <vector>

#include "storage/reco_hit.hpp"
#include "utils/definitions.hpp"

namespace reco {

using RecoHits = std::vector<std::reference_wrapper<RecoHit>>;

class RecoCluster {
public:
  RecoCluster(Vector4 position, SymMatrix4 covariance, double value, Index trackIndex=kInvalidIndex, Index caloTrackindex=kInvalidIndex, Index matchIndex=kInvalidIndex);
  
  const Vector4& position() const { return m_pos; }
  const SymMatrix4& covariance() const { return m_posCov; }
  template <typename OutputIterator>
  void getCovPacked(OutputIterator out) const;
  template <typename OutputIterator>
  void getCovPackedTransformed(OutputIterator out, Matrix4 jacobian) const;
  template <typename InputIterator>
  void setCovPacked(InputIterator in);
  double value() const { return m_value; }

  void setPosition(Vector4 position) { m_pos = position; }
  void setCovariance(SymMatrix4 cov) { m_posCov = cov; }

  std::size_t size() const { return m_hits.size(); }
  const RecoHits& hits() const { return m_hits; }
  void addHit(RecoHit& hit);

  void setTrack(Index track);
  bool isInTrack() const { return m_track != kInvalidIndex; }
  Index track() const { return m_track; }

  void setCaloTrack(Index caloTrack);
  bool isInCaloTrack() const { return m_track != kInvalidIndex; }
  Index caloTrack() const { return m_caloTrack; }

  bool isMatched() const { return m_matchedState != kInvalidIndex; }
  Index matchedState() const { return m_matchedState; }

  Index getClusterIndex() const { return m_index; }


private:
  Vector4 m_pos;
  SymMatrix4 m_posCov;
  double m_value;

  Index m_index; // this is set by RecoSensorEvent
  Index m_track;
  Index m_caloTrack;
  Index m_matchedState;

  RecoHits m_hits;

  friend class RecoSensorEvent;

};


  /** Store the spatial covariance into packed storage.
   *
   * The iterator must point to a 10 element container that will be filled
   * with the lower triangular block of the symmetric covariance matrix for the
   * parameters [x, y, z, t] (or [u, v, w, s]) in "diagonal major" layout.
   *
   *     | c[0]                |
   *     | c[4] c[1]           |
   *     | c[7] c[5] c[2]      |
   *     | c[9] c[8] c[6] c[3] |
   *
   */
template <typename OutputIterator>
inline void RecoCluster::getCovPacked(OutputIterator out) const
{
  // manual packing using symmetric, compressed, diagonal storage
  // clang-format off
  *(out++) = m_posCov(kU, kU);
  *(out++) = m_posCov(kV, kV);
  *(out++) = m_posCov(kW, kW);
  *(out++) = m_posCov(kS, kS);
  *(out++) = m_posCov(kU, kV);
  *(out++) = m_posCov(kV, kW);
  *(out++) = m_posCov(kW, kS);
  *(out++) = m_posCov(kU, kW);
  *(out++) = m_posCov(kV, kS);
  *(out++) = m_posCov(kU, kS);
  // clang-format on
}

/**
 * Same as SciFiCluster::getCovPacked(), but the covariance is transformed using
 * the provided jacobian matrix.
*/
template <typename OutputIterator>
inline void RecoCluster::getCovPackedTransformed(OutputIterator out, Matrix4 jacobian) const
{ 
  auto posCov = transformCovariance(jacobian, m_posCov);
  // manual packing using symmetric, compressed, diagonal storage
  // clang-format off
  *(out++) = posCov(kU, kU);
  *(out++) = posCov(kV, kV);
  *(out++) = posCov(kW, kW);
  *(out++) = posCov(kS, kS);
  *(out++) = posCov(kU, kV);
  *(out++) = posCov(kV, kW);
  *(out++) = posCov(kW, kS);
  *(out++) = posCov(kU, kW);
  *(out++) = posCov(kV, kS);
  *(out++) = posCov(kU, kS);
  // clang-format on
}

/**
 * Sets the covariance of the cluster from packed storage.
 * Refer to SciFiCluster::getCovPacked() for the format.
*/
template <typename InputIterator>
inline void RecoCluster::setCovPacked(InputIterator in)  {
  m_posCov(kU, kU) = *(in++);
  m_posCov(kV, kV) = *(in++);
  m_posCov(kW, kW) = *(in++);
  m_posCov(kS, kS) = *(in++);
  m_posCov(kU, kV) = m_posCov(kV, kU) = *(in++);
  m_posCov(kV, kW) = m_posCov(kW, kV) = *(in++);
  m_posCov(kW, kS) = m_posCov(kS, kW) = *(in++);
  m_posCov(kU, kW) = m_posCov(kW, kU) = *(in++);
  m_posCov(kV, kS) = m_posCov(kS, kV) = *(in++);
  m_posCov(kU, kS) = m_posCov(kS, kU) = *(in++);
}

} // namespace reco