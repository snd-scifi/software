#include "vata_filter.hpp"

#include "processing/cluster_track_update.hpp"
#include "storage/reco_cluster.hpp"
#include "utils/logger.hpp"
#include "tracking/line_fitter.hpp"
#include "tracking/propagation.hpp"
#include "storage/track_state.hpp"
#include "storage/track.hpp"

#include <vector>
#include <iterator>

namespace reco {

VataFilter::VataFilter(const nlohmann::json& settings) 
  : m_thresholdValue(settings.value("threshold_value", 0.))
{
 
}

bool VataFilter::process(RecoSensorEvent& event) const {
  RecoSensorEvent newEvent;
  for (size_t ihit{0}; ihit < event.numHits(); ihit++) {
    RecoHit& hit = event.getHit(ihit);
    if (hit.value() > m_thresholdValue) {
      newEvent.addHit(std::move(hit));
    }
  }
  event = newEvent;
  return true;
}

} // namespace reco