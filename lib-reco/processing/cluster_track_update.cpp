#include "cluster_track_update.hpp"

namespace reco {

void updateCluster(RecoCluster& cluster, const TrackState& trackState) {
    auto pos = cluster.position();
    auto posCov = cluster.covariance();

    pos[kV] = trackState.loc1();
    posCov(kV, kV) = trackState.loc01Cov()(kLoc1, kLoc1);

    pos[kS] -= pos[kV] / 150.; // 150 mm/ns speed of light in the fibre

    cluster.setPosition(pos);
    cluster.setCovariance(posCov);

    for (auto hit : cluster.hits()) {
      pos = hit.get().position();
      posCov = hit.get().covariance();

      pos[kV] = trackState.loc1();
      posCov(kV, kV) = trackState.loc01Cov()(kLoc1, kLoc1);
      pos[kS] -= pos[kV] / 150.; // 150 mm/ns speed of light in the fibre

      hit.get().setPosition(pos);
      hit.get().setCovariance(posCov);
    }
}

}