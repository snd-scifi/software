#include "non_clusterizer.hpp"

#include "storage/reco_cluster.hpp"


namespace reco {

NonClusterizer::NonClusterizer(const nlohmann::json& configuration) {}


bool NonClusterizer::process(RecoSensorEvent& sensorEvent) const {
  for (const auto& hit : sensorEvent.m_hits) {
    Vector4 pos = hit->position();
    SymMatrix4 cov = hit->covariance();
    const auto& value = hit->value();
    auto& cluster = sensorEvent.addCluster(pos, cov, value);
    cluster.addHit(*hit);
  }

  return true;
}

} // namespace reco