#pragma once

#include <string>
#include <vector>

#include "io/reco_event_reader.hpp"
#include "io/reco_event_writer.hpp"
#include "mechanics/device.hpp"
#include "processing/reco_event_processor.hpp"
#include "processing/reco_sensor_event_processor.hpp"

#include "json.hpp"

namespace reco {

class EventLoop {
public:
  enum class InputDataType {
    RawRoot,
    ProcessedRoot
  };

  EventLoop(const nlohmann::json& configuration, const std::string& inputPath, const std::string& outputPath, const uint64_t num=-1, const uint64_t skip=0, const bool noProgress=false, const InputDataType dataType=InputDataType::RawRoot);
  ~EventLoop() = default;

  void loop();

private:
  static std::unique_ptr<RecoEventReader> getReader(const InputDataType dataType, const std::string& inputPath, const Device& device);
  const Device m_dev;
  std::unique_ptr<RecoEventReader> m_reader;
  std::vector<std::vector<std::unique_ptr<RecoSensorEventProcessor>>> m_sensorProcessors;
  std::vector<std::unique_ptr<RecoEventProcessor>> m_processors;
  std::unique_ptr<RecoEventWriter> m_writer;
  const uint64_t m_maxEvents;
  const uint64_t m_skipEvents;
  const bool m_showProgress;

};


std::istream& operator>> (std::istream& is, EventLoop::InputDataType& dataType);
std::ostream& operator<< (std::ostream& os, const EventLoop::InputDataType& dataType);


} // namespace reco