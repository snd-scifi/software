#pragma once

#include <string>

#include "mechanics/device.hpp"
#include "processing/reco_sensor_event_processor.hpp"

#include "json.hpp"

namespace reco {

/**
 * Non-clusterizer, i.e. each hit will correspond to a cluster.
 */
class NonClusterizer : public RecoSensorEventProcessor {
public:
  NonClusterizer(const nlohmann::json& configuration);
  ~NonClusterizer() = default;

  std::string name() const { return "NonClusterizer"; }

  bool process(RecoSensorEvent& event) const override;

};

} // namespace reco