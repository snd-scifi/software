#include "ecal_simple_tracker.hpp"

#include "processing/cluster_track_update.hpp"
#include "storage/reco_cluster.hpp"
#include "utils/logger.hpp"
#include "tracking/line_fitter.hpp"
#include "tracking/propagation.hpp"
#include "storage/track_state.hpp"
#include "storage/calo_track.hpp"

#include <vector>
#include <iterator>

namespace reco {

EcalSimpleTracker::EcalSimpleTracker(const Device& device, const nlohmann::json& settings) 
  : m_device(device)
  , m_trackingSensorIds(settings.value("tracking_sensor_ids", std::vector<Index>()))
  , m_clustersMin(settings.value("clusters_min", 3))
  , m_clustersMax(settings.value("clusters_max", 8))
{

  if (m_trackingSensorIds.size() != 2) {
    THROW_RE("There must be exactly 2 sensor IDs");
  }

  if (m_clustersMin < 2) {
    THROW_RE("EcalSimpleTracker: Only {} clusters required for tracking in X and Y separately.", m_clustersMin);
  }

}

bool EcalSimpleTracker::process(RecoEvent& event) const {
  std::vector<std::tuple<double, double, double>> pointsX;
  std::vector<std::tuple<double, double, double>> pointsY;

  CaloTrack trackXY; // track used to correct timestamp and add v
  CaloTrack track; // final track


  // if one of the projections doesn't respect the clusters requirements, just return
  for (const auto sensorId : m_trackingSensorIds) {
    const auto& sensorEvent = event.getSensorEvent(sensorId);
    if (sensorEvent.numClusters() < m_clustersMin || sensorEvent.numClusters() > m_clustersMax) {
      return true;
    }
  }


  // all clusters are used for tracking in the calo
  for (const auto sensorId : m_trackingSensorIds) {
    const auto& sensorEvent = event.getSensorEvent(sensorId);
    const auto& plane = m_device.geometry().getPlane(sensorId);

    // Here we work in the assumption that we have X and Y planes only, so each cluster will be either X or Y
    // and the fit will be separate in X and Y
    const auto isX = std::abs(plane.linearToGlobal()(0, 0)) > 0.5;

    for (Index clusterIdx{0}; clusterIdx < sensorEvent.numClusters(); clusterIdx++) {
      trackXY.addCluster(sensorId, clusterIdx);
      track.addCluster(sensorId, clusterIdx);
      const auto& pos = plane.toGlobal(sensorEvent.getCluster(clusterIdx).position());
      const auto& cov = sensorEvent.getCluster(clusterIdx).covariance();
      Vector4 weight = transformCovariance(plane.linearToGlobal(), cov).diagonal().cwiseInverse();
      isX ? pointsX.push_back({pos[kX], pos[kZ], weight[kX]}) : pointsY.push_back({pos[kY], pos[kZ], weight[kY]});
    }
  }
  
  LineFitter fitterX;
  LineFitter fitterY;

  for (const auto& [x, z, w] : pointsX) {
    fitterX.addPoint(z, x, w);
  }
  fitterX.fit();

  for (const auto& [y, z, w] : pointsY) {
    fitterY.addPoint(z, y, w);
  }
  fitterY.fit();

  const Vector6 paramsXY{fitterX.offset(), fitterY.offset(), 0., fitterX.slope(), fitterY.slope(), 0.};
  SymMatrix6 paramsXYCov = SymMatrix6::Zero();
  
  paramsXYCov(kLoc0, kLoc0) = fitterX.varOffset();
  paramsXYCov(kSlopeLoc0, kSlopeLoc0) = fitterX.varSlope();
  paramsXYCov(kLoc0, kSlopeLoc0) = paramsXYCov(kSlopeLoc0, kLoc0) = fitterX.cov();
  paramsXYCov(kLoc1, kLoc1) = fitterY.varOffset();
  paramsXYCov(kSlopeLoc1, kSlopeLoc1) = fitterY.varSlope();
  paramsXYCov(kLoc1, kSlopeLoc1) = paramsXYCov(kSlopeLoc1, kLoc1) = fitterY.cov();

  trackXY.setGlobalState(TrackState(paramsXY, paramsXYCov));
  trackXY.setGoodnessOfFit(fitterX.chi2() + fitterY.chi2(), fitterX.dof() + fitterY.dof());
  
  std::vector<std::tuple<double, double, double>> pointsT;

  // here we correct the cluster time for the clusters in the track and obtain the points to fit the time part of the track
  for (const auto sensorId : m_trackingSensorIds) {
    auto clusters = trackXY.getClustersOn(sensorId);
    for (const auto cluIdx : clusters) {
      // get a reference to the cluster that is part of the track
      auto& cluster = event.getSensorEvent(sensorId).getCluster(cluIdx);
      const auto& plane = m_device.geometry().getPlane(sensorId);

      // extrapolate the track on the plane of this cluster
      auto trackState = propagateTo(trackXY.globalState(), Plane(), plane);

      // correct timestamp and add v position
      updateCluster(cluster, trackState);

      // fill the T points
      const auto& pos = plane.toGlobal(cluster.position());
      const auto& cov = cluster.covariance();
      Vector4 weight = transformCovariance(plane.linearToGlobal(), cov).diagonal().cwiseInverse();
      pointsT.push_back({pos[kT], pos[kZ], weight[kT]});
    }
  } 

  LineFitter fitterT;

  for (const auto& [t, z, w] : pointsT) {
    fitterT.addPoint(z, t, w);
  }
  fitterT.fit();

  const Vector6 params{fitterX.offset(), fitterY.offset(), fitterT.offset(), fitterX.slope(), fitterY.slope(), fitterT.slope()};
  SymMatrix6 paramsCov = SymMatrix6::Zero();

  paramsCov(kLoc0, kLoc0) = fitterX.varOffset();
  paramsCov(kSlopeLoc0, kSlopeLoc0) = fitterX.varSlope();
  paramsCov(kLoc0, kSlopeLoc0) = paramsCov(kSlopeLoc0, kLoc0) = fitterX.cov();
  paramsCov(kLoc1, kLoc1) = fitterY.varOffset();
  paramsCov(kSlopeLoc1, kSlopeLoc1) = fitterY.varSlope();
  paramsCov(kLoc1, kSlopeLoc1) = paramsCov(kSlopeLoc1, kLoc1) = fitterY.cov();
  paramsCov(kTime, kTime) = fitterT.varOffset();
  paramsCov(kSlopeTime, kSlopeTime) = fitterT.varSlope();
  paramsCov(kTime, kSlopeTime) = paramsCov(kSlopeTime, kTime) = fitterT.cov();

  track.setGlobalState(TrackState(params, paramsCov));
  track.setGoodnessOfFit(fitterX.chi2() + fitterY.chi2() + fitterT.chi2(), fitterX.dof() + fitterY.dof() + fitterT.dof());

  event.addCaloTrack(track);

  // add the local states in each plane
  for (Index iSensor = 0; iSensor < event.numSensorEvents(); iSensor++) {
    auto& sensorEvent = event.getSensorEvent(iSensor);
    const auto& plane = m_device.geometry().getPlane(iSensor);

    sensorEvent.setLocalState(0, propagateTo(track.globalState(), Plane(), plane));
  }

  return true;

}

} // namespace reco
