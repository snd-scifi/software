#include "track_event_filter.hpp"

#include "processing/cluster_track_update.hpp"
#include "storage/reco_cluster.hpp"
#include "utils/logger.hpp"
#include "tracking/line_fitter.hpp"
#include "tracking/propagation.hpp"
#include "storage/track_state.hpp"
#include "storage/track.hpp"

#include <vector>
#include <iterator>

namespace reco {

TrackEventFilter::TrackEventFilter(const nlohmann::json& settings) 
  : m_tracksMin(settings.value("n_tracks_min", 0))
  , m_redChi2Max(settings.value("red_chi2_max", 10000000.))
  , m_caloTracksMin(settings.value("n_calo_tracks_min", 0))
  , m_caloRedChi2Max(settings.value("calo_red_chi2_max", 10000000.))
  , m_andCondition(settings.value("and_condition", true))
{
 
}

bool TrackEventFilter::process(RecoEvent& event) const {
  auto tMin = m_tracksMin <= event.numTracks();
  auto ctMin = m_caloTracksMin <= event.numCaloTracks();

  // if the minimum number of tracks is 0, the chi2 doesn't matter anymore
  bool tChi2{m_tracksMin == 0 ? true : false};
  for (unsigned int i = 0; (i < event.numTracks()) && !tChi2; i++) {
    const auto& track = event.getTrack(i);
    if (track.chi2()/track.degreesOfFreedom() < m_redChi2Max) {
      tChi2 = true;
    }
  }

  bool ctChi2{m_caloTracksMin == 0 ? true : false};
  for (unsigned int i = 0; (i < event.numCaloTracks()) && !ctChi2; i++) {
    const auto& caloTrack = event.getCaloTrack(i);
    if (caloTrack.chi2()/caloTrack.degreesOfFreedom() < m_caloRedChi2Max) {
      ctChi2 = true;
    }
  }

  if (m_andCondition) {
    return (tMin && tChi2) && (ctMin && ctChi2);
  }
  else {
    return (tMin && tChi2) || (ctMin && ctChi2);
  }
}

} // namespace reco