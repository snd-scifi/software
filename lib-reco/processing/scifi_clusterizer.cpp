#include "scifi_clusterizer.hpp"

#include "storage/reco_cluster.hpp"


namespace reco {

SciFiClusterizer::SciFiClusterizer(const nlohmann::json& configuration)
  : m_allowedGap{configuration.value("allowed_gap", 0U)}
  {}

// return true if both hits are connected, i.e. share one edge
//
// WARNING: hits w/ the same position are counted as connected
static inline bool connected(const RecoHit& hit0, const RecoHit& hit1, const std::size_t allowedGap = 0)
{
  return std::abs(hit1.channel() - hit0.channel()) <= 1 + allowedGap;
}

// return true if the hit is connected to any hit in the range
template <typename HitIterator>
static inline bool
connected(HitIterator clusterBegin, HitIterator clusterEnd, const RecoHit& hit, const std::size_t allowedGap = 0)
{
  bool flag = false;
  for (; clusterBegin != clusterEnd; ++clusterBegin) {
    flag = (flag or connected(*clusterBegin->get(), hit, allowedGap));
  }
  return flag;
}

// rearange the input hit range so that pixels in a cluster are neighbours.
template <typename HitIterator, typename ClusterMaker>
static inline void clusterize(RecoSensorEvent& sensorEvent,
                              HitIterator hitsBegin,
                              HitIterator hitsEnd,
                              ClusterMaker makeCluster,
                              const std::size_t allowedGap = 0)
{
  // group all connected hits starting from an arbitrary seed hit (first hit).
  auto clusterBegin = hitsBegin;
  while (clusterBegin != hitsEnd) {
    // every cluster has at least one member
    auto clusterEnd = std::next(clusterBegin);

    // each iteration can only pick up the nearest-neighboring pixels, so we
    // need to iterate until we find no more connected pixels.
    while (clusterEnd != hitsEnd) {
      // accumulate all connected hits to the beginning of the range
      auto moreHits = std::partition(clusterEnd, hitsEnd, [=](const auto& hit) {
        return connected(clusterBegin, clusterEnd, *hit, allowedGap);
      });
      // no connected hits were found -> cluster is complete
      if (moreHits == clusterEnd) {
        break;
      }
      // some connected hits were found -> extend cluster
      clusterEnd = moreHits;
    }

    // sort cluster hits by value and time
    // auto compare = [](const std::unique_ptr<Hit>& hptr0,
    //                   const std::unique_ptr<Hit>& hptr1) {
    //   const auto& hit0 = *hptr0;
    //   const auto& hit1 = *hptr1;
    //   // 1. sort by value, highest first
    //   if (hit0.value() > hit1.value())
    //     return true;
    //   if (hit1.value() > hit0.value())
    //     return false;
    //   // 2. sort by timestamp, lowest first
    //   if (hit0.timestamp() < hit1.timestamp())
    //     return true;
    //   if (hit1.timestamp() < hit0.timestamp())
    //     return false;
    //   // equivalent hits w/ respect to value and time
    //   return false;
    // };
    // std::sort(clusterBegin, clusterEnd, compare);

    // add cluster to event
    auto& cluster = sensorEvent.addCluster(makeCluster(clusterBegin, clusterEnd));
    for (auto hit = clusterBegin; hit != clusterEnd; ++hit) {
      cluster.addHit(*hit->get());
    }

    // only consider the remaining hits for the next cluster
    clusterBegin = clusterEnd;
  }
}


bool SciFiClusterizer::process(RecoSensorEvent& event) const {
  auto makeCluster = [&](auto h0, auto h1) {
    Vector4 position{0, 0, 0, std::numeric_limits<double>::quiet_NaN()};
    SymMatrix4 cov = SymMatrix4::Zero();
    double value{0};
    int hitCounter{0};

    for (; h0 != h1; ++h0) {
      const RecoHit& hit = *(h0->get());
      
      position.head<3>() += hit.position().head<3>();
      // the timestamp is the smaller timestamp
      if ((hit.position()[kS] < position[kS]) || std::isnan(position[kS])){
        position[kS] = hit.position()[kS];
        // this is under the assumption that all crossed covariances between time and position are 0
        cov(kS, kS) = hit.covariance()(kS, kS);
      }
      
      cov.block<3, 3>(0, 0) += hit.covariance().block<3, 3>(0, 0);
      
      value += hit.value();
      hitCounter++;
    }
    position.head<3>() /= hitCounter;

    return RecoCluster(position, cov, value);
  };

  auto hitsEnd = event.m_hits.end();
  clusterize(event, event.m_hits.begin(), hitsEnd, makeCluster, m_allowedGap);
  return true;
}

} // namespace reco