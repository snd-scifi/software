#pragma once

#include "storage/reco_cluster.hpp"
#include "storage/track_state.hpp"

namespace reco {

void updateCluster(RecoCluster& cluster, const TrackState& trackState);

}