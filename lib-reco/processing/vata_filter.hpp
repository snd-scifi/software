#pragma once

#include <string>

#include "mechanics/device.hpp"
#include "processing/reco_sensor_event_processor.hpp"

#include "json.hpp"

namespace reco {

/**
 * Processors that performs zero-suppression on VATA data.
 * It assumes no clusters or tracks are present in the event.
 */
class VataFilter : public RecoSensorEventProcessor {
public:
  VataFilter(const nlohmann::json& settings);
  ~VataFilter() = default;

  std::string name() const { return "VataFilter"; }

  /**
   * Processes the event and returns true if the event should be saved, false otherwise.
   */
  bool process(RecoSensorEvent& event) const override;
  // void finalize() {};

private:
  const double m_thresholdValue;
};

} // namespace reco