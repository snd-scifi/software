#pragma once

#include <string>

#include "mechanics/device.hpp"
#include "processing/reco_event_processor.hpp"

#include "json.hpp"

namespace reco {

/**
 * Base event processor class.
 */
class EcalSimpleTracker : public RecoEventProcessor {
public:
  EcalSimpleTracker(const Device& device, const nlohmann::json& settings);
  ~EcalSimpleTracker() = default;

  std::string name() const { return "EcalSimpleTracker"; }

  /**
   * Processes the event and returns true if the event should be saved, false otherwise.
   */
  bool process(RecoEvent& event) const override;
  // void finalize() {};

private:
  const Device& m_device;
  const std::vector<Index> m_trackingSensorIds;
  const unsigned int m_clustersMin;
  const unsigned int m_clustersMax;
};

} // namespace reco