#pragma once

#include <string>

#include "mechanics/device.hpp"
#include "processing/reco_event_processor.hpp"

#include "json.hpp"

namespace reco {

/**
 * Base event processor class.
 */
class TrackEventFilter : public RecoEventProcessor {
public:
  TrackEventFilter(const nlohmann::json& settings);
  ~TrackEventFilter() = default;

  std::string name() const { return "TrackEventFilter"; }

  /**
   * Processes the event and returns true if the event should be saved, false otherwise.
   */
  bool process(RecoEvent& event) const override;
  // void finalize() {};

private:
  const unsigned int m_tracksMin;
  const double m_redChi2Max;
  const unsigned int m_caloTracksMin;
  const double m_caloRedChi2Max;
  const bool m_andCondition;
};

} // namespace reco