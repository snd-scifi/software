#pragma once

#include <string>

#include "processing/reco_sensor_event_processor.hpp"

#include "json.hpp"

namespace reco {

/**
 * Base event processor class.
 */
class SciFiClusterizer : public RecoSensorEventProcessor {
public:
  SciFiClusterizer(const nlohmann::json& configuration);
  ~SciFiClusterizer() = default;

  std::string name() const { return "SciFiClusterizer"; }

  /**
   * Processes the event and returns true if the event should be saved, false otherwise.
   */
  bool process(RecoSensorEvent& event) const override;
  // void finalize() {};

private:
  unsigned int m_allowedGap;

};

} // namespace reco