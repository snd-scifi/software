#pragma once

#include <string>

#include "mechanics/device.hpp"
#include "processing/reco_event_processor.hpp"
#include "tracking/line_fitter.hpp"

#include "json.hpp"

namespace reco {

enum class FitCoordinate {
  XZ,
  YZ,
  TZ,
};

/**
 * Tracker enabling multiple tracks per event, using a brute-force search algorithm.
 * For both XZ and XY planes, all possible track candidates are searched and fitted using linear regression.
 * The track candidate with the best reduced chi squared statistics is retained as a good candidate, and the procedure is 
 * then repeated for the remaining clusters, until the number of hit sensor is smaller than m_clustersMin.
 * Then, the tracks in XZ and XY are associated in a track in XYZ space.
 * Since the tracks are evaluated according to their chi2, for multiple track events the association is essentially random.
 * Thus, the relevant information is basically the number of tracks and their 2D projection.
 */
class SciFiTracker : public RecoEventProcessor {
public:
  SciFiTracker(const Device& device, const nlohmann::json& settings);
  ~SciFiTracker() = default;

  std::string name() const { return "SciFiTracker"; }


  /**
   * Processes the event and returns true if the event should be saved, false otherwise.
   */
  bool process(RecoEvent& event) const override;
  // void finalize() {};

  void performAnalysis(std::vector<std::vector<RecoCluster>>& clusterArray, RecoEvent& event, std::vector<Track>& bestTracks,
    const std::vector<Index>& trackingIndices, const unsigned int& clusterMin, const FitCoordinate& fitCoordinate) const;

  void fitTrack(Track& track, const RecoEvent& event, const FitCoordinate& fitCoordinate) const;

private:
  const Device& m_device;
  const std::vector<Index> m_trackingSensorIds;
  const std::vector<Index> m_matchingSensorIds;
  const unsigned int m_clustersMin;
  const unsigned int m_tracksMax;
  const unsigned int m_eventNClusterLimit;
};

} // namespace reco