#include "scifi_simple_tracker.hpp"

#include "processing/cluster_track_update.hpp"
#include "storage/reco_cluster.hpp"
#include "utils/logger.hpp"
#include "tracking/line_fitter.hpp"
#include "tracking/propagation.hpp"
#include "storage/track_state.hpp"
#include "storage/track.hpp"

#include <vector>
#include <iterator>

namespace reco {

SciFiSimpleTracker::SciFiSimpleTracker(const Device& device, const nlohmann::json& settings) 
  : m_device(device)
  , m_trackingSensorIds(settings.value("tracking_sensor_ids", std::vector<Index>()))
  , m_matchingSensorIds(settings.value("matching_sensor_ids", std::vector<Index>()))
  , m_clustersMin(settings.value("clusters_min", 2))
{
  if (m_trackingSensorIds.empty()) {
    THROW_RE("SciFiSimpleTracker: no sensors selected for tracking.");
  }
  if (m_clustersMin < 2) {
    THROW_RE("SciFiSimpleTracker: Only {} clusters required for tracking in X and Y separately.", m_clustersMin);
  }
}

bool SciFiSimpleTracker::process(RecoEvent& event) const {
  std::vector<std::tuple<double, double, double>> pointsX;
  std::vector<std::tuple<double, double, double>> pointsY;

  Track trackXY; // track used to correct timestamp and add v
  Track track; // final track

  // here we select the planes with exactly one cluster in at least m_clustersMin planes
  for (Index i = 0; i < event.numSensorEvents(); i++) {
    // if the sensor is not required for tracking, skip it
    if (std::find(m_trackingSensorIds.begin(), m_trackingSensorIds.end(), i) == m_trackingSensorIds.end()) {
      continue;
    }

    // we want exacly one cluster in each plane
    auto& sensorEvent = event.getSensorEvent(i);
    if (sensorEvent.numClusters() != 1) {
      continue;
    }

    // this cluster will be used for the track (in both cases)
    trackXY.addCluster(i, 0);
    track.addCluster(i, 0);

    // const auto& sensor = m_device.getSensor(i);
    const auto& plane = m_device.geometry().getPlane(i);

    // Here we work in the assumption that we have X and Y planes only, so each cluster will be either X or Y
    // and the fit will be separate in X and Y
    auto isX = std::abs(plane.linearToGlobal()(0, 0)) > 0.5;

    const auto& pos = plane.toGlobal(sensorEvent.getCluster(0).position());
    const auto& cov = sensorEvent.getCluster(0).covariance();
    Vector4 weight = transformCovariance(plane.linearToGlobal(), cov).diagonal().cwiseInverse();
    isX ? pointsX.push_back({pos[kX], pos[kZ], weight[kX]}) : pointsY.push_back({pos[kY], pos[kZ], weight[kY]});
  }

  // fit the track if the minimum number of clusters per plane is achieved
  if (pointsX.size() < m_clustersMin || pointsY.size() < m_clustersMin) {
    return true;
  }
  
  LineFitter fitterX;
  LineFitter fitterY;

  for (const auto& [x, z, w] : pointsX) {
    fitterX.addPoint(z, x, w);
  }
  fitterX.fit();

  for (const auto& [y, z, w] : pointsY) {
    fitterY.addPoint(z, y, w);
  }
  fitterY.fit();

  const Vector6 paramsXY{fitterX.offset(), fitterY.offset(), 0., fitterX.slope(), fitterY.slope(), 0.};
  SymMatrix6 paramsXYCov = SymMatrix6::Zero();
  
  paramsXYCov(kLoc0, kLoc0) = fitterX.varOffset();
  paramsXYCov(kSlopeLoc0, kSlopeLoc0) = fitterX.varSlope();
  paramsXYCov(kLoc0, kSlopeLoc0) = paramsXYCov(kSlopeLoc0, kLoc0) = fitterX.cov();
  paramsXYCov(kLoc1, kLoc1) = fitterY.varOffset();
  paramsXYCov(kSlopeLoc1, kSlopeLoc1) = fitterY.varSlope();
  paramsXYCov(kLoc1, kSlopeLoc1) = paramsXYCov(kSlopeLoc1, kLoc1) = fitterY.cov();

  trackXY.setGlobalState(TrackState(paramsXY, paramsXYCov));
  trackXY.setGoodnessOfFit(fitterX.chi2() + fitterY.chi2(), fitterX.dof() + fitterY.dof());
  
  std::vector<std::tuple<double, double, double>> pointsT;

  // here we correct the cluster time for the clusters in the track and obtain the points to fit the time part of the track
  for (Index i = 0; i < event.numSensorEvents(); i++) {
    if (!trackXY.hasClusterOn(i)) {
      continue;
    }
    
    // get a reference to the cluster that is part of the track
    auto& cluster = event.getSensorEvent(i).getCluster(trackXY.getClusterOn(i));
    const auto& plane = m_device.geometry().getPlane(i);

    // extrapolate the track on the plane of this cluster
    auto trackState = propagateTo(trackXY.globalState(), Plane(), plane);

    // correct timestamp and add v position
    updateCluster(cluster, trackState);

    // fill the T points
    const auto& pos = plane.toGlobal(cluster.position());
    const auto& cov = cluster.covariance();
    Vector4 weight = transformCovariance(plane.linearToGlobal(), cov).diagonal().cwiseInverse();
    pointsT.push_back({pos[kT], pos[kZ], weight[kT]});
  } 

  LineFitter fitterT;

  for (const auto& [t, z, w] : pointsT) {
    fitterT.addPoint(z, t, w);
  }
  fitterT.fit();

  const Vector6 params{fitterX.offset(), fitterY.offset(), fitterT.offset(), fitterX.slope(), fitterY.slope(), fitterT.slope()};
  SymMatrix6 paramsCov = SymMatrix6::Zero();

  paramsCov(kLoc0, kLoc0) = fitterX.varOffset();
  paramsCov(kSlopeLoc0, kSlopeLoc0) = fitterX.varSlope();
  paramsCov(kLoc0, kSlopeLoc0) = paramsCov(kSlopeLoc0, kLoc0) = fitterX.cov();
  paramsCov(kLoc1, kLoc1) = fitterY.varOffset();
  paramsCov(kSlopeLoc1, kSlopeLoc1) = fitterY.varSlope();
  paramsCov(kLoc1, kSlopeLoc1) = paramsCov(kSlopeLoc1, kLoc1) = fitterY.cov();
  paramsCov(kTime, kTime) = fitterT.varOffset();
  paramsCov(kSlopeTime, kSlopeTime) = fitterT.varSlope();
  paramsCov(kTime, kSlopeTime) = paramsCov(kSlopeTime, kTime) = fitterT.cov();

  track.setGlobalState(TrackState(params, paramsCov));
  track.setGoodnessOfFit(fitterX.chi2() + fitterY.chi2() + fitterT.chi2(), fitterX.dof() + fitterY.dof() + fitterT.dof());

  event.addTrack(track);

  // add the local states in each plane
  for (Index iSensor = 0; iSensor < event.numSensorEvents(); iSensor++) {
    auto& sensorEvent = event.getSensorEvent(iSensor);
    const auto& plane = m_device.geometry().getPlane(iSensor);

    sensorEvent.setLocalState(0, propagateTo(track.globalState(), Plane(), plane));
  }

  // match the clostest cluster of the matching sensors
  for (const auto sensorId : m_matchingSensorIds) {
    auto& sensorEvent = event.getSensorEvent(sensorId);
    const auto& trackState = sensorEvent.getLocalState(0); // we have only one track
    auto trackU = trackState.loc0();
    std::vector<double> trkCluDist;
    for (unsigned int i{0}; i < sensorEvent.numClusters(); i++) {
      trkCluDist.push_back(std::abs(trackU - sensorEvent.getCluster(i).position()[kU]));
    }

    if (trkCluDist.empty()) {
      continue;
    }

    auto idxMin = std::distance(std::begin(trkCluDist), std::min_element(std::begin(trkCluDist), std::end(trkCluDist)));
    updateCluster(sensorEvent.getCluster(idxMin), trackState);
    sensorEvent.addMatch(idxMin, 0);
  }

  return true;

}

} // namespace reco
