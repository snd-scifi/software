#pragma once

#include <string>

#include "storage/reco_event.hpp"

namespace reco {

/**
 * Base event processor class.
 */
class RecoEventProcessor {
public:
  virtual ~RecoEventProcessor() = default;

  virtual std::string name() const = 0;

  /**
   * Processes the event and returns true if the event should be saved, false otherwise.
   */
  virtual bool process(RecoEvent& event) const = 0;
  virtual void finalize() {};

};

} // namespace reco