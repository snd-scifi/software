/**
 * \author PYZIAK Lucas
 * \date 2024-03
 */


#include "scifi_tracker.hpp"

#include "processing/cluster_track_update.hpp"
#include "storage/reco_cluster.hpp"
#include "utils/logger.hpp"
#include "tracking/line_fitter.hpp"
#include "tracking/propagation.hpp"
#include "tracking/track_association.hpp"
#include "storage/track_state.hpp"
#include "storage/track.hpp"

#include <vector>
#include <iterator>
#include <list>
#include <algorithm>

namespace reco {




SciFiTracker::SciFiTracker(const Device& device, const nlohmann::json& settings)
  : m_device(device)
  , m_trackingSensorIds(settings.value("tracking_sensor_ids", std::vector<Index>()))
  , m_matchingSensorIds(settings.value("matching_sensor_ids", std::vector<Index>()))
  , m_clustersMin(settings.value("clusters_min", 2U))
  , m_tracksMax(settings.value("tracks_max", 256U))
  , m_eventNClusterLimit(settings.value("event_n_cluster_limit", -1U))
{
  if (m_trackingSensorIds.empty()) {
    THROW_RE("SciFiTracker: no sensors selected for tracking.");
  }
  if (m_clustersMin < 2) {
    THROW_RE("SciFiTracker: Only {} clusters required for tracking in X and Y separately.", m_clustersMin);
  }
}

/**
 * Counts the number of sensors, among the ones listes in indices, with at least one one cluster.
*/
unsigned int countActivatedSensors(const std::vector<std::vector<RecoCluster>>& clusterVector, const std::vector<Index>& indices) {
  unsigned int nSensorsTotal(0);
  for (const auto& index : indices) {
      if (clusterVector.at(index).size() > 0) {
        nSensorsTotal += 1;
      }
    }
  return nSensorsTotal;
}

/**
 * Returns an array with all the candidate tracks, i.e. all the cluster combinations among the selected sensors.
 * @param clusterArray: the clusters, split between sensors (the indices are sensorId and clusterId).
 * @param planeIndices: the indices of the sensors to be considered for tracking.
 * @return a vector containing the candidate tracks, with the track parameters to be determined.
*/ 
std::vector<Track> getCandidateTracks(const std::vector<std::vector<RecoCluster>>& clusterArray, const std::vector<Index>& planeIndices) {
    std::vector<Track> candidateTrackArray;
    bool ongoingTrack(false);
    unsigned int nCurrentTracks(0);
    unsigned int nClusters(0);

    for (auto planeIndex : planeIndices){
      auto& clusters = clusterArray.at(planeIndex);
      nClusters = clusters.size();
      if (nClusters > 0){
        // a track was not started, so the clusters on this plane act as "seeds"
        if (not ongoingTrack) {
          ongoingTrack = true;
          nCurrentTracks = nClusters;
          for (unsigned int i(0); i < nClusters; i++){
            auto& t = candidateTrackArray.emplace_back();
            t.addCluster(planeIndex, clusters.at(i).getClusterIndex());
          }
        }
        // a track was already ongoing
        else {
          // we loop over the current tracks and add each cluster to each of them.
          // if more than one cluster is present, a copy of each track is added for every additional cluster
          for (unsigned int j(0); j < nCurrentTracks; j++){   
            Track trackJCopy(candidateTrackArray.at(j));
            for (unsigned int clusterId(0); clusterId < nClusters; clusterId++){
              // the first cluster is added to the existing tracks
              if (clusterId == 0) {
                candidateTrackArray.at(j).addCluster(planeIndex, clusterArray.at(planeIndex).at(clusterId).getClusterIndex());  
              }
              // from the second on, a copy of the initial track candidate is added, with the current cluster
              else {
                auto& t = candidateTrackArray.emplace_back(trackJCopy);
                t.addCluster(planeIndex, clusterArray.at(planeIndex).at(clusterId).getClusterIndex());  
              }  
            }
          }
          nCurrentTracks *= nClusters;
        }
      }
    }
  return candidateTrackArray; 
}  

/**
  * Fits a single track. For XZ and YZ, the incoming track is empty, whereas for TZ the incoming track contains the XZ and YZ informations
  * (i.e. the incoming track is the associated XZ and YZ tracks)
  */
void SciFiTracker::fitTrack(Track& track, const RecoEvent& event, const FitCoordinate& fitCoordinate) const{
    LineFitter fitter;
    Vector6 params = Vector6::Zero();
    SymMatrix6 paramsCov = SymMatrix6::Zero();

    for (const auto& cluster : track.clusters()) {
      const auto& plane = m_device.geometry().getPlane(cluster.sensor);
      auto& clu = event.getSensorEvent(cluster.sensor).getCluster(cluster.cluster);
      
      const Vector4& pos(plane.toGlobal(clu.position()));
      const auto& cov = clu.covariance();
      Vector4 weight = transformCovariance(plane.linearToGlobal(), cov).diagonal().cwiseInverse();
      
      switch (fitCoordinate) {
      case FitCoordinate::XZ:
        fitter.addPoint(pos.z(), pos.x(), weight.x());
        break;

      case FitCoordinate::YZ:
        fitter.addPoint(pos.z(), pos.y(), weight.y());
        
        break;

      case FitCoordinate::TZ:
        fitter.addPoint(pos.z(), pos.w(), weight.w());

        break; 

      default:
        THROW_IA("fitCoordinate must be XZ, YZ or TZ");
        break;
      }
    }

    fitter.fit();

    switch (fitCoordinate) { 
    case FitCoordinate::XZ:
      params[kLoc0] = fitter.offset();
      params[kSlopeLoc0] = fitter.slope();
              
      paramsCov(kLoc0, kLoc0) = fitter.varOffset();
      paramsCov(kSlopeLoc0, kSlopeLoc0) = fitter.varSlope();
      paramsCov(kLoc0, kSlopeLoc0) = paramsCov(kSlopeLoc0, kLoc0) = paramsCov(kSlopeLoc1, kLoc1) = fitter.cov();

      track.setGlobalState(TrackState(params, paramsCov));
      track.setGoodnessOfFit(fitter.chi2(), fitter.dof());
        
      break;

    case FitCoordinate::YZ:
      params[kLoc1] = fitter.offset();
      params[kSlopeLoc1] = fitter.slope();
              
      paramsCov(kLoc1, kLoc1) = fitter.varOffset();
      paramsCov(kSlopeLoc1, kSlopeLoc1) = fitter.varSlope();
      paramsCov(kLoc1, kSlopeLoc1) = paramsCov(kSlopeLoc1, kLoc1) = fitter.cov();

      track.setGlobalState(TrackState(params, paramsCov));
      track.setGoodnessOfFit(fitter.chi2(), fitter.dof());

      break;

    case FitCoordinate::TZ:
      {
      params = {track.globalState().loc0(), 
      track.globalState().loc1(), 
      fitter.offset(), 
      track.globalState().slopeLoc0(), 
      track.globalState().slopeLoc1(), 
      fitter.slope()};
          
      const auto& cov = track.globalState().cov();
      paramsCov(kLoc0, kLoc0) = cov(kLoc0,kLoc0);
      paramsCov(kSlopeLoc0, kSlopeLoc0) = cov(kSlopeLoc0, kSlopeLoc0);
      paramsCov(kLoc0, kSlopeLoc0) = paramsCov(kSlopeLoc0, kLoc0) = cov(kLoc0, kSlopeLoc0);

      paramsCov(kLoc1, kLoc1) = cov(kLoc0,kLoc1);
      paramsCov(kSlopeLoc1, kSlopeLoc1) = cov(kSlopeLoc1, kSlopeLoc1);
      paramsCov(kLoc1, kSlopeLoc1) = paramsCov(kSlopeLoc1, kLoc1) = cov(kLoc1, kSlopeLoc1);
        
      paramsCov(kTime, kTime) = fitter.varOffset();
      paramsCov(kSlopeTime, kSlopeTime) = fitter.varSlope();
      paramsCov(kTime, kSlopeTime) = paramsCov(kSlopeTime, kTime) = fitter.cov();

      track.setGlobalState(TrackState(params, paramsCov));
      track.setGoodnessOfFit(track.chi2() + fitter.chi2(), 
      track.degreesOfFreedom() + fitter.dof());
      }
      break;

    default:
      THROW_IA("fitCoordinate must be XZ, YZ or TZ");
      break;
    }
}  

/**
 * Returns the track with the lowest reduced chi2 from the given vector.
*/
Track getBestTrack(const std::vector<Track>& trackVector) {
  auto it = std::min_element(
    trackVector.begin(),
    trackVector.end(),
    [] (const auto& a, const auto& b) {
      return a.chi2()/a.degreesOfFreedom() < b.chi2()/b.degreesOfFreedom();
    }
  );
  return *it;
}

/**
 * Removes the clusters used in the last added track.
*/
void eraseBest(std::vector<std::vector<RecoCluster>>& clusterArray, const Track& bestTrack) {
  for (Index planeId(0); planeId < clusterArray.size(); planeId++) {
    auto& clusters = clusterArray.at(planeId);
    if (not bestTrack.hasClusterOn(planeId)) {
      continue;
    }
    auto cluIdToDelete = bestTrack.getClusterOn(planeId);

    // searchs for the cluster to delete in "clusters", by comparing their indices
    auto it = std::find_if(
      clusters.begin(),
      clusters.end(),
      [&cluIdToDelete] (const auto& c) {
        return c.getClusterIndex() == cluIdToDelete;
      }
    );
    clusters.erase(it);
  }
}

/**
 * Given an array of clusters, while the number of sensors with clusters is greater than clusterMin, 
 * searchs for all possible cluster combinations (track candidates), make a linear fit for all candidates,
 * adds the track with the best goodness of the fit in a dedicated vector and erases the best track's clusters 
 * from the clusters array.
*/
void SciFiTracker::performAnalysis(std::vector<std::vector<RecoCluster>>& clusterArray, RecoEvent& event, std::vector<Track>& bestTracks,
    const std::vector<Index>& trackingIndices, const unsigned int& clusterMin, const FitCoordinate& fitCoordinate) const {
  while (countActivatedSensors(clusterArray, trackingIndices) >= clusterMin) {

    //Puts all possible track candidates in vector
    std::vector<Track> tracks(getCandidateTracks(clusterArray, trackingIndices));

    // fit track: after this tracks will have a valid track state
    for (auto& track : tracks) {  
      fitTrack(track, event, fitCoordinate);
    }

    // put the best track in the dedicated vector
    bestTracks.push_back(getBestTrack(tracks));

    // Erase the best track from the cluster array
    eraseBest(clusterArray, bestTracks.back());
  }
}

  /**
   * sets the global state of the XZ and YZ tracks association
   */
void mergeGlobalState(const Track& TrackX, const Track& TrackY, Track& trackXY) {
  const Vector6 paramsXY{TrackX.globalState().loc0(), TrackY.globalState().loc1(), 0.,
  TrackX.globalState().slopeLoc0(), TrackY.globalState().slopeLoc1() , 0.};
  SymMatrix6 paramsXYCov = SymMatrix6::Zero();
  
  const auto& covX = TrackX.globalState().cov();
  paramsXYCov(kLoc0, kLoc0) = covX(kLoc0,kLoc0);
  paramsXYCov(kSlopeLoc0, kSlopeLoc0) = covX(kSlopeLoc0, kSlopeLoc0);
  paramsXYCov(kLoc0, kSlopeLoc0) = paramsXYCov(kSlopeLoc0, kLoc0) = covX(kLoc0, kSlopeLoc0);

  const auto& covY = TrackY.globalState().cov();
  paramsXYCov(kLoc1, kLoc1) = covY(kLoc1, kLoc1);
  paramsXYCov(kSlopeLoc1, kSlopeLoc1) = covY(kSlopeLoc1, kSlopeLoc1);
  paramsXYCov(kLoc1, kSlopeLoc1) = paramsXYCov(kSlopeLoc1, kLoc1) = covY(kLoc1, kSlopeLoc1);

  trackXY.setGlobalState(TrackState(paramsXY, paramsXYCov));
  trackXY.setGoodnessOfFit(TrackX.chi2() + TrackY.chi2(), TrackX.degreesOfFreedom()
   + TrackY.degreesOfFreedom());
}

/**
   * returns the XZ and YZ tracks association  into a single track in XYZ space
   */
Track getMergeTrack(const Track& trackX, const Track& trackY, const std::vector<Index>& xIndices, const std::vector<Index>& yIndices) {
  Track trackXY;
  for (const auto& index : xIndices) {
    if (trackX.hasClusterOn(index)) {
      trackXY.addCluster(index, trackX.getClusterOn(index));
    }
  }

  for (const auto& index : yIndices) {
    if (trackY.hasClusterOn(index)) {
      trackXY.addCluster(index, trackY.getClusterOn(index));
    }
  }
  mergeGlobalState(trackX, trackY, trackXY);
  return trackXY;
}


bool SciFiTracker::process(RecoEvent& event) const {

  unsigned int nClustersTotal{0};
  unsigned int nTrkCandidatesTotal{1};
  for (const auto& se : event) {
    nClustersTotal += se.numClusters();
    nTrkCandidatesTotal *= se.numClusters() > 0 ? se.numClusters() : 1;
  }

  if (nClustersTotal > m_eventNClusterLimit) {
    return true;
  }
    
  // Puts the X and Y indicied of the clusters in dedicated vectors
  std::vector<Index> trackingSensorIdsX;
  std::vector<Index> trackingSensorIdsY;
  for (auto index : m_trackingSensorIds) {
    if (std::abs(m_device.geometry().getPlane(index).linearToGlobal()(0, 0)) > 0.5) {
      trackingSensorIdsX.push_back(index);
    } else {
      trackingSensorIdsY.push_back(index);
    }
  }

  //Sorts the sensor planes indicies correctly along the beam
  sortAlongBeam(m_device.geometry(), trackingSensorIdsX);
  sortAlongBeam(m_device.geometry(), trackingSensorIdsY);


  // Copies all clusters of the event in an array
  std::vector<std::vector<RecoCluster>> clusterArray;
  for (Index planeId(0); planeId < event.numSensorEvents(); planeId++) {
    clusterArray.emplace_back();
    for (Index clusterId(0); clusterId < event.getSensorEvent(planeId).numClusters(); clusterId++){
      clusterArray.at(planeId).push_back(event.getSensorEvent(planeId).getCluster(clusterId));
    }
  }

  // Creation of the vectors which will contain the retained track candidates
  std::vector<Track> bestTracksX;
  std::vector<Track> bestTracksY;
  
  //Gets the best tracks for the XZ plane, putting them in bestTracksX
  SciFiTracker::performAnalysis(clusterArray, event, bestTracksX, trackingSensorIdsX, m_clustersMin, FitCoordinate::XZ);
  
  
  //Gets the best tracks for the YZ plane, putting them in bestTracksY
  SciFiTracker::performAnalysis(clusterArray, event, bestTracksY, trackingSensorIdsY, m_clustersMin, FitCoordinate::YZ);


  // Associates the XZ and YZ tracks if possible
  if ((bestTracksX.size() > 0) and (bestTracksY.size() > 0)){
    unsigned int nTracks(std::min(bestTracksX.size(), bestTracksY.size()));
    if (nTracks > m_tracksMax) {
      WARN("Event {}: found {} tracks. Limiting to {}.", event.evtNumber(), nTracks, m_tracksMax);
      nTracks = m_tracksMax;
    }
    for (unsigned int trId(0); trId < nTracks; trId++) {

      Track track(getMergeTrack(bestTracksX.at(trId), bestTracksY.at(trId), trackingSensorIdsX, trackingSensorIdsY));

      correctClustersTime(event, track, m_device);

      SciFiTracker::fitTrack(track, event, FitCoordinate::TZ);

      event.addTrack(track);

      addLocalStates(event, m_device, track, trId);

      matchClusters(m_matchingSensorIds, event, trId);
    }
  }
  
  
  return true;
}

} // namespace reco