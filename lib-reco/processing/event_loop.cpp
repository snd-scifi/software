#include <filesystem>

#include "io/reco_tofpet_event_reader_root.hpp"
#include "io/reco_event_reader_root.hpp"
#include "io/reco_event_writer_root.hpp"
#include "processing/ecal_simple_tracker.hpp"
#include "processing/scifi_simple_tracker.hpp"
#include "processing/scifi_tracker.hpp"
#include "processing/track_event_filter.hpp"
#include "processing/scifi_clusterizer.hpp"
#include "processing/non_clusterizer.hpp"
#include "utils/logger.hpp"
#include "utils/progress.hpp"
#include "utils/timer.hpp"

#include "event_loop.hpp"

using json = nlohmann::json;
namespace fs = std::filesystem;

namespace reco {

#ifdef USE_ROOT

EventLoop::EventLoop(const nlohmann::json& configuration, const std::string& inputPath, const std::string& outputPath, const uint64_t num, const uint64_t skip, const bool noProgress, const InputDataType dataType) try
  : m_dev(reco::Device::fromFile(
      configuration.value("device_file", "device.toml"),
      configuration.value("geometry_file", "geometry.toml"),
      dataType == InputDataType::RawRoot ? fs::path(inputPath) / "board_mapping.json" : "")
    )
  , m_reader(getReader(dataType, inputPath, m_dev))
  , m_writer(std::make_unique<RecoEventWriterRoot>(outputPath, m_dev, configuration.at("event_writer")))
  , m_maxEvents{num}
  , m_skipEvents{skip}
  , m_showProgress{!noProgress}
{
  m_reader->skip(skip);

  static constexpr const char* addingSensorProcessorString = "Adding {} processor for sensor {}. Settings: {}";
  static constexpr const char* addingProcessorString = "Adding {} processor. Settings: {}";

  for (unsigned int i = 0; i < m_dev.numSensors(); i++) {
    m_sensorProcessors.emplace_back();
    const auto& sensorConf = m_dev.getSensor(i).configuration();
    for (const auto& sensorProcessor : sensorConf.value("sensor_processors", std::vector<std::string>())) {
      const auto settings = sensorConf.value(sensorProcessor, json::object({}));
      if (sensorProcessor == "scifi_clusterizer") {
        m_sensorProcessors.back().push_back(std::make_unique<SciFiClusterizer>(settings));
        NOTICE(addingSensorProcessorString, sensorProcessor, i, settings);
      }
      else if (sensorProcessor == "non_clusterizer") {
        m_sensorProcessors.back().push_back(std::make_unique<NonClusterizer>(settings));
        NOTICE(addingSensorProcessorString, sensorProcessor, i, settings);
      }
      else {
        WARN("Unknown sensor processor '{}'.", sensorProcessor);
      }
    }
  }

  json processorsConfiguration;
  try {
    processorsConfiguration = configuration.at("event_processors");
  }
  catch (const json::out_of_range& e) {
    WARN("The 'event_processors' section is missing in the configuration file.");
    return;
  }
  
  std::vector<std::string> processors;
  try {
    processors = processorsConfiguration.at("processors").get<std::vector<std::string>>();
  }
  catch (const json::out_of_range& e) {
    WARN("No processors added. The processors configuration does not contain the list of processors.");
  }

  for (const auto& processor : processors) {
    try {
      const auto settings = processorsConfiguration.value(processor, json::object({}));
      if (processor == "scifi_simple_tracker") {
        m_processors.push_back(std::make_unique<SciFiSimpleTracker>(m_dev, settings));
        NOTICE(addingProcessorString, processor, settings);
      }
      else if (processor == "scifi_tracker") {
        m_processors.push_back(std::make_unique<SciFiTracker>(m_dev, settings));
        NOTICE(addingProcessorString, processor, settings);
      }
      else if (processor == "ecal_simple_tracker") {
        m_processors.push_back(std::make_unique<EcalSimpleTracker>(m_dev, settings));
        NOTICE(addingProcessorString, processor, settings);
      }
      else if (processor == "track_event_filter") {
        m_processors.push_back(std::make_unique<TrackEventFilter>(settings));
        NOTICE(addingProcessorString, processor, settings);
      }
      else {
        WARN("Unknown processor '{}'.", processor);
      }
    }
    catch (const std::runtime_error& e) {
      WARN("Error adding processor '{}' (not added): {}", processor, e.what());
    }
    catch (const json::out_of_range& e) {
      WARN("Error adding processor '{}' (not added): {}", processor, e.what());
    }
  }
  

}
catch (const json::out_of_range& e) {
  const std::string what = e.what();
  if (what.find("key 'event_writer' not found") != std::string::npos) {
    ERROR("Section 'event_writer' is missing in the configuration file! Aborting...");
    std::abort();
  }
  
}

#else

EventLoop::EventLoop(const nlohmann::json& configuration, const std::string& inputPath, const std::string& outputPath, const uint64_t num, const uint64_t skip, const bool noProgress, const InputDataType dataType)
  : m_dev(reco::Device::fromFile(configuration.at("device_file").get<std::string>(), configuration.at("geometry_file").get<std::string>(), fs::path(inputPath) / "board_mapping.json"))
  , m_maxEvents{num}
  , m_skipEvents{skip}
  , m_showProgress{!noProgress}
{
  THROW_RE("Software was compiled without ROOT enabled. Call cmake with option -DUSE_ROOT=ON");
}

#endif // USE_ROOT

std::unique_ptr<RecoEventReader> EventLoop::getReader(const InputDataType dataType, const std::string& inputPath, const Device& device) {
  switch (dataType) {
#ifdef USE_ROOT
  case InputDataType::RawRoot:
    return std::make_unique<RecoTofpetEventReaderRoot>(inputPath, device, "event_data");
  case InputDataType::ProcessedRoot:
    return std::make_unique<RecoEventReaderRoot>(inputPath, device);
#else
  case InputDataType::RawRoot:
  case InputDataType::ProcessedRoot:
    THROW_RE("Software was compiled without ROOT enabled. Call cmake with option -DUSE_ROOT=ON");
#endif // USE_ROOT
  default:
    THROW_RE("Unknown InputDataType {}", dataType);
  }
}

void EventLoop::loop() {
  // TofpetEvent e;
  reco::RecoEvent scifiEvent(m_dev);
  uint64_t cnt{0};
  const auto totalEvents = std::min(m_reader->numEvents() - m_skipEvents, m_maxEvents);
  Progress progress(m_showProgress ? totalEvents : 0);
  progress.update(0);
  Timer t;
  while (m_reader->read(scifiEvent)) {
    if (m_maxEvents <= cnt) {
      break;
    }
    // reco::RecoEvent scifiEvent(e, m_dev);

    for (unsigned int i = 0; i < m_dev.numSensors(); i++) {
      auto& sensorEvent = scifiEvent.getSensorEvent(i);
      for (auto& sensorProcessor : m_sensorProcessors.at(i)) {
        sensorProcessor->process(sensorEvent);
      }
    }
    
    bool saveEvent{true};
    for (auto& processor : m_processors) {
      saveEvent = saveEvent && processor->process(scifiEvent);
      if (!saveEvent) {
        break;
      }
    }

    // if (scifiEvent.evtNumber() % 10000 == 0)
    //   NOTICE  ("Event {}, timestamp: {}", scifiEvent.evtNumber(), scifiEvent.timestamp());
    
    if (saveEvent) {
      m_writer->append(scifiEvent);
      cnt++;
    }
    progress.update(cnt);
  }
  progress.clear();
  NOTICE("Processed {} events in {:.0f} seconds.", cnt, t.elapsed());
}


std::istream& operator>> (std::istream& is, EventLoop::InputDataType& dataType) {
  std::string tmp;
  is >> tmp;

  //make tmp lowercase
  std::transform(tmp.begin(), tmp.end(), tmp.begin(),
    [](unsigned char c){ return std::tolower(c); });
  
  std::unordered_map<std::string, EventLoop::InputDataType> map{
    {"raw_root", EventLoop::InputDataType::RawRoot},
    {"processed_root", EventLoop::InputDataType::ProcessedRoot},
  };

  auto it = map.find(tmp);
  if (it != map.end()) {
      dataType = it->second;
  }
  else {
      THROW_IA("unknown logger level: {}", tmp);
  }

  return is;
}

std::ostream& operator<< (std::ostream& os, const EventLoop::InputDataType& dataType) {
  switch (dataType) {
  case EventLoop::InputDataType::RawRoot:
    return os << "raw_root";
  case EventLoop::InputDataType::ProcessedRoot:
    return os << "processed_root";
  default:
    return os;
  }
}

} // namespace reco
